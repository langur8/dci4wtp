<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPDISCOUNT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPDISCOUNT[] $D4WTPDISCOUNT
     */
    protected $D4WTPDISCOUNT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPDISCOUNT[]
     */
    public function getD4WTPDISCOUNT()
    {
      return $this->D4WTPDISCOUNT;
    }

    /**
     * @param D4WTPDISCOUNT[] $D4WTPDISCOUNT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPDISCOUNT
     */
    public function setD4WTPDISCOUNT(array $D4WTPDISCOUNT = null)
    {
      $this->D4WTPDISCOUNT = $D4WTPDISCOUNT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPDISCOUNT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPDISCOUNT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPDISCOUNT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPDISCOUNT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPDISCOUNT[] = $value;
      } else {
        $this->D4WTPDISCOUNT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPDISCOUNT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPDISCOUNT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPDISCOUNT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPDISCOUNT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPDISCOUNT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPDISCOUNT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPDISCOUNT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPDISCOUNT);
    }

}
