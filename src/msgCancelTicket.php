<?php

namespace Axess\Dci4Wtp;

class msgCancelTicket
{

    /**
     * @var D4WTPMSGCANCELTICKETREQUEST $i_ctMsgCancelReq
     */
    protected $i_ctMsgCancelReq = null;

    /**
     * @param D4WTPMSGCANCELTICKETREQUEST $i_ctMsgCancelReq
     */
    public function __construct($i_ctMsgCancelReq)
    {
      $this->i_ctMsgCancelReq = $i_ctMsgCancelReq;
    }

    /**
     * @return D4WTPMSGCANCELTICKETREQUEST
     */
    public function getI_ctMsgCancelReq()
    {
      return $this->i_ctMsgCancelReq;
    }

    /**
     * @param D4WTPMSGCANCELTICKETREQUEST $i_ctMsgCancelReq
     * @return \Axess\Dci4Wtp\msgCancelTicket
     */
    public function setI_ctMsgCancelReq($i_ctMsgCancelReq)
    {
      $this->i_ctMsgCancelReq = $i_ctMsgCancelReq;
      return $this;
    }

}
