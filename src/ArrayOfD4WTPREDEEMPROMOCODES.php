<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPREDEEMPROMOCODES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPREDEEMPROMOCODES[] $D4WTPREDEEMPROMOCODES
     */
    protected $D4WTPREDEEMPROMOCODES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPREDEEMPROMOCODES[]
     */
    public function getD4WTPREDEEMPROMOCODES()
    {
      return $this->D4WTPREDEEMPROMOCODES;
    }

    /**
     * @param D4WTPREDEEMPROMOCODES[] $D4WTPREDEEMPROMOCODES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPREDEEMPROMOCODES
     */
    public function setD4WTPREDEEMPROMOCODES(array $D4WTPREDEEMPROMOCODES = null)
    {
      $this->D4WTPREDEEMPROMOCODES = $D4WTPREDEEMPROMOCODES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPREDEEMPROMOCODES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPREDEEMPROMOCODES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPREDEEMPROMOCODES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPREDEEMPROMOCODES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPREDEEMPROMOCODES[] = $value;
      } else {
        $this->D4WTPREDEEMPROMOCODES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPREDEEMPROMOCODES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPREDEEMPROMOCODES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPREDEEMPROMOCODES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPREDEEMPROMOCODES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPREDEEMPROMOCODES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPREDEEMPROMOCODES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPREDEEMPROMOCODES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPREDEEMPROMOCODES);
    }

}
