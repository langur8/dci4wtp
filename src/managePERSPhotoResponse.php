<?php

namespace Axess\Dci4Wtp;

class managePERSPhotoResponse
{

    /**
     * @var D4WTPMNGPERSPHOTORESULT $managePERSPhotoResult
     */
    protected $managePERSPhotoResult = null;

    /**
     * @param D4WTPMNGPERSPHOTORESULT $managePERSPhotoResult
     */
    public function __construct($managePERSPhotoResult)
    {
      $this->managePERSPhotoResult = $managePERSPhotoResult;
    }

    /**
     * @return D4WTPMNGPERSPHOTORESULT
     */
    public function getManagePERSPhotoResult()
    {
      return $this->managePERSPhotoResult;
    }

    /**
     * @param D4WTPMNGPERSPHOTORESULT $managePERSPhotoResult
     * @return \Axess\Dci4Wtp\managePERSPhotoResponse
     */
    public function setManagePERSPhotoResult($managePERSPhotoResult)
    {
      $this->managePERSPhotoResult = $managePERSPhotoResult;
      return $this;
    }

}
