<?php

namespace Axess\Dci4Wtp;

class SHOPCBOOKTIMESLOT
{

    /**
     * @var float $NCONFIGLIMITNO
     */
    protected $NCONFIGLIMITNO = null;

    /**
     * @var float $NCONFIGNO
     */
    protected $NCONFIGNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSLOTLFDNR
     */
    protected $NSLOTLFDNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCONFIGLIMITNO()
    {
      return $this->NCONFIGLIMITNO;
    }

    /**
     * @param float $NCONFIGLIMITNO
     * @return \Axess\Dci4Wtp\SHOPCBOOKTIMESLOT
     */
    public function setNCONFIGLIMITNO($NCONFIGLIMITNO)
    {
      $this->NCONFIGLIMITNO = $NCONFIGLIMITNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONFIGNO()
    {
      return $this->NCONFIGNO;
    }

    /**
     * @param float $NCONFIGNO
     * @return \Axess\Dci4Wtp\SHOPCBOOKTIMESLOT
     */
    public function setNCONFIGNO($NCONFIGNO)
    {
      $this->NCONFIGNO = $NCONFIGNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\SHOPCBOOKTIMESLOT
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSLOTLFDNR()
    {
      return $this->NSLOTLFDNR;
    }

    /**
     * @param float $NSLOTLFDNR
     * @return \Axess\Dci4Wtp\SHOPCBOOKTIMESLOT
     */
    public function setNSLOTLFDNR($NSLOTLFDNR)
    {
      $this->NSLOTLFDNR = $NSLOTLFDNR;
      return $this;
    }

}
