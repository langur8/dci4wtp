<?php

namespace Axess\Dci4Wtp;

class D4WTPPROMPTTYPELIST
{

    /**
     * @var float $NPROMPTTYPENO
     */
    protected $NPROMPTTYPENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPROMPTTYPENO()
    {
      return $this->NPROMPTTYPENO;
    }

    /**
     * @param float $NPROMPTTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPROMPTTYPELIST
     */
    public function setNPROMPTTYPENO($NPROMPTTYPENO)
    {
      $this->NPROMPTTYPENO = $NPROMPTTYPENO;
      return $this;
    }

}
