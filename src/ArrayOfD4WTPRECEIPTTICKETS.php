<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPRECEIPTTICKETS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPRECEIPTTICKETS[] $D4WTPRECEIPTTICKETS
     */
    protected $D4WTPRECEIPTTICKETS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPRECEIPTTICKETS[]
     */
    public function getD4WTPRECEIPTTICKETS()
    {
      return $this->D4WTPRECEIPTTICKETS;
    }

    /**
     * @param D4WTPRECEIPTTICKETS[] $D4WTPRECEIPTTICKETS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPRECEIPTTICKETS
     */
    public function setD4WTPRECEIPTTICKETS(array $D4WTPRECEIPTTICKETS = null)
    {
      $this->D4WTPRECEIPTTICKETS = $D4WTPRECEIPTTICKETS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPRECEIPTTICKETS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPRECEIPTTICKETS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPRECEIPTTICKETS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPRECEIPTTICKETS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPRECEIPTTICKETS[] = $value;
      } else {
        $this->D4WTPRECEIPTTICKETS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPRECEIPTTICKETS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPRECEIPTTICKETS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPRECEIPTTICKETS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPRECEIPTTICKETS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPRECEIPTTICKETS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPRECEIPTTICKETS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPRECEIPTTICKETS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPRECEIPTTICKETS);
    }

}
