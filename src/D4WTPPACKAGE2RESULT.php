<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGE2RESULT
{

    /**
     * @var ArrayOfD4WTPPACKAGE2 $ACTPACKAGE2
     */
    protected $ACTPACKAGE2 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPACKAGE2
     */
    public function getACTPACKAGE2()
    {
      return $this->ACTPACKAGE2;
    }

    /**
     * @param ArrayOfD4WTPPACKAGE2 $ACTPACKAGE2
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE2RESULT
     */
    public function setACTPACKAGE2($ACTPACKAGE2)
    {
      $this->ACTPACKAGE2 = $ACTPACKAGE2;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
