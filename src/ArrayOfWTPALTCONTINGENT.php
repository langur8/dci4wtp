<?php

namespace Axess\Dci4Wtp;

class ArrayOfWTPALTCONTINGENT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var WTPALTCONTINGENT[] $WTPALTCONTINGENT
     */
    protected $WTPALTCONTINGENT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return WTPALTCONTINGENT[]
     */
    public function getWTPALTCONTINGENT()
    {
      return $this->WTPALTCONTINGENT;
    }

    /**
     * @param WTPALTCONTINGENT[] $WTPALTCONTINGENT
     * @return \Axess\Dci4Wtp\ArrayOfWTPALTCONTINGENT
     */
    public function setWTPALTCONTINGENT(array $WTPALTCONTINGENT = null)
    {
      $this->WTPALTCONTINGENT = $WTPALTCONTINGENT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->WTPALTCONTINGENT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return WTPALTCONTINGENT
     */
    public function offsetGet($offset)
    {
      return $this->WTPALTCONTINGENT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param WTPALTCONTINGENT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->WTPALTCONTINGENT[] = $value;
      } else {
        $this->WTPALTCONTINGENT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->WTPALTCONTINGENT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return WTPALTCONTINGENT Return the current element
     */
    public function current()
    {
      return current($this->WTPALTCONTINGENT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->WTPALTCONTINGENT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->WTPALTCONTINGENT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->WTPALTCONTINGENT);
    }

    /**
     * Countable implementation
     *
     * @return WTPALTCONTINGENT Return count of elements
     */
    public function count()
    {
      return count($this->WTPALTCONTINGENT);
    }

}
