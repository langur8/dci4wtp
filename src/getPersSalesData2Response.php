<?php

namespace Axess\Dci4Wtp;

class getPersSalesData2Response
{

    /**
     * @var D4WTPPERSSALESDATARESULT2 $getPersSalesData2Result
     */
    protected $getPersSalesData2Result = null;

    /**
     * @param D4WTPPERSSALESDATARESULT2 $getPersSalesData2Result
     */
    public function __construct($getPersSalesData2Result)
    {
      $this->getPersSalesData2Result = $getPersSalesData2Result;
    }

    /**
     * @return D4WTPPERSSALESDATARESULT2
     */
    public function getGetPersSalesData2Result()
    {
      return $this->getPersSalesData2Result;
    }

    /**
     * @param D4WTPPERSSALESDATARESULT2 $getPersSalesData2Result
     * @return \Axess\Dci4Wtp\getPersSalesData2Response
     */
    public function setGetPersSalesData2Result($getPersSalesData2Result)
    {
      $this->getPersSalesData2Result = $getPersSalesData2Result;
      return $this;
    }

}
