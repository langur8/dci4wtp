<?php

namespace Axess\Dci4Wtp;

class D4WTPREBOOKREQUEST
{

    /**
     * @var ArrayOfD4WTPTICKETSALE $ACTTICKETSALE
     */
    protected $ACTTICKETSALE = null;

    /**
     * @var D4WTPCREDITCARDDATA $CTCREDITCARDDATA
     */
    protected $CTCREDITCARDDATA = null;

    /**
     * @var float $NAPPTRANSLOGID
     */
    protected $NAPPTRANSLOGID = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKETSALE
     */
    public function getACTTICKETSALE()
    {
      return $this->ACTTICKETSALE;
    }

    /**
     * @param ArrayOfD4WTPTICKETSALE $ACTTICKETSALE
     * @return \Axess\Dci4Wtp\D4WTPREBOOKREQUEST
     */
    public function setACTTICKETSALE($ACTTICKETSALE)
    {
      $this->ACTTICKETSALE = $ACTTICKETSALE;
      return $this;
    }

    /**
     * @return D4WTPCREDITCARDDATA
     */
    public function getCTCREDITCARDDATA()
    {
      return $this->CTCREDITCARDDATA;
    }

    /**
     * @param D4WTPCREDITCARDDATA $CTCREDITCARDDATA
     * @return \Axess\Dci4Wtp\D4WTPREBOOKREQUEST
     */
    public function setCTCREDITCARDDATA($CTCREDITCARDDATA)
    {
      $this->CTCREDITCARDDATA = $CTCREDITCARDDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAPPTRANSLOGID()
    {
      return $this->NAPPTRANSLOGID;
    }

    /**
     * @param float $NAPPTRANSLOGID
     * @return \Axess\Dci4Wtp\D4WTPREBOOKREQUEST
     */
    public function setNAPPTRANSLOGID($NAPPTRANSLOGID)
    {
      $this->NAPPTRANSLOGID = $NAPPTRANSLOGID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPREBOOKREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
