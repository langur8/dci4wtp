<?php

namespace Axess\Dci4Wtp;

class managePERSPhoto
{

    /**
     * @var D4WTPMNGPERSPHOTOREQUEST $i_ctmngPERSPhotoReq
     */
    protected $i_ctmngPERSPhotoReq = null;

    /**
     * @param D4WTPMNGPERSPHOTOREQUEST $i_ctmngPERSPhotoReq
     */
    public function __construct($i_ctmngPERSPhotoReq)
    {
      $this->i_ctmngPERSPhotoReq = $i_ctmngPERSPhotoReq;
    }

    /**
     * @return D4WTPMNGPERSPHOTOREQUEST
     */
    public function getI_ctmngPERSPhotoReq()
    {
      return $this->i_ctmngPERSPhotoReq;
    }

    /**
     * @param D4WTPMNGPERSPHOTOREQUEST $i_ctmngPERSPhotoReq
     * @return \Axess\Dci4Wtp\managePERSPhoto
     */
    public function setI_ctmngPERSPhotoReq($i_ctmngPERSPhotoReq)
    {
      $this->i_ctmngPERSPhotoReq = $i_ctmngPERSPhotoReq;
      return $this;
    }

}
