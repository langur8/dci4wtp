<?php

namespace Axess\Dci4Wtp;

class getPersonTypes3Response
{

    /**
     * @var D4WTPPERSONTYPE3RESULT $getPersonTypes3Result
     */
    protected $getPersonTypes3Result = null;

    /**
     * @param D4WTPPERSONTYPE3RESULT $getPersonTypes3Result
     */
    public function __construct($getPersonTypes3Result)
    {
      $this->getPersonTypes3Result = $getPersonTypes3Result;
    }

    /**
     * @return D4WTPPERSONTYPE3RESULT
     */
    public function getGetPersonTypes3Result()
    {
      return $this->getPersonTypes3Result;
    }

    /**
     * @param D4WTPPERSONTYPE3RESULT $getPersonTypes3Result
     * @return \Axess\Dci4Wtp\getPersonTypes3Response
     */
    public function setGetPersonTypes3Result($getPersonTypes3Result)
    {
      $this->getPersonTypes3Result = $getPersonTypes3Result;
      return $this;
    }

}
