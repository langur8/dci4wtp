<?php

namespace Axess\Dci4Wtp;

class D4WTPPROMOCODERESULT
{

    /**
     * @var float $FDISCOUNTABS
     */
    protected $FDISCOUNTABS = null;

    /**
     * @var float $FDISCOUNTPERCENT
     */
    protected $FDISCOUNTPERCENT = null;

    /**
     * @var float $FORIGINALPRICE
     */
    protected $FORIGINALPRICE = null;

    /**
     * @var float $FPRICEDISCOUNTED
     */
    protected $FPRICEDISCOUNTED = null;

    /**
     * @var float $NCOUNTCURRPROTRANS
     */
    protected $NCOUNTCURRPROTRANS = null;

    /**
     * @var float $NCOUNTFREE
     */
    protected $NCOUNTFREE = null;

    /**
     * @var float $NCOUNTPROTRANSTOTAL
     */
    protected $NCOUNTPROTRANSTOTAL = null;

    /**
     * @var float $NCOUNTTOTAL
     */
    protected $NCOUNTTOTAL = null;

    /**
     * @var float $NCOUNTTRANSFREE
     */
    protected $NCOUNTTRANSFREE = null;

    /**
     * @var float $NCOUNTTRANSTOTAL
     */
    protected $NCOUNTTRANSTOTAL = null;

    /**
     * @var float $NCURRENCY
     */
    protected $NCURRENCY = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPROMOCODETYPENO
     */
    protected $NPROMOCODETYPENO = null;

    /**
     * @var float $NPROMONO
     */
    protected $NPROMONO = null;

    /**
     * @var string $SZCURRENCY
     */
    protected $SZCURRENCY = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFDISCOUNTABS()
    {
      return $this->FDISCOUNTABS;
    }

    /**
     * @param float $FDISCOUNTABS
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setFDISCOUNTABS($FDISCOUNTABS)
    {
      $this->FDISCOUNTABS = $FDISCOUNTABS;
      return $this;
    }

    /**
     * @return float
     */
    public function getFDISCOUNTPERCENT()
    {
      return $this->FDISCOUNTPERCENT;
    }

    /**
     * @param float $FDISCOUNTPERCENT
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setFDISCOUNTPERCENT($FDISCOUNTPERCENT)
    {
      $this->FDISCOUNTPERCENT = $FDISCOUNTPERCENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFORIGINALPRICE()
    {
      return $this->FORIGINALPRICE;
    }

    /**
     * @param float $FORIGINALPRICE
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setFORIGINALPRICE($FORIGINALPRICE)
    {
      $this->FORIGINALPRICE = $FORIGINALPRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getFPRICEDISCOUNTED()
    {
      return $this->FPRICEDISCOUNTED;
    }

    /**
     * @param float $FPRICEDISCOUNTED
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setFPRICEDISCOUNTED($FPRICEDISCOUNTED)
    {
      $this->FPRICEDISCOUNTED = $FPRICEDISCOUNTED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTCURRPROTRANS()
    {
      return $this->NCOUNTCURRPROTRANS;
    }

    /**
     * @param float $NCOUNTCURRPROTRANS
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setNCOUNTCURRPROTRANS($NCOUNTCURRPROTRANS)
    {
      $this->NCOUNTCURRPROTRANS = $NCOUNTCURRPROTRANS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTFREE()
    {
      return $this->NCOUNTFREE;
    }

    /**
     * @param float $NCOUNTFREE
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setNCOUNTFREE($NCOUNTFREE)
    {
      $this->NCOUNTFREE = $NCOUNTFREE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTPROTRANSTOTAL()
    {
      return $this->NCOUNTPROTRANSTOTAL;
    }

    /**
     * @param float $NCOUNTPROTRANSTOTAL
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setNCOUNTPROTRANSTOTAL($NCOUNTPROTRANSTOTAL)
    {
      $this->NCOUNTPROTRANSTOTAL = $NCOUNTPROTRANSTOTAL;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTTOTAL()
    {
      return $this->NCOUNTTOTAL;
    }

    /**
     * @param float $NCOUNTTOTAL
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setNCOUNTTOTAL($NCOUNTTOTAL)
    {
      $this->NCOUNTTOTAL = $NCOUNTTOTAL;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTTRANSFREE()
    {
      return $this->NCOUNTTRANSFREE;
    }

    /**
     * @param float $NCOUNTTRANSFREE
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setNCOUNTTRANSFREE($NCOUNTTRANSFREE)
    {
      $this->NCOUNTTRANSFREE = $NCOUNTTRANSFREE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTTRANSTOTAL()
    {
      return $this->NCOUNTTRANSTOTAL;
    }

    /**
     * @param float $NCOUNTTRANSTOTAL
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setNCOUNTTRANSTOTAL($NCOUNTTRANSTOTAL)
    {
      $this->NCOUNTTRANSTOTAL = $NCOUNTTRANSTOTAL;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCURRENCY()
    {
      return $this->NCURRENCY;
    }

    /**
     * @param float $NCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setNCURRENCY($NCURRENCY)
    {
      $this->NCURRENCY = $NCURRENCY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROMOCODETYPENO()
    {
      return $this->NPROMOCODETYPENO;
    }

    /**
     * @param float $NPROMOCODETYPENO
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setNPROMOCODETYPENO($NPROMOCODETYPENO)
    {
      $this->NPROMOCODETYPENO = $NPROMOCODETYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROMONO()
    {
      return $this->NPROMONO;
    }

    /**
     * @param float $NPROMONO
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setNPROMONO($NPROMONO)
    {
      $this->NPROMONO = $NPROMONO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCY()
    {
      return $this->SZCURRENCY;
    }

    /**
     * @param string $SZCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setSZCURRENCY($SZCURRENCY)
    {
      $this->SZCURRENCY = $SZCURRENCY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPROMOCODERESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
