<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCOMPENSATIONTICKET implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCOMPENSATIONTICKET[] $D4WTPCOMPENSATIONTICKET
     */
    protected $D4WTPCOMPENSATIONTICKET = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCOMPENSATIONTICKET[]
     */
    public function getD4WTPCOMPENSATIONTICKET()
    {
      return $this->D4WTPCOMPENSATIONTICKET;
    }

    /**
     * @param D4WTPCOMPENSATIONTICKET[] $D4WTPCOMPENSATIONTICKET
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCOMPENSATIONTICKET
     */
    public function setD4WTPCOMPENSATIONTICKET(array $D4WTPCOMPENSATIONTICKET = null)
    {
      $this->D4WTPCOMPENSATIONTICKET = $D4WTPCOMPENSATIONTICKET;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCOMPENSATIONTICKET[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCOMPENSATIONTICKET
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCOMPENSATIONTICKET[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCOMPENSATIONTICKET $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCOMPENSATIONTICKET[] = $value;
      } else {
        $this->D4WTPCOMPENSATIONTICKET[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCOMPENSATIONTICKET[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCOMPENSATIONTICKET Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCOMPENSATIONTICKET);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCOMPENSATIONTICKET);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCOMPENSATIONTICKET);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCOMPENSATIONTICKET);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCOMPENSATIONTICKET Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCOMPENSATIONTICKET);
    }

}
