<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCOMPANY2RESULT
{

    /**
     * @var ArrayOfD4WTPCOMPANYDATA2 $ACTCOMPANYDATA
     */
    protected $ACTCOMPANYDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCOMPANYDATA2
     */
    public function getACTCOMPANYDATA()
    {
      return $this->ACTCOMPANYDATA;
    }

    /**
     * @param ArrayOfD4WTPCOMPANYDATA2 $ACTCOMPANYDATA
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANY2RESULT
     */
    public function setACTCOMPANYDATA($ACTCOMPANYDATA)
    {
      $this->ACTCOMPANYDATA = $ACTCOMPANYDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANY2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANY2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
