<?php

namespace Axess\Dci4Wtp;

class msgReplaceTicket
{

    /**
     * @var D4WTPMSGREPLACETICKETREQ $i_ctMsgReplaceTicketReq
     */
    protected $i_ctMsgReplaceTicketReq = null;

    /**
     * @param D4WTPMSGREPLACETICKETREQ $i_ctMsgReplaceTicketReq
     */
    public function __construct($i_ctMsgReplaceTicketReq)
    {
      $this->i_ctMsgReplaceTicketReq = $i_ctMsgReplaceTicketReq;
    }

    /**
     * @return D4WTPMSGREPLACETICKETREQ
     */
    public function getI_ctMsgReplaceTicketReq()
    {
      return $this->i_ctMsgReplaceTicketReq;
    }

    /**
     * @param D4WTPMSGREPLACETICKETREQ $i_ctMsgReplaceTicketReq
     * @return \Axess\Dci4Wtp\msgReplaceTicket
     */
    public function setI_ctMsgReplaceTicketReq($i_ctMsgReplaceTicketReq)
    {
      $this->i_ctMsgReplaceTicketReq = $i_ctMsgReplaceTicketReq;
      return $this;
    }

}
