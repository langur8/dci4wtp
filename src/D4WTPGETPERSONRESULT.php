<?php

namespace Axess\Dci4Wtp;

class D4WTPGETPERSONRESULT
{

    /**
     * @var ArrayOfD4WTPERSONDATA $ACTPERSONDATA
     */
    protected $ACTPERSONDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPERSONDATA
     */
    public function getACTPERSONDATA()
    {
      return $this->ACTPERSONDATA;
    }

    /**
     * @param ArrayOfD4WTPERSONDATA $ACTPERSONDATA
     * @return \Axess\Dci4Wtp\D4WTPGETPERSONRESULT
     */
    public function setACTPERSONDATA($ACTPERSONDATA)
    {
      $this->ACTPERSONDATA = $ACTPERSONDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSONRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETPERSONRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
