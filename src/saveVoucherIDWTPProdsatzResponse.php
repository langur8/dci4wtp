<?php

namespace Axess\Dci4Wtp;

class saveVoucherIDWTPProdsatzResponse
{

    /**
     * @var D4WTPRESULT $saveVoucherIDWTPProdsatzResult
     */
    protected $saveVoucherIDWTPProdsatzResult = null;

    /**
     * @param D4WTPRESULT $saveVoucherIDWTPProdsatzResult
     */
    public function __construct($saveVoucherIDWTPProdsatzResult)
    {
      $this->saveVoucherIDWTPProdsatzResult = $saveVoucherIDWTPProdsatzResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSaveVoucherIDWTPProdsatzResult()
    {
      return $this->saveVoucherIDWTPProdsatzResult;
    }

    /**
     * @param D4WTPRESULT $saveVoucherIDWTPProdsatzResult
     * @return \Axess\Dci4Wtp\saveVoucherIDWTPProdsatzResponse
     */
    public function setSaveVoucherIDWTPProdsatzResult($saveVoucherIDWTPProdsatzResult)
    {
      $this->saveVoucherIDWTPProdsatzResult = $saveVoucherIDWTPProdsatzResult;
      return $this;
    }

}
