<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGETARIFF
{

    /**
     * @var ArrayOfD4WTPPACKAGEPOSTARIFFCALC $ACTPACKAGEPOSTARIFFCALC
     */
    protected $ACTPACKAGEPOSTARIFFCALC = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPACKAGEPOSTARIFFCALC
     */
    public function getACTPACKAGEPOSTARIFFCALC()
    {
      return $this->ACTPACKAGEPOSTARIFFCALC;
    }

    /**
     * @param ArrayOfD4WTPPACKAGEPOSTARIFFCALC $ACTPACKAGEPOSTARIFFCALC
     * @return \Axess\Dci4Wtp\D4WTPPACKAGETARIFF
     */
    public function setACTPACKAGEPOSTARIFFCALC($ACTPACKAGEPOSTARIFFCALC)
    {
      $this->ACTPACKAGEPOSTARIFFCALC = $ACTPACKAGEPOSTARIFFCALC;
      return $this;
    }

}
