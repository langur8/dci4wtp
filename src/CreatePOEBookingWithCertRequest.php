<?php

namespace Axess\Dci4Wtp;

class CreatePOEBookingWithCertRequest
{

    /**
     * @var ArrayOfPoeBooking $PoeBookings
     */
    protected $PoeBookings = null;

    /**
     * @var string $qrCodeData
     */
    protected $qrCodeData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfPoeBooking
     */
    public function getPoeBookings()
    {
      return $this->PoeBookings;
    }

    /**
     * @param ArrayOfPoeBooking $PoeBookings
     * @return \Axess\Dci4Wtp\CreatePOEBookingWithCertRequest
     */
    public function setPoeBookings($PoeBookings)
    {
      $this->PoeBookings = $PoeBookings;
      return $this;
    }

    /**
     * @return string
     */
    public function getQrCodeData()
    {
      return $this->qrCodeData;
    }

    /**
     * @param string $qrCodeData
     * @return \Axess\Dci4Wtp\CreatePOEBookingWithCertRequest
     */
    public function setQrCodeData($qrCodeData)
    {
      $this->qrCodeData = $qrCodeData;
      return $this;
    }

}
