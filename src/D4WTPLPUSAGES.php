<?php

namespace Axess\Dci4Wtp;

class D4WTPLPUSAGES
{

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NZUTRNR
     */
    protected $NZUTRNR = null;

    /**
     * @var string $SZTIMESTAMP
     */
    protected $SZTIMESTAMP = null;

    /**
     * @var string $SZZUTRNAME
     */
    protected $SZZUTRNAME = null;

    /**
     * @var string $SZZUTRTYP
     */
    protected $SZZUTRTYP = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPLPUSAGES
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPLPUSAGES
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPLPUSAGES
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNZUTRNR()
    {
      return $this->NZUTRNR;
    }

    /**
     * @param float $NZUTRNR
     * @return \Axess\Dci4Wtp\D4WTPLPUSAGES
     */
    public function setNZUTRNR($NZUTRNR)
    {
      $this->NZUTRNR = $NZUTRNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTIMESTAMP()
    {
      return $this->SZTIMESTAMP;
    }

    /**
     * @param string $SZTIMESTAMP
     * @return \Axess\Dci4Wtp\D4WTPLPUSAGES
     */
    public function setSZTIMESTAMP($SZTIMESTAMP)
    {
      $this->SZTIMESTAMP = $SZTIMESTAMP;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZZUTRNAME()
    {
      return $this->SZZUTRNAME;
    }

    /**
     * @param string $SZZUTRNAME
     * @return \Axess\Dci4Wtp\D4WTPLPUSAGES
     */
    public function setSZZUTRNAME($SZZUTRNAME)
    {
      $this->SZZUTRNAME = $SZZUTRNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZZUTRTYP()
    {
      return $this->SZZUTRTYP;
    }

    /**
     * @param string $SZZUTRTYP
     * @return \Axess\Dci4Wtp\D4WTPLPUSAGES
     */
    public function setSZZUTRTYP($SZZUTRTYP)
    {
      $this->SZZUTRTYP = $SZZUTRTYP;
      return $this;
    }

}
