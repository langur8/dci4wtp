<?php

namespace Axess\Dci4Wtp;

class ADDRESS
{

    /**
     * @var float $NADRNR
     */
    protected $NADRNR = null;

    /**
     * @var float $NADRPOSNR
     */
    protected $NADRPOSNR = null;

    /**
     * @var float $NADRPROJNR
     */
    protected $NADRPROJNR = null;

    /**
     * @var string $SZCITY
     */
    protected $SZCITY = null;

    /**
     * @var string $SZCO
     */
    protected $SZCO = null;

    /**
     * @var string $SZCOUNTRYSHORTNAME
     */
    protected $SZCOUNTRYSHORTNAME = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZFAXNR
     */
    protected $SZFAXNR = null;

    /**
     * @var string $SZPROVINCESHORTNAME
     */
    protected $SZPROVINCESHORTNAME = null;

    /**
     * @var string $SZSTREET
     */
    protected $SZSTREET = null;

    /**
     * @var string $SZSTREET2
     */
    protected $SZSTREET2 = null;

    /**
     * @var string $SZSTREET3
     */
    protected $SZSTREET3 = null;

    /**
     * @var string $SZTELNR
     */
    protected $SZTELNR = null;

    /**
     * @var string $SZZIPCODE
     */
    protected $SZZIPCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNADRNR()
    {
      return $this->NADRNR;
    }

    /**
     * @param float $NADRNR
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setNADRNR($NADRNR)
    {
      $this->NADRNR = $NADRNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNADRPOSNR()
    {
      return $this->NADRPOSNR;
    }

    /**
     * @param float $NADRPOSNR
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setNADRPOSNR($NADRPOSNR)
    {
      $this->NADRPOSNR = $NADRPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNADRPROJNR()
    {
      return $this->NADRPROJNR;
    }

    /**
     * @param float $NADRPROJNR
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setNADRPROJNR($NADRPROJNR)
    {
      $this->NADRPROJNR = $NADRPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCITY()
    {
      return $this->SZCITY;
    }

    /**
     * @param string $SZCITY
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setSZCITY($SZCITY)
    {
      $this->SZCITY = $SZCITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCO()
    {
      return $this->SZCO;
    }

    /**
     * @param string $SZCO
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setSZCO($SZCO)
    {
      $this->SZCO = $SZCO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYSHORTNAME()
    {
      return $this->SZCOUNTRYSHORTNAME;
    }

    /**
     * @param string $SZCOUNTRYSHORTNAME
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setSZCOUNTRYSHORTNAME($SZCOUNTRYSHORTNAME)
    {
      $this->SZCOUNTRYSHORTNAME = $SZCOUNTRYSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFAXNR()
    {
      return $this->SZFAXNR;
    }

    /**
     * @param string $SZFAXNR
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setSZFAXNR($SZFAXNR)
    {
      $this->SZFAXNR = $SZFAXNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPROVINCESHORTNAME()
    {
      return $this->SZPROVINCESHORTNAME;
    }

    /**
     * @param string $SZPROVINCESHORTNAME
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setSZPROVINCESHORTNAME($SZPROVINCESHORTNAME)
    {
      $this->SZPROVINCESHORTNAME = $SZPROVINCESHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREET()
    {
      return $this->SZSTREET;
    }

    /**
     * @param string $SZSTREET
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setSZSTREET($SZSTREET)
    {
      $this->SZSTREET = $SZSTREET;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREET2()
    {
      return $this->SZSTREET2;
    }

    /**
     * @param string $SZSTREET2
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setSZSTREET2($SZSTREET2)
    {
      $this->SZSTREET2 = $SZSTREET2;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREET3()
    {
      return $this->SZSTREET3;
    }

    /**
     * @param string $SZSTREET3
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setSZSTREET3($SZSTREET3)
    {
      $this->SZSTREET3 = $SZSTREET3;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTELNR()
    {
      return $this->SZTELNR;
    }

    /**
     * @param string $SZTELNR
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setSZTELNR($SZTELNR)
    {
      $this->SZTELNR = $SZTELNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZZIPCODE()
    {
      return $this->SZZIPCODE;
    }

    /**
     * @param string $SZZIPCODE
     * @return \Axess\Dci4Wtp\ADDRESS
     */
    public function setSZZIPCODE($SZZIPCODE)
    {
      $this->SZZIPCODE = $SZZIPCODE;
      return $this;
    }

}
