<?php

namespace Axess\Dci4Wtp;

class D4WTPADDDTL4FAMILYDAYSREQ
{

    /**
     * @var float $BREMOVE
     */
    protected $BREMOVE = null;

    /**
     * @var float $NEXTENSIONTARIFFNR
     */
    protected $NEXTENSIONTARIFFNR = null;

    /**
     * @var float $NMASTERJOURNALNR
     */
    protected $NMASTERJOURNALNR = null;

    /**
     * @var float $NMASTERPOSNR
     */
    protected $NMASTERPOSNR = null;

    /**
     * @var float $NMASTERPROJNR
     */
    protected $NMASTERPROJNR = null;

    /**
     * @var float $NNUMBEROFDAYS
     */
    protected $NNUMBEROFDAYS = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBREMOVE()
    {
      return $this->BREMOVE;
    }

    /**
     * @param float $BREMOVE
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYDAYSREQ
     */
    public function setBREMOVE($BREMOVE)
    {
      $this->BREMOVE = $BREMOVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEXTENSIONTARIFFNR()
    {
      return $this->NEXTENSIONTARIFFNR;
    }

    /**
     * @param float $NEXTENSIONTARIFFNR
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYDAYSREQ
     */
    public function setNEXTENSIONTARIFFNR($NEXTENSIONTARIFFNR)
    {
      $this->NEXTENSIONTARIFFNR = $NEXTENSIONTARIFFNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERJOURNALNR()
    {
      return $this->NMASTERJOURNALNR;
    }

    /**
     * @param float $NMASTERJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYDAYSREQ
     */
    public function setNMASTERJOURNALNR($NMASTERJOURNALNR)
    {
      $this->NMASTERJOURNALNR = $NMASTERJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERPOSNR()
    {
      return $this->NMASTERPOSNR;
    }

    /**
     * @param float $NMASTERPOSNR
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYDAYSREQ
     */
    public function setNMASTERPOSNR($NMASTERPOSNR)
    {
      $this->NMASTERPOSNR = $NMASTERPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERPROJNR()
    {
      return $this->NMASTERPROJNR;
    }

    /**
     * @param float $NMASTERPROJNR
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYDAYSREQ
     */
    public function setNMASTERPROJNR($NMASTERPROJNR)
    {
      $this->NMASTERPROJNR = $NMASTERPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNUMBEROFDAYS()
    {
      return $this->NNUMBEROFDAYS;
    }

    /**
     * @param float $NNUMBEROFDAYS
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYDAYSREQ
     */
    public function setNNUMBEROFDAYS($NNUMBEROFDAYS)
    {
      $this->NNUMBEROFDAYS = $NNUMBEROFDAYS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYDAYSREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
