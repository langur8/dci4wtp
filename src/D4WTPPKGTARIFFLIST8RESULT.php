<?php

namespace Axess\Dci4Wtp;

class D4WTPPKGTARIFFLIST8RESULT
{

    /**
     * @var ArrayOfD4WTPPKGTARIFFLIST8 $ACTPKGTARIFFLIST
     */
    protected $ACTPKGTARIFFLIST = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPKGTARIFFLIST8
     */
    public function getACTPKGTARIFFLIST()
    {
      return $this->ACTPKGTARIFFLIST;
    }

    /**
     * @param ArrayOfD4WTPPKGTARIFFLIST8 $ACTPKGTARIFFLIST
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST8RESULT
     */
    public function setACTPKGTARIFFLIST($ACTPKGTARIFFLIST)
    {
      $this->ACTPKGTARIFFLIST = $ACTPKGTARIFFLIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST8RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST8RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
