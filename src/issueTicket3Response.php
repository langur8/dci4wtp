<?php

namespace Axess\Dci4Wtp;

class issueTicket3Response
{

    /**
     * @var D4WTPISSUETICKET3RESULT $issueTicket3Result
     */
    protected $issueTicket3Result = null;

    /**
     * @param D4WTPISSUETICKET3RESULT $issueTicket3Result
     */
    public function __construct($issueTicket3Result)
    {
      $this->issueTicket3Result = $issueTicket3Result;
    }

    /**
     * @return D4WTPISSUETICKET3RESULT
     */
    public function getIssueTicket3Result()
    {
      return $this->issueTicket3Result;
    }

    /**
     * @param D4WTPISSUETICKET3RESULT $issueTicket3Result
     * @return \Axess\Dci4Wtp\issueTicket3Response
     */
    public function setIssueTicket3Result($issueTicket3Result)
    {
      $this->issueTicket3Result = $issueTicket3Result;
      return $this;
    }

}
