<?php

namespace Axess\Dci4Wtp;

class D4WTPGETADDDAYSTARIFFSRES
{

    /**
     * @var ArrayOfD4WTPEXTENSIONTARIFF $ACEXTENSIONTARIFF
     */
    protected $ACEXTENSIONTARIFF = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPEXTENSIONTARIFF
     */
    public function getACEXTENSIONTARIFF()
    {
      return $this->ACEXTENSIONTARIFF;
    }

    /**
     * @param ArrayOfD4WTPEXTENSIONTARIFF $ACEXTENSIONTARIFF
     * @return \Axess\Dci4Wtp\D4WTPGETADDDAYSTARIFFSRES
     */
    public function setACEXTENSIONTARIFF($ACEXTENSIONTARIFF)
    {
      $this->ACEXTENSIONTARIFF = $ACEXTENSIONTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETADDDAYSTARIFFSRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETADDDAYSTARIFFSRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
