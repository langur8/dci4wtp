<?php

namespace Axess\Dci4Wtp;

class issueTicket6Response
{

    /**
     * @var D4WTPISSUETICKET5RESULT $issueTicket6Result
     */
    protected $issueTicket6Result = null;

    /**
     * @param D4WTPISSUETICKET5RESULT $issueTicket6Result
     */
    public function __construct($issueTicket6Result)
    {
      $this->issueTicket6Result = $issueTicket6Result;
    }

    /**
     * @return D4WTPISSUETICKET5RESULT
     */
    public function getIssueTicket6Result()
    {
      return $this->issueTicket6Result;
    }

    /**
     * @param D4WTPISSUETICKET5RESULT $issueTicket6Result
     * @return \Axess\Dci4Wtp\issueTicket6Response
     */
    public function setIssueTicket6Result($issueTicket6Result)
    {
      $this->issueTicket6Result = $issueTicket6Result;
      return $this;
    }

}
