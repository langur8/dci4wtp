<?php

namespace Axess\Dci4Wtp;

class getPersTypeRules
{

    /**
     * @var D4WTPPERSTYPERULEREQUEST $i_ctPersTypeRuleReq
     */
    protected $i_ctPersTypeRuleReq = null;

    /**
     * @param D4WTPPERSTYPERULEREQUEST $i_ctPersTypeRuleReq
     */
    public function __construct($i_ctPersTypeRuleReq)
    {
      $this->i_ctPersTypeRuleReq = $i_ctPersTypeRuleReq;
    }

    /**
     * @return D4WTPPERSTYPERULEREQUEST
     */
    public function getI_ctPersTypeRuleReq()
    {
      return $this->i_ctPersTypeRuleReq;
    }

    /**
     * @param D4WTPPERSTYPERULEREQUEST $i_ctPersTypeRuleReq
     * @return \Axess\Dci4Wtp\getPersTypeRules
     */
    public function setI_ctPersTypeRuleReq($i_ctPersTypeRuleReq)
    {
      $this->i_ctPersTypeRuleReq = $i_ctPersTypeRuleReq;
      return $this;
    }

}
