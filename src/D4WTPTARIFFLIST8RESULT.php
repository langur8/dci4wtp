<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST8RESULT
{

    /**
     * @var ArrayOfD4WTPTARIFFLIST8 $ACTTARIFFLIST8
     */
    protected $ACTTARIFFLIST8 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLIST8
     */
    public function getACTTARIFFLIST8()
    {
      return $this->ACTTARIFFLIST8;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLIST8 $ACTTARIFFLIST8
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST8RESULT
     */
    public function setACTTARIFFLIST8($ACTTARIFFLIST8)
    {
      $this->ACTTARIFFLIST8 = $ACTTARIFFLIST8;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST8RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST8RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
