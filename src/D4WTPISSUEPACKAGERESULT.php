<?php

namespace Axess\Dci4Wtp;

class D4WTPISSUEPACKAGERESULT
{

    /**
     * @var D4WTPISSUETICKETRESULT $CTISSUETICKETRESULT
     */
    protected $CTISSUETICKETRESULT = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPISSUETICKETRESULT
     */
    public function getCTISSUETICKETRESULT()
    {
      return $this->CTISSUETICKETRESULT;
    }

    /**
     * @param D4WTPISSUETICKETRESULT $CTISSUETICKETRESULT
     * @return \Axess\Dci4Wtp\D4WTPISSUEPACKAGERESULT
     */
    public function setCTISSUETICKETRESULT($CTISSUETICKETRESULT)
    {
      $this->CTISSUETICKETRESULT = $CTISSUETICKETRESULT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPISSUEPACKAGERESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPISSUEPACKAGERESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
