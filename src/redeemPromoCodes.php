<?php

namespace Axess\Dci4Wtp;

class redeemPromoCodes
{

    /**
     * @var D4WTPREDEEMPROMOCODESREQUEST $i_ctRedeemPromoCodesReq
     */
    protected $i_ctRedeemPromoCodesReq = null;

    /**
     * @param D4WTPREDEEMPROMOCODESREQUEST $i_ctRedeemPromoCodesReq
     */
    public function __construct($i_ctRedeemPromoCodesReq)
    {
      $this->i_ctRedeemPromoCodesReq = $i_ctRedeemPromoCodesReq;
    }

    /**
     * @return D4WTPREDEEMPROMOCODESREQUEST
     */
    public function getI_ctRedeemPromoCodesReq()
    {
      return $this->i_ctRedeemPromoCodesReq;
    }

    /**
     * @param D4WTPREDEEMPROMOCODESREQUEST $i_ctRedeemPromoCodesReq
     * @return \Axess\Dci4Wtp\redeemPromoCodes
     */
    public function setI_ctRedeemPromoCodesReq($i_ctRedeemPromoCodesReq)
    {
      $this->i_ctRedeemPromoCodesReq = $i_ctRedeemPromoCodesReq;
      return $this;
    }

}
