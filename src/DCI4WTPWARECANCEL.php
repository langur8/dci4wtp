<?php

namespace Axess\Dci4Wtp;

class DCI4WTPWARECANCEL
{

    /**
     * @var float $NWAREBLOCKNO
     */
    protected $NWAREBLOCKNO = null;

    /**
     * @var float $NWAREPOSNO
     */
    protected $NWAREPOSNO = null;

    /**
     * @var float $NWAREPROJNO
     */
    protected $NWAREPROJNO = null;

    /**
     * @var float $NWARETRANSNO
     */
    protected $NWARETRANSNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNWAREBLOCKNO()
    {
      return $this->NWAREBLOCKNO;
    }

    /**
     * @param float $NWAREBLOCKNO
     * @return \Axess\Dci4Wtp\DCI4WTPWARECANCEL
     */
    public function setNWAREBLOCKNO($NWAREBLOCKNO)
    {
      $this->NWAREBLOCKNO = $NWAREBLOCKNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWAREPOSNO()
    {
      return $this->NWAREPOSNO;
    }

    /**
     * @param float $NWAREPOSNO
     * @return \Axess\Dci4Wtp\DCI4WTPWARECANCEL
     */
    public function setNWAREPOSNO($NWAREPOSNO)
    {
      $this->NWAREPOSNO = $NWAREPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWAREPROJNO()
    {
      return $this->NWAREPROJNO;
    }

    /**
     * @param float $NWAREPROJNO
     * @return \Axess\Dci4Wtp\DCI4WTPWARECANCEL
     */
    public function setNWAREPROJNO($NWAREPROJNO)
    {
      $this->NWAREPROJNO = $NWAREPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARETRANSNO()
    {
      return $this->NWARETRANSNO;
    }

    /**
     * @param float $NWARETRANSNO
     * @return \Axess\Dci4Wtp\DCI4WTPWARECANCEL
     */
    public function setNWARETRANSNO($NWARETRANSNO)
    {
      $this->NWARETRANSNO = $NWARETRANSNO;
      return $this;
    }

}
