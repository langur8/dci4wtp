<?php

namespace Axess\Dci4Wtp;

class getTariffList7Response
{

    /**
     * @var D4WTPTARIFFLIST7RESULT $getTariffList7Result
     */
    protected $getTariffList7Result = null;

    /**
     * @param D4WTPTARIFFLIST7RESULT $getTariffList7Result
     */
    public function __construct($getTariffList7Result)
    {
      $this->getTariffList7Result = $getTariffList7Result;
    }

    /**
     * @return D4WTPTARIFFLIST7RESULT
     */
    public function getGetTariffList7Result()
    {
      return $this->getTariffList7Result;
    }

    /**
     * @param D4WTPTARIFFLIST7RESULT $getTariffList7Result
     * @return \Axess\Dci4Wtp\getTariffList7Response
     */
    public function setGetTariffList7Result($getTariffList7Result)
    {
      $this->getTariffList7Result = $getTariffList7Result;
      return $this;
    }

}
