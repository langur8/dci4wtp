<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCANCELRENTAL implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCANCELRENTAL[] $D4WTPCANCELRENTAL
     */
    protected $D4WTPCANCELRENTAL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCANCELRENTAL[]
     */
    public function getD4WTPCANCELRENTAL()
    {
      return $this->D4WTPCANCELRENTAL;
    }

    /**
     * @param D4WTPCANCELRENTAL[] $D4WTPCANCELRENTAL
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCANCELRENTAL
     */
    public function setD4WTPCANCELRENTAL(array $D4WTPCANCELRENTAL = null)
    {
      $this->D4WTPCANCELRENTAL = $D4WTPCANCELRENTAL;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCANCELRENTAL[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCANCELRENTAL
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCANCELRENTAL[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCANCELRENTAL $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCANCELRENTAL[] = $value;
      } else {
        $this->D4WTPCANCELRENTAL[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCANCELRENTAL[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCANCELRENTAL Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCANCELRENTAL);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCANCELRENTAL);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCANCELRENTAL);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCANCELRENTAL);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCANCELRENTAL Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCANCELRENTAL);
    }

}
