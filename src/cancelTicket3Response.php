<?php

namespace Axess\Dci4Wtp;

class cancelTicket3Response
{

    /**
     * @var D4WTPCANCELTICKET3RESULT $cancelTicket3Result
     */
    protected $cancelTicket3Result = null;

    /**
     * @param D4WTPCANCELTICKET3RESULT $cancelTicket3Result
     */
    public function __construct($cancelTicket3Result)
    {
      $this->cancelTicket3Result = $cancelTicket3Result;
    }

    /**
     * @return D4WTPCANCELTICKET3RESULT
     */
    public function getCancelTicket3Result()
    {
      return $this->cancelTicket3Result;
    }

    /**
     * @param D4WTPCANCELTICKET3RESULT $cancelTicket3Result
     * @return \Axess\Dci4Wtp\cancelTicket3Response
     */
    public function setCancelTicket3Result($cancelTicket3Result)
    {
      $this->cancelTicket3Result = $cancelTicket3Result;
      return $this;
    }

}
