<?php

namespace Axess\Dci4Wtp;

class D4WTPGETWARELISTRESULT3
{

    /**
     * @var ArrayOfD4WTPWARELIST3 $ACTWARELIST
     */
    protected $ACTWARELIST = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPWARELIST3
     */
    public function getACTWARELIST()
    {
      return $this->ACTWARELIST;
    }

    /**
     * @param ArrayOfD4WTPWARELIST3 $ACTWARELIST
     * @return \Axess\Dci4Wtp\D4WTPGETWARELISTRESULT3
     */
    public function setACTWARELIST($ACTWARELIST)
    {
      $this->ACTWARELIST = $ACTWARELIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETWARELISTRESULT3
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETWARELISTRESULT3
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
