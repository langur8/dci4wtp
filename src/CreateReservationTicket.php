<?php

namespace Axess\Dci4Wtp;

class CreateReservationTicket
{

    /**
     * @var int $Amount
     */
    protected $Amount = null;

    /**
     * @var string $ExpirationDate
     */
    protected $ExpirationDate = null;

    /**
     * @var int $KGTNo
     */
    protected $KGTNo = null;

    /**
     * @var int $KundenKartenTypNr
     */
    protected $KundenKartenTypNr = null;

    /**
     * @var int $POSNo
     */
    protected $POSNo = null;

    /**
     * @var int $PersonTypeNr
     */
    protected $PersonTypeNr = null;

    /**
     * @var int $PoolNo
     */
    protected $PoolNo = null;

    /**
     * @var int $ProjNo
     */
    protected $ProjNo = null;

    /**
     * @var int $SerialNo
     */
    protected $SerialNo = null;

    /**
     * @var int $TicketPoolNr
     */
    protected $TicketPoolNr = null;

    /**
     * @var int $UnicodeNr
     */
    protected $UnicodeNr = null;

    /**
     * @var ArrayOfstring $ValidDays
     */
    protected $ValidDays = null;

    /**
     * @var string $ValidityStart
     */
    protected $ValidityStart = null;

    /**
     * @var int $nType
     */
    protected $nType = null;

    /**
     * @var string $szIdentifier
     */
    protected $szIdentifier = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getAmount()
    {
      return $this->Amount;
    }

    /**
     * @param int $Amount
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setAmount($Amount)
    {
      $this->Amount = $Amount;
      return $this;
    }

    /**
     * @return string
     */
    public function getExpirationDate()
    {
      return $this->ExpirationDate;
    }

    /**
     * @param string $ExpirationDate
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setExpirationDate($ExpirationDate)
    {
      $this->ExpirationDate = $ExpirationDate;
      return $this;
    }

    /**
     * @return int
     */
    public function getKGTNo()
    {
      return $this->KGTNo;
    }

    /**
     * @param int $KGTNo
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setKGTNo($KGTNo)
    {
      $this->KGTNo = $KGTNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getKundenKartenTypNr()
    {
      return $this->KundenKartenTypNr;
    }

    /**
     * @param int $KundenKartenTypNr
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setKundenKartenTypNr($KundenKartenTypNr)
    {
      $this->KundenKartenTypNr = $KundenKartenTypNr;
      return $this;
    }

    /**
     * @return int
     */
    public function getPOSNo()
    {
      return $this->POSNo;
    }

    /**
     * @param int $POSNo
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setPOSNo($POSNo)
    {
      $this->POSNo = $POSNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getPersonTypeNr()
    {
      return $this->PersonTypeNr;
    }

    /**
     * @param int $PersonTypeNr
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setPersonTypeNr($PersonTypeNr)
    {
      $this->PersonTypeNr = $PersonTypeNr;
      return $this;
    }

    /**
     * @return int
     */
    public function getPoolNo()
    {
      return $this->PoolNo;
    }

    /**
     * @param int $PoolNo
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setPoolNo($PoolNo)
    {
      $this->PoolNo = $PoolNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getProjNo()
    {
      return $this->ProjNo;
    }

    /**
     * @param int $ProjNo
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setProjNo($ProjNo)
    {
      $this->ProjNo = $ProjNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getSerialNo()
    {
      return $this->SerialNo;
    }

    /**
     * @param int $SerialNo
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setSerialNo($SerialNo)
    {
      $this->SerialNo = $SerialNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getTicketPoolNr()
    {
      return $this->TicketPoolNr;
    }

    /**
     * @param int $TicketPoolNr
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setTicketPoolNr($TicketPoolNr)
    {
      $this->TicketPoolNr = $TicketPoolNr;
      return $this;
    }

    /**
     * @return int
     */
    public function getUnicodeNr()
    {
      return $this->UnicodeNr;
    }

    /**
     * @param int $UnicodeNr
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setUnicodeNr($UnicodeNr)
    {
      $this->UnicodeNr = $UnicodeNr;
      return $this;
    }

    /**
     * @return ArrayOfstring
     */
    public function getValidDays()
    {
      return $this->ValidDays;
    }

    /**
     * @param ArrayOfstring $ValidDays
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setValidDays($ValidDays)
    {
      $this->ValidDays = $ValidDays;
      return $this;
    }

    /**
     * @return string
     */
    public function getValidityStart()
    {
      return $this->ValidityStart;
    }

    /**
     * @param string $ValidityStart
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setValidityStart($ValidityStart)
    {
      $this->ValidityStart = $ValidityStart;
      return $this;
    }

    /**
     * @return int
     */
    public function getNType()
    {
      return $this->nType;
    }

    /**
     * @param int $nType
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setNType($nType)
    {
      $this->nType = $nType;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzIdentifier()
    {
      return $this->szIdentifier;
    }

    /**
     * @param string $szIdentifier
     * @return \Axess\Dci4Wtp\CreateReservationTicket
     */
    public function setSzIdentifier($szIdentifier)
    {
      $this->szIdentifier = $szIdentifier;
      return $this;
    }

}
