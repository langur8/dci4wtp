<?php

namespace Axess\Dci4Wtp;

class getReceiptData
{

    /**
     * @var D4WTPGETRECEIPTDATAREQUEST $i_ctReceiptDataReq
     */
    protected $i_ctReceiptDataReq = null;

    /**
     * @param D4WTPGETRECEIPTDATAREQUEST $i_ctReceiptDataReq
     */
    public function __construct($i_ctReceiptDataReq)
    {
      $this->i_ctReceiptDataReq = $i_ctReceiptDataReq;
    }

    /**
     * @return D4WTPGETRECEIPTDATAREQUEST
     */
    public function getI_ctReceiptDataReq()
    {
      return $this->i_ctReceiptDataReq;
    }

    /**
     * @param D4WTPGETRECEIPTDATAREQUEST $i_ctReceiptDataReq
     * @return \Axess\Dci4Wtp\getReceiptData
     */
    public function setI_ctReceiptDataReq($i_ctReceiptDataReq)
    {
      $this->i_ctReceiptDataReq = $i_ctReceiptDataReq;
      return $this;
    }

}
