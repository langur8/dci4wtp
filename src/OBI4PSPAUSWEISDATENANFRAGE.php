<?php

namespace Axess\Dci4Wtp;

class OBI4PSPAUSWEISDATENANFRAGE
{

    /**
     * @var string $SZAUSWEISID
     */
    protected $SZAUSWEISID = null;

    /**
     * @var string $SZPOSTLEITZAHL
     */
    protected $SZPOSTLEITZAHL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZAUSWEISID()
    {
      return $this->SZAUSWEISID;
    }

    /**
     * @param string $SZAUSWEISID
     * @return \Axess\Dci4Wtp\OBI4PSPAUSWEISDATENANFRAGE
     */
    public function setSZAUSWEISID($SZAUSWEISID)
    {
      $this->SZAUSWEISID = $SZAUSWEISID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOSTLEITZAHL()
    {
      return $this->SZPOSTLEITZAHL;
    }

    /**
     * @param string $SZPOSTLEITZAHL
     * @return \Axess\Dci4Wtp\OBI4PSPAUSWEISDATENANFRAGE
     */
    public function setSZPOSTLEITZAHL($SZPOSTLEITZAHL)
    {
      $this->SZPOSTLEITZAHL = $SZPOSTLEITZAHL;
      return $this;
    }

}
