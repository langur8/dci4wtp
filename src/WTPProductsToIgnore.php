<?php

namespace Axess\Dci4Wtp;

class WTPProductsToIgnore
{

    /**
     * @var int $nConfigNr
     */
    protected $nConfigNr = null;

    /**
     * @var int $nKundenKartenTypNr
     */
    protected $nKundenKartenTypNr = null;

    /**
     * @var int $nPersTypNr
     */
    protected $nPersTypNr = null;

    /**
     * @var int $nPoolNr
     */
    protected $nPoolNr = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getNConfigNr()
    {
      return $this->nConfigNr;
    }

    /**
     * @param int $nConfigNr
     * @return \Axess\Dci4Wtp\WTPProductsToIgnore
     */
    public function setNConfigNr($nConfigNr)
    {
      $this->nConfigNr = $nConfigNr;
      return $this;
    }

    /**
     * @return int
     */
    public function getNKundenKartenTypNr()
    {
      return $this->nKundenKartenTypNr;
    }

    /**
     * @param int $nKundenKartenTypNr
     * @return \Axess\Dci4Wtp\WTPProductsToIgnore
     */
    public function setNKundenKartenTypNr($nKundenKartenTypNr)
    {
      $this->nKundenKartenTypNr = $nKundenKartenTypNr;
      return $this;
    }

    /**
     * @return int
     */
    public function getNPersTypNr()
    {
      return $this->nPersTypNr;
    }

    /**
     * @param int $nPersTypNr
     * @return \Axess\Dci4Wtp\WTPProductsToIgnore
     */
    public function setNPersTypNr($nPersTypNr)
    {
      $this->nPersTypNr = $nPersTypNr;
      return $this;
    }

    /**
     * @return int
     */
    public function getNPoolNr()
    {
      return $this->nPoolNr;
    }

    /**
     * @param int $nPoolNr
     * @return \Axess\Dci4Wtp\WTPProductsToIgnore
     */
    public function setNPoolNr($nPoolNr)
    {
      $this->nPoolNr = $nPoolNr;
      return $this;
    }

}
