<?php

namespace Axess\Dci4Wtp;

class getACPXImageWithImage
{

    /**
     * @var string $szPrintData
     */
    protected $szPrintData = null;

    /**
     * @var int $nResolutionMapToDPI
     */
    protected $nResolutionMapToDPI = null;

    /**
     * @var base64Binary $blPhotoData
     */
    protected $blPhotoData = null;

    /**
     * @var D4WTPBLANKTYPE $ctBlankType
     */
    protected $ctBlankType = null;

    /**
     * @param string $szPrintData
     * @param int $nResolutionMapToDPI
     * @param base64Binary $blPhotoData
     * @param D4WTPBLANKTYPE $ctBlankType
     */
    public function __construct($szPrintData, $nResolutionMapToDPI, $blPhotoData, $ctBlankType)
    {
      $this->szPrintData = $szPrintData;
      $this->nResolutionMapToDPI = $nResolutionMapToDPI;
      $this->blPhotoData = $blPhotoData;
      $this->ctBlankType = $ctBlankType;
    }

    /**
     * @return string
     */
    public function getSzPrintData()
    {
      return $this->szPrintData;
    }

    /**
     * @param string $szPrintData
     * @return \Axess\Dci4Wtp\getACPXImageWithImage
     */
    public function setSzPrintData($szPrintData)
    {
      $this->szPrintData = $szPrintData;
      return $this;
    }

    /**
     * @return int
     */
    public function getNResolutionMapToDPI()
    {
      return $this->nResolutionMapToDPI;
    }

    /**
     * @param int $nResolutionMapToDPI
     * @return \Axess\Dci4Wtp\getACPXImageWithImage
     */
    public function setNResolutionMapToDPI($nResolutionMapToDPI)
    {
      $this->nResolutionMapToDPI = $nResolutionMapToDPI;
      return $this;
    }

    /**
     * @return base64Binary
     */
    public function getBlPhotoData()
    {
      return $this->blPhotoData;
    }

    /**
     * @param base64Binary $blPhotoData
     * @return \Axess\Dci4Wtp\getACPXImageWithImage
     */
    public function setBlPhotoData($blPhotoData)
    {
      $this->blPhotoData = $blPhotoData;
      return $this;
    }

    /**
     * @return D4WTPBLANKTYPE
     */
    public function getCtBlankType()
    {
      return $this->ctBlankType;
    }

    /**
     * @param D4WTPBLANKTYPE $ctBlankType
     * @return \Axess\Dci4Wtp\getACPXImageWithImage
     */
    public function setCtBlankType($ctBlankType)
    {
      $this->ctBlankType = $ctBlankType;
      return $this;
    }

}
