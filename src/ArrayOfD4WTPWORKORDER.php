<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPWORKORDER implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPWORKORDER[] $D4WTPWORKORDER
     */
    protected $D4WTPWORKORDER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPWORKORDER[]
     */
    public function getD4WTPWORKORDER()
    {
      return $this->D4WTPWORKORDER;
    }

    /**
     * @param D4WTPWORKORDER[] $D4WTPWORKORDER
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPWORKORDER
     */
    public function setD4WTPWORKORDER(array $D4WTPWORKORDER = null)
    {
      $this->D4WTPWORKORDER = $D4WTPWORKORDER;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPWORKORDER[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPWORKORDER
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPWORKORDER[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPWORKORDER $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPWORKORDER[] = $value;
      } else {
        $this->D4WTPWORKORDER[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPWORKORDER[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPWORKORDER Return the current element
     */
    public function current()
    {
      return current($this->D4WTPWORKORDER);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPWORKORDER);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPWORKORDER);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPWORKORDER);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPWORKORDER Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPWORKORDER);
    }

}
