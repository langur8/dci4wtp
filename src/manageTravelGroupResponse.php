<?php

namespace Axess\Dci4Wtp;

class manageTravelGroupResponse
{

    /**
     * @var D4WTPMANAGETRAVELGRPRESULT $manageTravelGroupResult
     */
    protected $manageTravelGroupResult = null;

    /**
     * @param D4WTPMANAGETRAVELGRPRESULT $manageTravelGroupResult
     */
    public function __construct($manageTravelGroupResult)
    {
      $this->manageTravelGroupResult = $manageTravelGroupResult;
    }

    /**
     * @return D4WTPMANAGETRAVELGRPRESULT
     */
    public function getManageTravelGroupResult()
    {
      return $this->manageTravelGroupResult;
    }

    /**
     * @param D4WTPMANAGETRAVELGRPRESULT $manageTravelGroupResult
     * @return \Axess\Dci4Wtp\manageTravelGroupResponse
     */
    public function setManageTravelGroupResult($manageTravelGroupResult)
    {
      $this->manageTravelGroupResult = $manageTravelGroupResult;
      return $this;
    }

}
