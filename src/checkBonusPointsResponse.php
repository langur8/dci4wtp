<?php

namespace Axess\Dci4Wtp;

class checkBonusPointsResponse
{

    /**
     * @var D4WTPCHECKBONUSPOINTSRES $checkBonusPointsResult
     */
    protected $checkBonusPointsResult = null;

    /**
     * @param D4WTPCHECKBONUSPOINTSRES $checkBonusPointsResult
     */
    public function __construct($checkBonusPointsResult)
    {
      $this->checkBonusPointsResult = $checkBonusPointsResult;
    }

    /**
     * @return D4WTPCHECKBONUSPOINTSRES
     */
    public function getCheckBonusPointsResult()
    {
      return $this->checkBonusPointsResult;
    }

    /**
     * @param D4WTPCHECKBONUSPOINTSRES $checkBonusPointsResult
     * @return \Axess\Dci4Wtp\checkBonusPointsResponse
     */
    public function setCheckBonusPointsResult($checkBonusPointsResult)
    {
      $this->checkBonusPointsResult = $checkBonusPointsResult;
      return $this;
    }

}
