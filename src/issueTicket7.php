<?php

namespace Axess\Dci4Wtp;

class issueTicket7
{

    /**
     * @var D4WTPISSUETICKET7REQUEST $i_ctIssueTicketReq
     */
    protected $i_ctIssueTicketReq = null;

    /**
     * @param D4WTPISSUETICKET7REQUEST $i_ctIssueTicketReq
     */
    public function __construct($i_ctIssueTicketReq)
    {
      $this->i_ctIssueTicketReq = $i_ctIssueTicketReq;
    }

    /**
     * @return D4WTPISSUETICKET7REQUEST
     */
    public function getI_ctIssueTicketReq()
    {
      return $this->i_ctIssueTicketReq;
    }

    /**
     * @param D4WTPISSUETICKET7REQUEST $i_ctIssueTicketReq
     * @return \Axess\Dci4Wtp\issueTicket7
     */
    public function setI_ctIssueTicketReq($i_ctIssueTicketReq)
    {
      $this->i_ctIssueTicketReq = $i_ctIssueTicketReq;
      return $this;
    }

}
