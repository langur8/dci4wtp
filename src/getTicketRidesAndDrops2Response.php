<?php

namespace Axess\Dci4Wtp;

class getTicketRidesAndDrops2Response
{

    /**
     * @var D4WTPGETTCKTRIDESDROPSRES2 $getTicketRidesAndDrops2Result
     */
    protected $getTicketRidesAndDrops2Result = null;

    /**
     * @param D4WTPGETTCKTRIDESDROPSRES2 $getTicketRidesAndDrops2Result
     */
    public function __construct($getTicketRidesAndDrops2Result)
    {
      $this->getTicketRidesAndDrops2Result = $getTicketRidesAndDrops2Result;
    }

    /**
     * @return D4WTPGETTCKTRIDESDROPSRES2
     */
    public function getGetTicketRidesAndDrops2Result()
    {
      return $this->getTicketRidesAndDrops2Result;
    }

    /**
     * @param D4WTPGETTCKTRIDESDROPSRES2 $getTicketRidesAndDrops2Result
     * @return \Axess\Dci4Wtp\getTicketRidesAndDrops2Response
     */
    public function setGetTicketRidesAndDrops2Result($getTicketRidesAndDrops2Result)
    {
      $this->getTicketRidesAndDrops2Result = $getTicketRidesAndDrops2Result;
      return $this;
    }

}
