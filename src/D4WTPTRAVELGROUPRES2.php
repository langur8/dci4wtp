<?php

namespace Axess\Dci4Wtp;

class D4WTPTRAVELGROUPRES2
{

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var float $NSHOPPINGCARTPOSNO
     */
    protected $NSHOPPINGCARTPOSNO = null;

    /**
     * @var float $NSHOPPINGCARTPROJNO
     */
    protected $NSHOPPINGCARTPROJNO = null;

    /**
     * @var float $NSHOPPINGCARTSTATUSNO
     */
    protected $NSHOPPINGCARTSTATUSNO = null;

    /**
     * @var float $NSHOPPINGCARTTICKETCOUNT
     */
    protected $NSHOPPINGCARTTICKETCOUNT = null;

    /**
     * @var float $NTRAVELGROUPNO
     */
    protected $NTRAVELGROUPNO = null;

    /**
     * @var float $NTRAVELGROUPPOSNO
     */
    protected $NTRAVELGROUPPOSNO = null;

    /**
     * @var float $NTRAVELGROUPPROJNO
     */
    protected $NTRAVELGROUPPROJNO = null;

    /**
     * @var string $SZCOMPANYEMAIL
     */
    protected $SZCOMPANYEMAIL = null;

    /**
     * @var string $SZCOMPANYNAME
     */
    protected $SZCOMPANYNAME = null;

    /**
     * @var string $SZCOMPANYPHONENO
     */
    protected $SZCOMPANYPHONENO = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZSHOPPINGCARTDESC
     */
    protected $SZSHOPPINGCARTDESC = null;

    /**
     * @var string $SZTRAVELGROUPADDINFO
     */
    protected $SZTRAVELGROUPADDINFO = null;

    /**
     * @var string $SZTRAVELGROUPNAME
     */
    protected $SZTRAVELGROUPNAME = null;

    /**
     * @var string $SZTRAVELGROUPTEXT
     */
    protected $SZTRAVELGROUPTEXT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPOSNO()
    {
      return $this->NSHOPPINGCARTPOSNO;
    }

    /**
     * @param float $NSHOPPINGCARTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setNSHOPPINGCARTPOSNO($NSHOPPINGCARTPOSNO)
    {
      $this->NSHOPPINGCARTPOSNO = $NSHOPPINGCARTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPROJNO()
    {
      return $this->NSHOPPINGCARTPROJNO;
    }

    /**
     * @param float $NSHOPPINGCARTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setNSHOPPINGCARTPROJNO($NSHOPPINGCARTPROJNO)
    {
      $this->NSHOPPINGCARTPROJNO = $NSHOPPINGCARTPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTSTATUSNO()
    {
      return $this->NSHOPPINGCARTSTATUSNO;
    }

    /**
     * @param float $NSHOPPINGCARTSTATUSNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setNSHOPPINGCARTSTATUSNO($NSHOPPINGCARTSTATUSNO)
    {
      $this->NSHOPPINGCARTSTATUSNO = $NSHOPPINGCARTSTATUSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTTICKETCOUNT()
    {
      return $this->NSHOPPINGCARTTICKETCOUNT;
    }

    /**
     * @param float $NSHOPPINGCARTTICKETCOUNT
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setNSHOPPINGCARTTICKETCOUNT($NSHOPPINGCARTTICKETCOUNT)
    {
      $this->NSHOPPINGCARTTICKETCOUNT = $NSHOPPINGCARTTICKETCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPNO()
    {
      return $this->NTRAVELGROUPNO;
    }

    /**
     * @param float $NTRAVELGROUPNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setNTRAVELGROUPNO($NTRAVELGROUPNO)
    {
      $this->NTRAVELGROUPNO = $NTRAVELGROUPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPOSNO()
    {
      return $this->NTRAVELGROUPPOSNO;
    }

    /**
     * @param float $NTRAVELGROUPPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setNTRAVELGROUPPOSNO($NTRAVELGROUPPOSNO)
    {
      $this->NTRAVELGROUPPOSNO = $NTRAVELGROUPPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPROJNO()
    {
      return $this->NTRAVELGROUPPROJNO;
    }

    /**
     * @param float $NTRAVELGROUPPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setNTRAVELGROUPPROJNO($NTRAVELGROUPPROJNO)
    {
      $this->NTRAVELGROUPPROJNO = $NTRAVELGROUPPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYEMAIL()
    {
      return $this->SZCOMPANYEMAIL;
    }

    /**
     * @param string $SZCOMPANYEMAIL
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setSZCOMPANYEMAIL($SZCOMPANYEMAIL)
    {
      $this->SZCOMPANYEMAIL = $SZCOMPANYEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYNAME()
    {
      return $this->SZCOMPANYNAME;
    }

    /**
     * @param string $SZCOMPANYNAME
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setSZCOMPANYNAME($SZCOMPANYNAME)
    {
      $this->SZCOMPANYNAME = $SZCOMPANYNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYPHONENO()
    {
      return $this->SZCOMPANYPHONENO;
    }

    /**
     * @param string $SZCOMPANYPHONENO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setSZCOMPANYPHONENO($SZCOMPANYPHONENO)
    {
      $this->SZCOMPANYPHONENO = $SZCOMPANYPHONENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHOPPINGCARTDESC()
    {
      return $this->SZSHOPPINGCARTDESC;
    }

    /**
     * @param string $SZSHOPPINGCARTDESC
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setSZSHOPPINGCARTDESC($SZSHOPPINGCARTDESC)
    {
      $this->SZSHOPPINGCARTDESC = $SZSHOPPINGCARTDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTRAVELGROUPADDINFO()
    {
      return $this->SZTRAVELGROUPADDINFO;
    }

    /**
     * @param string $SZTRAVELGROUPADDINFO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setSZTRAVELGROUPADDINFO($SZTRAVELGROUPADDINFO)
    {
      $this->SZTRAVELGROUPADDINFO = $SZTRAVELGROUPADDINFO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTRAVELGROUPNAME()
    {
      return $this->SZTRAVELGROUPNAME;
    }

    /**
     * @param string $SZTRAVELGROUPNAME
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setSZTRAVELGROUPNAME($SZTRAVELGROUPNAME)
    {
      $this->SZTRAVELGROUPNAME = $SZTRAVELGROUPNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTRAVELGROUPTEXT()
    {
      return $this->SZTRAVELGROUPTEXT;
    }

    /**
     * @param string $SZTRAVELGROUPTEXT
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES2
     */
    public function setSZTRAVELGROUPTEXT($SZTRAVELGROUPTEXT)
    {
      $this->SZTRAVELGROUPTEXT = $SZTRAVELGROUPTEXT;
      return $this;
    }

}
