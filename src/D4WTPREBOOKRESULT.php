<?php

namespace Axess\Dci4Wtp;

class D4WTPREBOOKRESULT
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NRECEIPTID
     */
    protected $NRECEIPTID = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPREBOOKRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRECEIPTID()
    {
      return $this->NRECEIPTID;
    }

    /**
     * @param float $NRECEIPTID
     * @return \Axess\Dci4Wtp\D4WTPREBOOKRESULT
     */
    public function setNRECEIPTID($NRECEIPTID)
    {
      $this->NRECEIPTID = $NRECEIPTID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPREBOOKRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
