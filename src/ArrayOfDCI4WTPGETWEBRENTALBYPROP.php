<?php

namespace Axess\Dci4Wtp;

class ArrayOfDCI4WTPGETWEBRENTALBYPROP implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DCI4WTPGETWEBRENTALBYPROP[] $DCI4WTPGETWEBRENTALBYPROP
     */
    protected $DCI4WTPGETWEBRENTALBYPROP = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DCI4WTPGETWEBRENTALBYPROP[]
     */
    public function getDCI4WTPGETWEBRENTALBYPROP()
    {
      return $this->DCI4WTPGETWEBRENTALBYPROP;
    }

    /**
     * @param DCI4WTPGETWEBRENTALBYPROP[] $DCI4WTPGETWEBRENTALBYPROP
     * @return \Axess\Dci4Wtp\ArrayOfDCI4WTPGETWEBRENTALBYPROP
     */
    public function setDCI4WTPGETWEBRENTALBYPROP(array $DCI4WTPGETWEBRENTALBYPROP = null)
    {
      $this->DCI4WTPGETWEBRENTALBYPROP = $DCI4WTPGETWEBRENTALBYPROP;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DCI4WTPGETWEBRENTALBYPROP[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DCI4WTPGETWEBRENTALBYPROP
     */
    public function offsetGet($offset)
    {
      return $this->DCI4WTPGETWEBRENTALBYPROP[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DCI4WTPGETWEBRENTALBYPROP $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DCI4WTPGETWEBRENTALBYPROP[] = $value;
      } else {
        $this->DCI4WTPGETWEBRENTALBYPROP[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DCI4WTPGETWEBRENTALBYPROP[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DCI4WTPGETWEBRENTALBYPROP Return the current element
     */
    public function current()
    {
      return current($this->DCI4WTPGETWEBRENTALBYPROP);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DCI4WTPGETWEBRENTALBYPROP);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DCI4WTPGETWEBRENTALBYPROP);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DCI4WTPGETWEBRENTALBYPROP);
    }

    /**
     * Countable implementation
     *
     * @return DCI4WTPGETWEBRENTALBYPROP Return count of elements
     */
    public function count()
    {
      return count($this->DCI4WTPGETWEBRENTALBYPROP);
    }

}
