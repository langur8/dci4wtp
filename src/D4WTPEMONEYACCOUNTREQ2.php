<?php

namespace Axess\Dci4Wtp;

class D4WTPEMONEYACCOUNTREQ2
{

    /**
     * @var ArrayOfD4WTPAXCOUNTMEMBER $ACTAXCOUNTMEMBER
     */
    protected $ACTAXCOUNTMEMBER = null;

    /**
     * @var float $BISAXSCALED
     */
    protected $BISAXSCALED = null;

    /**
     * @var float $NAXCOUNTKONTONR
     */
    protected $NAXCOUNTKONTONR = null;

    /**
     * @var float $NBANKNR
     */
    protected $NBANKNR = null;

    /**
     * @var float $NBRANCHNR
     */
    protected $NBRANCHNR = null;

    /**
     * @var float $NDESKNR
     */
    protected $NDESKNR = null;

    /**
     * @var float $NGESNR
     */
    protected $NGESNR = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTYPE
     */
    protected $NTYPE = null;

    /**
     * @var string $SZFLAG
     */
    protected $SZFLAG = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPAXCOUNTMEMBER
     */
    public function getACTAXCOUNTMEMBER()
    {
      return $this->ACTAXCOUNTMEMBER;
    }

    /**
     * @param ArrayOfD4WTPAXCOUNTMEMBER $ACTAXCOUNTMEMBER
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setACTAXCOUNTMEMBER($ACTAXCOUNTMEMBER)
    {
      $this->ACTAXCOUNTMEMBER = $ACTAXCOUNTMEMBER;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISAXSCALED()
    {
      return $this->BISAXSCALED;
    }

    /**
     * @param float $BISAXSCALED
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setBISAXSCALED($BISAXSCALED)
    {
      $this->BISAXSCALED = $BISAXSCALED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAXCOUNTKONTONR()
    {
      return $this->NAXCOUNTKONTONR;
    }

    /**
     * @param float $NAXCOUNTKONTONR
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setNAXCOUNTKONTONR($NAXCOUNTKONTONR)
    {
      $this->NAXCOUNTKONTONR = $NAXCOUNTKONTONR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBANKNR()
    {
      return $this->NBANKNR;
    }

    /**
     * @param float $NBANKNR
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setNBANKNR($NBANKNR)
    {
      $this->NBANKNR = $NBANKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBRANCHNR()
    {
      return $this->NBRANCHNR;
    }

    /**
     * @param float $NBRANCHNR
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setNBRANCHNR($NBRANCHNR)
    {
      $this->NBRANCHNR = $NBRANCHNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDESKNR()
    {
      return $this->NDESKNR;
    }

    /**
     * @param float $NDESKNR
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setNDESKNR($NDESKNR)
    {
      $this->NDESKNR = $NDESKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNGESNR()
    {
      return $this->NGESNR;
    }

    /**
     * @param float $NGESNR
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setNGESNR($NGESNR)
    {
      $this->NGESNR = $NGESNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTYPE()
    {
      return $this->NTYPE;
    }

    /**
     * @param float $NTYPE
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setNTYPE($NTYPE)
    {
      $this->NTYPE = $NTYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFLAG()
    {
      return $this->SZFLAG;
    }

    /**
     * @param string $SZFLAG
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ2
     */
    public function setSZFLAG($SZFLAG)
    {
      $this->SZFLAG = $SZFLAG;
      return $this;
    }

}
