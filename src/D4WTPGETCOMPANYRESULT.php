<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCOMPANYRESULT
{

    /**
     * @var ArrayOfD4WTPCOMPANYDATA $ACTCOMPANYDATA
     */
    protected $ACTCOMPANYDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCOMPANYDATA
     */
    public function getACTCOMPANYDATA()
    {
      return $this->ACTCOMPANYDATA;
    }

    /**
     * @param ArrayOfD4WTPCOMPANYDATA $ACTCOMPANYDATA
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANYRESULT
     */
    public function setACTCOMPANYDATA($ACTCOMPANYDATA)
    {
      $this->ACTCOMPANYDATA = $ACTCOMPANYDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANYRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANYRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
