<?php

namespace Axess\Dci4Wtp;

class changePersData
{

    /**
     * @var D4WTPCHANGEPERSDATAREQUEST $i_ctChangePersDataReq
     */
    protected $i_ctChangePersDataReq = null;

    /**
     * @param D4WTPCHANGEPERSDATAREQUEST $i_ctChangePersDataReq
     */
    public function __construct($i_ctChangePersDataReq)
    {
      $this->i_ctChangePersDataReq = $i_ctChangePersDataReq;
    }

    /**
     * @return D4WTPCHANGEPERSDATAREQUEST
     */
    public function getI_ctChangePersDataReq()
    {
      return $this->i_ctChangePersDataReq;
    }

    /**
     * @param D4WTPCHANGEPERSDATAREQUEST $i_ctChangePersDataReq
     * @return \Axess\Dci4Wtp\changePersData
     */
    public function setI_ctChangePersDataReq($i_ctChangePersDataReq)
    {
      $this->i_ctChangePersDataReq = $i_ctChangePersDataReq;
      return $this;
    }

}
