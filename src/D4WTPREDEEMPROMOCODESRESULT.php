<?php

namespace Axess\Dci4Wtp;

class D4WTPREDEEMPROMOCODESRESULT
{

    /**
     * @var ArrayOfD4WTPPROMOCODERESULT $ACTPROMOCODERESULT
     */
    protected $ACTPROMOCODERESULT = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPROMOCODERESULT
     */
    public function getACTPROMOCODERESULT()
    {
      return $this->ACTPROMOCODERESULT;
    }

    /**
     * @param ArrayOfD4WTPPROMOCODERESULT $ACTPROMOCODERESULT
     * @return \Axess\Dci4Wtp\D4WTPREDEEMPROMOCODESRESULT
     */
    public function setACTPROMOCODERESULT($ACTPROMOCODERESULT)
    {
      $this->ACTPROMOCODERESULT = $ACTPROMOCODERESULT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPREDEEMPROMOCODESRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPREDEEMPROMOCODESRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
