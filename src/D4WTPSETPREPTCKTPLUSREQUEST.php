<?php

namespace Axess\Dci4Wtp;

class D4WTPSETPREPTCKTPLUSREQUEST
{

    /**
     * @var float $BOVERWRITE
     */
    protected $BOVERWRITE = null;

    /**
     * @var float $NAPPID
     */
    protected $NAPPID = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NNAMEDUSERID
     */
    protected $NNAMEDUSERID = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZEXTCARDID
     */
    protected $SZEXTCARDID = null;

    /**
     * @var string $SZEXTVOUCHERID
     */
    protected $SZEXTVOUCHERID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBOVERWRITE()
    {
      return $this->BOVERWRITE;
    }

    /**
     * @param float $BOVERWRITE
     * @return \Axess\Dci4Wtp\D4WTPSETPREPTCKTPLUSREQUEST
     */
    public function setBOVERWRITE($BOVERWRITE)
    {
      $this->BOVERWRITE = $BOVERWRITE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAPPID()
    {
      return $this->NAPPID;
    }

    /**
     * @param float $NAPPID
     * @return \Axess\Dci4Wtp\D4WTPSETPREPTCKTPLUSREQUEST
     */
    public function setNAPPID($NAPPID)
    {
      $this->NAPPID = $NAPPID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPSETPREPTCKTPLUSREQUEST
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNAMEDUSERID()
    {
      return $this->NNAMEDUSERID;
    }

    /**
     * @param float $NNAMEDUSERID
     * @return \Axess\Dci4Wtp\D4WTPSETPREPTCKTPLUSREQUEST
     */
    public function setNNAMEDUSERID($NNAMEDUSERID)
    {
      $this->NNAMEDUSERID = $NNAMEDUSERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSETPREPTCKTPLUSREQUEST
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSETPREPTCKTPLUSREQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSETPREPTCKTPLUSREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTCARDID()
    {
      return $this->SZEXTCARDID;
    }

    /**
     * @param string $SZEXTCARDID
     * @return \Axess\Dci4Wtp\D4WTPSETPREPTCKTPLUSREQUEST
     */
    public function setSZEXTCARDID($SZEXTCARDID)
    {
      $this->SZEXTCARDID = $SZEXTCARDID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTVOUCHERID()
    {
      return $this->SZEXTVOUCHERID;
    }

    /**
     * @param string $SZEXTVOUCHERID
     * @return \Axess\Dci4Wtp\D4WTPSETPREPTCKTPLUSREQUEST
     */
    public function setSZEXTVOUCHERID($SZEXTVOUCHERID)
    {
      $this->SZEXTVOUCHERID = $SZEXTVOUCHERID;
      return $this;
    }

}
