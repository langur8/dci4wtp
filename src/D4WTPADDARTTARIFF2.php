<?php

namespace Axess\Dci4Wtp;

class D4WTPADDARTTARIFF2
{

    /**
     * @var float $BMANDATORY
     */
    protected $BMANDATORY = null;

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NARTICLETYPENO
     */
    protected $NARTICLETYPENO = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPACKAGENO
     */
    protected $NPACKAGENO = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NSORTID
     */
    protected $NSORTID = null;

    /**
     * @var float $NSTEUERBETRAG
     */
    protected $NSTEUERBETRAG = null;

    /**
     * @var float $NSTEUERPROZENT
     */
    protected $NSTEUERPROZENT = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var string $SZARTICLETYPENAME
     */
    protected $SZARTICLETYPENAME = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBMANDATORY()
    {
      return $this->BMANDATORY;
    }

    /**
     * @param float $BMANDATORY
     * @return \Axess\Dci4Wtp\D4WTPADDARTTARIFF2
     */
    public function setBMANDATORY($BMANDATORY)
    {
      $this->BMANDATORY = $BMANDATORY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPADDARTTARIFF2
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLETYPENO()
    {
      return $this->NARTICLETYPENO;
    }

    /**
     * @param float $NARTICLETYPENO
     * @return \Axess\Dci4Wtp\D4WTPADDARTTARIFF2
     */
    public function setNARTICLETYPENO($NARTICLETYPENO)
    {
      $this->NARTICLETYPENO = $NARTICLETYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPADDARTTARIFF2
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKAGENO()
    {
      return $this->NPACKAGENO;
    }

    /**
     * @param float $NPACKAGENO
     * @return \Axess\Dci4Wtp\D4WTPADDARTTARIFF2
     */
    public function setNPACKAGENO($NPACKAGENO)
    {
      $this->NPACKAGENO = $NPACKAGENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPADDARTTARIFF2
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTID()
    {
      return $this->NSORTID;
    }

    /**
     * @param float $NSORTID
     * @return \Axess\Dci4Wtp\D4WTPADDARTTARIFF2
     */
    public function setNSORTID($NSORTID)
    {
      $this->NSORTID = $NSORTID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTEUERBETRAG()
    {
      return $this->NSTEUERBETRAG;
    }

    /**
     * @param float $NSTEUERBETRAG
     * @return \Axess\Dci4Wtp\D4WTPADDARTTARIFF2
     */
    public function setNSTEUERBETRAG($NSTEUERBETRAG)
    {
      $this->NSTEUERBETRAG = $NSTEUERBETRAG;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTEUERPROZENT()
    {
      return $this->NSTEUERPROZENT;
    }

    /**
     * @param float $NSTEUERPROZENT
     * @return \Axess\Dci4Wtp\D4WTPADDARTTARIFF2
     */
    public function setNSTEUERPROZENT($NSTEUERPROZENT)
    {
      $this->NSTEUERPROZENT = $NSTEUERPROZENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPADDARTTARIFF2
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZARTICLETYPENAME()
    {
      return $this->SZARTICLETYPENAME;
    }

    /**
     * @param string $SZARTICLETYPENAME
     * @return \Axess\Dci4Wtp\D4WTPADDARTTARIFF2
     */
    public function setSZARTICLETYPENAME($SZARTICLETYPENAME)
    {
      $this->SZARTICLETYPENAME = $SZARTICLETYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPADDARTTARIFF2
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPADDARTTARIFF2
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
