<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSHOPPINGCARTPOSADDART2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSHOPPINGCARTPOSADDART2[] $D4WTPSHOPPINGCARTPOSADDART2
     */
    protected $D4WTPSHOPPINGCARTPOSADDART2 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSHOPPINGCARTPOSADDART2[]
     */
    public function getD4WTPSHOPPINGCARTPOSADDART2()
    {
      return $this->D4WTPSHOPPINGCARTPOSADDART2;
    }

    /**
     * @param D4WTPSHOPPINGCARTPOSADDART2[] $D4WTPSHOPPINGCARTPOSADDART2
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSHOPPINGCARTPOSADDART2
     */
    public function setD4WTPSHOPPINGCARTPOSADDART2(array $D4WTPSHOPPINGCARTPOSADDART2 = null)
    {
      $this->D4WTPSHOPPINGCARTPOSADDART2 = $D4WTPSHOPPINGCARTPOSADDART2;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSHOPPINGCARTPOSADDART2[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSHOPPINGCARTPOSADDART2
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSHOPPINGCARTPOSADDART2[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSHOPPINGCARTPOSADDART2 $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSHOPPINGCARTPOSADDART2[] = $value;
      } else {
        $this->D4WTPSHOPPINGCARTPOSADDART2[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSHOPPINGCARTPOSADDART2[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSHOPPINGCARTPOSADDART2 Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSHOPPINGCARTPOSADDART2);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSHOPPINGCARTPOSADDART2);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSHOPPINGCARTPOSADDART2);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSHOPPINGCARTPOSADDART2);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSHOPPINGCARTPOSADDART2 Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSHOPPINGCARTPOSADDART2);
    }

}
