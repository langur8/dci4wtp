<?php

namespace Axess\Dci4Wtp;

class D4WTPPKGTARIFFLISTDAY7
{

    /**
     * @var ArrayOfD4WTPPACKAGELIST2 $ACTPACKAGELIST
     */
    protected $ACTPACKAGELIST = null;

    /**
     * @var D4WTPPACKAGEARTICLE2 $CTPACKAGEARTICLE
     */
    protected $CTPACKAGEARTICLE = null;

    /**
     * @var D4WTPPKGTARIFF5 $CTPKGTARIFF
     */
    protected $CTPKGTARIFF = null;

    /**
     * @var D4WTPRENTALITEMTARIFF2 $CTRENTALITEMTARIFF2
     */
    protected $CTRENTALITEMTARIFF2 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPACKAGELIST2
     */
    public function getACTPACKAGELIST()
    {
      return $this->ACTPACKAGELIST;
    }

    /**
     * @param ArrayOfD4WTPPACKAGELIST2 $ACTPACKAGELIST
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY7
     */
    public function setACTPACKAGELIST($ACTPACKAGELIST)
    {
      $this->ACTPACKAGELIST = $ACTPACKAGELIST;
      return $this;
    }

    /**
     * @return D4WTPPACKAGEARTICLE2
     */
    public function getCTPACKAGEARTICLE()
    {
      return $this->CTPACKAGEARTICLE;
    }

    /**
     * @param D4WTPPACKAGEARTICLE2 $CTPACKAGEARTICLE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY7
     */
    public function setCTPACKAGEARTICLE($CTPACKAGEARTICLE)
    {
      $this->CTPACKAGEARTICLE = $CTPACKAGEARTICLE;
      return $this;
    }

    /**
     * @return D4WTPPKGTARIFF5
     */
    public function getCTPKGTARIFF()
    {
      return $this->CTPKGTARIFF;
    }

    /**
     * @param D4WTPPKGTARIFF5 $CTPKGTARIFF
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY7
     */
    public function setCTPKGTARIFF($CTPKGTARIFF)
    {
      $this->CTPKGTARIFF = $CTPKGTARIFF;
      return $this;
    }

    /**
     * @return D4WTPRENTALITEMTARIFF2
     */
    public function getCTRENTALITEMTARIFF2()
    {
      return $this->CTRENTALITEMTARIFF2;
    }

    /**
     * @param D4WTPRENTALITEMTARIFF2 $CTRENTALITEMTARIFF2
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY7
     */
    public function setCTRENTALITEMTARIFF2($CTRENTALITEMTARIFF2)
    {
      $this->CTRENTALITEMTARIFF2 = $CTRENTALITEMTARIFF2;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY7
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY7
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
