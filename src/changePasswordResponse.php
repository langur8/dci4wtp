<?php

namespace Axess\Dci4Wtp;

class changePasswordResponse
{

    /**
     * @var D4WTPCHANGEPWRESULT $changePasswordResult
     */
    protected $changePasswordResult = null;

    /**
     * @param D4WTPCHANGEPWRESULT $changePasswordResult
     */
    public function __construct($changePasswordResult)
    {
      $this->changePasswordResult = $changePasswordResult;
    }

    /**
     * @return D4WTPCHANGEPWRESULT
     */
    public function getChangePasswordResult()
    {
      return $this->changePasswordResult;
    }

    /**
     * @param D4WTPCHANGEPWRESULT $changePasswordResult
     * @return \Axess\Dci4Wtp\changePasswordResponse
     */
    public function setChangePasswordResult($changePasswordResult)
    {
      $this->changePasswordResult = $changePasswordResult;
      return $this;
    }

}
