<?php

namespace Axess\Dci4Wtp;

class modifyReservation2
{

    /**
     * @var D4WTPMODIFYRES2REQUEST $i_ctmodifyReservationReq
     */
    protected $i_ctmodifyReservationReq = null;

    /**
     * @param D4WTPMODIFYRES2REQUEST $i_ctmodifyReservationReq
     */
    public function __construct($i_ctmodifyReservationReq)
    {
      $this->i_ctmodifyReservationReq = $i_ctmodifyReservationReq;
    }

    /**
     * @return D4WTPMODIFYRES2REQUEST
     */
    public function getI_ctmodifyReservationReq()
    {
      return $this->i_ctmodifyReservationReq;
    }

    /**
     * @param D4WTPMODIFYRES2REQUEST $i_ctmodifyReservationReq
     * @return \Axess\Dci4Wtp\modifyReservation2
     */
    public function setI_ctmodifyReservationReq($i_ctmodifyReservationReq)
    {
      $this->i_ctmodifyReservationReq = $i_ctmodifyReservationReq;
      return $this;
    }

}
