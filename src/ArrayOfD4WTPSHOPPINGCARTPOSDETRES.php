<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSHOPPINGCARTPOSDETRES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSHOPPINGCARTPOSDETRES[] $D4WTPSHOPPINGCARTPOSDETRES
     */
    protected $D4WTPSHOPPINGCARTPOSDETRES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSHOPPINGCARTPOSDETRES[]
     */
    public function getD4WTPSHOPPINGCARTPOSDETRES()
    {
      return $this->D4WTPSHOPPINGCARTPOSDETRES;
    }

    /**
     * @param D4WTPSHOPPINGCARTPOSDETRES[] $D4WTPSHOPPINGCARTPOSDETRES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSHOPPINGCARTPOSDETRES
     */
    public function setD4WTPSHOPPINGCARTPOSDETRES(array $D4WTPSHOPPINGCARTPOSDETRES = null)
    {
      $this->D4WTPSHOPPINGCARTPOSDETRES = $D4WTPSHOPPINGCARTPOSDETRES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSHOPPINGCARTPOSDETRES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSHOPPINGCARTPOSDETRES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSHOPPINGCARTPOSDETRES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSHOPPINGCARTPOSDETRES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSHOPPINGCARTPOSDETRES[] = $value;
      } else {
        $this->D4WTPSHOPPINGCARTPOSDETRES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSHOPPINGCARTPOSDETRES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSHOPPINGCARTPOSDETRES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSHOPPINGCARTPOSDETRES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSHOPPINGCARTPOSDETRES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSHOPPINGCARTPOSDETRES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSHOPPINGCARTPOSDETRES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSHOPPINGCARTPOSDETRES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSHOPPINGCARTPOSDETRES);
    }

}
