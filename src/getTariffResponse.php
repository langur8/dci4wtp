<?php

namespace Axess\Dci4Wtp;

class getTariffResponse
{

    /**
     * @var D4WTPTARIFFRESULT $getTariffResult
     */
    protected $getTariffResult = null;

    /**
     * @param D4WTPTARIFFRESULT $getTariffResult
     */
    public function __construct($getTariffResult)
    {
      $this->getTariffResult = $getTariffResult;
    }

    /**
     * @return D4WTPTARIFFRESULT
     */
    public function getGetTariffResult()
    {
      return $this->getTariffResult;
    }

    /**
     * @param D4WTPTARIFFRESULT $getTariffResult
     * @return \Axess\Dci4Wtp\getTariffResponse
     */
    public function setGetTariffResult($getTariffResult)
    {
      $this->getTariffResult = $getTariffResult;
      return $this;
    }

}
