<?php

namespace Axess\Dci4Wtp;

class getPersSalesData2
{

    /**
     * @var D4WTPPERSSALESDATAREQUEST $i_ctPersSalesDataReq
     */
    protected $i_ctPersSalesDataReq = null;

    /**
     * @param D4WTPPERSSALESDATAREQUEST $i_ctPersSalesDataReq
     */
    public function __construct($i_ctPersSalesDataReq)
    {
      $this->i_ctPersSalesDataReq = $i_ctPersSalesDataReq;
    }

    /**
     * @return D4WTPPERSSALESDATAREQUEST
     */
    public function getI_ctPersSalesDataReq()
    {
      return $this->i_ctPersSalesDataReq;
    }

    /**
     * @param D4WTPPERSSALESDATAREQUEST $i_ctPersSalesDataReq
     * @return \Axess\Dci4Wtp\getPersSalesData2
     */
    public function setI_ctPersSalesDataReq($i_ctPersSalesDataReq)
    {
      $this->i_ctPersSalesDataReq = $i_ctPersSalesDataReq;
      return $this;
    }

}
