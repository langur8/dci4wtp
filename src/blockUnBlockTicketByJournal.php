<?php

namespace Axess\Dci4Wtp;

class blockUnBlockTicketByJournal
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var float $i_nProjNo
     */
    protected $i_nProjNo = null;

    /**
     * @var float $i_nPOSNo
     */
    protected $i_nPOSNo = null;

    /**
     * @var float $i_nJournalNo
     */
    protected $i_nJournalNo = null;

    /**
     * @var string $i_szEndOfBlockingDate
     */
    protected $i_szEndOfBlockingDate = null;

    /**
     * @var float $i_bBlockTicket
     */
    protected $i_bBlockTicket = null;

    /**
     * @param float $i_nSessionID
     * @param float $i_nProjNo
     * @param float $i_nPOSNo
     * @param float $i_nJournalNo
     * @param string $i_szEndOfBlockingDate
     * @param float $i_bBlockTicket
     */
    public function __construct($i_nSessionID, $i_nProjNo, $i_nPOSNo, $i_nJournalNo, $i_szEndOfBlockingDate, $i_bBlockTicket)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_nProjNo = $i_nProjNo;
      $this->i_nPOSNo = $i_nPOSNo;
      $this->i_nJournalNo = $i_nJournalNo;
      $this->i_szEndOfBlockingDate = $i_szEndOfBlockingDate;
      $this->i_bBlockTicket = $i_bBlockTicket;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\blockUnBlockTicketByJournal
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nProjNo()
    {
      return $this->i_nProjNo;
    }

    /**
     * @param float $i_nProjNo
     * @return \Axess\Dci4Wtp\blockUnBlockTicketByJournal
     */
    public function setI_nProjNo($i_nProjNo)
    {
      $this->i_nProjNo = $i_nProjNo;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nPOSNo()
    {
      return $this->i_nPOSNo;
    }

    /**
     * @param float $i_nPOSNo
     * @return \Axess\Dci4Wtp\blockUnBlockTicketByJournal
     */
    public function setI_nPOSNo($i_nPOSNo)
    {
      $this->i_nPOSNo = $i_nPOSNo;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nJournalNo()
    {
      return $this->i_nJournalNo;
    }

    /**
     * @param float $i_nJournalNo
     * @return \Axess\Dci4Wtp\blockUnBlockTicketByJournal
     */
    public function setI_nJournalNo($i_nJournalNo)
    {
      $this->i_nJournalNo = $i_nJournalNo;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szEndOfBlockingDate()
    {
      return $this->i_szEndOfBlockingDate;
    }

    /**
     * @param string $i_szEndOfBlockingDate
     * @return \Axess\Dci4Wtp\blockUnBlockTicketByJournal
     */
    public function setI_szEndOfBlockingDate($i_szEndOfBlockingDate)
    {
      $this->i_szEndOfBlockingDate = $i_szEndOfBlockingDate;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_bBlockTicket()
    {
      return $this->i_bBlockTicket;
    }

    /**
     * @param float $i_bBlockTicket
     * @return \Axess\Dci4Wtp\blockUnBlockTicketByJournal
     */
    public function setI_bBlockTicket($i_bBlockTicket)
    {
      $this->i_bBlockTicket = $i_bBlockTicket;
      return $this;
    }

}
