<?php

namespace Axess\Dci4Wtp;

class D4WTPCHIPCARDENTRIESREQ
{

    /**
     * @var float $NCHIPPROJNR
     */
    protected $NCHIPPROJNR = null;

    /**
     * @var float $NDATACARRIERNR
     */
    protected $NDATACARRIERNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZCARDNR
     */
    protected $SZCARDNR = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    /**
     * @var string $SZWTPNUMBER64BIT
     */
    protected $SZWTPNUMBER64BIT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCHIPPROJNR()
    {
      return $this->NCHIPPROJNR;
    }

    /**
     * @param float $NCHIPPROJNR
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARDENTRIESREQ
     */
    public function setNCHIPPROJNR($NCHIPPROJNR)
    {
      $this->NCHIPPROJNR = $NCHIPPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATACARRIERNR()
    {
      return $this->NDATACARRIERNR;
    }

    /**
     * @param float $NDATACARRIERNR
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARDENTRIESREQ
     */
    public function setNDATACARRIERNR($NDATACARRIERNR)
    {
      $this->NDATACARRIERNR = $NDATACARRIERNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARDENTRIESREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDNR()
    {
      return $this->SZCARDNR;
    }

    /**
     * @param string $SZCARDNR
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARDENTRIESREQ
     */
    public function setSZCARDNR($SZCARDNR)
    {
      $this->SZCARDNR = $SZCARDNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARDENTRIESREQ
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNUMBER64BIT()
    {
      return $this->SZWTPNUMBER64BIT;
    }

    /**
     * @param string $SZWTPNUMBER64BIT
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARDENTRIESREQ
     */
    public function setSZWTPNUMBER64BIT($SZWTPNUMBER64BIT)
    {
      $this->SZWTPNUMBER64BIT = $SZWTPNUMBER64BIT;
      return $this;
    }

}
