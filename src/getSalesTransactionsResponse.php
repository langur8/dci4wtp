<?php

namespace Axess\Dci4Wtp;

class getSalesTransactionsResponse
{

    /**
     * @var D4WTPGETSALESTRANSRESULT $getSalesTransactionsResult
     */
    protected $getSalesTransactionsResult = null;

    /**
     * @param D4WTPGETSALESTRANSRESULT $getSalesTransactionsResult
     */
    public function __construct($getSalesTransactionsResult)
    {
      $this->getSalesTransactionsResult = $getSalesTransactionsResult;
    }

    /**
     * @return D4WTPGETSALESTRANSRESULT
     */
    public function getGetSalesTransactionsResult()
    {
      return $this->getSalesTransactionsResult;
    }

    /**
     * @param D4WTPGETSALESTRANSRESULT $getSalesTransactionsResult
     * @return \Axess\Dci4Wtp\getSalesTransactionsResponse
     */
    public function setGetSalesTransactionsResult($getSalesTransactionsResult)
    {
      $this->getSalesTransactionsResult = $getSalesTransactionsResult;
      return $this;
    }

}
