<?php

namespace Axess\Dci4Wtp;

class getPoolsResponse
{

    /**
     * @var D4WTPPOOLRESULT $getPoolsResult
     */
    protected $getPoolsResult = null;

    /**
     * @param D4WTPPOOLRESULT $getPoolsResult
     */
    public function __construct($getPoolsResult)
    {
      $this->getPoolsResult = $getPoolsResult;
    }

    /**
     * @return D4WTPPOOLRESULT
     */
    public function getGetPoolsResult()
    {
      return $this->getPoolsResult;
    }

    /**
     * @param D4WTPPOOLRESULT $getPoolsResult
     * @return \Axess\Dci4Wtp\getPoolsResponse
     */
    public function setGetPoolsResult($getPoolsResult)
    {
      $this->getPoolsResult = $getPoolsResult;
      return $this;
    }

}
