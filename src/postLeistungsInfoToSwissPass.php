<?php

namespace Axess\Dci4Wtp;

class postLeistungsInfoToSwissPass
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var float $i_NPROJNR
     */
    protected $i_NPROJNR = null;

    /**
     * @var float $i_nKassaNR
     */
    protected $i_nKassaNR = null;

    /**
     * @var float $i_nJournalNr
     */
    protected $i_nJournalNr = null;

    /**
     * @var float $i_fBetrag
     */
    protected $i_fBetrag = null;

    /**
     * @var string $i_szUidWert
     */
    protected $i_szUidWert = null;

    /**
     * @var float $i_nVerkaufsKanalNummer
     */
    protected $i_nVerkaufsKanalNummer = null;

    /**
     * @param float $i_nSessionID
     * @param float $i_NPROJNR
     * @param float $i_nKassaNR
     * @param float $i_nJournalNr
     * @param float $i_fBetrag
     * @param string $i_szUidWert
     * @param float $i_nVerkaufsKanalNummer
     */
    public function __construct($i_nSessionID, $i_NPROJNR, $i_nKassaNR, $i_nJournalNr, $i_fBetrag, $i_szUidWert, $i_nVerkaufsKanalNummer)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_NPROJNR = $i_NPROJNR;
      $this->i_nKassaNR = $i_nKassaNR;
      $this->i_nJournalNr = $i_nJournalNr;
      $this->i_fBetrag = $i_fBetrag;
      $this->i_szUidWert = $i_szUidWert;
      $this->i_nVerkaufsKanalNummer = $i_nVerkaufsKanalNummer;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\postLeistungsInfoToSwissPass
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_NPROJNR()
    {
      return $this->i_NPROJNR;
    }

    /**
     * @param float $i_NPROJNR
     * @return \Axess\Dci4Wtp\postLeistungsInfoToSwissPass
     */
    public function setI_NPROJNR($i_NPROJNR)
    {
      $this->i_NPROJNR = $i_NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nKassaNR()
    {
      return $this->i_nKassaNR;
    }

    /**
     * @param float $i_nKassaNR
     * @return \Axess\Dci4Wtp\postLeistungsInfoToSwissPass
     */
    public function setI_nKassaNR($i_nKassaNR)
    {
      $this->i_nKassaNR = $i_nKassaNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nJournalNr()
    {
      return $this->i_nJournalNr;
    }

    /**
     * @param float $i_nJournalNr
     * @return \Axess\Dci4Wtp\postLeistungsInfoToSwissPass
     */
    public function setI_nJournalNr($i_nJournalNr)
    {
      $this->i_nJournalNr = $i_nJournalNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_fBetrag()
    {
      return $this->i_fBetrag;
    }

    /**
     * @param float $i_fBetrag
     * @return \Axess\Dci4Wtp\postLeistungsInfoToSwissPass
     */
    public function setI_fBetrag($i_fBetrag)
    {
      $this->i_fBetrag = $i_fBetrag;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szUidWert()
    {
      return $this->i_szUidWert;
    }

    /**
     * @param string $i_szUidWert
     * @return \Axess\Dci4Wtp\postLeistungsInfoToSwissPass
     */
    public function setI_szUidWert($i_szUidWert)
    {
      $this->i_szUidWert = $i_szUidWert;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nVerkaufsKanalNummer()
    {
      return $this->i_nVerkaufsKanalNummer;
    }

    /**
     * @param float $i_nVerkaufsKanalNummer
     * @return \Axess\Dci4Wtp\postLeistungsInfoToSwissPass
     */
    public function setI_nVerkaufsKanalNummer($i_nVerkaufsKanalNummer)
    {
      $this->i_nVerkaufsKanalNummer = $i_nVerkaufsKanalNummer;
      return $this;
    }

}
