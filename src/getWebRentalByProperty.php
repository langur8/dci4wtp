<?php

namespace Axess\Dci4Wtp;

class getWebRentalByProperty
{

    /**
     * @var D4WTPWEBRENTALBYPROPERTYREQ $i_ctGetWebRenatlByPropertyReq
     */
    protected $i_ctGetWebRenatlByPropertyReq = null;

    /**
     * @param D4WTPWEBRENTALBYPROPERTYREQ $i_ctGetWebRenatlByPropertyReq
     */
    public function __construct($i_ctGetWebRenatlByPropertyReq)
    {
      $this->i_ctGetWebRenatlByPropertyReq = $i_ctGetWebRenatlByPropertyReq;
    }

    /**
     * @return D4WTPWEBRENTALBYPROPERTYREQ
     */
    public function getI_ctGetWebRenatlByPropertyReq()
    {
      return $this->i_ctGetWebRenatlByPropertyReq;
    }

    /**
     * @param D4WTPWEBRENTALBYPROPERTYREQ $i_ctGetWebRenatlByPropertyReq
     * @return \Axess\Dci4Wtp\getWebRentalByProperty
     */
    public function setI_ctGetWebRenatlByPropertyReq($i_ctGetWebRenatlByPropertyReq)
    {
      $this->i_ctGetWebRenatlByPropertyReq = $i_ctGetWebRenatlByPropertyReq;
      return $this;
    }

}
