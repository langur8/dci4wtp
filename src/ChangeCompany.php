<?php

namespace Axess\Dci4Wtp;

class ChangeCompany
{

    /**
     * @var D4WTPCOMPANYREQUEST $i_ctCompanyRequest
     */
    protected $i_ctCompanyRequest = null;

    /**
     * @param D4WTPCOMPANYREQUEST $i_ctCompanyRequest
     */
    public function __construct($i_ctCompanyRequest)
    {
      $this->i_ctCompanyRequest = $i_ctCompanyRequest;
    }

    /**
     * @return D4WTPCOMPANYREQUEST
     */
    public function getI_ctCompanyRequest()
    {
      return $this->i_ctCompanyRequest;
    }

    /**
     * @param D4WTPCOMPANYREQUEST $i_ctCompanyRequest
     * @return \Axess\Dci4Wtp\ChangeCompany
     */
    public function setI_ctCompanyRequest($i_ctCompanyRequest)
    {
      $this->i_ctCompanyRequest = $i_ctCompanyRequest;
      return $this;
    }

}
