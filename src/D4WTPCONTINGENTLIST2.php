<?php

namespace Axess\Dci4Wtp;

class D4WTPCONTINGENTLIST2
{

    /**
     * @var ArrayOfD4WTPSUBCONTINGENTLIST $ACTSUBCONTINGENTLIST
     */
    protected $ACTSUBCONTINGENTLIST = null;

    /**
     * @var float $BRESOPTIONAL
     */
    protected $BRESOPTIONAL = null;

    /**
     * @var D4WTPRESARTICLETARIFF $CTRESARTICLE
     */
    protected $CTRESARTICLE = null;

    /**
     * @var float $NCONTINGENTNR
     */
    protected $NCONTINGENTNR = null;

    /**
     * @var float $NCOUNTFREE
     */
    protected $NCOUNTFREE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSUBCONTINGENTLIST
     */
    public function getACTSUBCONTINGENTLIST()
    {
      return $this->ACTSUBCONTINGENTLIST;
    }

    /**
     * @param ArrayOfD4WTPSUBCONTINGENTLIST $ACTSUBCONTINGENTLIST
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST2
     */
    public function setACTSUBCONTINGENTLIST($ACTSUBCONTINGENTLIST)
    {
      $this->ACTSUBCONTINGENTLIST = $ACTSUBCONTINGENTLIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getBRESOPTIONAL()
    {
      return $this->BRESOPTIONAL;
    }

    /**
     * @param float $BRESOPTIONAL
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST2
     */
    public function setBRESOPTIONAL($BRESOPTIONAL)
    {
      $this->BRESOPTIONAL = $BRESOPTIONAL;
      return $this;
    }

    /**
     * @return D4WTPRESARTICLETARIFF
     */
    public function getCTRESARTICLE()
    {
      return $this->CTRESARTICLE;
    }

    /**
     * @param D4WTPRESARTICLETARIFF $CTRESARTICLE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST2
     */
    public function setCTRESARTICLE($CTRESARTICLE)
    {
      $this->CTRESARTICLE = $CTRESARTICLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNR()
    {
      return $this->NCONTINGENTNR;
    }

    /**
     * @param float $NCONTINGENTNR
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST2
     */
    public function setNCONTINGENTNR($NCONTINGENTNR)
    {
      $this->NCONTINGENTNR = $NCONTINGENTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTFREE()
    {
      return $this->NCOUNTFREE;
    }

    /**
     * @param float $NCOUNTFREE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST2
     */
    public function setNCOUNTFREE($NCOUNTFREE)
    {
      $this->NCOUNTFREE = $NCOUNTFREE;
      return $this;
    }

}
