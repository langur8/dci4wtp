<?php

namespace Axess\Dci4Wtp;

class getWTPGroupsResponse
{

    /**
     * @var D4WTPGROUPSRES $getWTPGroupsResult
     */
    protected $getWTPGroupsResult = null;

    /**
     * @param D4WTPGROUPSRES $getWTPGroupsResult
     */
    public function __construct($getWTPGroupsResult)
    {
      $this->getWTPGroupsResult = $getWTPGroupsResult;
    }

    /**
     * @return D4WTPGROUPSRES
     */
    public function getGetWTPGroupsResult()
    {
      return $this->getWTPGroupsResult;
    }

    /**
     * @param D4WTPGROUPSRES $getWTPGroupsResult
     * @return \Axess\Dci4Wtp\getWTPGroupsResponse
     */
    public function setGetWTPGroupsResult($getWTPGroupsResult)
    {
      $this->getWTPGroupsResult = $getWTPGroupsResult;
      return $this;
    }

}
