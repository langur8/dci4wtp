<?php

namespace Axess\Dci4Wtp;

class getClientSetup
{

    /**
     * @var D4WTPGETCLIENTSETUPREQUEST $i_ctGetClientSetupReq
     */
    protected $i_ctGetClientSetupReq = null;

    /**
     * @param D4WTPGETCLIENTSETUPREQUEST $i_ctGetClientSetupReq
     */
    public function __construct($i_ctGetClientSetupReq)
    {
      $this->i_ctGetClientSetupReq = $i_ctGetClientSetupReq;
    }

    /**
     * @return D4WTPGETCLIENTSETUPREQUEST
     */
    public function getI_ctGetClientSetupReq()
    {
      return $this->i_ctGetClientSetupReq;
    }

    /**
     * @param D4WTPGETCLIENTSETUPREQUEST $i_ctGetClientSetupReq
     * @return \Axess\Dci4Wtp\getClientSetup
     */
    public function setI_ctGetClientSetupReq($i_ctGetClientSetupReq)
    {
      $this->i_ctGetClientSetupReq = $i_ctGetClientSetupReq;
      return $this;
    }

}
