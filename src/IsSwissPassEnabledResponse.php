<?php

namespace Axess\Dci4Wtp;

class IsSwissPassEnabledResponse
{

    /**
     * @var D4WTPRESULT $IsSwissPassEnabledResult
     */
    protected $IsSwissPassEnabledResult = null;

    /**
     * @param D4WTPRESULT $IsSwissPassEnabledResult
     */
    public function __construct($IsSwissPassEnabledResult)
    {
      $this->IsSwissPassEnabledResult = $IsSwissPassEnabledResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getIsSwissPassEnabledResult()
    {
      return $this->IsSwissPassEnabledResult;
    }

    /**
     * @param D4WTPRESULT $IsSwissPassEnabledResult
     * @return \Axess\Dci4Wtp\IsSwissPassEnabledResponse
     */
    public function setIsSwissPassEnabledResult($IsSwissPassEnabledResult)
    {
      $this->IsSwissPassEnabledResult = $IsSwissPassEnabledResult;
      return $this;
    }

}
