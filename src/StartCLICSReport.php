<?php

namespace Axess\Dci4Wtp;

class StartCLICSReport
{

    /**
     * @var D4WTPSTARTCLICSREPORTREQ $i_ctStartCLICSReportsReq
     */
    protected $i_ctStartCLICSReportsReq = null;

    /**
     * @param D4WTPSTARTCLICSREPORTREQ $i_ctStartCLICSReportsReq
     */
    public function __construct($i_ctStartCLICSReportsReq)
    {
      $this->i_ctStartCLICSReportsReq = $i_ctStartCLICSReportsReq;
    }

    /**
     * @return D4WTPSTARTCLICSREPORTREQ
     */
    public function getI_ctStartCLICSReportsReq()
    {
      return $this->i_ctStartCLICSReportsReq;
    }

    /**
     * @param D4WTPSTARTCLICSREPORTREQ $i_ctStartCLICSReportsReq
     * @return \Axess\Dci4Wtp\StartCLICSReport
     */
    public function setI_ctStartCLICSReportsReq($i_ctStartCLICSReportsReq)
    {
      $this->i_ctStartCLICSReportsReq = $i_ctStartCLICSReportsReq;
      return $this;
    }

}
