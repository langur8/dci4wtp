<?php

namespace Axess\Dci4Wtp;

class D4WTPPERSSALESDATARESULT2
{

    /**
     * @var ArrayOfD4WTPPERSSALESDATA2 $ACTPERSSALESDATA
     */
    protected $ACTPERSSALESDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPERSSALESDATA2
     */
    public function getACTPERSSALESDATA()
    {
      return $this->ACTPERSSALESDATA;
    }

    /**
     * @param ArrayOfD4WTPPERSSALESDATA2 $ACTPERSSALESDATA
     * @return \Axess\Dci4Wtp\D4WTPPERSSALESDATARESULT2
     */
    public function setACTPERSSALESDATA($ACTPERSSALESDATA)
    {
      $this->ACTPERSSALESDATA = $ACTPERSSALESDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPERSSALESDATARESULT2
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPERSSALESDATARESULT2
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
