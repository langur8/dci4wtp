<?php

namespace Axess\Dci4Wtp;

class DCI4WTPGETWEBRIBYPROPOV
{

    /**
     * @var float $NPROPERTYEXTTYPE
     */
    protected $NPROPERTYEXTTYPE = null;

    /**
     * @var float $NPROPERTYEXTVALUE
     */
    protected $NPROPERTYEXTVALUE = null;

    /**
     * @var float $NPROPERTYTYPE
     */
    protected $NPROPERTYTYPE = null;

    /**
     * @var float $NPROPERTYVALUE
     */
    protected $NPROPERTYVALUE = null;

    /**
     * @var string $SZPROPERTYEXTVALUE
     */
    protected $SZPROPERTYEXTVALUE = null;

    /**
     * @var string $SZPROPERTYVALUE
     */
    protected $SZPROPERTYVALUE = null;

    /**
     * @var string $SZSHOWPROPERTYEXTVALUE
     */
    protected $SZSHOWPROPERTYEXTVALUE = null;

    /**
     * @var string $SZSHOWPROPERTYVALUE
     */
    protected $SZSHOWPROPERTYVALUE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPROPERTYEXTTYPE()
    {
      return $this->NPROPERTYEXTTYPE;
    }

    /**
     * @param float $NPROPERTYEXTTYPE
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRIBYPROPOV
     */
    public function setNPROPERTYEXTTYPE($NPROPERTYEXTTYPE)
    {
      $this->NPROPERTYEXTTYPE = $NPROPERTYEXTTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROPERTYEXTVALUE()
    {
      return $this->NPROPERTYEXTVALUE;
    }

    /**
     * @param float $NPROPERTYEXTVALUE
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRIBYPROPOV
     */
    public function setNPROPERTYEXTVALUE($NPROPERTYEXTVALUE)
    {
      $this->NPROPERTYEXTVALUE = $NPROPERTYEXTVALUE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROPERTYTYPE()
    {
      return $this->NPROPERTYTYPE;
    }

    /**
     * @param float $NPROPERTYTYPE
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRIBYPROPOV
     */
    public function setNPROPERTYTYPE($NPROPERTYTYPE)
    {
      $this->NPROPERTYTYPE = $NPROPERTYTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROPERTYVALUE()
    {
      return $this->NPROPERTYVALUE;
    }

    /**
     * @param float $NPROPERTYVALUE
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRIBYPROPOV
     */
    public function setNPROPERTYVALUE($NPROPERTYVALUE)
    {
      $this->NPROPERTYVALUE = $NPROPERTYVALUE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPROPERTYEXTVALUE()
    {
      return $this->SZPROPERTYEXTVALUE;
    }

    /**
     * @param string $SZPROPERTYEXTVALUE
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRIBYPROPOV
     */
    public function setSZPROPERTYEXTVALUE($SZPROPERTYEXTVALUE)
    {
      $this->SZPROPERTYEXTVALUE = $SZPROPERTYEXTVALUE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPROPERTYVALUE()
    {
      return $this->SZPROPERTYVALUE;
    }

    /**
     * @param string $SZPROPERTYVALUE
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRIBYPROPOV
     */
    public function setSZPROPERTYVALUE($SZPROPERTYVALUE)
    {
      $this->SZPROPERTYVALUE = $SZPROPERTYVALUE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHOWPROPERTYEXTVALUE()
    {
      return $this->SZSHOWPROPERTYEXTVALUE;
    }

    /**
     * @param string $SZSHOWPROPERTYEXTVALUE
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRIBYPROPOV
     */
    public function setSZSHOWPROPERTYEXTVALUE($SZSHOWPROPERTYEXTVALUE)
    {
      $this->SZSHOWPROPERTYEXTVALUE = $SZSHOWPROPERTYEXTVALUE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHOWPROPERTYVALUE()
    {
      return $this->SZSHOWPROPERTYVALUE;
    }

    /**
     * @param string $SZSHOWPROPERTYVALUE
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRIBYPROPOV
     */
    public function setSZSHOWPROPERTYVALUE($SZSHOWPROPERTYVALUE)
    {
      $this->SZSHOWPROPERTYVALUE = $SZSHOWPROPERTYVALUE;
      return $this;
    }

}
