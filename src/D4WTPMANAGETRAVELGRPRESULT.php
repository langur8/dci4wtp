<?php

namespace Axess\Dci4Wtp;

class D4WTPMANAGETRAVELGRPRESULT
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NTRAVELGROUPNO
     */
    protected $NTRAVELGROUPNO = null;

    /**
     * @var string $SZCREATEDATE
     */
    protected $SZCREATEDATE = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPMANAGETRAVELGRPRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPNO()
    {
      return $this->NTRAVELGROUPNO;
    }

    /**
     * @param float $NTRAVELGROUPNO
     * @return \Axess\Dci4Wtp\D4WTPMANAGETRAVELGRPRESULT
     */
    public function setNTRAVELGROUPNO($NTRAVELGROUPNO)
    {
      $this->NTRAVELGROUPNO = $NTRAVELGROUPNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCREATEDATE()
    {
      return $this->SZCREATEDATE;
    }

    /**
     * @param string $SZCREATEDATE
     * @return \Axess\Dci4Wtp\D4WTPMANAGETRAVELGRPRESULT
     */
    public function setSZCREATEDATE($SZCREATEDATE)
    {
      $this->SZCREATEDATE = $SZCREATEDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPMANAGETRAVELGRPRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
