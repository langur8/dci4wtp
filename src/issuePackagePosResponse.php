<?php

namespace Axess\Dci4Wtp;

class issuePackagePosResponse
{

    /**
     * @var D4WTPISSUEPACKAGERESULT $issuePackagePosResult
     */
    protected $issuePackagePosResult = null;

    /**
     * @param D4WTPISSUEPACKAGERESULT $issuePackagePosResult
     */
    public function __construct($issuePackagePosResult)
    {
      $this->issuePackagePosResult = $issuePackagePosResult;
    }

    /**
     * @return D4WTPISSUEPACKAGERESULT
     */
    public function getIssuePackagePosResult()
    {
      return $this->issuePackagePosResult;
    }

    /**
     * @param D4WTPISSUEPACKAGERESULT $issuePackagePosResult
     * @return \Axess\Dci4Wtp\issuePackagePosResponse
     */
    public function setIssuePackagePosResult($issuePackagePosResult)
    {
      $this->issuePackagePosResult = $issuePackagePosResult;
      return $this;
    }

}
