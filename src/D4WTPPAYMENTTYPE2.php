<?php

namespace Axess\Dci4Wtp;

class D4WTPPAYMENTTYPE2
{

    /**
     * @var float $BPAYWHOLETRANSACTION
     */
    protected $BPAYWHOLETRANSACTION = null;

    /**
     * @var D4WTPEMONEYACCOUNT $CTEMONEYACCOUNT
     */
    protected $CTEMONEYACCOUNT = null;

    /**
     * @var float $NAMOUNT
     */
    protected $NAMOUNT = null;

    /**
     * @var float $NBASICPAYMENTTYPE
     */
    protected $NBASICPAYMENTTYPE = null;

    /**
     * @var float $NCUSTPAYMENTTYPE
     */
    protected $NCUSTPAYMENTTYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBPAYWHOLETRANSACTION()
    {
      return $this->BPAYWHOLETRANSACTION;
    }

    /**
     * @param float $BPAYWHOLETRANSACTION
     * @return \Axess\Dci4Wtp\D4WTPPAYMENTTYPE2
     */
    public function setBPAYWHOLETRANSACTION($BPAYWHOLETRANSACTION)
    {
      $this->BPAYWHOLETRANSACTION = $BPAYWHOLETRANSACTION;
      return $this;
    }

    /**
     * @return D4WTPEMONEYACCOUNT
     */
    public function getCTEMONEYACCOUNT()
    {
      return $this->CTEMONEYACCOUNT;
    }

    /**
     * @param D4WTPEMONEYACCOUNT $CTEMONEYACCOUNT
     * @return \Axess\Dci4Wtp\D4WTPPAYMENTTYPE2
     */
    public function setCTEMONEYACCOUNT($CTEMONEYACCOUNT)
    {
      $this->CTEMONEYACCOUNT = $CTEMONEYACCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAMOUNT()
    {
      return $this->NAMOUNT;
    }

    /**
     * @param float $NAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPPAYMENTTYPE2
     */
    public function setNAMOUNT($NAMOUNT)
    {
      $this->NAMOUNT = $NAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBASICPAYMENTTYPE()
    {
      return $this->NBASICPAYMENTTYPE;
    }

    /**
     * @param float $NBASICPAYMENTTYPE
     * @return \Axess\Dci4Wtp\D4WTPPAYMENTTYPE2
     */
    public function setNBASICPAYMENTTYPE($NBASICPAYMENTTYPE)
    {
      $this->NBASICPAYMENTTYPE = $NBASICPAYMENTTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPAYMENTTYPE()
    {
      return $this->NCUSTPAYMENTTYPE;
    }

    /**
     * @param float $NCUSTPAYMENTTYPE
     * @return \Axess\Dci4Wtp\D4WTPPAYMENTTYPE2
     */
    public function setNCUSTPAYMENTTYPE($NCUSTPAYMENTTYPE)
    {
      $this->NCUSTPAYMENTTYPE = $NCUSTPAYMENTTYPE;
      return $this;
    }

}
