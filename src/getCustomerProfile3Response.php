<?php

namespace Axess\Dci4Wtp;

class getCustomerProfile3Response
{

    /**
     * @var D4WTPCUSTOMERPROFILE3RESULT $getCustomerProfile3Result
     */
    protected $getCustomerProfile3Result = null;

    /**
     * @param D4WTPCUSTOMERPROFILE3RESULT $getCustomerProfile3Result
     */
    public function __construct($getCustomerProfile3Result)
    {
      $this->getCustomerProfile3Result = $getCustomerProfile3Result;
    }

    /**
     * @return D4WTPCUSTOMERPROFILE3RESULT
     */
    public function getGetCustomerProfile3Result()
    {
      return $this->getCustomerProfile3Result;
    }

    /**
     * @param D4WTPCUSTOMERPROFILE3RESULT $getCustomerProfile3Result
     * @return \Axess\Dci4Wtp\getCustomerProfile3Response
     */
    public function setGetCustomerProfile3Result($getCustomerProfile3Result)
    {
      $this->getCustomerProfile3Result = $getCustomerProfile3Result;
      return $this;
    }

}
