<?php

namespace Axess\Dci4Wtp;

class setStateOCCPickupProductionResponse
{

    /**
     * @var D4WTPRESULT $setStateOCCPickupProductionResult
     */
    protected $setStateOCCPickupProductionResult = null;

    /**
     * @param D4WTPRESULT $setStateOCCPickupProductionResult
     */
    public function __construct($setStateOCCPickupProductionResult)
    {
      $this->setStateOCCPickupProductionResult = $setStateOCCPickupProductionResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSetStateOCCPickupProductionResult()
    {
      return $this->setStateOCCPickupProductionResult;
    }

    /**
     * @param D4WTPRESULT $setStateOCCPickupProductionResult
     * @return \Axess\Dci4Wtp\setStateOCCPickupProductionResponse
     */
    public function setSetStateOCCPickupProductionResult($setStateOCCPickupProductionResult)
    {
      $this->setStateOCCPickupProductionResult = $setStateOCCPickupProductionResult;
      return $this;
    }

}
