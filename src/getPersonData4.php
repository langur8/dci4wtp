<?php

namespace Axess\Dci4Wtp;

class getPersonData4
{

    /**
     * @var D4WTPGETPERSON3REQUEST $i_ctPersData3Req
     */
    protected $i_ctPersData3Req = null;

    /**
     * @param D4WTPGETPERSON3REQUEST $i_ctPersData3Req
     */
    public function __construct($i_ctPersData3Req)
    {
      $this->i_ctPersData3Req = $i_ctPersData3Req;
    }

    /**
     * @return D4WTPGETPERSON3REQUEST
     */
    public function getI_ctPersData3Req()
    {
      return $this->i_ctPersData3Req;
    }

    /**
     * @param D4WTPGETPERSON3REQUEST $i_ctPersData3Req
     * @return \Axess\Dci4Wtp\getPersonData4
     */
    public function setI_ctPersData3Req($i_ctPersData3Req)
    {
      $this->i_ctPersData3Req = $i_ctPersData3Req;
      return $this;
    }

}
