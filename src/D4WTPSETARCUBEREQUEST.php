<?php

namespace Axess\Dci4Wtp;

class D4WTPSETARCUBEREQUEST
{

    /**
     * @var D4WTPARCUBE $D4WTPARCUBE
     */
    protected $D4WTPARCUBE = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPARCUBE
     */
    public function getD4WTPARCUBE()
    {
      return $this->D4WTPARCUBE;
    }

    /**
     * @param D4WTPARCUBE $D4WTPARCUBE
     * @return \Axess\Dci4Wtp\D4WTPSETARCUBEREQUEST
     */
    public function setD4WTPARCUBE($D4WTPARCUBE)
    {
      $this->D4WTPARCUBE = $D4WTPARCUBE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSETARCUBEREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
