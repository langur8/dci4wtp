<?php

namespace Axess\Dci4Wtp;

class D4WTPMSGISSUETICKETREQUEST7
{

    /**
     * @var ArrayOfD4WTPMSGTICKETDATA4 $ACTMSGTICKETDATA
     */
    protected $ACTMSGTICKETDATA = null;

    /**
     * @var float $BMODIFYPRICE
     */
    protected $BMODIFYPRICE = null;

    /**
     * @var float $FPRICETRANSACTION
     */
    protected $FPRICETRANSACTION = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPMSGTICKETDATA4
     */
    public function getACTMSGTICKETDATA()
    {
      return $this->ACTMSGTICKETDATA;
    }

    /**
     * @param ArrayOfD4WTPMSGTICKETDATA4 $ACTMSGTICKETDATA
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETREQUEST7
     */
    public function setACTMSGTICKETDATA($ACTMSGTICKETDATA)
    {
      $this->ACTMSGTICKETDATA = $ACTMSGTICKETDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getBMODIFYPRICE()
    {
      return $this->BMODIFYPRICE;
    }

    /**
     * @param float $BMODIFYPRICE
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETREQUEST7
     */
    public function setBMODIFYPRICE($BMODIFYPRICE)
    {
      $this->BMODIFYPRICE = $BMODIFYPRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getFPRICETRANSACTION()
    {
      return $this->FPRICETRANSACTION;
    }

    /**
     * @param float $FPRICETRANSACTION
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETREQUEST7
     */
    public function setFPRICETRANSACTION($FPRICETRANSACTION)
    {
      $this->FPRICETRANSACTION = $FPRICETRANSACTION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETREQUEST7
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETREQUEST7
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

}
