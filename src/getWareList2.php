<?php

namespace Axess\Dci4Wtp;

class getWareList2
{

    /**
     * @var D4WTPGETWARELISTREQUEST $i_ctWareListReq
     */
    protected $i_ctWareListReq = null;

    /**
     * @param D4WTPGETWARELISTREQUEST $i_ctWareListReq
     */
    public function __construct($i_ctWareListReq)
    {
      $this->i_ctWareListReq = $i_ctWareListReq;
    }

    /**
     * @return D4WTPGETWARELISTREQUEST
     */
    public function getI_ctWareListReq()
    {
      return $this->i_ctWareListReq;
    }

    /**
     * @param D4WTPGETWARELISTREQUEST $i_ctWareListReq
     * @return \Axess\Dci4Wtp\getWareList2
     */
    public function setI_ctWareListReq($i_ctWareListReq)
    {
      $this->i_ctWareListReq = $i_ctWareListReq;
      return $this;
    }

}
