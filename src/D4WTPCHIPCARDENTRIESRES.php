<?php

namespace Axess\Dci4Wtp;

class D4WTPCHIPCARDENTRIESRES
{

    /**
     * @var ArrayOfD4WTPCHIPCARD $CHIPCARDS
     */
    protected $CHIPCARDS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCHIPCARD
     */
    public function getCHIPCARDS()
    {
      return $this->CHIPCARDS;
    }

    /**
     * @param ArrayOfD4WTPCHIPCARD $CHIPCARDS
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARDENTRIESRES
     */
    public function setCHIPCARDS($CHIPCARDS)
    {
      $this->CHIPCARDS = $CHIPCARDS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARDENTRIESRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARDENTRIESRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
