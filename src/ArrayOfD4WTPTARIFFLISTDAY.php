<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTARIFFLISTDAY implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTARIFFLISTDAY[] $D4WTPTARIFFLISTDAY
     */
    protected $D4WTPTARIFFLISTDAY = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTARIFFLISTDAY[]
     */
    public function getD4WTPTARIFFLISTDAY()
    {
      return $this->D4WTPTARIFFLISTDAY;
    }

    /**
     * @param D4WTPTARIFFLISTDAY[] $D4WTPTARIFFLISTDAY
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTARIFFLISTDAY
     */
    public function setD4WTPTARIFFLISTDAY(array $D4WTPTARIFFLISTDAY = null)
    {
      $this->D4WTPTARIFFLISTDAY = $D4WTPTARIFFLISTDAY;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTARIFFLISTDAY[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTARIFFLISTDAY
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTARIFFLISTDAY[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTARIFFLISTDAY $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTARIFFLISTDAY[] = $value;
      } else {
        $this->D4WTPTARIFFLISTDAY[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTARIFFLISTDAY[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTARIFFLISTDAY Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTARIFFLISTDAY);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTARIFFLISTDAY);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTARIFFLISTDAY);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTARIFFLISTDAY);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTARIFFLISTDAY Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTARIFFLISTDAY);
    }

}
