<?php

namespace Axess\Dci4Wtp;

class getRentalItemTariffResponse
{

    /**
     * @var D4WTPGETRENTALITEMTARIFFRES $getRentalItemTariffResult
     */
    protected $getRentalItemTariffResult = null;

    /**
     * @param D4WTPGETRENTALITEMTARIFFRES $getRentalItemTariffResult
     */
    public function __construct($getRentalItemTariffResult)
    {
      $this->getRentalItemTariffResult = $getRentalItemTariffResult;
    }

    /**
     * @return D4WTPGETRENTALITEMTARIFFRES
     */
    public function getGetRentalItemTariffResult()
    {
      return $this->getRentalItemTariffResult;
    }

    /**
     * @param D4WTPGETRENTALITEMTARIFFRES $getRentalItemTariffResult
     * @return \Axess\Dci4Wtp\getRentalItemTariffResponse
     */
    public function setGetRentalItemTariffResult($getRentalItemTariffResult)
    {
      $this->getRentalItemTariffResult = $getRentalItemTariffResult;
      return $this;
    }

}
