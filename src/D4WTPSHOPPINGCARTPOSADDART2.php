<?php

namespace Axess\Dci4Wtp;

class D4WTPSHOPPINGCARTPOSADDART2
{

    /**
     * @var float $FDISCOUNTEDPRICE
     */
    protected $FDISCOUNTEDPRICE = null;

    /**
     * @var float $NALLDISCOUNTNO
     */
    protected $NALLDISCOUNTNO = null;

    /**
     * @var float $NALLDISCOUNTSCALENO
     */
    protected $NALLDISCOUNTSCALENO = null;

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NARTICLEPACKAGENO
     */
    protected $NARTICLEPACKAGENO = null;

    /**
     * @var float $NLFDPOSNO
     */
    protected $NLFDPOSNO = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NUNITPRICE
     */
    protected $NUNITPRICE = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFDISCOUNTEDPRICE()
    {
      return $this->FDISCOUNTEDPRICE;
    }

    /**
     * @param float $FDISCOUNTEDPRICE
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSADDART2
     */
    public function setFDISCOUNTEDPRICE($FDISCOUNTEDPRICE)
    {
      $this->FDISCOUNTEDPRICE = $FDISCOUNTEDPRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNALLDISCOUNTNO()
    {
      return $this->NALLDISCOUNTNO;
    }

    /**
     * @param float $NALLDISCOUNTNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSADDART2
     */
    public function setNALLDISCOUNTNO($NALLDISCOUNTNO)
    {
      $this->NALLDISCOUNTNO = $NALLDISCOUNTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNALLDISCOUNTSCALENO()
    {
      return $this->NALLDISCOUNTSCALENO;
    }

    /**
     * @param float $NALLDISCOUNTSCALENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSADDART2
     */
    public function setNALLDISCOUNTSCALENO($NALLDISCOUNTSCALENO)
    {
      $this->NALLDISCOUNTSCALENO = $NALLDISCOUNTSCALENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSADDART2
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLEPACKAGENO()
    {
      return $this->NARTICLEPACKAGENO;
    }

    /**
     * @param float $NARTICLEPACKAGENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSADDART2
     */
    public function setNARTICLEPACKAGENO($NARTICLEPACKAGENO)
    {
      $this->NARTICLEPACKAGENO = $NARTICLEPACKAGENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDPOSNO()
    {
      return $this->NLFDPOSNO;
    }

    /**
     * @param float $NLFDPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSADDART2
     */
    public function setNLFDPOSNO($NLFDPOSNO)
    {
      $this->NLFDPOSNO = $NLFDPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSADDART2
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNITPRICE()
    {
      return $this->NUNITPRICE;
    }

    /**
     * @param float $NUNITPRICE
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSADDART2
     */
    public function setNUNITPRICE($NUNITPRICE)
    {
      $this->NUNITPRICE = $NUNITPRICE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSADDART2
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

}
