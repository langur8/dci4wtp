<?php

namespace Axess\Dci4Wtp;

class D4WTPPAYER2RESULT
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NFIRMENNR
     */
    protected $NFIRMENNR = null;

    /**
     * @var float $NFIRMENPOSNR
     */
    protected $NFIRMENPOSNR = null;

    /**
     * @var float $NFIRMENPROJNR
     */
    protected $NFIRMENPROJNR = null;

    /**
     * @var float $NPERSPERSNR
     */
    protected $NPERSPERSNR = null;

    /**
     * @var float $NPERSPOSNR
     */
    protected $NPERSPOSNR = null;

    /**
     * @var float $NPERSPROJNR
     */
    protected $NPERSPROJNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPAYER2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRMENNR()
    {
      return $this->NFIRMENNR;
    }

    /**
     * @param float $NFIRMENNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2RESULT
     */
    public function setNFIRMENNR($NFIRMENNR)
    {
      $this->NFIRMENNR = $NFIRMENNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRMENPOSNR()
    {
      return $this->NFIRMENPOSNR;
    }

    /**
     * @param float $NFIRMENPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2RESULT
     */
    public function setNFIRMENPOSNR($NFIRMENPOSNR)
    {
      $this->NFIRMENPOSNR = $NFIRMENPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRMENPROJNR()
    {
      return $this->NFIRMENPROJNR;
    }

    /**
     * @param float $NFIRMENPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2RESULT
     */
    public function setNFIRMENPROJNR($NFIRMENPROJNR)
    {
      $this->NFIRMENPROJNR = $NFIRMENPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPERSNR()
    {
      return $this->NPERSPERSNR;
    }

    /**
     * @param float $NPERSPERSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2RESULT
     */
    public function setNPERSPERSNR($NPERSPERSNR)
    {
      $this->NPERSPERSNR = $NPERSPERSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNR()
    {
      return $this->NPERSPOSNR;
    }

    /**
     * @param float $NPERSPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2RESULT
     */
    public function setNPERSPOSNR($NPERSPOSNR)
    {
      $this->NPERSPOSNR = $NPERSPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNR()
    {
      return $this->NPERSPROJNR;
    }

    /**
     * @param float $NPERSPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2RESULT
     */
    public function setNPERSPROJNR($NPERSPROJNR)
    {
      $this->NPERSPROJNR = $NPERSPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPAYER2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
