<?php

namespace Axess\Dci4Wtp;

class D4WTPDISCOUNTDEF
{

    /**
     * @var float $BISFREEDEFINABLE
     */
    protected $BISFREEDEFINABLE = null;

    /**
     * @var float $NDISCOUNTNO
     */
    protected $NDISCOUNTNO = null;

    /**
     * @var float $NDISCOUNTSHEETNO
     */
    protected $NDISCOUNTSHEETNO = null;

    /**
     * @var float $NPERCENT
     */
    protected $NPERCENT = null;

    /**
     * @var string $SZDISCOUNTNAME
     */
    protected $SZDISCOUNTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISFREEDEFINABLE()
    {
      return $this->BISFREEDEFINABLE;
    }

    /**
     * @param float $BISFREEDEFINABLE
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTDEF
     */
    public function setBISFREEDEFINABLE($BISFREEDEFINABLE)
    {
      $this->BISFREEDEFINABLE = $BISFREEDEFINABLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDISCOUNTNO()
    {
      return $this->NDISCOUNTNO;
    }

    /**
     * @param float $NDISCOUNTNO
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTDEF
     */
    public function setNDISCOUNTNO($NDISCOUNTNO)
    {
      $this->NDISCOUNTNO = $NDISCOUNTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDISCOUNTSHEETNO()
    {
      return $this->NDISCOUNTSHEETNO;
    }

    /**
     * @param float $NDISCOUNTSHEETNO
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTDEF
     */
    public function setNDISCOUNTSHEETNO($NDISCOUNTSHEETNO)
    {
      $this->NDISCOUNTSHEETNO = $NDISCOUNTSHEETNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERCENT()
    {
      return $this->NPERCENT;
    }

    /**
     * @param float $NPERCENT
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTDEF
     */
    public function setNPERCENT($NPERCENT)
    {
      $this->NPERCENT = $NPERCENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDISCOUNTNAME()
    {
      return $this->SZDISCOUNTNAME;
    }

    /**
     * @param string $SZDISCOUNTNAME
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTDEF
     */
    public function setSZDISCOUNTNAME($SZDISCOUNTNAME)
    {
      $this->SZDISCOUNTNAME = $SZDISCOUNTNAME;
      return $this;
    }

}
