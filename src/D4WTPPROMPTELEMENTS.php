<?php

namespace Axess\Dci4Wtp;

class D4WTPPROMPTELEMENTS
{

    /**
     * @var float $NPROMPTELEMENTNO
     */
    protected $NPROMPTELEMENTNO = null;

    /**
     * @var string $SZELEMENTTEXT
     */
    protected $SZELEMENTTEXT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPROMPTELEMENTNO()
    {
      return $this->NPROMPTELEMENTNO;
    }

    /**
     * @param float $NPROMPTELEMENTNO
     * @return \Axess\Dci4Wtp\D4WTPPROMPTELEMENTS
     */
    public function setNPROMPTELEMENTNO($NPROMPTELEMENTNO)
    {
      $this->NPROMPTELEMENTNO = $NPROMPTELEMENTNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZELEMENTTEXT()
    {
      return $this->SZELEMENTTEXT;
    }

    /**
     * @param string $SZELEMENTTEXT
     * @return \Axess\Dci4Wtp\D4WTPPROMPTELEMENTS
     */
    public function setSZELEMENTTEXT($SZELEMENTTEXT)
    {
      $this->SZELEMENTTEXT = $SZELEMENTTEXT;
      return $this;
    }

}
