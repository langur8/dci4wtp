<?php

namespace Axess\Dci4Wtp;

class D4WTPSUBCONTNGNTTICKET3
{

    /**
     * @var float $NDURATIONINMIN
     */
    protected $NDURATIONINMIN = null;

    /**
     * @var float $NENTRYDURATIONNMIN
     */
    protected $NENTRYDURATIONNMIN = null;

    /**
     * @var float $NENTRYOFFSETINMIN
     */
    protected $NENTRYOFFSETINMIN = null;

    /**
     * @var float $NLANGUAGEID
     */
    protected $NLANGUAGEID = null;

    /**
     * @var float $NLOCKTIMEINMIN
     */
    protected $NLOCKTIMEINMIN = null;

    /**
     * @var float $NSUBCONTINGENTNO
     */
    protected $NSUBCONTINGENTNO = null;

    /**
     * @var float $NTICKETCOUNTFREE
     */
    protected $NTICKETCOUNTFREE = null;

    /**
     * @var float $NTICKETCOUNTTOTAL
     */
    protected $NTICKETCOUNTTOTAL = null;

    /**
     * @var string $SZADDITIONALINFO
     */
    protected $SZADDITIONALINFO = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSHORTNAME
     */
    protected $SZSHORTNAME = null;

    /**
     * @var string $SZSTARTTIME
     */
    protected $SZSTARTTIME = null;

    /**
     * @var string $SZSUBCONTINGENTMATCHCODE
     */
    protected $SZSUBCONTINGENTMATCHCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNDURATIONINMIN()
    {
      return $this->NDURATIONINMIN;
    }

    /**
     * @param float $NDURATIONINMIN
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTNGNTTICKET3
     */
    public function setNDURATIONINMIN($NDURATIONINMIN)
    {
      $this->NDURATIONINMIN = $NDURATIONINMIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getNENTRYDURATIONNMIN()
    {
      return $this->NENTRYDURATIONNMIN;
    }

    /**
     * @param float $NENTRYDURATIONNMIN
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTNGNTTICKET3
     */
    public function setNENTRYDURATIONNMIN($NENTRYDURATIONNMIN)
    {
      $this->NENTRYDURATIONNMIN = $NENTRYDURATIONNMIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getNENTRYOFFSETINMIN()
    {
      return $this->NENTRYOFFSETINMIN;
    }

    /**
     * @param float $NENTRYOFFSETINMIN
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTNGNTTICKET3
     */
    public function setNENTRYOFFSETINMIN($NENTRYOFFSETINMIN)
    {
      $this->NENTRYOFFSETINMIN = $NENTRYOFFSETINMIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLANGUAGEID()
    {
      return $this->NLANGUAGEID;
    }

    /**
     * @param float $NLANGUAGEID
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTNGNTTICKET3
     */
    public function setNLANGUAGEID($NLANGUAGEID)
    {
      $this->NLANGUAGEID = $NLANGUAGEID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLOCKTIMEINMIN()
    {
      return $this->NLOCKTIMEINMIN;
    }

    /**
     * @param float $NLOCKTIMEINMIN
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTNGNTTICKET3
     */
    public function setNLOCKTIMEINMIN($NLOCKTIMEINMIN)
    {
      $this->NLOCKTIMEINMIN = $NLOCKTIMEINMIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTINGENTNO()
    {
      return $this->NSUBCONTINGENTNO;
    }

    /**
     * @param float $NSUBCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTNGNTTICKET3
     */
    public function setNSUBCONTINGENTNO($NSUBCONTINGENTNO)
    {
      $this->NSUBCONTINGENTNO = $NSUBCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETCOUNTFREE()
    {
      return $this->NTICKETCOUNTFREE;
    }

    /**
     * @param float $NTICKETCOUNTFREE
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTNGNTTICKET3
     */
    public function setNTICKETCOUNTFREE($NTICKETCOUNTFREE)
    {
      $this->NTICKETCOUNTFREE = $NTICKETCOUNTFREE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETCOUNTTOTAL()
    {
      return $this->NTICKETCOUNTTOTAL;
    }

    /**
     * @param float $NTICKETCOUNTTOTAL
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTNGNTTICKET3
     */
    public function setNTICKETCOUNTTOTAL($NTICKETCOUNTTOTAL)
    {
      $this->NTICKETCOUNTTOTAL = $NTICKETCOUNTTOTAL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZADDITIONALINFO()
    {
      return $this->SZADDITIONALINFO;
    }

    /**
     * @param string $SZADDITIONALINFO
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTNGNTTICKET3
     */
    public function setSZADDITIONALINFO($SZADDITIONALINFO)
    {
      $this->SZADDITIONALINFO = $SZADDITIONALINFO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTNGNTTICKET3
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHORTNAME()
    {
      return $this->SZSHORTNAME;
    }

    /**
     * @param string $SZSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTNGNTTICKET3
     */
    public function setSZSHORTNAME($SZSHORTNAME)
    {
      $this->SZSHORTNAME = $SZSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTARTTIME()
    {
      return $this->SZSTARTTIME;
    }

    /**
     * @param string $SZSTARTTIME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTNGNTTICKET3
     */
    public function setSZSTARTTIME($SZSTARTTIME)
    {
      $this->SZSTARTTIME = $SZSTARTTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBCONTINGENTMATCHCODE()
    {
      return $this->SZSUBCONTINGENTMATCHCODE;
    }

    /**
     * @param string $SZSUBCONTINGENTMATCHCODE
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTNGNTTICKET3
     */
    public function setSZSUBCONTINGENTMATCHCODE($SZSUBCONTINGENTMATCHCODE)
    {
      $this->SZSUBCONTINGENTMATCHCODE = $SZSUBCONTINGENTMATCHCODE;
      return $this;
    }

}
