<?php

namespace Axess\Dci4Wtp;

class produceShoppingBasketPrepaid2
{

    /**
     * @var D4WTPPRODSHOPPOSPREPAIDREQ2 $i_ctProdShopPosPrepaidREQ
     */
    protected $i_ctProdShopPosPrepaidREQ = null;

    /**
     * @param D4WTPPRODSHOPPOSPREPAIDREQ2 $i_ctProdShopPosPrepaidREQ
     */
    public function __construct($i_ctProdShopPosPrepaidREQ)
    {
      $this->i_ctProdShopPosPrepaidREQ = $i_ctProdShopPosPrepaidREQ;
    }

    /**
     * @return D4WTPPRODSHOPPOSPREPAIDREQ2
     */
    public function getI_ctProdShopPosPrepaidREQ()
    {
      return $this->i_ctProdShopPosPrepaidREQ;
    }

    /**
     * @param D4WTPPRODSHOPPOSPREPAIDREQ2 $i_ctProdShopPosPrepaidREQ
     * @return \Axess\Dci4Wtp\produceShoppingBasketPrepaid2
     */
    public function setI_ctProdShopPosPrepaidREQ($i_ctProdShopPosPrepaidREQ)
    {
      $this->i_ctProdShopPosPrepaidREQ = $i_ctProdShopPosPrepaidREQ;
      return $this;
    }

}
