<?php

namespace Axess\Dci4Wtp;

class getShoppingCartData4
{

    /**
     * @var D4WTPGETSHOPCARTDATAREQUEST $i_ctGetShoppingCartDataReq
     */
    protected $i_ctGetShoppingCartDataReq = null;

    /**
     * @param D4WTPGETSHOPCARTDATAREQUEST $i_ctGetShoppingCartDataReq
     */
    public function __construct($i_ctGetShoppingCartDataReq)
    {
      $this->i_ctGetShoppingCartDataReq = $i_ctGetShoppingCartDataReq;
    }

    /**
     * @return D4WTPGETSHOPCARTDATAREQUEST
     */
    public function getI_ctGetShoppingCartDataReq()
    {
      return $this->i_ctGetShoppingCartDataReq;
    }

    /**
     * @param D4WTPGETSHOPCARTDATAREQUEST $i_ctGetShoppingCartDataReq
     * @return \Axess\Dci4Wtp\getShoppingCartData4
     */
    public function setI_ctGetShoppingCartDataReq($i_ctGetShoppingCartDataReq)
    {
      $this->i_ctGetShoppingCartDataReq = $i_ctGetShoppingCartDataReq;
      return $this;
    }

}
