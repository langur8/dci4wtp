<?php

namespace Axess\Dci4Wtp;

class D4WTPMEMBERRESULT
{

    /**
     * @var ArrayOfD4WTPMEMBERTYPE $ACTMEMBERTYPE
     */
    protected $ACTMEMBERTYPE = null;

    /**
     * @var float $BMEMBERSHIPACTIVE
     */
    protected $BMEMBERSHIPACTIVE = null;

    /**
     * @var D4WTPMEMBERDETAILS $CTPMEMBERDETAILS
     */
    protected $CTPMEMBERDETAILS = null;

    /**
     * @var float $NCOMPANYNR
     */
    protected $NCOMPANYNR = null;

    /**
     * @var float $NCOMPANYPOSNR
     */
    protected $NCOMPANYPOSNR = null;

    /**
     * @var float $NCOMPANYPROJNR
     */
    protected $NCOMPANYPROJNR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var string $SZAREA
     */
    protected $SZAREA = null;

    /**
     * @var string $SZCITY
     */
    protected $SZCITY = null;

    /**
     * @var string $SZCOUNTRY
     */
    protected $SZCOUNTRY = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    /**
     * @var string $SZDATEOFBIRTH
     */
    protected $SZDATEOFBIRTH = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZGENDER
     */
    protected $SZGENDER = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZMEMBERCREATIONDATE
     */
    protected $SZMEMBERCREATIONDATE = null;

    /**
     * @var string $SZMEMBERDESC
     */
    protected $SZMEMBERDESC = null;

    /**
     * @var string $SZMEMBERTOCCONFIRMED
     */
    protected $SZMEMBERTOCCONFIRMED = null;

    /**
     * @var string $SZMEMBERUSER
     */
    protected $SZMEMBERUSER = null;

    /**
     * @var string $SZMOBILE
     */
    protected $SZMOBILE = null;

    /**
     * @var string $SZPHONE
     */
    protected $SZPHONE = null;

    /**
     * @var string $SZSALUTATION
     */
    protected $SZSALUTATION = null;

    /**
     * @var string $SZSTREET
     */
    protected $SZSTREET = null;

    /**
     * @var string $SZSTREETNUMBER
     */
    protected $SZSTREETNUMBER = null;

    /**
     * @var string $SZTITLE
     */
    protected $SZTITLE = null;

    /**
     * @var string $SZZIPCODE
     */
    protected $SZZIPCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPMEMBERTYPE
     */
    public function getACTMEMBERTYPE()
    {
      return $this->ACTMEMBERTYPE;
    }

    /**
     * @param ArrayOfD4WTPMEMBERTYPE $ACTMEMBERTYPE
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setACTMEMBERTYPE($ACTMEMBERTYPE)
    {
      $this->ACTMEMBERTYPE = $ACTMEMBERTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBMEMBERSHIPACTIVE()
    {
      return $this->BMEMBERSHIPACTIVE;
    }

    /**
     * @param float $BMEMBERSHIPACTIVE
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setBMEMBERSHIPACTIVE($BMEMBERSHIPACTIVE)
    {
      $this->BMEMBERSHIPACTIVE = $BMEMBERSHIPACTIVE;
      return $this;
    }

    /**
     * @return D4WTPMEMBERDETAILS
     */
    public function getCTPMEMBERDETAILS()
    {
      return $this->CTPMEMBERDETAILS;
    }

    /**
     * @param D4WTPMEMBERDETAILS $CTPMEMBERDETAILS
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setCTPMEMBERDETAILS($CTPMEMBERDETAILS)
    {
      $this->CTPMEMBERDETAILS = $CTPMEMBERDETAILS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNR()
    {
      return $this->NCOMPANYNR;
    }

    /**
     * @param float $NCOMPANYNR
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setNCOMPANYNR($NCOMPANYNR)
    {
      $this->NCOMPANYNR = $NCOMPANYNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNR()
    {
      return $this->NCOMPANYPOSNR;
    }

    /**
     * @param float $NCOMPANYPOSNR
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setNCOMPANYPOSNR($NCOMPANYPOSNR)
    {
      $this->NCOMPANYPOSNR = $NCOMPANYPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNR()
    {
      return $this->NCOMPANYPROJNR;
    }

    /**
     * @param float $NCOMPANYPROJNR
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setNCOMPANYPROJNR($NCOMPANYPROJNR)
    {
      $this->NCOMPANYPROJNR = $NCOMPANYPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAREA()
    {
      return $this->SZAREA;
    }

    /**
     * @param string $SZAREA
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZAREA($SZAREA)
    {
      $this->SZAREA = $SZAREA;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCITY()
    {
      return $this->SZCITY;
    }

    /**
     * @param string $SZCITY
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZCITY($SZCITY)
    {
      $this->SZCITY = $SZCITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRY()
    {
      return $this->SZCOUNTRY;
    }

    /**
     * @param string $SZCOUNTRY
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZCOUNTRY($SZCOUNTRY)
    {
      $this->SZCOUNTRY = $SZCOUNTRY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEOFBIRTH()
    {
      return $this->SZDATEOFBIRTH;
    }

    /**
     * @param string $SZDATEOFBIRTH
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZDATEOFBIRTH($SZDATEOFBIRTH)
    {
      $this->SZDATEOFBIRTH = $SZDATEOFBIRTH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGENDER()
    {
      return $this->SZGENDER;
    }

    /**
     * @param string $SZGENDER
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZGENDER($SZGENDER)
    {
      $this->SZGENDER = $SZGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEMBERCREATIONDATE()
    {
      return $this->SZMEMBERCREATIONDATE;
    }

    /**
     * @param string $SZMEMBERCREATIONDATE
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZMEMBERCREATIONDATE($SZMEMBERCREATIONDATE)
    {
      $this->SZMEMBERCREATIONDATE = $SZMEMBERCREATIONDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEMBERDESC()
    {
      return $this->SZMEMBERDESC;
    }

    /**
     * @param string $SZMEMBERDESC
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZMEMBERDESC($SZMEMBERDESC)
    {
      $this->SZMEMBERDESC = $SZMEMBERDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEMBERTOCCONFIRMED()
    {
      return $this->SZMEMBERTOCCONFIRMED;
    }

    /**
     * @param string $SZMEMBERTOCCONFIRMED
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZMEMBERTOCCONFIRMED($SZMEMBERTOCCONFIRMED)
    {
      $this->SZMEMBERTOCCONFIRMED = $SZMEMBERTOCCONFIRMED;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEMBERUSER()
    {
      return $this->SZMEMBERUSER;
    }

    /**
     * @param string $SZMEMBERUSER
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZMEMBERUSER($SZMEMBERUSER)
    {
      $this->SZMEMBERUSER = $SZMEMBERUSER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMOBILE()
    {
      return $this->SZMOBILE;
    }

    /**
     * @param string $SZMOBILE
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZMOBILE($SZMOBILE)
    {
      $this->SZMOBILE = $SZMOBILE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPHONE()
    {
      return $this->SZPHONE;
    }

    /**
     * @param string $SZPHONE
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZPHONE($SZPHONE)
    {
      $this->SZPHONE = $SZPHONE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSALUTATION()
    {
      return $this->SZSALUTATION;
    }

    /**
     * @param string $SZSALUTATION
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZSALUTATION($SZSALUTATION)
    {
      $this->SZSALUTATION = $SZSALUTATION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREET()
    {
      return $this->SZSTREET;
    }

    /**
     * @param string $SZSTREET
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZSTREET($SZSTREET)
    {
      $this->SZSTREET = $SZSTREET;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREETNUMBER()
    {
      return $this->SZSTREETNUMBER;
    }

    /**
     * @param string $SZSTREETNUMBER
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZSTREETNUMBER($SZSTREETNUMBER)
    {
      $this->SZSTREETNUMBER = $SZSTREETNUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTITLE()
    {
      return $this->SZTITLE;
    }

    /**
     * @param string $SZTITLE
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZTITLE($SZTITLE)
    {
      $this->SZTITLE = $SZTITLE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZZIPCODE()
    {
      return $this->SZZIPCODE;
    }

    /**
     * @param string $SZZIPCODE
     * @return \Axess\Dci4Wtp\D4WTPMEMBERRESULT
     */
    public function setSZZIPCODE($SZZIPCODE)
    {
      $this->SZZIPCODE = $SZZIPCODE;
      return $this;
    }

}
