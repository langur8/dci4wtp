<?php

namespace Axess\Dci4Wtp;

class D4WTPPARKING
{

    /**
     * @var string $SZLICENSEPLATE
     */
    protected $SZLICENSEPLATE = null;

    /**
     * @var string $SZPARKINGSPACE
     */
    protected $SZPARKINGSPACE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZLICENSEPLATE()
    {
      return $this->SZLICENSEPLATE;
    }

    /**
     * @param string $SZLICENSEPLATE
     * @return \Axess\Dci4Wtp\D4WTPPARKING
     */
    public function setSZLICENSEPLATE($SZLICENSEPLATE)
    {
      $this->SZLICENSEPLATE = $SZLICENSEPLATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPARKINGSPACE()
    {
      return $this->SZPARKINGSPACE;
    }

    /**
     * @param string $SZPARKINGSPACE
     * @return \Axess\Dci4Wtp\D4WTPPARKING
     */
    public function setSZPARKINGSPACE($SZPARKINGSPACE)
    {
      $this->SZPARKINGSPACE = $SZPARKINGSPACE;
      return $this;
    }

}
