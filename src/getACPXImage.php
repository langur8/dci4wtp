<?php

namespace Axess\Dci4Wtp;

class getACPXImage
{

    /**
     * @var string $szPrintData
     */
    protected $szPrintData = null;

    /**
     * @var int $nResolutionMapToDPI
     */
    protected $nResolutionMapToDPI = null;

    /**
     * @param string $szPrintData
     * @param int $nResolutionMapToDPI
     */
    public function __construct($szPrintData, $nResolutionMapToDPI)
    {
      $this->szPrintData = $szPrintData;
      $this->nResolutionMapToDPI = $nResolutionMapToDPI;
    }

    /**
     * @return string
     */
    public function getSzPrintData()
    {
      return $this->szPrintData;
    }

    /**
     * @param string $szPrintData
     * @return \Axess\Dci4Wtp\getACPXImage
     */
    public function setSzPrintData($szPrintData)
    {
      $this->szPrintData = $szPrintData;
      return $this;
    }

    /**
     * @return int
     */
    public function getNResolutionMapToDPI()
    {
      return $this->nResolutionMapToDPI;
    }

    /**
     * @param int $nResolutionMapToDPI
     * @return \Axess\Dci4Wtp\getACPXImage
     */
    public function setNResolutionMapToDPI($nResolutionMapToDPI)
    {
      $this->nResolutionMapToDPI = $nResolutionMapToDPI;
      return $this;
    }

}
