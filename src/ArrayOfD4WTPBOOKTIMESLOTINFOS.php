<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPBOOKTIMESLOTINFOS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPBOOKTIMESLOTINFOS[] $D4WTPBOOKTIMESLOTINFOS
     */
    protected $D4WTPBOOKTIMESLOTINFOS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPBOOKTIMESLOTINFOS[]
     */
    public function getD4WTPBOOKTIMESLOTINFOS()
    {
      return $this->D4WTPBOOKTIMESLOTINFOS;
    }

    /**
     * @param D4WTPBOOKTIMESLOTINFOS[] $D4WTPBOOKTIMESLOTINFOS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPBOOKTIMESLOTINFOS
     */
    public function setD4WTPBOOKTIMESLOTINFOS(array $D4WTPBOOKTIMESLOTINFOS = null)
    {
      $this->D4WTPBOOKTIMESLOTINFOS = $D4WTPBOOKTIMESLOTINFOS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPBOOKTIMESLOTINFOS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPBOOKTIMESLOTINFOS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPBOOKTIMESLOTINFOS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPBOOKTIMESLOTINFOS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPBOOKTIMESLOTINFOS[] = $value;
      } else {
        $this->D4WTPBOOKTIMESLOTINFOS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPBOOKTIMESLOTINFOS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPBOOKTIMESLOTINFOS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPBOOKTIMESLOTINFOS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPBOOKTIMESLOTINFOS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPBOOKTIMESLOTINFOS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPBOOKTIMESLOTINFOS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPBOOKTIMESLOTINFOS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPBOOKTIMESLOTINFOS);
    }

}
