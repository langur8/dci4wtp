<?php

namespace Axess\Dci4Wtp;

class switchPrepaidTcktStatus2
{

    /**
     * @var D4WTPSWITCHPREPSTAT2REQUEST $i_ctSwitchPrepStatusReq
     */
    protected $i_ctSwitchPrepStatusReq = null;

    /**
     * @param D4WTPSWITCHPREPSTAT2REQUEST $i_ctSwitchPrepStatusReq
     */
    public function __construct($i_ctSwitchPrepStatusReq)
    {
      $this->i_ctSwitchPrepStatusReq = $i_ctSwitchPrepStatusReq;
    }

    /**
     * @return D4WTPSWITCHPREPSTAT2REQUEST
     */
    public function getI_ctSwitchPrepStatusReq()
    {
      return $this->i_ctSwitchPrepStatusReq;
    }

    /**
     * @param D4WTPSWITCHPREPSTAT2REQUEST $i_ctSwitchPrepStatusReq
     * @return \Axess\Dci4Wtp\switchPrepaidTcktStatus2
     */
    public function setI_ctSwitchPrepStatusReq($i_ctSwitchPrepStatusReq)
    {
      $this->i_ctSwitchPrepStatusReq = $i_ctSwitchPrepStatusReq;
      return $this;
    }

}
