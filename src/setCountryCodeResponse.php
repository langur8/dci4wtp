<?php

namespace Axess\Dci4Wtp;

class setCountryCodeResponse
{

    /**
     * @var D4WTPSETCOUNTRYCODERESULT $setCountryCodeResult
     */
    protected $setCountryCodeResult = null;

    /**
     * @param D4WTPSETCOUNTRYCODERESULT $setCountryCodeResult
     */
    public function __construct($setCountryCodeResult)
    {
      $this->setCountryCodeResult = $setCountryCodeResult;
    }

    /**
     * @return D4WTPSETCOUNTRYCODERESULT
     */
    public function getSetCountryCodeResult()
    {
      return $this->setCountryCodeResult;
    }

    /**
     * @param D4WTPSETCOUNTRYCODERESULT $setCountryCodeResult
     * @return \Axess\Dci4Wtp\setCountryCodeResponse
     */
    public function setSetCountryCodeResult($setCountryCodeResult)
    {
      $this->setCountryCodeResult = $setCountryCodeResult;
      return $this;
    }

}
