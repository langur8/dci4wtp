<?php

namespace Axess\Dci4Wtp;

class D4WTPNONCONSECUTIVEDAYSINFO
{

    /**
     * @var float $NDAYSALLOWED
     */
    protected $NDAYSALLOWED = null;

    /**
     * @var float $NDAYSALLOWEDPOOL2
     */
    protected $NDAYSALLOWEDPOOL2 = null;

    /**
     * @var float $NDAYSDURATION
     */
    protected $NDAYSDURATION = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNDAYSALLOWED()
    {
      return $this->NDAYSALLOWED;
    }

    /**
     * @param float $NDAYSALLOWED
     * @return \Axess\Dci4Wtp\D4WTPNONCONSECUTIVEDAYSINFO
     */
    public function setNDAYSALLOWED($NDAYSALLOWED)
    {
      $this->NDAYSALLOWED = $NDAYSALLOWED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDAYSALLOWEDPOOL2()
    {
      return $this->NDAYSALLOWEDPOOL2;
    }

    /**
     * @param float $NDAYSALLOWEDPOOL2
     * @return \Axess\Dci4Wtp\D4WTPNONCONSECUTIVEDAYSINFO
     */
    public function setNDAYSALLOWEDPOOL2($NDAYSALLOWEDPOOL2)
    {
      $this->NDAYSALLOWEDPOOL2 = $NDAYSALLOWEDPOOL2;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDAYSDURATION()
    {
      return $this->NDAYSDURATION;
    }

    /**
     * @param float $NDAYSDURATION
     * @return \Axess\Dci4Wtp\D4WTPNONCONSECUTIVEDAYSINFO
     */
    public function setNDAYSDURATION($NDAYSDURATION)
    {
      $this->NDAYSDURATION = $NDAYSDURATION;
      return $this;
    }

}
