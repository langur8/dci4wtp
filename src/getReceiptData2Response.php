<?php

namespace Axess\Dci4Wtp;

class getReceiptData2Response
{

    /**
     * @var D4WTPRECEIPTDATARESULT2 $getReceiptData2Result
     */
    protected $getReceiptData2Result = null;

    /**
     * @param D4WTPRECEIPTDATARESULT2 $getReceiptData2Result
     */
    public function __construct($getReceiptData2Result)
    {
      $this->getReceiptData2Result = $getReceiptData2Result;
    }

    /**
     * @return D4WTPRECEIPTDATARESULT2
     */
    public function getGetReceiptData2Result()
    {
      return $this->getReceiptData2Result;
    }

    /**
     * @param D4WTPRECEIPTDATARESULT2 $getReceiptData2Result
     * @return \Axess\Dci4Wtp\getReceiptData2Response
     */
    public function setGetReceiptData2Result($getReceiptData2Result)
    {
      $this->getReceiptData2Result = $getReceiptData2Result;
      return $this;
    }

}
