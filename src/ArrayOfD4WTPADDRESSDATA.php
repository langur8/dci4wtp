<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPADDRESSDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPADDRESSDATA[] $D4WTPADDRESSDATA
     */
    protected $D4WTPADDRESSDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPADDRESSDATA[]
     */
    public function getD4WTPADDRESSDATA()
    {
      return $this->D4WTPADDRESSDATA;
    }

    /**
     * @param D4WTPADDRESSDATA[] $D4WTPADDRESSDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPADDRESSDATA
     */
    public function setD4WTPADDRESSDATA(array $D4WTPADDRESSDATA = null)
    {
      $this->D4WTPADDRESSDATA = $D4WTPADDRESSDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPADDRESSDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPADDRESSDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPADDRESSDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPADDRESSDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPADDRESSDATA[] = $value;
      } else {
        $this->D4WTPADDRESSDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPADDRESSDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPADDRESSDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPADDRESSDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPADDRESSDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPADDRESSDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPADDRESSDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPADDRESSDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPADDRESSDATA);
    }

}
