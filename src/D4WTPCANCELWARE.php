<?php

namespace Axess\Dci4Wtp;

class D4WTPCANCELWARE
{

    /**
     * @var float $NWAREBLOCKNR
     */
    protected $NWAREBLOCKNR = null;

    /**
     * @var float $NWAREITEMNR
     */
    protected $NWAREITEMNR = null;

    /**
     * @var float $NWAREPOSNR
     */
    protected $NWAREPOSNR = null;

    /**
     * @var float $NWAREPROJNR
     */
    protected $NWAREPROJNR = null;

    /**
     * @var float $NWARETARIF
     */
    protected $NWARETARIF = null;

    /**
     * @var float $NWARETRANSNR
     */
    protected $NWARETRANSNR = null;

    /**
     * @var string $SZWAREVALIDTO
     */
    protected $SZWAREVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNWAREBLOCKNR()
    {
      return $this->NWAREBLOCKNR;
    }

    /**
     * @param float $NWAREBLOCKNR
     * @return \Axess\Dci4Wtp\D4WTPCANCELWARE
     */
    public function setNWAREBLOCKNR($NWAREBLOCKNR)
    {
      $this->NWAREBLOCKNR = $NWAREBLOCKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWAREITEMNR()
    {
      return $this->NWAREITEMNR;
    }

    /**
     * @param float $NWAREITEMNR
     * @return \Axess\Dci4Wtp\D4WTPCANCELWARE
     */
    public function setNWAREITEMNR($NWAREITEMNR)
    {
      $this->NWAREITEMNR = $NWAREITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWAREPOSNR()
    {
      return $this->NWAREPOSNR;
    }

    /**
     * @param float $NWAREPOSNR
     * @return \Axess\Dci4Wtp\D4WTPCANCELWARE
     */
    public function setNWAREPOSNR($NWAREPOSNR)
    {
      $this->NWAREPOSNR = $NWAREPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWAREPROJNR()
    {
      return $this->NWAREPROJNR;
    }

    /**
     * @param float $NWAREPROJNR
     * @return \Axess\Dci4Wtp\D4WTPCANCELWARE
     */
    public function setNWAREPROJNR($NWAREPROJNR)
    {
      $this->NWAREPROJNR = $NWAREPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARETARIF()
    {
      return $this->NWARETARIF;
    }

    /**
     * @param float $NWARETARIF
     * @return \Axess\Dci4Wtp\D4WTPCANCELWARE
     */
    public function setNWARETARIF($NWARETARIF)
    {
      $this->NWARETARIF = $NWARETARIF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARETRANSNR()
    {
      return $this->NWARETRANSNR;
    }

    /**
     * @param float $NWARETRANSNR
     * @return \Axess\Dci4Wtp\D4WTPCANCELWARE
     */
    public function setNWARETRANSNR($NWARETRANSNR)
    {
      $this->NWARETRANSNR = $NWARETRANSNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWAREVALIDTO()
    {
      return $this->SZWAREVALIDTO;
    }

    /**
     * @param string $SZWAREVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPCANCELWARE
     */
    public function setSZWAREVALIDTO($SZWAREVALIDTO)
    {
      $this->SZWAREVALIDTO = $SZWAREVALIDTO;
      return $this;
    }

}
