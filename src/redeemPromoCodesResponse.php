<?php

namespace Axess\Dci4Wtp;

class redeemPromoCodesResponse
{

    /**
     * @var D4WTPREDEEMPROMOCODESRESULT $redeemPromoCodesResult
     */
    protected $redeemPromoCodesResult = null;

    /**
     * @param D4WTPREDEEMPROMOCODESRESULT $redeemPromoCodesResult
     */
    public function __construct($redeemPromoCodesResult)
    {
      $this->redeemPromoCodesResult = $redeemPromoCodesResult;
    }

    /**
     * @return D4WTPREDEEMPROMOCODESRESULT
     */
    public function getRedeemPromoCodesResult()
    {
      return $this->redeemPromoCodesResult;
    }

    /**
     * @param D4WTPREDEEMPROMOCODESRESULT $redeemPromoCodesResult
     * @return \Axess\Dci4Wtp\redeemPromoCodesResponse
     */
    public function setRedeemPromoCodesResult($redeemPromoCodesResult)
    {
      $this->redeemPromoCodesResult = $redeemPromoCodesResult;
      return $this;
    }

}
