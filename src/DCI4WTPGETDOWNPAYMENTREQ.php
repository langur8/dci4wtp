<?php

namespace Axess\Dci4Wtp;

class DCI4WTPGETDOWNPAYMENTREQ
{

    /**
     * @var float $FSUM
     */
    protected $FSUM = null;

    /**
     * @var float $NACCEPTFIRMENKASSANR
     */
    protected $NACCEPTFIRMENKASSANR = null;

    /**
     * @var float $NACCEPTFIRMENNR
     */
    protected $NACCEPTFIRMENNR = null;

    /**
     * @var float $NACCEPTFIRMENPROJNR
     */
    protected $NACCEPTFIRMENPROJNR = null;

    /**
     * @var float $NACCEPTNAMEDUSERID
     */
    protected $NACCEPTNAMEDUSERID = null;

    /**
     * @var float $NACCEPTSORTNT
     */
    protected $NACCEPTSORTNT = null;

    /**
     * @var float $NFIRMENKASSANR
     */
    protected $NFIRMENKASSANR = null;

    /**
     * @var float $NFIRMENNR
     */
    protected $NFIRMENNR = null;

    /**
     * @var float $NFIRMENPROJNR
     */
    protected $NFIRMENPROJNR = null;

    /**
     * @var float $NLFDNR
     */
    protected $NLFDNR = null;

    /**
     * @var float $NREISEGRUPPENKASSANR
     */
    protected $NREISEGRUPPENKASSANR = null;

    /**
     * @var float $NREISEGRUPPENPROJNR
     */
    protected $NREISEGRUPPENPROJNR = null;

    /**
     * @var float $NREISEGRUPPENR
     */
    protected $NREISEGRUPPENR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZCOMMENT
     */
    protected $SZCOMMENT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFSUM()
    {
      return $this->FSUM;
    }

    /**
     * @param float $FSUM
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setFSUM($FSUM)
    {
      $this->FSUM = $FSUM;
      return $this;
    }

    /**
     * @return float
     */
    public function getNACCEPTFIRMENKASSANR()
    {
      return $this->NACCEPTFIRMENKASSANR;
    }

    /**
     * @param float $NACCEPTFIRMENKASSANR
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setNACCEPTFIRMENKASSANR($NACCEPTFIRMENKASSANR)
    {
      $this->NACCEPTFIRMENKASSANR = $NACCEPTFIRMENKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNACCEPTFIRMENNR()
    {
      return $this->NACCEPTFIRMENNR;
    }

    /**
     * @param float $NACCEPTFIRMENNR
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setNACCEPTFIRMENNR($NACCEPTFIRMENNR)
    {
      $this->NACCEPTFIRMENNR = $NACCEPTFIRMENNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNACCEPTFIRMENPROJNR()
    {
      return $this->NACCEPTFIRMENPROJNR;
    }

    /**
     * @param float $NACCEPTFIRMENPROJNR
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setNACCEPTFIRMENPROJNR($NACCEPTFIRMENPROJNR)
    {
      $this->NACCEPTFIRMENPROJNR = $NACCEPTFIRMENPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNACCEPTNAMEDUSERID()
    {
      return $this->NACCEPTNAMEDUSERID;
    }

    /**
     * @param float $NACCEPTNAMEDUSERID
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setNACCEPTNAMEDUSERID($NACCEPTNAMEDUSERID)
    {
      $this->NACCEPTNAMEDUSERID = $NACCEPTNAMEDUSERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNACCEPTSORTNT()
    {
      return $this->NACCEPTSORTNT;
    }

    /**
     * @param float $NACCEPTSORTNT
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setNACCEPTSORTNT($NACCEPTSORTNT)
    {
      $this->NACCEPTSORTNT = $NACCEPTSORTNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRMENKASSANR()
    {
      return $this->NFIRMENKASSANR;
    }

    /**
     * @param float $NFIRMENKASSANR
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setNFIRMENKASSANR($NFIRMENKASSANR)
    {
      $this->NFIRMENKASSANR = $NFIRMENKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRMENNR()
    {
      return $this->NFIRMENNR;
    }

    /**
     * @param float $NFIRMENNR
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setNFIRMENNR($NFIRMENNR)
    {
      $this->NFIRMENNR = $NFIRMENNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRMENPROJNR()
    {
      return $this->NFIRMENPROJNR;
    }

    /**
     * @param float $NFIRMENPROJNR
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setNFIRMENPROJNR($NFIRMENPROJNR)
    {
      $this->NFIRMENPROJNR = $NFIRMENPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDNR()
    {
      return $this->NLFDNR;
    }

    /**
     * @param float $NLFDNR
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setNLFDNR($NLFDNR)
    {
      $this->NLFDNR = $NLFDNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNREISEGRUPPENKASSANR()
    {
      return $this->NREISEGRUPPENKASSANR;
    }

    /**
     * @param float $NREISEGRUPPENKASSANR
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setNREISEGRUPPENKASSANR($NREISEGRUPPENKASSANR)
    {
      $this->NREISEGRUPPENKASSANR = $NREISEGRUPPENKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNREISEGRUPPENPROJNR()
    {
      return $this->NREISEGRUPPENPROJNR;
    }

    /**
     * @param float $NREISEGRUPPENPROJNR
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setNREISEGRUPPENPROJNR($NREISEGRUPPENPROJNR)
    {
      $this->NREISEGRUPPENPROJNR = $NREISEGRUPPENPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNREISEGRUPPENR()
    {
      return $this->NREISEGRUPPENR;
    }

    /**
     * @param float $NREISEGRUPPENR
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setNREISEGRUPPENR($NREISEGRUPPENR)
    {
      $this->NREISEGRUPPENR = $NREISEGRUPPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMMENT()
    {
      return $this->SZCOMMENT;
    }

    /**
     * @param string $SZCOMMENT
     * @return \Axess\Dci4Wtp\DCI4WTPGETDOWNPAYMENTREQ
     */
    public function setSZCOMMENT($SZCOMMENT)
    {
      $this->SZCOMMENT = $SZCOMMENT;
      return $this;
    }

}
