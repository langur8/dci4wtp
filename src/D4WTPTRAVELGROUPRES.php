<?php

namespace Axess\Dci4Wtp;

class D4WTPTRAVELGROUPRES
{

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var float $NSHOPPINGCARTPOSNO
     */
    protected $NSHOPPINGCARTPOSNO = null;

    /**
     * @var float $NSHOPPINGCARTPROJNO
     */
    protected $NSHOPPINGCARTPROJNO = null;

    /**
     * @var float $NSHOPPINGCARTSTATUSNO
     */
    protected $NSHOPPINGCARTSTATUSNO = null;

    /**
     * @var float $NSHOPPINGCARTTICKETCOUNT
     */
    protected $NSHOPPINGCARTTICKETCOUNT = null;

    /**
     * @var float $NTRAVELGROUPNO
     */
    protected $NTRAVELGROUPNO = null;

    /**
     * @var float $NTRAVELGROUPPOSNO
     */
    protected $NTRAVELGROUPPOSNO = null;

    /**
     * @var float $NTRAVELGROUPPROJNO
     */
    protected $NTRAVELGROUPPROJNO = null;

    /**
     * @var string $SZCOMPANYEMAIL
     */
    protected $SZCOMPANYEMAIL = null;

    /**
     * @var string $SZCOMPANYNAME
     */
    protected $SZCOMPANYNAME = null;

    /**
     * @var string $SZCOMPANYPHONENO
     */
    protected $SZCOMPANYPHONENO = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZTRAVELGROUPNAME
     */
    protected $SZTRAVELGROUPNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPOSNO()
    {
      return $this->NSHOPPINGCARTPOSNO;
    }

    /**
     * @param float $NSHOPPINGCARTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES
     */
    public function setNSHOPPINGCARTPOSNO($NSHOPPINGCARTPOSNO)
    {
      $this->NSHOPPINGCARTPOSNO = $NSHOPPINGCARTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPROJNO()
    {
      return $this->NSHOPPINGCARTPROJNO;
    }

    /**
     * @param float $NSHOPPINGCARTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES
     */
    public function setNSHOPPINGCARTPROJNO($NSHOPPINGCARTPROJNO)
    {
      $this->NSHOPPINGCARTPROJNO = $NSHOPPINGCARTPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTSTATUSNO()
    {
      return $this->NSHOPPINGCARTSTATUSNO;
    }

    /**
     * @param float $NSHOPPINGCARTSTATUSNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES
     */
    public function setNSHOPPINGCARTSTATUSNO($NSHOPPINGCARTSTATUSNO)
    {
      $this->NSHOPPINGCARTSTATUSNO = $NSHOPPINGCARTSTATUSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTTICKETCOUNT()
    {
      return $this->NSHOPPINGCARTTICKETCOUNT;
    }

    /**
     * @param float $NSHOPPINGCARTTICKETCOUNT
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES
     */
    public function setNSHOPPINGCARTTICKETCOUNT($NSHOPPINGCARTTICKETCOUNT)
    {
      $this->NSHOPPINGCARTTICKETCOUNT = $NSHOPPINGCARTTICKETCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPNO()
    {
      return $this->NTRAVELGROUPNO;
    }

    /**
     * @param float $NTRAVELGROUPNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES
     */
    public function setNTRAVELGROUPNO($NTRAVELGROUPNO)
    {
      $this->NTRAVELGROUPNO = $NTRAVELGROUPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPOSNO()
    {
      return $this->NTRAVELGROUPPOSNO;
    }

    /**
     * @param float $NTRAVELGROUPPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES
     */
    public function setNTRAVELGROUPPOSNO($NTRAVELGROUPPOSNO)
    {
      $this->NTRAVELGROUPPOSNO = $NTRAVELGROUPPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPROJNO()
    {
      return $this->NTRAVELGROUPPROJNO;
    }

    /**
     * @param float $NTRAVELGROUPPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES
     */
    public function setNTRAVELGROUPPROJNO($NTRAVELGROUPPROJNO)
    {
      $this->NTRAVELGROUPPROJNO = $NTRAVELGROUPPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYEMAIL()
    {
      return $this->SZCOMPANYEMAIL;
    }

    /**
     * @param string $SZCOMPANYEMAIL
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES
     */
    public function setSZCOMPANYEMAIL($SZCOMPANYEMAIL)
    {
      $this->SZCOMPANYEMAIL = $SZCOMPANYEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYNAME()
    {
      return $this->SZCOMPANYNAME;
    }

    /**
     * @param string $SZCOMPANYNAME
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES
     */
    public function setSZCOMPANYNAME($SZCOMPANYNAME)
    {
      $this->SZCOMPANYNAME = $SZCOMPANYNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYPHONENO()
    {
      return $this->SZCOMPANYPHONENO;
    }

    /**
     * @param string $SZCOMPANYPHONENO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES
     */
    public function setSZCOMPANYPHONENO($SZCOMPANYPHONENO)
    {
      $this->SZCOMPANYPHONENO = $SZCOMPANYPHONENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTRAVELGROUPNAME()
    {
      return $this->SZTRAVELGROUPNAME;
    }

    /**
     * @param string $SZTRAVELGROUPNAME
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPRES
     */
    public function setSZTRAVELGROUPNAME($SZTRAVELGROUPNAME)
    {
      $this->SZTRAVELGROUPNAME = $SZTRAVELGROUPNAME;
      return $this;
    }

}
