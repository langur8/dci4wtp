<?php

namespace Axess\Dci4Wtp;

class D4WTPCUSTOMERPROFILEREQUEST
{

    /**
     * @var float $NCUSTOMERNO
     */
    protected $NCUSTOMERNO = null;

    /**
     * @var float $NCUSTOMERPOSNO
     */
    protected $NCUSTOMERPOSNO = null;

    /**
     * @var float $NCUSTOMERPROJNO
     */
    protected $NCUSTOMERPROJNO = null;

    /**
     * @var float $NEMPLOYEENO
     */
    protected $NEMPLOYEENO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCUSTOMERNO()
    {
      return $this->NCUSTOMERNO;
    }

    /**
     * @param float $NCUSTOMERNO
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILEREQUEST
     */
    public function setNCUSTOMERNO($NCUSTOMERNO)
    {
      $this->NCUSTOMERNO = $NCUSTOMERNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTOMERPOSNO()
    {
      return $this->NCUSTOMERPOSNO;
    }

    /**
     * @param float $NCUSTOMERPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILEREQUEST
     */
    public function setNCUSTOMERPOSNO($NCUSTOMERPOSNO)
    {
      $this->NCUSTOMERPOSNO = $NCUSTOMERPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTOMERPROJNO()
    {
      return $this->NCUSTOMERPROJNO;
    }

    /**
     * @param float $NCUSTOMERPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILEREQUEST
     */
    public function setNCUSTOMERPROJNO($NCUSTOMERPROJNO)
    {
      $this->NCUSTOMERPROJNO = $NCUSTOMERPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEMPLOYEENO()
    {
      return $this->NEMPLOYEENO;
    }

    /**
     * @param float $NEMPLOYEENO
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILEREQUEST
     */
    public function setNEMPLOYEENO($NEMPLOYEENO)
    {
      $this->NEMPLOYEENO = $NEMPLOYEENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILEREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
