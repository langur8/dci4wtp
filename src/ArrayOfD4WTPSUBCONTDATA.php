<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSUBCONTDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSUBCONTDATA[] $D4WTPSUBCONTDATA
     */
    protected $D4WTPSUBCONTDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSUBCONTDATA[]
     */
    public function getD4WTPSUBCONTDATA()
    {
      return $this->D4WTPSUBCONTDATA;
    }

    /**
     * @param D4WTPSUBCONTDATA[] $D4WTPSUBCONTDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSUBCONTDATA
     */
    public function setD4WTPSUBCONTDATA(array $D4WTPSUBCONTDATA = null)
    {
      $this->D4WTPSUBCONTDATA = $D4WTPSUBCONTDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSUBCONTDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSUBCONTDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSUBCONTDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSUBCONTDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSUBCONTDATA[] = $value;
      } else {
        $this->D4WTPSUBCONTDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSUBCONTDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSUBCONTDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSUBCONTDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSUBCONTDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSUBCONTDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSUBCONTDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSUBCONTDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSUBCONTDATA);
    }

}
