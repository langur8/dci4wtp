<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCLIENTSETUPREQUEST
{

    /**
     * @var float $BEMPMANDATORY
     */
    protected $BEMPMANDATORY = null;

    /**
     * @var float $NCASHIERID
     */
    protected $NCASHIERID = null;

    /**
     * @var float $NCUSTNO
     */
    protected $NCUSTNO = null;

    /**
     * @var float $NCUSTPOSNO
     */
    protected $NCUSTPOSNO = null;

    /**
     * @var float $NCUSTPROJNO
     */
    protected $NCUSTPROJNO = null;

    /**
     * @var float $NEMPLOYEENO
     */
    protected $NEMPLOYEENO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBEMPMANDATORY()
    {
      return $this->BEMPMANDATORY;
    }

    /**
     * @param float $BEMPMANDATORY
     * @return \Axess\Dci4Wtp\D4WTPGETCLIENTSETUPREQUEST
     */
    public function setBEMPMANDATORY($BEMPMANDATORY)
    {
      $this->BEMPMANDATORY = $BEMPMANDATORY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCASHIERID()
    {
      return $this->NCASHIERID;
    }

    /**
     * @param float $NCASHIERID
     * @return \Axess\Dci4Wtp\D4WTPGETCLIENTSETUPREQUEST
     */
    public function setNCASHIERID($NCASHIERID)
    {
      $this->NCASHIERID = $NCASHIERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTNO()
    {
      return $this->NCUSTNO;
    }

    /**
     * @param float $NCUSTNO
     * @return \Axess\Dci4Wtp\D4WTPGETCLIENTSETUPREQUEST
     */
    public function setNCUSTNO($NCUSTNO)
    {
      $this->NCUSTNO = $NCUSTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPOSNO()
    {
      return $this->NCUSTPOSNO;
    }

    /**
     * @param float $NCUSTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETCLIENTSETUPREQUEST
     */
    public function setNCUSTPOSNO($NCUSTPOSNO)
    {
      $this->NCUSTPOSNO = $NCUSTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPROJNO()
    {
      return $this->NCUSTPROJNO;
    }

    /**
     * @param float $NCUSTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETCLIENTSETUPREQUEST
     */
    public function setNCUSTPROJNO($NCUSTPROJNO)
    {
      $this->NCUSTPROJNO = $NCUSTPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEMPLOYEENO()
    {
      return $this->NEMPLOYEENO;
    }

    /**
     * @param float $NEMPLOYEENO
     * @return \Axess\Dci4Wtp\D4WTPGETCLIENTSETUPREQUEST
     */
    public function setNEMPLOYEENO($NEMPLOYEENO)
    {
      $this->NEMPLOYEENO = $NEMPLOYEENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETCLIENTSETUPREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
