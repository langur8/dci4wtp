<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSETUPITEM implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSETUPITEM[] $D4WTPSETUPITEM
     */
    protected $D4WTPSETUPITEM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSETUPITEM[]
     */
    public function getD4WTPSETUPITEM()
    {
      return $this->D4WTPSETUPITEM;
    }

    /**
     * @param D4WTPSETUPITEM[] $D4WTPSETUPITEM
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSETUPITEM
     */
    public function setD4WTPSETUPITEM(array $D4WTPSETUPITEM = null)
    {
      $this->D4WTPSETUPITEM = $D4WTPSETUPITEM;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSETUPITEM[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSETUPITEM
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSETUPITEM[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSETUPITEM $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSETUPITEM[] = $value;
      } else {
        $this->D4WTPSETUPITEM[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSETUPITEM[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSETUPITEM Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSETUPITEM);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSETUPITEM);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSETUPITEM);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSETUPITEM);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSETUPITEM Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSETUPITEM);
    }

}
