<?php

namespace Axess\Dci4Wtp;

class getTravelGrpShopBasPositionsResponse
{

    /**
     * @var D4WTPGETTRAVELGRPSHOPPOSRES $getTravelGrpShopBasPositionsResult
     */
    protected $getTravelGrpShopBasPositionsResult = null;

    /**
     * @param D4WTPGETTRAVELGRPSHOPPOSRES $getTravelGrpShopBasPositionsResult
     */
    public function __construct($getTravelGrpShopBasPositionsResult)
    {
      $this->getTravelGrpShopBasPositionsResult = $getTravelGrpShopBasPositionsResult;
    }

    /**
     * @return D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function getGetTravelGrpShopBasPositionsResult()
    {
      return $this->getTravelGrpShopBasPositionsResult;
    }

    /**
     * @param D4WTPGETTRAVELGRPSHOPPOSRES $getTravelGrpShopBasPositionsResult
     * @return \Axess\Dci4Wtp\getTravelGrpShopBasPositionsResponse
     */
    public function setGetTravelGrpShopBasPositionsResult($getTravelGrpShopBasPositionsResult)
    {
      $this->getTravelGrpShopBasPositionsResult = $getTravelGrpShopBasPositionsResult;
      return $this;
    }

}
