<?php

namespace Axess\Dci4Wtp;

class OBI4PSPUIDANFRAGE
{

    /**
     * @var string $SZUIDSTANDARD
     */
    protected $SZUIDSTANDARD = null;

    /**
     * @var string $SZUIDWERT
     */
    protected $SZUIDWERT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZUIDSTANDARD()
    {
      return $this->SZUIDSTANDARD;
    }

    /**
     * @param string $SZUIDSTANDARD
     * @return \Axess\Dci4Wtp\OBI4PSPUIDANFRAGE
     */
    public function setSZUIDSTANDARD($SZUIDSTANDARD)
    {
      $this->SZUIDSTANDARD = $SZUIDSTANDARD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUIDWERT()
    {
      return $this->SZUIDWERT;
    }

    /**
     * @param string $SZUIDWERT
     * @return \Axess\Dci4Wtp\OBI4PSPUIDANFRAGE
     */
    public function setSZUIDWERT($SZUIDWERT)
    {
      $this->SZUIDWERT = $SZUIDWERT;
      return $this;
    }

}
