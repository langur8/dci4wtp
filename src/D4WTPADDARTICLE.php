<?php

namespace Axess\Dci4Wtp;

class D4WTPADDARTICLE
{

    /**
     * @var float $BMANDATORY
     */
    protected $BMANDATORY = null;

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NPACKAGENO
     */
    protected $NPACKAGENO = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NSORTID
     */
    protected $NSORTID = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBMANDATORY()
    {
      return $this->BMANDATORY;
    }

    /**
     * @param float $BMANDATORY
     * @return \Axess\Dci4Wtp\D4WTPADDARTICLE
     */
    public function setBMANDATORY($BMANDATORY)
    {
      $this->BMANDATORY = $BMANDATORY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPADDARTICLE
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKAGENO()
    {
      return $this->NPACKAGENO;
    }

    /**
     * @param float $NPACKAGENO
     * @return \Axess\Dci4Wtp\D4WTPADDARTICLE
     */
    public function setNPACKAGENO($NPACKAGENO)
    {
      $this->NPACKAGENO = $NPACKAGENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPADDARTICLE
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTID()
    {
      return $this->NSORTID;
    }

    /**
     * @param float $NSORTID
     * @return \Axess\Dci4Wtp\D4WTPADDARTICLE
     */
    public function setNSORTID($NSORTID)
    {
      $this->NSORTID = $NSORTID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPADDARTICLE
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPADDARTICLE
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
