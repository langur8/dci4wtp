<?php

namespace Axess\Dci4Wtp;

class D4WTPOCCUPACYRESERVATIONS2
{

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var float $NSHOPPINGCARTPOSNO
     */
    protected $NSHOPPINGCARTPOSNO = null;

    /**
     * @var float $NSHOPPINGCARTPROJNO
     */
    protected $NSHOPPINGCARTPROJNO = null;

    /**
     * @var float $NSHOPPINGCARTSTATUS
     */
    protected $NSHOPPINGCARTSTATUS = null;

    /**
     * @var float $NTRAVELGROUPNO
     */
    protected $NTRAVELGROUPNO = null;

    /**
     * @var float $NTRAVELGROUPPOSNO
     */
    protected $NTRAVELGROUPPOSNO = null;

    /**
     * @var float $NTRAVELGROUPPROJNO
     */
    protected $NTRAVELGROUPPROJNO = null;

    /**
     * @var string $SZCOMPANYEMAIL
     */
    protected $SZCOMPANYEMAIL = null;

    /**
     * @var string $SZCOMPANYNAME
     */
    protected $SZCOMPANYNAME = null;

    /**
     * @var string $SZCOMPANYPHONE
     */
    protected $SZCOMPANYPHONE = null;

    /**
     * @var string $SZCONTACTPERSONEMAIL
     */
    protected $SZCONTACTPERSONEMAIL = null;

    /**
     * @var string $SZCONTACTPERSONPHONE
     */
    protected $SZCONTACTPERSONPHONE = null;

    /**
     * @var string $SZNOTE
     */
    protected $SZNOTE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPOSNO()
    {
      return $this->NSHOPPINGCARTPOSNO;
    }

    /**
     * @param float $NSHOPPINGCARTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setNSHOPPINGCARTPOSNO($NSHOPPINGCARTPOSNO)
    {
      $this->NSHOPPINGCARTPOSNO = $NSHOPPINGCARTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPROJNO()
    {
      return $this->NSHOPPINGCARTPROJNO;
    }

    /**
     * @param float $NSHOPPINGCARTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setNSHOPPINGCARTPROJNO($NSHOPPINGCARTPROJNO)
    {
      $this->NSHOPPINGCARTPROJNO = $NSHOPPINGCARTPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTSTATUS()
    {
      return $this->NSHOPPINGCARTSTATUS;
    }

    /**
     * @param float $NSHOPPINGCARTSTATUS
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setNSHOPPINGCARTSTATUS($NSHOPPINGCARTSTATUS)
    {
      $this->NSHOPPINGCARTSTATUS = $NSHOPPINGCARTSTATUS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPNO()
    {
      return $this->NTRAVELGROUPNO;
    }

    /**
     * @param float $NTRAVELGROUPNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setNTRAVELGROUPNO($NTRAVELGROUPNO)
    {
      $this->NTRAVELGROUPNO = $NTRAVELGROUPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPOSNO()
    {
      return $this->NTRAVELGROUPPOSNO;
    }

    /**
     * @param float $NTRAVELGROUPPOSNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setNTRAVELGROUPPOSNO($NTRAVELGROUPPOSNO)
    {
      $this->NTRAVELGROUPPOSNO = $NTRAVELGROUPPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPROJNO()
    {
      return $this->NTRAVELGROUPPROJNO;
    }

    /**
     * @param float $NTRAVELGROUPPROJNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setNTRAVELGROUPPROJNO($NTRAVELGROUPPROJNO)
    {
      $this->NTRAVELGROUPPROJNO = $NTRAVELGROUPPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYEMAIL()
    {
      return $this->SZCOMPANYEMAIL;
    }

    /**
     * @param string $SZCOMPANYEMAIL
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setSZCOMPANYEMAIL($SZCOMPANYEMAIL)
    {
      $this->SZCOMPANYEMAIL = $SZCOMPANYEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYNAME()
    {
      return $this->SZCOMPANYNAME;
    }

    /**
     * @param string $SZCOMPANYNAME
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setSZCOMPANYNAME($SZCOMPANYNAME)
    {
      $this->SZCOMPANYNAME = $SZCOMPANYNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYPHONE()
    {
      return $this->SZCOMPANYPHONE;
    }

    /**
     * @param string $SZCOMPANYPHONE
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setSZCOMPANYPHONE($SZCOMPANYPHONE)
    {
      $this->SZCOMPANYPHONE = $SZCOMPANYPHONE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCONTACTPERSONEMAIL()
    {
      return $this->SZCONTACTPERSONEMAIL;
    }

    /**
     * @param string $SZCONTACTPERSONEMAIL
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setSZCONTACTPERSONEMAIL($SZCONTACTPERSONEMAIL)
    {
      $this->SZCONTACTPERSONEMAIL = $SZCONTACTPERSONEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCONTACTPERSONPHONE()
    {
      return $this->SZCONTACTPERSONPHONE;
    }

    /**
     * @param string $SZCONTACTPERSONPHONE
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setSZCONTACTPERSONPHONE($SZCONTACTPERSONPHONE)
    {
      $this->SZCONTACTPERSONPHONE = $SZCONTACTPERSONPHONE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNOTE()
    {
      return $this->SZNOTE;
    }

    /**
     * @param string $SZNOTE
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYRESERVATIONS2
     */
    public function setSZNOTE($SZNOTE)
    {
      $this->SZNOTE = $SZNOTE;
      return $this;
    }

}
