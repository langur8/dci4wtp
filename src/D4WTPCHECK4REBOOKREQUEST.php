<?php

namespace Axess\Dci4Wtp;

class D4WTPCHECK4REBOOKREQUEST
{

    /**
     * @var ArrayOfD4WTPTICKETSALE $ACTTICKETSALE
     */
    protected $ACTTICKETSALE = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKETSALE
     */
    public function getACTTICKETSALE()
    {
      return $this->ACTTICKETSALE;
    }

    /**
     * @param ArrayOfD4WTPTICKETSALE $ACTTICKETSALE
     * @return \Axess\Dci4Wtp\D4WTPCHECK4REBOOKREQUEST
     */
    public function setACTTICKETSALE($ACTTICKETSALE)
    {
      $this->ACTTICKETSALE = $ACTTICKETSALE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCHECK4REBOOKREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
