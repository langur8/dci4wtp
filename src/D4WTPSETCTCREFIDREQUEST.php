<?php

namespace Axess\Dci4Wtp;

class D4WTPSETCTCREFIDREQUEST
{

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NCTCARDTYPENO
     */
    protected $NCTCARDTYPENO = null;

    /**
     * @var float $NMEMBERSHIPTYPENO
     */
    protected $NMEMBERSHIPTYPENO = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZCARDOWNER
     */
    protected $SZCARDOWNER = null;

    /**
     * @var string $SZCTCARDREFID
     */
    protected $SZCTCARDREFID = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZEXPIRYDATE
     */
    protected $SZEXPIRYDATE = null;

    /**
     * @var string $SZMASKEDCTCARDNO
     */
    protected $SZMASKEDCTCARDNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCTCARDTYPENO()
    {
      return $this->NCTCARDTYPENO;
    }

    /**
     * @param float $NCTCARDTYPENO
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setNCTCARDTYPENO($NCTCARDTYPENO)
    {
      $this->NCTCARDTYPENO = $NCTCARDTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMEMBERSHIPTYPENO()
    {
      return $this->NMEMBERSHIPTYPENO;
    }

    /**
     * @param float $NMEMBERSHIPTYPENO
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setNMEMBERSHIPTYPENO($NMEMBERSHIPTYPENO)
    {
      $this->NMEMBERSHIPTYPENO = $NMEMBERSHIPTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDOWNER()
    {
      return $this->SZCARDOWNER;
    }

    /**
     * @param string $SZCARDOWNER
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setSZCARDOWNER($SZCARDOWNER)
    {
      $this->SZCARDOWNER = $SZCARDOWNER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCTCARDREFID()
    {
      return $this->SZCTCARDREFID;
    }

    /**
     * @param string $SZCTCARDREFID
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setSZCTCARDREFID($SZCTCARDREFID)
    {
      $this->SZCTCARDREFID = $SZCTCARDREFID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXPIRYDATE()
    {
      return $this->SZEXPIRYDATE;
    }

    /**
     * @param string $SZEXPIRYDATE
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setSZEXPIRYDATE($SZEXPIRYDATE)
    {
      $this->SZEXPIRYDATE = $SZEXPIRYDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASKEDCTCARDNO()
    {
      return $this->SZMASKEDCTCARDNO;
    }

    /**
     * @param string $SZMASKEDCTCARDNO
     * @return \Axess\Dci4Wtp\D4WTPSETCTCREFIDREQUEST
     */
    public function setSZMASKEDCTCARDNO($SZMASKEDCTCARDNO)
    {
      $this->SZMASKEDCTCARDNO = $SZMASKEDCTCARDNO;
      return $this;
    }

}
