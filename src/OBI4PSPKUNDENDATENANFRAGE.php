<?php

namespace Axess\Dci4Wtp;

class OBI4PSPKUNDENDATENANFRAGE
{

    /**
     * @var string $SZKUNDENID
     */
    protected $SZKUNDENID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZKUNDENID()
    {
      return $this->SZKUNDENID;
    }

    /**
     * @param string $SZKUNDENID
     * @return \Axess\Dci4Wtp\OBI4PSPKUNDENDATENANFRAGE
     */
    public function setSZKUNDENID($SZKUNDENID)
    {
      $this->SZKUNDENID = $SZKUNDENID;
      return $this;
    }

}
