<?php

namespace Axess\Dci4Wtp;

class getWTPGroups2Response
{

    /**
     * @var D4WTPGROUPS2RES $getWTPGroups2Result
     */
    protected $getWTPGroups2Result = null;

    /**
     * @param D4WTPGROUPS2RES $getWTPGroups2Result
     */
    public function __construct($getWTPGroups2Result)
    {
      $this->getWTPGroups2Result = $getWTPGroups2Result;
    }

    /**
     * @return D4WTPGROUPS2RES
     */
    public function getGetWTPGroups2Result()
    {
      return $this->getWTPGroups2Result;
    }

    /**
     * @param D4WTPGROUPS2RES $getWTPGroups2Result
     * @return \Axess\Dci4Wtp\getWTPGroups2Response
     */
    public function setGetWTPGroups2Result($getWTPGroups2Result)
    {
      $this->getWTPGroups2Result = $getWTPGroups2Result;
      return $this;
    }

}
