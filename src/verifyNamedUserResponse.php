<?php

namespace Axess\Dci4Wtp;

class verifyNamedUserResponse
{

    /**
     * @var D4WTPRESULT $verifyNamedUserResult
     */
    protected $verifyNamedUserResult = null;

    /**
     * @param D4WTPRESULT $verifyNamedUserResult
     */
    public function __construct($verifyNamedUserResult)
    {
      $this->verifyNamedUserResult = $verifyNamedUserResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getVerifyNamedUserResult()
    {
      return $this->verifyNamedUserResult;
    }

    /**
     * @param D4WTPRESULT $verifyNamedUserResult
     * @return \Axess\Dci4Wtp\verifyNamedUserResponse
     */
    public function setVerifyNamedUserResult($verifyNamedUserResult)
    {
      $this->verifyNamedUserResult = $verifyNamedUserResult;
      return $this;
    }

}
