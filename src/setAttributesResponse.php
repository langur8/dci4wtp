<?php

namespace Axess\Dci4Wtp;

class setAttributesResponse
{

    /**
     * @var D4WTPTRANSATTRIBUTESRES $setAttributesResult
     */
    protected $setAttributesResult = null;

    /**
     * @param D4WTPTRANSATTRIBUTESRES $setAttributesResult
     */
    public function __construct($setAttributesResult)
    {
      $this->setAttributesResult = $setAttributesResult;
    }

    /**
     * @return D4WTPTRANSATTRIBUTESRES
     */
    public function getSetAttributesResult()
    {
      return $this->setAttributesResult;
    }

    /**
     * @param D4WTPTRANSATTRIBUTESRES $setAttributesResult
     * @return \Axess\Dci4Wtp\setAttributesResponse
     */
    public function setSetAttributesResult($setAttributesResult)
    {
      $this->setAttributesResult = $setAttributesResult;
      return $this;
    }

}
