<?php

namespace Axess\Dci4Wtp;

class D4WTPPOOL2RESULT
{

    /**
     * @var ArrayOfD4WTPPOOL2 $ACTPOOLS2
     */
    protected $ACTPOOLS2 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPOOL2
     */
    public function getACTPOOLS2()
    {
      return $this->ACTPOOLS2;
    }

    /**
     * @param ArrayOfD4WTPPOOL2 $ACTPOOLS2
     * @return \Axess\Dci4Wtp\D4WTPPOOL2RESULT
     */
    public function setACTPOOLS2($ACTPOOLS2)
    {
      $this->ACTPOOLS2 = $ACTPOOLS2;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPOOL2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPOOL2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
