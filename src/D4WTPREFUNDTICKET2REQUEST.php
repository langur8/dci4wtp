<?php

namespace Axess\Dci4Wtp;

class D4WTPREFUNDTICKET2REQUEST
{

    /**
     * @var ArrayOfD4WTPTICKET $ACTTICKETLIST
     */
    protected $ACTTICKETLIST = null;

    /**
     * @var float $BBLOCKTICKET
     */
    protected $BBLOCKTICKET = null;

    /**
     * @var float $BWITHOUTADDARTICLE
     */
    protected $BWITHOUTADDARTICLE = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPMODE
     */
    protected $NWTPMODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKET
     */
    public function getACTTICKETLIST()
    {
      return $this->ACTTICKETLIST;
    }

    /**
     * @param ArrayOfD4WTPTICKET $ACTTICKETLIST
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2REQUEST
     */
    public function setACTTICKETLIST($ACTTICKETLIST)
    {
      $this->ACTTICKETLIST = $ACTTICKETLIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getBBLOCKTICKET()
    {
      return $this->BBLOCKTICKET;
    }

    /**
     * @param float $BBLOCKTICKET
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2REQUEST
     */
    public function setBBLOCKTICKET($BBLOCKTICKET)
    {
      $this->BBLOCKTICKET = $BBLOCKTICKET;
      return $this;
    }

    /**
     * @return float
     */
    public function getBWITHOUTADDARTICLE()
    {
      return $this->BWITHOUTADDARTICLE;
    }

    /**
     * @param float $BWITHOUTADDARTICLE
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2REQUEST
     */
    public function setBWITHOUTADDARTICLE($BWITHOUTADDARTICLE)
    {
      $this->BWITHOUTADDARTICLE = $BWITHOUTADDARTICLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPMODE()
    {
      return $this->NWTPMODE;
    }

    /**
     * @param float $NWTPMODE
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2REQUEST
     */
    public function setNWTPMODE($NWTPMODE)
    {
      $this->NWTPMODE = $NWTPMODE;
      return $this;
    }

}
