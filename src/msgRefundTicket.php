<?php

namespace Axess\Dci4Wtp;

class msgRefundTicket
{

    /**
     * @var D4WTPMSGREFUNDTICKETREQUEST $i_ctMsgRefundReq
     */
    protected $i_ctMsgRefundReq = null;

    /**
     * @param D4WTPMSGREFUNDTICKETREQUEST $i_ctMsgRefundReq
     */
    public function __construct($i_ctMsgRefundReq)
    {
      $this->i_ctMsgRefundReq = $i_ctMsgRefundReq;
    }

    /**
     * @return D4WTPMSGREFUNDTICKETREQUEST
     */
    public function getI_ctMsgRefundReq()
    {
      return $this->i_ctMsgRefundReq;
    }

    /**
     * @param D4WTPMSGREFUNDTICKETREQUEST $i_ctMsgRefundReq
     * @return \Axess\Dci4Wtp\msgRefundTicket
     */
    public function setI_ctMsgRefundReq($i_ctMsgRefundReq)
    {
      $this->i_ctMsgRefundReq = $i_ctMsgRefundReq;
      return $this;
    }

}
