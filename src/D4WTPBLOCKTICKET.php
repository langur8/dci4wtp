<?php

namespace Axess\Dci4Wtp;

class D4WTPBLOCKTICKET
{

    /**
     * @var float $NBLOCKREASON
     */
    protected $NBLOCKREASON = null;

    /**
     * @var float $NBLOCKUNBLOCKPARAMETER
     */
    protected $NBLOCKUNBLOCKPARAMETER = null;

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSERIALNR
     */
    protected $NSERIALNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NUNICODENR
     */
    protected $NUNICODENR = null;

    /**
     * @var string $SZENDOFBLOCKINGDATE
     */
    protected $SZENDOFBLOCKINGDATE = null;

    /**
     * @var string $SZVALIDDATE
     */
    protected $SZVALIDDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNBLOCKREASON()
    {
      return $this->NBLOCKREASON;
    }

    /**
     * @param float $NBLOCKREASON
     * @return \Axess\Dci4Wtp\D4WTPBLOCKTICKET
     */
    public function setNBLOCKREASON($NBLOCKREASON)
    {
      $this->NBLOCKREASON = $NBLOCKREASON;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKUNBLOCKPARAMETER()
    {
      return $this->NBLOCKUNBLOCKPARAMETER;
    }

    /**
     * @param float $NBLOCKUNBLOCKPARAMETER
     * @return \Axess\Dci4Wtp\D4WTPBLOCKTICKET
     */
    public function setNBLOCKUNBLOCKPARAMETER($NBLOCKUNBLOCKPARAMETER)
    {
      $this->NBLOCKUNBLOCKPARAMETER = $NBLOCKUNBLOCKPARAMETER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKTICKET
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKTICKET
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKTICKET
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNR()
    {
      return $this->NSERIALNR;
    }

    /**
     * @param float $NSERIALNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKTICKET
     */
    public function setNSERIALNR($NSERIALNR)
    {
      $this->NSERIALNR = $NSERIALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPBLOCKTICKET
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENR()
    {
      return $this->NUNICODENR;
    }

    /**
     * @param float $NUNICODENR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKTICKET
     */
    public function setNUNICODENR($NUNICODENR)
    {
      $this->NUNICODENR = $NUNICODENR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZENDOFBLOCKINGDATE()
    {
      return $this->SZENDOFBLOCKINGDATE;
    }

    /**
     * @param string $SZENDOFBLOCKINGDATE
     * @return \Axess\Dci4Wtp\D4WTPBLOCKTICKET
     */
    public function setSZENDOFBLOCKINGDATE($SZENDOFBLOCKINGDATE)
    {
      $this->SZENDOFBLOCKINGDATE = $SZENDOFBLOCKINGDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDDATE()
    {
      return $this->SZVALIDDATE;
    }

    /**
     * @param string $SZVALIDDATE
     * @return \Axess\Dci4Wtp\D4WTPBLOCKTICKET
     */
    public function setSZVALIDDATE($SZVALIDDATE)
    {
      $this->SZVALIDDATE = $SZVALIDDATE;
      return $this;
    }

}
