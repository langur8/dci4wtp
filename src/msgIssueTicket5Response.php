<?php

namespace Axess\Dci4Wtp;

class msgIssueTicket5Response
{

    /**
     * @var D4WTPMSGISSUETICKETRESULT3 $msgIssueTicket5Result
     */
    protected $msgIssueTicket5Result = null;

    /**
     * @param D4WTPMSGISSUETICKETRESULT3 $msgIssueTicket5Result
     */
    public function __construct($msgIssueTicket5Result)
    {
      $this->msgIssueTicket5Result = $msgIssueTicket5Result;
    }

    /**
     * @return D4WTPMSGISSUETICKETRESULT3
     */
    public function getMsgIssueTicket5Result()
    {
      return $this->msgIssueTicket5Result;
    }

    /**
     * @param D4WTPMSGISSUETICKETRESULT3 $msgIssueTicket5Result
     * @return \Axess\Dci4Wtp\msgIssueTicket5Response
     */
    public function setMsgIssueTicket5Result($msgIssueTicket5Result)
    {
      $this->msgIssueTicket5Result = $msgIssueTicket5Result;
      return $this;
    }

}
