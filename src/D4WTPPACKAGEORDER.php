<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGEORDER
{

    /**
     * @var ArrayOfD4WTPADDARTICLE $ACTADDARTICLES
     */
    protected $ACTADDARTICLES = null;

    /**
     * @var D4WTPPACKAGEPOSPRODUCT $CTPACKAGEPRODUCT
     */
    protected $CTPACKAGEPRODUCT = null;

    /**
     * @var D4WTPERSONDATA $CTPERSON
     */
    protected $CTPERSON = null;

    /**
     * @var float $NAPPTRANSLOGID
     */
    protected $NAPPTRANSLOGID = null;

    /**
     * @var float $NDATACARRIERTYPENO
     */
    protected $NDATACARRIERTYPENO = null;

    /**
     * @var string $SZDCCONTENT
     */
    protected $SZDCCONTENT = null;

    /**
     * @var string $SZWTPNO
     */
    protected $SZWTPNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDARTICLE
     */
    public function getACTADDARTICLES()
    {
      return $this->ACTADDARTICLES;
    }

    /**
     * @param ArrayOfD4WTPADDARTICLE $ACTADDARTICLES
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEORDER
     */
    public function setACTADDARTICLES($ACTADDARTICLES)
    {
      $this->ACTADDARTICLES = $ACTADDARTICLES;
      return $this;
    }

    /**
     * @return D4WTPPACKAGEPOSPRODUCT
     */
    public function getCTPACKAGEPRODUCT()
    {
      return $this->CTPACKAGEPRODUCT;
    }

    /**
     * @param D4WTPPACKAGEPOSPRODUCT $CTPACKAGEPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEORDER
     */
    public function setCTPACKAGEPRODUCT($CTPACKAGEPRODUCT)
    {
      $this->CTPACKAGEPRODUCT = $CTPACKAGEPRODUCT;
      return $this;
    }

    /**
     * @return D4WTPERSONDATA
     */
    public function getCTPERSON()
    {
      return $this->CTPERSON;
    }

    /**
     * @param D4WTPERSONDATA $CTPERSON
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEORDER
     */
    public function setCTPERSON($CTPERSON)
    {
      $this->CTPERSON = $CTPERSON;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAPPTRANSLOGID()
    {
      return $this->NAPPTRANSLOGID;
    }

    /**
     * @param float $NAPPTRANSLOGID
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEORDER
     */
    public function setNAPPTRANSLOGID($NAPPTRANSLOGID)
    {
      $this->NAPPTRANSLOGID = $NAPPTRANSLOGID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATACARRIERTYPENO()
    {
      return $this->NDATACARRIERTYPENO;
    }

    /**
     * @param float $NDATACARRIERTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEORDER
     */
    public function setNDATACARRIERTYPENO($NDATACARRIERTYPENO)
    {
      $this->NDATACARRIERTYPENO = $NDATACARRIERTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDCCONTENT()
    {
      return $this->SZDCCONTENT;
    }

    /**
     * @param string $SZDCCONTENT
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEORDER
     */
    public function setSZDCCONTENT($SZDCCONTENT)
    {
      $this->SZDCCONTENT = $SZDCCONTENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNO()
    {
      return $this->SZWTPNO;
    }

    /**
     * @param string $SZWTPNO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEORDER
     */
    public function setSZWTPNO($SZWTPNO)
    {
      $this->SZWTPNO = $SZWTPNO;
      return $this;
    }

}
