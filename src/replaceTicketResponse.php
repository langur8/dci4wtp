<?php

namespace Axess\Dci4Wtp;

class replaceTicketResponse
{

    /**
     * @var D4WTPREPLACETICKETRES $replaceTicketResult
     */
    protected $replaceTicketResult = null;

    /**
     * @param D4WTPREPLACETICKETRES $replaceTicketResult
     */
    public function __construct($replaceTicketResult)
    {
      $this->replaceTicketResult = $replaceTicketResult;
    }

    /**
     * @return D4WTPREPLACETICKETRES
     */
    public function getReplaceTicketResult()
    {
      return $this->replaceTicketResult;
    }

    /**
     * @param D4WTPREPLACETICKETRES $replaceTicketResult
     * @return \Axess\Dci4Wtp\replaceTicketResponse
     */
    public function setReplaceTicketResult($replaceTicketResult)
    {
      $this->replaceTicketResult = $replaceTicketResult;
      return $this;
    }

}
