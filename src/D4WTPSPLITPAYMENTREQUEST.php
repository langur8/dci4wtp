<?php

namespace Axess\Dci4Wtp;

class D4WTPSPLITPAYMENTREQUEST
{

    /**
     * @var float $BUNDOSPLIT
     */
    protected $BUNDOSPLIT = null;

    /**
     * @var float $FAMOUNT
     */
    protected $FAMOUNT = null;

    /**
     * @var float $NCOREPAYMENTNR
     */
    protected $NCOREPAYMENTNR = null;

    /**
     * @var float $NPAYMENTNR
     */
    protected $NPAYMENTNR = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNR
     */
    protected $NTRANSNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBUNDOSPLIT()
    {
      return $this->BUNDOSPLIT;
    }

    /**
     * @param float $BUNDOSPLIT
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENTREQUEST
     */
    public function setBUNDOSPLIT($BUNDOSPLIT)
    {
      $this->BUNDOSPLIT = $BUNDOSPLIT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFAMOUNT()
    {
      return $this->FAMOUNT;
    }

    /**
     * @param float $FAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENTREQUEST
     */
    public function setFAMOUNT($FAMOUNT)
    {
      $this->FAMOUNT = $FAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOREPAYMENTNR()
    {
      return $this->NCOREPAYMENTNR;
    }

    /**
     * @param float $NCOREPAYMENTNR
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENTREQUEST
     */
    public function setNCOREPAYMENTNR($NCOREPAYMENTNR)
    {
      $this->NCOREPAYMENTNR = $NCOREPAYMENTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYMENTNR()
    {
      return $this->NPAYMENTNR;
    }

    /**
     * @param float $NPAYMENTNR
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENTREQUEST
     */
    public function setNPAYMENTNR($NPAYMENTNR)
    {
      $this->NPAYMENTNR = $NPAYMENTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENTREQUEST
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENTREQUEST
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENTREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNR()
    {
      return $this->NTRANSNR;
    }

    /**
     * @param float $NTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENTREQUEST
     */
    public function setNTRANSNR($NTRANSNR)
    {
      $this->NTRANSNR = $NTRANSNR;
      return $this;
    }

}
