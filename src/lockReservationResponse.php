<?php

namespace Axess\Dci4Wtp;

class lockReservationResponse
{

    /**
     * @var D4WTPRESULT $lockReservationResult
     */
    protected $lockReservationResult = null;

    /**
     * @param D4WTPRESULT $lockReservationResult
     */
    public function __construct($lockReservationResult)
    {
      $this->lockReservationResult = $lockReservationResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getLockReservationResult()
    {
      return $this->lockReservationResult;
    }

    /**
     * @param D4WTPRESULT $lockReservationResult
     * @return \Axess\Dci4Wtp\lockReservationResponse
     */
    public function setLockReservationResult($lockReservationResult)
    {
      $this->lockReservationResult = $lockReservationResult;
      return $this;
    }

}
