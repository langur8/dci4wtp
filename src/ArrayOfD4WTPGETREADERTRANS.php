<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPGETREADERTRANS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPGETREADERTRANS[] $D4WTPGETREADERTRANS
     */
    protected $D4WTPGETREADERTRANS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPGETREADERTRANS[]
     */
    public function getD4WTPGETREADERTRANS()
    {
      return $this->D4WTPGETREADERTRANS;
    }

    /**
     * @param D4WTPGETREADERTRANS[] $D4WTPGETREADERTRANS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPGETREADERTRANS
     */
    public function setD4WTPGETREADERTRANS(array $D4WTPGETREADERTRANS = null)
    {
      $this->D4WTPGETREADERTRANS = $D4WTPGETREADERTRANS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPGETREADERTRANS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPGETREADERTRANS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPGETREADERTRANS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPGETREADERTRANS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPGETREADERTRANS[] = $value;
      } else {
        $this->D4WTPGETREADERTRANS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPGETREADERTRANS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPGETREADERTRANS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPGETREADERTRANS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPGETREADERTRANS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPGETREADERTRANS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPGETREADERTRANS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPGETREADERTRANS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPGETREADERTRANS);
    }

}
