<?php

namespace Axess\Dci4Wtp;

class ArrayOfDTL4FAMCODINGMASTER implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DTL4FAMCODINGMASTER[] $DTL4FAMCODINGMASTER
     */
    protected $DTL4FAMCODINGMASTER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DTL4FAMCODINGMASTER[]
     */
    public function getDTL4FAMCODINGMASTER()
    {
      return $this->DTL4FAMCODINGMASTER;
    }

    /**
     * @param DTL4FAMCODINGMASTER[] $DTL4FAMCODINGMASTER
     * @return \Axess\Dci4Wtp\ArrayOfDTL4FAMCODINGMASTER
     */
    public function setDTL4FAMCODINGMASTER(array $DTL4FAMCODINGMASTER = null)
    {
      $this->DTL4FAMCODINGMASTER = $DTL4FAMCODINGMASTER;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DTL4FAMCODINGMASTER[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DTL4FAMCODINGMASTER
     */
    public function offsetGet($offset)
    {
      return $this->DTL4FAMCODINGMASTER[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DTL4FAMCODINGMASTER $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DTL4FAMCODINGMASTER[] = $value;
      } else {
        $this->DTL4FAMCODINGMASTER[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DTL4FAMCODINGMASTER[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DTL4FAMCODINGMASTER Return the current element
     */
    public function current()
    {
      return current($this->DTL4FAMCODINGMASTER);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DTL4FAMCODINGMASTER);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DTL4FAMCODINGMASTER);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DTL4FAMCODINGMASTER);
    }

    /**
     * Countable implementation
     *
     * @return DTL4FAMCODINGMASTER Return count of elements
     */
    public function count()
    {
      return count($this->DTL4FAMCODINGMASTER);
    }

}
