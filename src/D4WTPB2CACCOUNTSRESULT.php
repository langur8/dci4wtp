<?php

namespace Axess\Dci4Wtp;

class D4WTPB2CACCOUNTSRESULT
{

    /**
     * @var ArrayOfD4WTPB2CACCOUNT $ACTB2CACCOUNTS
     */
    protected $ACTB2CACCOUNTS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPB2CACCOUNT
     */
    public function getACTB2CACCOUNTS()
    {
      return $this->ACTB2CACCOUNTS;
    }

    /**
     * @param ArrayOfD4WTPB2CACCOUNT $ACTB2CACCOUNTS
     * @return \Axess\Dci4Wtp\D4WTPB2CACCOUNTSRESULT
     */
    public function setACTB2CACCOUNTS($ACTB2CACCOUNTS)
    {
      $this->ACTB2CACCOUNTS = $ACTB2CACCOUNTS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPB2CACCOUNTSRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPB2CACCOUNTSRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
