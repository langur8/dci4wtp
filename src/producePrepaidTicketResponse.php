<?php

namespace Axess\Dci4Wtp;

class producePrepaidTicketResponse
{

    /**
     * @var D4WTPRODUCEPREPAIDTICKETRES $producePrepaidTicketResult
     */
    protected $producePrepaidTicketResult = null;

    /**
     * @param D4WTPRODUCEPREPAIDTICKETRES $producePrepaidTicketResult
     */
    public function __construct($producePrepaidTicketResult)
    {
      $this->producePrepaidTicketResult = $producePrepaidTicketResult;
    }

    /**
     * @return D4WTPRODUCEPREPAIDTICKETRES
     */
    public function getProducePrepaidTicketResult()
    {
      return $this->producePrepaidTicketResult;
    }

    /**
     * @param D4WTPRODUCEPREPAIDTICKETRES $producePrepaidTicketResult
     * @return \Axess\Dci4Wtp\producePrepaidTicketResponse
     */
    public function setProducePrepaidTicketResult($producePrepaidTicketResult)
    {
      $this->producePrepaidTicketResult = $producePrepaidTicketResult;
      return $this;
    }

}
