<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPREPAIDARTICLESPRINT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPREPAIDARTICLESPRINT[] $D4WTPPREPAIDARTICLESPRINT
     */
    protected $D4WTPPREPAIDARTICLESPRINT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPREPAIDARTICLESPRINT[]
     */
    public function getD4WTPPREPAIDARTICLESPRINT()
    {
      return $this->D4WTPPREPAIDARTICLESPRINT;
    }

    /**
     * @param D4WTPPREPAIDARTICLESPRINT[] $D4WTPPREPAIDARTICLESPRINT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPREPAIDARTICLESPRINT
     */
    public function setD4WTPPREPAIDARTICLESPRINT(array $D4WTPPREPAIDARTICLESPRINT = null)
    {
      $this->D4WTPPREPAIDARTICLESPRINT = $D4WTPPREPAIDARTICLESPRINT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPREPAIDARTICLESPRINT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPREPAIDARTICLESPRINT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPREPAIDARTICLESPRINT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPREPAIDARTICLESPRINT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPREPAIDARTICLESPRINT[] = $value;
      } else {
        $this->D4WTPPREPAIDARTICLESPRINT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPREPAIDARTICLESPRINT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPREPAIDARTICLESPRINT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPREPAIDARTICLESPRINT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPREPAIDARTICLESPRINT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPREPAIDARTICLESPRINT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPREPAIDARTICLESPRINT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPREPAIDARTICLESPRINT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPREPAIDARTICLESPRINT);
    }

}
