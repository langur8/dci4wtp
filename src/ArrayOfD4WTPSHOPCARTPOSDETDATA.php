<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSHOPCARTPOSDETDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSHOPCARTPOSDETDATA[] $D4WTPSHOPCARTPOSDETDATA
     */
    protected $D4WTPSHOPCARTPOSDETDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSHOPCARTPOSDETDATA[]
     */
    public function getD4WTPSHOPCARTPOSDETDATA()
    {
      return $this->D4WTPSHOPCARTPOSDETDATA;
    }

    /**
     * @param D4WTPSHOPCARTPOSDETDATA[] $D4WTPSHOPCARTPOSDETDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSHOPCARTPOSDETDATA
     */
    public function setD4WTPSHOPCARTPOSDETDATA(array $D4WTPSHOPCARTPOSDETDATA = null)
    {
      $this->D4WTPSHOPCARTPOSDETDATA = $D4WTPSHOPCARTPOSDETDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSHOPCARTPOSDETDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSHOPCARTPOSDETDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSHOPCARTPOSDETDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSHOPCARTPOSDETDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSHOPCARTPOSDETDATA[] = $value;
      } else {
        $this->D4WTPSHOPCARTPOSDETDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSHOPCARTPOSDETDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSHOPCARTPOSDETDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSHOPCARTPOSDETDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSHOPCARTPOSDETDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSHOPCARTPOSDETDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSHOPCARTPOSDETDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSHOPCARTPOSDETDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSHOPCARTPOSDETDATA);
    }

}
