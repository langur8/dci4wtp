<?php

namespace Axess\Dci4Wtp;

class D4WTPMEMBERTYPE
{

    /**
     * @var float $NTYPEID
     */
    protected $NTYPEID = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNTYPEID()
    {
      return $this->NTYPEID;
    }

    /**
     * @param float $NTYPEID
     * @return \Axess\Dci4Wtp\D4WTPMEMBERTYPE
     */
    public function setNTYPEID($NTYPEID)
    {
      $this->NTYPEID = $NTYPEID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPMEMBERTYPE
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPMEMBERTYPE
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
