<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST7REQUEST
{

    /**
     * @var float $BGROUPCONTINGENT
     */
    protected $BGROUPCONTINGENT = null;

    /**
     * @var float $BINCLCONTINGENTCNT
     */
    protected $BINCLCONTINGENTCNT = null;

    /**
     * @var float $BINCLUDINGERRORS
     */
    protected $BINCLUDINGERRORS = null;

    /**
     * @var D4WTPPRODUCTLIST $CTPRODUCTLIST
     */
    protected $CTPRODUCTLIST = null;

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NPERSOWNERNO
     */
    protected $NPERSOWNERNO = null;

    /**
     * @var float $NPERSOWNERPOSNO
     */
    protected $NPERSOWNERPOSNO = null;

    /**
     * @var float $NPERSOWNERPROJNO
     */
    protected $NPERSOWNERPROJNO = null;

    /**
     * @var float $NPERSPAYERNO
     */
    protected $NPERSPAYERNO = null;

    /**
     * @var float $NPERSPAYERPOSNO
     */
    protected $NPERSPAYERPOSNO = null;

    /**
     * @var float $NPERSPAYERPROJNO
     */
    protected $NPERSPAYERPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBGROUPCONTINGENT()
    {
      return $this->BGROUPCONTINGENT;
    }

    /**
     * @param float $BGROUPCONTINGENT
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setBGROUPCONTINGENT($BGROUPCONTINGENT)
    {
      $this->BGROUPCONTINGENT = $BGROUPCONTINGENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getBINCLCONTINGENTCNT()
    {
      return $this->BINCLCONTINGENTCNT;
    }

    /**
     * @param float $BINCLCONTINGENTCNT
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setBINCLCONTINGENTCNT($BINCLCONTINGENTCNT)
    {
      $this->BINCLCONTINGENTCNT = $BINCLCONTINGENTCNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getBINCLUDINGERRORS()
    {
      return $this->BINCLUDINGERRORS;
    }

    /**
     * @param float $BINCLUDINGERRORS
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setBINCLUDINGERRORS($BINCLUDINGERRORS)
    {
      $this->BINCLUDINGERRORS = $BINCLUDINGERRORS;
      return $this;
    }

    /**
     * @return D4WTPPRODUCTLIST
     */
    public function getCTPRODUCTLIST()
    {
      return $this->CTPRODUCTLIST;
    }

    /**
     * @param D4WTPPRODUCTLIST $CTPRODUCTLIST
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setCTPRODUCTLIST($CTPRODUCTLIST)
    {
      $this->CTPRODUCTLIST = $CTPRODUCTLIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSOWNERNO()
    {
      return $this->NPERSOWNERNO;
    }

    /**
     * @param float $NPERSOWNERNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setNPERSOWNERNO($NPERSOWNERNO)
    {
      $this->NPERSOWNERNO = $NPERSOWNERNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSOWNERPOSNO()
    {
      return $this->NPERSOWNERPOSNO;
    }

    /**
     * @param float $NPERSOWNERPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setNPERSOWNERPOSNO($NPERSOWNERPOSNO)
    {
      $this->NPERSOWNERPOSNO = $NPERSOWNERPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSOWNERPROJNO()
    {
      return $this->NPERSOWNERPROJNO;
    }

    /**
     * @param float $NPERSOWNERPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setNPERSOWNERPROJNO($NPERSOWNERPROJNO)
    {
      $this->NPERSOWNERPROJNO = $NPERSOWNERPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPAYERNO()
    {
      return $this->NPERSPAYERNO;
    }

    /**
     * @param float $NPERSPAYERNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setNPERSPAYERNO($NPERSPAYERNO)
    {
      $this->NPERSPAYERNO = $NPERSPAYERNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPAYERPOSNO()
    {
      return $this->NPERSPAYERPOSNO;
    }

    /**
     * @param float $NPERSPAYERPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setNPERSPAYERPOSNO($NPERSPAYERPOSNO)
    {
      $this->NPERSPAYERPOSNO = $NPERSPAYERPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPAYERPROJNO()
    {
      return $this->NPERSPAYERPROJNO;
    }

    /**
     * @param float $NPERSPAYERPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setNPERSPAYERPROJNO($NPERSPAYERPROJNO)
    {
      $this->NPERSPAYERPROJNO = $NPERSPAYERPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7REQUEST
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

}
