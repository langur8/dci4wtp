<?php

namespace Axess\Dci4Wtp;

class D4WTPSALESDETAIL
{

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSERIALNR
     */
    protected $NSERIALNR = null;

    /**
     * @var float $NUNICODENR
     */
    protected $NUNICODENR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPSALESDETAIL
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPSALESDETAIL
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPSALESDETAIL
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNR()
    {
      return $this->NSERIALNR;
    }

    /**
     * @param float $NSERIALNR
     * @return \Axess\Dci4Wtp\D4WTPSALESDETAIL
     */
    public function setNSERIALNR($NSERIALNR)
    {
      $this->NSERIALNR = $NSERIALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENR()
    {
      return $this->NUNICODENR;
    }

    /**
     * @param float $NUNICODENR
     * @return \Axess\Dci4Wtp\D4WTPSALESDETAIL
     */
    public function setNUNICODENR($NUNICODENR)
    {
      $this->NUNICODENR = $NUNICODENR;
      return $this;
    }

}
