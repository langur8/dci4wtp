<?php

namespace Axess\Dci4Wtp;

class getFamilyResponse
{

    /**
     * @var D4WTPGETFAMILYRESULT $getFamilyResult
     */
    protected $getFamilyResult = null;

    /**
     * @param D4WTPGETFAMILYRESULT $getFamilyResult
     */
    public function __construct($getFamilyResult)
    {
      $this->getFamilyResult = $getFamilyResult;
    }

    /**
     * @return D4WTPGETFAMILYRESULT
     */
    public function getGetFamilyResult()
    {
      return $this->getFamilyResult;
    }

    /**
     * @param D4WTPGETFAMILYRESULT $getFamilyResult
     * @return \Axess\Dci4Wtp\getFamilyResponse
     */
    public function setGetFamilyResult($getFamilyResult)
    {
      $this->getFamilyResult = $getFamilyResult;
      return $this;
    }

}
