<?php

namespace Axess\Dci4Wtp;

class DTL4FAMCODINGTICKETUSAGE
{

    /**
     * @var string $SZTICKETUSAGETIME
     */
    protected $SZTICKETUSAGETIME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZTICKETUSAGETIME()
    {
      return $this->SZTICKETUSAGETIME;
    }

    /**
     * @param string $SZTICKETUSAGETIME
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGTICKETUSAGE
     */
    public function setSZTICKETUSAGETIME($SZTICKETUSAGETIME)
    {
      $this->SZTICKETUSAGETIME = $SZTICKETUSAGETIME;
      return $this;
    }

}
