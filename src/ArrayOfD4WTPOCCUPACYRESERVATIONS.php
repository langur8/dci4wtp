<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPOCCUPACYRESERVATIONS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPOCCUPACYRESERVATIONS[] $D4WTPOCCUPACYRESERVATIONS
     */
    protected $D4WTPOCCUPACYRESERVATIONS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPOCCUPACYRESERVATIONS[]
     */
    public function getD4WTPOCCUPACYRESERVATIONS()
    {
      return $this->D4WTPOCCUPACYRESERVATIONS;
    }

    /**
     * @param D4WTPOCCUPACYRESERVATIONS[] $D4WTPOCCUPACYRESERVATIONS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPOCCUPACYRESERVATIONS
     */
    public function setD4WTPOCCUPACYRESERVATIONS(array $D4WTPOCCUPACYRESERVATIONS = null)
    {
      $this->D4WTPOCCUPACYRESERVATIONS = $D4WTPOCCUPACYRESERVATIONS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPOCCUPACYRESERVATIONS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPOCCUPACYRESERVATIONS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPOCCUPACYRESERVATIONS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPOCCUPACYRESERVATIONS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPOCCUPACYRESERVATIONS[] = $value;
      } else {
        $this->D4WTPOCCUPACYRESERVATIONS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPOCCUPACYRESERVATIONS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPOCCUPACYRESERVATIONS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPOCCUPACYRESERVATIONS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPOCCUPACYRESERVATIONS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPOCCUPACYRESERVATIONS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPOCCUPACYRESERVATIONS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPOCCUPACYRESERVATIONS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPOCCUPACYRESERVATIONS);
    }

}
