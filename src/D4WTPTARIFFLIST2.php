<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST2
{

    /**
     * @var ArrayOfD4WTPTARIFFLISTDAY2 $ACTTARIFFLISTDAY2
     */
    protected $ACTTARIFFLISTDAY2 = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLISTDAY2
     */
    public function getACTTARIFFLISTDAY2()
    {
      return $this->ACTTARIFFLISTDAY2;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLISTDAY2 $ACTTARIFFLISTDAY2
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST2
     */
    public function setACTTARIFFLISTDAY2($ACTTARIFFLISTDAY2)
    {
      $this->ACTTARIFFLISTDAY2 = $ACTTARIFFLISTDAY2;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST2
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
