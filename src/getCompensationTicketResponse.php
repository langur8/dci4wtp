<?php

namespace Axess\Dci4Wtp;

class getCompensationTicketResponse
{

    /**
     * @var D4WTPGETCOMTICRES $getCompensationTicketResult
     */
    protected $getCompensationTicketResult = null;

    /**
     * @param D4WTPGETCOMTICRES $getCompensationTicketResult
     */
    public function __construct($getCompensationTicketResult)
    {
      $this->getCompensationTicketResult = $getCompensationTicketResult;
    }

    /**
     * @return D4WTPGETCOMTICRES
     */
    public function getGetCompensationTicketResult()
    {
      return $this->getCompensationTicketResult;
    }

    /**
     * @param D4WTPGETCOMTICRES $getCompensationTicketResult
     * @return \Axess\Dci4Wtp\getCompensationTicketResponse
     */
    public function setGetCompensationTicketResult($getCompensationTicketResult)
    {
      $this->getCompensationTicketResult = $getCompensationTicketResult;
      return $this;
    }

}
