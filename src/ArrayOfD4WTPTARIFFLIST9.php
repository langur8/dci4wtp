<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTARIFFLIST9 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTARIFFLIST9[] $D4WTPTARIFFLIST9
     */
    protected $D4WTPTARIFFLIST9 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTARIFFLIST9[]
     */
    public function getD4WTPTARIFFLIST9()
    {
      return $this->D4WTPTARIFFLIST9;
    }

    /**
     * @param D4WTPTARIFFLIST9[] $D4WTPTARIFFLIST9
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTARIFFLIST9
     */
    public function setD4WTPTARIFFLIST9(array $D4WTPTARIFFLIST9 = null)
    {
      $this->D4WTPTARIFFLIST9 = $D4WTPTARIFFLIST9;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTARIFFLIST9[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTARIFFLIST9
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTARIFFLIST9[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTARIFFLIST9 $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTARIFFLIST9[] = $value;
      } else {
        $this->D4WTPTARIFFLIST9[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTARIFFLIST9[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTARIFFLIST9 Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTARIFFLIST9);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTARIFFLIST9);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTARIFFLIST9);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTARIFFLIST9);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTARIFFLIST9 Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTARIFFLIST9);
    }

}
