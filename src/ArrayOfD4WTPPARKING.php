<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPARKING implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPARKING[] $D4WTPPARKING
     */
    protected $D4WTPPARKING = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPARKING[]
     */
    public function getD4WTPPARKING()
    {
      return $this->D4WTPPARKING;
    }

    /**
     * @param D4WTPPARKING[] $D4WTPPARKING
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPARKING
     */
    public function setD4WTPPARKING(array $D4WTPPARKING = null)
    {
      $this->D4WTPPARKING = $D4WTPPARKING;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPARKING[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPARKING
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPARKING[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPARKING $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPARKING[] = $value;
      } else {
        $this->D4WTPPARKING[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPARKING[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPARKING Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPARKING);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPARKING);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPARKING);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPARKING);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPARKING Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPARKING);
    }

}
