<?php

namespace Axess\Dci4Wtp;

class getPools2Response
{

    /**
     * @var D4WTPPOOL2RESULT $getPools2Result
     */
    protected $getPools2Result = null;

    /**
     * @param D4WTPPOOL2RESULT $getPools2Result
     */
    public function __construct($getPools2Result)
    {
      $this->getPools2Result = $getPools2Result;
    }

    /**
     * @return D4WTPPOOL2RESULT
     */
    public function getGetPools2Result()
    {
      return $this->getPools2Result;
    }

    /**
     * @param D4WTPPOOL2RESULT $getPools2Result
     * @return \Axess\Dci4Wtp\getPools2Response
     */
    public function setGetPools2Result($getPools2Result)
    {
      $this->getPools2Result = $getPools2Result;
      return $this;
    }

}
