<?php

namespace Axess\Dci4Wtp;

class D4WTPPRODUCTREQUEST
{

    /**
     * @var float $NPRODUCTTYPENO
     */
    protected $NPRODUCTTYPENO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPRODUCTTYPENO()
    {
      return $this->NPRODUCTTYPENO;
    }

    /**
     * @param float $NPRODUCTTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTREQUEST
     */
    public function setNPRODUCTTYPENO($NPRODUCTTYPENO)
    {
      $this->NPRODUCTTYPENO = $NPRODUCTTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTREQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

}
