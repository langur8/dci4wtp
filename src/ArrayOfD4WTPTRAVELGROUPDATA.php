<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTRAVELGROUPDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTRAVELGROUPDATA[] $D4WTPTRAVELGROUPDATA
     */
    protected $D4WTPTRAVELGROUPDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTRAVELGROUPDATA[]
     */
    public function getD4WTPTRAVELGROUPDATA()
    {
      return $this->D4WTPTRAVELGROUPDATA;
    }

    /**
     * @param D4WTPTRAVELGROUPDATA[] $D4WTPTRAVELGROUPDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTRAVELGROUPDATA
     */
    public function setD4WTPTRAVELGROUPDATA(array $D4WTPTRAVELGROUPDATA = null)
    {
      $this->D4WTPTRAVELGROUPDATA = $D4WTPTRAVELGROUPDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTRAVELGROUPDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTRAVELGROUPDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTRAVELGROUPDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTRAVELGROUPDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTRAVELGROUPDATA[] = $value;
      } else {
        $this->D4WTPTRAVELGROUPDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTRAVELGROUPDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTRAVELGROUPDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTRAVELGROUPDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTRAVELGROUPDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTRAVELGROUPDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTRAVELGROUPDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTRAVELGROUPDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTRAVELGROUPDATA);
    }

}
