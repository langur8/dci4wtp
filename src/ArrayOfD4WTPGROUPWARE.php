<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPGROUPWARE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPGROUPWARE[] $D4WTPGROUPWARE
     */
    protected $D4WTPGROUPWARE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPGROUPWARE[]
     */
    public function getD4WTPGROUPWARE()
    {
      return $this->D4WTPGROUPWARE;
    }

    /**
     * @param D4WTPGROUPWARE[] $D4WTPGROUPWARE
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPGROUPWARE
     */
    public function setD4WTPGROUPWARE(array $D4WTPGROUPWARE = null)
    {
      $this->D4WTPGROUPWARE = $D4WTPGROUPWARE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPGROUPWARE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPGROUPWARE
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPGROUPWARE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPGROUPWARE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPGROUPWARE[] = $value;
      } else {
        $this->D4WTPGROUPWARE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPGROUPWARE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPGROUPWARE Return the current element
     */
    public function current()
    {
      return current($this->D4WTPGROUPWARE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPGROUPWARE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPGROUPWARE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPGROUPWARE);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPGROUPWARE Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPGROUPWARE);
    }

}
