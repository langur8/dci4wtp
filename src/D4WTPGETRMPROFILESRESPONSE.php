<?php

namespace Axess\Dci4Wtp;

class D4WTPGETRMPROFILESRESPONSE
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var ArrayOfD4WTPNAMEDUSERS $RMUSERS
     */
    protected $RMUSERS = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETRMPROFILESRESPONSE
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPNAMEDUSERS
     */
    public function getRMUSERS()
    {
      return $this->RMUSERS;
    }

    /**
     * @param ArrayOfD4WTPNAMEDUSERS $RMUSERS
     * @return \Axess\Dci4Wtp\D4WTPGETRMPROFILESRESPONSE
     */
    public function setRMUSERS($RMUSERS)
    {
      $this->RMUSERS = $RMUSERS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETRMPROFILESRESPONSE
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
