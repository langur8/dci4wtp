<?php

namespace Axess\Dci4Wtp;

class getWareList3Response
{

    /**
     * @var D4WTPGETWARELISTRESULT3 $getWareList3Result
     */
    protected $getWareList3Result = null;

    /**
     * @param D4WTPGETWARELISTRESULT3 $getWareList3Result
     */
    public function __construct($getWareList3Result)
    {
      $this->getWareList3Result = $getWareList3Result;
    }

    /**
     * @return D4WTPGETWARELISTRESULT3
     */
    public function getGetWareList3Result()
    {
      return $this->getWareList3Result;
    }

    /**
     * @param D4WTPGETWARELISTRESULT3 $getWareList3Result
     * @return \Axess\Dci4Wtp\getWareList3Response
     */
    public function setGetWareList3Result($getWareList3Result)
    {
      $this->getWareList3Result = $getWareList3Result;
      return $this;
    }

}
