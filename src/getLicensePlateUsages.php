<?php

namespace Axess\Dci4Wtp;

class getLicensePlateUsages
{

    /**
     * @var float $i_nsessionID
     */
    protected $i_nsessionID = null;

    /**
     * @var string $i_szlicenseplate
     */
    protected $i_szlicenseplate = null;

    /**
     * @param float $i_nsessionID
     * @param string $i_szlicenseplate
     */
    public function __construct($i_nsessionID, $i_szlicenseplate)
    {
      $this->i_nsessionID = $i_nsessionID;
      $this->i_szlicenseplate = $i_szlicenseplate;
    }

    /**
     * @return float
     */
    public function getI_nsessionID()
    {
      return $this->i_nsessionID;
    }

    /**
     * @param float $i_nsessionID
     * @return \Axess\Dci4Wtp\getLicensePlateUsages
     */
    public function setI_nsessionID($i_nsessionID)
    {
      $this->i_nsessionID = $i_nsessionID;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szlicenseplate()
    {
      return $this->i_szlicenseplate;
    }

    /**
     * @param string $i_szlicenseplate
     * @return \Axess\Dci4Wtp\getLicensePlateUsages
     */
    public function setI_szlicenseplate($i_szlicenseplate)
    {
      $this->i_szlicenseplate = $i_szlicenseplate;
      return $this;
    }

}
