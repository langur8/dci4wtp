<?php

namespace Axess\Dci4Wtp;

class getTariff2
{

    /**
     * @var D4WTPTARIFF2REQUEST $i_ctTariffReq
     */
    protected $i_ctTariffReq = null;

    /**
     * @param D4WTPTARIFF2REQUEST $i_ctTariffReq
     */
    public function __construct($i_ctTariffReq)
    {
      $this->i_ctTariffReq = $i_ctTariffReq;
    }

    /**
     * @return D4WTPTARIFF2REQUEST
     */
    public function getI_ctTariffReq()
    {
      return $this->i_ctTariffReq;
    }

    /**
     * @param D4WTPTARIFF2REQUEST $i_ctTariffReq
     * @return \Axess\Dci4Wtp\getTariff2
     */
    public function setI_ctTariffReq($i_ctTariffReq)
    {
      $this->i_ctTariffReq = $i_ctTariffReq;
      return $this;
    }

}
