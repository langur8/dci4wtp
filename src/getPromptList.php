<?php

namespace Axess\Dci4Wtp;

class getPromptList
{

    /**
     * @var D4WTPGETPROMPTLISTREQUEST $i_ctGetPromptListReq
     */
    protected $i_ctGetPromptListReq = null;

    /**
     * @param D4WTPGETPROMPTLISTREQUEST $i_ctGetPromptListReq
     */
    public function __construct($i_ctGetPromptListReq)
    {
      $this->i_ctGetPromptListReq = $i_ctGetPromptListReq;
    }

    /**
     * @return D4WTPGETPROMPTLISTREQUEST
     */
    public function getI_ctGetPromptListReq()
    {
      return $this->i_ctGetPromptListReq;
    }

    /**
     * @param D4WTPGETPROMPTLISTREQUEST $i_ctGetPromptListReq
     * @return \Axess\Dci4Wtp\getPromptList
     */
    public function setI_ctGetPromptListReq($i_ctGetPromptListReq)
    {
      $this->i_ctGetPromptListReq = $i_ctGetPromptListReq;
      return $this;
    }

}
