<?php

namespace Axess\Dci4Wtp;

class getPersonData4Response
{

    /**
     * @var D4WTPGETPERSON4RESULT $getPersonData4Result
     */
    protected $getPersonData4Result = null;

    /**
     * @param D4WTPGETPERSON4RESULT $getPersonData4Result
     */
    public function __construct($getPersonData4Result)
    {
      $this->getPersonData4Result = $getPersonData4Result;
    }

    /**
     * @return D4WTPGETPERSON4RESULT
     */
    public function getGetPersonData4Result()
    {
      return $this->getPersonData4Result;
    }

    /**
     * @param D4WTPGETPERSON4RESULT $getPersonData4Result
     * @return \Axess\Dci4Wtp\getPersonData4Response
     */
    public function setGetPersonData4Result($getPersonData4Result)
    {
      $this->getPersonData4Result = $getPersonData4Result;
      return $this;
    }

}
