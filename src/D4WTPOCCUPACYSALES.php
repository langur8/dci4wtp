<?php

namespace Axess\Dci4Wtp;

class D4WTPOCCUPACYSALES
{

    /**
     * @var float $NBOOKINGFACTOR
     */
    protected $NBOOKINGFACTOR = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPERSONTYPENO
     */
    protected $NPERSONTYPENO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NSTATUS
     */
    protected $NSTATUS = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var float $NTRANSACTIONTYPE
     */
    protected $NTRANSACTIONTYPE = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    /**
     * @var float $NTRAVELGROUPNO
     */
    protected $NTRAVELGROUPNO = null;

    /**
     * @var float $NTRAVELGROUPPOSNO
     */
    protected $NTRAVELGROUPPOSNO = null;

    /**
     * @var float $NTRAVELGROUPPROJNO
     */
    protected $NTRAVELGROUPPROJNO = null;

    /**
     * @var float $NUNICODENO
     */
    protected $NUNICODENO = null;

    /**
     * @var string $SZCOMPANYNAME
     */
    protected $SZCOMPANYNAME = null;

    /**
     * @var string $SZCONTACTPERSONEMAIL
     */
    protected $SZCONTACTPERSONEMAIL = null;

    /**
     * @var string $SZCONTACTPERSONPHONE
     */
    protected $SZCONTACTPERSONPHONE = null;

    /**
     * @var string $SZDATEFROM
     */
    protected $SZDATEFROM = null;

    /**
     * @var string $SZDATETO
     */
    protected $SZDATETO = null;

    /**
     * @var string $SZOWNEREMAIL
     */
    protected $SZOWNEREMAIL = null;

    /**
     * @var string $SZOWNERNAME
     */
    protected $SZOWNERNAME = null;

    /**
     * @var string $SZOWNERPHONE
     */
    protected $SZOWNERPHONE = null;

    /**
     * @var string $SZPERSONTYPENAME
     */
    protected $SZPERSONTYPENAME = null;

    /**
     * @var string $SZPOSNAME
     */
    protected $SZPOSNAME = null;

    /**
     * @var string $SZTICKETTYPENAME
     */
    protected $SZTICKETTYPENAME = null;

    /**
     * @var string $SZTRANSTYPENAME
     */
    protected $SZTRANSTYPENAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNBOOKINGFACTOR()
    {
      return $this->NBOOKINGFACTOR;
    }

    /**
     * @param float $NBOOKINGFACTOR
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNBOOKINGFACTOR($NBOOKINGFACTOR)
    {
      $this->NBOOKINGFACTOR = $NBOOKINGFACTOR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSONTYPENO()
    {
      return $this->NPERSONTYPENO;
    }

    /**
     * @param float $NPERSONTYPENO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNPERSONTYPENO($NPERSONTYPENO)
    {
      $this->NPERSONTYPENO = $NPERSONTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTATUS()
    {
      return $this->NSTATUS;
    }

    /**
     * @param float $NSTATUS
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNSTATUS($NSTATUS)
    {
      $this->NSTATUS = $NSTATUS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSACTIONTYPE()
    {
      return $this->NTRANSACTIONTYPE;
    }

    /**
     * @param float $NTRANSACTIONTYPE
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNTRANSACTIONTYPE($NTRANSACTIONTYPE)
    {
      $this->NTRANSACTIONTYPE = $NTRANSACTIONTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPNO()
    {
      return $this->NTRAVELGROUPNO;
    }

    /**
     * @param float $NTRAVELGROUPNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNTRAVELGROUPNO($NTRAVELGROUPNO)
    {
      $this->NTRAVELGROUPNO = $NTRAVELGROUPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPOSNO()
    {
      return $this->NTRAVELGROUPPOSNO;
    }

    /**
     * @param float $NTRAVELGROUPPOSNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNTRAVELGROUPPOSNO($NTRAVELGROUPPOSNO)
    {
      $this->NTRAVELGROUPPOSNO = $NTRAVELGROUPPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPROJNO()
    {
      return $this->NTRAVELGROUPPROJNO;
    }

    /**
     * @param float $NTRAVELGROUPPROJNO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNTRAVELGROUPPROJNO($NTRAVELGROUPPROJNO)
    {
      $this->NTRAVELGROUPPROJNO = $NTRAVELGROUPPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENO()
    {
      return $this->NUNICODENO;
    }

    /**
     * @param float $NUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setNUNICODENO($NUNICODENO)
    {
      $this->NUNICODENO = $NUNICODENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYNAME()
    {
      return $this->SZCOMPANYNAME;
    }

    /**
     * @param string $SZCOMPANYNAME
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setSZCOMPANYNAME($SZCOMPANYNAME)
    {
      $this->SZCOMPANYNAME = $SZCOMPANYNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCONTACTPERSONEMAIL()
    {
      return $this->SZCONTACTPERSONEMAIL;
    }

    /**
     * @param string $SZCONTACTPERSONEMAIL
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setSZCONTACTPERSONEMAIL($SZCONTACTPERSONEMAIL)
    {
      $this->SZCONTACTPERSONEMAIL = $SZCONTACTPERSONEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCONTACTPERSONPHONE()
    {
      return $this->SZCONTACTPERSONPHONE;
    }

    /**
     * @param string $SZCONTACTPERSONPHONE
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setSZCONTACTPERSONPHONE($SZCONTACTPERSONPHONE)
    {
      $this->SZCONTACTPERSONPHONE = $SZCONTACTPERSONPHONE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEFROM()
    {
      return $this->SZDATEFROM;
    }

    /**
     * @param string $SZDATEFROM
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setSZDATEFROM($SZDATEFROM)
    {
      $this->SZDATEFROM = $SZDATEFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATETO()
    {
      return $this->SZDATETO;
    }

    /**
     * @param string $SZDATETO
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setSZDATETO($SZDATETO)
    {
      $this->SZDATETO = $SZDATETO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZOWNEREMAIL()
    {
      return $this->SZOWNEREMAIL;
    }

    /**
     * @param string $SZOWNEREMAIL
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setSZOWNEREMAIL($SZOWNEREMAIL)
    {
      $this->SZOWNEREMAIL = $SZOWNEREMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZOWNERNAME()
    {
      return $this->SZOWNERNAME;
    }

    /**
     * @param string $SZOWNERNAME
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setSZOWNERNAME($SZOWNERNAME)
    {
      $this->SZOWNERNAME = $SZOWNERNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZOWNERPHONE()
    {
      return $this->SZOWNERPHONE;
    }

    /**
     * @param string $SZOWNERPHONE
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setSZOWNERPHONE($SZOWNERPHONE)
    {
      $this->SZOWNERPHONE = $SZOWNERPHONE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSONTYPENAME()
    {
      return $this->SZPERSONTYPENAME;
    }

    /**
     * @param string $SZPERSONTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setSZPERSONTYPENAME($SZPERSONTYPENAME)
    {
      $this->SZPERSONTYPENAME = $SZPERSONTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOSNAME()
    {
      return $this->SZPOSNAME;
    }

    /**
     * @param string $SZPOSNAME
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setSZPOSNAME($SZPOSNAME)
    {
      $this->SZPOSNAME = $SZPOSNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPENAME()
    {
      return $this->SZTICKETTYPENAME;
    }

    /**
     * @param string $SZTICKETTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setSZTICKETTYPENAME($SZTICKETTYPENAME)
    {
      $this->SZTICKETTYPENAME = $SZTICKETTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTRANSTYPENAME()
    {
      return $this->SZTRANSTYPENAME;
    }

    /**
     * @param string $SZTRANSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPOCCUPACYSALES
     */
    public function setSZTRANSTYPENAME($SZTRANSTYPENAME)
    {
      $this->SZTRANSTYPENAME = $SZTRANSTYPENAME;
      return $this;
    }

}
