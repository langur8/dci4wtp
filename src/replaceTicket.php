<?php

namespace Axess\Dci4Wtp;

class replaceTicket
{

    /**
     * @var D4WTPREPLACETICKETREQ $i_ctReplaceTicketReq
     */
    protected $i_ctReplaceTicketReq = null;

    /**
     * @param D4WTPREPLACETICKETREQ $i_ctReplaceTicketReq
     */
    public function __construct($i_ctReplaceTicketReq)
    {
      $this->i_ctReplaceTicketReq = $i_ctReplaceTicketReq;
    }

    /**
     * @return D4WTPREPLACETICKETREQ
     */
    public function getI_ctReplaceTicketReq()
    {
      return $this->i_ctReplaceTicketReq;
    }

    /**
     * @param D4WTPREPLACETICKETREQ $i_ctReplaceTicketReq
     * @return \Axess\Dci4Wtp\replaceTicket
     */
    public function setI_ctReplaceTicketReq($i_ctReplaceTicketReq)
    {
      $this->i_ctReplaceTicketReq = $i_ctReplaceTicketReq;
      return $this;
    }

}
