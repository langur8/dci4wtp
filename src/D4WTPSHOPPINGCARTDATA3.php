<?php

namespace Axess\Dci4Wtp;

class D4WTPSHOPPINGCARTDATA3
{

    /**
     * @var ArrayOfD4WTPSHOPCARTPOSDATA3 $ACTSHOPCARTPOSDATA
     */
    protected $ACTSHOPCARTPOSDATA = null;

    /**
     * @var float $FOFFERPRICE
     */
    protected $FOFFERPRICE = null;

    /**
     * @var float $NBASICPAYMETHODNO
     */
    protected $NBASICPAYMETHODNO = null;

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NCUSTPAYMETHODNO
     */
    protected $NCUSTPAYMETHODNO = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NLOCKPOSNO
     */
    protected $NLOCKPOSNO = null;

    /**
     * @var float $NLOCKPROJNO
     */
    protected $NLOCKPROJNO = null;

    /**
     * @var float $NNAMEDUSERID
     */
    protected $NNAMEDUSERID = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var float $NSHOPPINGCARTPOSNO
     */
    protected $NSHOPPINGCARTPOSNO = null;

    /**
     * @var float $NSHOPPINGCARTPROJNO
     */
    protected $NSHOPPINGCARTPROJNO = null;

    /**
     * @var float $NSTATUSNO
     */
    protected $NSTATUSNO = null;

    /**
     * @var float $NSWITCHGESNO
     */
    protected $NSWITCHGESNO = null;

    /**
     * @var float $NSWITCHPROJNO
     */
    protected $NSWITCHPROJNO = null;

    /**
     * @var float $NSWITCHSUBPROJNO
     */
    protected $NSWITCHSUBPROJNO = null;

    /**
     * @var string $SZDBSESSIONID
     */
    protected $SZDBSESSIONID = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZEXPIRYDATE
     */
    protected $SZEXPIRYDATE = null;

    /**
     * @var string $SZLOCKUNTIL
     */
    protected $SZLOCKUNTIL = null;

    /**
     * @var string $SZSHOPPINGCARTCODE
     */
    protected $SZSHOPPINGCARTCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSHOPCARTPOSDATA3
     */
    public function getACTSHOPCARTPOSDATA()
    {
      return $this->ACTSHOPCARTPOSDATA;
    }

    /**
     * @param ArrayOfD4WTPSHOPCARTPOSDATA3 $ACTSHOPCARTPOSDATA
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setACTSHOPCARTPOSDATA($ACTSHOPCARTPOSDATA)
    {
      $this->ACTSHOPCARTPOSDATA = $ACTSHOPCARTPOSDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getFOFFERPRICE()
    {
      return $this->FOFFERPRICE;
    }

    /**
     * @param float $FOFFERPRICE
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setFOFFERPRICE($FOFFERPRICE)
    {
      $this->FOFFERPRICE = $FOFFERPRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBASICPAYMETHODNO()
    {
      return $this->NBASICPAYMETHODNO;
    }

    /**
     * @param float $NBASICPAYMETHODNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNBASICPAYMETHODNO($NBASICPAYMETHODNO)
    {
      $this->NBASICPAYMETHODNO = $NBASICPAYMETHODNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPAYMETHODNO()
    {
      return $this->NCUSTPAYMETHODNO;
    }

    /**
     * @param float $NCUSTPAYMETHODNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNCUSTPAYMETHODNO($NCUSTPAYMETHODNO)
    {
      $this->NCUSTPAYMETHODNO = $NCUSTPAYMETHODNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLOCKPOSNO()
    {
      return $this->NLOCKPOSNO;
    }

    /**
     * @param float $NLOCKPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNLOCKPOSNO($NLOCKPOSNO)
    {
      $this->NLOCKPOSNO = $NLOCKPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLOCKPROJNO()
    {
      return $this->NLOCKPROJNO;
    }

    /**
     * @param float $NLOCKPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNLOCKPROJNO($NLOCKPROJNO)
    {
      $this->NLOCKPROJNO = $NLOCKPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNAMEDUSERID()
    {
      return $this->NNAMEDUSERID;
    }

    /**
     * @param float $NNAMEDUSERID
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNNAMEDUSERID($NNAMEDUSERID)
    {
      $this->NNAMEDUSERID = $NNAMEDUSERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPOSNO()
    {
      return $this->NSHOPPINGCARTPOSNO;
    }

    /**
     * @param float $NSHOPPINGCARTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNSHOPPINGCARTPOSNO($NSHOPPINGCARTPOSNO)
    {
      $this->NSHOPPINGCARTPOSNO = $NSHOPPINGCARTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPROJNO()
    {
      return $this->NSHOPPINGCARTPROJNO;
    }

    /**
     * @param float $NSHOPPINGCARTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNSHOPPINGCARTPROJNO($NSHOPPINGCARTPROJNO)
    {
      $this->NSHOPPINGCARTPROJNO = $NSHOPPINGCARTPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTATUSNO()
    {
      return $this->NSTATUSNO;
    }

    /**
     * @param float $NSTATUSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNSTATUSNO($NSTATUSNO)
    {
      $this->NSTATUSNO = $NSTATUSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSWITCHGESNO()
    {
      return $this->NSWITCHGESNO;
    }

    /**
     * @param float $NSWITCHGESNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNSWITCHGESNO($NSWITCHGESNO)
    {
      $this->NSWITCHGESNO = $NSWITCHGESNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSWITCHPROJNO()
    {
      return $this->NSWITCHPROJNO;
    }

    /**
     * @param float $NSWITCHPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNSWITCHPROJNO($NSWITCHPROJNO)
    {
      $this->NSWITCHPROJNO = $NSWITCHPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSWITCHSUBPROJNO()
    {
      return $this->NSWITCHSUBPROJNO;
    }

    /**
     * @param float $NSWITCHSUBPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setNSWITCHSUBPROJNO($NSWITCHSUBPROJNO)
    {
      $this->NSWITCHSUBPROJNO = $NSWITCHSUBPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDBSESSIONID()
    {
      return $this->SZDBSESSIONID;
    }

    /**
     * @param string $SZDBSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setSZDBSESSIONID($SZDBSESSIONID)
    {
      $this->SZDBSESSIONID = $SZDBSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXPIRYDATE()
    {
      return $this->SZEXPIRYDATE;
    }

    /**
     * @param string $SZEXPIRYDATE
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setSZEXPIRYDATE($SZEXPIRYDATE)
    {
      $this->SZEXPIRYDATE = $SZEXPIRYDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOCKUNTIL()
    {
      return $this->SZLOCKUNTIL;
    }

    /**
     * @param string $SZLOCKUNTIL
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setSZLOCKUNTIL($SZLOCKUNTIL)
    {
      $this->SZLOCKUNTIL = $SZLOCKUNTIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHOPPINGCARTCODE()
    {
      return $this->SZSHOPPINGCARTCODE;
    }

    /**
     * @param string $SZSHOPPINGCARTCODE
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTDATA3
     */
    public function setSZSHOPPINGCARTCODE($SZSHOPPINGCARTCODE)
    {
      $this->SZSHOPPINGCARTCODE = $SZSHOPPINGCARTCODE;
      return $this;
    }

}
