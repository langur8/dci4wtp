<?php

namespace Axess\Dci4Wtp;

class sendWTPDataOCC
{

    /**
     * @var D4WTPSENDWTPDATAREQ $i_ctSendWTPDataReq
     */
    protected $i_ctSendWTPDataReq = null;

    /**
     * @param D4WTPSENDWTPDATAREQ $i_ctSendWTPDataReq
     */
    public function __construct($i_ctSendWTPDataReq)
    {
      $this->i_ctSendWTPDataReq = $i_ctSendWTPDataReq;
    }

    /**
     * @return D4WTPSENDWTPDATAREQ
     */
    public function getI_ctSendWTPDataReq()
    {
      return $this->i_ctSendWTPDataReq;
    }

    /**
     * @param D4WTPSENDWTPDATAREQ $i_ctSendWTPDataReq
     * @return \Axess\Dci4Wtp\sendWTPDataOCC
     */
    public function setI_ctSendWTPDataReq($i_ctSendWTPDataReq)
    {
      $this->i_ctSendWTPDataReq = $i_ctSendWTPDataReq;
      return $this;
    }

}
