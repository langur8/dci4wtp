<?php

namespace Axess\Dci4Wtp;

class D4WTPCASHIERSHIFT
{

    /**
     * @var ArrayOfD4WTPSALESSUMMARY $ACTSALESSUMMARY
     */
    protected $ACTSALESSUMMARY = null;

    /**
     * @var float $NCASHIERID
     */
    protected $NCASHIERID = null;

    /**
     * @var float $NCASHIERSHIFTNO
     */
    protected $NCASHIERSHIFTNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var string $SZINFO
     */
    protected $SZINFO = null;

    /**
     * @var string $SZSHIFTBEGIN
     */
    protected $SZSHIFTBEGIN = null;

    /**
     * @var string $SZSHIFTEND
     */
    protected $SZSHIFTEND = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSALESSUMMARY
     */
    public function getACTSALESSUMMARY()
    {
      return $this->ACTSALESSUMMARY;
    }

    /**
     * @param ArrayOfD4WTPSALESSUMMARY $ACTSALESSUMMARY
     * @return \Axess\Dci4Wtp\D4WTPCASHIERSHIFT
     */
    public function setACTSALESSUMMARY($ACTSALESSUMMARY)
    {
      $this->ACTSALESSUMMARY = $ACTSALESSUMMARY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCASHIERID()
    {
      return $this->NCASHIERID;
    }

    /**
     * @param float $NCASHIERID
     * @return \Axess\Dci4Wtp\D4WTPCASHIERSHIFT
     */
    public function setNCASHIERID($NCASHIERID)
    {
      $this->NCASHIERID = $NCASHIERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCASHIERSHIFTNO()
    {
      return $this->NCASHIERSHIFTNO;
    }

    /**
     * @param float $NCASHIERSHIFTNO
     * @return \Axess\Dci4Wtp\D4WTPCASHIERSHIFT
     */
    public function setNCASHIERSHIFTNO($NCASHIERSHIFTNO)
    {
      $this->NCASHIERSHIFTNO = $NCASHIERSHIFTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCASHIERSHIFT
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCASHIERSHIFT
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZINFO()
    {
      return $this->SZINFO;
    }

    /**
     * @param string $SZINFO
     * @return \Axess\Dci4Wtp\D4WTPCASHIERSHIFT
     */
    public function setSZINFO($SZINFO)
    {
      $this->SZINFO = $SZINFO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHIFTBEGIN()
    {
      return $this->SZSHIFTBEGIN;
    }

    /**
     * @param string $SZSHIFTBEGIN
     * @return \Axess\Dci4Wtp\D4WTPCASHIERSHIFT
     */
    public function setSZSHIFTBEGIN($SZSHIFTBEGIN)
    {
      $this->SZSHIFTBEGIN = $SZSHIFTBEGIN;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHIFTEND()
    {
      return $this->SZSHIFTEND;
    }

    /**
     * @param string $SZSHIFTEND
     * @return \Axess\Dci4Wtp\D4WTPCASHIERSHIFT
     */
    public function setSZSHIFTEND($SZSHIFTEND)
    {
      $this->SZSHIFTEND = $SZSHIFTEND;
      return $this;
    }

}
