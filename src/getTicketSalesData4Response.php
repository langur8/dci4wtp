<?php

namespace Axess\Dci4Wtp;

class getTicketSalesData4Response
{

    /**
     * @var D4WTPSALESDATARESULT4 $getTicketSalesData4Result
     */
    protected $getTicketSalesData4Result = null;

    /**
     * @param D4WTPSALESDATARESULT4 $getTicketSalesData4Result
     */
    public function __construct($getTicketSalesData4Result)
    {
      $this->getTicketSalesData4Result = $getTicketSalesData4Result;
    }

    /**
     * @return D4WTPSALESDATARESULT4
     */
    public function getGetTicketSalesData4Result()
    {
      return $this->getTicketSalesData4Result;
    }

    /**
     * @param D4WTPSALESDATARESULT4 $getTicketSalesData4Result
     * @return \Axess\Dci4Wtp\getTicketSalesData4Response
     */
    public function setGetTicketSalesData4Result($getTicketSalesData4Result)
    {
      $this->getTicketSalesData4Result = $getTicketSalesData4Result;
      return $this;
    }

}
