<?php

namespace Axess\Dci4Wtp;

class getGroupContingent2
{

    /**
     * @var D4WTPGETGRPCONTINGENTREQUEST $i_ctgetGroupContingentReq
     */
    protected $i_ctgetGroupContingentReq = null;

    /**
     * @param D4WTPGETGRPCONTINGENTREQUEST $i_ctgetGroupContingentReq
     */
    public function __construct($i_ctgetGroupContingentReq)
    {
      $this->i_ctgetGroupContingentReq = $i_ctgetGroupContingentReq;
    }

    /**
     * @return D4WTPGETGRPCONTINGENTREQUEST
     */
    public function getI_ctgetGroupContingentReq()
    {
      return $this->i_ctgetGroupContingentReq;
    }

    /**
     * @param D4WTPGETGRPCONTINGENTREQUEST $i_ctgetGroupContingentReq
     * @return \Axess\Dci4Wtp\getGroupContingent2
     */
    public function setI_ctgetGroupContingentReq($i_ctgetGroupContingentReq)
    {
      $this->i_ctgetGroupContingentReq = $i_ctgetGroupContingentReq;
      return $this;
    }

}
