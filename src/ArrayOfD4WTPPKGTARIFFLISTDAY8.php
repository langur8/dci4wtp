<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPKGTARIFFLISTDAY8 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPKGTARIFFLISTDAY8[] $D4WTPPKGTARIFFLISTDAY8
     */
    protected $D4WTPPKGTARIFFLISTDAY8 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPKGTARIFFLISTDAY8[]
     */
    public function getD4WTPPKGTARIFFLISTDAY8()
    {
      return $this->D4WTPPKGTARIFFLISTDAY8;
    }

    /**
     * @param D4WTPPKGTARIFFLISTDAY8[] $D4WTPPKGTARIFFLISTDAY8
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPKGTARIFFLISTDAY8
     */
    public function setD4WTPPKGTARIFFLISTDAY8(array $D4WTPPKGTARIFFLISTDAY8 = null)
    {
      $this->D4WTPPKGTARIFFLISTDAY8 = $D4WTPPKGTARIFFLISTDAY8;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPKGTARIFFLISTDAY8[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPKGTARIFFLISTDAY8
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPKGTARIFFLISTDAY8[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPKGTARIFFLISTDAY8 $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPKGTARIFFLISTDAY8[] = $value;
      } else {
        $this->D4WTPPKGTARIFFLISTDAY8[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPKGTARIFFLISTDAY8[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPKGTARIFFLISTDAY8 Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPKGTARIFFLISTDAY8);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPKGTARIFFLISTDAY8);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPKGTARIFFLISTDAY8);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPKGTARIFFLISTDAY8);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPKGTARIFFLISTDAY8 Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPKGTARIFFLISTDAY8);
    }

}
