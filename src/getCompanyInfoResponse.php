<?php

namespace Axess\Dci4Wtp;

class getCompanyInfoResponse
{

    /**
     * @var D4WTPCOMPANYINFORESULT $getCompanyInfoResult
     */
    protected $getCompanyInfoResult = null;

    /**
     * @param D4WTPCOMPANYINFORESULT $getCompanyInfoResult
     */
    public function __construct($getCompanyInfoResult)
    {
      $this->getCompanyInfoResult = $getCompanyInfoResult;
    }

    /**
     * @return D4WTPCOMPANYINFORESULT
     */
    public function getGetCompanyInfoResult()
    {
      return $this->getCompanyInfoResult;
    }

    /**
     * @param D4WTPCOMPANYINFORESULT $getCompanyInfoResult
     * @return \Axess\Dci4Wtp\getCompanyInfoResponse
     */
    public function setGetCompanyInfoResult($getCompanyInfoResult)
    {
      $this->getCompanyInfoResult = $getCompanyInfoResult;
      return $this;
    }

}
