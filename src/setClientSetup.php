<?php

namespace Axess\Dci4Wtp;

class setClientSetup
{

    /**
     * @var D4WTPSETCLIENTSETUPREQUEST $i_ctSetClientSetupReq
     */
    protected $i_ctSetClientSetupReq = null;

    /**
     * @param D4WTPSETCLIENTSETUPREQUEST $i_ctSetClientSetupReq
     */
    public function __construct($i_ctSetClientSetupReq)
    {
      $this->i_ctSetClientSetupReq = $i_ctSetClientSetupReq;
    }

    /**
     * @return D4WTPSETCLIENTSETUPREQUEST
     */
    public function getI_ctSetClientSetupReq()
    {
      return $this->i_ctSetClientSetupReq;
    }

    /**
     * @param D4WTPSETCLIENTSETUPREQUEST $i_ctSetClientSetupReq
     * @return \Axess\Dci4Wtp\setClientSetup
     */
    public function setI_ctSetClientSetupReq($i_ctSetClientSetupReq)
    {
      $this->i_ctSetClientSetupReq = $i_ctSetClientSetupReq;
      return $this;
    }

}
