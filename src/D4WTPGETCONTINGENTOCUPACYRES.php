<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCONTINGENTOCUPACYRES
{

    /**
     * @var ArrayOfD4WTPCONTINGENTOCCUPACY $CONTINGENTOCCUPACY
     */
    protected $CONTINGENTOCCUPACY = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCONTINGENTOCCUPACY
     */
    public function getCONTINGENTOCCUPACY()
    {
      return $this->CONTINGENTOCCUPACY;
    }

    /**
     * @param ArrayOfD4WTPCONTINGENTOCCUPACY $CONTINGENTOCCUPACY
     * @return \Axess\Dci4Wtp\D4WTPGETCONTINGENTOCUPACYRES
     */
    public function setCONTINGENTOCCUPACY($CONTINGENTOCCUPACY)
    {
      $this->CONTINGENTOCCUPACY = $CONTINGENTOCCUPACY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETCONTINGENTOCUPACYRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETCONTINGENTOCUPACYRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
