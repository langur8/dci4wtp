<?php

namespace Axess\Dci4Wtp;

class D4WTPCONTINGENTLIST6
{

    /**
     * @var ArrayOfD4WTPSUBCONTINGENTLIST4 $ACTSUBCONTINGENTLIST4
     */
    protected $ACTSUBCONTINGENTLIST4 = null;

    /**
     * @var float $BAUTOSELECT
     */
    protected $BAUTOSELECT = null;

    /**
     * @var float $BRESOPTIONAL
     */
    protected $BRESOPTIONAL = null;

    /**
     * @var D4WTPRESARTICLETARIFF $CTRESARTICLE
     */
    protected $CTRESARTICLE = null;

    /**
     * @var float $NAUTOSELECTSORTNR
     */
    protected $NAUTOSELECTSORTNR = null;

    /**
     * @var float $NCONTINGENTNR
     */
    protected $NCONTINGENTNR = null;

    /**
     * @var float $NCOUNTFREE
     */
    protected $NCOUNTFREE = null;

    /**
     * @var float $NSORTNR
     */
    protected $NSORTNR = null;

    /**
     * @var float $NTRAVELGROUPOFFSETMIN
     */
    protected $NTRAVELGROUPOFFSETMIN = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSUBCONTINGENTLIST4
     */
    public function getACTSUBCONTINGENTLIST4()
    {
      return $this->ACTSUBCONTINGENTLIST4;
    }

    /**
     * @param ArrayOfD4WTPSUBCONTINGENTLIST4 $ACTSUBCONTINGENTLIST4
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST6
     */
    public function setACTSUBCONTINGENTLIST4($ACTSUBCONTINGENTLIST4)
    {
      $this->ACTSUBCONTINGENTLIST4 = $ACTSUBCONTINGENTLIST4;
      return $this;
    }

    /**
     * @return float
     */
    public function getBAUTOSELECT()
    {
      return $this->BAUTOSELECT;
    }

    /**
     * @param float $BAUTOSELECT
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST6
     */
    public function setBAUTOSELECT($BAUTOSELECT)
    {
      $this->BAUTOSELECT = $BAUTOSELECT;
      return $this;
    }

    /**
     * @return float
     */
    public function getBRESOPTIONAL()
    {
      return $this->BRESOPTIONAL;
    }

    /**
     * @param float $BRESOPTIONAL
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST6
     */
    public function setBRESOPTIONAL($BRESOPTIONAL)
    {
      $this->BRESOPTIONAL = $BRESOPTIONAL;
      return $this;
    }

    /**
     * @return D4WTPRESARTICLETARIFF
     */
    public function getCTRESARTICLE()
    {
      return $this->CTRESARTICLE;
    }

    /**
     * @param D4WTPRESARTICLETARIFF $CTRESARTICLE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST6
     */
    public function setCTRESARTICLE($CTRESARTICLE)
    {
      $this->CTRESARTICLE = $CTRESARTICLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAUTOSELECTSORTNR()
    {
      return $this->NAUTOSELECTSORTNR;
    }

    /**
     * @param float $NAUTOSELECTSORTNR
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST6
     */
    public function setNAUTOSELECTSORTNR($NAUTOSELECTSORTNR)
    {
      $this->NAUTOSELECTSORTNR = $NAUTOSELECTSORTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNR()
    {
      return $this->NCONTINGENTNR;
    }

    /**
     * @param float $NCONTINGENTNR
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST6
     */
    public function setNCONTINGENTNR($NCONTINGENTNR)
    {
      $this->NCONTINGENTNR = $NCONTINGENTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTFREE()
    {
      return $this->NCOUNTFREE;
    }

    /**
     * @param float $NCOUNTFREE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST6
     */
    public function setNCOUNTFREE($NCOUNTFREE)
    {
      $this->NCOUNTFREE = $NCOUNTFREE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNR()
    {
      return $this->NSORTNR;
    }

    /**
     * @param float $NSORTNR
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST6
     */
    public function setNSORTNR($NSORTNR)
    {
      $this->NSORTNR = $NSORTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPOFFSETMIN()
    {
      return $this->NTRAVELGROUPOFFSETMIN;
    }

    /**
     * @param float $NTRAVELGROUPOFFSETMIN
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST6
     */
    public function setNTRAVELGROUPOFFSETMIN($NTRAVELGROUPOFFSETMIN)
    {
      $this->NTRAVELGROUPOFFSETMIN = $NTRAVELGROUPOFFSETMIN;
      return $this;
    }

}
