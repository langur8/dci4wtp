<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST3
{

    /**
     * @var ArrayOfD4WTPTARIFFLISTDAY3 $ACTTARIFFLISTDAY3
     */
    protected $ACTTARIFFLISTDAY3 = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLISTDAY3
     */
    public function getACTTARIFFLISTDAY3()
    {
      return $this->ACTTARIFFLISTDAY3;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLISTDAY3 $ACTTARIFFLISTDAY3
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST3
     */
    public function setACTTARIFFLISTDAY3($ACTTARIFFLISTDAY3)
    {
      $this->ACTTARIFFLISTDAY3 = $ACTTARIFFLISTDAY3;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST3
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
