<?php

namespace Axess\Dci4Wtp;

class getPackageTariffList7Response
{

    /**
     * @var D4WTPPKGTARIFFLIST7RESULT $getPackageTariffList7Result
     */
    protected $getPackageTariffList7Result = null;

    /**
     * @param D4WTPPKGTARIFFLIST7RESULT $getPackageTariffList7Result
     */
    public function __construct($getPackageTariffList7Result)
    {
      $this->getPackageTariffList7Result = $getPackageTariffList7Result;
    }

    /**
     * @return D4WTPPKGTARIFFLIST7RESULT
     */
    public function getGetPackageTariffList7Result()
    {
      return $this->getPackageTariffList7Result;
    }

    /**
     * @param D4WTPPKGTARIFFLIST7RESULT $getPackageTariffList7Result
     * @return \Axess\Dci4Wtp\getPackageTariffList7Response
     */
    public function setGetPackageTariffList7Result($getPackageTariffList7Result)
    {
      $this->getPackageTariffList7Result = $getPackageTariffList7Result;
      return $this;
    }

}
