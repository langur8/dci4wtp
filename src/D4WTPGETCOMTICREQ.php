<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCOMTICREQ
{

    /**
     * @var ArrayOfCOMPTICKET $ACTTICKET
     */
    protected $ACTTICKET = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfCOMPTICKET
     */
    public function getACTTICKET()
    {
      return $this->ACTTICKET;
    }

    /**
     * @param ArrayOfCOMPTICKET $ACTTICKET
     * @return \Axess\Dci4Wtp\D4WTPGETCOMTICREQ
     */
    public function setACTTICKET($ACTTICKET)
    {
      $this->ACTTICKET = $ACTTICKET;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETCOMTICREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
