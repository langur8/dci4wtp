<?php

namespace Axess\Dci4Wtp;

class rebookTransaction
{

    /**
     * @var D4WTPREBOOKTRANSREQ $i_ctRebookReq
     */
    protected $i_ctRebookReq = null;

    /**
     * @param D4WTPREBOOKTRANSREQ $i_ctRebookReq
     */
    public function __construct($i_ctRebookReq)
    {
      $this->i_ctRebookReq = $i_ctRebookReq;
    }

    /**
     * @return D4WTPREBOOKTRANSREQ
     */
    public function getI_ctRebookReq()
    {
      return $this->i_ctRebookReq;
    }

    /**
     * @param D4WTPREBOOKTRANSREQ $i_ctRebookReq
     * @return \Axess\Dci4Wtp\rebookTransaction
     */
    public function setI_ctRebookReq($i_ctRebookReq)
    {
      $this->i_ctRebookReq = $i_ctRebookReq;
      return $this;
    }

}
