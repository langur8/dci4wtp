<?php

namespace Axess\Dci4Wtp;

class D4WTPCREATEEXTORDERNRRESULT
{

    /**
     * @var ArrayOfD4WTPEXTORDERGROUPRESULT $ACTEXTORDERGROUPRESULT
     */
    protected $ACTEXTORDERGROUPRESULT = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPEXTORDERGROUPRESULT
     */
    public function getACTEXTORDERGROUPRESULT()
    {
      return $this->ACTEXTORDERGROUPRESULT;
    }

    /**
     * @param ArrayOfD4WTPEXTORDERGROUPRESULT $ACTEXTORDERGROUPRESULT
     * @return \Axess\Dci4Wtp\D4WTPCREATEEXTORDERNRRESULT
     */
    public function setACTEXTORDERGROUPRESULT($ACTEXTORDERGROUPRESULT)
    {
      $this->ACTEXTORDERGROUPRESULT = $ACTEXTORDERGROUPRESULT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCREATEEXTORDERNRRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCREATEEXTORDERNRRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
