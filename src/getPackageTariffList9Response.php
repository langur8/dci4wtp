<?php

namespace Axess\Dci4Wtp;

class getPackageTariffList9Response
{

    /**
     * @var D4WTPPKGTARIFFLIST10RESULT $getPackageTariffList9Result
     */
    protected $getPackageTariffList9Result = null;

    /**
     * @param D4WTPPKGTARIFFLIST10RESULT $getPackageTariffList9Result
     */
    public function __construct($getPackageTariffList9Result)
    {
      $this->getPackageTariffList9Result = $getPackageTariffList9Result;
    }

    /**
     * @return D4WTPPKGTARIFFLIST10RESULT
     */
    public function getGetPackageTariffList9Result()
    {
      return $this->getPackageTariffList9Result;
    }

    /**
     * @param D4WTPPKGTARIFFLIST10RESULT $getPackageTariffList9Result
     * @return \Axess\Dci4Wtp\getPackageTariffList9Response
     */
    public function setGetPackageTariffList9Result($getPackageTariffList9Result)
    {
      $this->getPackageTariffList9Result = $getPackageTariffList9Result;
      return $this;
    }

}
