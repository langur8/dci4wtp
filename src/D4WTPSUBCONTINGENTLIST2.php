<?php

namespace Axess\Dci4Wtp;

class D4WTPSUBCONTINGENTLIST2
{

    /**
     * @var float $NCOUNTFREE
     */
    protected $NCOUNTFREE = null;

    /**
     * @var float $NCOUNTRESERVED
     */
    protected $NCOUNTRESERVED = null;

    /**
     * @var float $NDURATIONINMINUTES
     */
    protected $NDURATIONINMINUTES = null;

    /**
     * @var float $NENTRYDURATIONNMIN
     */
    protected $NENTRYDURATIONNMIN = null;

    /**
     * @var float $NENTRYOFFSETINMIN
     */
    protected $NENTRYOFFSETINMIN = null;

    /**
     * @var float $NLOCKTIMEINMIN
     */
    protected $NLOCKTIMEINMIN = null;

    /**
     * @var float $NSUBCONTINGENTNR
     */
    protected $NSUBCONTINGENTNR = null;

    /**
     * @var string $SZSTARTTIME
     */
    protected $SZSTARTTIME = null;

    /**
     * @var string $SZSUBCONTINGENTMATCHCODE
     */
    protected $SZSUBCONTINGENTMATCHCODE = null;

    /**
     * @var string $SZSUBCONTINGENTNAME
     */
    protected $SZSUBCONTINGENTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCOUNTFREE()
    {
      return $this->NCOUNTFREE;
    }

    /**
     * @param float $NCOUNTFREE
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTLIST2
     */
    public function setNCOUNTFREE($NCOUNTFREE)
    {
      $this->NCOUNTFREE = $NCOUNTFREE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTRESERVED()
    {
      return $this->NCOUNTRESERVED;
    }

    /**
     * @param float $NCOUNTRESERVED
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTLIST2
     */
    public function setNCOUNTRESERVED($NCOUNTRESERVED)
    {
      $this->NCOUNTRESERVED = $NCOUNTRESERVED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDURATIONINMINUTES()
    {
      return $this->NDURATIONINMINUTES;
    }

    /**
     * @param float $NDURATIONINMINUTES
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTLIST2
     */
    public function setNDURATIONINMINUTES($NDURATIONINMINUTES)
    {
      $this->NDURATIONINMINUTES = $NDURATIONINMINUTES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNENTRYDURATIONNMIN()
    {
      return $this->NENTRYDURATIONNMIN;
    }

    /**
     * @param float $NENTRYDURATIONNMIN
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTLIST2
     */
    public function setNENTRYDURATIONNMIN($NENTRYDURATIONNMIN)
    {
      $this->NENTRYDURATIONNMIN = $NENTRYDURATIONNMIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getNENTRYOFFSETINMIN()
    {
      return $this->NENTRYOFFSETINMIN;
    }

    /**
     * @param float $NENTRYOFFSETINMIN
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTLIST2
     */
    public function setNENTRYOFFSETINMIN($NENTRYOFFSETINMIN)
    {
      $this->NENTRYOFFSETINMIN = $NENTRYOFFSETINMIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLOCKTIMEINMIN()
    {
      return $this->NLOCKTIMEINMIN;
    }

    /**
     * @param float $NLOCKTIMEINMIN
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTLIST2
     */
    public function setNLOCKTIMEINMIN($NLOCKTIMEINMIN)
    {
      $this->NLOCKTIMEINMIN = $NLOCKTIMEINMIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTINGENTNR()
    {
      return $this->NSUBCONTINGENTNR;
    }

    /**
     * @param float $NSUBCONTINGENTNR
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTLIST2
     */
    public function setNSUBCONTINGENTNR($NSUBCONTINGENTNR)
    {
      $this->NSUBCONTINGENTNR = $NSUBCONTINGENTNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTARTTIME()
    {
      return $this->SZSTARTTIME;
    }

    /**
     * @param string $SZSTARTTIME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTLIST2
     */
    public function setSZSTARTTIME($SZSTARTTIME)
    {
      $this->SZSTARTTIME = $SZSTARTTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBCONTINGENTMATCHCODE()
    {
      return $this->SZSUBCONTINGENTMATCHCODE;
    }

    /**
     * @param string $SZSUBCONTINGENTMATCHCODE
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTLIST2
     */
    public function setSZSUBCONTINGENTMATCHCODE($SZSUBCONTINGENTMATCHCODE)
    {
      $this->SZSUBCONTINGENTMATCHCODE = $SZSUBCONTINGENTMATCHCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBCONTINGENTNAME()
    {
      return $this->SZSUBCONTINGENTNAME;
    }

    /**
     * @param string $SZSUBCONTINGENTNAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTLIST2
     */
    public function setSZSUBCONTINGENTNAME($SZSUBCONTINGENTNAME)
    {
      $this->SZSUBCONTINGENTNAME = $SZSUBCONTINGENTNAME;
      return $this;
    }

}
