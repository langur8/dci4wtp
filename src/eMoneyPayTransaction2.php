<?php

namespace Axess\Dci4Wtp;

class eMoneyPayTransaction2
{

    /**
     * @var D4WTPEMONEYPAYTRANS2REQ $i_ctEmoneyPayTransactionReq
     */
    protected $i_ctEmoneyPayTransactionReq = null;

    /**
     * @param D4WTPEMONEYPAYTRANS2REQ $i_ctEmoneyPayTransactionReq
     */
    public function __construct($i_ctEmoneyPayTransactionReq)
    {
      $this->i_ctEmoneyPayTransactionReq = $i_ctEmoneyPayTransactionReq;
    }

    /**
     * @return D4WTPEMONEYPAYTRANS2REQ
     */
    public function getI_ctEmoneyPayTransactionReq()
    {
      return $this->i_ctEmoneyPayTransactionReq;
    }

    /**
     * @param D4WTPEMONEYPAYTRANS2REQ $i_ctEmoneyPayTransactionReq
     * @return \Axess\Dci4Wtp\eMoneyPayTransaction2
     */
    public function setI_ctEmoneyPayTransactionReq($i_ctEmoneyPayTransactionReq)
    {
      $this->i_ctEmoneyPayTransactionReq = $i_ctEmoneyPayTransactionReq;
      return $this;
    }

}
