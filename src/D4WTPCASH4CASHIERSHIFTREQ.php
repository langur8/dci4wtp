<?php

namespace Axess\Dci4Wtp;

class D4WTPCASH4CASHIERSHIFTREQ
{

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NSUM
     */
    protected $NSUM = null;

    /**
     * @var string $SZTRANSTEXT
     */
    protected $SZTRANSTEXT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCASH4CASHIERSHIFTREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUM()
    {
      return $this->NSUM;
    }

    /**
     * @param float $NSUM
     * @return \Axess\Dci4Wtp\D4WTPCASH4CASHIERSHIFTREQ
     */
    public function setNSUM($NSUM)
    {
      $this->NSUM = $NSUM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTRANSTEXT()
    {
      return $this->SZTRANSTEXT;
    }

    /**
     * @param string $SZTRANSTEXT
     * @return \Axess\Dci4Wtp\D4WTPCASH4CASHIERSHIFTREQ
     */
    public function setSZTRANSTEXT($SZTRANSTEXT)
    {
      $this->SZTRANSTEXT = $SZTRANSTEXT;
      return $this;
    }

}
