<?php

namespace Axess\Dci4Wtp;

class D4WTPCASHIERRIGHTS
{

    /**
     * @var float $NCMDNR
     */
    protected $NCMDNR = null;

    /**
     * @var float $NFUNCTIONNO
     */
    protected $NFUNCTIONNO = null;

    /**
     * @var float $NTYPENO
     */
    protected $NTYPENO = null;

    /**
     * @var string $SZFUNCTIONNAME
     */
    protected $SZFUNCTIONNAME = null;

    /**
     * @var string $SZINFO
     */
    protected $SZINFO = null;

    /**
     * @var string $SZTYPENAME
     */
    protected $SZTYPENAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCMDNR()
    {
      return $this->NCMDNR;
    }

    /**
     * @param float $NCMDNR
     * @return \Axess\Dci4Wtp\D4WTPCASHIERRIGHTS
     */
    public function setNCMDNR($NCMDNR)
    {
      $this->NCMDNR = $NCMDNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFUNCTIONNO()
    {
      return $this->NFUNCTIONNO;
    }

    /**
     * @param float $NFUNCTIONNO
     * @return \Axess\Dci4Wtp\D4WTPCASHIERRIGHTS
     */
    public function setNFUNCTIONNO($NFUNCTIONNO)
    {
      $this->NFUNCTIONNO = $NFUNCTIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTYPENO()
    {
      return $this->NTYPENO;
    }

    /**
     * @param float $NTYPENO
     * @return \Axess\Dci4Wtp\D4WTPCASHIERRIGHTS
     */
    public function setNTYPENO($NTYPENO)
    {
      $this->NTYPENO = $NTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFUNCTIONNAME()
    {
      return $this->SZFUNCTIONNAME;
    }

    /**
     * @param string $SZFUNCTIONNAME
     * @return \Axess\Dci4Wtp\D4WTPCASHIERRIGHTS
     */
    public function setSZFUNCTIONNAME($SZFUNCTIONNAME)
    {
      $this->SZFUNCTIONNAME = $SZFUNCTIONNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZINFO()
    {
      return $this->SZINFO;
    }

    /**
     * @param string $SZINFO
     * @return \Axess\Dci4Wtp\D4WTPCASHIERRIGHTS
     */
    public function setSZINFO($SZINFO)
    {
      $this->SZINFO = $SZINFO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTYPENAME()
    {
      return $this->SZTYPENAME;
    }

    /**
     * @param string $SZTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPCASHIERRIGHTS
     */
    public function setSZTYPENAME($SZTYPENAME)
    {
      $this->SZTYPENAME = $SZTYPENAME;
      return $this;
    }

}
