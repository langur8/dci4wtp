<?php

namespace Axess\Dci4Wtp;

class OBI4PSPKUNDENDATEN
{

    /**
     * @var base64Binary $BOBILD
     */
    protected $BOBILD = null;

    /**
     * @var OBI4PSPKMWNUTZERADRESSE $CTKMWNUTZERADRESSE
     */
    protected $CTKMWNUTZERADRESSE = null;

    /**
     * @var string $SZANREDE
     */
    protected $SZANREDE = null;

    /**
     * @var string $SZGEBURTSDATUM
     */
    protected $SZGEBURTSDATUM = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZTITEL
     */
    protected $SZTITEL = null;

    /**
     * @var string $SZVORNAME
     */
    protected $SZVORNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return base64Binary
     */
    public function getBOBILD()
    {
      return $this->BOBILD;
    }

    /**
     * @param base64Binary $BOBILD
     * @return \Axess\Dci4Wtp\OBI4PSPKUNDENDATEN
     */
    public function setBOBILD($BOBILD)
    {
      $this->BOBILD = $BOBILD;
      return $this;
    }

    /**
     * @return OBI4PSPKMWNUTZERADRESSE
     */
    public function getCTKMWNUTZERADRESSE()
    {
      return $this->CTKMWNUTZERADRESSE;
    }

    /**
     * @param OBI4PSPKMWNUTZERADRESSE $CTKMWNUTZERADRESSE
     * @return \Axess\Dci4Wtp\OBI4PSPKUNDENDATEN
     */
    public function setCTKMWNUTZERADRESSE($CTKMWNUTZERADRESSE)
    {
      $this->CTKMWNUTZERADRESSE = $CTKMWNUTZERADRESSE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZANREDE()
    {
      return $this->SZANREDE;
    }

    /**
     * @param string $SZANREDE
     * @return \Axess\Dci4Wtp\OBI4PSPKUNDENDATEN
     */
    public function setSZANREDE($SZANREDE)
    {
      $this->SZANREDE = $SZANREDE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGEBURTSDATUM()
    {
      return $this->SZGEBURTSDATUM;
    }

    /**
     * @param string $SZGEBURTSDATUM
     * @return \Axess\Dci4Wtp\OBI4PSPKUNDENDATEN
     */
    public function setSZGEBURTSDATUM($SZGEBURTSDATUM)
    {
      $this->SZGEBURTSDATUM = $SZGEBURTSDATUM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\OBI4PSPKUNDENDATEN
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTITEL()
    {
      return $this->SZTITEL;
    }

    /**
     * @param string $SZTITEL
     * @return \Axess\Dci4Wtp\OBI4PSPKUNDENDATEN
     */
    public function setSZTITEL($SZTITEL)
    {
      $this->SZTITEL = $SZTITEL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVORNAME()
    {
      return $this->SZVORNAME;
    }

    /**
     * @param string $SZVORNAME
     * @return \Axess\Dci4Wtp\OBI4PSPKUNDENDATEN
     */
    public function setSZVORNAME($SZVORNAME)
    {
      $this->SZVORNAME = $SZVORNAME;
      return $this;
    }

}
