<?php

namespace Axess\Dci4Wtp;

class getPackageContent3Response
{

    /**
     * @var D4WTPPACKAGECONTENT3RESULT $getPackageContent3Result
     */
    protected $getPackageContent3Result = null;

    /**
     * @param D4WTPPACKAGECONTENT3RESULT $getPackageContent3Result
     */
    public function __construct($getPackageContent3Result)
    {
      $this->getPackageContent3Result = $getPackageContent3Result;
    }

    /**
     * @return D4WTPPACKAGECONTENT3RESULT
     */
    public function getGetPackageContent3Result()
    {
      return $this->getPackageContent3Result;
    }

    /**
     * @param D4WTPPACKAGECONTENT3RESULT $getPackageContent3Result
     * @return \Axess\Dci4Wtp\getPackageContent3Response
     */
    public function setGetPackageContent3Result($getPackageContent3Result)
    {
      $this->getPackageContent3Result = $getPackageContent3Result;
      return $this;
    }

}
