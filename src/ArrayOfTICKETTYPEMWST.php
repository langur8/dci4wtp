<?php

namespace Axess\Dci4Wtp;

class ArrayOfTICKETTYPEMWST implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var TICKETTYPEMWST[] $TICKETTYPEMWST
     */
    protected $TICKETTYPEMWST = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return TICKETTYPEMWST[]
     */
    public function getTICKETTYPEMWST()
    {
      return $this->TICKETTYPEMWST;
    }

    /**
     * @param TICKETTYPEMWST[] $TICKETTYPEMWST
     * @return \Axess\Dci4Wtp\ArrayOfTICKETTYPEMWST
     */
    public function setTICKETTYPEMWST(array $TICKETTYPEMWST = null)
    {
      $this->TICKETTYPEMWST = $TICKETTYPEMWST;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->TICKETTYPEMWST[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return TICKETTYPEMWST
     */
    public function offsetGet($offset)
    {
      return $this->TICKETTYPEMWST[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param TICKETTYPEMWST $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->TICKETTYPEMWST[] = $value;
      } else {
        $this->TICKETTYPEMWST[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->TICKETTYPEMWST[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return TICKETTYPEMWST Return the current element
     */
    public function current()
    {
      return current($this->TICKETTYPEMWST);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->TICKETTYPEMWST);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->TICKETTYPEMWST);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->TICKETTYPEMWST);
    }

    /**
     * Countable implementation
     *
     * @return TICKETTYPEMWST Return count of elements
     */
    public function count()
    {
      return count($this->TICKETTYPEMWST);
    }

}
