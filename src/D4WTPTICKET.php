<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKET
{

    /**
     * @var float $BBLOCKTICKET
     */
    protected $BBLOCKTICKET = null;

    /**
     * @var float $BWITHOUTADDARTICLE
     */
    protected $BWITHOUTADDARTICLE = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NUNICODENO
     */
    protected $NUNICODENO = null;

    /**
     * @var float $NWTPMODE
     */
    protected $NWTPMODE = null;

    /**
     * @var string $SZBARCODE
     */
    protected $SZBARCODE = null;

    /**
     * @var string $SZVALIDDATE
     */
    protected $SZVALIDDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBBLOCKTICKET()
    {
      return $this->BBLOCKTICKET;
    }

    /**
     * @param float $BBLOCKTICKET
     * @return \Axess\Dci4Wtp\D4WTPTICKET
     */
    public function setBBLOCKTICKET($BBLOCKTICKET)
    {
      $this->BBLOCKTICKET = $BBLOCKTICKET;
      return $this;
    }

    /**
     * @return float
     */
    public function getBWITHOUTADDARTICLE()
    {
      return $this->BWITHOUTADDARTICLE;
    }

    /**
     * @param float $BWITHOUTADDARTICLE
     * @return \Axess\Dci4Wtp\D4WTPTICKET
     */
    public function setBWITHOUTADDARTICLE($BWITHOUTADDARTICLE)
    {
      $this->BWITHOUTADDARTICLE = $BWITHOUTADDARTICLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPTICKET
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTICKET
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTICKET
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPTICKET
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPTICKET
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENO()
    {
      return $this->NUNICODENO;
    }

    /**
     * @param float $NUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPTICKET
     */
    public function setNUNICODENO($NUNICODENO)
    {
      $this->NUNICODENO = $NUNICODENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPMODE()
    {
      return $this->NWTPMODE;
    }

    /**
     * @param float $NWTPMODE
     * @return \Axess\Dci4Wtp\D4WTPTICKET
     */
    public function setNWTPMODE($NWTPMODE)
    {
      $this->NWTPMODE = $NWTPMODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBARCODE()
    {
      return $this->SZBARCODE;
    }

    /**
     * @param string $SZBARCODE
     * @return \Axess\Dci4Wtp\D4WTPTICKET
     */
    public function setSZBARCODE($SZBARCODE)
    {
      $this->SZBARCODE = $SZBARCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDDATE()
    {
      return $this->SZVALIDDATE;
    }

    /**
     * @param string $SZVALIDDATE
     * @return \Axess\Dci4Wtp\D4WTPTICKET
     */
    public function setSZVALIDDATE($SZVALIDDATE)
    {
      $this->SZVALIDDATE = $SZVALIDDATE;
      return $this;
    }

}
