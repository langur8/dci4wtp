<?php

namespace Axess\Dci4Wtp;

class D4WTPLOGINNAMEDUSERREQUEST
{

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZPASSWORD
     */
    protected $SZPASSWORD = null;

    /**
     * @var string $SZUSERNAME
     */
    protected $SZUSERNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPLOGINNAMEDUSERREQUEST
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPLOGINNAMEDUSERREQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPLOGINNAMEDUSERREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPLOGINNAMEDUSERREQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPASSWORD()
    {
      return $this->SZPASSWORD;
    }

    /**
     * @param string $SZPASSWORD
     * @return \Axess\Dci4Wtp\D4WTPLOGINNAMEDUSERREQUEST
     */
    public function setSZPASSWORD($SZPASSWORD)
    {
      $this->SZPASSWORD = $SZPASSWORD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUSERNAME()
    {
      return $this->SZUSERNAME;
    }

    /**
     * @param string $SZUSERNAME
     * @return \Axess\Dci4Wtp\D4WTPLOGINNAMEDUSERREQUEST
     */
    public function setSZUSERNAME($SZUSERNAME)
    {
      $this->SZUSERNAME = $SZUSERNAME;
      return $this;
    }

}
