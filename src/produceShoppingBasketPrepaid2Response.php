<?php

namespace Axess\Dci4Wtp;

class produceShoppingBasketPrepaid2Response
{

    /**
     * @var D4WTPPRODSHOPPOSPREPAIDRES $produceShoppingBasketPrepaid2Result
     */
    protected $produceShoppingBasketPrepaid2Result = null;

    /**
     * @param D4WTPPRODSHOPPOSPREPAIDRES $produceShoppingBasketPrepaid2Result
     */
    public function __construct($produceShoppingBasketPrepaid2Result)
    {
      $this->produceShoppingBasketPrepaid2Result = $produceShoppingBasketPrepaid2Result;
    }

    /**
     * @return D4WTPPRODSHOPPOSPREPAIDRES
     */
    public function getProduceShoppingBasketPrepaid2Result()
    {
      return $this->produceShoppingBasketPrepaid2Result;
    }

    /**
     * @param D4WTPPRODSHOPPOSPREPAIDRES $produceShoppingBasketPrepaid2Result
     * @return \Axess\Dci4Wtp\produceShoppingBasketPrepaid2Response
     */
    public function setProduceShoppingBasketPrepaid2Result($produceShoppingBasketPrepaid2Result)
    {
      $this->produceShoppingBasketPrepaid2Result = $produceShoppingBasketPrepaid2Result;
      return $this;
    }

}
