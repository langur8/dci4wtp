<?php

namespace Axess\Dci4Wtp;

class getTravelGroupList2
{

    /**
     * @var D4WTPGETTRAVELGRPLISTREQUEST $i_ctGetTravelGroupListReq
     */
    protected $i_ctGetTravelGroupListReq = null;

    /**
     * @param D4WTPGETTRAVELGRPLISTREQUEST $i_ctGetTravelGroupListReq
     */
    public function __construct($i_ctGetTravelGroupListReq)
    {
      $this->i_ctGetTravelGroupListReq = $i_ctGetTravelGroupListReq;
    }

    /**
     * @return D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function getI_ctGetTravelGroupListReq()
    {
      return $this->i_ctGetTravelGroupListReq;
    }

    /**
     * @param D4WTPGETTRAVELGRPLISTREQUEST $i_ctGetTravelGroupListReq
     * @return \Axess\Dci4Wtp\getTravelGroupList2
     */
    public function setI_ctGetTravelGroupListReq($i_ctGetTravelGroupListReq)
    {
      $this->i_ctGetTravelGroupListReq = $i_ctGetTravelGroupListReq;
      return $this;
    }

}
