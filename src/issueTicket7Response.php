<?php

namespace Axess\Dci4Wtp;

class issueTicket7Response
{

    /**
     * @var D4WTPISSUETICKET5RESULT $issueTicket7Result
     */
    protected $issueTicket7Result = null;

    /**
     * @param D4WTPISSUETICKET5RESULT $issueTicket7Result
     */
    public function __construct($issueTicket7Result)
    {
      $this->issueTicket7Result = $issueTicket7Result;
    }

    /**
     * @return D4WTPISSUETICKET5RESULT
     */
    public function getIssueTicket7Result()
    {
      return $this->issueTicket7Result;
    }

    /**
     * @param D4WTPISSUETICKET5RESULT $issueTicket7Result
     * @return \Axess\Dci4Wtp\issueTicket7Response
     */
    public function setIssueTicket7Result($issueTicket7Result)
    {
      $this->issueTicket7Result = $issueTicket7Result;
      return $this;
    }

}
