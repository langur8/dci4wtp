<?php

namespace Axess\Dci4Wtp;

class D4WTPMNGPERSPHOTORESULT
{

    /**
     * @var base64Binary $BLPHOTODATA
     */
    protected $BLPHOTODATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPERSKASSANR
     */
    protected $NPERSKASSANR = null;

    /**
     * @var float $NPERSNR
     */
    protected $NPERSNR = null;

    /**
     * @var float $NPERSPROJNR
     */
    protected $NPERSPROJNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return base64Binary
     */
    public function getBLPHOTODATA()
    {
      return $this->BLPHOTODATA;
    }

    /**
     * @param base64Binary $BLPHOTODATA
     * @return \Axess\Dci4Wtp\D4WTPMNGPERSPHOTORESULT
     */
    public function setBLPHOTODATA($BLPHOTODATA)
    {
      $this->BLPHOTODATA = $BLPHOTODATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPMNGPERSPHOTORESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSKASSANR()
    {
      return $this->NPERSKASSANR;
    }

    /**
     * @param float $NPERSKASSANR
     * @return \Axess\Dci4Wtp\D4WTPMNGPERSPHOTORESULT
     */
    public function setNPERSKASSANR($NPERSKASSANR)
    {
      $this->NPERSKASSANR = $NPERSKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNR()
    {
      return $this->NPERSNR;
    }

    /**
     * @param float $NPERSNR
     * @return \Axess\Dci4Wtp\D4WTPMNGPERSPHOTORESULT
     */
    public function setNPERSNR($NPERSNR)
    {
      $this->NPERSNR = $NPERSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNR()
    {
      return $this->NPERSPROJNR;
    }

    /**
     * @param float $NPERSPROJNR
     * @return \Axess\Dci4Wtp\D4WTPMNGPERSPHOTORESULT
     */
    public function setNPERSPROJNR($NPERSPROJNR)
    {
      $this->NPERSPROJNR = $NPERSPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPMNGPERSPHOTORESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
