<?php

namespace Axess\Dci4Wtp;

class getPackageTariffList3Response
{

    /**
     * @var D4WTPPKGTARIFFLIST3RESULT $getPackageTariffList3Result
     */
    protected $getPackageTariffList3Result = null;

    /**
     * @param D4WTPPKGTARIFFLIST3RESULT $getPackageTariffList3Result
     */
    public function __construct($getPackageTariffList3Result)
    {
      $this->getPackageTariffList3Result = $getPackageTariffList3Result;
    }

    /**
     * @return D4WTPPKGTARIFFLIST3RESULT
     */
    public function getGetPackageTariffList3Result()
    {
      return $this->getPackageTariffList3Result;
    }

    /**
     * @param D4WTPPKGTARIFFLIST3RESULT $getPackageTariffList3Result
     * @return \Axess\Dci4Wtp\getPackageTariffList3Response
     */
    public function setGetPackageTariffList3Result($getPackageTariffList3Result)
    {
      $this->getPackageTariffList3Result = $getPackageTariffList3Result;
      return $this;
    }

}
