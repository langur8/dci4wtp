<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPNAMEDUSERS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPNAMEDUSERS[] $D4WTPNAMEDUSERS
     */
    protected $D4WTPNAMEDUSERS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPNAMEDUSERS[]
     */
    public function getD4WTPNAMEDUSERS()
    {
      return $this->D4WTPNAMEDUSERS;
    }

    /**
     * @param D4WTPNAMEDUSERS[] $D4WTPNAMEDUSERS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPNAMEDUSERS
     */
    public function setD4WTPNAMEDUSERS(array $D4WTPNAMEDUSERS = null)
    {
      $this->D4WTPNAMEDUSERS = $D4WTPNAMEDUSERS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPNAMEDUSERS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPNAMEDUSERS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPNAMEDUSERS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPNAMEDUSERS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPNAMEDUSERS[] = $value;
      } else {
        $this->D4WTPNAMEDUSERS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPNAMEDUSERS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPNAMEDUSERS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPNAMEDUSERS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPNAMEDUSERS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPNAMEDUSERS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPNAMEDUSERS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPNAMEDUSERS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPNAMEDUSERS);
    }

}
