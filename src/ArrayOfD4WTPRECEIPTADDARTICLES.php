<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPRECEIPTADDARTICLES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPRECEIPTADDARTICLES[] $D4WTPRECEIPTADDARTICLES
     */
    protected $D4WTPRECEIPTADDARTICLES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPRECEIPTADDARTICLES[]
     */
    public function getD4WTPRECEIPTADDARTICLES()
    {
      return $this->D4WTPRECEIPTADDARTICLES;
    }

    /**
     * @param D4WTPRECEIPTADDARTICLES[] $D4WTPRECEIPTADDARTICLES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPRECEIPTADDARTICLES
     */
    public function setD4WTPRECEIPTADDARTICLES(array $D4WTPRECEIPTADDARTICLES = null)
    {
      $this->D4WTPRECEIPTADDARTICLES = $D4WTPRECEIPTADDARTICLES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPRECEIPTADDARTICLES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPRECEIPTADDARTICLES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPRECEIPTADDARTICLES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPRECEIPTADDARTICLES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPRECEIPTADDARTICLES[] = $value;
      } else {
        $this->D4WTPRECEIPTADDARTICLES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPRECEIPTADDARTICLES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPRECEIPTADDARTICLES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPRECEIPTADDARTICLES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPRECEIPTADDARTICLES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPRECEIPTADDARTICLES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPRECEIPTADDARTICLES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPRECEIPTADDARTICLES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPRECEIPTADDARTICLES);
    }

}
