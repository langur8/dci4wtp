<?php

namespace Axess\Dci4Wtp;

class D4WTPSUBCONTINGENTRES
{

    /**
     * @var ArrayOfD4WTPTRAVELGROUPRES $ACTTRAVELGROUPRES
     */
    protected $ACTTRAVELGROUPRES = null;

    /**
     * @var float $NSUBCONTINGENTNO
     */
    protected $NSUBCONTINGENTNO = null;

    /**
     * @var float $NTICKETCOUNTFREE
     */
    protected $NTICKETCOUNTFREE = null;

    /**
     * @var float $NTICKETCOUNTRESERVED
     */
    protected $NTICKETCOUNTRESERVED = null;

    /**
     * @var float $NTICKETCOUNTTOTAL
     */
    protected $NTICKETCOUNTTOTAL = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSHORTNAME
     */
    protected $SZSHORTNAME = null;

    /**
     * @var string $SZSTARTTIME
     */
    protected $SZSTARTTIME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTRAVELGROUPRES
     */
    public function getACTTRAVELGROUPRES()
    {
      return $this->ACTTRAVELGROUPRES;
    }

    /**
     * @param ArrayOfD4WTPTRAVELGROUPRES $ACTTRAVELGROUPRES
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES
     */
    public function setACTTRAVELGROUPRES($ACTTRAVELGROUPRES)
    {
      $this->ACTTRAVELGROUPRES = $ACTTRAVELGROUPRES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTINGENTNO()
    {
      return $this->NSUBCONTINGENTNO;
    }

    /**
     * @param float $NSUBCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES
     */
    public function setNSUBCONTINGENTNO($NSUBCONTINGENTNO)
    {
      $this->NSUBCONTINGENTNO = $NSUBCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETCOUNTFREE()
    {
      return $this->NTICKETCOUNTFREE;
    }

    /**
     * @param float $NTICKETCOUNTFREE
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES
     */
    public function setNTICKETCOUNTFREE($NTICKETCOUNTFREE)
    {
      $this->NTICKETCOUNTFREE = $NTICKETCOUNTFREE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETCOUNTRESERVED()
    {
      return $this->NTICKETCOUNTRESERVED;
    }

    /**
     * @param float $NTICKETCOUNTRESERVED
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES
     */
    public function setNTICKETCOUNTRESERVED($NTICKETCOUNTRESERVED)
    {
      $this->NTICKETCOUNTRESERVED = $NTICKETCOUNTRESERVED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETCOUNTTOTAL()
    {
      return $this->NTICKETCOUNTTOTAL;
    }

    /**
     * @param float $NTICKETCOUNTTOTAL
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES
     */
    public function setNTICKETCOUNTTOTAL($NTICKETCOUNTTOTAL)
    {
      $this->NTICKETCOUNTTOTAL = $NTICKETCOUNTTOTAL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHORTNAME()
    {
      return $this->SZSHORTNAME;
    }

    /**
     * @param string $SZSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES
     */
    public function setSZSHORTNAME($SZSHORTNAME)
    {
      $this->SZSHORTNAME = $SZSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTARTTIME()
    {
      return $this->SZSTARTTIME;
    }

    /**
     * @param string $SZSTARTTIME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES
     */
    public function setSZSTARTTIME($SZSTARTTIME)
    {
      $this->SZSTARTTIME = $SZSTARTTIME;
      return $this;
    }

}
