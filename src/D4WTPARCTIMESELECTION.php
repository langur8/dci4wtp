<?php

namespace Axess\Dci4Wtp;

class D4WTPARCTIMESELECTION
{

    /**
     * @var float $BCHECKAVAILCOUNT
     */
    protected $BCHECKAVAILCOUNT = null;

    /**
     * @var D4WTPDURATIONINFO $CTDURATIONINFO
     */
    protected $CTDURATIONINFO = null;

    /**
     * @var float $NARCNR
     */
    protected $NARCNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBCHECKAVAILCOUNT()
    {
      return $this->BCHECKAVAILCOUNT;
    }

    /**
     * @param float $BCHECKAVAILCOUNT
     * @return \Axess\Dci4Wtp\D4WTPARCTIMESELECTION
     */
    public function setBCHECKAVAILCOUNT($BCHECKAVAILCOUNT)
    {
      $this->BCHECKAVAILCOUNT = $BCHECKAVAILCOUNT;
      return $this;
    }

    /**
     * @return D4WTPDURATIONINFO
     */
    public function getCTDURATIONINFO()
    {
      return $this->CTDURATIONINFO;
    }

    /**
     * @param D4WTPDURATIONINFO $CTDURATIONINFO
     * @return \Axess\Dci4Wtp\D4WTPARCTIMESELECTION
     */
    public function setCTDURATIONINFO($CTDURATIONINFO)
    {
      $this->CTDURATIONINFO = $CTDURATIONINFO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARCNR()
    {
      return $this->NARCNR;
    }

    /**
     * @param float $NARCNR
     * @return \Axess\Dci4Wtp\D4WTPARCTIMESELECTION
     */
    public function setNARCNR($NARCNR)
    {
      $this->NARCNR = $NARCNR;
      return $this;
    }

}
