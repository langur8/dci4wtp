<?php

namespace Axess\Dci4Wtp;

class D4WTPCOMPANYRESULT
{

    /**
     * @var D4WTPCOMPANYWTPFIELDS $COMPANYWTPFIELDS
     */
    protected $COMPANYWTPFIELDS = null;

    /**
     * @var ArrayOfCOMPANYCONTACTPERSON $COMPCONTACTPERSON
     */
    protected $COMPCONTACTPERSON = null;

    /**
     * @var D4WTPCOMPEXHIBITION $COMPEXHIBITION
     */
    protected $COMPEXHIBITION = null;

    /**
     * @var float $NCOMPANYNR
     */
    protected $NCOMPANYNR = null;

    /**
     * @var float $NCOMPANYPOSNR
     */
    protected $NCOMPANYPOSNR = null;

    /**
     * @var float $NCOMPANYPROJNR
     */
    protected $NCOMPANYPROJNR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var ArrayOfSPECIALADDRESS $SPECIALADDRESS
     */
    protected $SPECIALADDRESS = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var D4WTPCOMPANY $WTPCOMPANY
     */
    protected $WTPCOMPANY = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCOMPANYWTPFIELDS
     */
    public function getCOMPANYWTPFIELDS()
    {
      return $this->COMPANYWTPFIELDS;
    }

    /**
     * @param D4WTPCOMPANYWTPFIELDS $COMPANYWTPFIELDS
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYRESULT
     */
    public function setCOMPANYWTPFIELDS($COMPANYWTPFIELDS)
    {
      $this->COMPANYWTPFIELDS = $COMPANYWTPFIELDS;
      return $this;
    }

    /**
     * @return ArrayOfCOMPANYCONTACTPERSON
     */
    public function getCOMPCONTACTPERSON()
    {
      return $this->COMPCONTACTPERSON;
    }

    /**
     * @param ArrayOfCOMPANYCONTACTPERSON $COMPCONTACTPERSON
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYRESULT
     */
    public function setCOMPCONTACTPERSON($COMPCONTACTPERSON)
    {
      $this->COMPCONTACTPERSON = $COMPCONTACTPERSON;
      return $this;
    }

    /**
     * @return D4WTPCOMPEXHIBITION
     */
    public function getCOMPEXHIBITION()
    {
      return $this->COMPEXHIBITION;
    }

    /**
     * @param D4WTPCOMPEXHIBITION $COMPEXHIBITION
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYRESULT
     */
    public function setCOMPEXHIBITION($COMPEXHIBITION)
    {
      $this->COMPEXHIBITION = $COMPEXHIBITION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNR()
    {
      return $this->NCOMPANYNR;
    }

    /**
     * @param float $NCOMPANYNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYRESULT
     */
    public function setNCOMPANYNR($NCOMPANYNR)
    {
      $this->NCOMPANYNR = $NCOMPANYNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNR()
    {
      return $this->NCOMPANYPOSNR;
    }

    /**
     * @param float $NCOMPANYPOSNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYRESULT
     */
    public function setNCOMPANYPOSNR($NCOMPANYPOSNR)
    {
      $this->NCOMPANYPOSNR = $NCOMPANYPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNR()
    {
      return $this->NCOMPANYPROJNR;
    }

    /**
     * @param float $NCOMPANYPROJNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYRESULT
     */
    public function setNCOMPANYPROJNR($NCOMPANYPROJNR)
    {
      $this->NCOMPANYPROJNR = $NCOMPANYPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return ArrayOfSPECIALADDRESS
     */
    public function getSPECIALADDRESS()
    {
      return $this->SPECIALADDRESS;
    }

    /**
     * @param ArrayOfSPECIALADDRESS $SPECIALADDRESS
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYRESULT
     */
    public function setSPECIALADDRESS($SPECIALADDRESS)
    {
      $this->SPECIALADDRESS = $SPECIALADDRESS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return D4WTPCOMPANY
     */
    public function getWTPCOMPANY()
    {
      return $this->WTPCOMPANY;
    }

    /**
     * @param D4WTPCOMPANY $WTPCOMPANY
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYRESULT
     */
    public function setWTPCOMPANY($WTPCOMPANY)
    {
      $this->WTPCOMPANY = $WTPCOMPANY;
      return $this;
    }

}
