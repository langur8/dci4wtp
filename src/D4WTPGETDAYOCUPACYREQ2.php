<?php

namespace Axess\Dci4Wtp;

class D4WTPGETDAYOCUPACYREQ2
{

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NCONTINGENTNO
     */
    protected $NCONTINGENTNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var ArrayOfD4WTPPOSLIST $POSLIST
     */
    protected $POSLIST = null;

    /**
     * @var string $SZDATE
     */
    protected $SZDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPGETDAYOCUPACYREQ2
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETDAYOCUPACYREQ2
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETDAYOCUPACYREQ2
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNO()
    {
      return $this->NCONTINGENTNO;
    }

    /**
     * @param float $NCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPGETDAYOCUPACYREQ2
     */
    public function setNCONTINGENTNO($NCONTINGENTNO)
    {
      $this->NCONTINGENTNO = $NCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETDAYOCUPACYREQ2
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETDAYOCUPACYREQ2
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPPOSLIST
     */
    public function getPOSLIST()
    {
      return $this->POSLIST;
    }

    /**
     * @param ArrayOfD4WTPPOSLIST $POSLIST
     * @return \Axess\Dci4Wtp\D4WTPGETDAYOCUPACYREQ2
     */
    public function setPOSLIST($POSLIST)
    {
      $this->POSLIST = $POSLIST;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATE()
    {
      return $this->SZDATE;
    }

    /**
     * @param string $SZDATE
     * @return \Axess\Dci4Wtp\D4WTPGETDAYOCUPACYREQ2
     */
    public function setSZDATE($SZDATE)
    {
      $this->SZDATE = $SZDATE;
      return $this;
    }

}
