<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST6
{

    /**
     * @var ArrayOfD4WTPTARIFFLISTDAY6 $ACTTARIFFLISTDAY6
     */
    protected $ACTTARIFFLISTDAY6 = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLISTDAY6
     */
    public function getACTTARIFFLISTDAY6()
    {
      return $this->ACTTARIFFLISTDAY6;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLISTDAY6 $ACTTARIFFLISTDAY6
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST6
     */
    public function setACTTARIFFLISTDAY6($ACTTARIFFLISTDAY6)
    {
      $this->ACTTARIFFLISTDAY6 = $ACTTARIFFLISTDAY6;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST6
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
