<?php

namespace Axess\Dci4Wtp;

class D4WTPISTESTMODEENABLEDRESULT
{

    /**
     * @var float $BTESTMODEVALUE
     */
    protected $BTESTMODEVALUE = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZTESTMODEMESSAGE
     */
    protected $SZTESTMODEMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBTESTMODEVALUE()
    {
      return $this->BTESTMODEVALUE;
    }

    /**
     * @param float $BTESTMODEVALUE
     * @return \Axess\Dci4Wtp\D4WTPISTESTMODEENABLEDRESULT
     */
    public function setBTESTMODEVALUE($BTESTMODEVALUE)
    {
      $this->BTESTMODEVALUE = $BTESTMODEVALUE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPISTESTMODEENABLEDRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPISTESTMODEENABLEDRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTESTMODEMESSAGE()
    {
      return $this->SZTESTMODEMESSAGE;
    }

    /**
     * @param string $SZTESTMODEMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPISTESTMODEENABLEDRESULT
     */
    public function setSZTESTMODEMESSAGE($SZTESTMODEMESSAGE)
    {
      $this->SZTESTMODEMESSAGE = $SZTESTMODEMESSAGE;
      return $this;
    }

}
