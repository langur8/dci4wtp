<?php

namespace Axess\Dci4Wtp;

class D4WTPPRODUCTORDER10
{

    /**
     * @var ArrayOfD4WTPADDARTICLE $ACTADDARTICLES
     */
    protected $ACTADDARTICLES = null;

    /**
     * @var ArrayOfD4WTPDCCONTENT $ACTDCCONTENT
     */
    protected $ACTDCCONTENT = null;

    /**
     * @var ArrayOfD4WTPPARKING $ACTPARKING
     */
    protected $ACTPARKING = null;

    /**
     * @var ArrayOfD4WTPERSONDATA4 $ACTPERSON4
     */
    protected $ACTPERSON4 = null;

    /**
     * @var ArrayOfD4WTPSUBCONTINGENT $ACTSUBCONTINGENT
     */
    protected $ACTSUBCONTINGENT = null;

    /**
     * @var ArrayOfD4WTPWTPNO $ACTWTPNO
     */
    protected $ACTWTPNO = null;

    /**
     * @var float $BCREATEPHOTO
     */
    protected $BCREATEPHOTO = null;

    /**
     * @var float $BPERSONAL
     */
    protected $BPERSONAL = null;

    /**
     * @var float $BPREPAIDTICKET
     */
    protected $BPREPAIDTICKET = null;

    /**
     * @var D4WTPPACKAGEPOSPRODUCT $CTPACKAGEPRODUCT
     */
    protected $CTPACKAGEPRODUCT = null;

    /**
     * @var D4WTPPRODUCT $CTPRODUCT
     */
    protected $CTPRODUCT = null;

    /**
     * @var D4WTPRENTALPRODUCTORDER3 $CTRENTALPRODUCTORDER
     */
    protected $CTRENTALPRODUCTORDER = null;

    /**
     * @var float $NAPPTRANSLOGID
     */
    protected $NAPPTRANSLOGID = null;

    /**
     * @var float $NARCNR
     */
    protected $NARCNR = null;

    /**
     * @var float $NDATACARRIERTYPENO
     */
    protected $NDATACARRIERTYPENO = null;

    /**
     * @var float $NDISCOUNTNR
     */
    protected $NDISCOUNTNR = null;

    /**
     * @var float $NDISCOUNTSCALE
     */
    protected $NDISCOUNTSCALE = null;

    /**
     * @var float $NGROUPNUMBEROFFREEPERSONS
     */
    protected $NGROUPNUMBEROFFREEPERSONS = null;

    /**
     * @var float $NGROUPNUMBEROFPERSONS
     */
    protected $NGROUPNUMBEROFPERSONS = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NTRAVELGROUPNO
     */
    protected $NTRAVELGROUPNO = null;

    /**
     * @var float $NTRAVELGROUPPOSNO
     */
    protected $NTRAVELGROUPPOSNO = null;

    /**
     * @var float $NTRAVELGROUPPROJNO
     */
    protected $NTRAVELGROUPPROJNO = null;

    /**
     * @var string $SZCODINGMODE
     */
    protected $SZCODINGMODE = null;

    /**
     * @var string $SZPROMOCODE
     */
    protected $SZPROMOCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDARTICLE
     */
    public function getACTADDARTICLES()
    {
      return $this->ACTADDARTICLES;
    }

    /**
     * @param ArrayOfD4WTPADDARTICLE $ACTADDARTICLES
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setACTADDARTICLES($ACTADDARTICLES)
    {
      $this->ACTADDARTICLES = $ACTADDARTICLES;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPDCCONTENT
     */
    public function getACTDCCONTENT()
    {
      return $this->ACTDCCONTENT;
    }

    /**
     * @param ArrayOfD4WTPDCCONTENT $ACTDCCONTENT
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setACTDCCONTENT($ACTDCCONTENT)
    {
      $this->ACTDCCONTENT = $ACTDCCONTENT;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPPARKING
     */
    public function getACTPARKING()
    {
      return $this->ACTPARKING;
    }

    /**
     * @param ArrayOfD4WTPPARKING $ACTPARKING
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setACTPARKING($ACTPARKING)
    {
      $this->ACTPARKING = $ACTPARKING;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPERSONDATA4
     */
    public function getACTPERSON4()
    {
      return $this->ACTPERSON4;
    }

    /**
     * @param ArrayOfD4WTPERSONDATA4 $ACTPERSON4
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setACTPERSON4($ACTPERSON4)
    {
      $this->ACTPERSON4 = $ACTPERSON4;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPSUBCONTINGENT
     */
    public function getACTSUBCONTINGENT()
    {
      return $this->ACTSUBCONTINGENT;
    }

    /**
     * @param ArrayOfD4WTPSUBCONTINGENT $ACTSUBCONTINGENT
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setACTSUBCONTINGENT($ACTSUBCONTINGENT)
    {
      $this->ACTSUBCONTINGENT = $ACTSUBCONTINGENT;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPWTPNO
     */
    public function getACTWTPNO()
    {
      return $this->ACTWTPNO;
    }

    /**
     * @param ArrayOfD4WTPWTPNO $ACTWTPNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setACTWTPNO($ACTWTPNO)
    {
      $this->ACTWTPNO = $ACTWTPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getBCREATEPHOTO()
    {
      return $this->BCREATEPHOTO;
    }

    /**
     * @param float $BCREATEPHOTO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setBCREATEPHOTO($BCREATEPHOTO)
    {
      $this->BCREATEPHOTO = $BCREATEPHOTO;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPERSONAL()
    {
      return $this->BPERSONAL;
    }

    /**
     * @param float $BPERSONAL
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setBPERSONAL($BPERSONAL)
    {
      $this->BPERSONAL = $BPERSONAL;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPREPAIDTICKET()
    {
      return $this->BPREPAIDTICKET;
    }

    /**
     * @param float $BPREPAIDTICKET
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setBPREPAIDTICKET($BPREPAIDTICKET)
    {
      $this->BPREPAIDTICKET = $BPREPAIDTICKET;
      return $this;
    }

    /**
     * @return D4WTPPACKAGEPOSPRODUCT
     */
    public function getCTPACKAGEPRODUCT()
    {
      return $this->CTPACKAGEPRODUCT;
    }

    /**
     * @param D4WTPPACKAGEPOSPRODUCT $CTPACKAGEPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setCTPACKAGEPRODUCT($CTPACKAGEPRODUCT)
    {
      $this->CTPACKAGEPRODUCT = $CTPACKAGEPRODUCT;
      return $this;
    }

    /**
     * @return D4WTPPRODUCT
     */
    public function getCTPRODUCT()
    {
      return $this->CTPRODUCT;
    }

    /**
     * @param D4WTPPRODUCT $CTPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setCTPRODUCT($CTPRODUCT)
    {
      $this->CTPRODUCT = $CTPRODUCT;
      return $this;
    }

    /**
     * @return D4WTPRENTALPRODUCTORDER3
     */
    public function getCTRENTALPRODUCTORDER()
    {
      return $this->CTRENTALPRODUCTORDER;
    }

    /**
     * @param D4WTPRENTALPRODUCTORDER3 $CTRENTALPRODUCTORDER
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setCTRENTALPRODUCTORDER($CTRENTALPRODUCTORDER)
    {
      $this->CTRENTALPRODUCTORDER = $CTRENTALPRODUCTORDER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAPPTRANSLOGID()
    {
      return $this->NAPPTRANSLOGID;
    }

    /**
     * @param float $NAPPTRANSLOGID
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setNAPPTRANSLOGID($NAPPTRANSLOGID)
    {
      $this->NAPPTRANSLOGID = $NAPPTRANSLOGID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARCNR()
    {
      return $this->NARCNR;
    }

    /**
     * @param float $NARCNR
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setNARCNR($NARCNR)
    {
      $this->NARCNR = $NARCNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATACARRIERTYPENO()
    {
      return $this->NDATACARRIERTYPENO;
    }

    /**
     * @param float $NDATACARRIERTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setNDATACARRIERTYPENO($NDATACARRIERTYPENO)
    {
      $this->NDATACARRIERTYPENO = $NDATACARRIERTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDISCOUNTNR()
    {
      return $this->NDISCOUNTNR;
    }

    /**
     * @param float $NDISCOUNTNR
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setNDISCOUNTNR($NDISCOUNTNR)
    {
      $this->NDISCOUNTNR = $NDISCOUNTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDISCOUNTSCALE()
    {
      return $this->NDISCOUNTSCALE;
    }

    /**
     * @param float $NDISCOUNTSCALE
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setNDISCOUNTSCALE($NDISCOUNTSCALE)
    {
      $this->NDISCOUNTSCALE = $NDISCOUNTSCALE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNGROUPNUMBEROFFREEPERSONS()
    {
      return $this->NGROUPNUMBEROFFREEPERSONS;
    }

    /**
     * @param float $NGROUPNUMBEROFFREEPERSONS
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setNGROUPNUMBEROFFREEPERSONS($NGROUPNUMBEROFFREEPERSONS)
    {
      $this->NGROUPNUMBEROFFREEPERSONS = $NGROUPNUMBEROFFREEPERSONS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNGROUPNUMBEROFPERSONS()
    {
      return $this->NGROUPNUMBEROFPERSONS;
    }

    /**
     * @param float $NGROUPNUMBEROFPERSONS
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setNGROUPNUMBEROFPERSONS($NGROUPNUMBEROFPERSONS)
    {
      $this->NGROUPNUMBEROFPERSONS = $NGROUPNUMBEROFPERSONS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPNO()
    {
      return $this->NTRAVELGROUPNO;
    }

    /**
     * @param float $NTRAVELGROUPNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setNTRAVELGROUPNO($NTRAVELGROUPNO)
    {
      $this->NTRAVELGROUPNO = $NTRAVELGROUPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPOSNO()
    {
      return $this->NTRAVELGROUPPOSNO;
    }

    /**
     * @param float $NTRAVELGROUPPOSNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setNTRAVELGROUPPOSNO($NTRAVELGROUPPOSNO)
    {
      $this->NTRAVELGROUPPOSNO = $NTRAVELGROUPPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPROJNO()
    {
      return $this->NTRAVELGROUPPROJNO;
    }

    /**
     * @param float $NTRAVELGROUPPROJNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setNTRAVELGROUPPROJNO($NTRAVELGROUPPROJNO)
    {
      $this->NTRAVELGROUPPROJNO = $NTRAVELGROUPPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGMODE()
    {
      return $this->SZCODINGMODE;
    }

    /**
     * @param string $SZCODINGMODE
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setSZCODINGMODE($SZCODINGMODE)
    {
      $this->SZCODINGMODE = $SZCODINGMODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPROMOCODE()
    {
      return $this->SZPROMOCODE;
    }

    /**
     * @param string $SZPROMOCODE
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER10
     */
    public function setSZPROMOCODE($SZPROMOCODE)
    {
      $this->SZPROMOCODE = $SZPROMOCODE;
      return $this;
    }

}
