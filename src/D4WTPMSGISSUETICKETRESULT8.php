<?php

namespace Axess\Dci4Wtp;

class D4WTPMSGISSUETICKETRESULT8
{

    /**
     * @var ArrayOfD4WTPMSGTICKETRESDATA3 $ACTMSGTICKETRESDATA
     */
    protected $ACTMSGTICKETRESDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPMSGTICKETRESDATA3
     */
    public function getACTMSGTICKETRESDATA()
    {
      return $this->ACTMSGTICKETRESDATA;
    }

    /**
     * @param ArrayOfD4WTPMSGTICKETRESDATA3 $ACTMSGTICKETRESDATA
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETRESULT8
     */
    public function setACTMSGTICKETRESDATA($ACTMSGTICKETRESDATA)
    {
      $this->ACTMSGTICKETRESDATA = $ACTMSGTICKETRESDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETRESULT8
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETRESULT8
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
