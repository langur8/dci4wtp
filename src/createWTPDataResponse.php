<?php

namespace Axess\Dci4Wtp;

class createWTPDataResponse
{

    /**
     * @var D4WTPCREATEWTPDATARES $createWTPDataResult
     */
    protected $createWTPDataResult = null;

    /**
     * @param D4WTPCREATEWTPDATARES $createWTPDataResult
     */
    public function __construct($createWTPDataResult)
    {
      $this->createWTPDataResult = $createWTPDataResult;
    }

    /**
     * @return D4WTPCREATEWTPDATARES
     */
    public function getCreateWTPDataResult()
    {
      return $this->createWTPDataResult;
    }

    /**
     * @param D4WTPCREATEWTPDATARES $createWTPDataResult
     * @return \Axess\Dci4Wtp\createWTPDataResponse
     */
    public function setCreateWTPDataResult($createWTPDataResult)
    {
      $this->createWTPDataResult = $createWTPDataResult;
      return $this;
    }

}
