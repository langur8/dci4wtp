<?php

namespace Axess\Dci4Wtp;

class msgIssueTicket6
{

    /**
     * @var D4WTPMSGISSUETICKETREQUEST6 $i_ctMsgTicketReq
     */
    protected $i_ctMsgTicketReq = null;

    /**
     * @param D4WTPMSGISSUETICKETREQUEST6 $i_ctMsgTicketReq
     */
    public function __construct($i_ctMsgTicketReq)
    {
      $this->i_ctMsgTicketReq = $i_ctMsgTicketReq;
    }

    /**
     * @return D4WTPMSGISSUETICKETREQUEST6
     */
    public function getI_ctMsgTicketReq()
    {
      return $this->i_ctMsgTicketReq;
    }

    /**
     * @param D4WTPMSGISSUETICKETREQUEST6 $i_ctMsgTicketReq
     * @return \Axess\Dci4Wtp\msgIssueTicket6
     */
    public function setI_ctMsgTicketReq($i_ctMsgTicketReq)
    {
      $this->i_ctMsgTicketReq = $i_ctMsgTicketReq;
      return $this;
    }

}
