<?php

namespace Axess\Dci4Wtp;

class getChipcardEntriesResponse
{

    /**
     * @var D4WTPCHIPCARDENTRIESRES $getChipcardEntriesResult
     */
    protected $getChipcardEntriesResult = null;

    /**
     * @param D4WTPCHIPCARDENTRIESRES $getChipcardEntriesResult
     */
    public function __construct($getChipcardEntriesResult)
    {
      $this->getChipcardEntriesResult = $getChipcardEntriesResult;
    }

    /**
     * @return D4WTPCHIPCARDENTRIESRES
     */
    public function getGetChipcardEntriesResult()
    {
      return $this->getChipcardEntriesResult;
    }

    /**
     * @param D4WTPCHIPCARDENTRIESRES $getChipcardEntriesResult
     * @return \Axess\Dci4Wtp\getChipcardEntriesResponse
     */
    public function setGetChipcardEntriesResult($getChipcardEntriesResult)
    {
      $this->getChipcardEntriesResult = $getChipcardEntriesResult;
      return $this;
    }

}
