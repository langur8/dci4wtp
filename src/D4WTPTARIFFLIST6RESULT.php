<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST6RESULT
{

    /**
     * @var ArrayOfD4WTPTARIFFLIST6 $ACTTARIFFLIST6
     */
    protected $ACTTARIFFLIST6 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLIST6
     */
    public function getACTTARIFFLIST6()
    {
      return $this->ACTTARIFFLIST6;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLIST6 $ACTTARIFFLIST6
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST6RESULT
     */
    public function setACTTARIFFLIST6($ACTTARIFFLIST6)
    {
      $this->ACTTARIFFLIST6 = $ACTTARIFFLIST6;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST6RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST6RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
