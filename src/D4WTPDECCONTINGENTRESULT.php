<?php

namespace Axess\Dci4Wtp;

class D4WTPDECCONTINGENTRESULT
{

    /**
     * @var ArrayOfD4WTPCONTNGNTTICKET $ACTCONTNGNTTICKET
     */
    protected $ACTCONTNGNTTICKET = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCONTNGNTTICKET
     */
    public function getACTCONTNGNTTICKET()
    {
      return $this->ACTCONTNGNTTICKET;
    }

    /**
     * @param ArrayOfD4WTPCONTNGNTTICKET $ACTCONTNGNTTICKET
     * @return \Axess\Dci4Wtp\D4WTPDECCONTINGENTRESULT
     */
    public function setACTCONTNGNTTICKET($ACTCONTNGNTTICKET)
    {
      $this->ACTCONTNGNTTICKET = $ACTCONTNGNTTICKET;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPDECCONTINGENTRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPDECCONTINGENTRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
