<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPGROUP implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPGROUP[] $D4WTPGROUP
     */
    protected $D4WTPGROUP = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPGROUP[]
     */
    public function getD4WTPGROUP()
    {
      return $this->D4WTPGROUP;
    }

    /**
     * @param D4WTPGROUP[] $D4WTPGROUP
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPGROUP
     */
    public function setD4WTPGROUP(array $D4WTPGROUP = null)
    {
      $this->D4WTPGROUP = $D4WTPGROUP;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPGROUP[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPGROUP
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPGROUP[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPGROUP $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPGROUP[] = $value;
      } else {
        $this->D4WTPGROUP[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPGROUP[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPGROUP Return the current element
     */
    public function current()
    {
      return current($this->D4WTPGROUP);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPGROUP);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPGROUP);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPGROUP);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPGROUP Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPGROUP);
    }

}
