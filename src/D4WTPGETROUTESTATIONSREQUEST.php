<?php

namespace Axess\Dci4Wtp;

class D4WTPGETROUTESTATIONSREQUEST
{

    /**
     * @var ArrayOfD4WTPTICKETTYPENO $ACTTICKETTYPENO
     */
    protected $ACTTICKETTYPENO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKETTYPENO
     */
    public function getACTTICKETTYPENO()
    {
      return $this->ACTTICKETTYPENO;
    }

    /**
     * @param ArrayOfD4WTPTICKETTYPENO $ACTTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPGETROUTESTATIONSREQUEST
     */
    public function setACTTICKETTYPENO($ACTTICKETTYPENO)
    {
      $this->ACTTICKETTYPENO = $ACTTICKETTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETROUTESTATIONSREQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETROUTESTATIONSREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
