<?php

namespace Axess\Dci4Wtp;

class getPackageTariffList2Response
{

    /**
     * @var D4WTPPKGTARIFFLIST2RESULT $getPackageTariffList2Result
     */
    protected $getPackageTariffList2Result = null;

    /**
     * @param D4WTPPKGTARIFFLIST2RESULT $getPackageTariffList2Result
     */
    public function __construct($getPackageTariffList2Result)
    {
      $this->getPackageTariffList2Result = $getPackageTariffList2Result;
    }

    /**
     * @return D4WTPPKGTARIFFLIST2RESULT
     */
    public function getGetPackageTariffList2Result()
    {
      return $this->getPackageTariffList2Result;
    }

    /**
     * @param D4WTPPKGTARIFFLIST2RESULT $getPackageTariffList2Result
     * @return \Axess\Dci4Wtp\getPackageTariffList2Response
     */
    public function setGetPackageTariffList2Result($getPackageTariffList2Result)
    {
      $this->getPackageTariffList2Result = $getPackageTariffList2Result;
      return $this;
    }

}
