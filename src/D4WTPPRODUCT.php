<?php

namespace Axess\Dci4Wtp;

class D4WTPPRODUCT
{

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NPERSONTYPNO
     */
    protected $NPERSONTYPNO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCT
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSONTYPNO()
    {
      return $this->NPERSONTYPNO;
    }

    /**
     * @param float $NPERSONTYPNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCT
     */
    public function setNPERSONTYPNO($NPERSONTYPNO)
    {
      $this->NPERSONTYPNO = $NPERSONTYPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCT
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCT
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCT
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPPRODUCT
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
