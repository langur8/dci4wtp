<?php

namespace Axess\Dci4Wtp;

class getPackageTariffList6Response
{

    /**
     * @var D4WTPPKGTARIFFLIST6RESULT $getPackageTariffList6Result
     */
    protected $getPackageTariffList6Result = null;

    /**
     * @param D4WTPPKGTARIFFLIST6RESULT $getPackageTariffList6Result
     */
    public function __construct($getPackageTariffList6Result)
    {
      $this->getPackageTariffList6Result = $getPackageTariffList6Result;
    }

    /**
     * @return D4WTPPKGTARIFFLIST6RESULT
     */
    public function getGetPackageTariffList6Result()
    {
      return $this->getPackageTariffList6Result;
    }

    /**
     * @param D4WTPPKGTARIFFLIST6RESULT $getPackageTariffList6Result
     * @return \Axess\Dci4Wtp\getPackageTariffList6Response
     */
    public function setGetPackageTariffList6Result($getPackageTariffList6Result)
    {
      $this->getPackageTariffList6Result = $getPackageTariffList6Result;
      return $this;
    }

}
