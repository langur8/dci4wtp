<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCUSTOMERPROFSETUPITEM implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCUSTOMERPROFSETUPITEM[] $D4WTPCUSTOMERPROFSETUPITEM
     */
    protected $D4WTPCUSTOMERPROFSETUPITEM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCUSTOMERPROFSETUPITEM[]
     */
    public function getD4WTPCUSTOMERPROFSETUPITEM()
    {
      return $this->D4WTPCUSTOMERPROFSETUPITEM;
    }

    /**
     * @param D4WTPCUSTOMERPROFSETUPITEM[] $D4WTPCUSTOMERPROFSETUPITEM
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCUSTOMERPROFSETUPITEM
     */
    public function setD4WTPCUSTOMERPROFSETUPITEM(array $D4WTPCUSTOMERPROFSETUPITEM = null)
    {
      $this->D4WTPCUSTOMERPROFSETUPITEM = $D4WTPCUSTOMERPROFSETUPITEM;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCUSTOMERPROFSETUPITEM[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCUSTOMERPROFSETUPITEM
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCUSTOMERPROFSETUPITEM[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCUSTOMERPROFSETUPITEM $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCUSTOMERPROFSETUPITEM[] = $value;
      } else {
        $this->D4WTPCUSTOMERPROFSETUPITEM[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCUSTOMERPROFSETUPITEM[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCUSTOMERPROFSETUPITEM Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCUSTOMERPROFSETUPITEM);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCUSTOMERPROFSETUPITEM);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCUSTOMERPROFSETUPITEM);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCUSTOMERPROFSETUPITEM);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCUSTOMERPROFSETUPITEM Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCUSTOMERPROFSETUPITEM);
    }

}
