<?php

namespace Axess\Dci4Wtp;

class D4WTPMODIFYRESREQUEST
{

    /**
     * @var float $NCONTINGENTNO
     */
    protected $NCONTINGENTNO = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NSUBCONTINGENTNO
     */
    protected $NSUBCONTINGENTNO = null;

    /**
     * @var string $SZRESERVATIONDATE
     */
    protected $SZRESERVATIONDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNO()
    {
      return $this->NCONTINGENTNO;
    }

    /**
     * @param float $NCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPMODIFYRESREQUEST
     */
    public function setNCONTINGENTNO($NCONTINGENTNO)
    {
      $this->NCONTINGENTNO = $NCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPMODIFYRESREQUEST
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPMODIFYRESREQUEST
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPMODIFYRESREQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPMODIFYRESREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTINGENTNO()
    {
      return $this->NSUBCONTINGENTNO;
    }

    /**
     * @param float $NSUBCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPMODIFYRESREQUEST
     */
    public function setNSUBCONTINGENTNO($NSUBCONTINGENTNO)
    {
      $this->NSUBCONTINGENTNO = $NSUBCONTINGENTNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRESERVATIONDATE()
    {
      return $this->SZRESERVATIONDATE;
    }

    /**
     * @param string $SZRESERVATIONDATE
     * @return \Axess\Dci4Wtp\D4WTPMODIFYRESREQUEST
     */
    public function setSZRESERVATIONDATE($SZRESERVATIONDATE)
    {
      $this->SZRESERVATIONDATE = $SZRESERVATIONDATE;
      return $this;
    }

}
