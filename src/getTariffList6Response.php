<?php

namespace Axess\Dci4Wtp;

class getTariffList6Response
{

    /**
     * @var D4WTPTARIFFLIST6RESULT $getTariffList6Result
     */
    protected $getTariffList6Result = null;

    /**
     * @param D4WTPTARIFFLIST6RESULT $getTariffList6Result
     */
    public function __construct($getTariffList6Result)
    {
      $this->getTariffList6Result = $getTariffList6Result;
    }

    /**
     * @return D4WTPTARIFFLIST6RESULT
     */
    public function getGetTariffList6Result()
    {
      return $this->getTariffList6Result;
    }

    /**
     * @param D4WTPTARIFFLIST6RESULT $getTariffList6Result
     * @return \Axess\Dci4Wtp\getTariffList6Response
     */
    public function setGetTariffList6Result($getTariffList6Result)
    {
      $this->getTariffList6Result = $getTariffList6Result;
      return $this;
    }

}
