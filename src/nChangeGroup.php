<?php

namespace Axess\Dci4Wtp;

class nChangeGroup
{

    /**
     * @var D4WTPGROUPREQUEST $i_ctGroupRequest
     */
    protected $i_ctGroupRequest = null;

    /**
     * @param D4WTPGROUPREQUEST $i_ctGroupRequest
     */
    public function __construct($i_ctGroupRequest)
    {
      $this->i_ctGroupRequest = $i_ctGroupRequest;
    }

    /**
     * @return D4WTPGROUPREQUEST
     */
    public function getI_ctGroupRequest()
    {
      return $this->i_ctGroupRequest;
    }

    /**
     * @param D4WTPGROUPREQUEST $i_ctGroupRequest
     * @return \Axess\Dci4Wtp\nChangeGroup
     */
    public function setI_ctGroupRequest($i_ctGroupRequest)
    {
      $this->i_ctGroupRequest = $i_ctGroupRequest;
      return $this;
    }

}
