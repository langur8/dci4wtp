<?php

namespace Axess\Dci4Wtp;

class D4WTPREADTICKETRESULT
{

    /**
     * @var ArrayOfD4WTPTICKETCONTENT $ACTTICKETCONTENT
     */
    protected $ACTTICKETCONTENT = null;

    /**
     * @var D4WTPERSONDATA $CTPERSONDATA
     */
    protected $CTPERSONDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKETCONTENT
     */
    public function getACTTICKETCONTENT()
    {
      return $this->ACTTICKETCONTENT;
    }

    /**
     * @param ArrayOfD4WTPTICKETCONTENT $ACTTICKETCONTENT
     * @return \Axess\Dci4Wtp\D4WTPREADTICKETRESULT
     */
    public function setACTTICKETCONTENT($ACTTICKETCONTENT)
    {
      $this->ACTTICKETCONTENT = $ACTTICKETCONTENT;
      return $this;
    }

    /**
     * @return D4WTPERSONDATA
     */
    public function getCTPERSONDATA()
    {
      return $this->CTPERSONDATA;
    }

    /**
     * @param D4WTPERSONDATA $CTPERSONDATA
     * @return \Axess\Dci4Wtp\D4WTPREADTICKETRESULT
     */
    public function setCTPERSONDATA($CTPERSONDATA)
    {
      $this->CTPERSONDATA = $CTPERSONDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPREADTICKETRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPREADTICKETRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
