<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPDAYCONTINGENTS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPDAYCONTINGENTS[] $D4WTPDAYCONTINGENTS
     */
    protected $D4WTPDAYCONTINGENTS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPDAYCONTINGENTS[]
     */
    public function getD4WTPDAYCONTINGENTS()
    {
      return $this->D4WTPDAYCONTINGENTS;
    }

    /**
     * @param D4WTPDAYCONTINGENTS[] $D4WTPDAYCONTINGENTS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPDAYCONTINGENTS
     */
    public function setD4WTPDAYCONTINGENTS(array $D4WTPDAYCONTINGENTS = null)
    {
      $this->D4WTPDAYCONTINGENTS = $D4WTPDAYCONTINGENTS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPDAYCONTINGENTS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPDAYCONTINGENTS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPDAYCONTINGENTS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPDAYCONTINGENTS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPDAYCONTINGENTS[] = $value;
      } else {
        $this->D4WTPDAYCONTINGENTS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPDAYCONTINGENTS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPDAYCONTINGENTS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPDAYCONTINGENTS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPDAYCONTINGENTS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPDAYCONTINGENTS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPDAYCONTINGENTS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPDAYCONTINGENTS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPDAYCONTINGENTS);
    }

}
