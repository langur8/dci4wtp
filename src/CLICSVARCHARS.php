<?php

namespace Axess\Dci4Wtp;

class CLICSVARCHARS
{

    /**
     * @var string $SZTEXT1
     */
    protected $SZTEXT1 = null;

    /**
     * @var string $SZTEXT2
     */
    protected $SZTEXT2 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZTEXT1()
    {
      return $this->SZTEXT1;
    }

    /**
     * @param string $SZTEXT1
     * @return \Axess\Dci4Wtp\CLICSVARCHARS
     */
    public function setSZTEXT1($SZTEXT1)
    {
      $this->SZTEXT1 = $SZTEXT1;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTEXT2()
    {
      return $this->SZTEXT2;
    }

    /**
     * @param string $SZTEXT2
     * @return \Axess\Dci4Wtp\CLICSVARCHARS
     */
    public function setSZTEXT2($SZTEXT2)
    {
      $this->SZTEXT2 = $SZTEXT2;
      return $this;
    }

}
