<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETPRODDATA3
{

    /**
     * @var float $BPERSAKTIV
     */
    protected $BPERSAKTIV = null;

    /**
     * @var float $BPREPAIDTICKET
     */
    protected $BPREPAIDTICKET = null;

    /**
     * @var D4WTPCODINGDATA $CTCODINGDATA
     */
    protected $CTCODINGDATA = null;

    /**
     * @var D4WTPPRINTINGDATA $CTPRINTINGDATA
     */
    protected $CTPRINTINGDATA = null;

    /**
     * @var float $NAPPTRANSLOGID
     */
    protected $NAPPTRANSLOGID = null;

    /**
     * @var float $NBLOCKNR
     */
    protected $NBLOCKNR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var string $SZBCNR_24
     */
    protected $SZBCNR_24 = null;

    /**
     * @var string $SZCURRENCY
     */
    protected $SZCURRENCY = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBPERSAKTIV()
    {
      return $this->BPERSAKTIV;
    }

    /**
     * @param float $BPERSAKTIV
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setBPERSAKTIV($BPERSAKTIV)
    {
      $this->BPERSAKTIV = $BPERSAKTIV;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPREPAIDTICKET()
    {
      return $this->BPREPAIDTICKET;
    }

    /**
     * @param float $BPREPAIDTICKET
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setBPREPAIDTICKET($BPREPAIDTICKET)
    {
      $this->BPREPAIDTICKET = $BPREPAIDTICKET;
      return $this;
    }

    /**
     * @return D4WTPCODINGDATA
     */
    public function getCTCODINGDATA()
    {
      return $this->CTCODINGDATA;
    }

    /**
     * @param D4WTPCODINGDATA $CTCODINGDATA
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setCTCODINGDATA($CTCODINGDATA)
    {
      $this->CTCODINGDATA = $CTCODINGDATA;
      return $this;
    }

    /**
     * @return D4WTPPRINTINGDATA
     */
    public function getCTPRINTINGDATA()
    {
      return $this->CTPRINTINGDATA;
    }

    /**
     * @param D4WTPPRINTINGDATA $CTPRINTINGDATA
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setCTPRINTINGDATA($CTPRINTINGDATA)
    {
      $this->CTPRINTINGDATA = $CTPRINTINGDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAPPTRANSLOGID()
    {
      return $this->NAPPTRANSLOGID;
    }

    /**
     * @param float $NAPPTRANSLOGID
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setNAPPTRANSLOGID($NAPPTRANSLOGID)
    {
      $this->NAPPTRANSLOGID = $NAPPTRANSLOGID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKNR()
    {
      return $this->NBLOCKNR;
    }

    /**
     * @param float $NBLOCKNR
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setNBLOCKNR($NBLOCKNR)
    {
      $this->NBLOCKNR = $NBLOCKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBCNR_24()
    {
      return $this->SZBCNR_24;
    }

    /**
     * @param string $SZBCNR_24
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setSZBCNR_24($SZBCNR_24)
    {
      $this->SZBCNR_24 = $SZBCNR_24;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCY()
    {
      return $this->SZCURRENCY;
    }

    /**
     * @param string $SZCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setSZCURRENCY($SZCURRENCY)
    {
      $this->SZCURRENCY = $SZCURRENCY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA3
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
