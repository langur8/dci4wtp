<?php

namespace Axess\Dci4Wtp;

class decContingentResponse
{

    /**
     * @var D4WTPDECCONTINGENTRESULT $decContingentResult
     */
    protected $decContingentResult = null;

    /**
     * @param D4WTPDECCONTINGENTRESULT $decContingentResult
     */
    public function __construct($decContingentResult)
    {
      $this->decContingentResult = $decContingentResult;
    }

    /**
     * @return D4WTPDECCONTINGENTRESULT
     */
    public function getDecContingentResult()
    {
      return $this->decContingentResult;
    }

    /**
     * @param D4WTPDECCONTINGENTRESULT $decContingentResult
     * @return \Axess\Dci4Wtp\decContingentResponse
     */
    public function setDecContingentResult($decContingentResult)
    {
      $this->decContingentResult = $decContingentResult;
      return $this;
    }

}
