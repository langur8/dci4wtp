<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGEPOSTARIFFCALC
{

    /**
     * @var D4WTPPACKAGECONTENT $CTPACKAGECONTENT
     */
    protected $CTPACKAGECONTENT = null;

    /**
     * @var D4WTPPACKAGEPOSTARIFF $CTPACKAGEPOSTARIFF
     */
    protected $CTPACKAGEPOSTARIFF = null;

    /**
     * @var float $FSINGLETARIFFCALC
     */
    protected $FSINGLETARIFFCALC = null;

    /**
     * @var float $FTOTALPRICE
     */
    protected $FTOTALPRICE = null;

    /**
     * @var float $NPACKPOS
     */
    protected $NPACKPOS = null;

    /**
     * @var float $NPACKTARIFFSHEETNR
     */
    protected $NPACKTARIFFSHEETNR = null;

    /**
     * @var float $NPACKTARIFFVALIDNR
     */
    protected $NPACKTARIFFVALIDNR = null;

    /**
     * @var float $NPPACKNR
     */
    protected $NPPACKNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPACKAGECONTENT
     */
    public function getCTPACKAGECONTENT()
    {
      return $this->CTPACKAGECONTENT;
    }

    /**
     * @param D4WTPPACKAGECONTENT $CTPACKAGECONTENT
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFFCALC
     */
    public function setCTPACKAGECONTENT($CTPACKAGECONTENT)
    {
      $this->CTPACKAGECONTENT = $CTPACKAGECONTENT;
      return $this;
    }

    /**
     * @return D4WTPPACKAGEPOSTARIFF
     */
    public function getCTPACKAGEPOSTARIFF()
    {
      return $this->CTPACKAGEPOSTARIFF;
    }

    /**
     * @param D4WTPPACKAGEPOSTARIFF $CTPACKAGEPOSTARIFF
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFFCALC
     */
    public function setCTPACKAGEPOSTARIFF($CTPACKAGEPOSTARIFF)
    {
      $this->CTPACKAGEPOSTARIFF = $CTPACKAGEPOSTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getFSINGLETARIFFCALC()
    {
      return $this->FSINGLETARIFFCALC;
    }

    /**
     * @param float $FSINGLETARIFFCALC
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFFCALC
     */
    public function setFSINGLETARIFFCALC($FSINGLETARIFFCALC)
    {
      $this->FSINGLETARIFFCALC = $FSINGLETARIFFCALC;
      return $this;
    }

    /**
     * @return float
     */
    public function getFTOTALPRICE()
    {
      return $this->FTOTALPRICE;
    }

    /**
     * @param float $FTOTALPRICE
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFFCALC
     */
    public function setFTOTALPRICE($FTOTALPRICE)
    {
      $this->FTOTALPRICE = $FTOTALPRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKPOS()
    {
      return $this->NPACKPOS;
    }

    /**
     * @param float $NPACKPOS
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFFCALC
     */
    public function setNPACKPOS($NPACKPOS)
    {
      $this->NPACKPOS = $NPACKPOS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKTARIFFSHEETNR()
    {
      return $this->NPACKTARIFFSHEETNR;
    }

    /**
     * @param float $NPACKTARIFFSHEETNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFFCALC
     */
    public function setNPACKTARIFFSHEETNR($NPACKTARIFFSHEETNR)
    {
      $this->NPACKTARIFFSHEETNR = $NPACKTARIFFSHEETNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKTARIFFVALIDNR()
    {
      return $this->NPACKTARIFFVALIDNR;
    }

    /**
     * @param float $NPACKTARIFFVALIDNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFFCALC
     */
    public function setNPACKTARIFFVALIDNR($NPACKTARIFFVALIDNR)
    {
      $this->NPACKTARIFFVALIDNR = $NPACKTARIFFVALIDNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPPACKNR()
    {
      return $this->NPPACKNR;
    }

    /**
     * @param float $NPPACKNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFFCALC
     */
    public function setNPPACKNR($NPPACKNR)
    {
      $this->NPPACKNR = $NPPACKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFFCALC
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFFCALC
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

}
