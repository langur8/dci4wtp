<?php

namespace Axess\Dci4Wtp;

class uploadPrivacyDocResponse
{

    /**
     * @var D4WTPUPLOADPRIVACYDOCRES $uploadPrivacyDocResult
     */
    protected $uploadPrivacyDocResult = null;

    /**
     * @param D4WTPUPLOADPRIVACYDOCRES $uploadPrivacyDocResult
     */
    public function __construct($uploadPrivacyDocResult)
    {
      $this->uploadPrivacyDocResult = $uploadPrivacyDocResult;
    }

    /**
     * @return D4WTPUPLOADPRIVACYDOCRES
     */
    public function getUploadPrivacyDocResult()
    {
      return $this->uploadPrivacyDocResult;
    }

    /**
     * @param D4WTPUPLOADPRIVACYDOCRES $uploadPrivacyDocResult
     * @return \Axess\Dci4Wtp\uploadPrivacyDocResponse
     */
    public function setUploadPrivacyDocResult($uploadPrivacyDocResult)
    {
      $this->uploadPrivacyDocResult = $uploadPrivacyDocResult;
      return $this;
    }

}
