<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPERSTYPERULE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPERSTYPERULE[] $D4WTPPERSTYPERULE
     */
    protected $D4WTPPERSTYPERULE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPERSTYPERULE[]
     */
    public function getD4WTPPERSTYPERULE()
    {
      return $this->D4WTPPERSTYPERULE;
    }

    /**
     * @param D4WTPPERSTYPERULE[] $D4WTPPERSTYPERULE
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPERSTYPERULE
     */
    public function setD4WTPPERSTYPERULE(array $D4WTPPERSTYPERULE = null)
    {
      $this->D4WTPPERSTYPERULE = $D4WTPPERSTYPERULE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPERSTYPERULE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPERSTYPERULE
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPERSTYPERULE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPERSTYPERULE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPERSTYPERULE[] = $value;
      } else {
        $this->D4WTPPERSTYPERULE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPERSTYPERULE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPERSTYPERULE Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPERSTYPERULE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPERSTYPERULE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPERSTYPERULE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPERSTYPERULE);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPERSTYPERULE Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPERSTYPERULE);
    }

}
