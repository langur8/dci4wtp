<?php

namespace Axess\Dci4Wtp;

class D4WTPCONTINGENTLIST5
{

    /**
     * @var ArrayOfD4WTPSUBCONTINGENTLIST3 $ACTSUBCONTINGENTLIST3
     */
    protected $ACTSUBCONTINGENTLIST3 = null;

    /**
     * @var float $BRESOPTIONAL
     */
    protected $BRESOPTIONAL = null;

    /**
     * @var D4WTPRESARTICLETARIFF $CTRESARTICLE
     */
    protected $CTRESARTICLE = null;

    /**
     * @var float $NCONTINGENTNR
     */
    protected $NCONTINGENTNR = null;

    /**
     * @var float $NCOUNTFREE
     */
    protected $NCOUNTFREE = null;

    /**
     * @var float $NSORTNR
     */
    protected $NSORTNR = null;

    /**
     * @var float $NTRAVELGROUPOFFSETMIN
     */
    protected $NTRAVELGROUPOFFSETMIN = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSUBCONTINGENTLIST3
     */
    public function getACTSUBCONTINGENTLIST3()
    {
      return $this->ACTSUBCONTINGENTLIST3;
    }

    /**
     * @param ArrayOfD4WTPSUBCONTINGENTLIST3 $ACTSUBCONTINGENTLIST3
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST5
     */
    public function setACTSUBCONTINGENTLIST3($ACTSUBCONTINGENTLIST3)
    {
      $this->ACTSUBCONTINGENTLIST3 = $ACTSUBCONTINGENTLIST3;
      return $this;
    }

    /**
     * @return float
     */
    public function getBRESOPTIONAL()
    {
      return $this->BRESOPTIONAL;
    }

    /**
     * @param float $BRESOPTIONAL
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST5
     */
    public function setBRESOPTIONAL($BRESOPTIONAL)
    {
      $this->BRESOPTIONAL = $BRESOPTIONAL;
      return $this;
    }

    /**
     * @return D4WTPRESARTICLETARIFF
     */
    public function getCTRESARTICLE()
    {
      return $this->CTRESARTICLE;
    }

    /**
     * @param D4WTPRESARTICLETARIFF $CTRESARTICLE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST5
     */
    public function setCTRESARTICLE($CTRESARTICLE)
    {
      $this->CTRESARTICLE = $CTRESARTICLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNR()
    {
      return $this->NCONTINGENTNR;
    }

    /**
     * @param float $NCONTINGENTNR
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST5
     */
    public function setNCONTINGENTNR($NCONTINGENTNR)
    {
      $this->NCONTINGENTNR = $NCONTINGENTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTFREE()
    {
      return $this->NCOUNTFREE;
    }

    /**
     * @param float $NCOUNTFREE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST5
     */
    public function setNCOUNTFREE($NCOUNTFREE)
    {
      $this->NCOUNTFREE = $NCOUNTFREE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNR()
    {
      return $this->NSORTNR;
    }

    /**
     * @param float $NSORTNR
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST5
     */
    public function setNSORTNR($NSORTNR)
    {
      $this->NSORTNR = $NSORTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPOFFSETMIN()
    {
      return $this->NTRAVELGROUPOFFSETMIN;
    }

    /**
     * @param float $NTRAVELGROUPOFFSETMIN
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST5
     */
    public function setNTRAVELGROUPOFFSETMIN($NTRAVELGROUPOFFSETMIN)
    {
      $this->NTRAVELGROUPOFFSETMIN = $NTRAVELGROUPOFFSETMIN;
      return $this;
    }

}
