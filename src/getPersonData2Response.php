<?php

namespace Axess\Dci4Wtp;

class getPersonData2Response
{

    /**
     * @var D4WTPGETPERSON2RESULT $getPersonData2Result
     */
    protected $getPersonData2Result = null;

    /**
     * @param D4WTPGETPERSON2RESULT $getPersonData2Result
     */
    public function __construct($getPersonData2Result)
    {
      $this->getPersonData2Result = $getPersonData2Result;
    }

    /**
     * @return D4WTPGETPERSON2RESULT
     */
    public function getGetPersonData2Result()
    {
      return $this->getPersonData2Result;
    }

    /**
     * @param D4WTPGETPERSON2RESULT $getPersonData2Result
     * @return \Axess\Dci4Wtp\getPersonData2Response
     */
    public function setGetPersonData2Result($getPersonData2Result)
    {
      $this->getPersonData2Result = $getPersonData2Result;
      return $this;
    }

}
