<?php

namespace Axess\Dci4Wtp;

class editOrderPositionsResponse
{

    /**
     * @var D4WTPEDITORDERPOSRES $editOrderPositionsResult
     */
    protected $editOrderPositionsResult = null;

    /**
     * @param D4WTPEDITORDERPOSRES $editOrderPositionsResult
     */
    public function __construct($editOrderPositionsResult)
    {
      $this->editOrderPositionsResult = $editOrderPositionsResult;
    }

    /**
     * @return D4WTPEDITORDERPOSRES
     */
    public function getEditOrderPositionsResult()
    {
      return $this->editOrderPositionsResult;
    }

    /**
     * @param D4WTPEDITORDERPOSRES $editOrderPositionsResult
     * @return \Axess\Dci4Wtp\editOrderPositionsResponse
     */
    public function setEditOrderPositionsResult($editOrderPositionsResult)
    {
      $this->editOrderPositionsResult = $editOrderPositionsResult;
      return $this;
    }

}
