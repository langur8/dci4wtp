<?php

namespace Axess\Dci4Wtp;

class D4WTPGETPERSON6RESULT
{

    /**
     * @var ArrayOfD4WTPERSONDATA7 $ACTPERSONDATA6
     */
    protected $ACTPERSONDATA6 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NROWCOUNT
     */
    protected $NROWCOUNT = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPERSONDATA7
     */
    public function getACTPERSONDATA6()
    {
      return $this->ACTPERSONDATA6;
    }

    /**
     * @param ArrayOfD4WTPERSONDATA7 $ACTPERSONDATA6
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON6RESULT
     */
    public function setACTPERSONDATA6($ACTPERSONDATA6)
    {
      $this->ACTPERSONDATA6 = $ACTPERSONDATA6;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON6RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNROWCOUNT()
    {
      return $this->NROWCOUNT;
    }

    /**
     * @param float $NROWCOUNT
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON6RESULT
     */
    public function setNROWCOUNT($NROWCOUNT)
    {
      $this->NROWCOUNT = $NROWCOUNT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON6RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
