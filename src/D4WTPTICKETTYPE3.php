<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETTYPE3
{

    /**
     * @var float $BVORDATIERBAR
     */
    protected $BVORDATIERBAR = null;

    /**
     * @var float $BWTPMITDRUCKER
     */
    protected $BWTPMITDRUCKER = null;

    /**
     * @var float $BWTPOHNEDRUCKER
     */
    protected $BWTPOHNEDRUCKER = null;

    /**
     * @var float $NICONSYSNR
     */
    protected $NICONSYSNR = null;

    /**
     * @var float $NICONTYPNR
     */
    protected $NICONTYPNR = null;

    /**
     * @var float $NKARTENGRUNDTYPNR
     */
    protected $NKARTENGRUNDTYPNR = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var string $SZICONNAME
     */
    protected $SZICONNAME = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBVORDATIERBAR()
    {
      return $this->BVORDATIERBAR;
    }

    /**
     * @param float $BVORDATIERBAR
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3
     */
    public function setBVORDATIERBAR($BVORDATIERBAR)
    {
      $this->BVORDATIERBAR = $BVORDATIERBAR;
      return $this;
    }

    /**
     * @return float
     */
    public function getBWTPMITDRUCKER()
    {
      return $this->BWTPMITDRUCKER;
    }

    /**
     * @param float $BWTPMITDRUCKER
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3
     */
    public function setBWTPMITDRUCKER($BWTPMITDRUCKER)
    {
      $this->BWTPMITDRUCKER = $BWTPMITDRUCKER;
      return $this;
    }

    /**
     * @return float
     */
    public function getBWTPOHNEDRUCKER()
    {
      return $this->BWTPOHNEDRUCKER;
    }

    /**
     * @param float $BWTPOHNEDRUCKER
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3
     */
    public function setBWTPOHNEDRUCKER($BWTPOHNEDRUCKER)
    {
      $this->BWTPOHNEDRUCKER = $BWTPOHNEDRUCKER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNICONSYSNR()
    {
      return $this->NICONSYSNR;
    }

    /**
     * @param float $NICONSYSNR
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3
     */
    public function setNICONSYSNR($NICONSYSNR)
    {
      $this->NICONSYSNR = $NICONSYSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNICONTYPNR()
    {
      return $this->NICONTYPNR;
    }

    /**
     * @param float $NICONTYPNR
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3
     */
    public function setNICONTYPNR($NICONTYPNR)
    {
      $this->NICONTYPNR = $NICONTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKARTENGRUNDTYPNR()
    {
      return $this->NKARTENGRUNDTYPNR;
    }

    /**
     * @param float $NKARTENGRUNDTYPNR
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3
     */
    public function setNKARTENGRUNDTYPNR($NKARTENGRUNDTYPNR)
    {
      $this->NKARTENGRUNDTYPNR = $NKARTENGRUNDTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZICONNAME()
    {
      return $this->SZICONNAME;
    }

    /**
     * @param string $SZICONNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3
     */
    public function setSZICONNAME($SZICONNAME)
    {
      $this->SZICONNAME = $SZICONNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
