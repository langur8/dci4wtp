<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTICKET implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTICKET[] $D4WTPTICKET
     */
    protected $D4WTPTICKET = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTICKET[]
     */
    public function getD4WTPTICKET()
    {
      return $this->D4WTPTICKET;
    }

    /**
     * @param D4WTPTICKET[] $D4WTPTICKET
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTICKET
     */
    public function setD4WTPTICKET(array $D4WTPTICKET = null)
    {
      $this->D4WTPTICKET = $D4WTPTICKET;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTICKET[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTICKET
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTICKET[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTICKET $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTICKET[] = $value;
      } else {
        $this->D4WTPTICKET[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTICKET[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTICKET Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTICKET);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTICKET);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTICKET);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTICKET);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTICKET Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTICKET);
    }

}
