<?php

namespace Axess\Dci4Wtp;

class getPackagePosTarif
{

    /**
     * @var D4WTPPACKAGEPOSTARIFREQUEST $i_ctPackagePosTarifReq
     */
    protected $i_ctPackagePosTarifReq = null;

    /**
     * @param D4WTPPACKAGEPOSTARIFREQUEST $i_ctPackagePosTarifReq
     */
    public function __construct($i_ctPackagePosTarifReq)
    {
      $this->i_ctPackagePosTarifReq = $i_ctPackagePosTarifReq;
    }

    /**
     * @return D4WTPPACKAGEPOSTARIFREQUEST
     */
    public function getI_ctPackagePosTarifReq()
    {
      return $this->i_ctPackagePosTarifReq;
    }

    /**
     * @param D4WTPPACKAGEPOSTARIFREQUEST $i_ctPackagePosTarifReq
     * @return \Axess\Dci4Wtp\getPackagePosTarif
     */
    public function setI_ctPackagePosTarifReq($i_ctPackagePosTarifReq)
    {
      $this->i_ctPackagePosTarifReq = $i_ctPackagePosTarifReq;
      return $this;
    }

}
