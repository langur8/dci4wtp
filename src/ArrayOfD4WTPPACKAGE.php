<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPACKAGE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPACKAGE[] $D4WTPPACKAGE
     */
    protected $D4WTPPACKAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPACKAGE[]
     */
    public function getD4WTPPACKAGE()
    {
      return $this->D4WTPPACKAGE;
    }

    /**
     * @param D4WTPPACKAGE[] $D4WTPPACKAGE
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPACKAGE
     */
    public function setD4WTPPACKAGE(array $D4WTPPACKAGE = null)
    {
      $this->D4WTPPACKAGE = $D4WTPPACKAGE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPACKAGE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPACKAGE
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPACKAGE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPACKAGE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPACKAGE[] = $value;
      } else {
        $this->D4WTPPACKAGE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPACKAGE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPACKAGE Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPACKAGE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPACKAGE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPACKAGE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPACKAGE);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPACKAGE Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPACKAGE);
    }

}
