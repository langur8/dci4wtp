<?php

namespace Axess\Dci4Wtp;

class getRentalItemTariff
{

    /**
     * @var D4WTPGETRENTALITEMTARIFFREQ $i_ctGetRentalItemTariffReq
     */
    protected $i_ctGetRentalItemTariffReq = null;

    /**
     * @param D4WTPGETRENTALITEMTARIFFREQ $i_ctGetRentalItemTariffReq
     */
    public function __construct($i_ctGetRentalItemTariffReq)
    {
      $this->i_ctGetRentalItemTariffReq = $i_ctGetRentalItemTariffReq;
    }

    /**
     * @return D4WTPGETRENTALITEMTARIFFREQ
     */
    public function getI_ctGetRentalItemTariffReq()
    {
      return $this->i_ctGetRentalItemTariffReq;
    }

    /**
     * @param D4WTPGETRENTALITEMTARIFFREQ $i_ctGetRentalItemTariffReq
     * @return \Axess\Dci4Wtp\getRentalItemTariff
     */
    public function setI_ctGetRentalItemTariffReq($i_ctGetRentalItemTariffReq)
    {
      $this->i_ctGetRentalItemTariffReq = $i_ctGetRentalItemTariffReq;
      return $this;
    }

}
