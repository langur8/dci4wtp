<?php

namespace Axess\Dci4Wtp;

class D4WTPCOMPANY
{

    /**
     * @var ADDRESS $ADDRESS
     */
    protected $ADDRESS = null;

    /**
     * @var float $BCOMPANYACTIVE
     */
    protected $BCOMPANYACTIVE = null;

    /**
     * @var float $NADRTYPNR
     */
    protected $NADRTYPNR = null;

    /**
     * @var float $NCOMPANYTYPENO
     */
    protected $NCOMPANYTYPENO = null;

    /**
     * @var float $NCOMPNR
     */
    protected $NCOMPNR = null;

    /**
     * @var float $NCOMPPOSNR
     */
    protected $NCOMPPOSNR = null;

    /**
     * @var float $NCOMPPROJNR
     */
    protected $NCOMPPROJNR = null;

    /**
     * @var string $SZCARDMASKTEXT
     */
    protected $SZCARDMASKTEXT = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZINFO
     */
    protected $SZINFO = null;

    /**
     * @var string $SZMATCHCODE
     */
    protected $SZMATCHCODE = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZUID
     */
    protected $SZUID = null;

    /**
     * @var string $SZURL
     */
    protected $SZURL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ADDRESS
     */
    public function getADDRESS()
    {
      return $this->ADDRESS;
    }

    /**
     * @param ADDRESS $ADDRESS
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setADDRESS($ADDRESS)
    {
      $this->ADDRESS = $ADDRESS;
      return $this;
    }

    /**
     * @return float
     */
    public function getBCOMPANYACTIVE()
    {
      return $this->BCOMPANYACTIVE;
    }

    /**
     * @param float $BCOMPANYACTIVE
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setBCOMPANYACTIVE($BCOMPANYACTIVE)
    {
      $this->BCOMPANYACTIVE = $BCOMPANYACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNADRTYPNR()
    {
      return $this->NADRTYPNR;
    }

    /**
     * @param float $NADRTYPNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setNADRTYPNR($NADRTYPNR)
    {
      $this->NADRTYPNR = $NADRTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYTYPENO()
    {
      return $this->NCOMPANYTYPENO;
    }

    /**
     * @param float $NCOMPANYTYPENO
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setNCOMPANYTYPENO($NCOMPANYTYPENO)
    {
      $this->NCOMPANYTYPENO = $NCOMPANYTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPNR()
    {
      return $this->NCOMPNR;
    }

    /**
     * @param float $NCOMPNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setNCOMPNR($NCOMPNR)
    {
      $this->NCOMPNR = $NCOMPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPPOSNR()
    {
      return $this->NCOMPPOSNR;
    }

    /**
     * @param float $NCOMPPOSNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setNCOMPPOSNR($NCOMPPOSNR)
    {
      $this->NCOMPPOSNR = $NCOMPPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPPROJNR()
    {
      return $this->NCOMPPROJNR;
    }

    /**
     * @param float $NCOMPPROJNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setNCOMPPROJNR($NCOMPPROJNR)
    {
      $this->NCOMPPROJNR = $NCOMPPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDMASKTEXT()
    {
      return $this->SZCARDMASKTEXT;
    }

    /**
     * @param string $SZCARDMASKTEXT
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setSZCARDMASKTEXT($SZCARDMASKTEXT)
    {
      $this->SZCARDMASKTEXT = $SZCARDMASKTEXT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZINFO()
    {
      return $this->SZINFO;
    }

    /**
     * @param string $SZINFO
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setSZINFO($SZINFO)
    {
      $this->SZINFO = $SZINFO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMATCHCODE()
    {
      return $this->SZMATCHCODE;
    }

    /**
     * @param string $SZMATCHCODE
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setSZMATCHCODE($SZMATCHCODE)
    {
      $this->SZMATCHCODE = $SZMATCHCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUID()
    {
      return $this->SZUID;
    }

    /**
     * @param string $SZUID
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setSZUID($SZUID)
    {
      $this->SZUID = $SZUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZURL()
    {
      return $this->SZURL;
    }

    /**
     * @param string $SZURL
     * @return \Axess\Dci4Wtp\D4WTPCOMPANY
     */
    public function setSZURL($SZURL)
    {
      $this->SZURL = $SZURL;
      return $this;
    }

}
