<?php

namespace Axess\Dci4Wtp;

class D4WTPGETPROMPTLISTREQUEST
{

    /**
     * @var ArrayOfD4WTPPROMPTTYPELIST $ACTPROMPTTYPELIST
     */
    protected $ACTPROMPTTYPELIST = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPROMPTTYPELIST
     */
    public function getACTPROMPTTYPELIST()
    {
      return $this->ACTPROMPTTYPELIST;
    }

    /**
     * @param ArrayOfD4WTPPROMPTTYPELIST $ACTPROMPTTYPELIST
     * @return \Axess\Dci4Wtp\D4WTPGETPROMPTLISTREQUEST
     */
    public function setACTPROMPTTYPELIST($ACTPROMPTTYPELIST)
    {
      $this->ACTPROMPTTYPELIST = $ACTPROMPTTYPELIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETPROMPTLISTREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
