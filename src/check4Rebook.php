<?php

namespace Axess\Dci4Wtp;

class check4Rebook
{

    /**
     * @var D4WTPCHECK4REBOOKREQUEST $i_ctCheck4RebookReq
     */
    protected $i_ctCheck4RebookReq = null;

    /**
     * @param D4WTPCHECK4REBOOKREQUEST $i_ctCheck4RebookReq
     */
    public function __construct($i_ctCheck4RebookReq)
    {
      $this->i_ctCheck4RebookReq = $i_ctCheck4RebookReq;
    }

    /**
     * @return D4WTPCHECK4REBOOKREQUEST
     */
    public function getI_ctCheck4RebookReq()
    {
      return $this->i_ctCheck4RebookReq;
    }

    /**
     * @param D4WTPCHECK4REBOOKREQUEST $i_ctCheck4RebookReq
     * @return \Axess\Dci4Wtp\check4Rebook
     */
    public function setI_ctCheck4RebookReq($i_ctCheck4RebookReq)
    {
      $this->i_ctCheck4RebookReq = $i_ctCheck4RebookReq;
      return $this;
    }

}
