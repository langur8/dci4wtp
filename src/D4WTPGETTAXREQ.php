<?php

namespace Axess\Dci4Wtp;

class D4WTPGETTAXREQ
{

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NRENTALITEMNO
     */
    protected $NRENTALITEMNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTICKETTYPE
     */
    protected $NTICKETTYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPGETTAXREQ
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNO()
    {
      return $this->NRENTALITEMNO;
    }

    /**
     * @param float $NRENTALITEMNO
     * @return \Axess\Dci4Wtp\D4WTPGETTAXREQ
     */
    public function setNRENTALITEMNO($NRENTALITEMNO)
    {
      $this->NRENTALITEMNO = $NRENTALITEMNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETTAXREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPE()
    {
      return $this->NTICKETTYPE;
    }

    /**
     * @param float $NTICKETTYPE
     * @return \Axess\Dci4Wtp\D4WTPGETTAXREQ
     */
    public function setNTICKETTYPE($NTICKETTYPE)
    {
      $this->NTICKETTYPE = $NTICKETTYPE;
      return $this;
    }

}
