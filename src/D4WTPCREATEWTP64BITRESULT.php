<?php

namespace Axess\Dci4Wtp;

class D4WTPCREATEWTP64BITRESULT
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZWTPNO64
     */
    protected $SZWTPNO64 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCREATEWTP64BITRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCREATEWTP64BITRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNO64()
    {
      return $this->SZWTPNO64;
    }

    /**
     * @param string $SZWTPNO64
     * @return \Axess\Dci4Wtp\D4WTPCREATEWTP64BITRESULT
     */
    public function setSZWTPNO64($SZWTPNO64)
    {
      $this->SZWTPNO64 = $SZWTPNO64;
      return $this;
    }

}
