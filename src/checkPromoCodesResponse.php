<?php

namespace Axess\Dci4Wtp;

class checkPromoCodesResponse
{

    /**
     * @var D4WTPCHECKPROMOCODESRESULT $checkPromoCodesResult
     */
    protected $checkPromoCodesResult = null;

    /**
     * @param D4WTPCHECKPROMOCODESRESULT $checkPromoCodesResult
     */
    public function __construct($checkPromoCodesResult)
    {
      $this->checkPromoCodesResult = $checkPromoCodesResult;
    }

    /**
     * @return D4WTPCHECKPROMOCODESRESULT
     */
    public function getCheckPromoCodesResult()
    {
      return $this->checkPromoCodesResult;
    }

    /**
     * @param D4WTPCHECKPROMOCODESRESULT $checkPromoCodesResult
     * @return \Axess\Dci4Wtp\checkPromoCodesResponse
     */
    public function setCheckPromoCodesResult($checkPromoCodesResult)
    {
      $this->checkPromoCodesResult = $checkPromoCodesResult;
      return $this;
    }

}
