<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPROMPTS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPROMPTS[] $D4WTPPROMPTS
     */
    protected $D4WTPPROMPTS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPROMPTS[]
     */
    public function getD4WTPPROMPTS()
    {
      return $this->D4WTPPROMPTS;
    }

    /**
     * @param D4WTPPROMPTS[] $D4WTPPROMPTS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPROMPTS
     */
    public function setD4WTPPROMPTS(array $D4WTPPROMPTS = null)
    {
      $this->D4WTPPROMPTS = $D4WTPPROMPTS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPROMPTS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPROMPTS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPROMPTS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPROMPTS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPROMPTS[] = $value;
      } else {
        $this->D4WTPPROMPTS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPROMPTS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPROMPTS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPROMPTS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPROMPTS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPROMPTS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPROMPTS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPROMPTS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPROMPTS);
    }

}
