<?php

namespace Axess\Dci4Wtp;

class D4WTPCANCELTICKETREQUEST
{

    /**
     * @var float $NAPPTRANSLOGID
     */
    protected $NAPPTRANSLOGID = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTICKETPOSNO
     */
    protected $NTICKETPOSNO = null;

    /**
     * @var float $NTICKETPROJNO
     */
    protected $NTICKETPROJNO = null;

    /**
     * @var float $NTICKETSERIALNO
     */
    protected $NTICKETSERIALNO = null;

    /**
     * @var string $SZCODINGMODE
     */
    protected $SZCODINGMODE = null;

    /**
     * @var string $SZDCCONTENT
     */
    protected $SZDCCONTENT = null;

    /**
     * @var string $SZDRIVERTYPE
     */
    protected $SZDRIVERTYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNAPPTRANSLOGID()
    {
      return $this->NAPPTRANSLOGID;
    }

    /**
     * @param float $NAPPTRANSLOGID
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETREQUEST
     */
    public function setNAPPTRANSLOGID($NAPPTRANSLOGID)
    {
      $this->NAPPTRANSLOGID = $NAPPTRANSLOGID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETPOSNO()
    {
      return $this->NTICKETPOSNO;
    }

    /**
     * @param float $NTICKETPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETREQUEST
     */
    public function setNTICKETPOSNO($NTICKETPOSNO)
    {
      $this->NTICKETPOSNO = $NTICKETPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETPROJNO()
    {
      return $this->NTICKETPROJNO;
    }

    /**
     * @param float $NTICKETPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETREQUEST
     */
    public function setNTICKETPROJNO($NTICKETPROJNO)
    {
      $this->NTICKETPROJNO = $NTICKETPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETSERIALNO()
    {
      return $this->NTICKETSERIALNO;
    }

    /**
     * @param float $NTICKETSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETREQUEST
     */
    public function setNTICKETSERIALNO($NTICKETSERIALNO)
    {
      $this->NTICKETSERIALNO = $NTICKETSERIALNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGMODE()
    {
      return $this->SZCODINGMODE;
    }

    /**
     * @param string $SZCODINGMODE
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETREQUEST
     */
    public function setSZCODINGMODE($SZCODINGMODE)
    {
      $this->SZCODINGMODE = $SZCODINGMODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDCCONTENT()
    {
      return $this->SZDCCONTENT;
    }

    /**
     * @param string $SZDCCONTENT
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETREQUEST
     */
    public function setSZDCCONTENT($SZDCCONTENT)
    {
      $this->SZDCCONTENT = $SZDCCONTENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDRIVERTYPE()
    {
      return $this->SZDRIVERTYPE;
    }

    /**
     * @param string $SZDRIVERTYPE
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETREQUEST
     */
    public function setSZDRIVERTYPE($SZDRIVERTYPE)
    {
      $this->SZDRIVERTYPE = $SZDRIVERTYPE;
      return $this;
    }

}
