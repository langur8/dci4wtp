<?php

namespace Axess\Dci4Wtp;

class assignPersToCompany
{

    /**
     * @var D4WTPASSIGNPERSTOCOMPANYREQ $i_ctAssignPersToCompanyReq
     */
    protected $i_ctAssignPersToCompanyReq = null;

    /**
     * @param D4WTPASSIGNPERSTOCOMPANYREQ $i_ctAssignPersToCompanyReq
     */
    public function __construct($i_ctAssignPersToCompanyReq)
    {
      $this->i_ctAssignPersToCompanyReq = $i_ctAssignPersToCompanyReq;
    }

    /**
     * @return D4WTPASSIGNPERSTOCOMPANYREQ
     */
    public function getI_ctAssignPersToCompanyReq()
    {
      return $this->i_ctAssignPersToCompanyReq;
    }

    /**
     * @param D4WTPASSIGNPERSTOCOMPANYREQ $i_ctAssignPersToCompanyReq
     * @return \Axess\Dci4Wtp\assignPersToCompany
     */
    public function setI_ctAssignPersToCompanyReq($i_ctAssignPersToCompanyReq)
    {
      $this->i_ctAssignPersToCompanyReq = $i_ctAssignPersToCompanyReq;
      return $this;
    }

}
