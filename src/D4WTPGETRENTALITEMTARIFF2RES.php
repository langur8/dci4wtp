<?php

namespace Axess\Dci4Wtp;

class D4WTPGETRENTALITEMTARIFF2RES
{

    /**
     * @var ArrayOfD4WTPRENTALITEMTARIFF2 $ACTRENTALITEMTARIFF
     */
    protected $ACTRENTALITEMTARIFF = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPRENTALITEMTARIFF2
     */
    public function getACTRENTALITEMTARIFF()
    {
      return $this->ACTRENTALITEMTARIFF;
    }

    /**
     * @param ArrayOfD4WTPRENTALITEMTARIFF2 $ACTRENTALITEMTARIFF
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFF2RES
     */
    public function setACTRENTALITEMTARIFF($ACTRENTALITEMTARIFF)
    {
      $this->ACTRENTALITEMTARIFF = $ACTRENTALITEMTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFF2RES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFF2RES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
