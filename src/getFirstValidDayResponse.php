<?php

namespace Axess\Dci4Wtp;

class getFirstValidDayResponse
{

    /**
     * @var D4WTPGETFIRSTVALIDDAYRESULT $getFirstValidDayResult
     */
    protected $getFirstValidDayResult = null;

    /**
     * @param D4WTPGETFIRSTVALIDDAYRESULT $getFirstValidDayResult
     */
    public function __construct($getFirstValidDayResult)
    {
      $this->getFirstValidDayResult = $getFirstValidDayResult;
    }

    /**
     * @return D4WTPGETFIRSTVALIDDAYRESULT
     */
    public function getGetFirstValidDayResult()
    {
      return $this->getFirstValidDayResult;
    }

    /**
     * @param D4WTPGETFIRSTVALIDDAYRESULT $getFirstValidDayResult
     * @return \Axess\Dci4Wtp\getFirstValidDayResponse
     */
    public function setGetFirstValidDayResult($getFirstValidDayResult)
    {
      $this->getFirstValidDayResult = $getFirstValidDayResult;
      return $this;
    }

}
