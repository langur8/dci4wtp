<?php

namespace Axess\Dci4Wtp;

class MWSTSATZ
{

    /**
     * @var float $BISMULTITAX
     */
    protected $BISMULTITAX = null;

    /**
     * @var float $BISUSEDONNETTO
     */
    protected $BISUSEDONNETTO = null;

    /**
     * @var float $FPERCENT
     */
    protected $FPERCENT = null;

    /**
     * @var float $FPERCENT1
     */
    protected $FPERCENT1 = null;

    /**
     * @var float $NTAXID
     */
    protected $NTAXID = null;

    /**
     * @var string $SZDATEEND
     */
    protected $SZDATEEND = null;

    /**
     * @var string $SZDATESTART
     */
    protected $SZDATESTART = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZFISCALTYPE
     */
    protected $SZFISCALTYPE = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZNAME1
     */
    protected $SZNAME1 = null;

    /**
     * @var string $SZNAME2
     */
    protected $SZNAME2 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISMULTITAX()
    {
      return $this->BISMULTITAX;
    }

    /**
     * @param float $BISMULTITAX
     * @return \Axess\Dci4Wtp\MWSTSATZ
     */
    public function setBISMULTITAX($BISMULTITAX)
    {
      $this->BISMULTITAX = $BISMULTITAX;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISUSEDONNETTO()
    {
      return $this->BISUSEDONNETTO;
    }

    /**
     * @param float $BISUSEDONNETTO
     * @return \Axess\Dci4Wtp\MWSTSATZ
     */
    public function setBISUSEDONNETTO($BISUSEDONNETTO)
    {
      $this->BISUSEDONNETTO = $BISUSEDONNETTO;
      return $this;
    }

    /**
     * @return float
     */
    public function getFPERCENT()
    {
      return $this->FPERCENT;
    }

    /**
     * @param float $FPERCENT
     * @return \Axess\Dci4Wtp\MWSTSATZ
     */
    public function setFPERCENT($FPERCENT)
    {
      $this->FPERCENT = $FPERCENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFPERCENT1()
    {
      return $this->FPERCENT1;
    }

    /**
     * @param float $FPERCENT1
     * @return \Axess\Dci4Wtp\MWSTSATZ
     */
    public function setFPERCENT1($FPERCENT1)
    {
      $this->FPERCENT1 = $FPERCENT1;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTAXID()
    {
      return $this->NTAXID;
    }

    /**
     * @param float $NTAXID
     * @return \Axess\Dci4Wtp\MWSTSATZ
     */
    public function setNTAXID($NTAXID)
    {
      $this->NTAXID = $NTAXID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEEND()
    {
      return $this->SZDATEEND;
    }

    /**
     * @param string $SZDATEEND
     * @return \Axess\Dci4Wtp\MWSTSATZ
     */
    public function setSZDATEEND($SZDATEEND)
    {
      $this->SZDATEEND = $SZDATEEND;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATESTART()
    {
      return $this->SZDATESTART;
    }

    /**
     * @param string $SZDATESTART
     * @return \Axess\Dci4Wtp\MWSTSATZ
     */
    public function setSZDATESTART($SZDATESTART)
    {
      $this->SZDATESTART = $SZDATESTART;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\MWSTSATZ
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFISCALTYPE()
    {
      return $this->SZFISCALTYPE;
    }

    /**
     * @param string $SZFISCALTYPE
     * @return \Axess\Dci4Wtp\MWSTSATZ
     */
    public function setSZFISCALTYPE($SZFISCALTYPE)
    {
      $this->SZFISCALTYPE = $SZFISCALTYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\MWSTSATZ
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME1()
    {
      return $this->SZNAME1;
    }

    /**
     * @param string $SZNAME1
     * @return \Axess\Dci4Wtp\MWSTSATZ
     */
    public function setSZNAME1($SZNAME1)
    {
      $this->SZNAME1 = $SZNAME1;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME2()
    {
      return $this->SZNAME2;
    }

    /**
     * @param string $SZNAME2
     * @return \Axess\Dci4Wtp\MWSTSATZ
     */
    public function setSZNAME2($SZNAME2)
    {
      $this->SZNAME2 = $SZNAME2;
      return $this;
    }

}
