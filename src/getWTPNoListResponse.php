<?php

namespace Axess\Dci4Wtp;

class getWTPNoListResponse
{

    /**
     * @var D4WTPWTPNOLISTRESULT $getWTPNoListResult
     */
    protected $getWTPNoListResult = null;

    /**
     * @param D4WTPWTPNOLISTRESULT $getWTPNoListResult
     */
    public function __construct($getWTPNoListResult)
    {
      $this->getWTPNoListResult = $getWTPNoListResult;
    }

    /**
     * @return D4WTPWTPNOLISTRESULT
     */
    public function getGetWTPNoListResult()
    {
      return $this->getWTPNoListResult;
    }

    /**
     * @param D4WTPWTPNOLISTRESULT $getWTPNoListResult
     * @return \Axess\Dci4Wtp\getWTPNoListResponse
     */
    public function setGetWTPNoListResult($getWTPNoListResult)
    {
      $this->getWTPNoListResult = $getWTPNoListResult;
      return $this;
    }

}
