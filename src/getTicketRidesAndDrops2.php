<?php

namespace Axess\Dci4Wtp;

class getTicketRidesAndDrops2
{

    /**
     * @var D4WTPGETTCKTRIDESDROPSREQ2 $i_ctGetTcktRidesDropsReq
     */
    protected $i_ctGetTcktRidesDropsReq = null;

    /**
     * @param D4WTPGETTCKTRIDESDROPSREQ2 $i_ctGetTcktRidesDropsReq
     */
    public function __construct($i_ctGetTcktRidesDropsReq)
    {
      $this->i_ctGetTcktRidesDropsReq = $i_ctGetTcktRidesDropsReq;
    }

    /**
     * @return D4WTPGETTCKTRIDESDROPSREQ2
     */
    public function getI_ctGetTcktRidesDropsReq()
    {
      return $this->i_ctGetTcktRidesDropsReq;
    }

    /**
     * @param D4WTPGETTCKTRIDESDROPSREQ2 $i_ctGetTcktRidesDropsReq
     * @return \Axess\Dci4Wtp\getTicketRidesAndDrops2
     */
    public function setI_ctGetTcktRidesDropsReq($i_ctGetTcktRidesDropsReq)
    {
      $this->i_ctGetTcktRidesDropsReq = $i_ctGetTcktRidesDropsReq;
      return $this;
    }

}
