<?php

namespace Axess\Dci4Wtp;

class D4WTPAXCOUNTMEMBER
{

    /**
     * @var float $BISACTIVE
     */
    protected $BISACTIVE = null;

    /**
     * @var float $BISCLOSED
     */
    protected $BISCLOSED = null;

    /**
     * @var float $NAXCOUNTKONTOMEMBERNR
     */
    protected $NAXCOUNTKONTOMEMBERNR = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var string $SZCLOSINGDATE
     */
    protected $SZCLOSINGDATE = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    /**
     * @var string $SZOPENDATE
     */
    protected $SZOPENDATE = null;

    /**
     * @var string $SZWTPNUMBER
     */
    protected $SZWTPNUMBER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISACTIVE()
    {
      return $this->BISACTIVE;
    }

    /**
     * @param float $BISACTIVE
     * @return \Axess\Dci4Wtp\D4WTPAXCOUNTMEMBER
     */
    public function setBISACTIVE($BISACTIVE)
    {
      $this->BISACTIVE = $BISACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISCLOSED()
    {
      return $this->BISCLOSED;
    }

    /**
     * @param float $BISCLOSED
     * @return \Axess\Dci4Wtp\D4WTPAXCOUNTMEMBER
     */
    public function setBISCLOSED($BISCLOSED)
    {
      $this->BISCLOSED = $BISCLOSED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAXCOUNTKONTOMEMBERNR()
    {
      return $this->NAXCOUNTKONTOMEMBERNR;
    }

    /**
     * @param float $NAXCOUNTKONTOMEMBERNR
     * @return \Axess\Dci4Wtp\D4WTPAXCOUNTMEMBER
     */
    public function setNAXCOUNTKONTOMEMBERNR($NAXCOUNTKONTOMEMBERNR)
    {
      $this->NAXCOUNTKONTOMEMBERNR = $NAXCOUNTKONTOMEMBERNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPAXCOUNTMEMBER
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPAXCOUNTMEMBER
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPAXCOUNTMEMBER
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCLOSINGDATE()
    {
      return $this->SZCLOSINGDATE;
    }

    /**
     * @param string $SZCLOSINGDATE
     * @return \Axess\Dci4Wtp\D4WTPAXCOUNTMEMBER
     */
    public function setSZCLOSINGDATE($SZCLOSINGDATE)
    {
      $this->SZCLOSINGDATE = $SZCLOSINGDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPAXCOUNTMEMBER
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPAXCOUNTMEMBER
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPAXCOUNTMEMBER
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZOPENDATE()
    {
      return $this->SZOPENDATE;
    }

    /**
     * @param string $SZOPENDATE
     * @return \Axess\Dci4Wtp\D4WTPAXCOUNTMEMBER
     */
    public function setSZOPENDATE($SZOPENDATE)
    {
      $this->SZOPENDATE = $SZOPENDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNUMBER()
    {
      return $this->SZWTPNUMBER;
    }

    /**
     * @param string $SZWTPNUMBER
     * @return \Axess\Dci4Wtp\D4WTPAXCOUNTMEMBER
     */
    public function setSZWTPNUMBER($SZWTPNUMBER)
    {
      $this->SZWTPNUMBER = $SZWTPNUMBER;
      return $this;
    }

}
