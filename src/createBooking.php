<?php

namespace Axess\Dci4Wtp;

class createBooking
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var CreateBookingRequest $request
     */
    protected $request = null;

    /**
     * @param float $i_nSessionID
     * @param CreateBookingRequest $request
     */
    public function __construct($i_nSessionID, $request)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->request = $request;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\createBooking
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return CreateBookingRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param CreateBookingRequest $request
     * @return \Axess\Dci4Wtp\createBooking
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
