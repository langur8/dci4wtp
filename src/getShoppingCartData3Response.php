<?php

namespace Axess\Dci4Wtp;

class getShoppingCartData3Response
{

    /**
     * @var D4WTPGETSHOPCARTDATA3RESULT $getShoppingCartData3Result
     */
    protected $getShoppingCartData3Result = null;

    /**
     * @param D4WTPGETSHOPCARTDATA3RESULT $getShoppingCartData3Result
     */
    public function __construct($getShoppingCartData3Result)
    {
      $this->getShoppingCartData3Result = $getShoppingCartData3Result;
    }

    /**
     * @return D4WTPGETSHOPCARTDATA3RESULT
     */
    public function getGetShoppingCartData3Result()
    {
      return $this->getShoppingCartData3Result;
    }

    /**
     * @param D4WTPGETSHOPCARTDATA3RESULT $getShoppingCartData3Result
     * @return \Axess\Dci4Wtp\getShoppingCartData3Response
     */
    public function setGetShoppingCartData3Result($getShoppingCartData3Result)
    {
      $this->getShoppingCartData3Result = $getShoppingCartData3Result;
      return $this;
    }

}
