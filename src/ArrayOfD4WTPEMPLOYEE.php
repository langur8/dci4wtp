<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPEMPLOYEE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPEMPLOYEE[] $D4WTPEMPLOYEE
     */
    protected $D4WTPEMPLOYEE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPEMPLOYEE[]
     */
    public function getD4WTPEMPLOYEE()
    {
      return $this->D4WTPEMPLOYEE;
    }

    /**
     * @param D4WTPEMPLOYEE[] $D4WTPEMPLOYEE
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPEMPLOYEE
     */
    public function setD4WTPEMPLOYEE(array $D4WTPEMPLOYEE = null)
    {
      $this->D4WTPEMPLOYEE = $D4WTPEMPLOYEE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPEMPLOYEE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPEMPLOYEE
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPEMPLOYEE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPEMPLOYEE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPEMPLOYEE[] = $value;
      } else {
        $this->D4WTPEMPLOYEE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPEMPLOYEE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPEMPLOYEE Return the current element
     */
    public function current()
    {
      return current($this->D4WTPEMPLOYEE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPEMPLOYEE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPEMPLOYEE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPEMPLOYEE);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPEMPLOYEE Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPEMPLOYEE);
    }

}
