<?php

namespace Axess\Dci4Wtp;

class ArrayOfMWSTSATZ implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var MWSTSATZ[] $MWSTSATZ
     */
    protected $MWSTSATZ = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return MWSTSATZ[]
     */
    public function getMWSTSATZ()
    {
      return $this->MWSTSATZ;
    }

    /**
     * @param MWSTSATZ[] $MWSTSATZ
     * @return \Axess\Dci4Wtp\ArrayOfMWSTSATZ
     */
    public function setMWSTSATZ(array $MWSTSATZ = null)
    {
      $this->MWSTSATZ = $MWSTSATZ;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->MWSTSATZ[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return MWSTSATZ
     */
    public function offsetGet($offset)
    {
      return $this->MWSTSATZ[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param MWSTSATZ $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->MWSTSATZ[] = $value;
      } else {
        $this->MWSTSATZ[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->MWSTSATZ[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return MWSTSATZ Return the current element
     */
    public function current()
    {
      return current($this->MWSTSATZ);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->MWSTSATZ);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->MWSTSATZ);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->MWSTSATZ);
    }

    /**
     * Countable implementation
     *
     * @return MWSTSATZ Return count of elements
     */
    public function count()
    {
      return count($this->MWSTSATZ);
    }

}
