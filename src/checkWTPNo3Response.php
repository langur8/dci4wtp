<?php

namespace Axess\Dci4Wtp;

class checkWTPNo3Response
{

    /**
     * @var D4WTPCHECKWTPNO3RESULT $checkWTPNo3Result
     */
    protected $checkWTPNo3Result = null;

    /**
     * @param D4WTPCHECKWTPNO3RESULT $checkWTPNo3Result
     */
    public function __construct($checkWTPNo3Result)
    {
      $this->checkWTPNo3Result = $checkWTPNo3Result;
    }

    /**
     * @return D4WTPCHECKWTPNO3RESULT
     */
    public function getCheckWTPNo3Result()
    {
      return $this->checkWTPNo3Result;
    }

    /**
     * @param D4WTPCHECKWTPNO3RESULT $checkWTPNo3Result
     * @return \Axess\Dci4Wtp\checkWTPNo3Response
     */
    public function setCheckWTPNo3Result($checkWTPNo3Result)
    {
      $this->checkWTPNo3Result = $checkWTPNo3Result;
      return $this;
    }

}
