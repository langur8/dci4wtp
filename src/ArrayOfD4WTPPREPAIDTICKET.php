<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPREPAIDTICKET implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPREPAIDTICKET[] $D4WTPPREPAIDTICKET
     */
    protected $D4WTPPREPAIDTICKET = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPREPAIDTICKET[]
     */
    public function getD4WTPPREPAIDTICKET()
    {
      return $this->D4WTPPREPAIDTICKET;
    }

    /**
     * @param D4WTPPREPAIDTICKET[] $D4WTPPREPAIDTICKET
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPREPAIDTICKET
     */
    public function setD4WTPPREPAIDTICKET(array $D4WTPPREPAIDTICKET = null)
    {
      $this->D4WTPPREPAIDTICKET = $D4WTPPREPAIDTICKET;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPREPAIDTICKET[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPREPAIDTICKET
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPREPAIDTICKET[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPREPAIDTICKET $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPREPAIDTICKET[] = $value;
      } else {
        $this->D4WTPPREPAIDTICKET[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPREPAIDTICKET[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPREPAIDTICKET Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPREPAIDTICKET);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPREPAIDTICKET);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPREPAIDTICKET);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPREPAIDTICKET);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPREPAIDTICKET Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPREPAIDTICKET);
    }

}
