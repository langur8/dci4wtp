<?php

namespace Axess\Dci4Wtp;

class D4WTPGROUPS2REQ
{

    /**
     * @var float $BINCLPACKAGES
     */
    protected $BINCLPACKAGES = null;

    /**
     * @var float $BINCLSTANDARTICLE
     */
    protected $BINCLSTANDARTICLE = null;

    /**
     * @var float $BINCLTICKETTYPES
     */
    protected $BINCLTICKETTYPES = null;

    /**
     * @var float $BINCLWARE
     */
    protected $BINCLWARE = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBINCLPACKAGES()
    {
      return $this->BINCLPACKAGES;
    }

    /**
     * @param float $BINCLPACKAGES
     * @return \Axess\Dci4Wtp\D4WTPGROUPS2REQ
     */
    public function setBINCLPACKAGES($BINCLPACKAGES)
    {
      $this->BINCLPACKAGES = $BINCLPACKAGES;
      return $this;
    }

    /**
     * @return float
     */
    public function getBINCLSTANDARTICLE()
    {
      return $this->BINCLSTANDARTICLE;
    }

    /**
     * @param float $BINCLSTANDARTICLE
     * @return \Axess\Dci4Wtp\D4WTPGROUPS2REQ
     */
    public function setBINCLSTANDARTICLE($BINCLSTANDARTICLE)
    {
      $this->BINCLSTANDARTICLE = $BINCLSTANDARTICLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBINCLTICKETTYPES()
    {
      return $this->BINCLTICKETTYPES;
    }

    /**
     * @param float $BINCLTICKETTYPES
     * @return \Axess\Dci4Wtp\D4WTPGROUPS2REQ
     */
    public function setBINCLTICKETTYPES($BINCLTICKETTYPES)
    {
      $this->BINCLTICKETTYPES = $BINCLTICKETTYPES;
      return $this;
    }

    /**
     * @return float
     */
    public function getBINCLWARE()
    {
      return $this->BINCLWARE;
    }

    /**
     * @param float $BINCLWARE
     * @return \Axess\Dci4Wtp\D4WTPGROUPS2REQ
     */
    public function setBINCLWARE($BINCLWARE)
    {
      $this->BINCLWARE = $BINCLWARE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGROUPS2REQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPGROUPS2REQ
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPGROUPS2REQ
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

}
