<?php

namespace Axess\Dci4Wtp;

class D4WTPMANAGEEMPLOYEE
{

    /**
     * @var ArrayOfD4WTPEMPLOYEE $ACTEMPLOYEE
     */
    protected $ACTEMPLOYEE = null;

    /**
     * @var string $SZMODE
     */
    protected $SZMODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPEMPLOYEE
     */
    public function getACTEMPLOYEE()
    {
      return $this->ACTEMPLOYEE;
    }

    /**
     * @param ArrayOfD4WTPEMPLOYEE $ACTEMPLOYEE
     * @return \Axess\Dci4Wtp\D4WTPMANAGEEMPLOYEE
     */
    public function setACTEMPLOYEE($ACTEMPLOYEE)
    {
      $this->ACTEMPLOYEE = $ACTEMPLOYEE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMODE()
    {
      return $this->SZMODE;
    }

    /**
     * @param string $SZMODE
     * @return \Axess\Dci4Wtp\D4WTPMANAGEEMPLOYEE
     */
    public function setSZMODE($SZMODE)
    {
      $this->SZMODE = $SZMODE;
      return $this;
    }

}
