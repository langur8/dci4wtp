<?php

namespace Axess\Dci4Wtp;

class D4WTPERSONDATA4
{

    /**
     * @var float $FSOLELENGTH
     */
    protected $FSOLELENGTH = null;

    /**
     * @var float $NLANGUAGEID
     */
    protected $NLANGUAGEID = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NSKILEVEL
     */
    protected $NSKILEVEL = null;

    /**
     * @var float $NSOLELENGTHUNIT
     */
    protected $NSOLELENGTHUNIT = null;

    /**
     * @var string $SZAREA
     */
    protected $SZAREA = null;

    /**
     * @var string $SZCITY
     */
    protected $SZCITY = null;

    /**
     * @var string $SZCOUNTRY
     */
    protected $SZCOUNTRY = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    /**
     * @var string $SZDATEOFBIRTH
     */
    protected $SZDATEOFBIRTH = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZGENDER
     */
    protected $SZGENDER = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZMOBILE
     */
    protected $SZMOBILE = null;

    /**
     * @var string $SZPHONE
     */
    protected $SZPHONE = null;

    /**
     * @var string $SZSALUTATION
     */
    protected $SZSALUTATION = null;

    /**
     * @var string $SZSTREET
     */
    protected $SZSTREET = null;

    /**
     * @var string $SZSTREETNUMBER
     */
    protected $SZSTREETNUMBER = null;

    /**
     * @var string $SZTITLE
     */
    protected $SZTITLE = null;

    /**
     * @var string $SZZIPCODE
     */
    protected $SZZIPCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFSOLELENGTH()
    {
      return $this->FSOLELENGTH;
    }

    /**
     * @param float $FSOLELENGTH
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setFSOLELENGTH($FSOLELENGTH)
    {
      $this->FSOLELENGTH = $FSOLELENGTH;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLANGUAGEID()
    {
      return $this->NLANGUAGEID;
    }

    /**
     * @param float $NLANGUAGEID
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setNLANGUAGEID($NLANGUAGEID)
    {
      $this->NLANGUAGEID = $NLANGUAGEID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSKILEVEL()
    {
      return $this->NSKILEVEL;
    }

    /**
     * @param float $NSKILEVEL
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setNSKILEVEL($NSKILEVEL)
    {
      $this->NSKILEVEL = $NSKILEVEL;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSOLELENGTHUNIT()
    {
      return $this->NSOLELENGTHUNIT;
    }

    /**
     * @param float $NSOLELENGTHUNIT
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setNSOLELENGTHUNIT($NSOLELENGTHUNIT)
    {
      $this->NSOLELENGTHUNIT = $NSOLELENGTHUNIT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAREA()
    {
      return $this->SZAREA;
    }

    /**
     * @param string $SZAREA
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZAREA($SZAREA)
    {
      $this->SZAREA = $SZAREA;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCITY()
    {
      return $this->SZCITY;
    }

    /**
     * @param string $SZCITY
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZCITY($SZCITY)
    {
      $this->SZCITY = $SZCITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRY()
    {
      return $this->SZCOUNTRY;
    }

    /**
     * @param string $SZCOUNTRY
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZCOUNTRY($SZCOUNTRY)
    {
      $this->SZCOUNTRY = $SZCOUNTRY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEOFBIRTH()
    {
      return $this->SZDATEOFBIRTH;
    }

    /**
     * @param string $SZDATEOFBIRTH
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZDATEOFBIRTH($SZDATEOFBIRTH)
    {
      $this->SZDATEOFBIRTH = $SZDATEOFBIRTH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGENDER()
    {
      return $this->SZGENDER;
    }

    /**
     * @param string $SZGENDER
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZGENDER($SZGENDER)
    {
      $this->SZGENDER = $SZGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMOBILE()
    {
      return $this->SZMOBILE;
    }

    /**
     * @param string $SZMOBILE
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZMOBILE($SZMOBILE)
    {
      $this->SZMOBILE = $SZMOBILE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPHONE()
    {
      return $this->SZPHONE;
    }

    /**
     * @param string $SZPHONE
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZPHONE($SZPHONE)
    {
      $this->SZPHONE = $SZPHONE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSALUTATION()
    {
      return $this->SZSALUTATION;
    }

    /**
     * @param string $SZSALUTATION
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZSALUTATION($SZSALUTATION)
    {
      $this->SZSALUTATION = $SZSALUTATION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREET()
    {
      return $this->SZSTREET;
    }

    /**
     * @param string $SZSTREET
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZSTREET($SZSTREET)
    {
      $this->SZSTREET = $SZSTREET;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREETNUMBER()
    {
      return $this->SZSTREETNUMBER;
    }

    /**
     * @param string $SZSTREETNUMBER
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZSTREETNUMBER($SZSTREETNUMBER)
    {
      $this->SZSTREETNUMBER = $SZSTREETNUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTITLE()
    {
      return $this->SZTITLE;
    }

    /**
     * @param string $SZTITLE
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZTITLE($SZTITLE)
    {
      $this->SZTITLE = $SZTITLE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZZIPCODE()
    {
      return $this->SZZIPCODE;
    }

    /**
     * @param string $SZZIPCODE
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA4
     */
    public function setSZZIPCODE($SZZIPCODE)
    {
      $this->SZZIPCODE = $SZZIPCODE;
      return $this;
    }

}
