<?php

namespace Axess\Dci4Wtp;

class D4WTPPROMPTTYPES
{

    /**
     * @var ArrayOfD4WTPPROMPTS $ACTPROMPTS
     */
    protected $ACTPROMPTS = null;

    /**
     * @var float $NPROMPTTYPENO
     */
    protected $NPROMPTTYPENO = null;

    /**
     * @var string $SZPROMPTTYPENAME
     */
    protected $SZPROMPTTYPENAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPROMPTS
     */
    public function getACTPROMPTS()
    {
      return $this->ACTPROMPTS;
    }

    /**
     * @param ArrayOfD4WTPPROMPTS $ACTPROMPTS
     * @return \Axess\Dci4Wtp\D4WTPPROMPTTYPES
     */
    public function setACTPROMPTS($ACTPROMPTS)
    {
      $this->ACTPROMPTS = $ACTPROMPTS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROMPTTYPENO()
    {
      return $this->NPROMPTTYPENO;
    }

    /**
     * @param float $NPROMPTTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPROMPTTYPES
     */
    public function setNPROMPTTYPENO($NPROMPTTYPENO)
    {
      $this->NPROMPTTYPENO = $NPROMPTTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPROMPTTYPENAME()
    {
      return $this->SZPROMPTTYPENAME;
    }

    /**
     * @param string $SZPROMPTTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPPROMPTTYPES
     */
    public function setSZPROMPTTYPENAME($SZPROMPTTYPENAME)
    {
      $this->SZPROMPTTYPENAME = $SZPROMPTTYPENAME;
      return $this;
    }

}
