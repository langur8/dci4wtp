<?php

namespace Axess\Dci4Wtp;

class D4WTPCHECKBONUSPOINTSREQ
{

    /**
     * @var ArrayOfD4WTPPRODUCTORDER7 $ACTPRODUCTORDER
     */
    protected $ACTPRODUCTORDER = null;

    /**
     * @var float $NCUSTOMERACCOUNTNO
     */
    protected $NCUSTOMERACCOUNTNO = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNR
     */
    protected $NTRANSNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPRODUCTORDER7
     */
    public function getACTPRODUCTORDER()
    {
      return $this->ACTPRODUCTORDER;
    }

    /**
     * @param ArrayOfD4WTPPRODUCTORDER7 $ACTPRODUCTORDER
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSREQ
     */
    public function setACTPRODUCTORDER($ACTPRODUCTORDER)
    {
      $this->ACTPRODUCTORDER = $ACTPRODUCTORDER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTOMERACCOUNTNO()
    {
      return $this->NCUSTOMERACCOUNTNO;
    }

    /**
     * @param float $NCUSTOMERACCOUNTNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSREQ
     */
    public function setNCUSTOMERACCOUNTNO($NCUSTOMERACCOUNTNO)
    {
      $this->NCUSTOMERACCOUNTNO = $NCUSTOMERACCOUNTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSREQ
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNR()
    {
      return $this->NTRANSNR;
    }

    /**
     * @param float $NTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSREQ
     */
    public function setNTRANSNR($NTRANSNR)
    {
      $this->NTRANSNR = $NTRANSNR;
      return $this;
    }

}
