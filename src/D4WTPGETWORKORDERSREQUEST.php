<?php

namespace Axess\Dci4Wtp;

class D4WTPGETWORKORDERSREQUEST
{

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NFIRSTNROWS
     */
    protected $NFIRSTNROWS = null;

    /**
     * @var float $NMAXROWTOFETCH
     */
    protected $NMAXROWTOFETCH = null;

    /**
     * @var float $NMINROWTOFETCH
     */
    protected $NMINROWTOFETCH = null;

    /**
     * @var float $NPARENTWORKORDER
     */
    protected $NPARENTWORKORDER = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWORKORDER
     */
    protected $NWORKORDER = null;

    /**
     * @var float $NWORKORDERSTATUS
     */
    protected $NWORKORDERSTATUS = null;

    /**
     * @var string $SZDATEFROMFROM
     */
    protected $SZDATEFROMFROM = null;

    /**
     * @var string $SZDATEFROMTO
     */
    protected $SZDATEFROMTO = null;

    /**
     * @var string $SZDATETOFROM
     */
    protected $SZDATETOFROM = null;

    /**
     * @var string $SZDATETOTO
     */
    protected $SZDATETOTO = null;

    /**
     * @var string $SZEXTWORKORDER
     */
    protected $SZEXTWORKORDER = null;

    /**
     * @var string $SZORDERBYCOLUMN
     */
    protected $SZORDERBYCOLUMN = null;

    /**
     * @var string $SZORDERBYTYPE
     */
    protected $SZORDERBYTYPE = null;

    /**
     * @var string $SZTITLE
     */
    protected $SZTITLE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRSTNROWS()
    {
      return $this->NFIRSTNROWS;
    }

    /**
     * @param float $NFIRSTNROWS
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setNFIRSTNROWS($NFIRSTNROWS)
    {
      $this->NFIRSTNROWS = $NFIRSTNROWS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMAXROWTOFETCH()
    {
      return $this->NMAXROWTOFETCH;
    }

    /**
     * @param float $NMAXROWTOFETCH
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setNMAXROWTOFETCH($NMAXROWTOFETCH)
    {
      $this->NMAXROWTOFETCH = $NMAXROWTOFETCH;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMINROWTOFETCH()
    {
      return $this->NMINROWTOFETCH;
    }

    /**
     * @param float $NMINROWTOFETCH
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setNMINROWTOFETCH($NMINROWTOFETCH)
    {
      $this->NMINROWTOFETCH = $NMINROWTOFETCH;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPARENTWORKORDER()
    {
      return $this->NPARENTWORKORDER;
    }

    /**
     * @param float $NPARENTWORKORDER
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setNPARENTWORKORDER($NPARENTWORKORDER)
    {
      $this->NPARENTWORKORDER = $NPARENTWORKORDER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWORKORDER()
    {
      return $this->NWORKORDER;
    }

    /**
     * @param float $NWORKORDER
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setNWORKORDER($NWORKORDER)
    {
      $this->NWORKORDER = $NWORKORDER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWORKORDERSTATUS()
    {
      return $this->NWORKORDERSTATUS;
    }

    /**
     * @param float $NWORKORDERSTATUS
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setNWORKORDERSTATUS($NWORKORDERSTATUS)
    {
      $this->NWORKORDERSTATUS = $NWORKORDERSTATUS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEFROMFROM()
    {
      return $this->SZDATEFROMFROM;
    }

    /**
     * @param string $SZDATEFROMFROM
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setSZDATEFROMFROM($SZDATEFROMFROM)
    {
      $this->SZDATEFROMFROM = $SZDATEFROMFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEFROMTO()
    {
      return $this->SZDATEFROMTO;
    }

    /**
     * @param string $SZDATEFROMTO
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setSZDATEFROMTO($SZDATEFROMTO)
    {
      $this->SZDATEFROMTO = $SZDATEFROMTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATETOFROM()
    {
      return $this->SZDATETOFROM;
    }

    /**
     * @param string $SZDATETOFROM
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setSZDATETOFROM($SZDATETOFROM)
    {
      $this->SZDATETOFROM = $SZDATETOFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATETOTO()
    {
      return $this->SZDATETOTO;
    }

    /**
     * @param string $SZDATETOTO
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setSZDATETOTO($SZDATETOTO)
    {
      $this->SZDATETOTO = $SZDATETOTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTWORKORDER()
    {
      return $this->SZEXTWORKORDER;
    }

    /**
     * @param string $SZEXTWORKORDER
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setSZEXTWORKORDER($SZEXTWORKORDER)
    {
      $this->SZEXTWORKORDER = $SZEXTWORKORDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZORDERBYCOLUMN()
    {
      return $this->SZORDERBYCOLUMN;
    }

    /**
     * @param string $SZORDERBYCOLUMN
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setSZORDERBYCOLUMN($SZORDERBYCOLUMN)
    {
      $this->SZORDERBYCOLUMN = $SZORDERBYCOLUMN;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZORDERBYTYPE()
    {
      return $this->SZORDERBYTYPE;
    }

    /**
     * @param string $SZORDERBYTYPE
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setSZORDERBYTYPE($SZORDERBYTYPE)
    {
      $this->SZORDERBYTYPE = $SZORDERBYTYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTITLE()
    {
      return $this->SZTITLE;
    }

    /**
     * @param string $SZTITLE
     * @return \Axess\Dci4Wtp\D4WTPGETWORKORDERSREQUEST
     */
    public function setSZTITLE($SZTITLE)
    {
      $this->SZTITLE = $SZTITLE;
      return $this;
    }

}
