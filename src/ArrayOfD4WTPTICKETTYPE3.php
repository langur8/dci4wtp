<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTICKETTYPE3 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTICKETTYPE3[] $D4WTPTICKETTYPE3
     */
    protected $D4WTPTICKETTYPE3 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTICKETTYPE3[]
     */
    public function getD4WTPTICKETTYPE3()
    {
      return $this->D4WTPTICKETTYPE3;
    }

    /**
     * @param D4WTPTICKETTYPE3[] $D4WTPTICKETTYPE3
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTICKETTYPE3
     */
    public function setD4WTPTICKETTYPE3(array $D4WTPTICKETTYPE3 = null)
    {
      $this->D4WTPTICKETTYPE3 = $D4WTPTICKETTYPE3;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTICKETTYPE3[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTICKETTYPE3
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTICKETTYPE3[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTICKETTYPE3 $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTICKETTYPE3[] = $value;
      } else {
        $this->D4WTPTICKETTYPE3[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTICKETTYPE3[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTICKETTYPE3 Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTICKETTYPE3);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTICKETTYPE3);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTICKETTYPE3);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTICKETTYPE3);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTICKETTYPE3 Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTICKETTYPE3);
    }

}
