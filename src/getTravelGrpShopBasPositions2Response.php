<?php

namespace Axess\Dci4Wtp;

class getTravelGrpShopBasPositions2Response
{

    /**
     * @var D4WTPGETTRAVELGRPSHOPPOSRES2 $getTravelGrpShopBasPositions2Result
     */
    protected $getTravelGrpShopBasPositions2Result = null;

    /**
     * @param D4WTPGETTRAVELGRPSHOPPOSRES2 $getTravelGrpShopBasPositions2Result
     */
    public function __construct($getTravelGrpShopBasPositions2Result)
    {
      $this->getTravelGrpShopBasPositions2Result = $getTravelGrpShopBasPositions2Result;
    }

    /**
     * @return D4WTPGETTRAVELGRPSHOPPOSRES2
     */
    public function getGetTravelGrpShopBasPositions2Result()
    {
      return $this->getTravelGrpShopBasPositions2Result;
    }

    /**
     * @param D4WTPGETTRAVELGRPSHOPPOSRES2 $getTravelGrpShopBasPositions2Result
     * @return \Axess\Dci4Wtp\getTravelGrpShopBasPositions2Response
     */
    public function setGetTravelGrpShopBasPositions2Result($getTravelGrpShopBasPositions2Result)
    {
      $this->getTravelGrpShopBasPositions2Result = $getTravelGrpShopBasPositions2Result;
      return $this;
    }

}
