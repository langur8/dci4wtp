<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCURRENCIESREQUEST
{

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZDATE
     */
    protected $SZDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETCURRENCIESREQUEST
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETCURRENCIESREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATE()
    {
      return $this->SZDATE;
    }

    /**
     * @param string $SZDATE
     * @return \Axess\Dci4Wtp\D4WTPGETCURRENCIESREQUEST
     */
    public function setSZDATE($SZDATE)
    {
      $this->SZDATE = $SZDATE;
      return $this;
    }

}
