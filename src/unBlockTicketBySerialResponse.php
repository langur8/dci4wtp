<?php

namespace Axess\Dci4Wtp;

class unBlockTicketBySerialResponse
{

    /**
     * @var D4WTPRESULT $unBlockTicketBySerialResult
     */
    protected $unBlockTicketBySerialResult = null;

    /**
     * @param D4WTPRESULT $unBlockTicketBySerialResult
     */
    public function __construct($unBlockTicketBySerialResult)
    {
      $this->unBlockTicketBySerialResult = $unBlockTicketBySerialResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getUnBlockTicketBySerialResult()
    {
      return $this->unBlockTicketBySerialResult;
    }

    /**
     * @param D4WTPRESULT $unBlockTicketBySerialResult
     * @return \Axess\Dci4Wtp\unBlockTicketBySerialResponse
     */
    public function setUnBlockTicketBySerialResult($unBlockTicketBySerialResult)
    {
      $this->unBlockTicketBySerialResult = $unBlockTicketBySerialResult;
      return $this;
    }

}
