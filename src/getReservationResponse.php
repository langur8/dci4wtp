<?php

namespace Axess\Dci4Wtp;

class getReservationResponse
{

    /**
     * @var D4WTPGETRESERVATIONRESULT $getReservationResult
     */
    protected $getReservationResult = null;

    /**
     * @param D4WTPGETRESERVATIONRESULT $getReservationResult
     */
    public function __construct($getReservationResult)
    {
      $this->getReservationResult = $getReservationResult;
    }

    /**
     * @return D4WTPGETRESERVATIONRESULT
     */
    public function getGetReservationResult()
    {
      return $this->getReservationResult;
    }

    /**
     * @param D4WTPGETRESERVATIONRESULT $getReservationResult
     * @return \Axess\Dci4Wtp\getReservationResponse
     */
    public function setGetReservationResult($getReservationResult)
    {
      $this->getReservationResult = $getReservationResult;
      return $this;
    }

}
