<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSHOPPINGCARTPOSDETAIL implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSHOPPINGCARTPOSDETAIL[] $D4WTPSHOPPINGCARTPOSDETAIL
     */
    protected $D4WTPSHOPPINGCARTPOSDETAIL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSHOPPINGCARTPOSDETAIL[]
     */
    public function getD4WTPSHOPPINGCARTPOSDETAIL()
    {
      return $this->D4WTPSHOPPINGCARTPOSDETAIL;
    }

    /**
     * @param D4WTPSHOPPINGCARTPOSDETAIL[] $D4WTPSHOPPINGCARTPOSDETAIL
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setD4WTPSHOPPINGCARTPOSDETAIL(array $D4WTPSHOPPINGCARTPOSDETAIL = null)
    {
      $this->D4WTPSHOPPINGCARTPOSDETAIL = $D4WTPSHOPPINGCARTPOSDETAIL;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSHOPPINGCARTPOSDETAIL[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSHOPPINGCARTPOSDETAIL[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSHOPPINGCARTPOSDETAIL $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSHOPPINGCARTPOSDETAIL[] = $value;
      } else {
        $this->D4WTPSHOPPINGCARTPOSDETAIL[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSHOPPINGCARTPOSDETAIL[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSHOPPINGCARTPOSDETAIL Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSHOPPINGCARTPOSDETAIL);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSHOPPINGCARTPOSDETAIL);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSHOPPINGCARTPOSDETAIL);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSHOPPINGCARTPOSDETAIL);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSHOPPINGCARTPOSDETAIL Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSHOPPINGCARTPOSDETAIL);
    }

}
