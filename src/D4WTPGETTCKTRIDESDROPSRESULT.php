<?php

namespace Axess\Dci4Wtp;

class D4WTPGETTCKTRIDESDROPSRESULT
{

    /**
     * @var ArrayOfD4WTPRIDESANDDROPS $ACTRIDESANDDROPS
     */
    protected $ACTRIDESANDDROPS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPRIDESANDDROPS
     */
    public function getACTRIDESANDDROPS()
    {
      return $this->ACTRIDESANDDROPS;
    }

    /**
     * @param ArrayOfD4WTPRIDESANDDROPS $ACTRIDESANDDROPS
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTRIDESDROPSRESULT
     */
    public function setACTRIDESANDDROPS($ACTRIDESANDDROPS)
    {
      $this->ACTRIDESANDDROPS = $ACTRIDESANDDROPS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTRIDESDROPSRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTRIDESDROPSRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
