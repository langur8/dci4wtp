<?php

namespace Axess\Dci4Wtp;

class getPersSalesDataResponse
{

    /**
     * @var D4WTPPERSSALESDATARESULT $getPersSalesDataResult
     */
    protected $getPersSalesDataResult = null;

    /**
     * @param D4WTPPERSSALESDATARESULT $getPersSalesDataResult
     */
    public function __construct($getPersSalesDataResult)
    {
      $this->getPersSalesDataResult = $getPersSalesDataResult;
    }

    /**
     * @return D4WTPPERSSALESDATARESULT
     */
    public function getGetPersSalesDataResult()
    {
      return $this->getPersSalesDataResult;
    }

    /**
     * @param D4WTPPERSSALESDATARESULT $getPersSalesDataResult
     * @return \Axess\Dci4Wtp\getPersSalesDataResponse
     */
    public function setGetPersSalesDataResult($getPersSalesDataResult)
    {
      $this->getPersSalesDataResult = $getPersSalesDataResult;
      return $this;
    }

}
