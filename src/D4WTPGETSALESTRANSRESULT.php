<?php

namespace Axess\Dci4Wtp;

class D4WTPGETSALESTRANSRESULT
{

    /**
     * @var ArrayOfD4WTPSALESTRANS $ACTSALESTRANS
     */
    protected $ACTSALESTRANS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSALESTRANS
     */
    public function getACTSALESTRANS()
    {
      return $this->ACTSALESTRANS;
    }

    /**
     * @param ArrayOfD4WTPSALESTRANS $ACTSALESTRANS
     * @return \Axess\Dci4Wtp\D4WTPGETSALESTRANSRESULT
     */
    public function setACTSALESTRANS($ACTSALESTRANS)
    {
      $this->ACTSALESTRANS = $ACTSALESTRANS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETSALESTRANSRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETSALESTRANSRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
