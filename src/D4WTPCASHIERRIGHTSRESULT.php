<?php

namespace Axess\Dci4Wtp;

class D4WTPCASHIERRIGHTSRESULT
{

    /**
     * @var ArrayOfD4WTPCASHIERRIGHTS $ACTCASHIERRIGHTS
     */
    protected $ACTCASHIERRIGHTS = null;

    /**
     * @var float $NCASHIERRANK
     */
    protected $NCASHIERRANK = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCASHIERRIGHTS
     */
    public function getACTCASHIERRIGHTS()
    {
      return $this->ACTCASHIERRIGHTS;
    }

    /**
     * @param ArrayOfD4WTPCASHIERRIGHTS $ACTCASHIERRIGHTS
     * @return \Axess\Dci4Wtp\D4WTPCASHIERRIGHTSRESULT
     */
    public function setACTCASHIERRIGHTS($ACTCASHIERRIGHTS)
    {
      $this->ACTCASHIERRIGHTS = $ACTCASHIERRIGHTS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCASHIERRANK()
    {
      return $this->NCASHIERRANK;
    }

    /**
     * @param float $NCASHIERRANK
     * @return \Axess\Dci4Wtp\D4WTPCASHIERRIGHTSRESULT
     */
    public function setNCASHIERRANK($NCASHIERRANK)
    {
      $this->NCASHIERRANK = $NCASHIERRANK;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCASHIERRIGHTSRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCASHIERRIGHTSRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
