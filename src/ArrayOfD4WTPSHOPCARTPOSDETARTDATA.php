<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSHOPCARTPOSDETARTDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSHOPCARTPOSDETARTDATA[] $D4WTPSHOPCARTPOSDETARTDATA
     */
    protected $D4WTPSHOPCARTPOSDETARTDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSHOPCARTPOSDETARTDATA[]
     */
    public function getD4WTPSHOPCARTPOSDETARTDATA()
    {
      return $this->D4WTPSHOPCARTPOSDETARTDATA;
    }

    /**
     * @param D4WTPSHOPCARTPOSDETARTDATA[] $D4WTPSHOPCARTPOSDETARTDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSHOPCARTPOSDETARTDATA
     */
    public function setD4WTPSHOPCARTPOSDETARTDATA(array $D4WTPSHOPCARTPOSDETARTDATA = null)
    {
      $this->D4WTPSHOPCARTPOSDETARTDATA = $D4WTPSHOPCARTPOSDETARTDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSHOPCARTPOSDETARTDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSHOPCARTPOSDETARTDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSHOPCARTPOSDETARTDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSHOPCARTPOSDETARTDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSHOPCARTPOSDETARTDATA[] = $value;
      } else {
        $this->D4WTPSHOPCARTPOSDETARTDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSHOPCARTPOSDETARTDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSHOPCARTPOSDETARTDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSHOPCARTPOSDETARTDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSHOPCARTPOSDETARTDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSHOPCARTPOSDETARTDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSHOPCARTPOSDETARTDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSHOPCARTPOSDETARTDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSHOPCARTPOSDETARTDATA);
    }

}
