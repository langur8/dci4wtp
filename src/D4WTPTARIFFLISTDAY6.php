<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLISTDAY6
{

    /**
     * @var ArrayOfD4WTPADDARTTARIFF3 $ACTADDARTTARIFF
     */
    protected $ACTADDARTTARIFF = null;

    /**
     * @var ArrayOfD4WTPCONTINGENTLIST4 $ACTCONTINGENTLIST4
     */
    protected $ACTCONTINGENTLIST4 = null;

    /**
     * @var ArrayOfD4WTPDATTRAEGNO $ACTDATTRAEGNO
     */
    protected $ACTDATTRAEGNO = null;

    /**
     * @var float $BPARKINGFEATURE
     */
    protected $BPARKINGFEATURE = null;

    /**
     * @var float $BPERSDATA
     */
    protected $BPERSDATA = null;

    /**
     * @var float $BPHOTOMANDATORY
     */
    protected $BPHOTOMANDATORY = null;

    /**
     * @var D4WTPCOMPANYCONDITION $CTCOMPANYCONDITION
     */
    protected $CTCOMPANYCONDITION = null;

    /**
     * @var float $NDEFDATACARRIERTYPENO
     */
    protected $NDEFDATACARRIERTYPENO = null;

    /**
     * @var float $NDEFROHLINGSTYPENO
     */
    protected $NDEFROHLINGSTYPENO = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPERSONTYPENO
     */
    protected $NPERSONTYPENO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var float $NVATAMOUNT
     */
    protected $NVATAMOUNT = null;

    /**
     * @var float $NVATPERCENT
     */
    protected $NVATPERCENT = null;

    /**
     * @var string $SZCURRENCY
     */
    protected $SZCURRENCY = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZEXTMATERIALNO
     */
    protected $SZEXTMATERIALNO = null;

    /**
     * @var string $SZPERSTYPENAME
     */
    protected $SZPERSTYPENAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZTARIFVALIDTO
     */
    protected $SZTARIFVALIDTO = null;

    /**
     * @var string $SZTICKETTYPENAME
     */
    protected $SZTICKETTYPENAME = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDARTTARIFF3
     */
    public function getACTADDARTTARIFF()
    {
      return $this->ACTADDARTTARIFF;
    }

    /**
     * @param ArrayOfD4WTPADDARTTARIFF3 $ACTADDARTTARIFF
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setACTADDARTTARIFF($ACTADDARTTARIFF)
    {
      $this->ACTADDARTTARIFF = $ACTADDARTTARIFF;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPCONTINGENTLIST4
     */
    public function getACTCONTINGENTLIST4()
    {
      return $this->ACTCONTINGENTLIST4;
    }

    /**
     * @param ArrayOfD4WTPCONTINGENTLIST4 $ACTCONTINGENTLIST4
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setACTCONTINGENTLIST4($ACTCONTINGENTLIST4)
    {
      $this->ACTCONTINGENTLIST4 = $ACTCONTINGENTLIST4;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPDATTRAEGNO
     */
    public function getACTDATTRAEGNO()
    {
      return $this->ACTDATTRAEGNO;
    }

    /**
     * @param ArrayOfD4WTPDATTRAEGNO $ACTDATTRAEGNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setACTDATTRAEGNO($ACTDATTRAEGNO)
    {
      $this->ACTDATTRAEGNO = $ACTDATTRAEGNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPARKINGFEATURE()
    {
      return $this->BPARKINGFEATURE;
    }

    /**
     * @param float $BPARKINGFEATURE
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setBPARKINGFEATURE($BPARKINGFEATURE)
    {
      $this->BPARKINGFEATURE = $BPARKINGFEATURE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPERSDATA()
    {
      return $this->BPERSDATA;
    }

    /**
     * @param float $BPERSDATA
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setBPERSDATA($BPERSDATA)
    {
      $this->BPERSDATA = $BPERSDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPHOTOMANDATORY()
    {
      return $this->BPHOTOMANDATORY;
    }

    /**
     * @param float $BPHOTOMANDATORY
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setBPHOTOMANDATORY($BPHOTOMANDATORY)
    {
      $this->BPHOTOMANDATORY = $BPHOTOMANDATORY;
      return $this;
    }

    /**
     * @return D4WTPCOMPANYCONDITION
     */
    public function getCTCOMPANYCONDITION()
    {
      return $this->CTCOMPANYCONDITION;
    }

    /**
     * @param D4WTPCOMPANYCONDITION $CTCOMPANYCONDITION
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setCTCOMPANYCONDITION($CTCOMPANYCONDITION)
    {
      $this->CTCOMPANYCONDITION = $CTCOMPANYCONDITION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDEFDATACARRIERTYPENO()
    {
      return $this->NDEFDATACARRIERTYPENO;
    }

    /**
     * @param float $NDEFDATACARRIERTYPENO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setNDEFDATACARRIERTYPENO($NDEFDATACARRIERTYPENO)
    {
      $this->NDEFDATACARRIERTYPENO = $NDEFDATACARRIERTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDEFROHLINGSTYPENO()
    {
      return $this->NDEFROHLINGSTYPENO;
    }

    /**
     * @param float $NDEFROHLINGSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setNDEFROHLINGSTYPENO($NDEFROHLINGSTYPENO)
    {
      $this->NDEFROHLINGSTYPENO = $NDEFROHLINGSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSONTYPENO()
    {
      return $this->NPERSONTYPENO;
    }

    /**
     * @param float $NPERSONTYPENO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setNPERSONTYPENO($NPERSONTYPENO)
    {
      $this->NPERSONTYPENO = $NPERSONTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATAMOUNT()
    {
      return $this->NVATAMOUNT;
    }

    /**
     * @param float $NVATAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setNVATAMOUNT($NVATAMOUNT)
    {
      $this->NVATAMOUNT = $NVATAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATPERCENT()
    {
      return $this->NVATPERCENT;
    }

    /**
     * @param float $NVATPERCENT
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setNVATPERCENT($NVATPERCENT)
    {
      $this->NVATPERCENT = $NVATPERCENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCY()
    {
      return $this->SZCURRENCY;
    }

    /**
     * @param string $SZCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setSZCURRENCY($SZCURRENCY)
    {
      $this->SZCURRENCY = $SZCURRENCY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTMATERIALNO()
    {
      return $this->SZEXTMATERIALNO;
    }

    /**
     * @param string $SZEXTMATERIALNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setSZEXTMATERIALNO($SZEXTMATERIALNO)
    {
      $this->SZEXTMATERIALNO = $SZEXTMATERIALNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSTYPENAME()
    {
      return $this->SZPERSTYPENAME;
    }

    /**
     * @param string $SZPERSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setSZPERSTYPENAME($SZPERSTYPENAME)
    {
      $this->SZPERSTYPENAME = $SZPERSTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTARIFVALIDTO()
    {
      return $this->SZTARIFVALIDTO;
    }

    /**
     * @param string $SZTARIFVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setSZTARIFVALIDTO($SZTARIFVALIDTO)
    {
      $this->SZTARIFVALIDTO = $SZTARIFVALIDTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPENAME()
    {
      return $this->SZTICKETTYPENAME;
    }

    /**
     * @param string $SZTICKETTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setSZTICKETTYPENAME($SZTICKETTYPENAME)
    {
      $this->SZTICKETTYPENAME = $SZTICKETTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTDAY6
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
