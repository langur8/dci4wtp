<?php

namespace Axess\Dci4Wtp;

class getWTPRentalItemsResponse
{

    /**
     * @var D4WTPRENTALITEMRESULT $getWTPRentalItemsResult
     */
    protected $getWTPRentalItemsResult = null;

    /**
     * @param D4WTPRENTALITEMRESULT $getWTPRentalItemsResult
     */
    public function __construct($getWTPRentalItemsResult)
    {
      $this->getWTPRentalItemsResult = $getWTPRentalItemsResult;
    }

    /**
     * @return D4WTPRENTALITEMRESULT
     */
    public function getGetWTPRentalItemsResult()
    {
      return $this->getWTPRentalItemsResult;
    }

    /**
     * @param D4WTPRENTALITEMRESULT $getWTPRentalItemsResult
     * @return \Axess\Dci4Wtp\getWTPRentalItemsResponse
     */
    public function setGetWTPRentalItemsResult($getWTPRentalItemsResult)
    {
      $this->getWTPRentalItemsResult = $getWTPRentalItemsResult;
      return $this;
    }

}
