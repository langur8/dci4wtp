<?php

namespace Axess\Dci4Wtp;

class D4WTPERSDATA4FAMILY
{

    /**
     * @var float $BCREDITCARDPAY
     */
    protected $BCREDITCARDPAY = null;

    /**
     * @var float $BDELIVERYNOTEPAY
     */
    protected $BDELIVERYNOTEPAY = null;

    /**
     * @var float $BEUROCARDPAY
     */
    protected $BEUROCARDPAY = null;

    /**
     * @var float $NFAMPERSNO
     */
    protected $NFAMPERSNO = null;

    /**
     * @var float $NFAMPOSNO
     */
    protected $NFAMPOSNO = null;

    /**
     * @var float $NFAMPROJNO
     */
    protected $NFAMPROJNO = null;

    /**
     * @var float $NFAMSORTNO
     */
    protected $NFAMSORTNO = null;

    /**
     * @var float $NLANGUAGEID
     */
    protected $NLANGUAGEID = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var string $SZAREA
     */
    protected $SZAREA = null;

    /**
     * @var string $SZCITY
     */
    protected $SZCITY = null;

    /**
     * @var string $SZCOUNTRY
     */
    protected $SZCOUNTRY = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    /**
     * @var string $SZDATEOFBIRTH
     */
    protected $SZDATEOFBIRTH = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZEMAILVERIFICATION
     */
    protected $SZEMAILVERIFICATION = null;

    /**
     * @var string $SZEXTPERSONID
     */
    protected $SZEXTPERSONID = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZGENDER
     */
    protected $SZGENDER = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZMOBILE
     */
    protected $SZMOBILE = null;

    /**
     * @var string $SZMOBILENUMBERVERIFICATION
     */
    protected $SZMOBILENUMBERVERIFICATION = null;

    /**
     * @var string $SZPHONE
     */
    protected $SZPHONE = null;

    /**
     * @var string $SZSALUTATION
     */
    protected $SZSALUTATION = null;

    /**
     * @var string $SZSECONDSURNAME
     */
    protected $SZSECONDSURNAME = null;

    /**
     * @var string $SZSTREET
     */
    protected $SZSTREET = null;

    /**
     * @var string $SZSTREETNUMBER
     */
    protected $SZSTREETNUMBER = null;

    /**
     * @var string $SZSUBDIVISION
     */
    protected $SZSUBDIVISION = null;

    /**
     * @var string $SZTITLE
     */
    protected $SZTITLE = null;

    /**
     * @var string $SZZIPCODE
     */
    protected $SZZIPCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBCREDITCARDPAY()
    {
      return $this->BCREDITCARDPAY;
    }

    /**
     * @param float $BCREDITCARDPAY
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setBCREDITCARDPAY($BCREDITCARDPAY)
    {
      $this->BCREDITCARDPAY = $BCREDITCARDPAY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBDELIVERYNOTEPAY()
    {
      return $this->BDELIVERYNOTEPAY;
    }

    /**
     * @param float $BDELIVERYNOTEPAY
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setBDELIVERYNOTEPAY($BDELIVERYNOTEPAY)
    {
      $this->BDELIVERYNOTEPAY = $BDELIVERYNOTEPAY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBEUROCARDPAY()
    {
      return $this->BEUROCARDPAY;
    }

    /**
     * @param float $BEUROCARDPAY
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setBEUROCARDPAY($BEUROCARDPAY)
    {
      $this->BEUROCARDPAY = $BEUROCARDPAY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMPERSNO()
    {
      return $this->NFAMPERSNO;
    }

    /**
     * @param float $NFAMPERSNO
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setNFAMPERSNO($NFAMPERSNO)
    {
      $this->NFAMPERSNO = $NFAMPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMPOSNO()
    {
      return $this->NFAMPOSNO;
    }

    /**
     * @param float $NFAMPOSNO
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setNFAMPOSNO($NFAMPOSNO)
    {
      $this->NFAMPOSNO = $NFAMPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMPROJNO()
    {
      return $this->NFAMPROJNO;
    }

    /**
     * @param float $NFAMPROJNO
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setNFAMPROJNO($NFAMPROJNO)
    {
      $this->NFAMPROJNO = $NFAMPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMSORTNO()
    {
      return $this->NFAMSORTNO;
    }

    /**
     * @param float $NFAMSORTNO
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setNFAMSORTNO($NFAMSORTNO)
    {
      $this->NFAMSORTNO = $NFAMSORTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLANGUAGEID()
    {
      return $this->NLANGUAGEID;
    }

    /**
     * @param float $NLANGUAGEID
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setNLANGUAGEID($NLANGUAGEID)
    {
      $this->NLANGUAGEID = $NLANGUAGEID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAREA()
    {
      return $this->SZAREA;
    }

    /**
     * @param string $SZAREA
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZAREA($SZAREA)
    {
      $this->SZAREA = $SZAREA;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCITY()
    {
      return $this->SZCITY;
    }

    /**
     * @param string $SZCITY
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZCITY($SZCITY)
    {
      $this->SZCITY = $SZCITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRY()
    {
      return $this->SZCOUNTRY;
    }

    /**
     * @param string $SZCOUNTRY
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZCOUNTRY($SZCOUNTRY)
    {
      $this->SZCOUNTRY = $SZCOUNTRY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEOFBIRTH()
    {
      return $this->SZDATEOFBIRTH;
    }

    /**
     * @param string $SZDATEOFBIRTH
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZDATEOFBIRTH($SZDATEOFBIRTH)
    {
      $this->SZDATEOFBIRTH = $SZDATEOFBIRTH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAILVERIFICATION()
    {
      return $this->SZEMAILVERIFICATION;
    }

    /**
     * @param string $SZEMAILVERIFICATION
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZEMAILVERIFICATION($SZEMAILVERIFICATION)
    {
      $this->SZEMAILVERIFICATION = $SZEMAILVERIFICATION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTPERSONID()
    {
      return $this->SZEXTPERSONID;
    }

    /**
     * @param string $SZEXTPERSONID
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZEXTPERSONID($SZEXTPERSONID)
    {
      $this->SZEXTPERSONID = $SZEXTPERSONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGENDER()
    {
      return $this->SZGENDER;
    }

    /**
     * @param string $SZGENDER
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZGENDER($SZGENDER)
    {
      $this->SZGENDER = $SZGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMOBILE()
    {
      return $this->SZMOBILE;
    }

    /**
     * @param string $SZMOBILE
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZMOBILE($SZMOBILE)
    {
      $this->SZMOBILE = $SZMOBILE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMOBILENUMBERVERIFICATION()
    {
      return $this->SZMOBILENUMBERVERIFICATION;
    }

    /**
     * @param string $SZMOBILENUMBERVERIFICATION
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZMOBILENUMBERVERIFICATION($SZMOBILENUMBERVERIFICATION)
    {
      $this->SZMOBILENUMBERVERIFICATION = $SZMOBILENUMBERVERIFICATION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPHONE()
    {
      return $this->SZPHONE;
    }

    /**
     * @param string $SZPHONE
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZPHONE($SZPHONE)
    {
      $this->SZPHONE = $SZPHONE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSALUTATION()
    {
      return $this->SZSALUTATION;
    }

    /**
     * @param string $SZSALUTATION
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZSALUTATION($SZSALUTATION)
    {
      $this->SZSALUTATION = $SZSALUTATION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSECONDSURNAME()
    {
      return $this->SZSECONDSURNAME;
    }

    /**
     * @param string $SZSECONDSURNAME
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZSECONDSURNAME($SZSECONDSURNAME)
    {
      $this->SZSECONDSURNAME = $SZSECONDSURNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREET()
    {
      return $this->SZSTREET;
    }

    /**
     * @param string $SZSTREET
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZSTREET($SZSTREET)
    {
      $this->SZSTREET = $SZSTREET;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREETNUMBER()
    {
      return $this->SZSTREETNUMBER;
    }

    /**
     * @param string $SZSTREETNUMBER
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZSTREETNUMBER($SZSTREETNUMBER)
    {
      $this->SZSTREETNUMBER = $SZSTREETNUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBDIVISION()
    {
      return $this->SZSUBDIVISION;
    }

    /**
     * @param string $SZSUBDIVISION
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZSUBDIVISION($SZSUBDIVISION)
    {
      $this->SZSUBDIVISION = $SZSUBDIVISION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTITLE()
    {
      return $this->SZTITLE;
    }

    /**
     * @param string $SZTITLE
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZTITLE($SZTITLE)
    {
      $this->SZTITLE = $SZTITLE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZZIPCODE()
    {
      return $this->SZZIPCODE;
    }

    /**
     * @param string $SZZIPCODE
     * @return \Axess\Dci4Wtp\D4WTPERSDATA4FAMILY
     */
    public function setSZZIPCODE($SZZIPCODE)
    {
      $this->SZZIPCODE = $SZZIPCODE;
      return $this;
    }

}
