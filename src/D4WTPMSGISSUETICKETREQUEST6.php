<?php

namespace Axess\Dci4Wtp;

class D4WTPMSGISSUETICKETREQUEST6
{

    /**
     * @var ArrayOfD4WTPMSGTICKETDATA3 $ACTMSGTICKETDATA
     */
    protected $ACTMSGTICKETDATA = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPMSGTICKETDATA3
     */
    public function getACTMSGTICKETDATA()
    {
      return $this->ACTMSGTICKETDATA;
    }

    /**
     * @param ArrayOfD4WTPMSGTICKETDATA3 $ACTMSGTICKETDATA
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETREQUEST6
     */
    public function setACTMSGTICKETDATA($ACTMSGTICKETDATA)
    {
      $this->ACTMSGTICKETDATA = $ACTMSGTICKETDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETREQUEST6
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETREQUEST6
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

}
