<?php

namespace Axess\Dci4Wtp;

class changePersDataResponse
{

    /**
     * @var D4WTPCHANGEPERSDATARESULT $changePersDataResult
     */
    protected $changePersDataResult = null;

    /**
     * @param D4WTPCHANGEPERSDATARESULT $changePersDataResult
     */
    public function __construct($changePersDataResult)
    {
      $this->changePersDataResult = $changePersDataResult;
    }

    /**
     * @return D4WTPCHANGEPERSDATARESULT
     */
    public function getChangePersDataResult()
    {
      return $this->changePersDataResult;
    }

    /**
     * @param D4WTPCHANGEPERSDATARESULT $changePersDataResult
     * @return \Axess\Dci4Wtp\changePersDataResponse
     */
    public function setChangePersDataResult($changePersDataResult)
    {
      $this->changePersDataResult = $changePersDataResult;
      return $this;
    }

}
