<?php

namespace Axess\Dci4Wtp;

class decodeWTPNo64BitResponse
{

    /**
     * @var D4WTPDECODE64WTPNORESULT $decodeWTPNo64BitResult
     */
    protected $decodeWTPNo64BitResult = null;

    /**
     * @param D4WTPDECODE64WTPNORESULT $decodeWTPNo64BitResult
     */
    public function __construct($decodeWTPNo64BitResult)
    {
      $this->decodeWTPNo64BitResult = $decodeWTPNo64BitResult;
    }

    /**
     * @return D4WTPDECODE64WTPNORESULT
     */
    public function getDecodeWTPNo64BitResult()
    {
      return $this->decodeWTPNo64BitResult;
    }

    /**
     * @param D4WTPDECODE64WTPNORESULT $decodeWTPNo64BitResult
     * @return \Axess\Dci4Wtp\decodeWTPNo64BitResponse
     */
    public function setDecodeWTPNo64BitResult($decodeWTPNo64BitResult)
    {
      $this->decodeWTPNo64BitResult = $decodeWTPNo64BitResult;
      return $this;
    }

}
