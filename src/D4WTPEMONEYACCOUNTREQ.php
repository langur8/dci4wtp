<?php

namespace Axess\Dci4Wtp;

class D4WTPEMONEYACCOUNTREQ
{

    /**
     * @var ArrayOfD4WTPAXCOUNTMEMBER $ACTAXCOUNTMEMBER
     */
    protected $ACTAXCOUNTMEMBER = null;

    /**
     * @var float $BISAXSCALED
     */
    protected $BISAXSCALED = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTYPE
     */
    protected $NTYPE = null;

    /**
     * @var string $SZFLAG
     */
    protected $SZFLAG = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPAXCOUNTMEMBER
     */
    public function getACTAXCOUNTMEMBER()
    {
      return $this->ACTAXCOUNTMEMBER;
    }

    /**
     * @param ArrayOfD4WTPAXCOUNTMEMBER $ACTAXCOUNTMEMBER
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ
     */
    public function setACTAXCOUNTMEMBER($ACTAXCOUNTMEMBER)
    {
      $this->ACTAXCOUNTMEMBER = $ACTAXCOUNTMEMBER;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISAXSCALED()
    {
      return $this->BISAXSCALED;
    }

    /**
     * @param float $BISAXSCALED
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ
     */
    public function setBISAXSCALED($BISAXSCALED)
    {
      $this->BISAXSCALED = $BISAXSCALED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTYPE()
    {
      return $this->NTYPE;
    }

    /**
     * @param float $NTYPE
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ
     */
    public function setNTYPE($NTYPE)
    {
      $this->NTYPE = $NTYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFLAG()
    {
      return $this->SZFLAG;
    }

    /**
     * @param string $SZFLAG
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTREQ
     */
    public function setSZFLAG($SZFLAG)
    {
      $this->SZFLAG = $SZFLAG;
      return $this;
    }

}
