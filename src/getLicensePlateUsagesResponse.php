<?php

namespace Axess\Dci4Wtp;

class getLicensePlateUsagesResponse
{

    /**
     * @var D4WTPGETLPUSAGESRESULT $getLicensePlateUsagesResult
     */
    protected $getLicensePlateUsagesResult = null;

    /**
     * @param D4WTPGETLPUSAGESRESULT $getLicensePlateUsagesResult
     */
    public function __construct($getLicensePlateUsagesResult)
    {
      $this->getLicensePlateUsagesResult = $getLicensePlateUsagesResult;
    }

    /**
     * @return D4WTPGETLPUSAGESRESULT
     */
    public function getGetLicensePlateUsagesResult()
    {
      return $this->getLicensePlateUsagesResult;
    }

    /**
     * @param D4WTPGETLPUSAGESRESULT $getLicensePlateUsagesResult
     * @return \Axess\Dci4Wtp\getLicensePlateUsagesResponse
     */
    public function setGetLicensePlateUsagesResult($getLicensePlateUsagesResult)
    {
      $this->getLicensePlateUsagesResult = $getLicensePlateUsagesResult;
      return $this;
    }

}
