<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSHOPPINGCARTPOSADDART implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSHOPPINGCARTPOSADDART[] $D4WTPSHOPPINGCARTPOSADDART
     */
    protected $D4WTPSHOPPINGCARTPOSADDART = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSHOPPINGCARTPOSADDART[]
     */
    public function getD4WTPSHOPPINGCARTPOSADDART()
    {
      return $this->D4WTPSHOPPINGCARTPOSADDART;
    }

    /**
     * @param D4WTPSHOPPINGCARTPOSADDART[] $D4WTPSHOPPINGCARTPOSADDART
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSHOPPINGCARTPOSADDART
     */
    public function setD4WTPSHOPPINGCARTPOSADDART(array $D4WTPSHOPPINGCARTPOSADDART = null)
    {
      $this->D4WTPSHOPPINGCARTPOSADDART = $D4WTPSHOPPINGCARTPOSADDART;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSHOPPINGCARTPOSADDART[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSHOPPINGCARTPOSADDART
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSHOPPINGCARTPOSADDART[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSHOPPINGCARTPOSADDART $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSHOPPINGCARTPOSADDART[] = $value;
      } else {
        $this->D4WTPSHOPPINGCARTPOSADDART[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSHOPPINGCARTPOSADDART[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSHOPPINGCARTPOSADDART Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSHOPPINGCARTPOSADDART);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSHOPPINGCARTPOSADDART);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSHOPPINGCARTPOSADDART);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSHOPPINGCARTPOSADDART);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSHOPPINGCARTPOSADDART Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSHOPPINGCARTPOSADDART);
    }

}
