<?php

namespace Axess\Dci4Wtp;

class D4WTPB2CACCOUNT
{

    /**
     * @var float $NACCOUNTNO
     */
    protected $NACCOUNTNO = null;

    /**
     * @var float $NACCOUNTPOSNO
     */
    protected $NACCOUNTPOSNO = null;

    /**
     * @var float $NACCOUNTPROJNO
     */
    protected $NACCOUNTPROJNO = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNACCOUNTNO()
    {
      return $this->NACCOUNTNO;
    }

    /**
     * @param float $NACCOUNTNO
     * @return \Axess\Dci4Wtp\D4WTPB2CACCOUNT
     */
    public function setNACCOUNTNO($NACCOUNTNO)
    {
      $this->NACCOUNTNO = $NACCOUNTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNACCOUNTPOSNO()
    {
      return $this->NACCOUNTPOSNO;
    }

    /**
     * @param float $NACCOUNTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPB2CACCOUNT
     */
    public function setNACCOUNTPOSNO($NACCOUNTPOSNO)
    {
      $this->NACCOUNTPOSNO = $NACCOUNTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNACCOUNTPROJNO()
    {
      return $this->NACCOUNTPROJNO;
    }

    /**
     * @param float $NACCOUNTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPB2CACCOUNT
     */
    public function setNACCOUNTPROJNO($NACCOUNTPROJNO)
    {
      $this->NACCOUNTPROJNO = $NACCOUNTPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPB2CACCOUNT
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
