<?php

namespace Axess\Dci4Wtp;

class D4WTPGETPROCESSEDTKTDATA
{

    /**
     * @var float $NACTPOSNO
     */
    protected $NACTPOSNO = null;

    /**
     * @var float $NACTPROJNO
     */
    protected $NACTPROJNO = null;

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NBLOCKNO
     */
    protected $NBLOCKNO = null;

    /**
     * @var float $NCOMPNO
     */
    protected $NCOMPNO = null;

    /**
     * @var float $NCOMPPOSNO
     */
    protected $NCOMPPOSNO = null;

    /**
     * @var float $NCOMPPROJNO
     */
    protected $NCOMPPROJNO = null;

    /**
     * @var float $NEINZELJOURNALNO
     */
    protected $NEINZELJOURNALNO = null;

    /**
     * @var float $NKASSATRANSAKTART
     */
    protected $NKASSATRANSAKTART = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NPERSTYPENO
     */
    protected $NPERSTYPENO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERAILNO
     */
    protected $NSERAILNO = null;

    /**
     * @var float $NSTK
     */
    protected $NSTK = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    /**
     * @var float $NUNICODENO
     */
    protected $NUNICODENO = null;

    /**
     * @var string $SZARTICLENAME
     */
    protected $SZARTICLENAME = null;

    /**
     * @var string $SZCOMPNAME
     */
    protected $SZCOMPNAME = null;

    /**
     * @var string $SZCREATIONTIME
     */
    protected $SZCREATIONTIME = null;

    /**
     * @var string $SZPERSBIRTHDATE
     */
    protected $SZPERSBIRTHDATE = null;

    /**
     * @var string $SZPERSFIRSTNAME
     */
    protected $SZPERSFIRSTNAME = null;

    /**
     * @var string $SZPERSLASTNAME
     */
    protected $SZPERSLASTNAME = null;

    /**
     * @var string $SZPERSTITLE
     */
    protected $SZPERSTITLE = null;

    /**
     * @var string $SZPERSTYPENAME
     */
    protected $SZPERSTYPENAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZTICKETTYPENAME
     */
    protected $SZTICKETTYPENAME = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNACTPOSNO()
    {
      return $this->NACTPOSNO;
    }

    /**
     * @param float $NACTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNACTPOSNO($NACTPOSNO)
    {
      $this->NACTPOSNO = $NACTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNACTPROJNO()
    {
      return $this->NACTPROJNO;
    }

    /**
     * @param float $NACTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNACTPROJNO($NACTPROJNO)
    {
      $this->NACTPROJNO = $NACTPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKNO()
    {
      return $this->NBLOCKNO;
    }

    /**
     * @param float $NBLOCKNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNBLOCKNO($NBLOCKNO)
    {
      $this->NBLOCKNO = $NBLOCKNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPNO()
    {
      return $this->NCOMPNO;
    }

    /**
     * @param float $NCOMPNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNCOMPNO($NCOMPNO)
    {
      $this->NCOMPNO = $NCOMPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPPOSNO()
    {
      return $this->NCOMPPOSNO;
    }

    /**
     * @param float $NCOMPPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNCOMPPOSNO($NCOMPPOSNO)
    {
      $this->NCOMPPOSNO = $NCOMPPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPPROJNO()
    {
      return $this->NCOMPPROJNO;
    }

    /**
     * @param float $NCOMPPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNCOMPPROJNO($NCOMPPROJNO)
    {
      $this->NCOMPPROJNO = $NCOMPPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEINZELJOURNALNO()
    {
      return $this->NEINZELJOURNALNO;
    }

    /**
     * @param float $NEINZELJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNEINZELJOURNALNO($NEINZELJOURNALNO)
    {
      $this->NEINZELJOURNALNO = $NEINZELJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKASSATRANSAKTART()
    {
      return $this->NKASSATRANSAKTART;
    }

    /**
     * @param float $NKASSATRANSAKTART
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNKASSATRANSAKTART($NKASSATRANSAKTART)
    {
      $this->NKASSATRANSAKTART = $NKASSATRANSAKTART;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSTYPENO()
    {
      return $this->NPERSTYPENO;
    }

    /**
     * @param float $NPERSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNPERSTYPENO($NPERSTYPENO)
    {
      $this->NPERSTYPENO = $NPERSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERAILNO()
    {
      return $this->NSERAILNO;
    }

    /**
     * @param float $NSERAILNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNSERAILNO($NSERAILNO)
    {
      $this->NSERAILNO = $NSERAILNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTK()
    {
      return $this->NSTK;
    }

    /**
     * @param float $NSTK
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNSTK($NSTK)
    {
      $this->NSTK = $NSTK;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENO()
    {
      return $this->NUNICODENO;
    }

    /**
     * @param float $NUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setNUNICODENO($NUNICODENO)
    {
      $this->NUNICODENO = $NUNICODENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZARTICLENAME()
    {
      return $this->SZARTICLENAME;
    }

    /**
     * @param string $SZARTICLENAME
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setSZARTICLENAME($SZARTICLENAME)
    {
      $this->SZARTICLENAME = $SZARTICLENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPNAME()
    {
      return $this->SZCOMPNAME;
    }

    /**
     * @param string $SZCOMPNAME
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setSZCOMPNAME($SZCOMPNAME)
    {
      $this->SZCOMPNAME = $SZCOMPNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCREATIONTIME()
    {
      return $this->SZCREATIONTIME;
    }

    /**
     * @param string $SZCREATIONTIME
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setSZCREATIONTIME($SZCREATIONTIME)
    {
      $this->SZCREATIONTIME = $SZCREATIONTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSBIRTHDATE()
    {
      return $this->SZPERSBIRTHDATE;
    }

    /**
     * @param string $SZPERSBIRTHDATE
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setSZPERSBIRTHDATE($SZPERSBIRTHDATE)
    {
      $this->SZPERSBIRTHDATE = $SZPERSBIRTHDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSFIRSTNAME()
    {
      return $this->SZPERSFIRSTNAME;
    }

    /**
     * @param string $SZPERSFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setSZPERSFIRSTNAME($SZPERSFIRSTNAME)
    {
      $this->SZPERSFIRSTNAME = $SZPERSFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSLASTNAME()
    {
      return $this->SZPERSLASTNAME;
    }

    /**
     * @param string $SZPERSLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setSZPERSLASTNAME($SZPERSLASTNAME)
    {
      $this->SZPERSLASTNAME = $SZPERSLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSTITLE()
    {
      return $this->SZPERSTITLE;
    }

    /**
     * @param string $SZPERSTITLE
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setSZPERSTITLE($SZPERSTITLE)
    {
      $this->SZPERSTITLE = $SZPERSTITLE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSTYPENAME()
    {
      return $this->SZPERSTYPENAME;
    }

    /**
     * @param string $SZPERSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setSZPERSTYPENAME($SZPERSTYPENAME)
    {
      $this->SZPERSTYPENAME = $SZPERSTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPENAME()
    {
      return $this->SZTICKETTYPENAME;
    }

    /**
     * @param string $SZTICKETTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setSZTICKETTYPENAME($SZTICKETTYPENAME)
    {
      $this->SZTICKETTYPENAME = $SZTICKETTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATA
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
