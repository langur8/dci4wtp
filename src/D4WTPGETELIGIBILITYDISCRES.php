<?php

namespace Axess\Dci4Wtp;

class D4WTPGETELIGIBILITYDISCRES
{

    /**
     * @var ArrayOfD4WTPDISCOUNTSHEETDEF $ACTDISCOUNTSHEETS
     */
    protected $ACTDISCOUNTSHEETS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPDISCOUNTSHEETDEF
     */
    public function getACTDISCOUNTSHEETS()
    {
      return $this->ACTDISCOUNTSHEETS;
    }

    /**
     * @param ArrayOfD4WTPDISCOUNTSHEETDEF $ACTDISCOUNTSHEETS
     * @return \Axess\Dci4Wtp\D4WTPGETELIGIBILITYDISCRES
     */
    public function setACTDISCOUNTSHEETS($ACTDISCOUNTSHEETS)
    {
      $this->ACTDISCOUNTSHEETS = $ACTDISCOUNTSHEETS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETELIGIBILITYDISCRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETELIGIBILITYDISCRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
