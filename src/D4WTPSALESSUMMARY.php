<?php

namespace Axess\Dci4Wtp;

class D4WTPSALESSUMMARY
{

    /**
     * @var float $NBASPAYMENTTYPENR
     */
    protected $NBASPAYMENTTYPENR = null;

    /**
     * @var float $NCUSTPAYMENTTYPENR
     */
    protected $NCUSTPAYMENTTYPENR = null;

    /**
     * @var float $NTOTAL
     */
    protected $NTOTAL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNBASPAYMENTTYPENR()
    {
      return $this->NBASPAYMENTTYPENR;
    }

    /**
     * @param float $NBASPAYMENTTYPENR
     * @return \Axess\Dci4Wtp\D4WTPSALESSUMMARY
     */
    public function setNBASPAYMENTTYPENR($NBASPAYMENTTYPENR)
    {
      $this->NBASPAYMENTTYPENR = $NBASPAYMENTTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPAYMENTTYPENR()
    {
      return $this->NCUSTPAYMENTTYPENR;
    }

    /**
     * @param float $NCUSTPAYMENTTYPENR
     * @return \Axess\Dci4Wtp\D4WTPSALESSUMMARY
     */
    public function setNCUSTPAYMENTTYPENR($NCUSTPAYMENTTYPENR)
    {
      $this->NCUSTPAYMENTTYPENR = $NCUSTPAYMENTTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTOTAL()
    {
      return $this->NTOTAL;
    }

    /**
     * @param float $NTOTAL
     * @return \Axess\Dci4Wtp\D4WTPSALESSUMMARY
     */
    public function setNTOTAL($NTOTAL)
    {
      $this->NTOTAL = $NTOTAL;
      return $this;
    }

}
