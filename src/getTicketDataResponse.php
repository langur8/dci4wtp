<?php

namespace Axess\Dci4Wtp;

class getTicketDataResponse
{

    /**
     * @var D4WTPTICKETDATARESULT $getTicketDataResult
     */
    protected $getTicketDataResult = null;

    /**
     * @param D4WTPTICKETDATARESULT $getTicketDataResult
     */
    public function __construct($getTicketDataResult)
    {
      $this->getTicketDataResult = $getTicketDataResult;
    }

    /**
     * @return D4WTPTICKETDATARESULT
     */
    public function getGetTicketDataResult()
    {
      return $this->getTicketDataResult;
    }

    /**
     * @param D4WTPTICKETDATARESULT $getTicketDataResult
     * @return \Axess\Dci4Wtp\getTicketDataResponse
     */
    public function setGetTicketDataResult($getTicketDataResult)
    {
      $this->getTicketDataResult = $getTicketDataResult;
      return $this;
    }

}
