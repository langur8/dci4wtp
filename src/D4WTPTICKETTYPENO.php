<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETTYPENO
{

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPENO
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

}
