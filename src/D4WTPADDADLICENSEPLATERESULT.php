<?php

namespace Axess\Dci4Wtp;

class D4WTPADDADLICENSEPLATERESULT
{

    /**
     * @var ArrayOfD4WTPADDLICENSEPLATES $ACTADDLICENSEPLATES
     */
    protected $ACTADDLICENSEPLATES = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NFIRSTLFDNR
     */
    protected $NFIRSTLFDNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZFIRSTLICENSEPLATE
     */
    protected $SZFIRSTLICENSEPLATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDLICENSEPLATES
     */
    public function getACTADDLICENSEPLATES()
    {
      return $this->ACTADDLICENSEPLATES;
    }

    /**
     * @param ArrayOfD4WTPADDLICENSEPLATES $ACTADDLICENSEPLATES
     * @return \Axess\Dci4Wtp\D4WTPADDADLICENSEPLATERESULT
     */
    public function setACTADDLICENSEPLATES($ACTADDLICENSEPLATES)
    {
      $this->ACTADDLICENSEPLATES = $ACTADDLICENSEPLATES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPADDADLICENSEPLATERESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRSTLFDNR()
    {
      return $this->NFIRSTLFDNR;
    }

    /**
     * @param float $NFIRSTLFDNR
     * @return \Axess\Dci4Wtp\D4WTPADDADLICENSEPLATERESULT
     */
    public function setNFIRSTLFDNR($NFIRSTLFDNR)
    {
      $this->NFIRSTLFDNR = $NFIRSTLFDNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPADDADLICENSEPLATERESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTLICENSEPLATE()
    {
      return $this->SZFIRSTLICENSEPLATE;
    }

    /**
     * @param string $SZFIRSTLICENSEPLATE
     * @return \Axess\Dci4Wtp\D4WTPADDADLICENSEPLATERESULT
     */
    public function setSZFIRSTLICENSEPLATE($SZFIRSTLICENSEPLATE)
    {
      $this->SZFIRSTLICENSEPLATE = $SZFIRSTLICENSEPLATE;
      return $this;
    }

}
