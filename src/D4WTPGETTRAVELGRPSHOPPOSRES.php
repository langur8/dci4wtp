<?php

namespace Axess\Dci4Wtp;

class D4WTPGETTRAVELGRPSHOPPOSRES
{

    /**
     * @var ArrayOfD4WTPSHOPCARTPOSDATA3 $ACTSHOPCARTPOSDATA3
     */
    protected $ACTSHOPCARTPOSDATA3 = null;

    /**
     * @var float $BDELETED
     */
    protected $BDELETED = null;

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var float $NSHOPPINGCARTPOSNO
     */
    protected $NSHOPPINGCARTPOSNO = null;

    /**
     * @var float $NSHOPPINGCARTPROJNO
     */
    protected $NSHOPPINGCARTPROJNO = null;

    /**
     * @var float $NTRAVELGROUPNO
     */
    protected $NTRAVELGROUPNO = null;

    /**
     * @var float $NTRAVELGROUPPOSNO
     */
    protected $NTRAVELGROUPPOSNO = null;

    /**
     * @var float $NTRAVELGROUPPROJNO
     */
    protected $NTRAVELGROUPPROJNO = null;

    /**
     * @var string $SZADDITIONALINFO
     */
    protected $SZADDITIONALINFO = null;

    /**
     * @var string $SZCREATIONDATE
     */
    protected $SZCREATIONDATE = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZEXTGROUPCODE
     */
    protected $SZEXTGROUPCODE = null;

    /**
     * @var string $SZGROUPID
     */
    protected $SZGROUPID = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZTEXT
     */
    protected $SZTEXT = null;

    /**
     * @var string $SZTRAVELDATE
     */
    protected $SZTRAVELDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSHOPCARTPOSDATA3
     */
    public function getACTSHOPCARTPOSDATA3()
    {
      return $this->ACTSHOPCARTPOSDATA3;
    }

    /**
     * @param ArrayOfD4WTPSHOPCARTPOSDATA3 $ACTSHOPCARTPOSDATA3
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setACTSHOPCARTPOSDATA3($ACTSHOPCARTPOSDATA3)
    {
      $this->ACTSHOPCARTPOSDATA3 = $ACTSHOPCARTPOSDATA3;
      return $this;
    }

    /**
     * @return float
     */
    public function getBDELETED()
    {
      return $this->BDELETED;
    }

    /**
     * @param float $BDELETED
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setBDELETED($BDELETED)
    {
      $this->BDELETED = $BDELETED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPOSNO()
    {
      return $this->NSHOPPINGCARTPOSNO;
    }

    /**
     * @param float $NSHOPPINGCARTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setNSHOPPINGCARTPOSNO($NSHOPPINGCARTPOSNO)
    {
      $this->NSHOPPINGCARTPOSNO = $NSHOPPINGCARTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPROJNO()
    {
      return $this->NSHOPPINGCARTPROJNO;
    }

    /**
     * @param float $NSHOPPINGCARTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setNSHOPPINGCARTPROJNO($NSHOPPINGCARTPROJNO)
    {
      $this->NSHOPPINGCARTPROJNO = $NSHOPPINGCARTPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPNO()
    {
      return $this->NTRAVELGROUPNO;
    }

    /**
     * @param float $NTRAVELGROUPNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setNTRAVELGROUPNO($NTRAVELGROUPNO)
    {
      $this->NTRAVELGROUPNO = $NTRAVELGROUPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPOSNO()
    {
      return $this->NTRAVELGROUPPOSNO;
    }

    /**
     * @param float $NTRAVELGROUPPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setNTRAVELGROUPPOSNO($NTRAVELGROUPPOSNO)
    {
      $this->NTRAVELGROUPPOSNO = $NTRAVELGROUPPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPROJNO()
    {
      return $this->NTRAVELGROUPPROJNO;
    }

    /**
     * @param float $NTRAVELGROUPPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setNTRAVELGROUPPROJNO($NTRAVELGROUPPROJNO)
    {
      $this->NTRAVELGROUPPROJNO = $NTRAVELGROUPPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZADDITIONALINFO()
    {
      return $this->SZADDITIONALINFO;
    }

    /**
     * @param string $SZADDITIONALINFO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setSZADDITIONALINFO($SZADDITIONALINFO)
    {
      $this->SZADDITIONALINFO = $SZADDITIONALINFO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCREATIONDATE()
    {
      return $this->SZCREATIONDATE;
    }

    /**
     * @param string $SZCREATIONDATE
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setSZCREATIONDATE($SZCREATIONDATE)
    {
      $this->SZCREATIONDATE = $SZCREATIONDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTGROUPCODE()
    {
      return $this->SZEXTGROUPCODE;
    }

    /**
     * @param string $SZEXTGROUPCODE
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setSZEXTGROUPCODE($SZEXTGROUPCODE)
    {
      $this->SZEXTGROUPCODE = $SZEXTGROUPCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGROUPID()
    {
      return $this->SZGROUPID;
    }

    /**
     * @param string $SZGROUPID
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setSZGROUPID($SZGROUPID)
    {
      $this->SZGROUPID = $SZGROUPID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTEXT()
    {
      return $this->SZTEXT;
    }

    /**
     * @param string $SZTEXT
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setSZTEXT($SZTEXT)
    {
      $this->SZTEXT = $SZTEXT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTRAVELDATE()
    {
      return $this->SZTRAVELDATE;
    }

    /**
     * @param string $SZTRAVELDATE
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSRES
     */
    public function setSZTRAVELDATE($SZTRAVELDATE)
    {
      $this->SZTRAVELDATE = $SZTRAVELDATE;
      return $this;
    }

}
