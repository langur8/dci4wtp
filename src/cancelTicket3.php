<?php

namespace Axess\Dci4Wtp;

class cancelTicket3
{

    /**
     * @var D4WTPCANCELTICKET3REQUEST $i_ctCancelTicket3Req
     */
    protected $i_ctCancelTicket3Req = null;

    /**
     * @param D4WTPCANCELTICKET3REQUEST $i_ctCancelTicket3Req
     */
    public function __construct($i_ctCancelTicket3Req)
    {
      $this->i_ctCancelTicket3Req = $i_ctCancelTicket3Req;
    }

    /**
     * @return D4WTPCANCELTICKET3REQUEST
     */
    public function getI_ctCancelTicket3Req()
    {
      return $this->i_ctCancelTicket3Req;
    }

    /**
     * @param D4WTPCANCELTICKET3REQUEST $i_ctCancelTicket3Req
     * @return \Axess\Dci4Wtp\cancelTicket3
     */
    public function setI_ctCancelTicket3Req($i_ctCancelTicket3Req)
    {
      $this->i_ctCancelTicket3Req = $i_ctCancelTicket3Req;
      return $this;
    }

}
