<?php

namespace Axess\Dci4Wtp;

class D4WTPGETRESERVATIONDATA
{

    /**
     * @var float $NCONTINGENTNO
     */
    protected $NCONTINGENTNO = null;

    /**
     * @var float $NSUBCONTINGENTNO
     */
    protected $NSUBCONTINGENTNO = null;

    /**
     * @var string $SZRESERVATIONDATE
     */
    protected $SZRESERVATIONDATE = null;

    /**
     * @var string $SZSTARTTIME
     */
    protected $SZSTARTTIME = null;

    /**
     * @var string $SZSUBCONTINGENTNAME
     */
    protected $SZSUBCONTINGENTNAME = null;

    /**
     * @var string $SZSUBCONTINGENTSHORTNAME
     */
    protected $SZSUBCONTINGENTSHORTNAME = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNO()
    {
      return $this->NCONTINGENTNO;
    }

    /**
     * @param float $NCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPGETRESERVATIONDATA
     */
    public function setNCONTINGENTNO($NCONTINGENTNO)
    {
      $this->NCONTINGENTNO = $NCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTINGENTNO()
    {
      return $this->NSUBCONTINGENTNO;
    }

    /**
     * @param float $NSUBCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPGETRESERVATIONDATA
     */
    public function setNSUBCONTINGENTNO($NSUBCONTINGENTNO)
    {
      $this->NSUBCONTINGENTNO = $NSUBCONTINGENTNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRESERVATIONDATE()
    {
      return $this->SZRESERVATIONDATE;
    }

    /**
     * @param string $SZRESERVATIONDATE
     * @return \Axess\Dci4Wtp\D4WTPGETRESERVATIONDATA
     */
    public function setSZRESERVATIONDATE($SZRESERVATIONDATE)
    {
      $this->SZRESERVATIONDATE = $SZRESERVATIONDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTARTTIME()
    {
      return $this->SZSTARTTIME;
    }

    /**
     * @param string $SZSTARTTIME
     * @return \Axess\Dci4Wtp\D4WTPGETRESERVATIONDATA
     */
    public function setSZSTARTTIME($SZSTARTTIME)
    {
      $this->SZSTARTTIME = $SZSTARTTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBCONTINGENTNAME()
    {
      return $this->SZSUBCONTINGENTNAME;
    }

    /**
     * @param string $SZSUBCONTINGENTNAME
     * @return \Axess\Dci4Wtp\D4WTPGETRESERVATIONDATA
     */
    public function setSZSUBCONTINGENTNAME($SZSUBCONTINGENTNAME)
    {
      $this->SZSUBCONTINGENTNAME = $SZSUBCONTINGENTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBCONTINGENTSHORTNAME()
    {
      return $this->SZSUBCONTINGENTSHORTNAME;
    }

    /**
     * @param string $SZSUBCONTINGENTSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPGETRESERVATIONDATA
     */
    public function setSZSUBCONTINGENTSHORTNAME($SZSUBCONTINGENTSHORTNAME)
    {
      $this->SZSUBCONTINGENTSHORTNAME = $SZSUBCONTINGENTSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPGETRESERVATIONDATA
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPGETRESERVATIONDATA
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
