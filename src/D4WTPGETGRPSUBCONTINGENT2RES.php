<?php

namespace Axess\Dci4Wtp;

class D4WTPGETGRPSUBCONTINGENT2RES
{

    /**
     * @var ArrayOfD4WTPSUBCONTINGENTRES2 $ACTSUBCONTINGENTRES
     */
    protected $ACTSUBCONTINGENTRES = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSUBCONTINGENTRES2
     */
    public function getACTSUBCONTINGENTRES()
    {
      return $this->ACTSUBCONTINGENTRES;
    }

    /**
     * @param ArrayOfD4WTPSUBCONTINGENTRES2 $ACTSUBCONTINGENTRES
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENT2RES
     */
    public function setACTSUBCONTINGENTRES($ACTSUBCONTINGENTRES)
    {
      $this->ACTSUBCONTINGENTRES = $ACTSUBCONTINGENTRES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENT2RES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENT2RES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
