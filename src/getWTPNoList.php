<?php

namespace Axess\Dci4Wtp;

class getWTPNoList
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var float $i_nPersProjNr
     */
    protected $i_nPersProjNr = null;

    /**
     * @var float $i_nPersPOSNr
     */
    protected $i_nPersPOSNr = null;

    /**
     * @var float $i_nPersNr
     */
    protected $i_nPersNr = null;

    /**
     * @param float $i_nSessionID
     * @param float $i_nPersProjNr
     * @param float $i_nPersPOSNr
     * @param float $i_nPersNr
     */
    public function __construct($i_nSessionID, $i_nPersProjNr, $i_nPersPOSNr, $i_nPersNr)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_nPersProjNr = $i_nPersProjNr;
      $this->i_nPersPOSNr = $i_nPersPOSNr;
      $this->i_nPersNr = $i_nPersNr;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\getWTPNoList
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nPersProjNr()
    {
      return $this->i_nPersProjNr;
    }

    /**
     * @param float $i_nPersProjNr
     * @return \Axess\Dci4Wtp\getWTPNoList
     */
    public function setI_nPersProjNr($i_nPersProjNr)
    {
      $this->i_nPersProjNr = $i_nPersProjNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nPersPOSNr()
    {
      return $this->i_nPersPOSNr;
    }

    /**
     * @param float $i_nPersPOSNr
     * @return \Axess\Dci4Wtp\getWTPNoList
     */
    public function setI_nPersPOSNr($i_nPersPOSNr)
    {
      $this->i_nPersPOSNr = $i_nPersPOSNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nPersNr()
    {
      return $this->i_nPersNr;
    }

    /**
     * @param float $i_nPersNr
     * @return \Axess\Dci4Wtp\getWTPNoList
     */
    public function setI_nPersNr($i_nPersNr)
    {
      $this->i_nPersNr = $i_nPersNr;
      return $this;
    }

}
