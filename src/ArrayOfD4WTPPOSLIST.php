<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPOSLIST implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPOSLIST[] $D4WTPPOSLIST
     */
    protected $D4WTPPOSLIST = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPOSLIST[]
     */
    public function getD4WTPPOSLIST()
    {
      return $this->D4WTPPOSLIST;
    }

    /**
     * @param D4WTPPOSLIST[] $D4WTPPOSLIST
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPOSLIST
     */
    public function setD4WTPPOSLIST(array $D4WTPPOSLIST = null)
    {
      $this->D4WTPPOSLIST = $D4WTPPOSLIST;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPOSLIST[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPOSLIST
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPOSLIST[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPOSLIST $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPOSLIST[] = $value;
      } else {
        $this->D4WTPPOSLIST[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPOSLIST[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPOSLIST Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPOSLIST);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPOSLIST);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPOSLIST);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPOSLIST);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPOSLIST Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPOSLIST);
    }

}
