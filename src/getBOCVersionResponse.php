<?php

namespace Axess\Dci4Wtp;

class getBOCVersionResponse
{

    /**
     * @var string $getBOCVersionResult
     */
    protected $getBOCVersionResult = null;

    /**
     * @param string $getBOCVersionResult
     */
    public function __construct($getBOCVersionResult)
    {
      $this->getBOCVersionResult = $getBOCVersionResult;
    }

    /**
     * @return string
     */
    public function getGetBOCVersionResult()
    {
      return $this->getBOCVersionResult;
    }

    /**
     * @param string $getBOCVersionResult
     * @return \Axess\Dci4Wtp\getBOCVersionResponse
     */
    public function setGetBOCVersionResult($getBOCVersionResult)
    {
      $this->getBOCVersionResult = $getBOCVersionResult;
      return $this;
    }

}
