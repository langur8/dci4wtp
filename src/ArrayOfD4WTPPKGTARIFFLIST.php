<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPKGTARIFFLIST implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPKGTARIFFLIST[] $D4WTPPKGTARIFFLIST
     */
    protected $D4WTPPKGTARIFFLIST = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPKGTARIFFLIST[]
     */
    public function getD4WTPPKGTARIFFLIST()
    {
      return $this->D4WTPPKGTARIFFLIST;
    }

    /**
     * @param D4WTPPKGTARIFFLIST[] $D4WTPPKGTARIFFLIST
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPKGTARIFFLIST
     */
    public function setD4WTPPKGTARIFFLIST(array $D4WTPPKGTARIFFLIST = null)
    {
      $this->D4WTPPKGTARIFFLIST = $D4WTPPKGTARIFFLIST;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPKGTARIFFLIST[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPKGTARIFFLIST
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPKGTARIFFLIST[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPKGTARIFFLIST $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPKGTARIFFLIST[] = $value;
      } else {
        $this->D4WTPPKGTARIFFLIST[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPKGTARIFFLIST[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPKGTARIFFLIST Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPKGTARIFFLIST);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPKGTARIFFLIST);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPKGTARIFFLIST);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPKGTARIFFLIST);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPKGTARIFFLIST Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPKGTARIFFLIST);
    }

}
