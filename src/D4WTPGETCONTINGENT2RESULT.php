<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCONTINGENT2RESULT
{

    /**
     * @var ArrayOfD4WTPCONTNGNTTICKET2 $ACTCONTNGNTTICKET2
     */
    protected $ACTCONTNGNTTICKET2 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCONTNGNTTICKET2
     */
    public function getACTCONTNGNTTICKET2()
    {
      return $this->ACTCONTNGNTTICKET2;
    }

    /**
     * @param ArrayOfD4WTPCONTNGNTTICKET2 $ACTCONTNGNTTICKET2
     * @return \Axess\Dci4Wtp\D4WTPGETCONTINGENT2RESULT
     */
    public function setACTCONTNGNTTICKET2($ACTCONTNGNTTICKET2)
    {
      $this->ACTCONTNGNTTICKET2 = $ACTCONTNGNTTICKET2;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETCONTINGENT2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETCONTINGENT2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
