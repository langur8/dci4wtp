<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTICKETTYPELIST implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTICKETTYPELIST[] $D4WTPTICKETTYPELIST
     */
    protected $D4WTPTICKETTYPELIST = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTICKETTYPELIST[]
     */
    public function getD4WTPTICKETTYPELIST()
    {
      return $this->D4WTPTICKETTYPELIST;
    }

    /**
     * @param D4WTPTICKETTYPELIST[] $D4WTPTICKETTYPELIST
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTICKETTYPELIST
     */
    public function setD4WTPTICKETTYPELIST(array $D4WTPTICKETTYPELIST = null)
    {
      $this->D4WTPTICKETTYPELIST = $D4WTPTICKETTYPELIST;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTICKETTYPELIST[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTICKETTYPELIST
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTICKETTYPELIST[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTICKETTYPELIST $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTICKETTYPELIST[] = $value;
      } else {
        $this->D4WTPTICKETTYPELIST[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTICKETTYPELIST[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTICKETTYPELIST Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTICKETTYPELIST);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTICKETTYPELIST);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTICKETTYPELIST);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTICKETTYPELIST);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTICKETTYPELIST Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTICKETTYPELIST);
    }

}
