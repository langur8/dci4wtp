<?php

namespace Axess\Dci4Wtp;

class DTL4FAMCODINGMASTER
{

    /**
     * @var ArrayOfDTL4FAMCODINGMEMBER $ACTMCMEMBER
     */
    protected $ACTMCMEMBER = null;

    /**
     * @var float $NALLOCATEDTICKETS
     */
    protected $NALLOCATEDTICKETS = null;

    /**
     * @var float $NBLOCKEDTICKETS
     */
    protected $NBLOCKEDTICKETS = null;

    /**
     * @var float $NMASTERJOURNALNO
     */
    protected $NMASTERJOURNALNO = null;

    /**
     * @var float $NMASTERPOSNO
     */
    protected $NMASTERPOSNO = null;

    /**
     * @var float $NMASTERPROJNO
     */
    protected $NMASTERPROJNO = null;

    /**
     * @var float $NMASTERSERIALNO
     */
    protected $NMASTERSERIALNO = null;

    /**
     * @var float $NUNALLOCATEDTICKETS
     */
    protected $NUNALLOCATEDTICKETS = null;

    /**
     * @var string $SZMASTERMEDIAID
     */
    protected $SZMASTERMEDIAID = null;

    /**
     * @var string $SZMASTERVALIDFROM
     */
    protected $SZMASTERVALIDFROM = null;

    /**
     * @var string $SZMASTERVALIDTO
     */
    protected $SZMASTERVALIDTO = null;

    /**
     * @var string $SZMASTERWTPNUMBER
     */
    protected $SZMASTERWTPNUMBER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfDTL4FAMCODINGMEMBER
     */
    public function getACTMCMEMBER()
    {
      return $this->ACTMCMEMBER;
    }

    /**
     * @param ArrayOfDTL4FAMCODINGMEMBER $ACTMCMEMBER
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMASTER
     */
    public function setACTMCMEMBER($ACTMCMEMBER)
    {
      $this->ACTMCMEMBER = $ACTMCMEMBER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNALLOCATEDTICKETS()
    {
      return $this->NALLOCATEDTICKETS;
    }

    /**
     * @param float $NALLOCATEDTICKETS
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMASTER
     */
    public function setNALLOCATEDTICKETS($NALLOCATEDTICKETS)
    {
      $this->NALLOCATEDTICKETS = $NALLOCATEDTICKETS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKEDTICKETS()
    {
      return $this->NBLOCKEDTICKETS;
    }

    /**
     * @param float $NBLOCKEDTICKETS
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMASTER
     */
    public function setNBLOCKEDTICKETS($NBLOCKEDTICKETS)
    {
      $this->NBLOCKEDTICKETS = $NBLOCKEDTICKETS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERJOURNALNO()
    {
      return $this->NMASTERJOURNALNO;
    }

    /**
     * @param float $NMASTERJOURNALNO
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMASTER
     */
    public function setNMASTERJOURNALNO($NMASTERJOURNALNO)
    {
      $this->NMASTERJOURNALNO = $NMASTERJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERPOSNO()
    {
      return $this->NMASTERPOSNO;
    }

    /**
     * @param float $NMASTERPOSNO
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMASTER
     */
    public function setNMASTERPOSNO($NMASTERPOSNO)
    {
      $this->NMASTERPOSNO = $NMASTERPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERPROJNO()
    {
      return $this->NMASTERPROJNO;
    }

    /**
     * @param float $NMASTERPROJNO
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMASTER
     */
    public function setNMASTERPROJNO($NMASTERPROJNO)
    {
      $this->NMASTERPROJNO = $NMASTERPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERSERIALNO()
    {
      return $this->NMASTERSERIALNO;
    }

    /**
     * @param float $NMASTERSERIALNO
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMASTER
     */
    public function setNMASTERSERIALNO($NMASTERSERIALNO)
    {
      $this->NMASTERSERIALNO = $NMASTERSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNALLOCATEDTICKETS()
    {
      return $this->NUNALLOCATEDTICKETS;
    }

    /**
     * @param float $NUNALLOCATEDTICKETS
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMASTER
     */
    public function setNUNALLOCATEDTICKETS($NUNALLOCATEDTICKETS)
    {
      $this->NUNALLOCATEDTICKETS = $NUNALLOCATEDTICKETS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASTERMEDIAID()
    {
      return $this->SZMASTERMEDIAID;
    }

    /**
     * @param string $SZMASTERMEDIAID
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMASTER
     */
    public function setSZMASTERMEDIAID($SZMASTERMEDIAID)
    {
      $this->SZMASTERMEDIAID = $SZMASTERMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASTERVALIDFROM()
    {
      return $this->SZMASTERVALIDFROM;
    }

    /**
     * @param string $SZMASTERVALIDFROM
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMASTER
     */
    public function setSZMASTERVALIDFROM($SZMASTERVALIDFROM)
    {
      $this->SZMASTERVALIDFROM = $SZMASTERVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASTERVALIDTO()
    {
      return $this->SZMASTERVALIDTO;
    }

    /**
     * @param string $SZMASTERVALIDTO
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMASTER
     */
    public function setSZMASTERVALIDTO($SZMASTERVALIDTO)
    {
      $this->SZMASTERVALIDTO = $SZMASTERVALIDTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASTERWTPNUMBER()
    {
      return $this->SZMASTERWTPNUMBER;
    }

    /**
     * @param string $SZMASTERWTPNUMBER
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMASTER
     */
    public function setSZMASTERWTPNUMBER($SZMASTERWTPNUMBER)
    {
      $this->SZMASTERWTPNUMBER = $SZMASTERWTPNUMBER;
      return $this;
    }

}
