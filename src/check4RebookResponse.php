<?php

namespace Axess\Dci4Wtp;

class check4RebookResponse
{

    /**
     * @var D4WTPCHECK4REBOOKRESULT $check4RebookResult
     */
    protected $check4RebookResult = null;

    /**
     * @param D4WTPCHECK4REBOOKRESULT $check4RebookResult
     */
    public function __construct($check4RebookResult)
    {
      $this->check4RebookResult = $check4RebookResult;
    }

    /**
     * @return D4WTPCHECK4REBOOKRESULT
     */
    public function getCheck4RebookResult()
    {
      return $this->check4RebookResult;
    }

    /**
     * @param D4WTPCHECK4REBOOKRESULT $check4RebookResult
     * @return \Axess\Dci4Wtp\check4RebookResponse
     */
    public function setCheck4RebookResult($check4RebookResult)
    {
      $this->check4RebookResult = $check4RebookResult;
      return $this;
    }

}
