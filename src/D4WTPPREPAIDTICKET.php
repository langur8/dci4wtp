<?php

namespace Axess\Dci4Wtp;

class D4WTPPREPAIDTICKET
{

    /**
     * @var float $NCODINGCASHIERID
     */
    protected $NCODINGCASHIERID = null;

    /**
     * @var float $NCODINGPOSNR
     */
    protected $NCODINGPOSNR = null;

    /**
     * @var float $NCODINGPROJNR
     */
    protected $NCODINGPROJNR = null;

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var string $NKARTENNR
     */
    protected $NKARTENNR = null;

    /**
     * @var string $NMEDIAID
     */
    protected $NMEDIAID = null;

    /**
     * @var float $NORDERSTATUSNR
     */
    protected $NORDERSTATUSNR = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var string $SZCODINGCASHIERNAME
     */
    protected $SZCODINGCASHIERNAME = null;

    /**
     * @var string $SZCODINGDATE
     */
    protected $SZCODINGDATE = null;

    /**
     * @var string $SZCODINGPOSNAME
     */
    protected $SZCODINGPOSNAME = null;

    /**
     * @var string $SZEXTORDERNR
     */
    protected $SZEXTORDERNR = null;

    /**
     * @var string $SZINTERNALORDERNR
     */
    protected $SZINTERNALORDERNR = null;

    /**
     * @var string $SZORDERSTATUSNAME
     */
    protected $SZORDERSTATUSNAME = null;

    /**
     * @var string $SZPRODTIMESTAMP
     */
    protected $SZPRODTIMESTAMP = null;

    /**
     * @var string $SZUPDATE
     */
    protected $SZUPDATE = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    /**
     * @var string $SZWTPNR32
     */
    protected $SZWTPNR32 = null;

    /**
     * @var string $SZWTPNR64
     */
    protected $SZWTPNR64 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCODINGCASHIERID()
    {
      return $this->NCODINGCASHIERID;
    }

    /**
     * @param float $NCODINGCASHIERID
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setNCODINGCASHIERID($NCODINGCASHIERID)
    {
      $this->NCODINGCASHIERID = $NCODINGCASHIERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCODINGPOSNR()
    {
      return $this->NCODINGPOSNR;
    }

    /**
     * @param float $NCODINGPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setNCODINGPOSNR($NCODINGPOSNR)
    {
      $this->NCODINGPOSNR = $NCODINGPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCODINGPROJNR()
    {
      return $this->NCODINGPROJNR;
    }

    /**
     * @param float $NCODINGPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setNCODINGPROJNR($NCODINGPROJNR)
    {
      $this->NCODINGPROJNR = $NCODINGPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getNKARTENNR()
    {
      return $this->NKARTENNR;
    }

    /**
     * @param string $NKARTENNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setNKARTENNR($NKARTENNR)
    {
      $this->NKARTENNR = $NKARTENNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getNMEDIAID()
    {
      return $this->NMEDIAID;
    }

    /**
     * @param string $NMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setNMEDIAID($NMEDIAID)
    {
      $this->NMEDIAID = $NMEDIAID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORDERSTATUSNR()
    {
      return $this->NORDERSTATUSNR;
    }

    /**
     * @param float $NORDERSTATUSNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setNORDERSTATUSNR($NORDERSTATUSNR)
    {
      $this->NORDERSTATUSNR = $NORDERSTATUSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGCASHIERNAME()
    {
      return $this->SZCODINGCASHIERNAME;
    }

    /**
     * @param string $SZCODINGCASHIERNAME
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setSZCODINGCASHIERNAME($SZCODINGCASHIERNAME)
    {
      $this->SZCODINGCASHIERNAME = $SZCODINGCASHIERNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGDATE()
    {
      return $this->SZCODINGDATE;
    }

    /**
     * @param string $SZCODINGDATE
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setSZCODINGDATE($SZCODINGDATE)
    {
      $this->SZCODINGDATE = $SZCODINGDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGPOSNAME()
    {
      return $this->SZCODINGPOSNAME;
    }

    /**
     * @param string $SZCODINGPOSNAME
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setSZCODINGPOSNAME($SZCODINGPOSNAME)
    {
      $this->SZCODINGPOSNAME = $SZCODINGPOSNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTORDERNR()
    {
      return $this->SZEXTORDERNR;
    }

    /**
     * @param string $SZEXTORDERNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setSZEXTORDERNR($SZEXTORDERNR)
    {
      $this->SZEXTORDERNR = $SZEXTORDERNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZINTERNALORDERNR()
    {
      return $this->SZINTERNALORDERNR;
    }

    /**
     * @param string $SZINTERNALORDERNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setSZINTERNALORDERNR($SZINTERNALORDERNR)
    {
      $this->SZINTERNALORDERNR = $SZINTERNALORDERNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZORDERSTATUSNAME()
    {
      return $this->SZORDERSTATUSNAME;
    }

    /**
     * @param string $SZORDERSTATUSNAME
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setSZORDERSTATUSNAME($SZORDERSTATUSNAME)
    {
      $this->SZORDERSTATUSNAME = $SZORDERSTATUSNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPRODTIMESTAMP()
    {
      return $this->SZPRODTIMESTAMP;
    }

    /**
     * @param string $SZPRODTIMESTAMP
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setSZPRODTIMESTAMP($SZPRODTIMESTAMP)
    {
      $this->SZPRODTIMESTAMP = $SZPRODTIMESTAMP;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUPDATE()
    {
      return $this->SZUPDATE;
    }

    /**
     * @param string $SZUPDATE
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setSZUPDATE($SZUPDATE)
    {
      $this->SZUPDATE = $SZUPDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNR32()
    {
      return $this->SZWTPNR32;
    }

    /**
     * @param string $SZWTPNR32
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setSZWTPNR32($SZWTPNR32)
    {
      $this->SZWTPNR32 = $SZWTPNR32;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNR64()
    {
      return $this->SZWTPNR64;
    }

    /**
     * @param string $SZWTPNR64
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET
     */
    public function setSZWTPNR64($SZWTPNR64)
    {
      $this->SZWTPNR64 = $SZWTPNR64;
      return $this;
    }

}
