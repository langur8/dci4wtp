<?php

namespace Axess\Dci4Wtp;

class editOrderHeaderResponse
{

    /**
     * @var D4WTPEDITORDERRES $editOrderHeaderResult
     */
    protected $editOrderHeaderResult = null;

    /**
     * @param D4WTPEDITORDERRES $editOrderHeaderResult
     */
    public function __construct($editOrderHeaderResult)
    {
      $this->editOrderHeaderResult = $editOrderHeaderResult;
    }

    /**
     * @return D4WTPEDITORDERRES
     */
    public function getEditOrderHeaderResult()
    {
      return $this->editOrderHeaderResult;
    }

    /**
     * @param D4WTPEDITORDERRES $editOrderHeaderResult
     * @return \Axess\Dci4Wtp\editOrderHeaderResponse
     */
    public function setEditOrderHeaderResult($editOrderHeaderResult)
    {
      $this->editOrderHeaderResult = $editOrderHeaderResult;
      return $this;
    }

}
