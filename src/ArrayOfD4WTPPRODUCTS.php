<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPRODUCTS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPRODUCTS[] $D4WTPPRODUCTS
     */
    protected $D4WTPPRODUCTS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPRODUCTS[]
     */
    public function getD4WTPPRODUCTS()
    {
      return $this->D4WTPPRODUCTS;
    }

    /**
     * @param D4WTPPRODUCTS[] $D4WTPPRODUCTS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPRODUCTS
     */
    public function setD4WTPPRODUCTS(array $D4WTPPRODUCTS = null)
    {
      $this->D4WTPPRODUCTS = $D4WTPPRODUCTS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPRODUCTS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPRODUCTS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPRODUCTS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPRODUCTS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPRODUCTS[] = $value;
      } else {
        $this->D4WTPPRODUCTS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPRODUCTS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPRODUCTS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPRODUCTS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPRODUCTS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPRODUCTS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPRODUCTS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPRODUCTS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPRODUCTS);
    }

}
