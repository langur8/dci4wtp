<?php

namespace Axess\Dci4Wtp;

class getCompanyResponse
{

    /**
     * @var D4WTPGETCOMPANYRESULT $getCompanyResult
     */
    protected $getCompanyResult = null;

    /**
     * @param D4WTPGETCOMPANYRESULT $getCompanyResult
     */
    public function __construct($getCompanyResult)
    {
      $this->getCompanyResult = $getCompanyResult;
    }

    /**
     * @return D4WTPGETCOMPANYRESULT
     */
    public function getGetCompanyResult()
    {
      return $this->getCompanyResult;
    }

    /**
     * @param D4WTPGETCOMPANYRESULT $getCompanyResult
     * @return \Axess\Dci4Wtp\getCompanyResponse
     */
    public function setGetCompanyResult($getCompanyResult)
    {
      $this->getCompanyResult = $getCompanyResult;
      return $this;
    }

}
