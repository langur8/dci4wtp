<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPBLOCKSATZ implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPBLOCKSATZ[] $D4WTPBLOCKSATZ
     */
    protected $D4WTPBLOCKSATZ = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPBLOCKSATZ[]
     */
    public function getD4WTPBLOCKSATZ()
    {
      return $this->D4WTPBLOCKSATZ;
    }

    /**
     * @param D4WTPBLOCKSATZ[] $D4WTPBLOCKSATZ
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPBLOCKSATZ
     */
    public function setD4WTPBLOCKSATZ(array $D4WTPBLOCKSATZ = null)
    {
      $this->D4WTPBLOCKSATZ = $D4WTPBLOCKSATZ;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPBLOCKSATZ[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPBLOCKSATZ
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPBLOCKSATZ[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPBLOCKSATZ $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPBLOCKSATZ[] = $value;
      } else {
        $this->D4WTPBLOCKSATZ[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPBLOCKSATZ[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPBLOCKSATZ Return the current element
     */
    public function current()
    {
      return current($this->D4WTPBLOCKSATZ);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPBLOCKSATZ);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPBLOCKSATZ);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPBLOCKSATZ);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPBLOCKSATZ Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPBLOCKSATZ);
    }

}
