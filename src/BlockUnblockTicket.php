<?php

namespace Axess\Dci4Wtp;

class BlockUnblockTicket
{

    /**
     * @var D4WTPBLOCKTICKET $i_blockunblockticketpara
     */
    protected $i_blockunblockticketpara = null;

    /**
     * @param D4WTPBLOCKTICKET $i_blockunblockticketpara
     */
    public function __construct($i_blockunblockticketpara)
    {
      $this->i_blockunblockticketpara = $i_blockunblockticketpara;
    }

    /**
     * @return D4WTPBLOCKTICKET
     */
    public function getI_blockunblockticketpara()
    {
      return $this->i_blockunblockticketpara;
    }

    /**
     * @param D4WTPBLOCKTICKET $i_blockunblockticketpara
     * @return \Axess\Dci4Wtp\BlockUnblockTicket
     */
    public function setI_blockunblockticketpara($i_blockunblockticketpara)
    {
      $this->i_blockunblockticketpara = $i_blockunblockticketpara;
      return $this;
    }

}
