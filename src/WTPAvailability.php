<?php

namespace Axess\Dci4Wtp;

class WTPAvailability
{

    /**
     * @var string $Date
     */
    protected $Date = null;

    /**
     * @var int $nConfigNo
     */
    protected $nConfigNo = null;

    /**
     * @var int $nFree
     */
    protected $nFree = null;

    /**
     * @var int $nPoolNr
     */
    protected $nPoolNr = null;

    /**
     * @var string $szName
     */
    protected $szName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDate()
    {
      return $this->Date;
    }

    /**
     * @param string $Date
     * @return \Axess\Dci4Wtp\WTPAvailability
     */
    public function setDate($Date)
    {
      $this->Date = $Date;
      return $this;
    }

    /**
     * @return int
     */
    public function getNConfigNo()
    {
      return $this->nConfigNo;
    }

    /**
     * @param int $nConfigNo
     * @return \Axess\Dci4Wtp\WTPAvailability
     */
    public function setNConfigNo($nConfigNo)
    {
      $this->nConfigNo = $nConfigNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getNFree()
    {
      return $this->nFree;
    }

    /**
     * @param int $nFree
     * @return \Axess\Dci4Wtp\WTPAvailability
     */
    public function setNFree($nFree)
    {
      $this->nFree = $nFree;
      return $this;
    }

    /**
     * @return int
     */
    public function getNPoolNr()
    {
      return $this->nPoolNr;
    }

    /**
     * @param int $nPoolNr
     * @return \Axess\Dci4Wtp\WTPAvailability
     */
    public function setNPoolNr($nPoolNr)
    {
      $this->nPoolNr = $nPoolNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzName()
    {
      return $this->szName;
    }

    /**
     * @param string $szName
     * @return \Axess\Dci4Wtp\WTPAvailability
     */
    public function setSzName($szName)
    {
      $this->szName = $szName;
      return $this;
    }

}
