<?php

namespace Axess\Dci4Wtp;

class D4WTPPAYMENTTYPE
{

    /**
     * @var float $NAMOUNT
     */
    protected $NAMOUNT = null;

    /**
     * @var float $NBASICPAYMENTTYPE
     */
    protected $NBASICPAYMENTTYPE = null;

    /**
     * @var float $NCUSTPAYMENTTYPE
     */
    protected $NCUSTPAYMENTTYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNAMOUNT()
    {
      return $this->NAMOUNT;
    }

    /**
     * @param float $NAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPPAYMENTTYPE
     */
    public function setNAMOUNT($NAMOUNT)
    {
      $this->NAMOUNT = $NAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBASICPAYMENTTYPE()
    {
      return $this->NBASICPAYMENTTYPE;
    }

    /**
     * @param float $NBASICPAYMENTTYPE
     * @return \Axess\Dci4Wtp\D4WTPPAYMENTTYPE
     */
    public function setNBASICPAYMENTTYPE($NBASICPAYMENTTYPE)
    {
      $this->NBASICPAYMENTTYPE = $NBASICPAYMENTTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPAYMENTTYPE()
    {
      return $this->NCUSTPAYMENTTYPE;
    }

    /**
     * @param float $NCUSTPAYMENTTYPE
     * @return \Axess\Dci4Wtp\D4WTPPAYMENTTYPE
     */
    public function setNCUSTPAYMENTTYPE($NCUSTPAYMENTTYPE)
    {
      $this->NCUSTPAYMENTTYPE = $NCUSTPAYMENTTYPE;
      return $this;
    }

}
