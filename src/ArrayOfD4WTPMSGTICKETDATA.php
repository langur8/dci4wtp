<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPMSGTICKETDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPMSGTICKETDATA[] $D4WTPMSGTICKETDATA
     */
    protected $D4WTPMSGTICKETDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPMSGTICKETDATA[]
     */
    public function getD4WTPMSGTICKETDATA()
    {
      return $this->D4WTPMSGTICKETDATA;
    }

    /**
     * @param D4WTPMSGTICKETDATA[] $D4WTPMSGTICKETDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPMSGTICKETDATA
     */
    public function setD4WTPMSGTICKETDATA(array $D4WTPMSGTICKETDATA = null)
    {
      $this->D4WTPMSGTICKETDATA = $D4WTPMSGTICKETDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPMSGTICKETDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPMSGTICKETDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPMSGTICKETDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPMSGTICKETDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPMSGTICKETDATA[] = $value;
      } else {
        $this->D4WTPMSGTICKETDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPMSGTICKETDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPMSGTICKETDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPMSGTICKETDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPMSGTICKETDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPMSGTICKETDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPMSGTICKETDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPMSGTICKETDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPMSGTICKETDATA);
    }

}
