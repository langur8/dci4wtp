<?php

namespace Axess\Dci4Wtp;

class D4WTPRIDESANDDROPS2
{

    /**
     * @var float $NALTITUDE
     */
    protected $NALTITUDE = null;

    /**
     * @var float $NPOENR
     */
    protected $NPOENR = null;

    /**
     * @var float $NPOOLNR
     */
    protected $NPOOLNR = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSERIALNR
     */
    protected $NSERIALNR = null;

    /**
     * @var float $NUNICODENR
     */
    protected $NUNICODENR = null;

    /**
     * @var float $NVERTICALDISTANCE
     */
    protected $NVERTICALDISTANCE = null;

    /**
     * @var string $SZDATEOFRIDE
     */
    protected $SZDATEOFRIDE = null;

    /**
     * @var \DateTime $SZDURATIONOFRIDE
     */
    protected $SZDURATIONOFRIDE = null;

    /**
     * @var string $SZPOENAME
     */
    protected $SZPOENAME = null;

    /**
     * @var string $SZTIMEOFRIDE
     */
    protected $SZTIMEOFRIDE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNALTITUDE()
    {
      return $this->NALTITUDE;
    }

    /**
     * @param float $NALTITUDE
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS2
     */
    public function setNALTITUDE($NALTITUDE)
    {
      $this->NALTITUDE = $NALTITUDE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOENR()
    {
      return $this->NPOENR;
    }

    /**
     * @param float $NPOENR
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS2
     */
    public function setNPOENR($NPOENR)
    {
      $this->NPOENR = $NPOENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNR()
    {
      return $this->NPOOLNR;
    }

    /**
     * @param float $NPOOLNR
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS2
     */
    public function setNPOOLNR($NPOOLNR)
    {
      $this->NPOOLNR = $NPOOLNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS2
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS2
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNR()
    {
      return $this->NSERIALNR;
    }

    /**
     * @param float $NSERIALNR
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS2
     */
    public function setNSERIALNR($NSERIALNR)
    {
      $this->NSERIALNR = $NSERIALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENR()
    {
      return $this->NUNICODENR;
    }

    /**
     * @param float $NUNICODENR
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS2
     */
    public function setNUNICODENR($NUNICODENR)
    {
      $this->NUNICODENR = $NUNICODENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVERTICALDISTANCE()
    {
      return $this->NVERTICALDISTANCE;
    }

    /**
     * @param float $NVERTICALDISTANCE
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS2
     */
    public function setNVERTICALDISTANCE($NVERTICALDISTANCE)
    {
      $this->NVERTICALDISTANCE = $NVERTICALDISTANCE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEOFRIDE()
    {
      return $this->SZDATEOFRIDE;
    }

    /**
     * @param string $SZDATEOFRIDE
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS2
     */
    public function setSZDATEOFRIDE($SZDATEOFRIDE)
    {
      $this->SZDATEOFRIDE = $SZDATEOFRIDE;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSZDURATIONOFRIDE()
    {
      if ($this->SZDURATIONOFRIDE == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->SZDURATIONOFRIDE);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $SZDURATIONOFRIDE
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS2
     */
    public function setSZDURATIONOFRIDE(\DateTime $SZDURATIONOFRIDE = null)
    {
      if ($SZDURATIONOFRIDE == null) {
       $this->SZDURATIONOFRIDE = null;
      } else {
        $this->SZDURATIONOFRIDE = $SZDURATIONOFRIDE->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOENAME()
    {
      return $this->SZPOENAME;
    }

    /**
     * @param string $SZPOENAME
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS2
     */
    public function setSZPOENAME($SZPOENAME)
    {
      $this->SZPOENAME = $SZPOENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTIMEOFRIDE()
    {
      return $this->SZTIMEOFRIDE;
    }

    /**
     * @param string $SZTIMEOFRIDE
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS2
     */
    public function setSZTIMEOFRIDE($SZTIMEOFRIDE)
    {
      $this->SZTIMEOFRIDE = $SZTIMEOFRIDE;
      return $this;
    }

}
