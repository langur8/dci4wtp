<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCONTINGENTOCCUPACY implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCONTINGENTOCCUPACY[] $D4WTPCONTINGENTOCCUPACY
     */
    protected $D4WTPCONTINGENTOCCUPACY = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCONTINGENTOCCUPACY[]
     */
    public function getD4WTPCONTINGENTOCCUPACY()
    {
      return $this->D4WTPCONTINGENTOCCUPACY;
    }

    /**
     * @param D4WTPCONTINGENTOCCUPACY[] $D4WTPCONTINGENTOCCUPACY
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCONTINGENTOCCUPACY
     */
    public function setD4WTPCONTINGENTOCCUPACY(array $D4WTPCONTINGENTOCCUPACY = null)
    {
      $this->D4WTPCONTINGENTOCCUPACY = $D4WTPCONTINGENTOCCUPACY;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCONTINGENTOCCUPACY[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCONTINGENTOCCUPACY
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCONTINGENTOCCUPACY[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCONTINGENTOCCUPACY $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCONTINGENTOCCUPACY[] = $value;
      } else {
        $this->D4WTPCONTINGENTOCCUPACY[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCONTINGENTOCCUPACY[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCONTINGENTOCCUPACY Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCONTINGENTOCCUPACY);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCONTINGENTOCCUPACY);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCONTINGENTOCCUPACY);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCONTINGENTOCCUPACY);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCONTINGENTOCCUPACY Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCONTINGENTOCCUPACY);
    }

}
