<?php

namespace Axess\Dci4Wtp;

class getDayOccupacy2Response
{

    /**
     * @var D4WTPGETDAYOCUPACYRESULT2 $getDayOccupacy2Result
     */
    protected $getDayOccupacy2Result = null;

    /**
     * @param D4WTPGETDAYOCUPACYRESULT2 $getDayOccupacy2Result
     */
    public function __construct($getDayOccupacy2Result)
    {
      $this->getDayOccupacy2Result = $getDayOccupacy2Result;
    }

    /**
     * @return D4WTPGETDAYOCUPACYRESULT2
     */
    public function getGetDayOccupacy2Result()
    {
      return $this->getDayOccupacy2Result;
    }

    /**
     * @param D4WTPGETDAYOCUPACYRESULT2 $getDayOccupacy2Result
     * @return \Axess\Dci4Wtp\getDayOccupacy2Response
     */
    public function setGetDayOccupacy2Result($getDayOccupacy2Result)
    {
      $this->getDayOccupacy2Result = $getDayOccupacy2Result;
      return $this;
    }

}
