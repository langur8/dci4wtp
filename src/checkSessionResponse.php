<?php

namespace Axess\Dci4Wtp;

class checkSessionResponse
{

    /**
     * @var D4WTPCHECKSESSIONRESULT $checkSessionResult
     */
    protected $checkSessionResult = null;

    /**
     * @param D4WTPCHECKSESSIONRESULT $checkSessionResult
     */
    public function __construct($checkSessionResult)
    {
      $this->checkSessionResult = $checkSessionResult;
    }

    /**
     * @return D4WTPCHECKSESSIONRESULT
     */
    public function getCheckSessionResult()
    {
      return $this->checkSessionResult;
    }

    /**
     * @param D4WTPCHECKSESSIONRESULT $checkSessionResult
     * @return \Axess\Dci4Wtp\checkSessionResponse
     */
    public function setCheckSessionResult($checkSessionResult)
    {
      $this->checkSessionResult = $checkSessionResult;
      return $this;
    }

}
