<?php

namespace Axess\Dci4Wtp;

class D4WTPRENTALSALE
{

    /**
     * @var ArrayOfD4WTPRENTALPRODUCT $ACTRENTALPRODUCT
     */
    protected $ACTRENTALPRODUCT = null;

    /**
     * @var float $NRENTALPOSNR
     */
    protected $NRENTALPOSNR = null;

    /**
     * @var float $NRENTALPROJNR
     */
    protected $NRENTALPROJNR = null;

    /**
     * @var float $NRENTALTRANSNR
     */
    protected $NRENTALTRANSNR = null;

    /**
     * @var string $SZEXTERNALREFERENCE
     */
    protected $SZEXTERNALREFERENCE = null;

    /**
     * @var string $SZISSUEDATE
     */
    protected $SZISSUEDATE = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPRENTALPRODUCT
     */
    public function getACTRENTALPRODUCT()
    {
      return $this->ACTRENTALPRODUCT;
    }

    /**
     * @param ArrayOfD4WTPRENTALPRODUCT $ACTRENTALPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPRENTALSALE
     */
    public function setACTRENTALPRODUCT($ACTRENTALPRODUCT)
    {
      $this->ACTRENTALPRODUCT = $ACTRENTALPRODUCT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPOSNR()
    {
      return $this->NRENTALPOSNR;
    }

    /**
     * @param float $NRENTALPOSNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALSALE
     */
    public function setNRENTALPOSNR($NRENTALPOSNR)
    {
      $this->NRENTALPOSNR = $NRENTALPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPROJNR()
    {
      return $this->NRENTALPROJNR;
    }

    /**
     * @param float $NRENTALPROJNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALSALE
     */
    public function setNRENTALPROJNR($NRENTALPROJNR)
    {
      $this->NRENTALPROJNR = $NRENTALPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALTRANSNR()
    {
      return $this->NRENTALTRANSNR;
    }

    /**
     * @param float $NRENTALTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALSALE
     */
    public function setNRENTALTRANSNR($NRENTALTRANSNR)
    {
      $this->NRENTALTRANSNR = $NRENTALTRANSNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTERNALREFERENCE()
    {
      return $this->SZEXTERNALREFERENCE;
    }

    /**
     * @param string $SZEXTERNALREFERENCE
     * @return \Axess\Dci4Wtp\D4WTPRENTALSALE
     */
    public function setSZEXTERNALREFERENCE($SZEXTERNALREFERENCE)
    {
      $this->SZEXTERNALREFERENCE = $SZEXTERNALREFERENCE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZISSUEDATE()
    {
      return $this->SZISSUEDATE;
    }

    /**
     * @param string $SZISSUEDATE
     * @return \Axess\Dci4Wtp\D4WTPRENTALSALE
     */
    public function setSZISSUEDATE($SZISSUEDATE)
    {
      $this->SZISSUEDATE = $SZISSUEDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPRENTALSALE
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPRENTALSALE
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
