<?php

namespace Axess\Dci4Wtp;

class QRCodeRecoveryData
{

    /**
     * @var string $CertId
     */
    protected $CertId = null;

    /**
     * @var string $CertIssuer
     */
    protected $CertIssuer = null;

    /**
     * @var string $CertValidFrom
     */
    protected $CertValidFrom = null;

    /**
     * @var string $CertValidUntil
     */
    protected $CertValidUntil = null;

    /**
     * @var string $CountryOfRecovery
     */
    protected $CountryOfRecovery = null;

    /**
     * @var string $FirstPositiveTestResult
     */
    protected $FirstPositiveTestResult = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCertId()
    {
      return $this->CertId;
    }

    /**
     * @param string $CertId
     * @return \Axess\Dci4Wtp\QRCodeRecoveryData
     */
    public function setCertId($CertId)
    {
      $this->CertId = $CertId;
      return $this;
    }

    /**
     * @return string
     */
    public function getCertIssuer()
    {
      return $this->CertIssuer;
    }

    /**
     * @param string $CertIssuer
     * @return \Axess\Dci4Wtp\QRCodeRecoveryData
     */
    public function setCertIssuer($CertIssuer)
    {
      $this->CertIssuer = $CertIssuer;
      return $this;
    }

    /**
     * @return string
     */
    public function getCertValidFrom()
    {
      return $this->CertValidFrom;
    }

    /**
     * @param string $CertValidFrom
     * @return \Axess\Dci4Wtp\QRCodeRecoveryData
     */
    public function setCertValidFrom($CertValidFrom)
    {
      $this->CertValidFrom = $CertValidFrom;
      return $this;
    }

    /**
     * @return string
     */
    public function getCertValidUntil()
    {
      return $this->CertValidUntil;
    }

    /**
     * @param string $CertValidUntil
     * @return \Axess\Dci4Wtp\QRCodeRecoveryData
     */
    public function setCertValidUntil($CertValidUntil)
    {
      $this->CertValidUntil = $CertValidUntil;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryOfRecovery()
    {
      return $this->CountryOfRecovery;
    }

    /**
     * @param string $CountryOfRecovery
     * @return \Axess\Dci4Wtp\QRCodeRecoveryData
     */
    public function setCountryOfRecovery($CountryOfRecovery)
    {
      $this->CountryOfRecovery = $CountryOfRecovery;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstPositiveTestResult()
    {
      return $this->FirstPositiveTestResult;
    }

    /**
     * @param string $FirstPositiveTestResult
     * @return \Axess\Dci4Wtp\QRCodeRecoveryData
     */
    public function setFirstPositiveTestResult($FirstPositiveTestResult)
    {
      $this->FirstPositiveTestResult = $FirstPositiveTestResult;
      return $this;
    }

}
