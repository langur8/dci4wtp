<?php

namespace Axess\Dci4Wtp;

class msgIssueTicket3Response
{

    /**
     * @var D4WTPMSGISSUETICKETRESULT2 $msgIssueTicket3Result
     */
    protected $msgIssueTicket3Result = null;

    /**
     * @param D4WTPMSGISSUETICKETRESULT2 $msgIssueTicket3Result
     */
    public function __construct($msgIssueTicket3Result)
    {
      $this->msgIssueTicket3Result = $msgIssueTicket3Result;
    }

    /**
     * @return D4WTPMSGISSUETICKETRESULT2
     */
    public function getMsgIssueTicket3Result()
    {
      return $this->msgIssueTicket3Result;
    }

    /**
     * @param D4WTPMSGISSUETICKETRESULT2 $msgIssueTicket3Result
     * @return \Axess\Dci4Wtp\msgIssueTicket3Response
     */
    public function setMsgIssueTicket3Result($msgIssueTicket3Result)
    {
      $this->msgIssueTicket3Result = $msgIssueTicket3Result;
      return $this;
    }

}
