<?php

namespace Axess\Dci4Wtp;

class D4WTPFIRMENBEZAHLARTENEXCL
{

    /**
     * @var float $FTOTALLIMIT
     */
    protected $FTOTALLIMIT = null;

    /**
     * @var float $FTRANSACTIONLIMIT
     */
    protected $FTRANSACTIONLIMIT = null;

    /**
     * @var float $NFIRMENKASSANR
     */
    protected $NFIRMENKASSANR = null;

    /**
     * @var float $NFIRMENNR
     */
    protected $NFIRMENNR = null;

    /**
     * @var float $NFIRMENPROJNR
     */
    protected $NFIRMENPROJNR = null;

    /**
     * @var float $NGRUNDBEZARTNR
     */
    protected $NGRUNDBEZARTNR = null;

    /**
     * @var float $NKUNDENBEZARTNR
     */
    protected $NKUNDENBEZARTNR = null;

    /**
     * @var string $SZBESCH
     */
    protected $SZBESCH = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFTOTALLIMIT()
    {
      return $this->FTOTALLIMIT;
    }

    /**
     * @param float $FTOTALLIMIT
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBEZAHLARTENEXCL
     */
    public function setFTOTALLIMIT($FTOTALLIMIT)
    {
      $this->FTOTALLIMIT = $FTOTALLIMIT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFTRANSACTIONLIMIT()
    {
      return $this->FTRANSACTIONLIMIT;
    }

    /**
     * @param float $FTRANSACTIONLIMIT
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBEZAHLARTENEXCL
     */
    public function setFTRANSACTIONLIMIT($FTRANSACTIONLIMIT)
    {
      $this->FTRANSACTIONLIMIT = $FTRANSACTIONLIMIT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRMENKASSANR()
    {
      return $this->NFIRMENKASSANR;
    }

    /**
     * @param float $NFIRMENKASSANR
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBEZAHLARTENEXCL
     */
    public function setNFIRMENKASSANR($NFIRMENKASSANR)
    {
      $this->NFIRMENKASSANR = $NFIRMENKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRMENNR()
    {
      return $this->NFIRMENNR;
    }

    /**
     * @param float $NFIRMENNR
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBEZAHLARTENEXCL
     */
    public function setNFIRMENNR($NFIRMENNR)
    {
      $this->NFIRMENNR = $NFIRMENNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRMENPROJNR()
    {
      return $this->NFIRMENPROJNR;
    }

    /**
     * @param float $NFIRMENPROJNR
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBEZAHLARTENEXCL
     */
    public function setNFIRMENPROJNR($NFIRMENPROJNR)
    {
      $this->NFIRMENPROJNR = $NFIRMENPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNGRUNDBEZARTNR()
    {
      return $this->NGRUNDBEZARTNR;
    }

    /**
     * @param float $NGRUNDBEZARTNR
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBEZAHLARTENEXCL
     */
    public function setNGRUNDBEZARTNR($NGRUNDBEZARTNR)
    {
      $this->NGRUNDBEZARTNR = $NGRUNDBEZARTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKUNDENBEZARTNR()
    {
      return $this->NKUNDENBEZARTNR;
    }

    /**
     * @param float $NKUNDENBEZARTNR
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBEZAHLARTENEXCL
     */
    public function setNKUNDENBEZARTNR($NKUNDENBEZARTNR)
    {
      $this->NKUNDENBEZARTNR = $NKUNDENBEZARTNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBESCH()
    {
      return $this->SZBESCH;
    }

    /**
     * @param string $SZBESCH
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBEZAHLARTENEXCL
     */
    public function setSZBESCH($SZBESCH)
    {
      $this->SZBESCH = $SZBESCH;
      return $this;
    }

}
