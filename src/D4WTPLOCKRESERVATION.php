<?php

namespace Axess\Dci4Wtp;

class D4WTPLOCKRESERVATION
{

    /**
     * @var float $NCONTINGENTNO
     */
    protected $NCONTINGENTNO = null;

    /**
     * @var float $NCOUNTER
     */
    protected $NCOUNTER = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSUBCONTINGENTNO
     */
    protected $NSUBCONTINGENTNO = null;

    /**
     * @var float $NTIMEOUTINMINUTES
     */
    protected $NTIMEOUTINMINUTES = null;

    /**
     * @var string $SZDAY
     */
    protected $SZDAY = null;

    /**
     * @var string $SZRESERVATIONREFERENCE
     */
    protected $SZRESERVATIONREFERENCE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNO()
    {
      return $this->NCONTINGENTNO;
    }

    /**
     * @param float $NCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPLOCKRESERVATION
     */
    public function setNCONTINGENTNO($NCONTINGENTNO)
    {
      $this->NCONTINGENTNO = $NCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTER()
    {
      return $this->NCOUNTER;
    }

    /**
     * @param float $NCOUNTER
     * @return \Axess\Dci4Wtp\D4WTPLOCKRESERVATION
     */
    public function setNCOUNTER($NCOUNTER)
    {
      $this->NCOUNTER = $NCOUNTER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPLOCKRESERVATION
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPLOCKRESERVATION
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTINGENTNO()
    {
      return $this->NSUBCONTINGENTNO;
    }

    /**
     * @param float $NSUBCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPLOCKRESERVATION
     */
    public function setNSUBCONTINGENTNO($NSUBCONTINGENTNO)
    {
      $this->NSUBCONTINGENTNO = $NSUBCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTIMEOUTINMINUTES()
    {
      return $this->NTIMEOUTINMINUTES;
    }

    /**
     * @param float $NTIMEOUTINMINUTES
     * @return \Axess\Dci4Wtp\D4WTPLOCKRESERVATION
     */
    public function setNTIMEOUTINMINUTES($NTIMEOUTINMINUTES)
    {
      $this->NTIMEOUTINMINUTES = $NTIMEOUTINMINUTES;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDAY()
    {
      return $this->SZDAY;
    }

    /**
     * @param string $SZDAY
     * @return \Axess\Dci4Wtp\D4WTPLOCKRESERVATION
     */
    public function setSZDAY($SZDAY)
    {
      $this->SZDAY = $SZDAY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRESERVATIONREFERENCE()
    {
      return $this->SZRESERVATIONREFERENCE;
    }

    /**
     * @param string $SZRESERVATIONREFERENCE
     * @return \Axess\Dci4Wtp\D4WTPLOCKRESERVATION
     */
    public function setSZRESERVATIONREFERENCE($SZRESERVATIONREFERENCE)
    {
      $this->SZRESERVATIONREFERENCE = $SZRESERVATIONREFERENCE;
      return $this;
    }

}
