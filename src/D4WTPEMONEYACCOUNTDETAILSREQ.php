<?php

namespace Axess\Dci4Wtp;

class D4WTPEMONEYACCOUNTDETAILSREQ
{

    /**
     * @var D4WTPEMONEYACCOUNT $CTEMONEYACCOUNT
     */
    protected $CTEMONEYACCOUNT = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPEMONEYACCOUNT
     */
    public function getCTEMONEYACCOUNT()
    {
      return $this->CTEMONEYACCOUNT;
    }

    /**
     * @param D4WTPEMONEYACCOUNT $CTEMONEYACCOUNT
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSREQ
     */
    public function setCTEMONEYACCOUNT($CTEMONEYACCOUNT)
    {
      $this->CTEMONEYACCOUNT = $CTEMONEYACCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
