<?php

namespace Axess\Dci4Wtp;

class D4WTPCANCELARTICLE2RESULT
{

    /**
     * @var float $FTAXAMOUNT
     */
    protected $FTAXAMOUNT = null;

    /**
     * @var float $FTAXPERCENT
     */
    protected $FTAXPERCENT = null;

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NNETTOTARIFF
     */
    protected $NNETTOTARIFF = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NREFUNDTARIF
     */
    protected $NREFUNDTARIF = null;

    /**
     * @var float $NSTUCKNR
     */
    protected $NSTUCKNR = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    /**
     * @var string $SZARTICLENAME
     */
    protected $SZARTICLENAME = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZTAXNAME
     */
    protected $SZTAXNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFTAXAMOUNT()
    {
      return $this->FTAXAMOUNT;
    }

    /**
     * @param float $FTAXAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPCANCELARTICLE2RESULT
     */
    public function setFTAXAMOUNT($FTAXAMOUNT)
    {
      $this->FTAXAMOUNT = $FTAXAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFTAXPERCENT()
    {
      return $this->FTAXPERCENT;
    }

    /**
     * @param float $FTAXPERCENT
     * @return \Axess\Dci4Wtp\D4WTPCANCELARTICLE2RESULT
     */
    public function setFTAXPERCENT($FTAXPERCENT)
    {
      $this->FTAXPERCENT = $FTAXPERCENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPCANCELARTICLE2RESULT
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELARTICLE2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNETTOTARIFF()
    {
      return $this->NNETTOTARIFF;
    }

    /**
     * @param float $NNETTOTARIFF
     * @return \Axess\Dci4Wtp\D4WTPCANCELARTICLE2RESULT
     */
    public function setNNETTOTARIFF($NNETTOTARIFF)
    {
      $this->NNETTOTARIFF = $NNETTOTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELARTICLE2RESULT
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELARTICLE2RESULT
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNREFUNDTARIF()
    {
      return $this->NREFUNDTARIF;
    }

    /**
     * @param float $NREFUNDTARIF
     * @return \Axess\Dci4Wtp\D4WTPCANCELARTICLE2RESULT
     */
    public function setNREFUNDTARIF($NREFUNDTARIF)
    {
      $this->NREFUNDTARIF = $NREFUNDTARIF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTUCKNR()
    {
      return $this->NSTUCKNR;
    }

    /**
     * @param float $NSTUCKNR
     * @return \Axess\Dci4Wtp\D4WTPCANCELARTICLE2RESULT
     */
    public function setNSTUCKNR($NSTUCKNR)
    {
      $this->NSTUCKNR = $NSTUCKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELARTICLE2RESULT
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZARTICLENAME()
    {
      return $this->SZARTICLENAME;
    }

    /**
     * @param string $SZARTICLENAME
     * @return \Axess\Dci4Wtp\D4WTPCANCELARTICLE2RESULT
     */
    public function setSZARTICLENAME($SZARTICLENAME)
    {
      $this->SZARTICLENAME = $SZARTICLENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCANCELARTICLE2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTAXNAME()
    {
      return $this->SZTAXNAME;
    }

    /**
     * @param string $SZTAXNAME
     * @return \Axess\Dci4Wtp\D4WTPCANCELARTICLE2RESULT
     */
    public function setSZTAXNAME($SZTAXNAME)
    {
      $this->SZTAXNAME = $SZTAXNAME;
      return $this;
    }

}
