<?php

namespace Axess\Dci4Wtp;

class D4WTPCREATEEXTORDERNRREQUEST
{

    /**
     * @var ArrayOfD4WTPEXTORDERGROUP $ACTEXTORDERGROUP
     */
    protected $ACTEXTORDERGROUP = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZPREFIXTYPE
     */
    protected $SZPREFIXTYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPEXTORDERGROUP
     */
    public function getACTEXTORDERGROUP()
    {
      return $this->ACTEXTORDERGROUP;
    }

    /**
     * @param ArrayOfD4WTPEXTORDERGROUP $ACTEXTORDERGROUP
     * @return \Axess\Dci4Wtp\D4WTPCREATEEXTORDERNRREQUEST
     */
    public function setACTEXTORDERGROUP($ACTEXTORDERGROUP)
    {
      $this->ACTEXTORDERGROUP = $ACTEXTORDERGROUP;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCREATEEXTORDERNRREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPREFIXTYPE()
    {
      return $this->SZPREFIXTYPE;
    }

    /**
     * @param string $SZPREFIXTYPE
     * @return \Axess\Dci4Wtp\D4WTPCREATEEXTORDERNRREQUEST
     */
    public function setSZPREFIXTYPE($SZPREFIXTYPE)
    {
      $this->SZPREFIXTYPE = $SZPREFIXTYPE;
      return $this;
    }

}
