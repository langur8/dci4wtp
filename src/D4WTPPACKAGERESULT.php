<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGERESULT
{

    /**
     * @var ArrayOfD4WTPPACKAGE $ACTPACKAGE
     */
    protected $ACTPACKAGE = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPACKAGE
     */
    public function getACTPACKAGE()
    {
      return $this->ACTPACKAGE;
    }

    /**
     * @param ArrayOfD4WTPPACKAGE $ACTPACKAGE
     * @return \Axess\Dci4Wtp\D4WTPPACKAGERESULT
     */
    public function setACTPACKAGE($ACTPACKAGE)
    {
      $this->ACTPACKAGE = $ACTPACKAGE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGERESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPACKAGERESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
