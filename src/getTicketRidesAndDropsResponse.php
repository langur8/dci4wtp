<?php

namespace Axess\Dci4Wtp;

class getTicketRidesAndDropsResponse
{

    /**
     * @var D4WTPGETTCKTRIDESDROPSRESULT $getTicketRidesAndDropsResult
     */
    protected $getTicketRidesAndDropsResult = null;

    /**
     * @param D4WTPGETTCKTRIDESDROPSRESULT $getTicketRidesAndDropsResult
     */
    public function __construct($getTicketRidesAndDropsResult)
    {
      $this->getTicketRidesAndDropsResult = $getTicketRidesAndDropsResult;
    }

    /**
     * @return D4WTPGETTCKTRIDESDROPSRESULT
     */
    public function getGetTicketRidesAndDropsResult()
    {
      return $this->getTicketRidesAndDropsResult;
    }

    /**
     * @param D4WTPGETTCKTRIDESDROPSRESULT $getTicketRidesAndDropsResult
     * @return \Axess\Dci4Wtp\getTicketRidesAndDropsResponse
     */
    public function setGetTicketRidesAndDropsResult($getTicketRidesAndDropsResult)
    {
      $this->getTicketRidesAndDropsResult = $getTicketRidesAndDropsResult;
      return $this;
    }

}
