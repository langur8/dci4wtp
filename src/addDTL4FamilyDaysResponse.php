<?php

namespace Axess\Dci4Wtp;

class addDTL4FamilyDaysResponse
{

    /**
     * @var D4WTPADDDTL4FAMILYDAYSRES $addDTL4FamilyDaysResult
     */
    protected $addDTL4FamilyDaysResult = null;

    /**
     * @param D4WTPADDDTL4FAMILYDAYSRES $addDTL4FamilyDaysResult
     */
    public function __construct($addDTL4FamilyDaysResult)
    {
      $this->addDTL4FamilyDaysResult = $addDTL4FamilyDaysResult;
    }

    /**
     * @return D4WTPADDDTL4FAMILYDAYSRES
     */
    public function getAddDTL4FamilyDaysResult()
    {
      return $this->addDTL4FamilyDaysResult;
    }

    /**
     * @param D4WTPADDDTL4FAMILYDAYSRES $addDTL4FamilyDaysResult
     * @return \Axess\Dci4Wtp\addDTL4FamilyDaysResponse
     */
    public function setAddDTL4FamilyDaysResult($addDTL4FamilyDaysResult)
    {
      $this->addDTL4FamilyDaysResult = $addDTL4FamilyDaysResult;
      return $this;
    }

}
