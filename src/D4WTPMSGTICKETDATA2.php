<?php

namespace Axess\Dci4Wtp;

class D4WTPMSGTICKETDATA2
{

    /**
     * @var float $BPREPAIDTICKET
     */
    protected $BPREPAIDTICKET = null;

    /**
     * @var float $NAPPTRANSLOGID
     */
    protected $NAPPTRANSLOGID = null;

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NBLOCKNO
     */
    protected $NBLOCKNO = null;

    /**
     * @var float $NEXTORDERTYPE
     */
    protected $NEXTORDERTYPE = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var string $SZEXTORDERNUMBER
     */
    protected $SZEXTORDERNUMBER = null;

    /**
     * @var string $SZMESSAGETYPE
     */
    protected $SZMESSAGETYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBPREPAIDTICKET()
    {
      return $this->BPREPAIDTICKET;
    }

    /**
     * @param float $BPREPAIDTICKET
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETDATA2
     */
    public function setBPREPAIDTICKET($BPREPAIDTICKET)
    {
      $this->BPREPAIDTICKET = $BPREPAIDTICKET;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAPPTRANSLOGID()
    {
      return $this->NAPPTRANSLOGID;
    }

    /**
     * @param float $NAPPTRANSLOGID
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETDATA2
     */
    public function setNAPPTRANSLOGID($NAPPTRANSLOGID)
    {
      $this->NAPPTRANSLOGID = $NAPPTRANSLOGID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETDATA2
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKNO()
    {
      return $this->NBLOCKNO;
    }

    /**
     * @param float $NBLOCKNO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETDATA2
     */
    public function setNBLOCKNO($NBLOCKNO)
    {
      $this->NBLOCKNO = $NBLOCKNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEXTORDERTYPE()
    {
      return $this->NEXTORDERTYPE;
    }

    /**
     * @param float $NEXTORDERTYPE
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETDATA2
     */
    public function setNEXTORDERTYPE($NEXTORDERTYPE)
    {
      $this->NEXTORDERTYPE = $NEXTORDERTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETDATA2
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETDATA2
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETDATA2
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTORDERNUMBER()
    {
      return $this->SZEXTORDERNUMBER;
    }

    /**
     * @param string $SZEXTORDERNUMBER
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETDATA2
     */
    public function setSZEXTORDERNUMBER($SZEXTORDERNUMBER)
    {
      $this->SZEXTORDERNUMBER = $SZEXTORDERNUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMESSAGETYPE()
    {
      return $this->SZMESSAGETYPE;
    }

    /**
     * @param string $SZMESSAGETYPE
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETDATA2
     */
    public function setSZMESSAGETYPE($SZMESSAGETYPE)
    {
      $this->SZMESSAGETYPE = $SZMESSAGETYPE;
      return $this;
    }

}
