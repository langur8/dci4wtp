<?php

namespace Axess\Dci4Wtp;

class getPackageTariffList
{

    /**
     * @var D4WTPPKGTARIFFLISTREQUEST $i_ctPkgTariffListReq
     */
    protected $i_ctPkgTariffListReq = null;

    /**
     * @param D4WTPPKGTARIFFLISTREQUEST $i_ctPkgTariffListReq
     */
    public function __construct($i_ctPkgTariffListReq)
    {
      $this->i_ctPkgTariffListReq = $i_ctPkgTariffListReq;
    }

    /**
     * @return D4WTPPKGTARIFFLISTREQUEST
     */
    public function getI_ctPkgTariffListReq()
    {
      return $this->i_ctPkgTariffListReq;
    }

    /**
     * @param D4WTPPKGTARIFFLISTREQUEST $i_ctPkgTariffListReq
     * @return \Axess\Dci4Wtp\getPackageTariffList
     */
    public function setI_ctPkgTariffListReq($i_ctPkgTariffListReq)
    {
      $this->i_ctPkgTariffListReq = $i_ctPkgTariffListReq;
      return $this;
    }

}
