<?php

namespace Axess\Dci4Wtp;

class issueTicket13Response
{

    /**
     * @var D4WTPISSUETICKET11RESULT $issueTicket13Result
     */
    protected $issueTicket13Result = null;

    /**
     * @param D4WTPISSUETICKET11RESULT $issueTicket13Result
     */
    public function __construct($issueTicket13Result)
    {
      $this->issueTicket13Result = $issueTicket13Result;
    }

    /**
     * @return D4WTPISSUETICKET11RESULT
     */
    public function getIssueTicket13Result()
    {
      return $this->issueTicket13Result;
    }

    /**
     * @param D4WTPISSUETICKET11RESULT $issueTicket13Result
     * @return \Axess\Dci4Wtp\issueTicket13Response
     */
    public function setIssueTicket13Result($issueTicket13Result)
    {
      $this->issueTicket13Result = $issueTicket13Result;
      return $this;
    }

}
