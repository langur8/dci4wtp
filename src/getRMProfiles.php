<?php

namespace Axess\Dci4Wtp;

class getRMProfiles
{

    /**
     * @var D4WTPGETRMPROFILESREQUEST $i_request
     */
    protected $i_request = null;

    /**
     * @param D4WTPGETRMPROFILESREQUEST $i_request
     */
    public function __construct($i_request)
    {
      $this->i_request = $i_request;
    }

    /**
     * @return D4WTPGETRMPROFILESREQUEST
     */
    public function getI_request()
    {
      return $this->i_request;
    }

    /**
     * @param D4WTPGETRMPROFILESREQUEST $i_request
     * @return \Axess\Dci4Wtp\getRMProfiles
     */
    public function setI_request($i_request)
    {
      $this->i_request = $i_request;
      return $this;
    }

}
