<?php

namespace Axess\Dci4Wtp;

class getCompany3Response
{

    /**
     * @var D4WTPGETCOMPANY3RESULT $getCompany3Result
     */
    protected $getCompany3Result = null;

    /**
     * @param D4WTPGETCOMPANY3RESULT $getCompany3Result
     */
    public function __construct($getCompany3Result)
    {
      $this->getCompany3Result = $getCompany3Result;
    }

    /**
     * @return D4WTPGETCOMPANY3RESULT
     */
    public function getGetCompany3Result()
    {
      return $this->getCompany3Result;
    }

    /**
     * @param D4WTPGETCOMPANY3RESULT $getCompany3Result
     * @return \Axess\Dci4Wtp\getCompany3Response
     */
    public function setGetCompany3Result($getCompany3Result)
    {
      $this->getCompany3Result = $getCompany3Result;
      return $this;
    }

}
