<?php

namespace Axess\Dci4Wtp;

class getARCData
{

    /**
     * @var D4WTPGETARCDATAREQ $i_ctGetArcDataReq
     */
    protected $i_ctGetArcDataReq = null;

    /**
     * @param D4WTPGETARCDATAREQ $i_ctGetArcDataReq
     */
    public function __construct($i_ctGetArcDataReq)
    {
      $this->i_ctGetArcDataReq = $i_ctGetArcDataReq;
    }

    /**
     * @return D4WTPGETARCDATAREQ
     */
    public function getI_ctGetArcDataReq()
    {
      return $this->i_ctGetArcDataReq;
    }

    /**
     * @param D4WTPGETARCDATAREQ $i_ctGetArcDataReq
     * @return \Axess\Dci4Wtp\getARCData
     */
    public function setI_ctGetArcDataReq($i_ctGetArcDataReq)
    {
      $this->i_ctGetArcDataReq = $i_ctGetArcDataReq;
      return $this;
    }

}
