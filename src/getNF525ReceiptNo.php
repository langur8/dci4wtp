<?php

namespace Axess\Dci4Wtp;

class getNF525ReceiptNo
{

    /**
     * @var D4WTPNF525RECEIPTNOREQUEST $i_ctNF525ReceiptNoReq
     */
    protected $i_ctNF525ReceiptNoReq = null;

    /**
     * @param D4WTPNF525RECEIPTNOREQUEST $i_ctNF525ReceiptNoReq
     */
    public function __construct($i_ctNF525ReceiptNoReq)
    {
      $this->i_ctNF525ReceiptNoReq = $i_ctNF525ReceiptNoReq;
    }

    /**
     * @return D4WTPNF525RECEIPTNOREQUEST
     */
    public function getI_ctNF525ReceiptNoReq()
    {
      return $this->i_ctNF525ReceiptNoReq;
    }

    /**
     * @param D4WTPNF525RECEIPTNOREQUEST $i_ctNF525ReceiptNoReq
     * @return \Axess\Dci4Wtp\getNF525ReceiptNo
     */
    public function setI_ctNF525ReceiptNoReq($i_ctNF525ReceiptNoReq)
    {
      $this->i_ctNF525ReceiptNoReq = $i_ctNF525ReceiptNoReq;
      return $this;
    }

}
