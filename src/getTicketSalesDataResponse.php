<?php

namespace Axess\Dci4Wtp;

class getTicketSalesDataResponse
{

    /**
     * @var D4WTPSALESDATARESULT $getTicketSalesDataResult
     */
    protected $getTicketSalesDataResult = null;

    /**
     * @param D4WTPSALESDATARESULT $getTicketSalesDataResult
     */
    public function __construct($getTicketSalesDataResult)
    {
      $this->getTicketSalesDataResult = $getTicketSalesDataResult;
    }

    /**
     * @return D4WTPSALESDATARESULT
     */
    public function getGetTicketSalesDataResult()
    {
      return $this->getTicketSalesDataResult;
    }

    /**
     * @param D4WTPSALESDATARESULT $getTicketSalesDataResult
     * @return \Axess\Dci4Wtp\getTicketSalesDataResponse
     */
    public function setGetTicketSalesDataResult($getTicketSalesDataResult)
    {
      $this->getTicketSalesDataResult = $getTicketSalesDataResult;
      return $this;
    }

}
