<?php

namespace Axess\Dci4Wtp;

class addDTL4FamilyDays
{

    /**
     * @var D4WTPADDDTL4FAMILYDAYSREQ $i_ctAddDTL4FamilyDaysReq
     */
    protected $i_ctAddDTL4FamilyDaysReq = null;

    /**
     * @param D4WTPADDDTL4FAMILYDAYSREQ $i_ctAddDTL4FamilyDaysReq
     */
    public function __construct($i_ctAddDTL4FamilyDaysReq)
    {
      $this->i_ctAddDTL4FamilyDaysReq = $i_ctAddDTL4FamilyDaysReq;
    }

    /**
     * @return D4WTPADDDTL4FAMILYDAYSREQ
     */
    public function getI_ctAddDTL4FamilyDaysReq()
    {
      return $this->i_ctAddDTL4FamilyDaysReq;
    }

    /**
     * @param D4WTPADDDTL4FAMILYDAYSREQ $i_ctAddDTL4FamilyDaysReq
     * @return \Axess\Dci4Wtp\addDTL4FamilyDays
     */
    public function setI_ctAddDTL4FamilyDaysReq($i_ctAddDTL4FamilyDaysReq)
    {
      $this->i_ctAddDTL4FamilyDaysReq = $i_ctAddDTL4FamilyDaysReq;
      return $this;
    }

}
