<?php

namespace Axess\Dci4Wtp;

class getDSGVOFlagsResponse
{

    /**
     * @var D4WTPGETDSGVOFLAGSRESULT $getDSGVOFlagsResult
     */
    protected $getDSGVOFlagsResult = null;

    /**
     * @param D4WTPGETDSGVOFLAGSRESULT $getDSGVOFlagsResult
     */
    public function __construct($getDSGVOFlagsResult)
    {
      $this->getDSGVOFlagsResult = $getDSGVOFlagsResult;
    }

    /**
     * @return D4WTPGETDSGVOFLAGSRESULT
     */
    public function getGetDSGVOFlagsResult()
    {
      return $this->getDSGVOFlagsResult;
    }

    /**
     * @param D4WTPGETDSGVOFLAGSRESULT $getDSGVOFlagsResult
     * @return \Axess\Dci4Wtp\getDSGVOFlagsResponse
     */
    public function setGetDSGVOFlagsResult($getDSGVOFlagsResult)
    {
      $this->getDSGVOFlagsResult = $getDSGVOFlagsResult;
      return $this;
    }

}
