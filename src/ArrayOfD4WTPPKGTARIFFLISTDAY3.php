<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPKGTARIFFLISTDAY3 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPKGTARIFFLISTDAY3[] $D4WTPPKGTARIFFLISTDAY3
     */
    protected $D4WTPPKGTARIFFLISTDAY3 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPKGTARIFFLISTDAY3[]
     */
    public function getD4WTPPKGTARIFFLISTDAY3()
    {
      return $this->D4WTPPKGTARIFFLISTDAY3;
    }

    /**
     * @param D4WTPPKGTARIFFLISTDAY3[] $D4WTPPKGTARIFFLISTDAY3
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPKGTARIFFLISTDAY3
     */
    public function setD4WTPPKGTARIFFLISTDAY3(array $D4WTPPKGTARIFFLISTDAY3 = null)
    {
      $this->D4WTPPKGTARIFFLISTDAY3 = $D4WTPPKGTARIFFLISTDAY3;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPKGTARIFFLISTDAY3[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPKGTARIFFLISTDAY3
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPKGTARIFFLISTDAY3[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPKGTARIFFLISTDAY3 $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPKGTARIFFLISTDAY3[] = $value;
      } else {
        $this->D4WTPPKGTARIFFLISTDAY3[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPKGTARIFFLISTDAY3[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPKGTARIFFLISTDAY3 Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPKGTARIFFLISTDAY3);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPKGTARIFFLISTDAY3);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPKGTARIFFLISTDAY3);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPKGTARIFFLISTDAY3);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPKGTARIFFLISTDAY3 Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPKGTARIFFLISTDAY3);
    }

}
