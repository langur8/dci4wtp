<?php

namespace Axess\Dci4Wtp;

class D4WTPCONTINGENTLIST
{

    /**
     * @var float $NCONTINGENTNR
     */
    protected $NCONTINGENTNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNR()
    {
      return $this->NCONTINGENTNR;
    }

    /**
     * @param float $NCONTINGENTNR
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST
     */
    public function setNCONTINGENTNR($NCONTINGENTNR)
    {
      $this->NCONTINGENTNR = $NCONTINGENTNR;
      return $this;
    }

}
