<?php

namespace Axess\Dci4Wtp;

class getBlankTypeListResponse
{

    /**
     * @var BLANKTYPESRES $getBlankTypeListResult
     */
    protected $getBlankTypeListResult = null;

    /**
     * @param BLANKTYPESRES $getBlankTypeListResult
     */
    public function __construct($getBlankTypeListResult)
    {
      $this->getBlankTypeListResult = $getBlankTypeListResult;
    }

    /**
     * @return BLANKTYPESRES
     */
    public function getGetBlankTypeListResult()
    {
      return $this->getBlankTypeListResult;
    }

    /**
     * @param BLANKTYPESRES $getBlankTypeListResult
     * @return \Axess\Dci4Wtp\getBlankTypeListResponse
     */
    public function setGetBlankTypeListResult($getBlankTypeListResult)
    {
      $this->getBlankTypeListResult = $getBlankTypeListResult;
      return $this;
    }

}
