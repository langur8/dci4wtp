<?php

namespace Axess\Dci4Wtp;

class D4AXCOUNTSTEPRES
{

    /**
     * @var float $FAXCOUNTDEPOSITSTEPSIZE
     */
    protected $FAXCOUNTDEPOSITSTEPSIZE = null;

    /**
     * @var float $FAXCOUNTLOWERLIMIT
     */
    protected $FAXCOUNTLOWERLIMIT = null;

    /**
     * @var float $FAXCOUNTPAYMENTSTEPSIZE
     */
    protected $FAXCOUNTPAYMENTSTEPSIZE = null;

    /**
     * @var float $FAXCOUNTUPPERLIMIT
     */
    protected $FAXCOUNTUPPERLIMIT = null;

    /**
     * @var float $NBANKNR
     */
    protected $NBANKNR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NGESNR
     */
    protected $NGESNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFAXCOUNTDEPOSITSTEPSIZE()
    {
      return $this->FAXCOUNTDEPOSITSTEPSIZE;
    }

    /**
     * @param float $FAXCOUNTDEPOSITSTEPSIZE
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPRES
     */
    public function setFAXCOUNTDEPOSITSTEPSIZE($FAXCOUNTDEPOSITSTEPSIZE)
    {
      $this->FAXCOUNTDEPOSITSTEPSIZE = $FAXCOUNTDEPOSITSTEPSIZE;
      return $this;
    }

    /**
     * @return float
     */
    public function getFAXCOUNTLOWERLIMIT()
    {
      return $this->FAXCOUNTLOWERLIMIT;
    }

    /**
     * @param float $FAXCOUNTLOWERLIMIT
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPRES
     */
    public function setFAXCOUNTLOWERLIMIT($FAXCOUNTLOWERLIMIT)
    {
      $this->FAXCOUNTLOWERLIMIT = $FAXCOUNTLOWERLIMIT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFAXCOUNTPAYMENTSTEPSIZE()
    {
      return $this->FAXCOUNTPAYMENTSTEPSIZE;
    }

    /**
     * @param float $FAXCOUNTPAYMENTSTEPSIZE
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPRES
     */
    public function setFAXCOUNTPAYMENTSTEPSIZE($FAXCOUNTPAYMENTSTEPSIZE)
    {
      $this->FAXCOUNTPAYMENTSTEPSIZE = $FAXCOUNTPAYMENTSTEPSIZE;
      return $this;
    }

    /**
     * @return float
     */
    public function getFAXCOUNTUPPERLIMIT()
    {
      return $this->FAXCOUNTUPPERLIMIT;
    }

    /**
     * @param float $FAXCOUNTUPPERLIMIT
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPRES
     */
    public function setFAXCOUNTUPPERLIMIT($FAXCOUNTUPPERLIMIT)
    {
      $this->FAXCOUNTUPPERLIMIT = $FAXCOUNTUPPERLIMIT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBANKNR()
    {
      return $this->NBANKNR;
    }

    /**
     * @param float $NBANKNR
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPRES
     */
    public function setNBANKNR($NBANKNR)
    {
      $this->NBANKNR = $NBANKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNGESNR()
    {
      return $this->NGESNR;
    }

    /**
     * @param float $NGESNR
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPRES
     */
    public function setNGESNR($NGESNR)
    {
      $this->NGESNR = $NGESNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPRES
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPRES
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
