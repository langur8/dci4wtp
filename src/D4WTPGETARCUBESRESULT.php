<?php

namespace Axess\Dci4Wtp;

class D4WTPGETARCUBESRESULT
{

    /**
     * @var ArrayOfD4WTPARCUBE $ACTARCUBE
     */
    protected $ACTARCUBE = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPARCUBE
     */
    public function getACTARCUBE()
    {
      return $this->ACTARCUBE;
    }

    /**
     * @param ArrayOfD4WTPARCUBE $ACTARCUBE
     * @return \Axess\Dci4Wtp\D4WTPGETARCUBESRESULT
     */
    public function setACTARCUBE($ACTARCUBE)
    {
      $this->ACTARCUBE = $ACTARCUBE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETARCUBESRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETARCUBESRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
