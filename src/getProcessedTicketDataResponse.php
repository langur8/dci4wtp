<?php

namespace Axess\Dci4Wtp;

class getProcessedTicketDataResponse
{

    /**
     * @var D4WTPGETPROCESSEDTKTDATARES $getProcessedTicketDataResult
     */
    protected $getProcessedTicketDataResult = null;

    /**
     * @param D4WTPGETPROCESSEDTKTDATARES $getProcessedTicketDataResult
     */
    public function __construct($getProcessedTicketDataResult)
    {
      $this->getProcessedTicketDataResult = $getProcessedTicketDataResult;
    }

    /**
     * @return D4WTPGETPROCESSEDTKTDATARES
     */
    public function getGetProcessedTicketDataResult()
    {
      return $this->getProcessedTicketDataResult;
    }

    /**
     * @param D4WTPGETPROCESSEDTKTDATARES $getProcessedTicketDataResult
     * @return \Axess\Dci4Wtp\getProcessedTicketDataResponse
     */
    public function setGetProcessedTicketDataResult($getProcessedTicketDataResult)
    {
      $this->getProcessedTicketDataResult = $getProcessedTicketDataResult;
      return $this;
    }

}
