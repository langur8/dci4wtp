<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTRANSACTION implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTRANSACTION[] $D4WTPTRANSACTION
     */
    protected $D4WTPTRANSACTION = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTRANSACTION[]
     */
    public function getD4WTPTRANSACTION()
    {
      return $this->D4WTPTRANSACTION;
    }

    /**
     * @param D4WTPTRANSACTION[] $D4WTPTRANSACTION
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTRANSACTION
     */
    public function setD4WTPTRANSACTION(array $D4WTPTRANSACTION = null)
    {
      $this->D4WTPTRANSACTION = $D4WTPTRANSACTION;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTRANSACTION[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTRANSACTION
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTRANSACTION[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTRANSACTION $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTRANSACTION[] = $value;
      } else {
        $this->D4WTPTRANSACTION[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTRANSACTION[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTRANSACTION Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTRANSACTION);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTRANSACTION);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTRANSACTION);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTRANSACTION);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTRANSACTION Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTRANSACTION);
    }

}
