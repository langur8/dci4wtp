<?php

namespace Axess\Dci4Wtp;

class eMoneyPayTransactionResponse
{

    /**
     * @var D4WTPEMONEYPAYTRANSACTIONRES $eMoneyPayTransactionResult
     */
    protected $eMoneyPayTransactionResult = null;

    /**
     * @param D4WTPEMONEYPAYTRANSACTIONRES $eMoneyPayTransactionResult
     */
    public function __construct($eMoneyPayTransactionResult)
    {
      $this->eMoneyPayTransactionResult = $eMoneyPayTransactionResult;
    }

    /**
     * @return D4WTPEMONEYPAYTRANSACTIONRES
     */
    public function getEMoneyPayTransactionResult()
    {
      return $this->eMoneyPayTransactionResult;
    }

    /**
     * @param D4WTPEMONEYPAYTRANSACTIONRES $eMoneyPayTransactionResult
     * @return \Axess\Dci4Wtp\eMoneyPayTransactionResponse
     */
    public function setEMoneyPayTransactionResult($eMoneyPayTransactionResult)
    {
      $this->eMoneyPayTransactionResult = $eMoneyPayTransactionResult;
      return $this;
    }

}
