<?php

namespace Axess\Dci4Wtp;

class D4WTPMNGPERSPHOTOREQUEST
{

    /**
     * @var base64Binary $BLPHOTODATA
     */
    protected $BLPHOTODATA = null;

    /**
     * @var float $NFILEOVERWRITE
     */
    protected $NFILEOVERWRITE = null;

    /**
     * @var float $NPERSKASSANR
     */
    protected $NPERSKASSANR = null;

    /**
     * @var float $NPERSNR
     */
    protected $NPERSNR = null;

    /**
     * @var float $NPERSPROJNR
     */
    protected $NPERSPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZMODE
     */
    protected $SZMODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return base64Binary
     */
    public function getBLPHOTODATA()
    {
      return $this->BLPHOTODATA;
    }

    /**
     * @param base64Binary $BLPHOTODATA
     * @return \Axess\Dci4Wtp\D4WTPMNGPERSPHOTOREQUEST
     */
    public function setBLPHOTODATA($BLPHOTODATA)
    {
      $this->BLPHOTODATA = $BLPHOTODATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFILEOVERWRITE()
    {
      return $this->NFILEOVERWRITE;
    }

    /**
     * @param float $NFILEOVERWRITE
     * @return \Axess\Dci4Wtp\D4WTPMNGPERSPHOTOREQUEST
     */
    public function setNFILEOVERWRITE($NFILEOVERWRITE)
    {
      $this->NFILEOVERWRITE = $NFILEOVERWRITE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSKASSANR()
    {
      return $this->NPERSKASSANR;
    }

    /**
     * @param float $NPERSKASSANR
     * @return \Axess\Dci4Wtp\D4WTPMNGPERSPHOTOREQUEST
     */
    public function setNPERSKASSANR($NPERSKASSANR)
    {
      $this->NPERSKASSANR = $NPERSKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNR()
    {
      return $this->NPERSNR;
    }

    /**
     * @param float $NPERSNR
     * @return \Axess\Dci4Wtp\D4WTPMNGPERSPHOTOREQUEST
     */
    public function setNPERSNR($NPERSNR)
    {
      $this->NPERSNR = $NPERSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNR()
    {
      return $this->NPERSPROJNR;
    }

    /**
     * @param float $NPERSPROJNR
     * @return \Axess\Dci4Wtp\D4WTPMNGPERSPHOTOREQUEST
     */
    public function setNPERSPROJNR($NPERSPROJNR)
    {
      $this->NPERSPROJNR = $NPERSPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPMNGPERSPHOTOREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMODE()
    {
      return $this->SZMODE;
    }

    /**
     * @param string $SZMODE
     * @return \Axess\Dci4Wtp\D4WTPMNGPERSPHOTOREQUEST
     */
    public function setSZMODE($SZMODE)
    {
      $this->SZMODE = $SZMODE;
      return $this;
    }

}
