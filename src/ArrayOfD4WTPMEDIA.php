<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPMEDIA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPMEDIA[] $D4WTPMEDIA
     */
    protected $D4WTPMEDIA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPMEDIA[]
     */
    public function getD4WTPMEDIA()
    {
      return $this->D4WTPMEDIA;
    }

    /**
     * @param D4WTPMEDIA[] $D4WTPMEDIA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPMEDIA
     */
    public function setD4WTPMEDIA(array $D4WTPMEDIA = null)
    {
      $this->D4WTPMEDIA = $D4WTPMEDIA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPMEDIA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPMEDIA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPMEDIA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPMEDIA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPMEDIA[] = $value;
      } else {
        $this->D4WTPMEDIA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPMEDIA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPMEDIA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPMEDIA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPMEDIA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPMEDIA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPMEDIA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPMEDIA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPMEDIA);
    }

}
