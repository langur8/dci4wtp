<?php

namespace Axess\Dci4Wtp;

class getPackageTariffListResponse
{

    /**
     * @var D4WTPPKGTARIFFLISTRESULT $getPackageTariffListResult
     */
    protected $getPackageTariffListResult = null;

    /**
     * @param D4WTPPKGTARIFFLISTRESULT $getPackageTariffListResult
     */
    public function __construct($getPackageTariffListResult)
    {
      $this->getPackageTariffListResult = $getPackageTariffListResult;
    }

    /**
     * @return D4WTPPKGTARIFFLISTRESULT
     */
    public function getGetPackageTariffListResult()
    {
      return $this->getPackageTariffListResult;
    }

    /**
     * @param D4WTPPKGTARIFFLISTRESULT $getPackageTariffListResult
     * @return \Axess\Dci4Wtp\getPackageTariffListResponse
     */
    public function setGetPackageTariffListResult($getPackageTariffListResult)
    {
      $this->getPackageTariffListResult = $getPackageTariffListResult;
      return $this;
    }

}
