<?php

namespace Axess\Dci4Wtp;

class getPersonTypesResponse
{

    /**
     * @var D4WTPPERSONTYPERESULT $getPersonTypesResult
     */
    protected $getPersonTypesResult = null;

    /**
     * @param D4WTPPERSONTYPERESULT $getPersonTypesResult
     */
    public function __construct($getPersonTypesResult)
    {
      $this->getPersonTypesResult = $getPersonTypesResult;
    }

    /**
     * @return D4WTPPERSONTYPERESULT
     */
    public function getGetPersonTypesResult()
    {
      return $this->getPersonTypesResult;
    }

    /**
     * @param D4WTPPERSONTYPERESULT $getPersonTypesResult
     * @return \Axess\Dci4Wtp\getPersonTypesResponse
     */
    public function setGetPersonTypesResult($getPersonTypesResult)
    {
      $this->getPersonTypesResult = $getPersonTypesResult;
      return $this;
    }

}
