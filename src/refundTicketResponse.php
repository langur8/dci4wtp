<?php

namespace Axess\Dci4Wtp;

class refundTicketResponse
{

    /**
     * @var D4WTPREFUNDTICKETRESULT $refundTicketResult
     */
    protected $refundTicketResult = null;

    /**
     * @param D4WTPREFUNDTICKETRESULT $refundTicketResult
     */
    public function __construct($refundTicketResult)
    {
      $this->refundTicketResult = $refundTicketResult;
    }

    /**
     * @return D4WTPREFUNDTICKETRESULT
     */
    public function getRefundTicketResult()
    {
      return $this->refundTicketResult;
    }

    /**
     * @param D4WTPREFUNDTICKETRESULT $refundTicketResult
     * @return \Axess\Dci4Wtp\refundTicketResponse
     */
    public function setRefundTicketResult($refundTicketResult)
    {
      $this->refundTicketResult = $refundTicketResult;
      return $this;
    }

}
