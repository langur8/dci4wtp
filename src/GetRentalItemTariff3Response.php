<?php

namespace Axess\Dci4Wtp;

class GetRentalItemTariff3Response
{

    /**
     * @var D4WTPGETRENTALITEMTARIFF3RES $GetRentalItemTariff3Result
     */
    protected $GetRentalItemTariff3Result = null;

    /**
     * @param D4WTPGETRENTALITEMTARIFF3RES $GetRentalItemTariff3Result
     */
    public function __construct($GetRentalItemTariff3Result)
    {
      $this->GetRentalItemTariff3Result = $GetRentalItemTariff3Result;
    }

    /**
     * @return D4WTPGETRENTALITEMTARIFF3RES
     */
    public function getGetRentalItemTariff3Result()
    {
      return $this->GetRentalItemTariff3Result;
    }

    /**
     * @param D4WTPGETRENTALITEMTARIFF3RES $GetRentalItemTariff3Result
     * @return \Axess\Dci4Wtp\GetRentalItemTariff3Response
     */
    public function setGetRentalItemTariff3Result($GetRentalItemTariff3Result)
    {
      $this->GetRentalItemTariff3Result = $GetRentalItemTariff3Result;
      return $this;
    }

}
