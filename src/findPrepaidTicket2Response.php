<?php

namespace Axess\Dci4Wtp;

class findPrepaidTicket2Response
{

    /**
     * @var D4WTPREPAIDRESULT2 $findPrepaidTicket2Result
     */
    protected $findPrepaidTicket2Result = null;

    /**
     * @param D4WTPREPAIDRESULT2 $findPrepaidTicket2Result
     */
    public function __construct($findPrepaidTicket2Result)
    {
      $this->findPrepaidTicket2Result = $findPrepaidTicket2Result;
    }

    /**
     * @return D4WTPREPAIDRESULT2
     */
    public function getFindPrepaidTicket2Result()
    {
      return $this->findPrepaidTicket2Result;
    }

    /**
     * @param D4WTPREPAIDRESULT2 $findPrepaidTicket2Result
     * @return \Axess\Dci4Wtp\findPrepaidTicket2Response
     */
    public function setFindPrepaidTicket2Result($findPrepaidTicket2Result)
    {
      $this->findPrepaidTicket2Result = $findPrepaidTicket2Result;
      return $this;
    }

}
