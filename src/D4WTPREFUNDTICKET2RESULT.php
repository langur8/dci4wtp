<?php

namespace Axess\Dci4Wtp;

class D4WTPREFUNDTICKET2RESULT
{

    /**
     * @var ArrayOfD4WTPTICKETTARIFINFO $ACTTICKETTARIFINFO
     */
    protected $ACTTICKETTARIFINFO = null;

    /**
     * @var float $NCURRENCYNO
     */
    protected $NCURRENCYNO = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NREFUNDTARIF
     */
    protected $NREFUNDTARIF = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    /**
     * @var string $SZCURRENCY
     */
    protected $SZCURRENCY = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKETTARIFINFO
     */
    public function getACTTICKETTARIFINFO()
    {
      return $this->ACTTICKETTARIFINFO;
    }

    /**
     * @param ArrayOfD4WTPTICKETTARIFINFO $ACTTICKETTARIFINFO
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2RESULT
     */
    public function setACTTICKETTARIFINFO($ACTTICKETTARIFINFO)
    {
      $this->ACTTICKETTARIFINFO = $ACTTICKETTARIFINFO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCURRENCYNO()
    {
      return $this->NCURRENCYNO;
    }

    /**
     * @param float $NCURRENCYNO
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2RESULT
     */
    public function setNCURRENCYNO($NCURRENCYNO)
    {
      $this->NCURRENCYNO = $NCURRENCYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2RESULT
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2RESULT
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNREFUNDTARIF()
    {
      return $this->NREFUNDTARIF;
    }

    /**
     * @param float $NREFUNDTARIF
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2RESULT
     */
    public function setNREFUNDTARIF($NREFUNDTARIF)
    {
      $this->NREFUNDTARIF = $NREFUNDTARIF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2RESULT
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCY()
    {
      return $this->SZCURRENCY;
    }

    /**
     * @param string $SZCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2RESULT
     */
    public function setSZCURRENCY($SZCURRENCY)
    {
      $this->SZCURRENCY = $SZCURRENCY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPREFUNDTICKET2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
