<?php

namespace Axess\Dci4Wtp;

class D4WTPERSONDATA3
{

    /**
     * @var ArrayOfD4WTPMEMBERTYPE $ACTMEMBERTYPE
     */
    protected $ACTMEMBERTYPE = null;

    /**
     * @var float $BCREDITCARDREF
     */
    protected $BCREDITCARDREF = null;

    /**
     * @var D4WTPMEMBER $CTMEMBER
     */
    protected $CTMEMBER = null;

    /**
     * @var D4WTPMEMBERDETAILS $CTMEMBERDETAILS
     */
    protected $CTMEMBERDETAILS = null;

    /**
     * @var D4WTPPERSRENTAL $CTPERSRENTAL
     */
    protected $CTPERSRENTAL = null;

    /**
     * @var float $NFAMPERSNO
     */
    protected $NFAMPERSNO = null;

    /**
     * @var float $NFAMPOSNO
     */
    protected $NFAMPOSNO = null;

    /**
     * @var float $NFAMPROJNO
     */
    protected $NFAMPROJNO = null;

    /**
     * @var float $NFAMSORTNO
     */
    protected $NFAMSORTNO = null;

    /**
     * @var float $NLANGUAGEID
     */
    protected $NLANGUAGEID = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var string $SZAREA
     */
    protected $SZAREA = null;

    /**
     * @var string $SZCITY
     */
    protected $SZCITY = null;

    /**
     * @var string $SZCOUNTRY
     */
    protected $SZCOUNTRY = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    /**
     * @var string $SZDATEOFBIRTH
     */
    protected $SZDATEOFBIRTH = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZGENDER
     */
    protected $SZGENDER = null;

    /**
     * @var string $SZINFORMATION1
     */
    protected $SZINFORMATION1 = null;

    /**
     * @var string $SZINFORMATION2
     */
    protected $SZINFORMATION2 = null;

    /**
     * @var string $SZINFORMATION3
     */
    protected $SZINFORMATION3 = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZMEMO
     */
    protected $SZMEMO = null;

    /**
     * @var string $SZMESSAGE
     */
    protected $SZMESSAGE = null;

    /**
     * @var string $SZMOBILE
     */
    protected $SZMOBILE = null;

    /**
     * @var string $SZPHONE
     */
    protected $SZPHONE = null;

    /**
     * @var string $SZSALUTATION
     */
    protected $SZSALUTATION = null;

    /**
     * @var string $SZSTREET
     */
    protected $SZSTREET = null;

    /**
     * @var string $SZSTREETNUMBER
     */
    protected $SZSTREETNUMBER = null;

    /**
     * @var string $SZTITLE
     */
    protected $SZTITLE = null;

    /**
     * @var string $SZZIPCODE
     */
    protected $SZZIPCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPMEMBERTYPE
     */
    public function getACTMEMBERTYPE()
    {
      return $this->ACTMEMBERTYPE;
    }

    /**
     * @param ArrayOfD4WTPMEMBERTYPE $ACTMEMBERTYPE
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setACTMEMBERTYPE($ACTMEMBERTYPE)
    {
      $this->ACTMEMBERTYPE = $ACTMEMBERTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBCREDITCARDREF()
    {
      return $this->BCREDITCARDREF;
    }

    /**
     * @param float $BCREDITCARDREF
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setBCREDITCARDREF($BCREDITCARDREF)
    {
      $this->BCREDITCARDREF = $BCREDITCARDREF;
      return $this;
    }

    /**
     * @return D4WTPMEMBER
     */
    public function getCTMEMBER()
    {
      return $this->CTMEMBER;
    }

    /**
     * @param D4WTPMEMBER $CTMEMBER
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setCTMEMBER($CTMEMBER)
    {
      $this->CTMEMBER = $CTMEMBER;
      return $this;
    }

    /**
     * @return D4WTPMEMBERDETAILS
     */
    public function getCTMEMBERDETAILS()
    {
      return $this->CTMEMBERDETAILS;
    }

    /**
     * @param D4WTPMEMBERDETAILS $CTMEMBERDETAILS
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setCTMEMBERDETAILS($CTMEMBERDETAILS)
    {
      $this->CTMEMBERDETAILS = $CTMEMBERDETAILS;
      return $this;
    }

    /**
     * @return D4WTPPERSRENTAL
     */
    public function getCTPERSRENTAL()
    {
      return $this->CTPERSRENTAL;
    }

    /**
     * @param D4WTPPERSRENTAL $CTPERSRENTAL
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setCTPERSRENTAL($CTPERSRENTAL)
    {
      $this->CTPERSRENTAL = $CTPERSRENTAL;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMPERSNO()
    {
      return $this->NFAMPERSNO;
    }

    /**
     * @param float $NFAMPERSNO
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setNFAMPERSNO($NFAMPERSNO)
    {
      $this->NFAMPERSNO = $NFAMPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMPOSNO()
    {
      return $this->NFAMPOSNO;
    }

    /**
     * @param float $NFAMPOSNO
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setNFAMPOSNO($NFAMPOSNO)
    {
      $this->NFAMPOSNO = $NFAMPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMPROJNO()
    {
      return $this->NFAMPROJNO;
    }

    /**
     * @param float $NFAMPROJNO
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setNFAMPROJNO($NFAMPROJNO)
    {
      $this->NFAMPROJNO = $NFAMPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMSORTNO()
    {
      return $this->NFAMSORTNO;
    }

    /**
     * @param float $NFAMSORTNO
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setNFAMSORTNO($NFAMSORTNO)
    {
      $this->NFAMSORTNO = $NFAMSORTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLANGUAGEID()
    {
      return $this->NLANGUAGEID;
    }

    /**
     * @param float $NLANGUAGEID
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setNLANGUAGEID($NLANGUAGEID)
    {
      $this->NLANGUAGEID = $NLANGUAGEID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAREA()
    {
      return $this->SZAREA;
    }

    /**
     * @param string $SZAREA
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZAREA($SZAREA)
    {
      $this->SZAREA = $SZAREA;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCITY()
    {
      return $this->SZCITY;
    }

    /**
     * @param string $SZCITY
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZCITY($SZCITY)
    {
      $this->SZCITY = $SZCITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRY()
    {
      return $this->SZCOUNTRY;
    }

    /**
     * @param string $SZCOUNTRY
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZCOUNTRY($SZCOUNTRY)
    {
      $this->SZCOUNTRY = $SZCOUNTRY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEOFBIRTH()
    {
      return $this->SZDATEOFBIRTH;
    }

    /**
     * @param string $SZDATEOFBIRTH
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZDATEOFBIRTH($SZDATEOFBIRTH)
    {
      $this->SZDATEOFBIRTH = $SZDATEOFBIRTH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGENDER()
    {
      return $this->SZGENDER;
    }

    /**
     * @param string $SZGENDER
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZGENDER($SZGENDER)
    {
      $this->SZGENDER = $SZGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZINFORMATION1()
    {
      return $this->SZINFORMATION1;
    }

    /**
     * @param string $SZINFORMATION1
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZINFORMATION1($SZINFORMATION1)
    {
      $this->SZINFORMATION1 = $SZINFORMATION1;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZINFORMATION2()
    {
      return $this->SZINFORMATION2;
    }

    /**
     * @param string $SZINFORMATION2
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZINFORMATION2($SZINFORMATION2)
    {
      $this->SZINFORMATION2 = $SZINFORMATION2;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZINFORMATION3()
    {
      return $this->SZINFORMATION3;
    }

    /**
     * @param string $SZINFORMATION3
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZINFORMATION3($SZINFORMATION3)
    {
      $this->SZINFORMATION3 = $SZINFORMATION3;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEMO()
    {
      return $this->SZMEMO;
    }

    /**
     * @param string $SZMEMO
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZMEMO($SZMEMO)
    {
      $this->SZMEMO = $SZMEMO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMESSAGE()
    {
      return $this->SZMESSAGE;
    }

    /**
     * @param string $SZMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZMESSAGE($SZMESSAGE)
    {
      $this->SZMESSAGE = $SZMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMOBILE()
    {
      return $this->SZMOBILE;
    }

    /**
     * @param string $SZMOBILE
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZMOBILE($SZMOBILE)
    {
      $this->SZMOBILE = $SZMOBILE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPHONE()
    {
      return $this->SZPHONE;
    }

    /**
     * @param string $SZPHONE
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZPHONE($SZPHONE)
    {
      $this->SZPHONE = $SZPHONE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSALUTATION()
    {
      return $this->SZSALUTATION;
    }

    /**
     * @param string $SZSALUTATION
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZSALUTATION($SZSALUTATION)
    {
      $this->SZSALUTATION = $SZSALUTATION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREET()
    {
      return $this->SZSTREET;
    }

    /**
     * @param string $SZSTREET
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZSTREET($SZSTREET)
    {
      $this->SZSTREET = $SZSTREET;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREETNUMBER()
    {
      return $this->SZSTREETNUMBER;
    }

    /**
     * @param string $SZSTREETNUMBER
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZSTREETNUMBER($SZSTREETNUMBER)
    {
      $this->SZSTREETNUMBER = $SZSTREETNUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTITLE()
    {
      return $this->SZTITLE;
    }

    /**
     * @param string $SZTITLE
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZTITLE($SZTITLE)
    {
      $this->SZTITLE = $SZTITLE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZZIPCODE()
    {
      return $this->SZZIPCODE;
    }

    /**
     * @param string $SZZIPCODE
     * @return \Axess\Dci4Wtp\D4WTPERSONDATA3
     */
    public function setSZZIPCODE($SZZIPCODE)
    {
      $this->SZZIPCODE = $SZZIPCODE;
      return $this;
    }

}
