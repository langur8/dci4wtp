<?php

namespace Axess\Dci4Wtp;

class D4WTPISSUETICKET12REQUEST
{

    /**
     * @var ArrayOfD4WTPPAYMENTTYPE $ACTPAYMENTTYPE
     */
    protected $ACTPAYMENTTYPE = null;

    /**
     * @var ArrayOfD4WTPPRODUCTORDER10 $ACTPRODUCTORDER
     */
    protected $ACTPRODUCTORDER = null;

    /**
     * @var D4WTPCREDITCARDDATA $CTCCDATA
     */
    protected $CTCCDATA = null;

    /**
     * @var D4WTPPAYERDATA $CTPAYERDATA
     */
    protected $CTPAYERDATA = null;

    /**
     * @var float $NCANCELAFTERINMIN
     */
    protected $NCANCELAFTERINMIN = null;

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    /**
     * @var string $SZDRIVERTYPE
     */
    protected $SZDRIVERTYPE = null;

    /**
     * @var string $SZPROMOCODEREFERENCE
     */
    protected $SZPROMOCODEREFERENCE = null;

    /**
     * @var string $SZRESERVATIONREFERENCE
     */
    protected $SZRESERVATIONREFERENCE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPAYMENTTYPE
     */
    public function getACTPAYMENTTYPE()
    {
      return $this->ACTPAYMENTTYPE;
    }

    /**
     * @param ArrayOfD4WTPPAYMENTTYPE $ACTPAYMENTTYPE
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET12REQUEST
     */
    public function setACTPAYMENTTYPE($ACTPAYMENTTYPE)
    {
      $this->ACTPAYMENTTYPE = $ACTPAYMENTTYPE;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPPRODUCTORDER10
     */
    public function getACTPRODUCTORDER()
    {
      return $this->ACTPRODUCTORDER;
    }

    /**
     * @param ArrayOfD4WTPPRODUCTORDER10 $ACTPRODUCTORDER
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET12REQUEST
     */
    public function setACTPRODUCTORDER($ACTPRODUCTORDER)
    {
      $this->ACTPRODUCTORDER = $ACTPRODUCTORDER;
      return $this;
    }

    /**
     * @return D4WTPCREDITCARDDATA
     */
    public function getCTCCDATA()
    {
      return $this->CTCCDATA;
    }

    /**
     * @param D4WTPCREDITCARDDATA $CTCCDATA
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET12REQUEST
     */
    public function setCTCCDATA($CTCCDATA)
    {
      $this->CTCCDATA = $CTCCDATA;
      return $this;
    }

    /**
     * @return D4WTPPAYERDATA
     */
    public function getCTPAYERDATA()
    {
      return $this->CTPAYERDATA;
    }

    /**
     * @param D4WTPPAYERDATA $CTPAYERDATA
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET12REQUEST
     */
    public function setCTPAYERDATA($CTPAYERDATA)
    {
      $this->CTPAYERDATA = $CTPAYERDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCANCELAFTERINMIN()
    {
      return $this->NCANCELAFTERINMIN;
    }

    /**
     * @param float $NCANCELAFTERINMIN
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET12REQUEST
     */
    public function setNCANCELAFTERINMIN($NCANCELAFTERINMIN)
    {
      $this->NCANCELAFTERINMIN = $NCANCELAFTERINMIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET12REQUEST
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET12REQUEST
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET12REQUEST
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET12REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET12REQUEST
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDRIVERTYPE()
    {
      return $this->SZDRIVERTYPE;
    }

    /**
     * @param string $SZDRIVERTYPE
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET12REQUEST
     */
    public function setSZDRIVERTYPE($SZDRIVERTYPE)
    {
      $this->SZDRIVERTYPE = $SZDRIVERTYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPROMOCODEREFERENCE()
    {
      return $this->SZPROMOCODEREFERENCE;
    }

    /**
     * @param string $SZPROMOCODEREFERENCE
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET12REQUEST
     */
    public function setSZPROMOCODEREFERENCE($SZPROMOCODEREFERENCE)
    {
      $this->SZPROMOCODEREFERENCE = $SZPROMOCODEREFERENCE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRESERVATIONREFERENCE()
    {
      return $this->SZRESERVATIONREFERENCE;
    }

    /**
     * @param string $SZRESERVATIONREFERENCE
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET12REQUEST
     */
    public function setSZRESERVATIONREFERENCE($SZRESERVATIONREFERENCE)
    {
      $this->SZRESERVATIONREFERENCE = $SZRESERVATIONREFERENCE;
      return $this;
    }

}
