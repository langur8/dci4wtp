<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETCONTENT
{

    /**
     * @var float $NFIELDTYPENO
     */
    protected $NFIELDTYPENO = null;

    /**
     * @var float $NPERMISSIONNO
     */
    protected $NPERMISSIONNO = null;

    /**
     * @var float $NSORTNO
     */
    protected $NSORTNO = null;

    /**
     * @var float $NTICKETSTATUS
     */
    protected $NTICKETSTATUS = null;

    /**
     * @var float $NVALUE
     */
    protected $NVALUE = null;

    /**
     * @var string $SZFIELDTYPENAME
     */
    protected $SZFIELDTYPENAME = null;

    /**
     * @var string $SZFIELDTYPETEXT
     */
    protected $SZFIELDTYPETEXT = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSEGMENT
     */
    protected $SZSEGMENT = null;

    /**
     * @var string $SZTICKETSTATUSNAME
     */
    protected $SZTICKETSTATUSNAME = null;

    /**
     * @var string $SZVALUE
     */
    protected $SZVALUE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNFIELDTYPENO()
    {
      return $this->NFIELDTYPENO;
    }

    /**
     * @param float $NFIELDTYPENO
     * @return \Axess\Dci4Wtp\D4WTPTICKETCONTENT
     */
    public function setNFIELDTYPENO($NFIELDTYPENO)
    {
      $this->NFIELDTYPENO = $NFIELDTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERMISSIONNO()
    {
      return $this->NPERMISSIONNO;
    }

    /**
     * @param float $NPERMISSIONNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETCONTENT
     */
    public function setNPERMISSIONNO($NPERMISSIONNO)
    {
      $this->NPERMISSIONNO = $NPERMISSIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNO()
    {
      return $this->NSORTNO;
    }

    /**
     * @param float $NSORTNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETCONTENT
     */
    public function setNSORTNO($NSORTNO)
    {
      $this->NSORTNO = $NSORTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETSTATUS()
    {
      return $this->NTICKETSTATUS;
    }

    /**
     * @param float $NTICKETSTATUS
     * @return \Axess\Dci4Wtp\D4WTPTICKETCONTENT
     */
    public function setNTICKETSTATUS($NTICKETSTATUS)
    {
      $this->NTICKETSTATUS = $NTICKETSTATUS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVALUE()
    {
      return $this->NVALUE;
    }

    /**
     * @param float $NVALUE
     * @return \Axess\Dci4Wtp\D4WTPTICKETCONTENT
     */
    public function setNVALUE($NVALUE)
    {
      $this->NVALUE = $NVALUE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIELDTYPENAME()
    {
      return $this->SZFIELDTYPENAME;
    }

    /**
     * @param string $SZFIELDTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETCONTENT
     */
    public function setSZFIELDTYPENAME($SZFIELDTYPENAME)
    {
      $this->SZFIELDTYPENAME = $SZFIELDTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIELDTYPETEXT()
    {
      return $this->SZFIELDTYPETEXT;
    }

    /**
     * @param string $SZFIELDTYPETEXT
     * @return \Axess\Dci4Wtp\D4WTPTICKETCONTENT
     */
    public function setSZFIELDTYPETEXT($SZFIELDTYPETEXT)
    {
      $this->SZFIELDTYPETEXT = $SZFIELDTYPETEXT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETCONTENT
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSEGMENT()
    {
      return $this->SZSEGMENT;
    }

    /**
     * @param string $SZSEGMENT
     * @return \Axess\Dci4Wtp\D4WTPTICKETCONTENT
     */
    public function setSZSEGMENT($SZSEGMENT)
    {
      $this->SZSEGMENT = $SZSEGMENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETSTATUSNAME()
    {
      return $this->SZTICKETSTATUSNAME;
    }

    /**
     * @param string $SZTICKETSTATUSNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETCONTENT
     */
    public function setSZTICKETSTATUSNAME($SZTICKETSTATUSNAME)
    {
      $this->SZTICKETSTATUSNAME = $SZTICKETSTATUSNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALUE()
    {
      return $this->SZVALUE;
    }

    /**
     * @param string $SZVALUE
     * @return \Axess\Dci4Wtp\D4WTPTICKETCONTENT
     */
    public function setSZVALUE($SZVALUE)
    {
      $this->SZVALUE = $SZVALUE;
      return $this;
    }

}
