<?php

namespace Axess\Dci4Wtp;

class getContingent2Response
{

    /**
     * @var D4WTPGETCONTINGENT2RESULT $getContingent2Result
     */
    protected $getContingent2Result = null;

    /**
     * @param D4WTPGETCONTINGENT2RESULT $getContingent2Result
     */
    public function __construct($getContingent2Result)
    {
      $this->getContingent2Result = $getContingent2Result;
    }

    /**
     * @return D4WTPGETCONTINGENT2RESULT
     */
    public function getGetContingent2Result()
    {
      return $this->getContingent2Result;
    }

    /**
     * @param D4WTPGETCONTINGENT2RESULT $getContingent2Result
     * @return \Axess\Dci4Wtp\getContingent2Response
     */
    public function setGetContingent2Result($getContingent2Result)
    {
      $this->getContingent2Result = $getContingent2Result;
      return $this;
    }

}
