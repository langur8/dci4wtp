<?php

namespace Axess\Dci4Wtp;

class changePersData3Response
{

    /**
     * @var D4WTPCHANGEPERSDATA3RESULT $changePersData3Result
     */
    protected $changePersData3Result = null;

    /**
     * @param D4WTPCHANGEPERSDATA3RESULT $changePersData3Result
     */
    public function __construct($changePersData3Result)
    {
      $this->changePersData3Result = $changePersData3Result;
    }

    /**
     * @return D4WTPCHANGEPERSDATA3RESULT
     */
    public function getChangePersData3Result()
    {
      return $this->changePersData3Result;
    }

    /**
     * @param D4WTPCHANGEPERSDATA3RESULT $changePersData3Result
     * @return \Axess\Dci4Wtp\changePersData3Response
     */
    public function setChangePersData3Result($changePersData3Result)
    {
      $this->changePersData3Result = $changePersData3Result;
      return $this;
    }

}
