<?php

namespace Axess\Dci4Wtp;

class setCreditCardReferenceResponse
{

    /**
     * @var D4WTPRESULT $setCreditCardReferenceResult
     */
    protected $setCreditCardReferenceResult = null;

    /**
     * @param D4WTPRESULT $setCreditCardReferenceResult
     */
    public function __construct($setCreditCardReferenceResult)
    {
      $this->setCreditCardReferenceResult = $setCreditCardReferenceResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSetCreditCardReferenceResult()
    {
      return $this->setCreditCardReferenceResult;
    }

    /**
     * @param D4WTPRESULT $setCreditCardReferenceResult
     * @return \Axess\Dci4Wtp\setCreditCardReferenceResponse
     */
    public function setSetCreditCardReferenceResult($setCreditCardReferenceResult)
    {
      $this->setCreditCardReferenceResult = $setCreditCardReferenceResult;
      return $this;
    }

}
