<?php

namespace Axess\Dci4Wtp;

class getCurrenciesResponse
{

    /**
     * @var D4WTPGETCURRENCIESRESULT $getCurrenciesResult
     */
    protected $getCurrenciesResult = null;

    /**
     * @param D4WTPGETCURRENCIESRESULT $getCurrenciesResult
     */
    public function __construct($getCurrenciesResult)
    {
      $this->getCurrenciesResult = $getCurrenciesResult;
    }

    /**
     * @return D4WTPGETCURRENCIESRESULT
     */
    public function getGetCurrenciesResult()
    {
      return $this->getCurrenciesResult;
    }

    /**
     * @param D4WTPGETCURRENCIESRESULT $getCurrenciesResult
     * @return \Axess\Dci4Wtp\getCurrenciesResponse
     */
    public function setGetCurrenciesResult($getCurrenciesResult)
    {
      $this->getCurrenciesResult = $getCurrenciesResult;
      return $this;
    }

}
