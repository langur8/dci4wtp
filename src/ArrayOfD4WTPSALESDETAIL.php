<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSALESDETAIL implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSALESDETAIL[] $D4WTPSALESDETAIL
     */
    protected $D4WTPSALESDETAIL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSALESDETAIL[]
     */
    public function getD4WTPSALESDETAIL()
    {
      return $this->D4WTPSALESDETAIL;
    }

    /**
     * @param D4WTPSALESDETAIL[] $D4WTPSALESDETAIL
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSALESDETAIL
     */
    public function setD4WTPSALESDETAIL(array $D4WTPSALESDETAIL = null)
    {
      $this->D4WTPSALESDETAIL = $D4WTPSALESDETAIL;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSALESDETAIL[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSALESDETAIL
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSALESDETAIL[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSALESDETAIL $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSALESDETAIL[] = $value;
      } else {
        $this->D4WTPSALESDETAIL[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSALESDETAIL[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSALESDETAIL Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSALESDETAIL);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSALESDETAIL);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSALESDETAIL);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSALESDETAIL);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSALESDETAIL Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSALESDETAIL);
    }

}
