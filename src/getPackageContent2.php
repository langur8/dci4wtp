<?php

namespace Axess\Dci4Wtp;

class getPackageContent2
{

    /**
     * @var D4WTPPACKAGECONTENTREQUEST $i_ctPackageContentReq
     */
    protected $i_ctPackageContentReq = null;

    /**
     * @param D4WTPPACKAGECONTENTREQUEST $i_ctPackageContentReq
     */
    public function __construct($i_ctPackageContentReq)
    {
      $this->i_ctPackageContentReq = $i_ctPackageContentReq;
    }

    /**
     * @return D4WTPPACKAGECONTENTREQUEST
     */
    public function getI_ctPackageContentReq()
    {
      return $this->i_ctPackageContentReq;
    }

    /**
     * @param D4WTPPACKAGECONTENTREQUEST $i_ctPackageContentReq
     * @return \Axess\Dci4Wtp\getPackageContent2
     */
    public function setI_ctPackageContentReq($i_ctPackageContentReq)
    {
      $this->i_ctPackageContentReq = $i_ctPackageContentReq;
      return $this;
    }

}
