<?php

namespace Axess\Dci4Wtp;

class getEMoneySteps
{

    /**
     * @var D4AXCOUNTSTEPREQ $i_CTD4AxCountStepReq
     */
    protected $i_CTD4AxCountStepReq = null;

    /**
     * @param D4AXCOUNTSTEPREQ $i_CTD4AxCountStepReq
     */
    public function __construct($i_CTD4AxCountStepReq)
    {
      $this->i_CTD4AxCountStepReq = $i_CTD4AxCountStepReq;
    }

    /**
     * @return D4AXCOUNTSTEPREQ
     */
    public function getI_CTD4AxCountStepReq()
    {
      return $this->i_CTD4AxCountStepReq;
    }

    /**
     * @param D4AXCOUNTSTEPREQ $i_CTD4AxCountStepReq
     * @return \Axess\Dci4Wtp\getEMoneySteps
     */
    public function setI_CTD4AxCountStepReq($i_CTD4AxCountStepReq)
    {
      $this->i_CTD4AxCountStepReq = $i_CTD4AxCountStepReq;
      return $this;
    }

}
