<?php

namespace Axess\Dci4Wtp;

class getRMProfilesResponse
{

    /**
     * @var D4WTPGETRMPROFILESRESPONSE $getRMProfilesResult
     */
    protected $getRMProfilesResult = null;

    /**
     * @param D4WTPGETRMPROFILESRESPONSE $getRMProfilesResult
     */
    public function __construct($getRMProfilesResult)
    {
      $this->getRMProfilesResult = $getRMProfilesResult;
    }

    /**
     * @return D4WTPGETRMPROFILESRESPONSE
     */
    public function getGetRMProfilesResult()
    {
      return $this->getRMProfilesResult;
    }

    /**
     * @param D4WTPGETRMPROFILESRESPONSE $getRMProfilesResult
     * @return \Axess\Dci4Wtp\getRMProfilesResponse
     */
    public function setGetRMProfilesResult($getRMProfilesResult)
    {
      $this->getRMProfilesResult = $getRMProfilesResult;
      return $this;
    }

}
