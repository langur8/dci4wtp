<?php

namespace Axess\Dci4Wtp;

class getWTPConfigurationResponse
{

    /**
     * @var D4WTPCONFIGRESULT $getWTPConfigurationResult
     */
    protected $getWTPConfigurationResult = null;

    /**
     * @param D4WTPCONFIGRESULT $getWTPConfigurationResult
     */
    public function __construct($getWTPConfigurationResult)
    {
      $this->getWTPConfigurationResult = $getWTPConfigurationResult;
    }

    /**
     * @return D4WTPCONFIGRESULT
     */
    public function getGetWTPConfigurationResult()
    {
      return $this->getWTPConfigurationResult;
    }

    /**
     * @param D4WTPCONFIGRESULT $getWTPConfigurationResult
     * @return \Axess\Dci4Wtp\getWTPConfigurationResponse
     */
    public function setGetWTPConfigurationResult($getWTPConfigurationResult)
    {
      $this->getWTPConfigurationResult = $getWTPConfigurationResult;
      return $this;
    }

}
