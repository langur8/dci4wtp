<?php

namespace Axess\Dci4Wtp;

class D4WTPPRODUCTLIST
{

    /**
     * @var ArrayOfD4WTPPERSONTYPENO $ACTPERSONTYPENO
     */
    protected $ACTPERSONTYPENO = null;

    /**
     * @var ArrayOfD4WTPPOOLNO $ACTPOOLNO
     */
    protected $ACTPOOLNO = null;

    /**
     * @var ArrayOfD4WTPTICKETTYPENO $ACTTICKETTYPENO
     */
    protected $ACTTICKETTYPENO = null;

    /**
     * @var string $SZVALIDFROMEND
     */
    protected $SZVALIDFROMEND = null;

    /**
     * @var string $SZVALIDFROMSTART
     */
    protected $SZVALIDFROMSTART = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPERSONTYPENO
     */
    public function getACTPERSONTYPENO()
    {
      return $this->ACTPERSONTYPENO;
    }

    /**
     * @param ArrayOfD4WTPPERSONTYPENO $ACTPERSONTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTLIST
     */
    public function setACTPERSONTYPENO($ACTPERSONTYPENO)
    {
      $this->ACTPERSONTYPENO = $ACTPERSONTYPENO;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPPOOLNO
     */
    public function getACTPOOLNO()
    {
      return $this->ACTPOOLNO;
    }

    /**
     * @param ArrayOfD4WTPPOOLNO $ACTPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTLIST
     */
    public function setACTPOOLNO($ACTPOOLNO)
    {
      $this->ACTPOOLNO = $ACTPOOLNO;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPTICKETTYPENO
     */
    public function getACTTICKETTYPENO()
    {
      return $this->ACTTICKETTYPENO;
    }

    /**
     * @param ArrayOfD4WTPTICKETTYPENO $ACTTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTLIST
     */
    public function setACTTICKETTYPENO($ACTTICKETTYPENO)
    {
      $this->ACTTICKETTYPENO = $ACTTICKETTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROMEND()
    {
      return $this->SZVALIDFROMEND;
    }

    /**
     * @param string $SZVALIDFROMEND
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTLIST
     */
    public function setSZVALIDFROMEND($SZVALIDFROMEND)
    {
      $this->SZVALIDFROMEND = $SZVALIDFROMEND;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROMSTART()
    {
      return $this->SZVALIDFROMSTART;
    }

    /**
     * @param string $SZVALIDFROMSTART
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTLIST
     */
    public function setSZVALIDFROMSTART($SZVALIDFROMSTART)
    {
      $this->SZVALIDFROMSTART = $SZVALIDFROMSTART;
      return $this;
    }

}
