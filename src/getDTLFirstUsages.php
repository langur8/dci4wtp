<?php

namespace Axess\Dci4Wtp;

class getDTLFirstUsages
{

    /**
     * @var D4WTPGETDTLUSAGESREQUEST $i_ctUsagesReq
     */
    protected $i_ctUsagesReq = null;

    /**
     * @param D4WTPGETDTLUSAGESREQUEST $i_ctUsagesReq
     */
    public function __construct($i_ctUsagesReq)
    {
      $this->i_ctUsagesReq = $i_ctUsagesReq;
    }

    /**
     * @return D4WTPGETDTLUSAGESREQUEST
     */
    public function getI_ctUsagesReq()
    {
      return $this->i_ctUsagesReq;
    }

    /**
     * @param D4WTPGETDTLUSAGESREQUEST $i_ctUsagesReq
     * @return \Axess\Dci4Wtp\getDTLFirstUsages
     */
    public function setI_ctUsagesReq($i_ctUsagesReq)
    {
      $this->i_ctUsagesReq = $i_ctUsagesReq;
      return $this;
    }

}
