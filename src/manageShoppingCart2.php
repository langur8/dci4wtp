<?php

namespace Axess\Dci4Wtp;

class manageShoppingCart2
{

    /**
     * @var D4WTPMANAGESHOPCARTREQ2 $i_ctManageShoppingCartReq
     */
    protected $i_ctManageShoppingCartReq = null;

    /**
     * @param D4WTPMANAGESHOPCARTREQ2 $i_ctManageShoppingCartReq
     */
    public function __construct($i_ctManageShoppingCartReq)
    {
      $this->i_ctManageShoppingCartReq = $i_ctManageShoppingCartReq;
    }

    /**
     * @return D4WTPMANAGESHOPCARTREQ2
     */
    public function getI_ctManageShoppingCartReq()
    {
      return $this->i_ctManageShoppingCartReq;
    }

    /**
     * @param D4WTPMANAGESHOPCARTREQ2 $i_ctManageShoppingCartReq
     * @return \Axess\Dci4Wtp\manageShoppingCart2
     */
    public function setI_ctManageShoppingCartReq($i_ctManageShoppingCartReq)
    {
      $this->i_ctManageShoppingCartReq = $i_ctManageShoppingCartReq;
      return $this;
    }

}
