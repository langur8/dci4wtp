<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPB2CACCOUNT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPB2CACCOUNT[] $D4WTPB2CACCOUNT
     */
    protected $D4WTPB2CACCOUNT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPB2CACCOUNT[]
     */
    public function getD4WTPB2CACCOUNT()
    {
      return $this->D4WTPB2CACCOUNT;
    }

    /**
     * @param D4WTPB2CACCOUNT[] $D4WTPB2CACCOUNT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPB2CACCOUNT
     */
    public function setD4WTPB2CACCOUNT(array $D4WTPB2CACCOUNT = null)
    {
      $this->D4WTPB2CACCOUNT = $D4WTPB2CACCOUNT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPB2CACCOUNT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPB2CACCOUNT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPB2CACCOUNT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPB2CACCOUNT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPB2CACCOUNT[] = $value;
      } else {
        $this->D4WTPB2CACCOUNT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPB2CACCOUNT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPB2CACCOUNT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPB2CACCOUNT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPB2CACCOUNT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPB2CACCOUNT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPB2CACCOUNT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPB2CACCOUNT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPB2CACCOUNT);
    }

}
