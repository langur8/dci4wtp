<?php

namespace Axess\Dci4Wtp;

class D4WTPCREATEWTPDATAREQ
{

    /**
     * @var ArrayOfD4WTPWTPDATA $ACTWTPDATA
     */
    protected $ACTWTPDATA = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPWTPDATA
     */
    public function getACTWTPDATA()
    {
      return $this->ACTWTPDATA;
    }

    /**
     * @param ArrayOfD4WTPWTPDATA $ACTWTPDATA
     * @return \Axess\Dci4Wtp\D4WTPCREATEWTPDATAREQ
     */
    public function setACTWTPDATA($ACTWTPDATA)
    {
      $this->ACTWTPDATA = $ACTWTPDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCREATEWTPDATAREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
