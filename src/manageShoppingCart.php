<?php

namespace Axess\Dci4Wtp;

class manageShoppingCart
{

    /**
     * @var D4WTPMANAGESHOPCARTREQUEST $i_ctManageShoppingCartReq
     */
    protected $i_ctManageShoppingCartReq = null;

    /**
     * @param D4WTPMANAGESHOPCARTREQUEST $i_ctManageShoppingCartReq
     */
    public function __construct($i_ctManageShoppingCartReq)
    {
      $this->i_ctManageShoppingCartReq = $i_ctManageShoppingCartReq;
    }

    /**
     * @return D4WTPMANAGESHOPCARTREQUEST
     */
    public function getI_ctManageShoppingCartReq()
    {
      return $this->i_ctManageShoppingCartReq;
    }

    /**
     * @param D4WTPMANAGESHOPCARTREQUEST $i_ctManageShoppingCartReq
     * @return \Axess\Dci4Wtp\manageShoppingCart
     */
    public function setI_ctManageShoppingCartReq($i_ctManageShoppingCartReq)
    {
      $this->i_ctManageShoppingCartReq = $i_ctManageShoppingCartReq;
      return $this;
    }

}
