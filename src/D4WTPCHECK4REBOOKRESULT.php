<?php

namespace Axess\Dci4Wtp;

class D4WTPCHECK4REBOOKRESULT
{

    /**
     * @var ArrayOfD4WTPCHECKED4REBOOK $ACTCHECKED4REBOOK
     */
    protected $ACTCHECKED4REBOOK = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCHECKED4REBOOK
     */
    public function getACTCHECKED4REBOOK()
    {
      return $this->ACTCHECKED4REBOOK;
    }

    /**
     * @param ArrayOfD4WTPCHECKED4REBOOK $ACTCHECKED4REBOOK
     * @return \Axess\Dci4Wtp\D4WTPCHECK4REBOOKRESULT
     */
    public function setACTCHECKED4REBOOK($ACTCHECKED4REBOOK)
    {
      $this->ACTCHECKED4REBOOK = $ACTCHECKED4REBOOK;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCHECK4REBOOKRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCHECK4REBOOKRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
