<?php

namespace Axess\Dci4Wtp;

class getCash4CashierShiftResponse
{

    /**
     * @var D4WTPGETCASH4CASHIERSHIFTRES $getCash4CashierShiftResult
     */
    protected $getCash4CashierShiftResult = null;

    /**
     * @param D4WTPGETCASH4CASHIERSHIFTRES $getCash4CashierShiftResult
     */
    public function __construct($getCash4CashierShiftResult)
    {
      $this->getCash4CashierShiftResult = $getCash4CashierShiftResult;
    }

    /**
     * @return D4WTPGETCASH4CASHIERSHIFTRES
     */
    public function getGetCash4CashierShiftResult()
    {
      return $this->getCash4CashierShiftResult;
    }

    /**
     * @param D4WTPGETCASH4CASHIERSHIFTRES $getCash4CashierShiftResult
     * @return \Axess\Dci4Wtp\getCash4CashierShiftResponse
     */
    public function setGetCash4CashierShiftResult($getCash4CashierShiftResult)
    {
      $this->getCash4CashierShiftResult = $getCash4CashierShiftResult;
      return $this;
    }

}
