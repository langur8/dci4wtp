<?php

namespace Axess\Dci4Wtp;

class issueTicket8Response
{

    /**
     * @var D4WTPISSUETICKET5RESULT $issueTicket8Result
     */
    protected $issueTicket8Result = null;

    /**
     * @param D4WTPISSUETICKET5RESULT $issueTicket8Result
     */
    public function __construct($issueTicket8Result)
    {
      $this->issueTicket8Result = $issueTicket8Result;
    }

    /**
     * @return D4WTPISSUETICKET5RESULT
     */
    public function getIssueTicket8Result()
    {
      return $this->issueTicket8Result;
    }

    /**
     * @param D4WTPISSUETICKET5RESULT $issueTicket8Result
     * @return \Axess\Dci4Wtp\issueTicket8Response
     */
    public function setIssueTicket8Result($issueTicket8Result)
    {
      $this->issueTicket8Result = $issueTicket8Result;
      return $this;
    }

}
