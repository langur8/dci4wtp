<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPROMPTELEMENTS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPROMPTELEMENTS[] $D4WTPPROMPTELEMENTS
     */
    protected $D4WTPPROMPTELEMENTS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPROMPTELEMENTS[]
     */
    public function getD4WTPPROMPTELEMENTS()
    {
      return $this->D4WTPPROMPTELEMENTS;
    }

    /**
     * @param D4WTPPROMPTELEMENTS[] $D4WTPPROMPTELEMENTS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPROMPTELEMENTS
     */
    public function setD4WTPPROMPTELEMENTS(array $D4WTPPROMPTELEMENTS = null)
    {
      $this->D4WTPPROMPTELEMENTS = $D4WTPPROMPTELEMENTS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPROMPTELEMENTS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPROMPTELEMENTS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPROMPTELEMENTS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPROMPTELEMENTS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPROMPTELEMENTS[] = $value;
      } else {
        $this->D4WTPPROMPTELEMENTS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPROMPTELEMENTS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPROMPTELEMENTS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPROMPTELEMENTS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPROMPTELEMENTS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPROMPTELEMENTS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPROMPTELEMENTS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPROMPTELEMENTS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPROMPTELEMENTS);
    }

}
