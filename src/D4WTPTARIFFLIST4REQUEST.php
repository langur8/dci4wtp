<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST4REQUEST
{

    /**
     * @var float $BINCLCONTINGENTCNT
     */
    protected $BINCLCONTINGENTCNT = null;

    /**
     * @var float $BINCLUDINGERRORS
     */
    protected $BINCLUDINGERRORS = null;

    /**
     * @var D4WTPPRODUCTLIST $CTPRODUCTLIST
     */
    protected $CTPRODUCTLIST = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBINCLCONTINGENTCNT()
    {
      return $this->BINCLCONTINGENTCNT;
    }

    /**
     * @param float $BINCLCONTINGENTCNT
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST4REQUEST
     */
    public function setBINCLCONTINGENTCNT($BINCLCONTINGENTCNT)
    {
      $this->BINCLCONTINGENTCNT = $BINCLCONTINGENTCNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getBINCLUDINGERRORS()
    {
      return $this->BINCLUDINGERRORS;
    }

    /**
     * @param float $BINCLUDINGERRORS
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST4REQUEST
     */
    public function setBINCLUDINGERRORS($BINCLUDINGERRORS)
    {
      $this->BINCLUDINGERRORS = $BINCLUDINGERRORS;
      return $this;
    }

    /**
     * @return D4WTPPRODUCTLIST
     */
    public function getCTPRODUCTLIST()
    {
      return $this->CTPRODUCTLIST;
    }

    /**
     * @param D4WTPPRODUCTLIST $CTPRODUCTLIST
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST4REQUEST
     */
    public function setCTPRODUCTLIST($CTPRODUCTLIST)
    {
      $this->CTPRODUCTLIST = $CTPRODUCTLIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST4REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST4REQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST4REQUEST
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

}
