<?php

namespace Axess\Dci4Wtp;

class ArrayOfDTL4FAMCODINGMEMBER implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DTL4FAMCODINGMEMBER[] $DTL4FAMCODINGMEMBER
     */
    protected $DTL4FAMCODINGMEMBER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DTL4FAMCODINGMEMBER[]
     */
    public function getDTL4FAMCODINGMEMBER()
    {
      return $this->DTL4FAMCODINGMEMBER;
    }

    /**
     * @param DTL4FAMCODINGMEMBER[] $DTL4FAMCODINGMEMBER
     * @return \Axess\Dci4Wtp\ArrayOfDTL4FAMCODINGMEMBER
     */
    public function setDTL4FAMCODINGMEMBER(array $DTL4FAMCODINGMEMBER = null)
    {
      $this->DTL4FAMCODINGMEMBER = $DTL4FAMCODINGMEMBER;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DTL4FAMCODINGMEMBER[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DTL4FAMCODINGMEMBER
     */
    public function offsetGet($offset)
    {
      return $this->DTL4FAMCODINGMEMBER[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DTL4FAMCODINGMEMBER $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DTL4FAMCODINGMEMBER[] = $value;
      } else {
        $this->DTL4FAMCODINGMEMBER[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DTL4FAMCODINGMEMBER[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DTL4FAMCODINGMEMBER Return the current element
     */
    public function current()
    {
      return current($this->DTL4FAMCODINGMEMBER);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DTL4FAMCODINGMEMBER);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DTL4FAMCODINGMEMBER);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DTL4FAMCODINGMEMBER);
    }

    /**
     * Countable implementation
     *
     * @return DTL4FAMCODINGMEMBER Return count of elements
     */
    public function count()
    {
      return count($this->DTL4FAMCODINGMEMBER);
    }

}
