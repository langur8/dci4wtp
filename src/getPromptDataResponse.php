<?php

namespace Axess\Dci4Wtp;

class getPromptDataResponse
{

    /**
     * @var D4WTPGETPROMPTDATARESULT $getPromptDataResult
     */
    protected $getPromptDataResult = null;

    /**
     * @param D4WTPGETPROMPTDATARESULT $getPromptDataResult
     */
    public function __construct($getPromptDataResult)
    {
      $this->getPromptDataResult = $getPromptDataResult;
    }

    /**
     * @return D4WTPGETPROMPTDATARESULT
     */
    public function getGetPromptDataResult()
    {
      return $this->getPromptDataResult;
    }

    /**
     * @param D4WTPGETPROMPTDATARESULT $getPromptDataResult
     * @return \Axess\Dci4Wtp\getPromptDataResponse
     */
    public function setGetPromptDataResult($getPromptDataResult)
    {
      $this->getPromptDataResult = $getPromptDataResult;
      return $this;
    }

}
