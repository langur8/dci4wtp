<?php

namespace Axess\Dci4Wtp;

class BLANKTYPESRES
{

    /**
     * @var ArrayOfD4WTPBLANKTYPEEN $ACTBLANKTYPE
     */
    protected $ACTBLANKTYPE = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPBLANKTYPEEN
     */
    public function getACTBLANKTYPE()
    {
      return $this->ACTBLANKTYPE;
    }

    /**
     * @param ArrayOfD4WTPBLANKTYPEEN $ACTBLANKTYPE
     * @return \Axess\Dci4Wtp\BLANKTYPESRES
     */
    public function setACTBLANKTYPE($ACTBLANKTYPE)
    {
      $this->ACTBLANKTYPE = $ACTBLANKTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\BLANKTYPESRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\BLANKTYPESRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
