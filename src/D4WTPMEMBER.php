<?php

namespace Axess\Dci4Wtp;

class D4WTPMEMBER
{

    /**
     * @var float $BMEMBERSHIPACTIVE
     */
    protected $BMEMBERSHIPACTIVE = null;

    /**
     * @var string $SZCREATIONDATE
     */
    protected $SZCREATIONDATE = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZPASSWORD
     */
    protected $SZPASSWORD = null;

    /**
     * @var string $SZTOCCONFIRMED
     */
    protected $SZTOCCONFIRMED = null;

    /**
     * @var string $SZUPDATE
     */
    protected $SZUPDATE = null;

    /**
     * @var string $SZUSER
     */
    protected $SZUSER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBMEMBERSHIPACTIVE()
    {
      return $this->BMEMBERSHIPACTIVE;
    }

    /**
     * @param float $BMEMBERSHIPACTIVE
     * @return \Axess\Dci4Wtp\D4WTPMEMBER
     */
    public function setBMEMBERSHIPACTIVE($BMEMBERSHIPACTIVE)
    {
      $this->BMEMBERSHIPACTIVE = $BMEMBERSHIPACTIVE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCREATIONDATE()
    {
      return $this->SZCREATIONDATE;
    }

    /**
     * @param string $SZCREATIONDATE
     * @return \Axess\Dci4Wtp\D4WTPMEMBER
     */
    public function setSZCREATIONDATE($SZCREATIONDATE)
    {
      $this->SZCREATIONDATE = $SZCREATIONDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPMEMBER
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPASSWORD()
    {
      return $this->SZPASSWORD;
    }

    /**
     * @param string $SZPASSWORD
     * @return \Axess\Dci4Wtp\D4WTPMEMBER
     */
    public function setSZPASSWORD($SZPASSWORD)
    {
      $this->SZPASSWORD = $SZPASSWORD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTOCCONFIRMED()
    {
      return $this->SZTOCCONFIRMED;
    }

    /**
     * @param string $SZTOCCONFIRMED
     * @return \Axess\Dci4Wtp\D4WTPMEMBER
     */
    public function setSZTOCCONFIRMED($SZTOCCONFIRMED)
    {
      $this->SZTOCCONFIRMED = $SZTOCCONFIRMED;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUPDATE()
    {
      return $this->SZUPDATE;
    }

    /**
     * @param string $SZUPDATE
     * @return \Axess\Dci4Wtp\D4WTPMEMBER
     */
    public function setSZUPDATE($SZUPDATE)
    {
      $this->SZUPDATE = $SZUPDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUSER()
    {
      return $this->SZUSER;
    }

    /**
     * @param string $SZUSER
     * @return \Axess\Dci4Wtp\D4WTPMEMBER
     */
    public function setSZUSER($SZUSER)
    {
      $this->SZUSER = $SZUSER;
      return $this;
    }

}
