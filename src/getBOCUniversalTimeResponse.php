<?php

namespace Axess\Dci4Wtp;

class getBOCUniversalTimeResponse
{

    /**
     * @var string $getBOCUniversalTimeResult
     */
    protected $getBOCUniversalTimeResult = null;

    /**
     * @param string $getBOCUniversalTimeResult
     */
    public function __construct($getBOCUniversalTimeResult)
    {
      $this->getBOCUniversalTimeResult = $getBOCUniversalTimeResult;
    }

    /**
     * @return string
     */
    public function getGetBOCUniversalTimeResult()
    {
      return $this->getBOCUniversalTimeResult;
    }

    /**
     * @param string $getBOCUniversalTimeResult
     * @return \Axess\Dci4Wtp\getBOCUniversalTimeResponse
     */
    public function setGetBOCUniversalTimeResult($getBOCUniversalTimeResult)
    {
      $this->getBOCUniversalTimeResult = $getBOCUniversalTimeResult;
      return $this;
    }

}
