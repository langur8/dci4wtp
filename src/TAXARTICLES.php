<?php

namespace Axess\Dci4Wtp;

class TAXARTICLES
{

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NARTIKELTYPENO
     */
    protected $NARTIKELTYPENO = null;

    /**
     * @var float $NTAXNO
     */
    protected $NTAXNO = null;

    /**
     * @var string $SZARTICLETYPENAME
     */
    protected $SZARTICLETYPENAME = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\TAXARTICLES
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTIKELTYPENO()
    {
      return $this->NARTIKELTYPENO;
    }

    /**
     * @param float $NARTIKELTYPENO
     * @return \Axess\Dci4Wtp\TAXARTICLES
     */
    public function setNARTIKELTYPENO($NARTIKELTYPENO)
    {
      $this->NARTIKELTYPENO = $NARTIKELTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTAXNO()
    {
      return $this->NTAXNO;
    }

    /**
     * @param float $NTAXNO
     * @return \Axess\Dci4Wtp\TAXARTICLES
     */
    public function setNTAXNO($NTAXNO)
    {
      $this->NTAXNO = $NTAXNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZARTICLETYPENAME()
    {
      return $this->SZARTICLETYPENAME;
    }

    /**
     * @param string $SZARTICLETYPENAME
     * @return \Axess\Dci4Wtp\TAXARTICLES
     */
    public function setSZARTICLETYPENAME($SZARTICLETYPENAME)
    {
      $this->SZARTICLETYPENAME = $SZARTICLETYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\TAXARTICLES
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
