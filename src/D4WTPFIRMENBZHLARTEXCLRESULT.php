<?php

namespace Axess\Dci4Wtp;

class D4WTPFIRMENBZHLARTEXCLRESULT
{

    /**
     * @var ArrayOfD4WTPFIRMENBEZAHLARTENEXCL $ACTFIRMENBEZAHLARTENEXCL
     */
    protected $ACTFIRMENBEZAHLARTENEXCL = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPFIRMENBEZAHLARTENEXCL
     */
    public function getACTFIRMENBEZAHLARTENEXCL()
    {
      return $this->ACTFIRMENBEZAHLARTENEXCL;
    }

    /**
     * @param ArrayOfD4WTPFIRMENBEZAHLARTENEXCL $ACTFIRMENBEZAHLARTENEXCL
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBZHLARTEXCLRESULT
     */
    public function setACTFIRMENBEZAHLARTENEXCL($ACTFIRMENBEZAHLARTENEXCL)
    {
      $this->ACTFIRMENBEZAHLARTENEXCL = $ACTFIRMENBEZAHLARTENEXCL;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBZHLARTEXCLRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBZHLARTEXCLRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
