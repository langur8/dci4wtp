<?php

namespace Axess\Dci4Wtp;

class authenticateMember
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var string $i_szUser
     */
    protected $i_szUser = null;

    /**
     * @var string $i_szPassword
     */
    protected $i_szPassword = null;

    /**
     * @var float $i_bOnlyCheckUserName
     */
    protected $i_bOnlyCheckUserName = null;

    /**
     * @var ArrayOfD4WTPMEMBERTYPE $i_actMemberType
     */
    protected $i_actMemberType = null;

    /**
     * @param float $i_nSessionID
     * @param string $i_szUser
     * @param string $i_szPassword
     * @param float $i_bOnlyCheckUserName
     * @param ArrayOfD4WTPMEMBERTYPE $i_actMemberType
     */
    public function __construct($i_nSessionID, $i_szUser, $i_szPassword, $i_bOnlyCheckUserName, $i_actMemberType)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_szUser = $i_szUser;
      $this->i_szPassword = $i_szPassword;
      $this->i_bOnlyCheckUserName = $i_bOnlyCheckUserName;
      $this->i_actMemberType = $i_actMemberType;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\authenticateMember
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szUser()
    {
      return $this->i_szUser;
    }

    /**
     * @param string $i_szUser
     * @return \Axess\Dci4Wtp\authenticateMember
     */
    public function setI_szUser($i_szUser)
    {
      $this->i_szUser = $i_szUser;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szPassword()
    {
      return $this->i_szPassword;
    }

    /**
     * @param string $i_szPassword
     * @return \Axess\Dci4Wtp\authenticateMember
     */
    public function setI_szPassword($i_szPassword)
    {
      $this->i_szPassword = $i_szPassword;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_bOnlyCheckUserName()
    {
      return $this->i_bOnlyCheckUserName;
    }

    /**
     * @param float $i_bOnlyCheckUserName
     * @return \Axess\Dci4Wtp\authenticateMember
     */
    public function setI_bOnlyCheckUserName($i_bOnlyCheckUserName)
    {
      $this->i_bOnlyCheckUserName = $i_bOnlyCheckUserName;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPMEMBERTYPE
     */
    public function getI_actMemberType()
    {
      return $this->i_actMemberType;
    }

    /**
     * @param ArrayOfD4WTPMEMBERTYPE $i_actMemberType
     * @return \Axess\Dci4Wtp\authenticateMember
     */
    public function setI_actMemberType($i_actMemberType)
    {
      $this->i_actMemberType = $i_actMemberType;
      return $this;
    }

}
