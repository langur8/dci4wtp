<?php

namespace Axess\Dci4Wtp;

class bIsLpCheckInOut
{

    /**
     * @var float $i_nsessionid
     */
    protected $i_nsessionid = null;

    /**
     * @var string $i_szflag
     */
    protected $i_szflag = null;

    /**
     * @var string $i_szlicenseplate
     */
    protected $i_szlicenseplate = null;

    /**
     * @var float $i_nprojnr
     */
    protected $i_nprojnr = null;

    /**
     * @var float $i_nkassanr
     */
    protected $i_nkassanr = null;

    /**
     * @var float $i_nseriennr
     */
    protected $i_nseriennr = null;

    /**
     * @param float $i_nsessionid
     * @param string $i_szflag
     * @param string $i_szlicenseplate
     * @param float $i_nprojnr
     * @param float $i_nkassanr
     * @param float $i_nseriennr
     */
    public function __construct($i_nsessionid, $i_szflag, $i_szlicenseplate, $i_nprojnr, $i_nkassanr, $i_nseriennr)
    {
      $this->i_nsessionid = $i_nsessionid;
      $this->i_szflag = $i_szflag;
      $this->i_szlicenseplate = $i_szlicenseplate;
      $this->i_nprojnr = $i_nprojnr;
      $this->i_nkassanr = $i_nkassanr;
      $this->i_nseriennr = $i_nseriennr;
    }

    /**
     * @return float
     */
    public function getI_nsessionid()
    {
      return $this->i_nsessionid;
    }

    /**
     * @param float $i_nsessionid
     * @return \Axess\Dci4Wtp\bIsLpCheckInOut
     */
    public function setI_nsessionid($i_nsessionid)
    {
      $this->i_nsessionid = $i_nsessionid;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szflag()
    {
      return $this->i_szflag;
    }

    /**
     * @param string $i_szflag
     * @return \Axess\Dci4Wtp\bIsLpCheckInOut
     */
    public function setI_szflag($i_szflag)
    {
      $this->i_szflag = $i_szflag;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szlicenseplate()
    {
      return $this->i_szlicenseplate;
    }

    /**
     * @param string $i_szlicenseplate
     * @return \Axess\Dci4Wtp\bIsLpCheckInOut
     */
    public function setI_szlicenseplate($i_szlicenseplate)
    {
      $this->i_szlicenseplate = $i_szlicenseplate;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nprojnr()
    {
      return $this->i_nprojnr;
    }

    /**
     * @param float $i_nprojnr
     * @return \Axess\Dci4Wtp\bIsLpCheckInOut
     */
    public function setI_nprojnr($i_nprojnr)
    {
      $this->i_nprojnr = $i_nprojnr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nkassanr()
    {
      return $this->i_nkassanr;
    }

    /**
     * @param float $i_nkassanr
     * @return \Axess\Dci4Wtp\bIsLpCheckInOut
     */
    public function setI_nkassanr($i_nkassanr)
    {
      $this->i_nkassanr = $i_nkassanr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nseriennr()
    {
      return $this->i_nseriennr;
    }

    /**
     * @param float $i_nseriennr
     * @return \Axess\Dci4Wtp\bIsLpCheckInOut
     */
    public function setI_nseriennr($i_nseriennr)
    {
      $this->i_nseriennr = $i_nseriennr;
      return $this;
    }

}
