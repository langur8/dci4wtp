<?php

namespace Axess\Dci4Wtp;

class D4WTPADDDTL4FAMILYMEMBERREQ
{

    /**
     * @var ArrayOfD4WTPMEDIA $ACTMEDIA
     */
    protected $ACTMEDIA = null;

    /**
     * @var float $NMASTERJOURNALNR
     */
    protected $NMASTERJOURNALNR = null;

    /**
     * @var float $NMASTERPOSNR
     */
    protected $NMASTERPOSNR = null;

    /**
     * @var float $NMASTERPROJNR
     */
    protected $NMASTERPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPMEDIA
     */
    public function getACTMEDIA()
    {
      return $this->ACTMEDIA;
    }

    /**
     * @param ArrayOfD4WTPMEDIA $ACTMEDIA
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYMEMBERREQ
     */
    public function setACTMEDIA($ACTMEDIA)
    {
      $this->ACTMEDIA = $ACTMEDIA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERJOURNALNR()
    {
      return $this->NMASTERJOURNALNR;
    }

    /**
     * @param float $NMASTERJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYMEMBERREQ
     */
    public function setNMASTERJOURNALNR($NMASTERJOURNALNR)
    {
      $this->NMASTERJOURNALNR = $NMASTERJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERPOSNR()
    {
      return $this->NMASTERPOSNR;
    }

    /**
     * @param float $NMASTERPOSNR
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYMEMBERREQ
     */
    public function setNMASTERPOSNR($NMASTERPOSNR)
    {
      $this->NMASTERPOSNR = $NMASTERPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERPROJNR()
    {
      return $this->NMASTERPROJNR;
    }

    /**
     * @param float $NMASTERPROJNR
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYMEMBERREQ
     */
    public function setNMASTERPROJNR($NMASTERPROJNR)
    {
      $this->NMASTERPROJNR = $NMASTERPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYMEMBERREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
