<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPEMONEYACCOUNT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPEMONEYACCOUNT[] $D4WTPEMONEYACCOUNT
     */
    protected $D4WTPEMONEYACCOUNT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPEMONEYACCOUNT[]
     */
    public function getD4WTPEMONEYACCOUNT()
    {
      return $this->D4WTPEMONEYACCOUNT;
    }

    /**
     * @param D4WTPEMONEYACCOUNT[] $D4WTPEMONEYACCOUNT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPEMONEYACCOUNT
     */
    public function setD4WTPEMONEYACCOUNT(array $D4WTPEMONEYACCOUNT = null)
    {
      $this->D4WTPEMONEYACCOUNT = $D4WTPEMONEYACCOUNT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPEMONEYACCOUNT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPEMONEYACCOUNT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPEMONEYACCOUNT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPEMONEYACCOUNT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPEMONEYACCOUNT[] = $value;
      } else {
        $this->D4WTPEMONEYACCOUNT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPEMONEYACCOUNT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPEMONEYACCOUNT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPEMONEYACCOUNT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPEMONEYACCOUNT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPEMONEYACCOUNT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPEMONEYACCOUNT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPEMONEYACCOUNT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPEMONEYACCOUNT);
    }

}
