<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPDATTRAEGNO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPDATTRAEGNO[] $D4WTPDATTRAEGNO
     */
    protected $D4WTPDATTRAEGNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPDATTRAEGNO[]
     */
    public function getD4WTPDATTRAEGNO()
    {
      return $this->D4WTPDATTRAEGNO;
    }

    /**
     * @param D4WTPDATTRAEGNO[] $D4WTPDATTRAEGNO
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPDATTRAEGNO
     */
    public function setD4WTPDATTRAEGNO(array $D4WTPDATTRAEGNO = null)
    {
      $this->D4WTPDATTRAEGNO = $D4WTPDATTRAEGNO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPDATTRAEGNO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPDATTRAEGNO
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPDATTRAEGNO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPDATTRAEGNO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPDATTRAEGNO[] = $value;
      } else {
        $this->D4WTPDATTRAEGNO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPDATTRAEGNO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPDATTRAEGNO Return the current element
     */
    public function current()
    {
      return current($this->D4WTPDATTRAEGNO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPDATTRAEGNO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPDATTRAEGNO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPDATTRAEGNO);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPDATTRAEGNO Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPDATTRAEGNO);
    }

}
