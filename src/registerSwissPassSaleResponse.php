<?php

namespace Axess\Dci4Wtp;

class registerSwissPassSaleResponse
{

    /**
     * @var D4WTPSWISSPASSREGRES $registerSwissPassSaleResult
     */
    protected $registerSwissPassSaleResult = null;

    /**
     * @param D4WTPSWISSPASSREGRES $registerSwissPassSaleResult
     */
    public function __construct($registerSwissPassSaleResult)
    {
      $this->registerSwissPassSaleResult = $registerSwissPassSaleResult;
    }

    /**
     * @return D4WTPSWISSPASSREGRES
     */
    public function getRegisterSwissPassSaleResult()
    {
      return $this->registerSwissPassSaleResult;
    }

    /**
     * @param D4WTPSWISSPASSREGRES $registerSwissPassSaleResult
     * @return \Axess\Dci4Wtp\registerSwissPassSaleResponse
     */
    public function setRegisterSwissPassSaleResult($registerSwissPassSaleResult)
    {
      $this->registerSwissPassSaleResult = $registerSwissPassSaleResult;
      return $this;
    }

}
