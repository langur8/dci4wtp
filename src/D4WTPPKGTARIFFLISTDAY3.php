<?php

namespace Axess\Dci4Wtp;

class D4WTPPKGTARIFFLISTDAY3
{

    /**
     * @var ArrayOfD4WTPPACKAGELIST $ACTPACKAGELIST
     */
    protected $ACTPACKAGELIST = null;

    /**
     * @var D4WTPPACKAGEARTICLE $CTPACKAGEARTICLE
     */
    protected $CTPACKAGEARTICLE = null;

    /**
     * @var D4WTPPKGTARIFF3 $CTPKGTARIFF
     */
    protected $CTPKGTARIFF = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPACKAGELIST
     */
    public function getACTPACKAGELIST()
    {
      return $this->ACTPACKAGELIST;
    }

    /**
     * @param ArrayOfD4WTPPACKAGELIST $ACTPACKAGELIST
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY3
     */
    public function setACTPACKAGELIST($ACTPACKAGELIST)
    {
      $this->ACTPACKAGELIST = $ACTPACKAGELIST;
      return $this;
    }

    /**
     * @return D4WTPPACKAGEARTICLE
     */
    public function getCTPACKAGEARTICLE()
    {
      return $this->CTPACKAGEARTICLE;
    }

    /**
     * @param D4WTPPACKAGEARTICLE $CTPACKAGEARTICLE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY3
     */
    public function setCTPACKAGEARTICLE($CTPACKAGEARTICLE)
    {
      $this->CTPACKAGEARTICLE = $CTPACKAGEARTICLE;
      return $this;
    }

    /**
     * @return D4WTPPKGTARIFF3
     */
    public function getCTPKGTARIFF()
    {
      return $this->CTPKGTARIFF;
    }

    /**
     * @param D4WTPPKGTARIFF3 $CTPKGTARIFF
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY3
     */
    public function setCTPKGTARIFF($CTPKGTARIFF)
    {
      $this->CTPKGTARIFF = $CTPKGTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY3
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY3
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
