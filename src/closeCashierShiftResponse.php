<?php

namespace Axess\Dci4Wtp;

class closeCashierShiftResponse
{

    /**
     * @var D4WTPCLOSECASHIERSHIFTRESULT $closeCashierShiftResult
     */
    protected $closeCashierShiftResult = null;

    /**
     * @param D4WTPCLOSECASHIERSHIFTRESULT $closeCashierShiftResult
     */
    public function __construct($closeCashierShiftResult)
    {
      $this->closeCashierShiftResult = $closeCashierShiftResult;
    }

    /**
     * @return D4WTPCLOSECASHIERSHIFTRESULT
     */
    public function getCloseCashierShiftResult()
    {
      return $this->closeCashierShiftResult;
    }

    /**
     * @param D4WTPCLOSECASHIERSHIFTRESULT $closeCashierShiftResult
     * @return \Axess\Dci4Wtp\closeCashierShiftResponse
     */
    public function setCloseCashierShiftResult($closeCashierShiftResult)
    {
      $this->closeCashierShiftResult = $closeCashierShiftResult;
      return $this;
    }

}
