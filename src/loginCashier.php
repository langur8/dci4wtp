<?php

namespace Axess\Dci4Wtp;

class loginCashier
{

    /**
     * @var D4WTPLOGINCASHIERREQUEST $i_ctLoginCashierRequest
     */
    protected $i_ctLoginCashierRequest = null;

    /**
     * @param D4WTPLOGINCASHIERREQUEST $i_ctLoginCashierRequest
     */
    public function __construct($i_ctLoginCashierRequest)
    {
      $this->i_ctLoginCashierRequest = $i_ctLoginCashierRequest;
    }

    /**
     * @return D4WTPLOGINCASHIERREQUEST
     */
    public function getI_ctLoginCashierRequest()
    {
      return $this->i_ctLoginCashierRequest;
    }

    /**
     * @param D4WTPLOGINCASHIERREQUEST $i_ctLoginCashierRequest
     * @return \Axess\Dci4Wtp\loginCashier
     */
    public function setI_ctLoginCashierRequest($i_ctLoginCashierRequest)
    {
      $this->i_ctLoginCashierRequest = $i_ctLoginCashierRequest;
      return $this;
    }

}
