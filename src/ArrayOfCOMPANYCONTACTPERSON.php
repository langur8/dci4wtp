<?php

namespace Axess\Dci4Wtp;

class ArrayOfCOMPANYCONTACTPERSON implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var COMPANYCONTACTPERSON[] $COMPANYCONTACTPERSON
     */
    protected $COMPANYCONTACTPERSON = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return COMPANYCONTACTPERSON[]
     */
    public function getCOMPANYCONTACTPERSON()
    {
      return $this->COMPANYCONTACTPERSON;
    }

    /**
     * @param COMPANYCONTACTPERSON[] $COMPANYCONTACTPERSON
     * @return \Axess\Dci4Wtp\ArrayOfCOMPANYCONTACTPERSON
     */
    public function setCOMPANYCONTACTPERSON(array $COMPANYCONTACTPERSON = null)
    {
      $this->COMPANYCONTACTPERSON = $COMPANYCONTACTPERSON;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->COMPANYCONTACTPERSON[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return COMPANYCONTACTPERSON
     */
    public function offsetGet($offset)
    {
      return $this->COMPANYCONTACTPERSON[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param COMPANYCONTACTPERSON $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->COMPANYCONTACTPERSON[] = $value;
      } else {
        $this->COMPANYCONTACTPERSON[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->COMPANYCONTACTPERSON[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return COMPANYCONTACTPERSON Return the current element
     */
    public function current()
    {
      return current($this->COMPANYCONTACTPERSON);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->COMPANYCONTACTPERSON);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->COMPANYCONTACTPERSON);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->COMPANYCONTACTPERSON);
    }

    /**
     * Countable implementation
     *
     * @return COMPANYCONTACTPERSON Return count of elements
     */
    public function count()
    {
      return count($this->COMPANYCONTACTPERSON);
    }

}
