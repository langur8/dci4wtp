<?php

namespace Axess\Dci4Wtp;

class createExtOrderNr
{

    /**
     * @var D4WTPCREATEEXTORDERNRREQUEST $i_ctCreateExtOrderNrReq
     */
    protected $i_ctCreateExtOrderNrReq = null;

    /**
     * @param D4WTPCREATEEXTORDERNRREQUEST $i_ctCreateExtOrderNrReq
     */
    public function __construct($i_ctCreateExtOrderNrReq)
    {
      $this->i_ctCreateExtOrderNrReq = $i_ctCreateExtOrderNrReq;
    }

    /**
     * @return D4WTPCREATEEXTORDERNRREQUEST
     */
    public function getI_ctCreateExtOrderNrReq()
    {
      return $this->i_ctCreateExtOrderNrReq;
    }

    /**
     * @param D4WTPCREATEEXTORDERNRREQUEST $i_ctCreateExtOrderNrReq
     * @return \Axess\Dci4Wtp\createExtOrderNr
     */
    public function setI_ctCreateExtOrderNrReq($i_ctCreateExtOrderNrReq)
    {
      $this->i_ctCreateExtOrderNrReq = $i_ctCreateExtOrderNrReq;
      return $this;
    }

}
