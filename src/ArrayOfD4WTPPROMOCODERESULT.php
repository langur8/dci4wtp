<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPROMOCODERESULT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPROMOCODERESULT[] $D4WTPPROMOCODERESULT
     */
    protected $D4WTPPROMOCODERESULT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPROMOCODERESULT[]
     */
    public function getD4WTPPROMOCODERESULT()
    {
      return $this->D4WTPPROMOCODERESULT;
    }

    /**
     * @param D4WTPPROMOCODERESULT[] $D4WTPPROMOCODERESULT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPROMOCODERESULT
     */
    public function setD4WTPPROMOCODERESULT(array $D4WTPPROMOCODERESULT = null)
    {
      $this->D4WTPPROMOCODERESULT = $D4WTPPROMOCODERESULT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPROMOCODERESULT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPROMOCODERESULT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPROMOCODERESULT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPROMOCODERESULT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPROMOCODERESULT[] = $value;
      } else {
        $this->D4WTPPROMOCODERESULT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPROMOCODERESULT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPROMOCODERESULT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPROMOCODERESULT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPROMOCODERESULT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPROMOCODERESULT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPROMOCODERESULT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPROMOCODERESULT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPROMOCODERESULT);
    }

}
