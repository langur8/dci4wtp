<?php

namespace Axess\Dci4Wtp;

class getDSGVOFlags
{

    /**
     * @var D4WTPGETDSGVOFLAGSREQUEST $i_ctgetDSGVOFlagsReq
     */
    protected $i_ctgetDSGVOFlagsReq = null;

    /**
     * @param D4WTPGETDSGVOFLAGSREQUEST $i_ctgetDSGVOFlagsReq
     */
    public function __construct($i_ctgetDSGVOFlagsReq)
    {
      $this->i_ctgetDSGVOFlagsReq = $i_ctgetDSGVOFlagsReq;
    }

    /**
     * @return D4WTPGETDSGVOFLAGSREQUEST
     */
    public function getI_ctgetDSGVOFlagsReq()
    {
      return $this->i_ctgetDSGVOFlagsReq;
    }

    /**
     * @param D4WTPGETDSGVOFLAGSREQUEST $i_ctgetDSGVOFlagsReq
     * @return \Axess\Dci4Wtp\getDSGVOFlags
     */
    public function setI_ctgetDSGVOFlagsReq($i_ctgetDSGVOFlagsReq)
    {
      $this->i_ctgetDSGVOFlagsReq = $i_ctgetDSGVOFlagsReq;
      return $this;
    }

}
