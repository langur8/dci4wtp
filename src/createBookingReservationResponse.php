<?php

namespace Axess\Dci4Wtp;

class createBookingReservationResponse
{

    /**
     * @var WTPCreateReservationResult $createBookingReservationResult
     */
    protected $createBookingReservationResult = null;

    /**
     * @param WTPCreateReservationResult $createBookingReservationResult
     */
    public function __construct($createBookingReservationResult)
    {
      $this->createBookingReservationResult = $createBookingReservationResult;
    }

    /**
     * @return WTPCreateReservationResult
     */
    public function getCreateBookingReservationResult()
    {
      return $this->createBookingReservationResult;
    }

    /**
     * @param WTPCreateReservationResult $createBookingReservationResult
     * @return \Axess\Dci4Wtp\createBookingReservationResponse
     */
    public function setCreateBookingReservationResult($createBookingReservationResult)
    {
      $this->createBookingReservationResult = $createBookingReservationResult;
      return $this;
    }

}
