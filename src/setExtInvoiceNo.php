<?php

namespace Axess\Dci4Wtp;

class setExtInvoiceNo
{

    /**
     * @var D4WTPSETEXTINVOICENOREQUEST $i_ctSetExtInvoiceNoReq
     */
    protected $i_ctSetExtInvoiceNoReq = null;

    /**
     * @param D4WTPSETEXTINVOICENOREQUEST $i_ctSetExtInvoiceNoReq
     */
    public function __construct($i_ctSetExtInvoiceNoReq)
    {
      $this->i_ctSetExtInvoiceNoReq = $i_ctSetExtInvoiceNoReq;
    }

    /**
     * @return D4WTPSETEXTINVOICENOREQUEST
     */
    public function getI_ctSetExtInvoiceNoReq()
    {
      return $this->i_ctSetExtInvoiceNoReq;
    }

    /**
     * @param D4WTPSETEXTINVOICENOREQUEST $i_ctSetExtInvoiceNoReq
     * @return \Axess\Dci4Wtp\setExtInvoiceNo
     */
    public function setI_ctSetExtInvoiceNoReq($i_ctSetExtInvoiceNoReq)
    {
      $this->i_ctSetExtInvoiceNoReq = $i_ctSetExtInvoiceNoReq;
      return $this;
    }

}
