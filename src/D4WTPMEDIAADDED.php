<?php

namespace Axess\Dci4Wtp;

class D4WTPMEDIAADDED
{

    /**
     * @var float $BDELETED
     */
    protected $BDELETED = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    /**
     * @var string $SZWTPNR64
     */
    protected $SZWTPNR64 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBDELETED()
    {
      return $this->BDELETED;
    }

    /**
     * @param float $BDELETED
     * @return \Axess\Dci4Wtp\D4WTPMEDIAADDED
     */
    public function setBDELETED($BDELETED)
    {
      $this->BDELETED = $BDELETED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPMEDIAADDED
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPMEDIAADDED
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPMEDIAADDED
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNR64()
    {
      return $this->SZWTPNR64;
    }

    /**
     * @param string $SZWTPNR64
     * @return \Axess\Dci4Wtp\D4WTPMEDIAADDED
     */
    public function setSZWTPNR64($SZWTPNR64)
    {
      $this->SZWTPNR64 = $SZWTPNR64;
      return $this;
    }

}
