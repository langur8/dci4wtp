<?php

namespace Axess\Dci4Wtp;

class openCashierShiftResponse
{

    /**
     * @var D4WTPOPENCASHIERSHIFTRESULT $openCashierShiftResult
     */
    protected $openCashierShiftResult = null;

    /**
     * @param D4WTPOPENCASHIERSHIFTRESULT $openCashierShiftResult
     */
    public function __construct($openCashierShiftResult)
    {
      $this->openCashierShiftResult = $openCashierShiftResult;
    }

    /**
     * @return D4WTPOPENCASHIERSHIFTRESULT
     */
    public function getOpenCashierShiftResult()
    {
      return $this->openCashierShiftResult;
    }

    /**
     * @param D4WTPOPENCASHIERSHIFTRESULT $openCashierShiftResult
     * @return \Axess\Dci4Wtp\openCashierShiftResponse
     */
    public function setOpenCashierShiftResult($openCashierShiftResult)
    {
      $this->openCashierShiftResult = $openCashierShiftResult;
      return $this;
    }

}
