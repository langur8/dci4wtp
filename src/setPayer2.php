<?php

namespace Axess\Dci4Wtp;

class setPayer2
{

    /**
     * @var D4WTPPAYER2REQUEST $i_payer2Req
     */
    protected $i_payer2Req = null;

    /**
     * @param D4WTPPAYER2REQUEST $i_payer2Req
     */
    public function __construct($i_payer2Req)
    {
      $this->i_payer2Req = $i_payer2Req;
    }

    /**
     * @return D4WTPPAYER2REQUEST
     */
    public function getI_payer2Req()
    {
      return $this->i_payer2Req;
    }

    /**
     * @param D4WTPPAYER2REQUEST $i_payer2Req
     * @return \Axess\Dci4Wtp\setPayer2
     */
    public function setI_payer2Req($i_payer2Req)
    {
      $this->i_payer2Req = $i_payer2Req;
      return $this;
    }

}
