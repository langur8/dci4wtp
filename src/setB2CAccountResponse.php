<?php

namespace Axess\Dci4Wtp;

class setB2CAccountResponse
{

    /**
     * @var D4WTPSETB2CACCOUNTSRESULT $setB2CAccountResult
     */
    protected $setB2CAccountResult = null;

    /**
     * @param D4WTPSETB2CACCOUNTSRESULT $setB2CAccountResult
     */
    public function __construct($setB2CAccountResult)
    {
      $this->setB2CAccountResult = $setB2CAccountResult;
    }

    /**
     * @return D4WTPSETB2CACCOUNTSRESULT
     */
    public function getSetB2CAccountResult()
    {
      return $this->setB2CAccountResult;
    }

    /**
     * @param D4WTPSETB2CACCOUNTSRESULT $setB2CAccountResult
     * @return \Axess\Dci4Wtp\setB2CAccountResponse
     */
    public function setSetB2CAccountResult($setB2CAccountResult)
    {
      $this->setB2CAccountResult = $setB2CAccountResult;
      return $this;
    }

}
