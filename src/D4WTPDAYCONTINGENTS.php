<?php

namespace Axess\Dci4Wtp;

class D4WTPDAYCONTINGENTS
{

    /**
     * @var ArrayOfD4WTPCONTNGNTTICKET4 $ACTCONTNGNTTICKET
     */
    protected $ACTCONTNGNTTICKET = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCONTNGNTTICKET4
     */
    public function getACTCONTNGNTTICKET()
    {
      return $this->ACTCONTNGNTTICKET;
    }

    /**
     * @param ArrayOfD4WTPCONTNGNTTICKET4 $ACTCONTNGNTTICKET
     * @return \Axess\Dci4Wtp\D4WTPDAYCONTINGENTS
     */
    public function setACTCONTNGNTTICKET($ACTCONTNGNTTICKET)
    {
      $this->ACTCONTNGNTTICKET = $ACTCONTNGNTTICKET;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPDAYCONTINGENTS
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
