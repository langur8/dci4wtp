<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTICKETPRODDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTICKETPRODDATA[] $D4WTPTICKETPRODDATA
     */
    protected $D4WTPTICKETPRODDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTICKETPRODDATA[]
     */
    public function getD4WTPTICKETPRODDATA()
    {
      return $this->D4WTPTICKETPRODDATA;
    }

    /**
     * @param D4WTPTICKETPRODDATA[] $D4WTPTICKETPRODDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTICKETPRODDATA
     */
    public function setD4WTPTICKETPRODDATA(array $D4WTPTICKETPRODDATA = null)
    {
      $this->D4WTPTICKETPRODDATA = $D4WTPTICKETPRODDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTICKETPRODDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTICKETPRODDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTICKETPRODDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTICKETPRODDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTICKETPRODDATA[] = $value;
      } else {
        $this->D4WTPTICKETPRODDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTICKETPRODDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTICKETPRODDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTICKETPRODDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTICKETPRODDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTICKETPRODDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTICKETPRODDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTICKETPRODDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTICKETPRODDATA);
    }

}
