<?php

namespace Axess\Dci4Wtp;

class D4WTPGETTRAVELGRPLISTREQUEST
{

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NFIRSTNROWS
     */
    protected $NFIRSTNROWS = null;

    /**
     * @var float $NMAXROWTOFETCH
     */
    protected $NMAXROWTOFETCH = null;

    /**
     * @var float $NMINROWTOFETCH
     */
    protected $NMINROWTOFETCH = null;

    /**
     * @var float $NNAMEDUSERID
     */
    protected $NNAMEDUSERID = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NSTATUSNO
     */
    protected $NSTATUSNO = null;

    /**
     * @var float $NTRAVELGROUPNO
     */
    protected $NTRAVELGROUPNO = null;

    /**
     * @var string $SZDATEFROM
     */
    protected $SZDATEFROM = null;

    /**
     * @var string $SZDATETO
     */
    protected $SZDATETO = null;

    /**
     * @var string $SZEXTGROUPCODE
     */
    protected $SZEXTGROUPCODE = null;

    /**
     * @var string $SZGROUPID
     */
    protected $SZGROUPID = null;

    /**
     * @var string $SZORDERBYCOLUMN
     */
    protected $SZORDERBYCOLUMN = null;

    /**
     * @var string $SZORDERBYTYPE
     */
    protected $SZORDERBYTYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRSTNROWS()
    {
      return $this->NFIRSTNROWS;
    }

    /**
     * @param float $NFIRSTNROWS
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setNFIRSTNROWS($NFIRSTNROWS)
    {
      $this->NFIRSTNROWS = $NFIRSTNROWS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMAXROWTOFETCH()
    {
      return $this->NMAXROWTOFETCH;
    }

    /**
     * @param float $NMAXROWTOFETCH
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setNMAXROWTOFETCH($NMAXROWTOFETCH)
    {
      $this->NMAXROWTOFETCH = $NMAXROWTOFETCH;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMINROWTOFETCH()
    {
      return $this->NMINROWTOFETCH;
    }

    /**
     * @param float $NMINROWTOFETCH
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setNMINROWTOFETCH($NMINROWTOFETCH)
    {
      $this->NMINROWTOFETCH = $NMINROWTOFETCH;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNAMEDUSERID()
    {
      return $this->NNAMEDUSERID;
    }

    /**
     * @param float $NNAMEDUSERID
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setNNAMEDUSERID($NNAMEDUSERID)
    {
      $this->NNAMEDUSERID = $NNAMEDUSERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTATUSNO()
    {
      return $this->NSTATUSNO;
    }

    /**
     * @param float $NSTATUSNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setNSTATUSNO($NSTATUSNO)
    {
      $this->NSTATUSNO = $NSTATUSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPNO()
    {
      return $this->NTRAVELGROUPNO;
    }

    /**
     * @param float $NTRAVELGROUPNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setNTRAVELGROUPNO($NTRAVELGROUPNO)
    {
      $this->NTRAVELGROUPNO = $NTRAVELGROUPNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEFROM()
    {
      return $this->SZDATEFROM;
    }

    /**
     * @param string $SZDATEFROM
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setSZDATEFROM($SZDATEFROM)
    {
      $this->SZDATEFROM = $SZDATEFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATETO()
    {
      return $this->SZDATETO;
    }

    /**
     * @param string $SZDATETO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setSZDATETO($SZDATETO)
    {
      $this->SZDATETO = $SZDATETO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTGROUPCODE()
    {
      return $this->SZEXTGROUPCODE;
    }

    /**
     * @param string $SZEXTGROUPCODE
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setSZEXTGROUPCODE($SZEXTGROUPCODE)
    {
      $this->SZEXTGROUPCODE = $SZEXTGROUPCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGROUPID()
    {
      return $this->SZGROUPID;
    }

    /**
     * @param string $SZGROUPID
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setSZGROUPID($SZGROUPID)
    {
      $this->SZGROUPID = $SZGROUPID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZORDERBYCOLUMN()
    {
      return $this->SZORDERBYCOLUMN;
    }

    /**
     * @param string $SZORDERBYCOLUMN
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setSZORDERBYCOLUMN($SZORDERBYCOLUMN)
    {
      $this->SZORDERBYCOLUMN = $SZORDERBYCOLUMN;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZORDERBYTYPE()
    {
      return $this->SZORDERBYTYPE;
    }

    /**
     * @param string $SZORDERBYTYPE
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTREQUEST
     */
    public function setSZORDERBYTYPE($SZORDERBYTYPE)
    {
      $this->SZORDERBYTYPE = $SZORDERBYTYPE;
      return $this;
    }

}
