<?php

namespace Axess\Dci4Wtp;

class D4WTPADDRESSDATA
{

    /**
     * @var float $NADDRNO
     */
    protected $NADDRNO = null;

    /**
     * @var float $NADDRPOSNO
     */
    protected $NADDRPOSNO = null;

    /**
     * @var float $NADDRPROJNO
     */
    protected $NADDRPROJNO = null;

    /**
     * @var float $NADDRTYPENO
     */
    protected $NADDRTYPENO = null;

    /**
     * @var float $NCUSTNO
     */
    protected $NCUSTNO = null;

    /**
     * @var float $NCUSTPOSNO
     */
    protected $NCUSTPOSNO = null;

    /**
     * @var float $NCUSTPROJNO
     */
    protected $NCUSTPROJNO = null;

    /**
     * @var string $SZADDRTYPENAME
     */
    protected $SZADDRTYPENAME = null;

    /**
     * @var string $SZCITY
     */
    protected $SZCITY = null;

    /**
     * @var string $SZCO
     */
    protected $SZCO = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZMOBILE
     */
    protected $SZMOBILE = null;

    /**
     * @var string $SZPHONE
     */
    protected $SZPHONE = null;

    /**
     * @var string $SZSTREETNAME
     */
    protected $SZSTREETNAME = null;

    /**
     * @var string $SZSTREETNAME2
     */
    protected $SZSTREETNAME2 = null;

    /**
     * @var string $SZSTREETNAME3
     */
    protected $SZSTREETNAME3 = null;

    /**
     * @var string $SZZIPCODE
     */
    protected $SZZIPCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNADDRNO()
    {
      return $this->NADDRNO;
    }

    /**
     * @param float $NADDRNO
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setNADDRNO($NADDRNO)
    {
      $this->NADDRNO = $NADDRNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNADDRPOSNO()
    {
      return $this->NADDRPOSNO;
    }

    /**
     * @param float $NADDRPOSNO
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setNADDRPOSNO($NADDRPOSNO)
    {
      $this->NADDRPOSNO = $NADDRPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNADDRPROJNO()
    {
      return $this->NADDRPROJNO;
    }

    /**
     * @param float $NADDRPROJNO
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setNADDRPROJNO($NADDRPROJNO)
    {
      $this->NADDRPROJNO = $NADDRPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNADDRTYPENO()
    {
      return $this->NADDRTYPENO;
    }

    /**
     * @param float $NADDRTYPENO
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setNADDRTYPENO($NADDRTYPENO)
    {
      $this->NADDRTYPENO = $NADDRTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTNO()
    {
      return $this->NCUSTNO;
    }

    /**
     * @param float $NCUSTNO
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setNCUSTNO($NCUSTNO)
    {
      $this->NCUSTNO = $NCUSTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPOSNO()
    {
      return $this->NCUSTPOSNO;
    }

    /**
     * @param float $NCUSTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setNCUSTPOSNO($NCUSTPOSNO)
    {
      $this->NCUSTPOSNO = $NCUSTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPROJNO()
    {
      return $this->NCUSTPROJNO;
    }

    /**
     * @param float $NCUSTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setNCUSTPROJNO($NCUSTPROJNO)
    {
      $this->NCUSTPROJNO = $NCUSTPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZADDRTYPENAME()
    {
      return $this->SZADDRTYPENAME;
    }

    /**
     * @param string $SZADDRTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setSZADDRTYPENAME($SZADDRTYPENAME)
    {
      $this->SZADDRTYPENAME = $SZADDRTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCITY()
    {
      return $this->SZCITY;
    }

    /**
     * @param string $SZCITY
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setSZCITY($SZCITY)
    {
      $this->SZCITY = $SZCITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCO()
    {
      return $this->SZCO;
    }

    /**
     * @param string $SZCO
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setSZCO($SZCO)
    {
      $this->SZCO = $SZCO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMOBILE()
    {
      return $this->SZMOBILE;
    }

    /**
     * @param string $SZMOBILE
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setSZMOBILE($SZMOBILE)
    {
      $this->SZMOBILE = $SZMOBILE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPHONE()
    {
      return $this->SZPHONE;
    }

    /**
     * @param string $SZPHONE
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setSZPHONE($SZPHONE)
    {
      $this->SZPHONE = $SZPHONE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREETNAME()
    {
      return $this->SZSTREETNAME;
    }

    /**
     * @param string $SZSTREETNAME
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setSZSTREETNAME($SZSTREETNAME)
    {
      $this->SZSTREETNAME = $SZSTREETNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREETNAME2()
    {
      return $this->SZSTREETNAME2;
    }

    /**
     * @param string $SZSTREETNAME2
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setSZSTREETNAME2($SZSTREETNAME2)
    {
      $this->SZSTREETNAME2 = $SZSTREETNAME2;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREETNAME3()
    {
      return $this->SZSTREETNAME3;
    }

    /**
     * @param string $SZSTREETNAME3
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setSZSTREETNAME3($SZSTREETNAME3)
    {
      $this->SZSTREETNAME3 = $SZSTREETNAME3;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZZIPCODE()
    {
      return $this->SZZIPCODE;
    }

    /**
     * @param string $SZZIPCODE
     * @return \Axess\Dci4Wtp\D4WTPADDRESSDATA
     */
    public function setSZZIPCODE($SZZIPCODE)
    {
      $this->SZZIPCODE = $SZZIPCODE;
      return $this;
    }

}
