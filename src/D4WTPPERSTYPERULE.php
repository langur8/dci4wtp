<?php

namespace Axess\Dci4Wtp;

class D4WTPPERSTYPERULE
{

    /**
     * @var float $NAGEFROM
     */
    protected $NAGEFROM = null;

    /**
     * @var float $NAGETO
     */
    protected $NAGETO = null;

    /**
     * @var float $NPERSONTYPENO
     */
    protected $NPERSONTYPENO = null;

    /**
     * @var string $SZGENDER
     */
    protected $SZGENDER = null;

    /**
     * @var string $SZYEARFROM
     */
    protected $SZYEARFROM = null;

    /**
     * @var string $SZYEARTO
     */
    protected $SZYEARTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNAGEFROM()
    {
      return $this->NAGEFROM;
    }

    /**
     * @param float $NAGEFROM
     * @return \Axess\Dci4Wtp\D4WTPPERSTYPERULE
     */
    public function setNAGEFROM($NAGEFROM)
    {
      $this->NAGEFROM = $NAGEFROM;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAGETO()
    {
      return $this->NAGETO;
    }

    /**
     * @param float $NAGETO
     * @return \Axess\Dci4Wtp\D4WTPPERSTYPERULE
     */
    public function setNAGETO($NAGETO)
    {
      $this->NAGETO = $NAGETO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSONTYPENO()
    {
      return $this->NPERSONTYPENO;
    }

    /**
     * @param float $NPERSONTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPERSTYPERULE
     */
    public function setNPERSONTYPENO($NPERSONTYPENO)
    {
      $this->NPERSONTYPENO = $NPERSONTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGENDER()
    {
      return $this->SZGENDER;
    }

    /**
     * @param string $SZGENDER
     * @return \Axess\Dci4Wtp\D4WTPPERSTYPERULE
     */
    public function setSZGENDER($SZGENDER)
    {
      $this->SZGENDER = $SZGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZYEARFROM()
    {
      return $this->SZYEARFROM;
    }

    /**
     * @param string $SZYEARFROM
     * @return \Axess\Dci4Wtp\D4WTPPERSTYPERULE
     */
    public function setSZYEARFROM($SZYEARFROM)
    {
      $this->SZYEARFROM = $SZYEARFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZYEARTO()
    {
      return $this->SZYEARTO;
    }

    /**
     * @param string $SZYEARTO
     * @return \Axess\Dci4Wtp\D4WTPPERSTYPERULE
     */
    public function setSZYEARTO($SZYEARTO)
    {
      $this->SZYEARTO = $SZYEARTO;
      return $this;
    }

}
