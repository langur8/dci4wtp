<?php

namespace Axess\Dci4Wtp;

class getGroupSubContingent2Response
{

    /**
     * @var D4WTPGETGRPSUBCONTINGENT2RES $getGroupSubContingent2Result
     */
    protected $getGroupSubContingent2Result = null;

    /**
     * @param D4WTPGETGRPSUBCONTINGENT2RES $getGroupSubContingent2Result
     */
    public function __construct($getGroupSubContingent2Result)
    {
      $this->getGroupSubContingent2Result = $getGroupSubContingent2Result;
    }

    /**
     * @return D4WTPGETGRPSUBCONTINGENT2RES
     */
    public function getGetGroupSubContingent2Result()
    {
      return $this->getGroupSubContingent2Result;
    }

    /**
     * @param D4WTPGETGRPSUBCONTINGENT2RES $getGroupSubContingent2Result
     * @return \Axess\Dci4Wtp\getGroupSubContingent2Response
     */
    public function setGetGroupSubContingent2Result($getGroupSubContingent2Result)
    {
      $this->getGroupSubContingent2Result = $getGroupSubContingent2Result;
      return $this;
    }

}
