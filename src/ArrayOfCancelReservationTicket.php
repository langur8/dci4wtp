<?php

namespace Axess\Dci4Wtp;

class ArrayOfCancelReservationTicket implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CancelReservationTicket[] $CancelReservationTicket
     */
    protected $CancelReservationTicket = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CancelReservationTicket[]
     */
    public function getCancelReservationTicket()
    {
      return $this->CancelReservationTicket;
    }

    /**
     * @param CancelReservationTicket[] $CancelReservationTicket
     * @return \Axess\Dci4Wtp\ArrayOfCancelReservationTicket
     */
    public function setCancelReservationTicket(array $CancelReservationTicket = null)
    {
      $this->CancelReservationTicket = $CancelReservationTicket;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CancelReservationTicket[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CancelReservationTicket
     */
    public function offsetGet($offset)
    {
      return $this->CancelReservationTicket[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CancelReservationTicket $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->CancelReservationTicket[] = $value;
      } else {
        $this->CancelReservationTicket[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CancelReservationTicket[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CancelReservationTicket Return the current element
     */
    public function current()
    {
      return current($this->CancelReservationTicket);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CancelReservationTicket);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CancelReservationTicket);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CancelReservationTicket);
    }

    /**
     * Countable implementation
     *
     * @return CancelReservationTicket Return count of elements
     */
    public function count()
    {
      return count($this->CancelReservationTicket);
    }

}
