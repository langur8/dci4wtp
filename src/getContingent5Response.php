<?php

namespace Axess\Dci4Wtp;

class getContingent5Response
{

    /**
     * @var D4WTPGETCONTINGENT5RESULT $getContingent5Result
     */
    protected $getContingent5Result = null;

    /**
     * @param D4WTPGETCONTINGENT5RESULT $getContingent5Result
     */
    public function __construct($getContingent5Result)
    {
      $this->getContingent5Result = $getContingent5Result;
    }

    /**
     * @return D4WTPGETCONTINGENT5RESULT
     */
    public function getGetContingent5Result()
    {
      return $this->getContingent5Result;
    }

    /**
     * @param D4WTPGETCONTINGENT5RESULT $getContingent5Result
     * @return \Axess\Dci4Wtp\getContingent5Response
     */
    public function setGetContingent5Result($getContingent5Result)
    {
      $this->getContingent5Result = $getContingent5Result;
      return $this;
    }

}
