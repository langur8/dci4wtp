<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCHECKPROMOCODES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCHECKPROMOCODES[] $D4WTPCHECKPROMOCODES
     */
    protected $D4WTPCHECKPROMOCODES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCHECKPROMOCODES[]
     */
    public function getD4WTPCHECKPROMOCODES()
    {
      return $this->D4WTPCHECKPROMOCODES;
    }

    /**
     * @param D4WTPCHECKPROMOCODES[] $D4WTPCHECKPROMOCODES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCHECKPROMOCODES
     */
    public function setD4WTPCHECKPROMOCODES(array $D4WTPCHECKPROMOCODES = null)
    {
      $this->D4WTPCHECKPROMOCODES = $D4WTPCHECKPROMOCODES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCHECKPROMOCODES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCHECKPROMOCODES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCHECKPROMOCODES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCHECKPROMOCODES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCHECKPROMOCODES[] = $value;
      } else {
        $this->D4WTPCHECKPROMOCODES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCHECKPROMOCODES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCHECKPROMOCODES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCHECKPROMOCODES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCHECKPROMOCODES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCHECKPROMOCODES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCHECKPROMOCODES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCHECKPROMOCODES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCHECKPROMOCODES);
    }

}
