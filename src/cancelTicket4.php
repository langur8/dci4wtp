<?php

namespace Axess\Dci4Wtp;

class cancelTicket4
{

    /**
     * @var D4WTPCANCELTICKET4REQUEST $i_ctCancelTicket4Request
     */
    protected $i_ctCancelTicket4Request = null;

    /**
     * @param D4WTPCANCELTICKET4REQUEST $i_ctCancelTicket4Request
     */
    public function __construct($i_ctCancelTicket4Request)
    {
      $this->i_ctCancelTicket4Request = $i_ctCancelTicket4Request;
    }

    /**
     * @return D4WTPCANCELTICKET4REQUEST
     */
    public function getI_ctCancelTicket4Request()
    {
      return $this->i_ctCancelTicket4Request;
    }

    /**
     * @param D4WTPCANCELTICKET4REQUEST $i_ctCancelTicket4Request
     * @return \Axess\Dci4Wtp\cancelTicket4
     */
    public function setI_ctCancelTicket4Request($i_ctCancelTicket4Request)
    {
      $this->i_ctCancelTicket4Request = $i_ctCancelTicket4Request;
      return $this;
    }

}
