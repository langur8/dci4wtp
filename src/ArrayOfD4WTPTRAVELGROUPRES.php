<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTRAVELGROUPRES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTRAVELGROUPRES[] $D4WTPTRAVELGROUPRES
     */
    protected $D4WTPTRAVELGROUPRES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTRAVELGROUPRES[]
     */
    public function getD4WTPTRAVELGROUPRES()
    {
      return $this->D4WTPTRAVELGROUPRES;
    }

    /**
     * @param D4WTPTRAVELGROUPRES[] $D4WTPTRAVELGROUPRES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTRAVELGROUPRES
     */
    public function setD4WTPTRAVELGROUPRES(array $D4WTPTRAVELGROUPRES = null)
    {
      $this->D4WTPTRAVELGROUPRES = $D4WTPTRAVELGROUPRES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTRAVELGROUPRES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTRAVELGROUPRES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTRAVELGROUPRES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTRAVELGROUPRES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTRAVELGROUPRES[] = $value;
      } else {
        $this->D4WTPTRAVELGROUPRES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTRAVELGROUPRES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTRAVELGROUPRES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTRAVELGROUPRES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTRAVELGROUPRES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTRAVELGROUPRES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTRAVELGROUPRES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTRAVELGROUPRES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTRAVELGROUPRES);
    }

}
