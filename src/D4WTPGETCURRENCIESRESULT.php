<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCURRENCIESRESULT
{

    /**
     * @var ArrayOfD4WTPCURRENCYREC $ACTCURRENCY
     */
    protected $ACTCURRENCY = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCURRENCYREC
     */
    public function getACTCURRENCY()
    {
      return $this->ACTCURRENCY;
    }

    /**
     * @param ArrayOfD4WTPCURRENCYREC $ACTCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPGETCURRENCIESRESULT
     */
    public function setACTCURRENCY($ACTCURRENCY)
    {
      $this->ACTCURRENCY = $ACTCURRENCY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETCURRENCIESRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETCURRENCIESRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
