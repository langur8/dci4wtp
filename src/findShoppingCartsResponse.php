<?php

namespace Axess\Dci4Wtp;

class findShoppingCartsResponse
{

    /**
     * @var D4WTPFINDSHOPCARTSRESULT $findShoppingCartsResult
     */
    protected $findShoppingCartsResult = null;

    /**
     * @param D4WTPFINDSHOPCARTSRESULT $findShoppingCartsResult
     */
    public function __construct($findShoppingCartsResult)
    {
      $this->findShoppingCartsResult = $findShoppingCartsResult;
    }

    /**
     * @return D4WTPFINDSHOPCARTSRESULT
     */
    public function getFindShoppingCartsResult()
    {
      return $this->findShoppingCartsResult;
    }

    /**
     * @param D4WTPFINDSHOPCARTSRESULT $findShoppingCartsResult
     * @return \Axess\Dci4Wtp\findShoppingCartsResponse
     */
    public function setFindShoppingCartsResult($findShoppingCartsResult)
    {
      $this->findShoppingCartsResult = $findShoppingCartsResult;
      return $this;
    }

}
