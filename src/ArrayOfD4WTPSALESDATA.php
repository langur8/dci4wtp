<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSALESDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSALESDATA[] $D4WTPSALESDATA
     */
    protected $D4WTPSALESDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSALESDATA[]
     */
    public function getD4WTPSALESDATA()
    {
      return $this->D4WTPSALESDATA;
    }

    /**
     * @param D4WTPSALESDATA[] $D4WTPSALESDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSALESDATA
     */
    public function setD4WTPSALESDATA(array $D4WTPSALESDATA = null)
    {
      $this->D4WTPSALESDATA = $D4WTPSALESDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSALESDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSALESDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSALESDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSALESDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSALESDATA[] = $value;
      } else {
        $this->D4WTPSALESDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSALESDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSALESDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSALESDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSALESDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSALESDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSALESDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSALESDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSALESDATA);
    }

}
