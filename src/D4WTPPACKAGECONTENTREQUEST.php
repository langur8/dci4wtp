<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGECONTENTREQUEST
{

    /**
     * @var D4WTPPACKAGE $CTPACKAGE
     */
    protected $CTPACKAGE = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPACKAGE
     */
    public function getCTPACKAGE()
    {
      return $this->CTPACKAGE;
    }

    /**
     * @param D4WTPPACKAGE $CTPACKAGE
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENTREQUEST
     */
    public function setCTPACKAGE($CTPACKAGE)
    {
      $this->CTPACKAGE = $CTPACKAGE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENTREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENTREQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

}
