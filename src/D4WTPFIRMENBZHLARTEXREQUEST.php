<?php

namespace Axess\Dci4Wtp;

class D4WTPFIRMENBZHLARTEXREQUEST
{

    /**
     * @var float $NFIRMENKASSANR
     */
    protected $NFIRMENKASSANR = null;

    /**
     * @var float $NFIRMENNR
     */
    protected $NFIRMENNR = null;

    /**
     * @var float $NFIRMENPROJNR
     */
    protected $NFIRMENPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNFIRMENKASSANR()
    {
      return $this->NFIRMENKASSANR;
    }

    /**
     * @param float $NFIRMENKASSANR
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBZHLARTEXREQUEST
     */
    public function setNFIRMENKASSANR($NFIRMENKASSANR)
    {
      $this->NFIRMENKASSANR = $NFIRMENKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRMENNR()
    {
      return $this->NFIRMENNR;
    }

    /**
     * @param float $NFIRMENNR
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBZHLARTEXREQUEST
     */
    public function setNFIRMENNR($NFIRMENNR)
    {
      $this->NFIRMENNR = $NFIRMENNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRMENPROJNR()
    {
      return $this->NFIRMENPROJNR;
    }

    /**
     * @param float $NFIRMENPROJNR
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBZHLARTEXREQUEST
     */
    public function setNFIRMENPROJNR($NFIRMENPROJNR)
    {
      $this->NFIRMENPROJNR = $NFIRMENPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPFIRMENBZHLARTEXREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
