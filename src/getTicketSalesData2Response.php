<?php

namespace Axess\Dci4Wtp;

class getTicketSalesData2Response
{

    /**
     * @var D4WTPSALESDATARESULT2 $getTicketSalesData2Result
     */
    protected $getTicketSalesData2Result = null;

    /**
     * @param D4WTPSALESDATARESULT2 $getTicketSalesData2Result
     */
    public function __construct($getTicketSalesData2Result)
    {
      $this->getTicketSalesData2Result = $getTicketSalesData2Result;
    }

    /**
     * @return D4WTPSALESDATARESULT2
     */
    public function getGetTicketSalesData2Result()
    {
      return $this->getTicketSalesData2Result;
    }

    /**
     * @param D4WTPSALESDATARESULT2 $getTicketSalesData2Result
     * @return \Axess\Dci4Wtp\getTicketSalesData2Response
     */
    public function setGetTicketSalesData2Result($getTicketSalesData2Result)
    {
      $this->getTicketSalesData2Result = $getTicketSalesData2Result;
      return $this;
    }

}
