<?php

namespace Axess\Dci4Wtp;

class D4WTPFINDSHOPCARTSRESULT
{

    /**
     * @var ArrayOfD4WTPSHOPCARTFOUND $ACTSHOPCARTFOUND
     */
    protected $ACTSHOPCARTFOUND = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSHOPCARTFOUND
     */
    public function getACTSHOPCARTFOUND()
    {
      return $this->ACTSHOPCARTFOUND;
    }

    /**
     * @param ArrayOfD4WTPSHOPCARTFOUND $ACTSHOPCARTFOUND
     * @return \Axess\Dci4Wtp\D4WTPFINDSHOPCARTSRESULT
     */
    public function setACTSHOPCARTFOUND($ACTSHOPCARTFOUND)
    {
      $this->ACTSHOPCARTFOUND = $ACTSHOPCARTFOUND;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPFINDSHOPCARTSRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPFINDSHOPCARTSRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
