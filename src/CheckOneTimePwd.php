<?php

namespace Axess\Dci4Wtp;

class CheckOneTimePwd
{

    /**
     * @var string $i_szUserID
     */
    protected $i_szUserID = null;

    /**
     * @var string $i_szPassword
     */
    protected $i_szPassword = null;

    /**
     * @param string $i_szUserID
     * @param string $i_szPassword
     */
    public function __construct($i_szUserID, $i_szPassword)
    {
      $this->i_szUserID = $i_szUserID;
      $this->i_szPassword = $i_szPassword;
    }

    /**
     * @return string
     */
    public function getI_szUserID()
    {
      return $this->i_szUserID;
    }

    /**
     * @param string $i_szUserID
     * @return \Axess\Dci4Wtp\CheckOneTimePwd
     */
    public function setI_szUserID($i_szUserID)
    {
      $this->i_szUserID = $i_szUserID;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szPassword()
    {
      return $this->i_szPassword;
    }

    /**
     * @param string $i_szPassword
     * @return \Axess\Dci4Wtp\CheckOneTimePwd
     */
    public function setI_szPassword($i_szPassword)
    {
      $this->i_szPassword = $i_szPassword;
      return $this;
    }

}
