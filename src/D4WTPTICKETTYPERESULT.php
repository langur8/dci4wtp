<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETTYPERESULT
{

    /**
     * @var ArrayOfD4WTPTICKETTYPE $ACTTICKETTYPES
     */
    protected $ACTTICKETTYPES = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKETTYPE
     */
    public function getACTTICKETTYPES()
    {
      return $this->ACTTICKETTYPES;
    }

    /**
     * @param ArrayOfD4WTPTICKETTYPE $ACTTICKETTYPES
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPERESULT
     */
    public function setACTTICKETTYPES($ACTTICKETTYPES)
    {
      $this->ACTTICKETTYPES = $ACTTICKETTYPES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPERESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPERESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
