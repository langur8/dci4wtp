<?php

namespace Axess\Dci4Wtp;

class cancelTicket4Response
{

    /**
     * @var D4WTPCANCELTICKET4RESULT $cancelTicket4Result
     */
    protected $cancelTicket4Result = null;

    /**
     * @param D4WTPCANCELTICKET4RESULT $cancelTicket4Result
     */
    public function __construct($cancelTicket4Result)
    {
      $this->cancelTicket4Result = $cancelTicket4Result;
    }

    /**
     * @return D4WTPCANCELTICKET4RESULT
     */
    public function getCancelTicket4Result()
    {
      return $this->cancelTicket4Result;
    }

    /**
     * @param D4WTPCANCELTICKET4RESULT $cancelTicket4Result
     * @return \Axess\Dci4Wtp\cancelTicket4Response
     */
    public function setCancelTicket4Result($cancelTicket4Result)
    {
      $this->cancelTicket4Result = $cancelTicket4Result;
      return $this;
    }

}
