<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPRODUCTORDER11 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPRODUCTORDER11[] $D4WTPPRODUCTORDER11
     */
    protected $D4WTPPRODUCTORDER11 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPRODUCTORDER11[]
     */
    public function getD4WTPPRODUCTORDER11()
    {
      return $this->D4WTPPRODUCTORDER11;
    }

    /**
     * @param D4WTPPRODUCTORDER11[] $D4WTPPRODUCTORDER11
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPRODUCTORDER11
     */
    public function setD4WTPPRODUCTORDER11(array $D4WTPPRODUCTORDER11 = null)
    {
      $this->D4WTPPRODUCTORDER11 = $D4WTPPRODUCTORDER11;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPRODUCTORDER11[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPRODUCTORDER11
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPRODUCTORDER11[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPRODUCTORDER11 $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPRODUCTORDER11[] = $value;
      } else {
        $this->D4WTPPRODUCTORDER11[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPRODUCTORDER11[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPRODUCTORDER11 Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPRODUCTORDER11);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPRODUCTORDER11);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPRODUCTORDER11);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPRODUCTORDER11);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPRODUCTORDER11 Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPRODUCTORDER11);
    }

}
