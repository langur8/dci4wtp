<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGEPOSTARIFF
{

    /**
     * @var float $BPRICETARIFFSHEET
     */
    protected $BPRICETARIFFSHEET = null;

    /**
     * @var float $FSINGLETARIFF
     */
    protected $FSINGLETARIFF = null;

    /**
     * @var float $NARTICLENR
     */
    protected $NARTICLENR = null;

    /**
     * @var float $NCUSTOMERTICKETTYPENR
     */
    protected $NCUSTOMERTICKETTYPENR = null;

    /**
     * @var float $NPACKNR
     */
    protected $NPACKNR = null;

    /**
     * @var float $NPACKPOS
     */
    protected $NPACKPOS = null;

    /**
     * @var float $NPACKTARIFFSHEETNR
     */
    protected $NPACKTARIFFSHEETNR = null;

    /**
     * @var float $NPACKTARIFFVALIDNR
     */
    protected $NPACKTARIFFVALIDNR = null;

    /**
     * @var float $NPERSTYPENR
     */
    protected $NPERSTYPENR = null;

    /**
     * @var float $NPOOLNR
     */
    protected $NPOOLNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBPRICETARIFFSHEET()
    {
      return $this->BPRICETARIFFSHEET;
    }

    /**
     * @param float $BPRICETARIFFSHEET
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFF
     */
    public function setBPRICETARIFFSHEET($BPRICETARIFFSHEET)
    {
      $this->BPRICETARIFFSHEET = $BPRICETARIFFSHEET;
      return $this;
    }

    /**
     * @return float
     */
    public function getFSINGLETARIFF()
    {
      return $this->FSINGLETARIFF;
    }

    /**
     * @param float $FSINGLETARIFF
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFF
     */
    public function setFSINGLETARIFF($FSINGLETARIFF)
    {
      $this->FSINGLETARIFF = $FSINGLETARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENR()
    {
      return $this->NARTICLENR;
    }

    /**
     * @param float $NARTICLENR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFF
     */
    public function setNARTICLENR($NARTICLENR)
    {
      $this->NARTICLENR = $NARTICLENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTOMERTICKETTYPENR()
    {
      return $this->NCUSTOMERTICKETTYPENR;
    }

    /**
     * @param float $NCUSTOMERTICKETTYPENR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFF
     */
    public function setNCUSTOMERTICKETTYPENR($NCUSTOMERTICKETTYPENR)
    {
      $this->NCUSTOMERTICKETTYPENR = $NCUSTOMERTICKETTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKNR()
    {
      return $this->NPACKNR;
    }

    /**
     * @param float $NPACKNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFF
     */
    public function setNPACKNR($NPACKNR)
    {
      $this->NPACKNR = $NPACKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKPOS()
    {
      return $this->NPACKPOS;
    }

    /**
     * @param float $NPACKPOS
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFF
     */
    public function setNPACKPOS($NPACKPOS)
    {
      $this->NPACKPOS = $NPACKPOS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKTARIFFSHEETNR()
    {
      return $this->NPACKTARIFFSHEETNR;
    }

    /**
     * @param float $NPACKTARIFFSHEETNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFF
     */
    public function setNPACKTARIFFSHEETNR($NPACKTARIFFSHEETNR)
    {
      $this->NPACKTARIFFSHEETNR = $NPACKTARIFFSHEETNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKTARIFFVALIDNR()
    {
      return $this->NPACKTARIFFVALIDNR;
    }

    /**
     * @param float $NPACKTARIFFVALIDNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFF
     */
    public function setNPACKTARIFFVALIDNR($NPACKTARIFFVALIDNR)
    {
      $this->NPACKTARIFFVALIDNR = $NPACKTARIFFVALIDNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSTYPENR()
    {
      return $this->NPERSTYPENR;
    }

    /**
     * @param float $NPERSTYPENR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFF
     */
    public function setNPERSTYPENR($NPERSTYPENR)
    {
      $this->NPERSTYPENR = $NPERSTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNR()
    {
      return $this->NPOOLNR;
    }

    /**
     * @param float $NPOOLNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFF
     */
    public function setNPOOLNR($NPOOLNR)
    {
      $this->NPOOLNR = $NPOOLNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSTARIFF
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

}
