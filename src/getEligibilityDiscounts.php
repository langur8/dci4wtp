<?php

namespace Axess\Dci4Wtp;

class getEligibilityDiscounts
{

    /**
     * @var D4WTPGETELIGIBILITYDISCREQ $i_request
     */
    protected $i_request = null;

    /**
     * @param D4WTPGETELIGIBILITYDISCREQ $i_request
     */
    public function __construct($i_request)
    {
      $this->i_request = $i_request;
    }

    /**
     * @return D4WTPGETELIGIBILITYDISCREQ
     */
    public function getI_request()
    {
      return $this->i_request;
    }

    /**
     * @param D4WTPGETELIGIBILITYDISCREQ $i_request
     * @return \Axess\Dci4Wtp\getEligibilityDiscounts
     */
    public function setI_request($i_request)
    {
      $this->i_request = $i_request;
      return $this;
    }

}
