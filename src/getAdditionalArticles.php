<?php

namespace Axess\Dci4Wtp;

class getAdditionalArticles
{

    /**
     * @var D4WTPADDARTICLEREQUEST $i_ctAddArticleReq
     */
    protected $i_ctAddArticleReq = null;

    /**
     * @param D4WTPADDARTICLEREQUEST $i_ctAddArticleReq
     */
    public function __construct($i_ctAddArticleReq)
    {
      $this->i_ctAddArticleReq = $i_ctAddArticleReq;
    }

    /**
     * @return D4WTPADDARTICLEREQUEST
     */
    public function getI_ctAddArticleReq()
    {
      return $this->i_ctAddArticleReq;
    }

    /**
     * @param D4WTPADDARTICLEREQUEST $i_ctAddArticleReq
     * @return \Axess\Dci4Wtp\getAdditionalArticles
     */
    public function setI_ctAddArticleReq($i_ctAddArticleReq)
    {
      $this->i_ctAddArticleReq = $i_ctAddArticleReq;
      return $this;
    }

}
