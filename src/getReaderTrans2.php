<?php

namespace Axess\Dci4Wtp;

class getReaderTrans2
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var float $i_nProjNo
     */
    protected $i_nProjNo = null;

    /**
     * @var float $i_nPOSNo
     */
    protected $i_nPOSNo = null;

    /**
     * @var float $i_nSerialNo
     */
    protected $i_nSerialNo = null;

    /**
     * @var float $i_nUnicodeNo
     */
    protected $i_nUnicodeNo = null;

    /**
     * @var string $i_szValidDate
     */
    protected $i_szValidDate = null;

    /**
     * @var string $i_szOCCPermissionHandle
     */
    protected $i_szOCCPermissionHandle = null;

    /**
     * @var float $i_nMaxNoOfRows
     */
    protected $i_nMaxNoOfRows = null;

    /**
     * @var string $i_szUsageDateFrom
     */
    protected $i_szUsageDateFrom = null;

    /**
     * @var string $i_szUsageDateTo
     */
    protected $i_szUsageDateTo = null;

    /**
     * @var float $i_nTicketTypeNo
     */
    protected $i_nTicketTypeNo = null;

    /**
     * @var float $i_nPoolNo
     */
    protected $i_nPoolNo = null;

    /**
     * @param float $i_nSessionID
     * @param float $i_nProjNo
     * @param float $i_nPOSNo
     * @param float $i_nSerialNo
     * @param float $i_nUnicodeNo
     * @param string $i_szValidDate
     * @param string $i_szOCCPermissionHandle
     * @param float $i_nMaxNoOfRows
     * @param string $i_szUsageDateFrom
     * @param string $i_szUsageDateTo
     * @param float $i_nTicketTypeNo
     * @param float $i_nPoolNo
     */
    public function __construct($i_nSessionID, $i_nProjNo, $i_nPOSNo, $i_nSerialNo, $i_nUnicodeNo, $i_szValidDate, $i_szOCCPermissionHandle, $i_nMaxNoOfRows, $i_szUsageDateFrom, $i_szUsageDateTo, $i_nTicketTypeNo, $i_nPoolNo)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_nProjNo = $i_nProjNo;
      $this->i_nPOSNo = $i_nPOSNo;
      $this->i_nSerialNo = $i_nSerialNo;
      $this->i_nUnicodeNo = $i_nUnicodeNo;
      $this->i_szValidDate = $i_szValidDate;
      $this->i_szOCCPermissionHandle = $i_szOCCPermissionHandle;
      $this->i_nMaxNoOfRows = $i_nMaxNoOfRows;
      $this->i_szUsageDateFrom = $i_szUsageDateFrom;
      $this->i_szUsageDateTo = $i_szUsageDateTo;
      $this->i_nTicketTypeNo = $i_nTicketTypeNo;
      $this->i_nPoolNo = $i_nPoolNo;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\getReaderTrans2
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nProjNo()
    {
      return $this->i_nProjNo;
    }

    /**
     * @param float $i_nProjNo
     * @return \Axess\Dci4Wtp\getReaderTrans2
     */
    public function setI_nProjNo($i_nProjNo)
    {
      $this->i_nProjNo = $i_nProjNo;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nPOSNo()
    {
      return $this->i_nPOSNo;
    }

    /**
     * @param float $i_nPOSNo
     * @return \Axess\Dci4Wtp\getReaderTrans2
     */
    public function setI_nPOSNo($i_nPOSNo)
    {
      $this->i_nPOSNo = $i_nPOSNo;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nSerialNo()
    {
      return $this->i_nSerialNo;
    }

    /**
     * @param float $i_nSerialNo
     * @return \Axess\Dci4Wtp\getReaderTrans2
     */
    public function setI_nSerialNo($i_nSerialNo)
    {
      $this->i_nSerialNo = $i_nSerialNo;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nUnicodeNo()
    {
      return $this->i_nUnicodeNo;
    }

    /**
     * @param float $i_nUnicodeNo
     * @return \Axess\Dci4Wtp\getReaderTrans2
     */
    public function setI_nUnicodeNo($i_nUnicodeNo)
    {
      $this->i_nUnicodeNo = $i_nUnicodeNo;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szValidDate()
    {
      return $this->i_szValidDate;
    }

    /**
     * @param string $i_szValidDate
     * @return \Axess\Dci4Wtp\getReaderTrans2
     */
    public function setI_szValidDate($i_szValidDate)
    {
      $this->i_szValidDate = $i_szValidDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szOCCPermissionHandle()
    {
      return $this->i_szOCCPermissionHandle;
    }

    /**
     * @param string $i_szOCCPermissionHandle
     * @return \Axess\Dci4Wtp\getReaderTrans2
     */
    public function setI_szOCCPermissionHandle($i_szOCCPermissionHandle)
    {
      $this->i_szOCCPermissionHandle = $i_szOCCPermissionHandle;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nMaxNoOfRows()
    {
      return $this->i_nMaxNoOfRows;
    }

    /**
     * @param float $i_nMaxNoOfRows
     * @return \Axess\Dci4Wtp\getReaderTrans2
     */
    public function setI_nMaxNoOfRows($i_nMaxNoOfRows)
    {
      $this->i_nMaxNoOfRows = $i_nMaxNoOfRows;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szUsageDateFrom()
    {
      return $this->i_szUsageDateFrom;
    }

    /**
     * @param string $i_szUsageDateFrom
     * @return \Axess\Dci4Wtp\getReaderTrans2
     */
    public function setI_szUsageDateFrom($i_szUsageDateFrom)
    {
      $this->i_szUsageDateFrom = $i_szUsageDateFrom;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szUsageDateTo()
    {
      return $this->i_szUsageDateTo;
    }

    /**
     * @param string $i_szUsageDateTo
     * @return \Axess\Dci4Wtp\getReaderTrans2
     */
    public function setI_szUsageDateTo($i_szUsageDateTo)
    {
      $this->i_szUsageDateTo = $i_szUsageDateTo;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nTicketTypeNo()
    {
      return $this->i_nTicketTypeNo;
    }

    /**
     * @param float $i_nTicketTypeNo
     * @return \Axess\Dci4Wtp\getReaderTrans2
     */
    public function setI_nTicketTypeNo($i_nTicketTypeNo)
    {
      $this->i_nTicketTypeNo = $i_nTicketTypeNo;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nPoolNo()
    {
      return $this->i_nPoolNo;
    }

    /**
     * @param float $i_nPoolNo
     * @return \Axess\Dci4Wtp\getReaderTrans2
     */
    public function setI_nPoolNo($i_nPoolNo)
    {
      $this->i_nPoolNo = $i_nPoolNo;
      return $this;
    }

}
