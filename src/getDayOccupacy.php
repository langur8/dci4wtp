<?php

namespace Axess\Dci4Wtp;

class getDayOccupacy
{

    /**
     * @var D4WTPGETDAYOCUPACYREQ $i_request
     */
    protected $i_request = null;

    /**
     * @param D4WTPGETDAYOCUPACYREQ $i_request
     */
    public function __construct($i_request)
    {
      $this->i_request = $i_request;
    }

    /**
     * @return D4WTPGETDAYOCUPACYREQ
     */
    public function getI_request()
    {
      return $this->i_request;
    }

    /**
     * @param D4WTPGETDAYOCUPACYREQ $i_request
     * @return \Axess\Dci4Wtp\getDayOccupacy
     */
    public function setI_request($i_request)
    {
      $this->i_request = $i_request;
      return $this;
    }

}
