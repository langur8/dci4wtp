<?php

namespace Axess\Dci4Wtp;

class D4WTPDSGVOFLAGS
{

    /**
     * @var float $BMARKETING
     */
    protected $BMARKETING = null;

    /**
     * @var float $BRETRANSMISSION3RDPARTY
     */
    protected $BRETRANSMISSION3RDPARTY = null;

    /**
     * @var float $BSTORAGE
     */
    protected $BSTORAGE = null;

    /**
     * @var float $BTRANSACTRETRANSMISSION
     */
    protected $BTRANSACTRETRANSMISSION = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBMARKETING()
    {
      return $this->BMARKETING;
    }

    /**
     * @param float $BMARKETING
     * @return \Axess\Dci4Wtp\D4WTPDSGVOFLAGS
     */
    public function setBMARKETING($BMARKETING)
    {
      $this->BMARKETING = $BMARKETING;
      return $this;
    }

    /**
     * @return float
     */
    public function getBRETRANSMISSION3RDPARTY()
    {
      return $this->BRETRANSMISSION3RDPARTY;
    }

    /**
     * @param float $BRETRANSMISSION3RDPARTY
     * @return \Axess\Dci4Wtp\D4WTPDSGVOFLAGS
     */
    public function setBRETRANSMISSION3RDPARTY($BRETRANSMISSION3RDPARTY)
    {
      $this->BRETRANSMISSION3RDPARTY = $BRETRANSMISSION3RDPARTY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBSTORAGE()
    {
      return $this->BSTORAGE;
    }

    /**
     * @param float $BSTORAGE
     * @return \Axess\Dci4Wtp\D4WTPDSGVOFLAGS
     */
    public function setBSTORAGE($BSTORAGE)
    {
      $this->BSTORAGE = $BSTORAGE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBTRANSACTRETRANSMISSION()
    {
      return $this->BTRANSACTRETRANSMISSION;
    }

    /**
     * @param float $BTRANSACTRETRANSMISSION
     * @return \Axess\Dci4Wtp\D4WTPDSGVOFLAGS
     */
    public function setBTRANSACTRETRANSMISSION($BTRANSACTRETRANSMISSION)
    {
      $this->BTRANSACTRETRANSMISSION = $BTRANSACTRETRANSMISSION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPDSGVOFLAGS
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPDSGVOFLAGS
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPDSGVOFLAGS
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

}
