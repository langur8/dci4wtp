<?php

namespace Axess\Dci4Wtp;

class getACPXImageResponse
{

    /**
     * @var D4WTPACPXIMAGERESULT $getACPXImageResult
     */
    protected $getACPXImageResult = null;

    /**
     * @param D4WTPACPXIMAGERESULT $getACPXImageResult
     */
    public function __construct($getACPXImageResult)
    {
      $this->getACPXImageResult = $getACPXImageResult;
    }

    /**
     * @return D4WTPACPXIMAGERESULT
     */
    public function getGetACPXImageResult()
    {
      return $this->getACPXImageResult;
    }

    /**
     * @param D4WTPACPXIMAGERESULT $getACPXImageResult
     * @return \Axess\Dci4Wtp\getACPXImageResponse
     */
    public function setGetACPXImageResult($getACPXImageResult)
    {
      $this->getACPXImageResult = $getACPXImageResult;
      return $this;
    }

}
