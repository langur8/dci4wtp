<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPUSERROLES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPUSERROLES[] $D4WTPUSERROLES
     */
    protected $D4WTPUSERROLES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPUSERROLES[]
     */
    public function getD4WTPUSERROLES()
    {
      return $this->D4WTPUSERROLES;
    }

    /**
     * @param D4WTPUSERROLES[] $D4WTPUSERROLES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPUSERROLES
     */
    public function setD4WTPUSERROLES(array $D4WTPUSERROLES = null)
    {
      $this->D4WTPUSERROLES = $D4WTPUSERROLES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPUSERROLES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPUSERROLES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPUSERROLES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPUSERROLES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPUSERROLES[] = $value;
      } else {
        $this->D4WTPUSERROLES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPUSERROLES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPUSERROLES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPUSERROLES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPUSERROLES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPUSERROLES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPUSERROLES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPUSERROLES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPUSERROLES);
    }

}
