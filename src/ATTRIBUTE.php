<?php

namespace Axess\Dci4Wtp;

class ATTRIBUTE
{

    /**
     * @var float $BVALUE
     */
    protected $BVALUE = null;

    /**
     * @var \DateTime $DTVALUE
     */
    protected $DTVALUE = null;

    /**
     * @var float $NATTRIBUTENR
     */
    protected $NATTRIBUTENR = null;

    /**
     * @var float $NVALUE
     */
    protected $NVALUE = null;

    /**
     * @var float $NVALUETYPE
     */
    protected $NVALUETYPE = null;

    /**
     * @var string $SZATTRIBUTENAME
     */
    protected $SZATTRIBUTENAME = null;

    /**
     * @var string $SZVALUE
     */
    protected $SZVALUE = null;

    /**
     * @var string $SZVALUEDATE
     */
    protected $SZVALUEDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBVALUE()
    {
      return $this->BVALUE;
    }

    /**
     * @param float $BVALUE
     * @return \Axess\Dci4Wtp\ATTRIBUTE
     */
    public function setBVALUE($BVALUE)
    {
      $this->BVALUE = $BVALUE;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDTVALUE()
    {
      if ($this->DTVALUE == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DTVALUE);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DTVALUE
     * @return \Axess\Dci4Wtp\ATTRIBUTE
     */
    public function setDTVALUE(\DateTime $DTVALUE = null)
    {
      if ($DTVALUE == null) {
       $this->DTVALUE = null;
      } else {
        $this->DTVALUE = $DTVALUE->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return float
     */
    public function getNATTRIBUTENR()
    {
      return $this->NATTRIBUTENR;
    }

    /**
     * @param float $NATTRIBUTENR
     * @return \Axess\Dci4Wtp\ATTRIBUTE
     */
    public function setNATTRIBUTENR($NATTRIBUTENR)
    {
      $this->NATTRIBUTENR = $NATTRIBUTENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVALUE()
    {
      return $this->NVALUE;
    }

    /**
     * @param float $NVALUE
     * @return \Axess\Dci4Wtp\ATTRIBUTE
     */
    public function setNVALUE($NVALUE)
    {
      $this->NVALUE = $NVALUE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVALUETYPE()
    {
      return $this->NVALUETYPE;
    }

    /**
     * @param float $NVALUETYPE
     * @return \Axess\Dci4Wtp\ATTRIBUTE
     */
    public function setNVALUETYPE($NVALUETYPE)
    {
      $this->NVALUETYPE = $NVALUETYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZATTRIBUTENAME()
    {
      return $this->SZATTRIBUTENAME;
    }

    /**
     * @param string $SZATTRIBUTENAME
     * @return \Axess\Dci4Wtp\ATTRIBUTE
     */
    public function setSZATTRIBUTENAME($SZATTRIBUTENAME)
    {
      $this->SZATTRIBUTENAME = $SZATTRIBUTENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALUE()
    {
      return $this->SZVALUE;
    }

    /**
     * @param string $SZVALUE
     * @return \Axess\Dci4Wtp\ATTRIBUTE
     */
    public function setSZVALUE($SZVALUE)
    {
      $this->SZVALUE = $SZVALUE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALUEDATE()
    {
      return $this->SZVALUEDATE;
    }

    /**
     * @param string $SZVALUEDATE
     * @return \Axess\Dci4Wtp\ATTRIBUTE
     */
    public function setSZVALUEDATE($SZVALUEDATE)
    {
      $this->SZVALUEDATE = $SZVALUEDATE;
      return $this;
    }

}
