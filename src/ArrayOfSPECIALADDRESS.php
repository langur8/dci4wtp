<?php

namespace Axess\Dci4Wtp;

class ArrayOfSPECIALADDRESS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var SPECIALADDRESS[] $SPECIALADDRESS
     */
    protected $SPECIALADDRESS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SPECIALADDRESS[]
     */
    public function getSPECIALADDRESS()
    {
      return $this->SPECIALADDRESS;
    }

    /**
     * @param SPECIALADDRESS[] $SPECIALADDRESS
     * @return \Axess\Dci4Wtp\ArrayOfSPECIALADDRESS
     */
    public function setSPECIALADDRESS(array $SPECIALADDRESS = null)
    {
      $this->SPECIALADDRESS = $SPECIALADDRESS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->SPECIALADDRESS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return SPECIALADDRESS
     */
    public function offsetGet($offset)
    {
      return $this->SPECIALADDRESS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param SPECIALADDRESS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->SPECIALADDRESS[] = $value;
      } else {
        $this->SPECIALADDRESS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->SPECIALADDRESS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return SPECIALADDRESS Return the current element
     */
    public function current()
    {
      return current($this->SPECIALADDRESS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->SPECIALADDRESS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->SPECIALADDRESS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->SPECIALADDRESS);
    }

    /**
     * Countable implementation
     *
     * @return SPECIALADDRESS Return count of elements
     */
    public function count()
    {
      return count($this->SPECIALADDRESS);
    }

}
