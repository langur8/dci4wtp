<?php

namespace Axess\Dci4Wtp;

class setDSGVOFlagsResponse
{

    /**
     * @var D4WTPRESULT $setDSGVOFlagsResult
     */
    protected $setDSGVOFlagsResult = null;

    /**
     * @param D4WTPRESULT $setDSGVOFlagsResult
     */
    public function __construct($setDSGVOFlagsResult)
    {
      $this->setDSGVOFlagsResult = $setDSGVOFlagsResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSetDSGVOFlagsResult()
    {
      return $this->setDSGVOFlagsResult;
    }

    /**
     * @param D4WTPRESULT $setDSGVOFlagsResult
     * @return \Axess\Dci4Wtp\setDSGVOFlagsResponse
     */
    public function setSetDSGVOFlagsResult($setDSGVOFlagsResult)
    {
      $this->setDSGVOFlagsResult = $setDSGVOFlagsResult;
      return $this;
    }

}
