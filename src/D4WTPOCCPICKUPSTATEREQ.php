<?php

namespace Axess\Dci4Wtp;

class D4WTPOCCPICKUPSTATEREQ
{

    /**
     * @var float $NPRODUCTIONSTATE
     */
    protected $NPRODUCTIONSTATE = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZDATACARRIERSERIALNO
     */
    protected $SZDATACARRIERSERIALNO = null;

    /**
     * @var string $SZPRODUCTIONSTATE
     */
    protected $SZPRODUCTIONSTATE = null;

    /**
     * @var string $SZTICKETITEMID
     */
    protected $SZTICKETITEMID = null;

    /**
     * @var string $SZTRANSACTIONID
     */
    protected $SZTRANSACTIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPRODUCTIONSTATE()
    {
      return $this->NPRODUCTIONSTATE;
    }

    /**
     * @param float $NPRODUCTIONSTATE
     * @return \Axess\Dci4Wtp\D4WTPOCCPICKUPSTATEREQ
     */
    public function setNPRODUCTIONSTATE($NPRODUCTIONSTATE)
    {
      $this->NPRODUCTIONSTATE = $NPRODUCTIONSTATE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPOCCPICKUPSTATEREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATACARRIERSERIALNO()
    {
      return $this->SZDATACARRIERSERIALNO;
    }

    /**
     * @param string $SZDATACARRIERSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPOCCPICKUPSTATEREQ
     */
    public function setSZDATACARRIERSERIALNO($SZDATACARRIERSERIALNO)
    {
      $this->SZDATACARRIERSERIALNO = $SZDATACARRIERSERIALNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPRODUCTIONSTATE()
    {
      return $this->SZPRODUCTIONSTATE;
    }

    /**
     * @param string $SZPRODUCTIONSTATE
     * @return \Axess\Dci4Wtp\D4WTPOCCPICKUPSTATEREQ
     */
    public function setSZPRODUCTIONSTATE($SZPRODUCTIONSTATE)
    {
      $this->SZPRODUCTIONSTATE = $SZPRODUCTIONSTATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETITEMID()
    {
      return $this->SZTICKETITEMID;
    }

    /**
     * @param string $SZTICKETITEMID
     * @return \Axess\Dci4Wtp\D4WTPOCCPICKUPSTATEREQ
     */
    public function setSZTICKETITEMID($SZTICKETITEMID)
    {
      $this->SZTICKETITEMID = $SZTICKETITEMID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTRANSACTIONID()
    {
      return $this->SZTRANSACTIONID;
    }

    /**
     * @param string $SZTRANSACTIONID
     * @return \Axess\Dci4Wtp\D4WTPOCCPICKUPSTATEREQ
     */
    public function setSZTRANSACTIONID($SZTRANSACTIONID)
    {
      $this->SZTRANSACTIONID = $SZTRANSACTIONID;
      return $this;
    }

}
