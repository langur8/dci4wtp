<?php

namespace Axess\Dci4Wtp;

class D4WTPPRODSHOPPOSPREPAIDREQ
{

    /**
     * @var float $NINDEXNO
     */
    protected $NINDEXNO = null;

    /**
     * @var float $NPOSITIONNO
     */
    protected $NPOSITIONNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var float $NSHOPPINGCARTPOSNO
     */
    protected $NSHOPPINGCARTPOSNO = null;

    /**
     * @var float $NSHOPPINGCARTPROJNO
     */
    protected $NSHOPPINGCARTPROJNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNINDEXNO()
    {
      return $this->NINDEXNO;
    }

    /**
     * @param float $NINDEXNO
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ
     */
    public function setNINDEXNO($NINDEXNO)
    {
      $this->NINDEXNO = $NINDEXNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSITIONNO()
    {
      return $this->NPOSITIONNO;
    }

    /**
     * @param float $NPOSITIONNO
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ
     */
    public function setNPOSITIONNO($NPOSITIONNO)
    {
      $this->NPOSITIONNO = $NPOSITIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPOSNO()
    {
      return $this->NSHOPPINGCARTPOSNO;
    }

    /**
     * @param float $NSHOPPINGCARTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ
     */
    public function setNSHOPPINGCARTPOSNO($NSHOPPINGCARTPOSNO)
    {
      $this->NSHOPPINGCARTPOSNO = $NSHOPPINGCARTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPROJNO()
    {
      return $this->NSHOPPINGCARTPROJNO;
    }

    /**
     * @param float $NSHOPPINGCARTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ
     */
    public function setNSHOPPINGCARTPROJNO($NSHOPPINGCARTPROJNO)
    {
      $this->NSHOPPINGCARTPROJNO = $NSHOPPINGCARTPROJNO;
      return $this;
    }

}
