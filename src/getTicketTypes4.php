<?php

namespace Axess\Dci4Wtp;

class getTicketTypes4
{

    /**
     * @var D4WTPTICKETTYPEREQUEST $i_ctTicketTypeReq
     */
    protected $i_ctTicketTypeReq = null;

    /**
     * @param D4WTPTICKETTYPEREQUEST $i_ctTicketTypeReq
     */
    public function __construct($i_ctTicketTypeReq)
    {
      $this->i_ctTicketTypeReq = $i_ctTicketTypeReq;
    }

    /**
     * @return D4WTPTICKETTYPEREQUEST
     */
    public function getI_ctTicketTypeReq()
    {
      return $this->i_ctTicketTypeReq;
    }

    /**
     * @param D4WTPTICKETTYPEREQUEST $i_ctTicketTypeReq
     * @return \Axess\Dci4Wtp\getTicketTypes4
     */
    public function setI_ctTicketTypeReq($i_ctTicketTypeReq)
    {
      $this->i_ctTicketTypeReq = $i_ctTicketTypeReq;
      return $this;
    }

}
