<?php

namespace Axess\Dci4Wtp;

class StartCLICSReportResponse
{

    /**
     * @var D4WTPSTARTCLICSREPORTRES $StartCLICSReportResult
     */
    protected $StartCLICSReportResult = null;

    /**
     * @param D4WTPSTARTCLICSREPORTRES $StartCLICSReportResult
     */
    public function __construct($StartCLICSReportResult)
    {
      $this->StartCLICSReportResult = $StartCLICSReportResult;
    }

    /**
     * @return D4WTPSTARTCLICSREPORTRES
     */
    public function getStartCLICSReportResult()
    {
      return $this->StartCLICSReportResult;
    }

    /**
     * @param D4WTPSTARTCLICSREPORTRES $StartCLICSReportResult
     * @return \Axess\Dci4Wtp\StartCLICSReportResponse
     */
    public function setStartCLICSReportResult($StartCLICSReportResult)
    {
      $this->StartCLICSReportResult = $StartCLICSReportResult;
      return $this;
    }

}
