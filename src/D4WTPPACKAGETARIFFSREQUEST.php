<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGETARIFFSREQUEST
{

    /**
     * @var D4WTPPACKAGERESTRICTION $CTPACKAGERESTRICTION
     */
    protected $CTPACKAGERESTRICTION = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPACKAGERESTRICTION
     */
    public function getCTPACKAGERESTRICTION()
    {
      return $this->CTPACKAGERESTRICTION;
    }

    /**
     * @param D4WTPPACKAGERESTRICTION $CTPACKAGERESTRICTION
     * @return \Axess\Dci4Wtp\D4WTPPACKAGETARIFFSREQUEST
     */
    public function setCTPACKAGERESTRICTION($CTPACKAGERESTRICTION)
    {
      $this->CTPACKAGERESTRICTION = $CTPACKAGERESTRICTION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGETARIFFSREQUEST
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPPACKAGETARIFFSREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGETARIFFSREQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

}
