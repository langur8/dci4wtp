<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPGETRESERVATIONDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPGETRESERVATIONDATA[] $D4WTPGETRESERVATIONDATA
     */
    protected $D4WTPGETRESERVATIONDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPGETRESERVATIONDATA[]
     */
    public function getD4WTPGETRESERVATIONDATA()
    {
      return $this->D4WTPGETRESERVATIONDATA;
    }

    /**
     * @param D4WTPGETRESERVATIONDATA[] $D4WTPGETRESERVATIONDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPGETRESERVATIONDATA
     */
    public function setD4WTPGETRESERVATIONDATA(array $D4WTPGETRESERVATIONDATA = null)
    {
      $this->D4WTPGETRESERVATIONDATA = $D4WTPGETRESERVATIONDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPGETRESERVATIONDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPGETRESERVATIONDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPGETRESERVATIONDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPGETRESERVATIONDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPGETRESERVATIONDATA[] = $value;
      } else {
        $this->D4WTPGETRESERVATIONDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPGETRESERVATIONDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPGETRESERVATIONDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPGETRESERVATIONDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPGETRESERVATIONDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPGETRESERVATIONDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPGETRESERVATIONDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPGETRESERVATIONDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPGETRESERVATIONDATA);
    }

}
