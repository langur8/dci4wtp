<?php

namespace Axess\Dci4Wtp;

class D4WTPLOGINREQUEST
{

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    /**
     * @var string $SZLOGINID
     */
    protected $SZLOGINID = null;

    /**
     * @var string $SZLOGINMODE
     */
    protected $SZLOGINMODE = null;

    /**
     * @var string $SZPASSWORD
     */
    protected $SZPASSWORD = null;

    /**
     * @var string $SZSOAPPASSWORD
     */
    protected $SZSOAPPASSWORD = null;

    /**
     * @var string $SZSOAPUSERNAME
     */
    protected $SZSOAPUSERNAME = null;

    /**
     * @var string $SZUSERNAME
     */
    protected $SZUSERNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPLOGINREQUEST
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOGINID()
    {
      return $this->SZLOGINID;
    }

    /**
     * @param string $SZLOGINID
     * @return \Axess\Dci4Wtp\D4WTPLOGINREQUEST
     */
    public function setSZLOGINID($SZLOGINID)
    {
      $this->SZLOGINID = $SZLOGINID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOGINMODE()
    {
      return $this->SZLOGINMODE;
    }

    /**
     * @param string $SZLOGINMODE
     * @return \Axess\Dci4Wtp\D4WTPLOGINREQUEST
     */
    public function setSZLOGINMODE($SZLOGINMODE)
    {
      $this->SZLOGINMODE = $SZLOGINMODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPASSWORD()
    {
      return $this->SZPASSWORD;
    }

    /**
     * @param string $SZPASSWORD
     * @return \Axess\Dci4Wtp\D4WTPLOGINREQUEST
     */
    public function setSZPASSWORD($SZPASSWORD)
    {
      $this->SZPASSWORD = $SZPASSWORD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSOAPPASSWORD()
    {
      return $this->SZSOAPPASSWORD;
    }

    /**
     * @param string $SZSOAPPASSWORD
     * @return \Axess\Dci4Wtp\D4WTPLOGINREQUEST
     */
    public function setSZSOAPPASSWORD($SZSOAPPASSWORD)
    {
      $this->SZSOAPPASSWORD = $SZSOAPPASSWORD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSOAPUSERNAME()
    {
      return $this->SZSOAPUSERNAME;
    }

    /**
     * @param string $SZSOAPUSERNAME
     * @return \Axess\Dci4Wtp\D4WTPLOGINREQUEST
     */
    public function setSZSOAPUSERNAME($SZSOAPUSERNAME)
    {
      $this->SZSOAPUSERNAME = $SZSOAPUSERNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUSERNAME()
    {
      return $this->SZUSERNAME;
    }

    /**
     * @param string $SZUSERNAME
     * @return \Axess\Dci4Wtp\D4WTPLOGINREQUEST
     */
    public function setSZUSERNAME($SZUSERNAME)
    {
      $this->SZUSERNAME = $SZUSERNAME;
      return $this;
    }

}
