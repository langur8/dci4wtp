<?php

namespace Axess\Dci4Wtp;

class D4AXCOUNTSTEPREQ
{

    /**
     * @var float $NBANKNR
     */
    protected $NBANKNR = null;

    /**
     * @var float $NGESNR
     */
    protected $NGESNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNBANKNR()
    {
      return $this->NBANKNR;
    }

    /**
     * @param float $NBANKNR
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPREQ
     */
    public function setNBANKNR($NBANKNR)
    {
      $this->NBANKNR = $NBANKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNGESNR()
    {
      return $this->NGESNR;
    }

    /**
     * @param float $NGESNR
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPREQ
     */
    public function setNGESNR($NGESNR)
    {
      $this->NGESNR = $NGESNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPREQ
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4AXCOUNTSTEPREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
