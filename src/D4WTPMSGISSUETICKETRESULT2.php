<?php

namespace Axess\Dci4Wtp;

class D4WTPMSGISSUETICKETRESULT2
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZEXTORDERNUMBER
     */
    protected $SZEXTORDERNUMBER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETRESULT2
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETRESULT2
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTORDERNUMBER()
    {
      return $this->SZEXTORDERNUMBER;
    }

    /**
     * @param string $SZEXTORDERNUMBER
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETRESULT2
     */
    public function setSZEXTORDERNUMBER($SZEXTORDERNUMBER)
    {
      $this->SZEXTORDERNUMBER = $SZEXTORDERNUMBER;
      return $this;
    }

}
