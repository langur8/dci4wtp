<?php

namespace Axess\Dci4Wtp;

class checkWTPNoResponse
{

    /**
     * @var D4WTPCHECKWTPNORESULT $checkWTPNoResult
     */
    protected $checkWTPNoResult = null;

    /**
     * @param D4WTPCHECKWTPNORESULT $checkWTPNoResult
     */
    public function __construct($checkWTPNoResult)
    {
      $this->checkWTPNoResult = $checkWTPNoResult;
    }

    /**
     * @return D4WTPCHECKWTPNORESULT
     */
    public function getCheckWTPNoResult()
    {
      return $this->checkWTPNoResult;
    }

    /**
     * @param D4WTPCHECKWTPNORESULT $checkWTPNoResult
     * @return \Axess\Dci4Wtp\checkWTPNoResponse
     */
    public function setCheckWTPNoResult($checkWTPNoResult)
    {
      $this->checkWTPNoResult = $checkWTPNoResult;
      return $this;
    }

}
