<?php

namespace Axess\Dci4Wtp;

class D4WTPGETGRPRESERVATIONRESULT
{

    /**
     * @var ArrayOfD4WTPGETGRPRESERVATIONDATA $ACTGETGRPRESERVATIONDATA
     */
    protected $ACTGETGRPRESERVATIONDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPGETGRPRESERVATIONDATA
     */
    public function getACTGETGRPRESERVATIONDATA()
    {
      return $this->ACTGETGRPRESERVATIONDATA;
    }

    /**
     * @param ArrayOfD4WTPGETGRPRESERVATIONDATA $ACTGETGRPRESERVATIONDATA
     * @return \Axess\Dci4Wtp\D4WTPGETGRPRESERVATIONRESULT
     */
    public function setACTGETGRPRESERVATIONDATA($ACTGETGRPRESERVATIONDATA)
    {
      $this->ACTGETGRPRESERVATIONDATA = $ACTGETGRPRESERVATIONDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETGRPRESERVATIONRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETGRPRESERVATIONRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
