<?php

namespace Axess\Dci4Wtp;

class GetCLICSReportStatus
{

    /**
     * @var D4WTPCLICSREPORTSTATUSREQ $i_ctCLICSReportStatusReq
     */
    protected $i_ctCLICSReportStatusReq = null;

    /**
     * @param D4WTPCLICSREPORTSTATUSREQ $i_ctCLICSReportStatusReq
     */
    public function __construct($i_ctCLICSReportStatusReq)
    {
      $this->i_ctCLICSReportStatusReq = $i_ctCLICSReportStatusReq;
    }

    /**
     * @return D4WTPCLICSREPORTSTATUSREQ
     */
    public function getI_ctCLICSReportStatusReq()
    {
      return $this->i_ctCLICSReportStatusReq;
    }

    /**
     * @param D4WTPCLICSREPORTSTATUSREQ $i_ctCLICSReportStatusReq
     * @return \Axess\Dci4Wtp\GetCLICSReportStatus
     */
    public function setI_ctCLICSReportStatusReq($i_ctCLICSReportStatusReq)
    {
      $this->i_ctCLICSReportStatusReq = $i_ctCLICSReportStatusReq;
      return $this;
    }

}
