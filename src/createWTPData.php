<?php

namespace Axess\Dci4Wtp;

class createWTPData
{

    /**
     * @var D4WTPCREATEWTPDATAREQ $i_ctCreateWTPDataReq
     */
    protected $i_ctCreateWTPDataReq = null;

    /**
     * @param D4WTPCREATEWTPDATAREQ $i_ctCreateWTPDataReq
     */
    public function __construct($i_ctCreateWTPDataReq)
    {
      $this->i_ctCreateWTPDataReq = $i_ctCreateWTPDataReq;
    }

    /**
     * @return D4WTPCREATEWTPDATAREQ
     */
    public function getI_ctCreateWTPDataReq()
    {
      return $this->i_ctCreateWTPDataReq;
    }

    /**
     * @param D4WTPCREATEWTPDATAREQ $i_ctCreateWTPDataReq
     * @return \Axess\Dci4Wtp\createWTPData
     */
    public function setI_ctCreateWTPDataReq($i_ctCreateWTPDataReq)
    {
      $this->i_ctCreateWTPDataReq = $i_ctCreateWTPDataReq;
      return $this;
    }

}
