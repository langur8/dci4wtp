<?php

namespace Axess\Dci4Wtp;

class getTicketTypesResponse
{

    /**
     * @var D4WTPTICKETTYPERESULT $getTicketTypesResult
     */
    protected $getTicketTypesResult = null;

    /**
     * @param D4WTPTICKETTYPERESULT $getTicketTypesResult
     */
    public function __construct($getTicketTypesResult)
    {
      $this->getTicketTypesResult = $getTicketTypesResult;
    }

    /**
     * @return D4WTPTICKETTYPERESULT
     */
    public function getGetTicketTypesResult()
    {
      return $this->getTicketTypesResult;
    }

    /**
     * @param D4WTPTICKETTYPERESULT $getTicketTypesResult
     * @return \Axess\Dci4Wtp\getTicketTypesResponse
     */
    public function setGetTicketTypesResult($getTicketTypesResult)
    {
      $this->getTicketTypesResult = $getTicketTypesResult;
      return $this;
    }

}
