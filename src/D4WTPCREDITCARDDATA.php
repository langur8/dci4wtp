<?php

namespace Axess\Dci4Wtp;

class D4WTPCREDITCARDDATA
{

    /**
     * @var string $SZCCNO
     */
    protected $SZCCNO = null;

    /**
     * @var string $SZCCOWNER
     */
    protected $SZCCOWNER = null;

    /**
     * @var string $SZCCTYPE
     */
    protected $SZCCTYPE = null;

    /**
     * @var string $SZEXPIRATIONDATE
     */
    protected $SZEXPIRATIONDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZCCNO()
    {
      return $this->SZCCNO;
    }

    /**
     * @param string $SZCCNO
     * @return \Axess\Dci4Wtp\D4WTPCREDITCARDDATA
     */
    public function setSZCCNO($SZCCNO)
    {
      $this->SZCCNO = $SZCCNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCCOWNER()
    {
      return $this->SZCCOWNER;
    }

    /**
     * @param string $SZCCOWNER
     * @return \Axess\Dci4Wtp\D4WTPCREDITCARDDATA
     */
    public function setSZCCOWNER($SZCCOWNER)
    {
      $this->SZCCOWNER = $SZCCOWNER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCCTYPE()
    {
      return $this->SZCCTYPE;
    }

    /**
     * @param string $SZCCTYPE
     * @return \Axess\Dci4Wtp\D4WTPCREDITCARDDATA
     */
    public function setSZCCTYPE($SZCCTYPE)
    {
      $this->SZCCTYPE = $SZCCTYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXPIRATIONDATE()
    {
      return $this->SZEXPIRATIONDATE;
    }

    /**
     * @param string $SZEXPIRATIONDATE
     * @return \Axess\Dci4Wtp\D4WTPCREDITCARDDATA
     */
    public function setSZEXPIRATIONDATE($SZEXPIRATIONDATE)
    {
      $this->SZEXPIRATIONDATE = $SZEXPIRATIONDATE;
      return $this;
    }

}
