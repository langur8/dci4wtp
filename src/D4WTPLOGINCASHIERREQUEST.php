<?php

namespace Axess\Dci4Wtp;

class D4WTPLOGINCASHIERREQUEST
{

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZCASHIERPASSWORD
     */
    protected $SZCASHIERPASSWORD = null;

    /**
     * @var string $SZCASHIERUSERNAME
     */
    protected $SZCASHIERUSERNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPLOGINCASHIERREQUEST
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPLOGINCASHIERREQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPLOGINCASHIERREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPLOGINCASHIERREQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCASHIERPASSWORD()
    {
      return $this->SZCASHIERPASSWORD;
    }

    /**
     * @param string $SZCASHIERPASSWORD
     * @return \Axess\Dci4Wtp\D4WTPLOGINCASHIERREQUEST
     */
    public function setSZCASHIERPASSWORD($SZCASHIERPASSWORD)
    {
      $this->SZCASHIERPASSWORD = $SZCASHIERPASSWORD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCASHIERUSERNAME()
    {
      return $this->SZCASHIERUSERNAME;
    }

    /**
     * @param string $SZCASHIERUSERNAME
     * @return \Axess\Dci4Wtp\D4WTPLOGINCASHIERREQUEST
     */
    public function setSZCASHIERUSERNAME($SZCASHIERUSERNAME)
    {
      $this->SZCASHIERUSERNAME = $SZCASHIERUSERNAME;
      return $this;
    }

}
