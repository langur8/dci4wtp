<?php

namespace Axess\Dci4Wtp;

class login2Response
{

    /**
     * @var D4WTPLOGIN2RESULT $login2Result
     */
    protected $login2Result = null;

    /**
     * @param D4WTPLOGIN2RESULT $login2Result
     */
    public function __construct($login2Result)
    {
      $this->login2Result = $login2Result;
    }

    /**
     * @return D4WTPLOGIN2RESULT
     */
    public function getLogin2Result()
    {
      return $this->login2Result;
    }

    /**
     * @param D4WTPLOGIN2RESULT $login2Result
     * @return \Axess\Dci4Wtp\login2Response
     */
    public function setLogin2Result($login2Result)
    {
      $this->login2Result = $login2Result;
      return $this;
    }

}
