<?php

namespace Axess\Dci4Wtp;

class D4WTPPKGTARIFFLISTDAY9
{

    /**
     * @var ArrayOfD4WTPPACKAGELIST $ACTPACKAGELIST
     */
    protected $ACTPACKAGELIST = null;

    /**
     * @var D4WTPPACKAGEARTICLE2 $CTPACKAGEARTICLE
     */
    protected $CTPACKAGEARTICLE = null;

    /**
     * @var D4WTPPKGTARIFF6 $CTPKGTARIFF
     */
    protected $CTPKGTARIFF = null;

    /**
     * @var D4WTPRENTALITEMTARIFF3 $CTRENTALITEMTARIFF
     */
    protected $CTRENTALITEMTARIFF = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPACKAGELIST
     */
    public function getACTPACKAGELIST()
    {
      return $this->ACTPACKAGELIST;
    }

    /**
     * @param ArrayOfD4WTPPACKAGELIST $ACTPACKAGELIST
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY9
     */
    public function setACTPACKAGELIST($ACTPACKAGELIST)
    {
      $this->ACTPACKAGELIST = $ACTPACKAGELIST;
      return $this;
    }

    /**
     * @return D4WTPPACKAGEARTICLE2
     */
    public function getCTPACKAGEARTICLE()
    {
      return $this->CTPACKAGEARTICLE;
    }

    /**
     * @param D4WTPPACKAGEARTICLE2 $CTPACKAGEARTICLE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY9
     */
    public function setCTPACKAGEARTICLE($CTPACKAGEARTICLE)
    {
      $this->CTPACKAGEARTICLE = $CTPACKAGEARTICLE;
      return $this;
    }

    /**
     * @return D4WTPPKGTARIFF6
     */
    public function getCTPKGTARIFF()
    {
      return $this->CTPKGTARIFF;
    }

    /**
     * @param D4WTPPKGTARIFF6 $CTPKGTARIFF
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY9
     */
    public function setCTPKGTARIFF($CTPKGTARIFF)
    {
      $this->CTPKGTARIFF = $CTPKGTARIFF;
      return $this;
    }

    /**
     * @return D4WTPRENTALITEMTARIFF3
     */
    public function getCTRENTALITEMTARIFF()
    {
      return $this->CTRENTALITEMTARIFF;
    }

    /**
     * @param D4WTPRENTALITEMTARIFF3 $CTRENTALITEMTARIFF
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY9
     */
    public function setCTRENTALITEMTARIFF($CTRENTALITEMTARIFF)
    {
      $this->CTRENTALITEMTARIFF = $CTRENTALITEMTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY9
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTDAY9
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
