<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCONTNGNTTICKET implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCONTNGNTTICKET[] $D4WTPCONTNGNTTICKET
     */
    protected $D4WTPCONTNGNTTICKET = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCONTNGNTTICKET[]
     */
    public function getD4WTPCONTNGNTTICKET()
    {
      return $this->D4WTPCONTNGNTTICKET;
    }

    /**
     * @param D4WTPCONTNGNTTICKET[] $D4WTPCONTNGNTTICKET
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCONTNGNTTICKET
     */
    public function setD4WTPCONTNGNTTICKET(array $D4WTPCONTNGNTTICKET = null)
    {
      $this->D4WTPCONTNGNTTICKET = $D4WTPCONTNGNTTICKET;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCONTNGNTTICKET[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCONTNGNTTICKET
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCONTNGNTTICKET[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCONTNGNTTICKET $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCONTNGNTTICKET[] = $value;
      } else {
        $this->D4WTPCONTNGNTTICKET[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCONTNGNTTICKET[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCONTNGNTTICKET Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCONTNGNTTICKET);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCONTNGNTTICKET);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCONTNGNTTICKET);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCONTNGNTTICKET);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCONTNGNTTICKET Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCONTNGNTTICKET);
    }

}
