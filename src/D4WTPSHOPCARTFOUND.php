<?php

namespace Axess\Dci4Wtp;

class D4WTPSHOPCARTFOUND
{

    /**
     * @var float $NLOCKPOSNO
     */
    protected $NLOCKPOSNO = null;

    /**
     * @var float $NLOCKPROJNO
     */
    protected $NLOCKPROJNO = null;

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var float $NSHOPPINGCARTPOSNO
     */
    protected $NSHOPPINGCARTPOSNO = null;

    /**
     * @var float $NSHOPPINGCARTPROJNO
     */
    protected $NSHOPPINGCARTPROJNO = null;

    /**
     * @var float $NSTATUSNO
     */
    protected $NSTATUSNO = null;

    /**
     * @var string $SZDBSESSIONID
     */
    protected $SZDBSESSIONID = null;

    /**
     * @var string $SZLOCKUNTILDATE
     */
    protected $SZLOCKUNTILDATE = null;

    /**
     * @var string $SZSHOPPINGCARTCODE
     */
    protected $SZSHOPPINGCARTCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNLOCKPOSNO()
    {
      return $this->NLOCKPOSNO;
    }

    /**
     * @param float $NLOCKPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTFOUND
     */
    public function setNLOCKPOSNO($NLOCKPOSNO)
    {
      $this->NLOCKPOSNO = $NLOCKPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLOCKPROJNO()
    {
      return $this->NLOCKPROJNO;
    }

    /**
     * @param float $NLOCKPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTFOUND
     */
    public function setNLOCKPROJNO($NLOCKPROJNO)
    {
      $this->NLOCKPROJNO = $NLOCKPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTFOUND
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPOSNO()
    {
      return $this->NSHOPPINGCARTPOSNO;
    }

    /**
     * @param float $NSHOPPINGCARTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTFOUND
     */
    public function setNSHOPPINGCARTPOSNO($NSHOPPINGCARTPOSNO)
    {
      $this->NSHOPPINGCARTPOSNO = $NSHOPPINGCARTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPROJNO()
    {
      return $this->NSHOPPINGCARTPROJNO;
    }

    /**
     * @param float $NSHOPPINGCARTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTFOUND
     */
    public function setNSHOPPINGCARTPROJNO($NSHOPPINGCARTPROJNO)
    {
      $this->NSHOPPINGCARTPROJNO = $NSHOPPINGCARTPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTATUSNO()
    {
      return $this->NSTATUSNO;
    }

    /**
     * @param float $NSTATUSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTFOUND
     */
    public function setNSTATUSNO($NSTATUSNO)
    {
      $this->NSTATUSNO = $NSTATUSNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDBSESSIONID()
    {
      return $this->SZDBSESSIONID;
    }

    /**
     * @param string $SZDBSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTFOUND
     */
    public function setSZDBSESSIONID($SZDBSESSIONID)
    {
      $this->SZDBSESSIONID = $SZDBSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOCKUNTILDATE()
    {
      return $this->SZLOCKUNTILDATE;
    }

    /**
     * @param string $SZLOCKUNTILDATE
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTFOUND
     */
    public function setSZLOCKUNTILDATE($SZLOCKUNTILDATE)
    {
      $this->SZLOCKUNTILDATE = $SZLOCKUNTILDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHOPPINGCARTCODE()
    {
      return $this->SZSHOPPINGCARTCODE;
    }

    /**
     * @param string $SZSHOPPINGCARTCODE
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTFOUND
     */
    public function setSZSHOPPINGCARTCODE($SZSHOPPINGCARTCODE)
    {
      $this->SZSHOPPINGCARTCODE = $SZSHOPPINGCARTCODE;
      return $this;
    }

}
