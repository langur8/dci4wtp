<?php

namespace Axess\Dci4Wtp;

class getProducts
{

    /**
     * @var D4WTPPRODUCTREQUEST $i_ctProductReq
     */
    protected $i_ctProductReq = null;

    /**
     * @param D4WTPPRODUCTREQUEST $i_ctProductReq
     */
    public function __construct($i_ctProductReq)
    {
      $this->i_ctProductReq = $i_ctProductReq;
    }

    /**
     * @return D4WTPPRODUCTREQUEST
     */
    public function getI_ctProductReq()
    {
      return $this->i_ctProductReq;
    }

    /**
     * @param D4WTPPRODUCTREQUEST $i_ctProductReq
     * @return \Axess\Dci4Wtp\getProducts
     */
    public function setI_ctProductReq($i_ctProductReq)
    {
      $this->i_ctProductReq = $i_ctProductReq;
      return $this;
    }

}
