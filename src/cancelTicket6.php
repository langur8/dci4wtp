<?php

namespace Axess\Dci4Wtp;

class cancelTicket6
{

    /**
     * @var D4WTPCANCELTICKET6REQUEST $i_ctCancelTicket6Request
     */
    protected $i_ctCancelTicket6Request = null;

    /**
     * @param D4WTPCANCELTICKET6REQUEST $i_ctCancelTicket6Request
     */
    public function __construct($i_ctCancelTicket6Request)
    {
      $this->i_ctCancelTicket6Request = $i_ctCancelTicket6Request;
    }

    /**
     * @return D4WTPCANCELTICKET6REQUEST
     */
    public function getI_ctCancelTicket6Request()
    {
      return $this->i_ctCancelTicket6Request;
    }

    /**
     * @param D4WTPCANCELTICKET6REQUEST $i_ctCancelTicket6Request
     * @return \Axess\Dci4Wtp\cancelTicket6
     */
    public function setI_ctCancelTicket6Request($i_ctCancelTicket6Request)
    {
      $this->i_ctCancelTicket6Request = $i_ctCancelTicket6Request;
      return $this;
    }

}
