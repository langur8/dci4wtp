<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSHOPPINGCARTPOSRES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSHOPPINGCARTPOSRES[] $D4WTPSHOPPINGCARTPOSRES
     */
    protected $D4WTPSHOPPINGCARTPOSRES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSHOPPINGCARTPOSRES[]
     */
    public function getD4WTPSHOPPINGCARTPOSRES()
    {
      return $this->D4WTPSHOPPINGCARTPOSRES;
    }

    /**
     * @param D4WTPSHOPPINGCARTPOSRES[] $D4WTPSHOPPINGCARTPOSRES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSHOPPINGCARTPOSRES
     */
    public function setD4WTPSHOPPINGCARTPOSRES(array $D4WTPSHOPPINGCARTPOSRES = null)
    {
      $this->D4WTPSHOPPINGCARTPOSRES = $D4WTPSHOPPINGCARTPOSRES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSHOPPINGCARTPOSRES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSHOPPINGCARTPOSRES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSHOPPINGCARTPOSRES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSHOPPINGCARTPOSRES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSHOPPINGCARTPOSRES[] = $value;
      } else {
        $this->D4WTPSHOPPINGCARTPOSRES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSHOPPINGCARTPOSRES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSHOPPINGCARTPOSRES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSHOPPINGCARTPOSRES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSHOPPINGCARTPOSRES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSHOPPINGCARTPOSRES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSHOPPINGCARTPOSRES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSHOPPINGCARTPOSRES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSHOPPINGCARTPOSRES);
    }

}
