<?php

namespace Axess\Dci4Wtp;

class getPersRentalSalesResponse
{

    /**
     * @var D4WTPGETPERSRENTALSALESRES $getPersRentalSalesResult
     */
    protected $getPersRentalSalesResult = null;

    /**
     * @param D4WTPGETPERSRENTALSALESRES $getPersRentalSalesResult
     */
    public function __construct($getPersRentalSalesResult)
    {
      $this->getPersRentalSalesResult = $getPersRentalSalesResult;
    }

    /**
     * @return D4WTPGETPERSRENTALSALESRES
     */
    public function getGetPersRentalSalesResult()
    {
      return $this->getPersRentalSalesResult;
    }

    /**
     * @param D4WTPGETPERSRENTALSALESRES $getPersRentalSalesResult
     * @return \Axess\Dci4Wtp\getPersRentalSalesResponse
     */
    public function setGetPersRentalSalesResult($getPersRentalSalesResult)
    {
      $this->getPersRentalSalesResult = $getPersRentalSalesResult;
      return $this;
    }

}
