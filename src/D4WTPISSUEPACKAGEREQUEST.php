<?php

namespace Axess\Dci4Wtp;

class D4WTPISSUEPACKAGEREQUEST
{

    /**
     * @var float $BCREATEPHOTO
     */
    protected $BCREATEPHOTO = null;

    /**
     * @var float $BPERSONAL
     */
    protected $BPERSONAL = null;

    /**
     * @var D4WTPPACKAGEORDER $CTPACKAGEORDER
     */
    protected $CTPACKAGEORDER = null;

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZCODINGMODE
     */
    protected $SZCODINGMODE = null;

    /**
     * @var string $SZDRIVERTYPE
     */
    protected $SZDRIVERTYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBCREATEPHOTO()
    {
      return $this->BCREATEPHOTO;
    }

    /**
     * @param float $BCREATEPHOTO
     * @return \Axess\Dci4Wtp\D4WTPISSUEPACKAGEREQUEST
     */
    public function setBCREATEPHOTO($BCREATEPHOTO)
    {
      $this->BCREATEPHOTO = $BCREATEPHOTO;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPERSONAL()
    {
      return $this->BPERSONAL;
    }

    /**
     * @param float $BPERSONAL
     * @return \Axess\Dci4Wtp\D4WTPISSUEPACKAGEREQUEST
     */
    public function setBPERSONAL($BPERSONAL)
    {
      $this->BPERSONAL = $BPERSONAL;
      return $this;
    }

    /**
     * @return D4WTPPACKAGEORDER
     */
    public function getCTPACKAGEORDER()
    {
      return $this->CTPACKAGEORDER;
    }

    /**
     * @param D4WTPPACKAGEORDER $CTPACKAGEORDER
     * @return \Axess\Dci4Wtp\D4WTPISSUEPACKAGEREQUEST
     */
    public function setCTPACKAGEORDER($CTPACKAGEORDER)
    {
      $this->CTPACKAGEORDER = $CTPACKAGEORDER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPISSUEPACKAGEREQUEST
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPISSUEPACKAGEREQUEST
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPISSUEPACKAGEREQUEST
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPISSUEPACKAGEREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGMODE()
    {
      return $this->SZCODINGMODE;
    }

    /**
     * @param string $SZCODINGMODE
     * @return \Axess\Dci4Wtp\D4WTPISSUEPACKAGEREQUEST
     */
    public function setSZCODINGMODE($SZCODINGMODE)
    {
      $this->SZCODINGMODE = $SZCODINGMODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDRIVERTYPE()
    {
      return $this->SZDRIVERTYPE;
    }

    /**
     * @param string $SZDRIVERTYPE
     * @return \Axess\Dci4Wtp\D4WTPISSUEPACKAGEREQUEST
     */
    public function setSZDRIVERTYPE($SZDRIVERTYPE)
    {
      $this->SZDRIVERTYPE = $SZDRIVERTYPE;
      return $this;
    }

}
