<?php

namespace Axess\Dci4Wtp;

class addLicensePlateResponse
{

    /**
     * @var D4WTPADDLICENSEPLATERESULT $addLicensePlateResult
     */
    protected $addLicensePlateResult = null;

    /**
     * @param D4WTPADDLICENSEPLATERESULT $addLicensePlateResult
     */
    public function __construct($addLicensePlateResult)
    {
      $this->addLicensePlateResult = $addLicensePlateResult;
    }

    /**
     * @return D4WTPADDLICENSEPLATERESULT
     */
    public function getAddLicensePlateResult()
    {
      return $this->addLicensePlateResult;
    }

    /**
     * @param D4WTPADDLICENSEPLATERESULT $addLicensePlateResult
     * @return \Axess\Dci4Wtp\addLicensePlateResponse
     */
    public function setAddLicensePlateResult($addLicensePlateResult)
    {
      $this->addLicensePlateResult = $addLicensePlateResult;
      return $this;
    }

}
