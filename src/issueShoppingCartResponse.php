<?php

namespace Axess\Dci4Wtp;

class issueShoppingCartResponse
{

    /**
     * @var D4WTPISSUESHOPCARTRESULT $issueShoppingCartResult
     */
    protected $issueShoppingCartResult = null;

    /**
     * @param D4WTPISSUESHOPCARTRESULT $issueShoppingCartResult
     */
    public function __construct($issueShoppingCartResult)
    {
      $this->issueShoppingCartResult = $issueShoppingCartResult;
    }

    /**
     * @return D4WTPISSUESHOPCARTRESULT
     */
    public function getIssueShoppingCartResult()
    {
      return $this->issueShoppingCartResult;
    }

    /**
     * @param D4WTPISSUESHOPCARTRESULT $issueShoppingCartResult
     * @return \Axess\Dci4Wtp\issueShoppingCartResponse
     */
    public function setIssueShoppingCartResult($issueShoppingCartResult)
    {
      $this->issueShoppingCartResult = $issueShoppingCartResult;
      return $this;
    }

}
