<?php

namespace Axess\Dci4Wtp;

class getReaderTrans2Response
{

    /**
     * @var D4WTPGETREADERTRANS2RESULT $getReaderTrans2Result
     */
    protected $getReaderTrans2Result = null;

    /**
     * @param D4WTPGETREADERTRANS2RESULT $getReaderTrans2Result
     */
    public function __construct($getReaderTrans2Result)
    {
      $this->getReaderTrans2Result = $getReaderTrans2Result;
    }

    /**
     * @return D4WTPGETREADERTRANS2RESULT
     */
    public function getGetReaderTrans2Result()
    {
      return $this->getReaderTrans2Result;
    }

    /**
     * @param D4WTPGETREADERTRANS2RESULT $getReaderTrans2Result
     * @return \Axess\Dci4Wtp\getReaderTrans2Response
     */
    public function setGetReaderTrans2Result($getReaderTrans2Result)
    {
      $this->getReaderTrans2Result = $getReaderTrans2Result;
      return $this;
    }

}
