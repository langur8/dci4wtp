<?php

namespace Axess\Dci4Wtp;

class D4WTPSHOPPINGCARTPOSDETRES
{

    /**
     * @var ArrayOfD4WTPSHOPPINGCARTPOSARTRES $ACTSHOPPINGCARTPOSARTRES
     */
    protected $ACTSHOPPINGCARTPOSARTRES = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NLFDPOSNO
     */
    protected $NLFDPOSNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSHOPPINGCARTPOSARTRES
     */
    public function getACTSHOPPINGCARTPOSARTRES()
    {
      return $this->ACTSHOPPINGCARTPOSARTRES;
    }

    /**
     * @param ArrayOfD4WTPSHOPPINGCARTPOSARTRES $ACTSHOPPINGCARTPOSARTRES
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETRES
     */
    public function setACTSHOPPINGCARTPOSARTRES($ACTSHOPPINGCARTPOSARTRES)
    {
      $this->ACTSHOPPINGCARTPOSARTRES = $ACTSHOPPINGCARTPOSARTRES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDPOSNO()
    {
      return $this->NLFDPOSNO;
    }

    /**
     * @param float $NLFDPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETRES
     */
    public function setNLFDPOSNO($NLFDPOSNO)
    {
      $this->NLFDPOSNO = $NLFDPOSNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
