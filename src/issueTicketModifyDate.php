<?php

namespace Axess\Dci4Wtp;

class issueTicketModifyDate
{

    /**
     * @var D4WTPISSUEMODIFYREQ $i_ctIssueModifyReq
     */
    protected $i_ctIssueModifyReq = null;

    /**
     * @param D4WTPISSUEMODIFYREQ $i_ctIssueModifyReq
     */
    public function __construct($i_ctIssueModifyReq)
    {
      $this->i_ctIssueModifyReq = $i_ctIssueModifyReq;
    }

    /**
     * @return D4WTPISSUEMODIFYREQ
     */
    public function getI_ctIssueModifyReq()
    {
      return $this->i_ctIssueModifyReq;
    }

    /**
     * @param D4WTPISSUEMODIFYREQ $i_ctIssueModifyReq
     * @return \Axess\Dci4Wtp\issueTicketModifyDate
     */
    public function setI_ctIssueModifyReq($i_ctIssueModifyReq)
    {
      $this->i_ctIssueModifyReq = $i_ctIssueModifyReq;
      return $this;
    }

}
