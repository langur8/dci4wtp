<?php

namespace Axess\Dci4Wtp;

class D4WTPGROUPRESULT
{

    /**
     * @var float $NCOMPANYNR
     */
    protected $NCOMPANYNR = null;

    /**
     * @var float $NCOMPANYPOSNR
     */
    protected $NCOMPANYPOSNR = null;

    /**
     * @var float $NCOMPANYPROJNR
     */
    protected $NCOMPANYPROJNR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NLOGINWTPPROFILNR
     */
    protected $NLOGINWTPPROFILNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZLOGINWTPLOGINID
     */
    protected $SZLOGINWTPLOGINID = null;

    /**
     * @var string $SZLOGINWTPPASSWORD
     */
    protected $SZLOGINWTPPASSWORD = null;

    /**
     * @var string $SZLOGINWTPUSERNAME
     */
    protected $SZLOGINWTPUSERNAME = null;

    /**
     * @var string $SZLOGINXMLPWD
     */
    protected $SZLOGINXMLPWD = null;

    /**
     * @var string $SZLOGINXMLUSER
     */
    protected $SZLOGINXMLUSER = null;

    /**
     * @var string $SZNAME1
     */
    protected $SZNAME1 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCOMPANYNR()
    {
      return $this->NCOMPANYNR;
    }

    /**
     * @param float $NCOMPANYNR
     * @return \Axess\Dci4Wtp\D4WTPGROUPRESULT
     */
    public function setNCOMPANYNR($NCOMPANYNR)
    {
      $this->NCOMPANYNR = $NCOMPANYNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNR()
    {
      return $this->NCOMPANYPOSNR;
    }

    /**
     * @param float $NCOMPANYPOSNR
     * @return \Axess\Dci4Wtp\D4WTPGROUPRESULT
     */
    public function setNCOMPANYPOSNR($NCOMPANYPOSNR)
    {
      $this->NCOMPANYPOSNR = $NCOMPANYPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNR()
    {
      return $this->NCOMPANYPROJNR;
    }

    /**
     * @param float $NCOMPANYPROJNR
     * @return \Axess\Dci4Wtp\D4WTPGROUPRESULT
     */
    public function setNCOMPANYPROJNR($NCOMPANYPROJNR)
    {
      $this->NCOMPANYPROJNR = $NCOMPANYPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGROUPRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLOGINWTPPROFILNR()
    {
      return $this->NLOGINWTPPROFILNR;
    }

    /**
     * @param float $NLOGINWTPPROFILNR
     * @return \Axess\Dci4Wtp\D4WTPGROUPRESULT
     */
    public function setNLOGINWTPPROFILNR($NLOGINWTPPROFILNR)
    {
      $this->NLOGINWTPPROFILNR = $NLOGINWTPPROFILNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGROUPRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOGINWTPLOGINID()
    {
      return $this->SZLOGINWTPLOGINID;
    }

    /**
     * @param string $SZLOGINWTPLOGINID
     * @return \Axess\Dci4Wtp\D4WTPGROUPRESULT
     */
    public function setSZLOGINWTPLOGINID($SZLOGINWTPLOGINID)
    {
      $this->SZLOGINWTPLOGINID = $SZLOGINWTPLOGINID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOGINWTPPASSWORD()
    {
      return $this->SZLOGINWTPPASSWORD;
    }

    /**
     * @param string $SZLOGINWTPPASSWORD
     * @return \Axess\Dci4Wtp\D4WTPGROUPRESULT
     */
    public function setSZLOGINWTPPASSWORD($SZLOGINWTPPASSWORD)
    {
      $this->SZLOGINWTPPASSWORD = $SZLOGINWTPPASSWORD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOGINWTPUSERNAME()
    {
      return $this->SZLOGINWTPUSERNAME;
    }

    /**
     * @param string $SZLOGINWTPUSERNAME
     * @return \Axess\Dci4Wtp\D4WTPGROUPRESULT
     */
    public function setSZLOGINWTPUSERNAME($SZLOGINWTPUSERNAME)
    {
      $this->SZLOGINWTPUSERNAME = $SZLOGINWTPUSERNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOGINXMLPWD()
    {
      return $this->SZLOGINXMLPWD;
    }

    /**
     * @param string $SZLOGINXMLPWD
     * @return \Axess\Dci4Wtp\D4WTPGROUPRESULT
     */
    public function setSZLOGINXMLPWD($SZLOGINXMLPWD)
    {
      $this->SZLOGINXMLPWD = $SZLOGINXMLPWD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOGINXMLUSER()
    {
      return $this->SZLOGINXMLUSER;
    }

    /**
     * @param string $SZLOGINXMLUSER
     * @return \Axess\Dci4Wtp\D4WTPGROUPRESULT
     */
    public function setSZLOGINXMLUSER($SZLOGINXMLUSER)
    {
      $this->SZLOGINXMLUSER = $SZLOGINXMLUSER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME1()
    {
      return $this->SZNAME1;
    }

    /**
     * @param string $SZNAME1
     * @return \Axess\Dci4Wtp\D4WTPGROUPRESULT
     */
    public function setSZNAME1($SZNAME1)
    {
      $this->SZNAME1 = $SZNAME1;
      return $this;
    }

}
