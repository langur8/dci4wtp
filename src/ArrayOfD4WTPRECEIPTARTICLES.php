<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPRECEIPTARTICLES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPRECEIPTARTICLES[] $D4WTPRECEIPTARTICLES
     */
    protected $D4WTPRECEIPTARTICLES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPRECEIPTARTICLES[]
     */
    public function getD4WTPRECEIPTARTICLES()
    {
      return $this->D4WTPRECEIPTARTICLES;
    }

    /**
     * @param D4WTPRECEIPTARTICLES[] $D4WTPRECEIPTARTICLES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPRECEIPTARTICLES
     */
    public function setD4WTPRECEIPTARTICLES(array $D4WTPRECEIPTARTICLES = null)
    {
      $this->D4WTPRECEIPTARTICLES = $D4WTPRECEIPTARTICLES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPRECEIPTARTICLES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPRECEIPTARTICLES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPRECEIPTARTICLES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPRECEIPTARTICLES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPRECEIPTARTICLES[] = $value;
      } else {
        $this->D4WTPRECEIPTARTICLES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPRECEIPTARTICLES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPRECEIPTARTICLES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPRECEIPTARTICLES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPRECEIPTARTICLES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPRECEIPTARTICLES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPRECEIPTARTICLES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPRECEIPTARTICLES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPRECEIPTARTICLES);
    }

}
