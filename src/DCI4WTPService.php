<?php

namespace Axess\Dci4Wtp;

use dlouhy\Soap\Client;

class DCI4WTPService extends Client
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'getConfigFile' => 'Axess\\Dci4Wtp\\getConfigFile',
      'getConfigFileResponse' => 'Axess\\Dci4Wtp\\getConfigFileResponse',
      'getBOCVersion' => 'Axess\\Dci4Wtp\\getBOCVersion',
      'getBOCVersionResponse' => 'Axess\\Dci4Wtp\\getBOCVersionResponse',
      'getBOCUniversalTime' => 'Axess\\Dci4Wtp\\getBOCUniversalTime',
      'getBOCUniversalTimeResponse' => 'Axess\\Dci4Wtp\\getBOCUniversalTimeResponse',
      'axessForceGarbageColletor' => 'Axess\\Dci4Wtp\\axessForceGarbageColletor',
      'axessForceGarbageColletorResponse' => 'Axess\\Dci4Wtp\\axessForceGarbageColletorResponse',
      'CheckOneTimePwd' => 'Axess\\Dci4Wtp\\CheckOneTimePwd',
      'CheckOneTimePwdResponse' => 'Axess\\Dci4Wtp\\CheckOneTimePwdResponse',
      'checkPromoCodes' => 'Axess\\Dci4Wtp\\checkPromoCodes',
      'checkPromoCodesResponse' => 'Axess\\Dci4Wtp\\checkPromoCodesResponse',
      'editOrderHeader' => 'Axess\\Dci4Wtp\\editOrderHeader',
      'editOrderHeaderResponse' => 'Axess\\Dci4Wtp\\editOrderHeaderResponse',
      'editOrderPositions' => 'Axess\\Dci4Wtp\\editOrderPositions',
      'editOrderPositionsResponse' => 'Axess\\Dci4Wtp\\editOrderPositionsResponse',
      'getChipcardEntries' => 'Axess\\Dci4Wtp\\getChipcardEntries',
      'getChipcardEntriesResponse' => 'Axess\\Dci4Wtp\\getChipcardEntriesResponse',
      'getAdditionalCashiers' => 'Axess\\Dci4Wtp\\getAdditionalCashiers',
      'getAdditionalCashiersResponse' => 'Axess\\Dci4Wtp\\getAdditionalCashiersResponse',
      'getSalesTransactions' => 'Axess\\Dci4Wtp\\getSalesTransactions',
      'getSalesTransactionsResponse' => 'Axess\\Dci4Wtp\\getSalesTransactionsResponse',
      'getRentalItemTariff' => 'Axess\\Dci4Wtp\\getRentalItemTariff',
      'getRentalItemTariffResponse' => 'Axess\\Dci4Wtp\\getRentalItemTariffResponse',
      'getRentalItemTariff2' => 'Axess\\Dci4Wtp\\getRentalItemTariff2',
      'getRentalItemTariff2Response' => 'Axess\\Dci4Wtp\\getRentalItemTariff2Response',
      'GetRentalItemTariff3' => 'Axess\\Dci4Wtp\\GetRentalItemTariff3',
      'GetRentalItemTariff3Response' => 'Axess\\Dci4Wtp\\GetRentalItemTariff3Response',
      'GetRentalItemTariff4' => 'Axess\\Dci4Wtp\\GetRentalItemTariff4',
      'GetRentalItemTariff4Response' => 'Axess\\Dci4Wtp\\GetRentalItemTariff4Response',
      'getPersRentalSales' => 'Axess\\Dci4Wtp\\getPersRentalSales',
      'getPersRentalSalesResponse' => 'Axess\\Dci4Wtp\\getPersRentalSalesResponse',
      'getWebRentalByProperty' => 'Axess\\Dci4Wtp\\getWebRentalByProperty',
      'getWebRentalByPropertyResponse' => 'Axess\\Dci4Wtp\\getWebRentalByPropertyResponse',
      'findShoppingCarts' => 'Axess\\Dci4Wtp\\findShoppingCarts',
      'findShoppingCartsResponse' => 'Axess\\Dci4Wtp\\findShoppingCartsResponse',
      'lockShoppingCart' => 'Axess\\Dci4Wtp\\lockShoppingCart',
      'lockShoppingCartResponse' => 'Axess\\Dci4Wtp\\lockShoppingCartResponse',
      'createExtOrderNr' => 'Axess\\Dci4Wtp\\createExtOrderNr',
      'createExtOrderNrResponse' => 'Axess\\Dci4Wtp\\createExtOrderNrResponse',
      'issueShoppingCart' => 'Axess\\Dci4Wtp\\issueShoppingCart',
      'issueShoppingCartResponse' => 'Axess\\Dci4Wtp\\issueShoppingCartResponse',
      'assignPersToCompany' => 'Axess\\Dci4Wtp\\assignPersToCompany',
      'assignPersToCompanyResponse' => 'Axess\\Dci4Wtp\\assignPersToCompanyResponse',
      'getPersFromCompany' => 'Axess\\Dci4Wtp\\getPersFromCompany',
      'getPersFromCompanyResponse' => 'Axess\\Dci4Wtp\\getPersFromCompanyResponse',
      'manageWorkOrder' => 'Axess\\Dci4Wtp\\manageWorkOrder',
      'manageWorkOrderResponse' => 'Axess\\Dci4Wtp\\manageWorkOrderResponse',
      'getWorkOrders' => 'Axess\\Dci4Wtp\\getWorkOrders',
      'getWorkOrdersResponse' => 'Axess\\Dci4Wtp\\getWorkOrdersResponse',
      'refundTicket' => 'Axess\\Dci4Wtp\\refundTicket',
      'refundTicketResponse' => 'Axess\\Dci4Wtp\\refundTicketResponse',
      'refundTicket2' => 'Axess\\Dci4Wtp\\refundTicket2',
      'refundTicket2Response' => 'Axess\\Dci4Wtp\\refundTicket2Response',
      'msgRefundTicket' => 'Axess\\Dci4Wtp\\msgRefundTicket',
      'msgRefundTicketResponse' => 'Axess\\Dci4Wtp\\msgRefundTicketResponse',
      'getTravelGroupList' => 'Axess\\Dci4Wtp\\getTravelGroupList',
      'getTravelGroupListResponse' => 'Axess\\Dci4Wtp\\getTravelGroupListResponse',
      'getTravelGroupList2' => 'Axess\\Dci4Wtp\\getTravelGroupList2',
      'getTravelGroupList2Response' => 'Axess\\Dci4Wtp\\getTravelGroupList2Response',
      'getGroupSubContingent' => 'Axess\\Dci4Wtp\\getGroupSubContingent',
      'getGroupSubContingentResponse' => 'Axess\\Dci4Wtp\\getGroupSubContingentResponse',
      'getGroupSubContingent2' => 'Axess\\Dci4Wtp\\getGroupSubContingent2',
      'getGroupSubContingent2Response' => 'Axess\\Dci4Wtp\\getGroupSubContingent2Response',
      'getGroupSubContingent3' => 'Axess\\Dci4Wtp\\getGroupSubContingent3',
      'getGroupSubContingent3Response' => 'Axess\\Dci4Wtp\\getGroupSubContingent3Response',
      'checkBonusPoints' => 'Axess\\Dci4Wtp\\checkBonusPoints',
      'checkBonusPointsResponse' => 'Axess\\Dci4Wtp\\checkBonusPointsResponse',
      'confirmBonusPointsRedeem' => 'Axess\\Dci4Wtp\\confirmBonusPointsRedeem',
      'confirmBonusPointsRedeemResponse' => 'Axess\\Dci4Wtp\\confirmBonusPointsRedeemResponse',
      'getDSGVOFlags' => 'Axess\\Dci4Wtp\\getDSGVOFlags',
      'getDSGVOFlagsResponse' => 'Axess\\Dci4Wtp\\getDSGVOFlagsResponse',
      'setDSGVOFlags' => 'Axess\\Dci4Wtp\\setDSGVOFlags',
      'setDSGVOFlagsResponse' => 'Axess\\Dci4Wtp\\setDSGVOFlagsResponse',
      'uploadPrivacyDoc' => 'Axess\\Dci4Wtp\\uploadPrivacyDoc',
      'uploadPrivacyDocResponse' => 'Axess\\Dci4Wtp\\uploadPrivacyDocResponse',
      'createWTPData' => 'Axess\\Dci4Wtp\\createWTPData',
      'createWTPDataResponse' => 'Axess\\Dci4Wtp\\createWTPDataResponse',
      'sendWTPDataOCC' => 'Axess\\Dci4Wtp\\sendWTPDataOCC',
      'sendWTPDataOCCResponse' => 'Axess\\Dci4Wtp\\sendWTPDataOCCResponse',
      'addDTL4FamilyMember' => 'Axess\\Dci4Wtp\\addDTL4FamilyMember',
      'addDTL4FamilyMemberResponse' => 'Axess\\Dci4Wtp\\addDTL4FamilyMemberResponse',
      'getDTL4FamilyExtendTariffs' => 'Axess\\Dci4Wtp\\getDTL4FamilyExtendTariffs',
      'getDTL4FamilyExtendTariffsResponse' => 'Axess\\Dci4Wtp\\getDTL4FamilyExtendTariffsResponse',
      'addDTL4FamilyDays' => 'Axess\\Dci4Wtp\\addDTL4FamilyDays',
      'addDTL4FamilyDaysResponse' => 'Axess\\Dci4Wtp\\addDTL4FamilyDaysResponse',
      'getDTL4FamilyData' => 'Axess\\Dci4Wtp\\getDTL4FamilyData',
      'getDTL4FamilyDataResponse' => 'Axess\\Dci4Wtp\\getDTL4FamilyDataResponse',
      'getBlankTypeList' => 'Axess\\Dci4Wtp\\getBlankTypeList',
      'getBlankTypeListResponse' => 'Axess\\Dci4Wtp\\getBlankTypeListResponse',
      'getEMoneySteps' => 'Axess\\Dci4Wtp\\getEMoneySteps',
      'getEMoneyStepsResponse' => 'Axess\\Dci4Wtp\\getEMoneyStepsResponse',
      'getEmoneyAccountDetails' => 'Axess\\Dci4Wtp\\getEmoneyAccountDetails',
      'getEmoneyAccountDetailsResponse' => 'Axess\\Dci4Wtp\\getEmoneyAccountDetailsResponse',
      'changeEmoneyAccount' => 'Axess\\Dci4Wtp\\changeEmoneyAccount',
      'changeEmoneyAccountResponse' => 'Axess\\Dci4Wtp\\changeEmoneyAccountResponse',
      'changeEmoneyAccount2' => 'Axess\\Dci4Wtp\\changeEmoneyAccount2',
      'changeEmoneyAccount2Response' => 'Axess\\Dci4Wtp\\changeEmoneyAccount2Response',
      'eMoneyPayInOut' => 'Axess\\Dci4Wtp\\eMoneyPayInOut',
      'eMoneyPayInOutResponse' => 'Axess\\Dci4Wtp\\eMoneyPayInOutResponse',
      'eMoneyPayTransaction' => 'Axess\\Dci4Wtp\\eMoneyPayTransaction',
      'eMoneyPayTransactionResponse' => 'Axess\\Dci4Wtp\\eMoneyPayTransactionResponse',
      'eMoneyPayTransaction2' => 'Axess\\Dci4Wtp\\eMoneyPayTransaction2',
      'eMoneyPayTransaction2Response' => 'Axess\\Dci4Wtp\\eMoneyPayTransaction2Response',
      'eMoneyBalanceHistory' => 'Axess\\Dci4Wtp\\eMoneyBalanceHistory',
      'eMoneyBalanceHistoryResponse' => 'Axess\\Dci4Wtp\\eMoneyBalanceHistoryResponse',
      'getAttributes' => 'Axess\\Dci4Wtp\\getAttributes',
      'getAttributesResponse' => 'Axess\\Dci4Wtp\\getAttributesResponse',
      'setAttributes' => 'Axess\\Dci4Wtp\\setAttributes',
      'setAttributesResponse' => 'Axess\\Dci4Wtp\\setAttributesResponse',
      'registerSwissPassSale' => 'Axess\\Dci4Wtp\\registerSwissPassSale',
      'registerSwissPassSaleResponse' => 'Axess\\Dci4Wtp\\registerSwissPassSaleResponse',
      'isSwissPassSoldonDay' => 'Axess\\Dci4Wtp\\isSwissPassSoldonDay',
      'isSwissPassSoldonDayResponse' => 'Axess\\Dci4Wtp\\isSwissPassSoldonDayResponse',
      'saveVoucherIDWTPProdsatz' => 'Axess\\Dci4Wtp\\saveVoucherIDWTPProdsatz',
      'saveVoucherIDWTPProdsatzResponse' => 'Axess\\Dci4Wtp\\saveVoucherIDWTPProdsatzResponse',
      'getFirstValidDay' => 'Axess\\Dci4Wtp\\getFirstValidDay',
      'getFirstValidDayResponse' => 'Axess\\Dci4Wtp\\getFirstValidDayResponse',
      'setCompensationTicket' => 'Axess\\Dci4Wtp\\setCompensationTicket',
      'setCompensationTicketResponse' => 'Axess\\Dci4Wtp\\setCompensationTicketResponse',
      'getCompensationTicket' => 'Axess\\Dci4Wtp\\getCompensationTicket',
      'getCompensationTicketResponse' => 'Axess\\Dci4Wtp\\getCompensationTicketResponse',
      'getContingentOccupacy' => 'Axess\\Dci4Wtp\\getContingentOccupacy',
      'getContingentOccupacyResponse' => 'Axess\\Dci4Wtp\\getContingentOccupacyResponse',
      'getContingentOccupacy2' => 'Axess\\Dci4Wtp\\getContingentOccupacy2',
      'getContingentOccupacy2Response' => 'Axess\\Dci4Wtp\\getContingentOccupacy2Response',
      'getDayOccupacy' => 'Axess\\Dci4Wtp\\getDayOccupacy',
      'getDayOccupacyResponse' => 'Axess\\Dci4Wtp\\getDayOccupacyResponse',
      'getDayOccupacy2' => 'Axess\\Dci4Wtp\\getDayOccupacy2',
      'getDayOccupacy2Response' => 'Axess\\Dci4Wtp\\getDayOccupacy2Response',
      'getRMProfiles' => 'Axess\\Dci4Wtp\\getRMProfiles',
      'getRMProfilesResponse' => 'Axess\\Dci4Wtp\\getRMProfilesResponse',
      'getEligibilityDiscounts' => 'Axess\\Dci4Wtp\\getEligibilityDiscounts',
      'getEligibilityDiscountsResponse' => 'Axess\\Dci4Wtp\\getEligibilityDiscountsResponse',
      'getCompanyBalance' => 'Axess\\Dci4Wtp\\getCompanyBalance',
      'getCompanyBalanceResponse' => 'Axess\\Dci4Wtp\\getCompanyBalanceResponse',
      'getProcessedTicketData' => 'Axess\\Dci4Wtp\\getProcessedTicketData',
      'getProcessedTicketDataResponse' => 'Axess\\Dci4Wtp\\getProcessedTicketDataResponse',
      'replaceTicket' => 'Axess\\Dci4Wtp\\replaceTicket',
      'replaceTicketResponse' => 'Axess\\Dci4Wtp\\replaceTicketResponse',
      'msgReplaceTicket' => 'Axess\\Dci4Wtp\\msgReplaceTicket',
      'msgReplaceTicketResponse' => 'Axess\\Dci4Wtp\\msgReplaceTicketResponse',
      'changePersData' => 'Axess\\Dci4Wtp\\changePersData',
      'changePersDataResponse' => 'Axess\\Dci4Wtp\\changePersDataResponse',
      'changePersData2' => 'Axess\\Dci4Wtp\\changePersData2',
      'changePersData2Response' => 'Axess\\Dci4Wtp\\changePersData2Response',
      'changePersData3' => 'Axess\\Dci4Wtp\\changePersData3',
      'changePersData3Response' => 'Axess\\Dci4Wtp\\changePersData3Response',
      'changePersData4' => 'Axess\\Dci4Wtp\\changePersData4',
      'changePersData4Response' => 'Axess\\Dci4Wtp\\changePersData4Response',
      'readAuthorization' => 'Axess\\Dci4Wtp\\readAuthorization',
      'readAuthorizationResponse' => 'Axess\\Dci4Wtp\\readAuthorizationResponse',
      'createWTP64Bit' => 'Axess\\Dci4Wtp\\createWTP64Bit',
      'createWTP64BitResponse' => 'Axess\\Dci4Wtp\\createWTP64BitResponse',
      'blockTicketBySerial' => 'Axess\\Dci4Wtp\\blockTicketBySerial',
      'blockTicketBySerialResponse' => 'Axess\\Dci4Wtp\\blockTicketBySerialResponse',
      'unBlockTicketBySerial' => 'Axess\\Dci4Wtp\\unBlockTicketBySerial',
      'unBlockTicketBySerialResponse' => 'Axess\\Dci4Wtp\\unBlockTicketBySerialResponse',
      'blockUnBlockTicketByJournal' => 'Axess\\Dci4Wtp\\blockUnBlockTicketByJournal',
      'blockUnBlockTicketByJournalResponse' => 'Axess\\Dci4Wtp\\blockUnBlockTicketByJournalResponse',
      'BlockUnblockTicket' => 'Axess\\Dci4Wtp\\BlockUnblockTicket',
      'BlockUnblockTicketResponse' => 'Axess\\Dci4Wtp\\BlockUnblockTicketResponse',
      'findPrepaidTicket' => 'Axess\\Dci4Wtp\\findPrepaidTicket',
      'findPrepaidTicketResponse' => 'Axess\\Dci4Wtp\\findPrepaidTicketResponse',
      'findPrepaidTicket2' => 'Axess\\Dci4Wtp\\findPrepaidTicket2',
      'findPrepaidTicket2Response' => 'Axess\\Dci4Wtp\\findPrepaidTicket2Response',
      'findPrepaidTicket3' => 'Axess\\Dci4Wtp\\findPrepaidTicket3',
      'findPrepaidTicket3Response' => 'Axess\\Dci4Wtp\\findPrepaidTicket3Response',
      'findPrepaidTicket4' => 'Axess\\Dci4Wtp\\findPrepaidTicket4',
      'findPrepaidTicket4Response' => 'Axess\\Dci4Wtp\\findPrepaidTicket4Response',
      'producePrepaidTicket' => 'Axess\\Dci4Wtp\\producePrepaidTicket',
      'producePrepaidTicketResponse' => 'Axess\\Dci4Wtp\\producePrepaidTicketResponse',
      'producePrepaidTicket2' => 'Axess\\Dci4Wtp\\producePrepaidTicket2',
      'producePrepaidTicket2Response' => 'Axess\\Dci4Wtp\\producePrepaidTicket2Response',
      'producePrepaidTicket3' => 'Axess\\Dci4Wtp\\producePrepaidTicket3',
      'producePrepaidTicket3Response' => 'Axess\\Dci4Wtp\\producePrepaidTicket3Response',
      'producePrepaidTicket4' => 'Axess\\Dci4Wtp\\producePrepaidTicket4',
      'producePrepaidTicket4Response' => 'Axess\\Dci4Wtp\\producePrepaidTicket4Response',
      'setStateOCCPickupProduction' => 'Axess\\Dci4Wtp\\setStateOCCPickupProduction',
      'setStateOCCPickupProductionResponse' => 'Axess\\Dci4Wtp\\setStateOCCPickupProductionResponse',
      'switchPrepaidTcktStatus' => 'Axess\\Dci4Wtp\\switchPrepaidTcktStatus',
      'switchPrepaidTcktStatusResponse' => 'Axess\\Dci4Wtp\\switchPrepaidTcktStatusResponse',
      'switchPrepaidTcktStatus2' => 'Axess\\Dci4Wtp\\switchPrepaidTcktStatus2',
      'switchPrepaidTcktStatus2Response' => 'Axess\\Dci4Wtp\\switchPrepaidTcktStatus2Response',
      'managePERSPhoto' => 'Axess\\Dci4Wtp\\managePERSPhoto',
      'managePERSPhotoResponse' => 'Axess\\Dci4Wtp\\managePERSPhotoResponse',
      'decodeWTPNo64Bit' => 'Axess\\Dci4Wtp\\decodeWTPNo64Bit',
      'decodeWTPNo64BitResponse' => 'Axess\\Dci4Wtp\\decodeWTPNo64BitResponse',
      'checkIsSkiClubMember' => 'Axess\\Dci4Wtp\\checkIsSkiClubMember',
      'checkIsSkiClubMemberResponse' => 'Axess\\Dci4Wtp\\checkIsSkiClubMemberResponse',
      'holeKundenDatenFromSwissPass' => 'Axess\\Dci4Wtp\\holeKundenDatenFromSwissPass',
      'holeKundenDatenFromSwissPassResponse' => 'Axess\\Dci4Wtp\\holeKundenDatenFromSwissPassResponse',
      'postLeistungsInfoToSwissPass' => 'Axess\\Dci4Wtp\\postLeistungsInfoToSwissPass',
      'postLeistungsInfoToSwissPassResponse' => 'Axess\\Dci4Wtp\\postLeistungsInfoToSwissPassResponse',
      'getWTPNoList' => 'Axess\\Dci4Wtp\\getWTPNoList',
      'getWTPNoListResponse' => 'Axess\\Dci4Wtp\\getWTPNoListResponse',
      'IsSwissPassEnabled' => 'Axess\\Dci4Wtp\\IsSwissPassEnabled',
      'IsSwissPassEnabledResponse' => 'Axess\\Dci4Wtp\\IsSwissPassEnabledResponse',
      'getReaderTrans' => 'Axess\\Dci4Wtp\\getReaderTrans',
      'getReaderTransResponse' => 'Axess\\Dci4Wtp\\getReaderTransResponse',
      'getReaderTrans2' => 'Axess\\Dci4Wtp\\getReaderTrans2',
      'getReaderTrans2Response' => 'Axess\\Dci4Wtp\\getReaderTrans2Response',
      'getContingent' => 'Axess\\Dci4Wtp\\getContingent',
      'getContingentResponse' => 'Axess\\Dci4Wtp\\getContingentResponse',
      'getContingent2' => 'Axess\\Dci4Wtp\\getContingent2',
      'getContingent2Response' => 'Axess\\Dci4Wtp\\getContingent2Response',
      'getContingent3' => 'Axess\\Dci4Wtp\\getContingent3',
      'getContingent3Response' => 'Axess\\Dci4Wtp\\getContingent3Response',
      'getContingent4' => 'Axess\\Dci4Wtp\\getContingent4',
      'getContingent4Response' => 'Axess\\Dci4Wtp\\getContingent4Response',
      'getContingent5' => 'Axess\\Dci4Wtp\\getContingent5',
      'getContingent5Response' => 'Axess\\Dci4Wtp\\getContingent5Response',
      'decContingent' => 'Axess\\Dci4Wtp\\decContingent',
      'decContingentResponse' => 'Axess\\Dci4Wtp\\decContingentResponse',
      'nChangeGroup' => 'Axess\\Dci4Wtp\\nChangeGroup',
      'nChangeGroupResponse' => 'Axess\\Dci4Wtp\\nChangeGroupResponse',
      'getPersSalesData' => 'Axess\\Dci4Wtp\\getPersSalesData',
      'getPersSalesDataResponse' => 'Axess\\Dci4Wtp\\getPersSalesDataResponse',
      'getPersSalesData2' => 'Axess\\Dci4Wtp\\getPersSalesData2',
      'getPersSalesData2Response' => 'Axess\\Dci4Wtp\\getPersSalesData2Response',
      'getWTPNoSalesData' => 'Axess\\Dci4Wtp\\getWTPNoSalesData',
      'getWTPNoSalesDataResponse' => 'Axess\\Dci4Wtp\\getWTPNoSalesDataResponse',
      'getWTPNoSalesData2' => 'Axess\\Dci4Wtp\\getWTPNoSalesData2',
      'getWTPNoSalesData2Response' => 'Axess\\Dci4Wtp\\getWTPNoSalesData2Response',
      'authenticateMember' => 'Axess\\Dci4Wtp\\authenticateMember',
      'authenticateMemberResponse' => 'Axess\\Dci4Wtp\\authenticateMemberResponse',
      'getFamily' => 'Axess\\Dci4Wtp\\getFamily',
      'getFamilyResponse' => 'Axess\\Dci4Wtp\\getFamilyResponse',
      'setCreditCardReference' => 'Axess\\Dci4Wtp\\setCreditCardReference',
      'setCreditCardReferenceResponse' => 'Axess\\Dci4Wtp\\setCreditCardReferenceResponse',
      'getCreditCardReference' => 'Axess\\Dci4Wtp\\getCreditCardReference',
      'getCreditCardReferenceResponse' => 'Axess\\Dci4Wtp\\getCreditCardReferenceResponse',
      'delCreditCardReference' => 'Axess\\Dci4Wtp\\delCreditCardReference',
      'delCreditCardReferenceResponse' => 'Axess\\Dci4Wtp\\delCreditCardReferenceResponse',
      'getDTLFirstUsages' => 'Axess\\Dci4Wtp\\getDTLFirstUsages',
      'getDTLFirstUsagesResponse' => 'Axess\\Dci4Wtp\\getDTLFirstUsagesResponse',
      'verifyNamedUser' => 'Axess\\Dci4Wtp\\verifyNamedUser',
      'verifyNamedUserResponse' => 'Axess\\Dci4Wtp\\verifyNamedUserResponse',
      'setPrepaidTcktPlusData' => 'Axess\\Dci4Wtp\\setPrepaidTcktPlusData',
      'setPrepaidTcktPlusDataResponse' => 'Axess\\Dci4Wtp\\setPrepaidTcktPlusDataResponse',
      'restoreTicketStatus' => 'Axess\\Dci4Wtp\\restoreTicketStatus',
      'restoreTicketStatusResponse' => 'Axess\\Dci4Wtp\\restoreTicketStatusResponse',
      'getArticleList' => 'Axess\\Dci4Wtp\\getArticleList',
      'getArticleListResponse' => 'Axess\\Dci4Wtp\\getArticleListResponse',
      'getArticleList2' => 'Axess\\Dci4Wtp\\getArticleList2',
      'getArticleList2Response' => 'Axess\\Dci4Wtp\\getArticleList2Response',
      'getArticleList3' => 'Axess\\Dci4Wtp\\getArticleList3',
      'getArticleList3Response' => 'Axess\\Dci4Wtp\\getArticleList3Response',
      'getArticleList4' => 'Axess\\Dci4Wtp\\getArticleList4',
      'getArticleList4Response' => 'Axess\\Dci4Wtp\\getArticleList4Response',
      'getWareList' => 'Axess\\Dci4Wtp\\getWareList',
      'getWareListResponse' => 'Axess\\Dci4Wtp\\getWareListResponse',
      'getWareList2' => 'Axess\\Dci4Wtp\\getWareList2',
      'getWareList2Response' => 'Axess\\Dci4Wtp\\getWareList2Response',
      'getWareList3' => 'Axess\\Dci4Wtp\\getWareList3',
      'getWareList3Response' => 'Axess\\Dci4Wtp\\getWareList3Response',
      'getCurrencies' => 'Axess\\Dci4Wtp\\getCurrencies',
      'getCurrenciesResponse' => 'Axess\\Dci4Wtp\\getCurrenciesResponse',
      'getTicketBlockStatus' => 'Axess\\Dci4Wtp\\getTicketBlockStatus',
      'getTicketBlockStatusResponse' => 'Axess\\Dci4Wtp\\getTicketBlockStatusResponse',
      'setClientSetup' => 'Axess\\Dci4Wtp\\setClientSetup',
      'setClientSetupResponse' => 'Axess\\Dci4Wtp\\setClientSetupResponse',
      'getClientSetup' => 'Axess\\Dci4Wtp\\getClientSetup',
      'getClientSetupResponse' => 'Axess\\Dci4Wtp\\getClientSetupResponse',
      'getTicketRidesAndDrops' => 'Axess\\Dci4Wtp\\getTicketRidesAndDrops',
      'getTicketRidesAndDropsResponse' => 'Axess\\Dci4Wtp\\getTicketRidesAndDropsResponse',
      'getTicketRidesAndDrops2' => 'Axess\\Dci4Wtp\\getTicketRidesAndDrops2',
      'getTicketRidesAndDrops2Response' => 'Axess\\Dci4Wtp\\getTicketRidesAndDrops2Response',
      'ChangeCompany' => 'Axess\\Dci4Wtp\\ChangeCompany',
      'ChangeCompanyResponse' => 'Axess\\Dci4Wtp\\ChangeCompanyResponse',
      'decodeBarcode' => 'Axess\\Dci4Wtp\\decodeBarcode',
      'decodeBarcodeResponse' => 'Axess\\Dci4Wtp\\decodeBarcodeResponse',
      'getPromptList' => 'Axess\\Dci4Wtp\\getPromptList',
      'getPromptListResponse' => 'Axess\\Dci4Wtp\\getPromptListResponse',
      'setPromptData' => 'Axess\\Dci4Wtp\\setPromptData',
      'setPromptDataResponse' => 'Axess\\Dci4Wtp\\setPromptDataResponse',
      'getPromptData' => 'Axess\\Dci4Wtp\\getPromptData',
      'getPromptDataResponse' => 'Axess\\Dci4Wtp\\getPromptDataResponse',
      'setExtInvoiceNo' => 'Axess\\Dci4Wtp\\setExtInvoiceNo',
      'setExtInvoiceNoResponse' => 'Axess\\Dci4Wtp\\setExtInvoiceNoResponse',
      'ReleaseGlobalPrepaidTicket' => 'Axess\\Dci4Wtp\\ReleaseGlobalPrepaidTicket',
      'ReleaseGlobalPrepaidTicketResponse' => 'Axess\\Dci4Wtp\\ReleaseGlobalPrepaidTicketResponse',
      'getReservation' => 'Axess\\Dci4Wtp\\getReservation',
      'getReservationResponse' => 'Axess\\Dci4Wtp\\getReservationResponse',
      'modifyReservation' => 'Axess\\Dci4Wtp\\modifyReservation',
      'modifyReservationResponse' => 'Axess\\Dci4Wtp\\modifyReservationResponse',
      'modifyReservation2' => 'Axess\\Dci4Wtp\\modifyReservation2',
      'modifyReservation2Response' => 'Axess\\Dci4Wtp\\modifyReservation2Response',
      'addReservation' => 'Axess\\Dci4Wtp\\addReservation',
      'addReservationResponse' => 'Axess\\Dci4Wtp\\addReservationResponse',
      'getLNECertificateNumber' => 'Axess\\Dci4Wtp\\getLNECertificateNumber',
      'getLNECertificateNumberResponse' => 'Axess\\Dci4Wtp\\getLNECertificateNumberResponse',
      'isNF525TestModeEnabled' => 'Axess\\Dci4Wtp\\isNF525TestModeEnabled',
      'isNF525TestModeEnabledResponse' => 'Axess\\Dci4Wtp\\isNF525TestModeEnabledResponse',
      'addLicensePlate' => 'Axess\\Dci4Wtp\\addLicensePlate',
      'addLicensePlateResponse' => 'Axess\\Dci4Wtp\\addLicensePlateResponse',
      'addAdditionalLicensePlate' => 'Axess\\Dci4Wtp\\addAdditionalLicensePlate',
      'addAdditionalLicensePlateResponse' => 'Axess\\Dci4Wtp\\addAdditionalLicensePlateResponse',
      'StartCLICSReport' => 'Axess\\Dci4Wtp\\StartCLICSReport',
      'StartCLICSReportResponse' => 'Axess\\Dci4Wtp\\StartCLICSReportResponse',
      'GetCLICSReportStatus' => 'Axess\\Dci4Wtp\\GetCLICSReportStatus',
      'GetCLICSReportStatusResponse' => 'Axess\\Dci4Wtp\\GetCLICSReportStatusResponse',
      'getLicensePlateUsages' => 'Axess\\Dci4Wtp\\getLicensePlateUsages',
      'getLicensePlateUsagesResponse' => 'Axess\\Dci4Wtp\\getLicensePlateUsagesResponse',
      'bIsLpCheckInOut' => 'Axess\\Dci4Wtp\\bIsLpCheckInOut',
      'bIsLpCheckInOutResponse' => 'Axess\\Dci4Wtp\\bIsLpCheckInOutResponse',
      'setPayer' => 'Axess\\Dci4Wtp\\setPayer',
      'setPayerResponse' => 'Axess\\Dci4Wtp\\setPayerResponse',
      'setPayer2' => 'Axess\\Dci4Wtp\\setPayer2',
      'setPayer2Response' => 'Axess\\Dci4Wtp\\setPayer2Response',
      'manageShoppingCart' => 'Axess\\Dci4Wtp\\manageShoppingCart',
      'manageShoppingCartResponse' => 'Axess\\Dci4Wtp\\manageShoppingCartResponse',
      'manageShoppingCart2' => 'Axess\\Dci4Wtp\\manageShoppingCart2',
      'manageShoppingCart2Response' => 'Axess\\Dci4Wtp\\manageShoppingCart2Response',
      'manageShoppingCart3' => 'Axess\\Dci4Wtp\\manageShoppingCart3',
      'manageShoppingCart3Response' => 'Axess\\Dci4Wtp\\manageShoppingCart3Response',
      'manageDownPayment' => 'Axess\\Dci4Wtp\\manageDownPayment',
      'manageDownPaymentResponse' => 'Axess\\Dci4Wtp\\manageDownPaymentResponse',
      'manageTravelGroup' => 'Axess\\Dci4Wtp\\manageTravelGroup',
      'manageTravelGroupResponse' => 'Axess\\Dci4Wtp\\manageTravelGroupResponse',
      'setShoppingCartStatus' => 'Axess\\Dci4Wtp\\setShoppingCartStatus',
      'setShoppingCartStatusResponse' => 'Axess\\Dci4Wtp\\setShoppingCartStatusResponse',
      'setTravelGroupStatus' => 'Axess\\Dci4Wtp\\setTravelGroupStatus',
      'setTravelGroupStatusResponse' => 'Axess\\Dci4Wtp\\setTravelGroupStatusResponse',
      'getShoppingCartData' => 'Axess\\Dci4Wtp\\getShoppingCartData',
      'getShoppingCartDataResponse' => 'Axess\\Dci4Wtp\\getShoppingCartDataResponse',
      'getShoppingCartData2' => 'Axess\\Dci4Wtp\\getShoppingCartData2',
      'getShoppingCartData2Response' => 'Axess\\Dci4Wtp\\getShoppingCartData2Response',
      'getShoppingCartData3' => 'Axess\\Dci4Wtp\\getShoppingCartData3',
      'getShoppingCartData3Response' => 'Axess\\Dci4Wtp\\getShoppingCartData3Response',
      'getShoppingCartData4' => 'Axess\\Dci4Wtp\\getShoppingCartData4',
      'getShoppingCartData4Response' => 'Axess\\Dci4Wtp\\getShoppingCartData4Response',
      'getShoppingCartData5' => 'Axess\\Dci4Wtp\\getShoppingCartData5',
      'getShoppingCartData5Response' => 'Axess\\Dci4Wtp\\getShoppingCartData5Response',
      'getTravelGroupData' => 'Axess\\Dci4Wtp\\getTravelGroupData',
      'getTravelGroupDataResponse' => 'Axess\\Dci4Wtp\\getTravelGroupDataResponse',
      'getTravelGroupData2' => 'Axess\\Dci4Wtp\\getTravelGroupData2',
      'getTravelGroupData2Response' => 'Axess\\Dci4Wtp\\getTravelGroupData2Response',
      'getTravelGrpShopBasPositions' => 'Axess\\Dci4Wtp\\getTravelGrpShopBasPositions',
      'getTravelGrpShopBasPositionsResponse' => 'Axess\\Dci4Wtp\\getTravelGrpShopBasPositionsResponse',
      'getTravelGrpShopBasPositions2' => 'Axess\\Dci4Wtp\\getTravelGrpShopBasPositions2',
      'getTravelGrpShopBasPositions2Response' => 'Axess\\Dci4Wtp\\getTravelGrpShopBasPositions2Response',
      'produceShoppingBasketPrepaid' => 'Axess\\Dci4Wtp\\produceShoppingBasketPrepaid',
      'produceShoppingBasketPrepaidResponse' => 'Axess\\Dci4Wtp\\produceShoppingBasketPrepaidResponse',
      'produceShoppingBasketPrepaid2' => 'Axess\\Dci4Wtp\\produceShoppingBasketPrepaid2',
      'produceShoppingBasketPrepaid2Response' => 'Axess\\Dci4Wtp\\produceShoppingBasketPrepaid2Response',
      'getCompanyPaymentTypeExcl' => 'Axess\\Dci4Wtp\\getCompanyPaymentTypeExcl',
      'getCompanyPaymentTypeExclResponse' => 'Axess\\Dci4Wtp\\getCompanyPaymentTypeExclResponse',
      'storeTempTXT' => 'Axess\\Dci4Wtp\\storeTempTXT',
      'storeTempTXTResponse' => 'Axess\\Dci4Wtp\\storeTempTXTResponse',
      'getOpenCashierShifts' => 'Axess\\Dci4Wtp\\getOpenCashierShifts',
      'getOpenCashierShiftsResponse' => 'Axess\\Dci4Wtp\\getOpenCashierShiftsResponse',
      'openCashierShift' => 'Axess\\Dci4Wtp\\openCashierShift',
      'openCashierShiftResponse' => 'Axess\\Dci4Wtp\\openCashierShiftResponse',
      'closeCashierShift' => 'Axess\\Dci4Wtp\\closeCashierShift',
      'closeCashierShiftResponse' => 'Axess\\Dci4Wtp\\closeCashierShiftResponse',
      'getCashierRights' => 'Axess\\Dci4Wtp\\getCashierRights',
      'getCashierRightsResponse' => 'Axess\\Dci4Wtp\\getCashierRightsResponse',
      'getGroupReservation' => 'Axess\\Dci4Wtp\\getGroupReservation',
      'getGroupReservationResponse' => 'Axess\\Dci4Wtp\\getGroupReservationResponse',
      'getGroupContingent' => 'Axess\\Dci4Wtp\\getGroupContingent',
      'getGroupContingentResponse' => 'Axess\\Dci4Wtp\\getGroupContingentResponse',
      'getGroupContingent2' => 'Axess\\Dci4Wtp\\getGroupContingent2',
      'getGroupContingent2Response' => 'Axess\\Dci4Wtp\\getGroupContingent2Response',
      'lockReservation' => 'Axess\\Dci4Wtp\\lockReservation',
      'lockReservationResponse' => 'Axess\\Dci4Wtp\\lockReservationResponse',
      'getNF525ReceiptNo' => 'Axess\\Dci4Wtp\\getNF525ReceiptNo',
      'getNF525ReceiptNoResponse' => 'Axess\\Dci4Wtp\\getNF525ReceiptNoResponse',
      'getReceiptData' => 'Axess\\Dci4Wtp\\getReceiptData',
      'getReceiptDataResponse' => 'Axess\\Dci4Wtp\\getReceiptDataResponse',
      'getReceiptData2' => 'Axess\\Dci4Wtp\\getReceiptData2',
      'getReceiptData2Response' => 'Axess\\Dci4Wtp\\getReceiptData2Response',
      'getARCubes' => 'Axess\\Dci4Wtp\\getARCubes',
      'getARCubesResponse' => 'Axess\\Dci4Wtp\\getARCubesResponse',
      'setARCube' => 'Axess\\Dci4Wtp\\setARCube',
      'setARCubeResponse' => 'Axess\\Dci4Wtp\\setARCubeResponse',
      'getWTPRentalItems' => 'Axess\\Dci4Wtp\\getWTPRentalItems',
      'getWTPRentalItemsResponse' => 'Axess\\Dci4Wtp\\getWTPRentalItemsResponse',
      'getWTPRentalItems2' => 'Axess\\Dci4Wtp\\getWTPRentalItems2',
      'getWTPRentalItems2Response' => 'Axess\\Dci4Wtp\\getWTPRentalItems2Response',
      'getWTPRentalPersTypes' => 'Axess\\Dci4Wtp\\getWTPRentalPersTypes',
      'getWTPRentalPersTypesResponse' => 'Axess\\Dci4Wtp\\getWTPRentalPersTypesResponse',
      'getARCData' => 'Axess\\Dci4Wtp\\getARCData',
      'getARCDataResponse' => 'Axess\\Dci4Wtp\\getARCDataResponse',
      'getRoutesStations' => 'Axess\\Dci4Wtp\\getRoutesStations',
      'getRoutesStationsResponse' => 'Axess\\Dci4Wtp\\getRoutesStationsResponse',
      'getCompany' => 'Axess\\Dci4Wtp\\getCompany',
      'getCompanyResponse' => 'Axess\\Dci4Wtp\\getCompanyResponse',
      'getCompany2' => 'Axess\\Dci4Wtp\\getCompany2',
      'getCompany2Response' => 'Axess\\Dci4Wtp\\getCompany2Response',
      'getCompany3' => 'Axess\\Dci4Wtp\\getCompany3',
      'getCompany3Response' => 'Axess\\Dci4Wtp\\getCompany3Response',
      'getCash4CashierShift' => 'Axess\\Dci4Wtp\\getCash4CashierShift',
      'getCash4CashierShiftResponse' => 'Axess\\Dci4Wtp\\getCash4CashierShiftResponse',
      'moveCash4CashierShift' => 'Axess\\Dci4Wtp\\moveCash4CashierShift',
      'moveCash4CashierShiftResponse' => 'Axess\\Dci4Wtp\\moveCash4CashierShiftResponse',
      'redeemPromoCodes' => 'Axess\\Dci4Wtp\\redeemPromoCodes',
      'redeemPromoCodesResponse' => 'Axess\\Dci4Wtp\\redeemPromoCodesResponse',
      'getACPXImage' => 'Axess\\Dci4Wtp\\getACPXImage',
      'getACPXImageResponse' => 'Axess\\Dci4Wtp\\getACPXImageResponse',
      'getACPXImageWithImage' => 'Axess\\Dci4Wtp\\getACPXImageWithImage',
      'getACPXImageWithImageResponse' => 'Axess\\Dci4Wtp\\getACPXImageWithImageResponse',
      'createBookingReservation' => 'Axess\\Dci4Wtp\\createBookingReservation',
      'createBookingReservationResponse' => 'Axess\\Dci4Wtp\\createBookingReservationResponse',
      'createBooking' => 'Axess\\Dci4Wtp\\createBooking',
      'createBookingResponse' => 'Axess\\Dci4Wtp\\createBookingResponse',
      'cancelBookingReservation' => 'Axess\\Dci4Wtp\\cancelBookingReservation',
      'cancelBookingReservationResponse' => 'Axess\\Dci4Wtp\\cancelBookingReservationResponse',
      'getBookingLimits' => 'Axess\\Dci4Wtp\\getBookingLimits',
      'getBookingLimitsResponse' => 'Axess\\Dci4Wtp\\getBookingLimitsResponse',
      'getDataForTicket' => 'Axess\\Dci4Wtp\\getDataForTicket',
      'getDataForTicketResponse' => 'Axess\\Dci4Wtp\\getDataForTicketResponse',
      'createPOEBooking' => 'Axess\\Dci4Wtp\\createPOEBooking',
      'createPOEBookingResponse' => 'Axess\\Dci4Wtp\\createPOEBookingResponse',
      'cancelPOEBooking' => 'Axess\\Dci4Wtp\\cancelPOEBooking',
      'cancelPOEBookingResponse' => 'Axess\\Dci4Wtp\\cancelPOEBookingResponse',
      'createPOEBookingWithCertificate' => 'Axess\\Dci4Wtp\\createPOEBookingWithCertificate',
      'createPOEBookingWithCertificateResponse' => 'Axess\\Dci4Wtp\\createPOEBookingWithCertificateResponse',
      'createPOEBookingWithManualCertificate' => 'Axess\\Dci4Wtp\\createPOEBookingWithManualCertificate',
      'createPOEBookingWithManualCertificateResponse' => 'Axess\\Dci4Wtp\\createPOEBookingWithManualCertificateResponse',
      'subscribeForLimitCheck' => 'Axess\\Dci4Wtp\\subscribeForLimitCheck',
      'subscribeForLimitCheckResponse' => 'Axess\\Dci4Wtp\\subscribeForLimitCheckResponse',
      'subscribeForTariffCheck' => 'Axess\\Dci4Wtp\\subscribeForTariffCheck',
      'subscribeForTariffCheckResponse' => 'Axess\\Dci4Wtp\\subscribeForTariffCheckResponse',
      'getServerVersion' => 'Axess\\Dci4Wtp\\getServerVersion',
      'getServerVersionResponse' => 'Axess\\Dci4Wtp\\getServerVersionResponse',
      'getModulVersion' => 'Axess\\Dci4Wtp\\getModulVersion',
      'getModulVersionResponse' => 'Axess\\Dci4Wtp\\getModulVersionResponse',
      'login' => 'Axess\\Dci4Wtp\\login',
      'loginResponse' => 'Axess\\Dci4Wtp\\loginResponse',
      'login2' => 'Axess\\Dci4Wtp\\login2',
      'login2Response' => 'Axess\\Dci4Wtp\\login2Response',
      'loginCashier' => 'Axess\\Dci4Wtp\\loginCashier',
      'loginCashierResponse' => 'Axess\\Dci4Wtp\\loginCashierResponse',
      'loginNamedUser' => 'Axess\\Dci4Wtp\\loginNamedUser',
      'loginNamedUserResponse' => 'Axess\\Dci4Wtp\\loginNamedUserResponse',
      'logout' => 'Axess\\Dci4Wtp\\logout',
      'logoutResponse' => 'Axess\\Dci4Wtp\\logoutResponse',
      'setCountryCode' => 'Axess\\Dci4Wtp\\setCountryCode',
      'setCountryCodeResponse' => 'Axess\\Dci4Wtp\\setCountryCodeResponse',
      'checkSession' => 'Axess\\Dci4Wtp\\checkSession',
      'checkSessionResponse' => 'Axess\\Dci4Wtp\\checkSessionResponse',
      'getWTPConfiguration' => 'Axess\\Dci4Wtp\\getWTPConfiguration',
      'getWTPConfigurationResponse' => 'Axess\\Dci4Wtp\\getWTPConfigurationResponse',
      'forgotPassword' => 'Axess\\Dci4Wtp\\forgotPassword',
      'forgotPasswordResponse' => 'Axess\\Dci4Wtp\\forgotPasswordResponse',
      'getCustomerProfile' => 'Axess\\Dci4Wtp\\getCustomerProfile',
      'getCustomerProfileResponse' => 'Axess\\Dci4Wtp\\getCustomerProfileResponse',
      'getCustomerProfile2' => 'Axess\\Dci4Wtp\\getCustomerProfile2',
      'getCustomerProfile2Response' => 'Axess\\Dci4Wtp\\getCustomerProfile2Response',
      'getCustomerProfile3' => 'Axess\\Dci4Wtp\\getCustomerProfile3',
      'getCustomerProfile3Response' => 'Axess\\Dci4Wtp\\getCustomerProfile3Response',
      'getB2CAccounts' => 'Axess\\Dci4Wtp\\getB2CAccounts',
      'getB2CAccountsResponse' => 'Axess\\Dci4Wtp\\getB2CAccountsResponse',
      'setB2CAccount' => 'Axess\\Dci4Wtp\\setB2CAccount',
      'setB2CAccountResponse' => 'Axess\\Dci4Wtp\\setB2CAccountResponse',
      'changePassword' => 'Axess\\Dci4Wtp\\changePassword',
      'changePasswordResponse' => 'Axess\\Dci4Wtp\\changePasswordResponse',
      'getCompanyInfo' => 'Axess\\Dci4Wtp\\getCompanyInfo',
      'getCompanyInfoResponse' => 'Axess\\Dci4Wtp\\getCompanyInfoResponse',
      'getCompanyInfo2' => 'Axess\\Dci4Wtp\\getCompanyInfo2',
      'getCompanyInfo2Response' => 'Axess\\Dci4Wtp\\getCompanyInfo2Response',
      'getPools' => 'Axess\\Dci4Wtp\\getPools',
      'getPoolsResponse' => 'Axess\\Dci4Wtp\\getPoolsResponse',
      'getPools2' => 'Axess\\Dci4Wtp\\getPools2',
      'getPools2Response' => 'Axess\\Dci4Wtp\\getPools2Response',
      'getTicketTypes' => 'Axess\\Dci4Wtp\\getTicketTypes',
      'getTicketTypesResponse' => 'Axess\\Dci4Wtp\\getTicketTypesResponse',
      'getTicketTypes2' => 'Axess\\Dci4Wtp\\getTicketTypes2',
      'getTicketTypes2Response' => 'Axess\\Dci4Wtp\\getTicketTypes2Response',
      'getTicketTypes3' => 'Axess\\Dci4Wtp\\getTicketTypes3',
      'getTicketTypes3Response' => 'Axess\\Dci4Wtp\\getTicketTypes3Response',
      'getTicketTypes4' => 'Axess\\Dci4Wtp\\getTicketTypes4',
      'getTicketTypes4Response' => 'Axess\\Dci4Wtp\\getTicketTypes4Response',
      'getWTPGroups' => 'Axess\\Dci4Wtp\\getWTPGroups',
      'getWTPGroupsResponse' => 'Axess\\Dci4Wtp\\getWTPGroupsResponse',
      'getWTPGroups2' => 'Axess\\Dci4Wtp\\getWTPGroups2',
      'getWTPGroups2Response' => 'Axess\\Dci4Wtp\\getWTPGroups2Response',
      'getPersonTypes' => 'Axess\\Dci4Wtp\\getPersonTypes',
      'getPersonTypesResponse' => 'Axess\\Dci4Wtp\\getPersonTypesResponse',
      'getPersonTypes2' => 'Axess\\Dci4Wtp\\getPersonTypes2',
      'getPersonTypes2Response' => 'Axess\\Dci4Wtp\\getPersonTypes2Response',
      'getPersonTypes3' => 'Axess\\Dci4Wtp\\getPersonTypes3',
      'getPersonTypes3Response' => 'Axess\\Dci4Wtp\\getPersonTypes3Response',
      'getPersTypeRules' => 'Axess\\Dci4Wtp\\getPersTypeRules',
      'getPersTypeRulesResponse' => 'Axess\\Dci4Wtp\\getPersTypeRulesResponse',
      'getAdditionalArticles' => 'Axess\\Dci4Wtp\\getAdditionalArticles',
      'getAdditionalArticlesResponse' => 'Axess\\Dci4Wtp\\getAdditionalArticlesResponse',
      'getPackages' => 'Axess\\Dci4Wtp\\getPackages',
      'getPackagesResponse' => 'Axess\\Dci4Wtp\\getPackagesResponse',
      'getPackages2' => 'Axess\\Dci4Wtp\\getPackages2',
      'getPackages2Response' => 'Axess\\Dci4Wtp\\getPackages2Response',
      'getPackages3' => 'Axess\\Dci4Wtp\\getPackages3',
      'getPackages3Response' => 'Axess\\Dci4Wtp\\getPackages3Response',
      'getPackageContent' => 'Axess\\Dci4Wtp\\getPackageContent',
      'getPackageContentResponse' => 'Axess\\Dci4Wtp\\getPackageContentResponse',
      'getPackageContent2' => 'Axess\\Dci4Wtp\\getPackageContent2',
      'getPackageContent2Response' => 'Axess\\Dci4Wtp\\getPackageContent2Response',
      'getPackageContent3' => 'Axess\\Dci4Wtp\\getPackageContent3',
      'getPackageContent3Response' => 'Axess\\Dci4Wtp\\getPackageContent3Response',
      'getPackageContent4' => 'Axess\\Dci4Wtp\\getPackageContent4',
      'getPackageContent4Response' => 'Axess\\Dci4Wtp\\getPackageContent4Response',
      'getPackageTariffList' => 'Axess\\Dci4Wtp\\getPackageTariffList',
      'getPackageTariffListResponse' => 'Axess\\Dci4Wtp\\getPackageTariffListResponse',
      'getPackageTariffList2' => 'Axess\\Dci4Wtp\\getPackageTariffList2',
      'getPackageTariffList2Response' => 'Axess\\Dci4Wtp\\getPackageTariffList2Response',
      'getPackageTariffList3' => 'Axess\\Dci4Wtp\\getPackageTariffList3',
      'getPackageTariffList3Response' => 'Axess\\Dci4Wtp\\getPackageTariffList3Response',
      'getPackageTariffList4' => 'Axess\\Dci4Wtp\\getPackageTariffList4',
      'getPackageTariffList4Response' => 'Axess\\Dci4Wtp\\getPackageTariffList4Response',
      'getPackageTariffList5' => 'Axess\\Dci4Wtp\\getPackageTariffList5',
      'getPackageTariffList5Response' => 'Axess\\Dci4Wtp\\getPackageTariffList5Response',
      'getPackageTariffList6' => 'Axess\\Dci4Wtp\\getPackageTariffList6',
      'getPackageTariffList6Response' => 'Axess\\Dci4Wtp\\getPackageTariffList6Response',
      'getPackageTariffList7' => 'Axess\\Dci4Wtp\\getPackageTariffList7',
      'getPackageTariffList7Response' => 'Axess\\Dci4Wtp\\getPackageTariffList7Response',
      'getPackageTariffList8' => 'Axess\\Dci4Wtp\\getPackageTariffList8',
      'getPackageTariffList8Response' => 'Axess\\Dci4Wtp\\getPackageTariffList8Response',
      'getPackageTariffList9' => 'Axess\\Dci4Wtp\\getPackageTariffList9',
      'getPackageTariffList9Response' => 'Axess\\Dci4Wtp\\getPackageTariffList9Response',
      'getPackagePosTarif' => 'Axess\\Dci4Wtp\\getPackagePosTarif',
      'getPackagePosTarifResponse' => 'Axess\\Dci4Wtp\\getPackagePosTarifResponse',
      'getProducts' => 'Axess\\Dci4Wtp\\getProducts',
      'getProductsResponse' => 'Axess\\Dci4Wtp\\getProductsResponse',
      'getTax' => 'Axess\\Dci4Wtp\\getTax',
      'getTaxResponse' => 'Axess\\Dci4Wtp\\getTaxResponse',
      'getTariff' => 'Axess\\Dci4Wtp\\getTariff',
      'getTariffResponse' => 'Axess\\Dci4Wtp\\getTariffResponse',
      'getTariff2' => 'Axess\\Dci4Wtp\\getTariff2',
      'getTariff2Response' => 'Axess\\Dci4Wtp\\getTariff2Response',
      'getTariffList' => 'Axess\\Dci4Wtp\\getTariffList',
      'getTariffListResponse' => 'Axess\\Dci4Wtp\\getTariffListResponse',
      'getTariffList2' => 'Axess\\Dci4Wtp\\getTariffList2',
      'getTariffList2Response' => 'Axess\\Dci4Wtp\\getTariffList2Response',
      'getTariffList3' => 'Axess\\Dci4Wtp\\getTariffList3',
      'getTariffList3Response' => 'Axess\\Dci4Wtp\\getTariffList3Response',
      'getTariffList4' => 'Axess\\Dci4Wtp\\getTariffList4',
      'getTariffList4Response' => 'Axess\\Dci4Wtp\\getTariffList4Response',
      'getTariffList5' => 'Axess\\Dci4Wtp\\getTariffList5',
      'getTariffList5Response' => 'Axess\\Dci4Wtp\\getTariffList5Response',
      'getTariffList6' => 'Axess\\Dci4Wtp\\getTariffList6',
      'getTariffList6Response' => 'Axess\\Dci4Wtp\\getTariffList6Response',
      'getTariffList7' => 'Axess\\Dci4Wtp\\getTariffList7',
      'getTariffList7Response' => 'Axess\\Dci4Wtp\\getTariffList7Response',
      'getTariffList8' => 'Axess\\Dci4Wtp\\getTariffList8',
      'getTariffList8Response' => 'Axess\\Dci4Wtp\\getTariffList8Response',
      'getTariffList9' => 'Axess\\Dci4Wtp\\getTariffList9',
      'getTariffList9Response' => 'Axess\\Dci4Wtp\\getTariffList9Response',
      'getPackageTariffs' => 'Axess\\Dci4Wtp\\getPackageTariffs',
      'getPackageTariffsResponse' => 'Axess\\Dci4Wtp\\getPackageTariffsResponse',
      'issueTicket' => 'Axess\\Dci4Wtp\\issueTicket',
      'issueTicketResponse' => 'Axess\\Dci4Wtp\\issueTicketResponse',
      'issueTicket2' => 'Axess\\Dci4Wtp\\issueTicket2',
      'issueTicket2Response' => 'Axess\\Dci4Wtp\\issueTicket2Response',
      'issueTicket3' => 'Axess\\Dci4Wtp\\issueTicket3',
      'issueTicket3Response' => 'Axess\\Dci4Wtp\\issueTicket3Response',
      'issueTicket4' => 'Axess\\Dci4Wtp\\issueTicket4',
      'issueTicket4Response' => 'Axess\\Dci4Wtp\\issueTicket4Response',
      'issueTicket5' => 'Axess\\Dci4Wtp\\issueTicket5',
      'issueTicket5Response' => 'Axess\\Dci4Wtp\\issueTicket5Response',
      'issueTicket6' => 'Axess\\Dci4Wtp\\issueTicket6',
      'issueTicket6Response' => 'Axess\\Dci4Wtp\\issueTicket6Response',
      'issueTicket7' => 'Axess\\Dci4Wtp\\issueTicket7',
      'issueTicket7Response' => 'Axess\\Dci4Wtp\\issueTicket7Response',
      'issueTicket8' => 'Axess\\Dci4Wtp\\issueTicket8',
      'issueTicket8Response' => 'Axess\\Dci4Wtp\\issueTicket8Response',
      'issueTicket9' => 'Axess\\Dci4Wtp\\issueTicket9',
      'issueTicket9Response' => 'Axess\\Dci4Wtp\\issueTicket9Response',
      'issueTicket10' => 'Axess\\Dci4Wtp\\issueTicket10',
      'issueTicket10Response' => 'Axess\\Dci4Wtp\\issueTicket10Response',
      'issueTicket11' => 'Axess\\Dci4Wtp\\issueTicket11',
      'issueTicket11Response' => 'Axess\\Dci4Wtp\\issueTicket11Response',
      'issueTicket12' => 'Axess\\Dci4Wtp\\issueTicket12',
      'issueTicket12Response' => 'Axess\\Dci4Wtp\\issueTicket12Response',
      'issueTicket13' => 'Axess\\Dci4Wtp\\issueTicket13',
      'issueTicket13Response' => 'Axess\\Dci4Wtp\\issueTicket13Response',
      'issueTicketModifyDate' => 'Axess\\Dci4Wtp\\issueTicketModifyDate',
      'issueTicketModifyDateResponse' => 'Axess\\Dci4Wtp\\issueTicketModifyDateResponse',
      'msgIssueTicket' => 'Axess\\Dci4Wtp\\msgIssueTicket',
      'msgIssueTicketResponse' => 'Axess\\Dci4Wtp\\msgIssueTicketResponse',
      'msgIssueTicket2' => 'Axess\\Dci4Wtp\\msgIssueTicket2',
      'msgIssueTicket2Response' => 'Axess\\Dci4Wtp\\msgIssueTicket2Response',
      'msgIssueTicket3' => 'Axess\\Dci4Wtp\\msgIssueTicket3',
      'msgIssueTicket3Response' => 'Axess\\Dci4Wtp\\msgIssueTicket3Response',
      'msgIssueTicket4' => 'Axess\\Dci4Wtp\\msgIssueTicket4',
      'msgIssueTicket4Response' => 'Axess\\Dci4Wtp\\msgIssueTicket4Response',
      'msgIssueTicket5' => 'Axess\\Dci4Wtp\\msgIssueTicket5',
      'msgIssueTicket5Response' => 'Axess\\Dci4Wtp\\msgIssueTicket5Response',
      'msgIssueTicket6' => 'Axess\\Dci4Wtp\\msgIssueTicket6',
      'msgIssueTicket6Response' => 'Axess\\Dci4Wtp\\msgIssueTicket6Response',
      'msgIssueTicket7' => 'Axess\\Dci4Wtp\\msgIssueTicket7',
      'msgIssueTicket7Response' => 'Axess\\Dci4Wtp\\msgIssueTicket7Response',
      'msgIssueTicket8' => 'Axess\\Dci4Wtp\\msgIssueTicket8',
      'msgIssueTicket8Response' => 'Axess\\Dci4Wtp\\msgIssueTicket8Response',
      'issuePackagePos' => 'Axess\\Dci4Wtp\\issuePackagePos',
      'issuePackagePosResponse' => 'Axess\\Dci4Wtp\\issuePackagePosResponse',
      'check4Rebook' => 'Axess\\Dci4Wtp\\check4Rebook',
      'check4RebookResponse' => 'Axess\\Dci4Wtp\\check4RebookResponse',
      'rebook' => 'Axess\\Dci4Wtp\\rebook',
      'rebookResponse' => 'Axess\\Dci4Wtp\\rebookResponse',
      'rebook2' => 'Axess\\Dci4Wtp\\rebook2',
      'rebook2Response' => 'Axess\\Dci4Wtp\\rebook2Response',
      'rebookTransaction' => 'Axess\\Dci4Wtp\\rebookTransaction',
      'rebookTransactionResponse' => 'Axess\\Dci4Wtp\\rebookTransactionResponse',
      'DoSplitPayment' => 'Axess\\Dci4Wtp\\DoSplitPayment',
      'DoSplitPaymentResponse' => 'Axess\\Dci4Wtp\\DoSplitPaymentResponse',
      'reloadTicket' => 'Axess\\Dci4Wtp\\reloadTicket',
      'reloadTicketResponse' => 'Axess\\Dci4Wtp\\reloadTicketResponse',
      'cancelTicket2' => 'Axess\\Dci4Wtp\\cancelTicket2',
      'cancelTicket2Response' => 'Axess\\Dci4Wtp\\cancelTicket2Response',
      'cancelTicket' => 'Axess\\Dci4Wtp\\cancelTicket',
      'cancelTicketResponse' => 'Axess\\Dci4Wtp\\cancelTicketResponse',
      'cancelTicket3' => 'Axess\\Dci4Wtp\\cancelTicket3',
      'cancelTicket3Response' => 'Axess\\Dci4Wtp\\cancelTicket3Response',
      'cancelTicket4' => 'Axess\\Dci4Wtp\\cancelTicket4',
      'cancelTicket4Response' => 'Axess\\Dci4Wtp\\cancelTicket4Response',
      'cancelTicket5' => 'Axess\\Dci4Wtp\\cancelTicket5',
      'cancelTicket5Response' => 'Axess\\Dci4Wtp\\cancelTicket5Response',
      'cancelTicket6' => 'Axess\\Dci4Wtp\\cancelTicket6',
      'cancelTicket6Response' => 'Axess\\Dci4Wtp\\cancelTicket6Response',
      'msgCancelTicket' => 'Axess\\Dci4Wtp\\msgCancelTicket',
      'msgCancelTicketResponse' => 'Axess\\Dci4Wtp\\msgCancelTicketResponse',
      'getTicketSalesData' => 'Axess\\Dci4Wtp\\getTicketSalesData',
      'getTicketSalesDataResponse' => 'Axess\\Dci4Wtp\\getTicketSalesDataResponse',
      'getTicketSalesData2' => 'Axess\\Dci4Wtp\\getTicketSalesData2',
      'getTicketSalesData2Response' => 'Axess\\Dci4Wtp\\getTicketSalesData2Response',
      'getTicketSalesData3' => 'Axess\\Dci4Wtp\\getTicketSalesData3',
      'getTicketSalesData3Response' => 'Axess\\Dci4Wtp\\getTicketSalesData3Response',
      'getTicketSalesData4' => 'Axess\\Dci4Wtp\\getTicketSalesData4',
      'getTicketSalesData4Response' => 'Axess\\Dci4Wtp\\getTicketSalesData4Response',
      'createReport' => 'Axess\\Dci4Wtp\\createReport',
      'createReportResponse' => 'Axess\\Dci4Wtp\\createReportResponse',
      'getTicketData' => 'Axess\\Dci4Wtp\\getTicketData',
      'getTicketDataResponse' => 'Axess\\Dci4Wtp\\getTicketDataResponse',
      'checkWTPNo' => 'Axess\\Dci4Wtp\\checkWTPNo',
      'checkWTPNoResponse' => 'Axess\\Dci4Wtp\\checkWTPNoResponse',
      'checkWTPNo2' => 'Axess\\Dci4Wtp\\checkWTPNo2',
      'checkWTPNo2Response' => 'Axess\\Dci4Wtp\\checkWTPNo2Response',
      'checkWTPNo3' => 'Axess\\Dci4Wtp\\checkWTPNo3',
      'checkWTPNo3Response' => 'Axess\\Dci4Wtp\\checkWTPNo3Response',
      'checkWTPNo4' => 'Axess\\Dci4Wtp\\checkWTPNo4',
      'checkWTPNo4Response' => 'Axess\\Dci4Wtp\\checkWTPNo4Response',
      'getPersonData' => 'Axess\\Dci4Wtp\\getPersonData',
      'getPersonDataResponse' => 'Axess\\Dci4Wtp\\getPersonDataResponse',
      'getPersonData2' => 'Axess\\Dci4Wtp\\getPersonData2',
      'getPersonData2Response' => 'Axess\\Dci4Wtp\\getPersonData2Response',
      'getPersonData3' => 'Axess\\Dci4Wtp\\getPersonData3',
      'getPersonData3Response' => 'Axess\\Dci4Wtp\\getPersonData3Response',
      'getPersonData4' => 'Axess\\Dci4Wtp\\getPersonData4',
      'getPersonData4Response' => 'Axess\\Dci4Wtp\\getPersonData4Response',
      'getPersonData5' => 'Axess\\Dci4Wtp\\getPersonData5',
      'getPersonData5Response' => 'Axess\\Dci4Wtp\\getPersonData5Response',
      'getPersonData6' => 'Axess\\Dci4Wtp\\getPersonData6',
      'getPersonData6Response' => 'Axess\\Dci4Wtp\\getPersonData6Response',
      'CTCONFIGRESULT' => 'Axess\\Dci4Wtp\\CTCONFIGRESULT',
      'D4WTPCHECKPROMOCODESREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCHECKPROMOCODESREQUEST',
      'ArrayOfD4WTPCHECKPROMOCODES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCHECKPROMOCODES',
      'D4WTPCHECKPROMOCODES' => 'Axess\\Dci4Wtp\\D4WTPCHECKPROMOCODES',
      'D4WTPPRODUCT' => 'Axess\\Dci4Wtp\\D4WTPPRODUCT',
      'D4WTPCHECKPROMOCODESRESULT' => 'Axess\\Dci4Wtp\\D4WTPCHECKPROMOCODESRESULT',
      'ArrayOfD4WTPPROMOCODERESULT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPROMOCODERESULT',
      'D4WTPPROMOCODERESULT' => 'Axess\\Dci4Wtp\\D4WTPPROMOCODERESULT',
      'D4WTPEDITORDERREQ' => 'Axess\\Dci4Wtp\\D4WTPEDITORDERREQ',
      'ADDRESS' => 'Axess\\Dci4Wtp\\ADDRESS',
      'D4WTPCOMPANY' => 'Axess\\Dci4Wtp\\D4WTPCOMPANY',
      'D4WTPERSONDATA3' => 'Axess\\Dci4Wtp\\D4WTPERSONDATA3',
      'ArrayOfD4WTPMEMBERTYPE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPMEMBERTYPE',
      'D4WTPMEMBERTYPE' => 'Axess\\Dci4Wtp\\D4WTPMEMBERTYPE',
      'D4WTPMEMBER' => 'Axess\\Dci4Wtp\\D4WTPMEMBER',
      'D4WTPMEMBERDETAILS' => 'Axess\\Dci4Wtp\\D4WTPMEMBERDETAILS',
      'D4WTPPERSRENTAL' => 'Axess\\Dci4Wtp\\D4WTPPERSRENTAL',
      'D4WTPEDITORDERRES' => 'Axess\\Dci4Wtp\\D4WTPEDITORDERRES',
      'D4WTPEDITORDERPOSREQ' => 'Axess\\Dci4Wtp\\D4WTPEDITORDERPOSREQ',
      'ArrayOfDCI4WTPORDERPOSITION' => 'Axess\\Dci4Wtp\\ArrayOfDCI4WTPORDERPOSITION',
      'DCI4WTPORDERPOSITION' => 'Axess\\Dci4Wtp\\DCI4WTPORDERPOSITION',
      'ArrayOfD4WTPADDARTICLEINFO' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPADDARTICLEINFO',
      'D4WTPADDARTICLEINFO' => 'Axess\\Dci4Wtp\\D4WTPADDARTICLEINFO',
      'D4WTPEDITORDERPOSRES' => 'Axess\\Dci4Wtp\\D4WTPEDITORDERPOSRES',
      'ArrayOfD4WTPEDITORDERPOSITION' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPEDITORDERPOSITION',
      'D4WTPEDITORDERPOSITION' => 'Axess\\Dci4Wtp\\D4WTPEDITORDERPOSITION',
      'D4WTPCHIPCARDENTRIESREQ' => 'Axess\\Dci4Wtp\\D4WTPCHIPCARDENTRIESREQ',
      'D4WTPCHIPCARDENTRIESRES' => 'Axess\\Dci4Wtp\\D4WTPCHIPCARDENTRIESRES',
      'ArrayOfD4WTPCHIPCARD' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCHIPCARD',
      'D4WTPCHIPCARD' => 'Axess\\Dci4Wtp\\D4WTPCHIPCARD',
      'D4WTPGETADDCASHIERSREQ' => 'Axess\\Dci4Wtp\\D4WTPGETADDCASHIERSREQ',
      'D4WTPGETADDCASHIERSRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETADDCASHIERSRESULT',
      'ArrayOfD4WTPADDCASHIER' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPADDCASHIER',
      'D4WTPADDCASHIER' => 'Axess\\Dci4Wtp\\D4WTPADDCASHIER',
      'ArrayOfD4WTPCASHIERRIGHTS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCASHIERRIGHTS',
      'D4WTPCASHIERRIGHTS' => 'Axess\\Dci4Wtp\\D4WTPCASHIERRIGHTS',
      'D4WTPGETSALESTRANSREQ' => 'Axess\\Dci4Wtp\\D4WTPGETSALESTRANSREQ',
      'D4WTPGETSALESTRANSRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETSALESTRANSRESULT',
      'ArrayOfD4WTPSALESTRANS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSALESTRANS',
      'D4WTPSALESTRANS' => 'Axess\\Dci4Wtp\\D4WTPSALESTRANS',
      'ArrayOfD4WTPSALESDETAIL' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSALESDETAIL',
      'D4WTPSALESDETAIL' => 'Axess\\Dci4Wtp\\D4WTPSALESDETAIL',
      'D4WTPGETRENTALITEMTARIFFREQ' => 'Axess\\Dci4Wtp\\D4WTPGETRENTALITEMTARIFFREQ',
      'D4WTPRENTALITEMPRODUCT' => 'Axess\\Dci4Wtp\\D4WTPRENTALITEMPRODUCT',
      'D4WTPGETRENTALITEMTARIFFRES' => 'Axess\\Dci4Wtp\\D4WTPGETRENTALITEMTARIFFRES',
      'ArrayOfD4WTPRENTALITEMTARIFF' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRENTALITEMTARIFF',
      'D4WTPRENTALITEMTARIFF' => 'Axess\\Dci4Wtp\\D4WTPRENTALITEMTARIFF',
      'D4WTPGETRENTALITEMTARIFF2REQ' => 'Axess\\Dci4Wtp\\D4WTPGETRENTALITEMTARIFF2REQ',
      'D4WTPRENTALITEMPRODUCT2' => 'Axess\\Dci4Wtp\\D4WTPRENTALITEMPRODUCT2',
      'D4WTPGETRENTALITEMTARIFF2RES' => 'Axess\\Dci4Wtp\\D4WTPGETRENTALITEMTARIFF2RES',
      'ArrayOfD4WTPRENTALITEMTARIFF2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRENTALITEMTARIFF2',
      'D4WTPRENTALITEMTARIFF2' => 'Axess\\Dci4Wtp\\D4WTPRENTALITEMTARIFF2',
      'D4WTPGETRENTALITEMTARIFF3RES' => 'Axess\\Dci4Wtp\\D4WTPGETRENTALITEMTARIFF3RES',
      'ArrayOfD4WTPRENTALITEMTARIFF3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRENTALITEMTARIFF3',
      'D4WTPRENTALITEMTARIFF3' => 'Axess\\Dci4Wtp\\D4WTPRENTALITEMTARIFF3',
      'D4WTPGETRENTALITEMTARIFF3REQ' => 'Axess\\Dci4Wtp\\D4WTPGETRENTALITEMTARIFF3REQ',
      'ArrayOfD4WTPRENTALITEMPRODUCT5' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRENTALITEMPRODUCT5',
      'D4WTPRENTALITEMPRODUCT5' => 'Axess\\Dci4Wtp\\D4WTPRENTALITEMPRODUCT5',
      'ArrayOfDCI4WTPGETWEBRENTALBYPROP' => 'Axess\\Dci4Wtp\\ArrayOfDCI4WTPGETWEBRENTALBYPROP',
      'DCI4WTPGETWEBRENTALBYPROP' => 'Axess\\Dci4Wtp\\DCI4WTPGETWEBRENTALBYPROP',
      'D4WTPPERSRENTALSALESREQ' => 'Axess\\Dci4Wtp\\D4WTPPERSRENTALSALESREQ',
      'D4WTPGETPERSRENTALSALESRES' => 'Axess\\Dci4Wtp\\D4WTPGETPERSRENTALSALESRES',
      'ArrayOfD4WTPRENTALSALE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRENTALSALE',
      'D4WTPRENTALSALE' => 'Axess\\Dci4Wtp\\D4WTPRENTALSALE',
      'ArrayOfD4WTPRENTALPRODUCT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRENTALPRODUCT',
      'D4WTPRENTALPRODUCT' => 'Axess\\Dci4Wtp\\D4WTPRENTALPRODUCT',
      'D4WTPWEBRENTALBYPROPERTYREQ' => 'Axess\\Dci4Wtp\\D4WTPWEBRENTALBYPROPERTYREQ',
      'D4WTPWEBRENTALBYPROPERTYRES' => 'Axess\\Dci4Wtp\\D4WTPWEBRENTALBYPROPERTYRES',
      'ArrayOfDCI4WTPGETWEBRENTALBYPRO' => 'Axess\\Dci4Wtp\\ArrayOfDCI4WTPGETWEBRENTALBYPRO',
      'DCI4WTPGETWEBRENTALBYPRO' => 'Axess\\Dci4Wtp\\DCI4WTPGETWEBRENTALBYPRO',
      'ArrayOfDCI4WTPGETWEBRIBYPROPOV' => 'Axess\\Dci4Wtp\\ArrayOfDCI4WTPGETWEBRIBYPROPOV',
      'DCI4WTPGETWEBRIBYPROPOV' => 'Axess\\Dci4Wtp\\DCI4WTPGETWEBRIBYPROPOV',
      'D4WTPFINDSHOPCARTSREQUEST' => 'Axess\\Dci4Wtp\\D4WTPFINDSHOPCARTSREQUEST',
      'D4WTPFINDSHOPCARTSRESULT' => 'Axess\\Dci4Wtp\\D4WTPFINDSHOPCARTSRESULT',
      'ArrayOfD4WTPSHOPCARTFOUND' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCARTFOUND',
      'D4WTPSHOPCARTFOUND' => 'Axess\\Dci4Wtp\\D4WTPSHOPCARTFOUND',
      'D4WTPLOCKSHOPCARTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPLOCKSHOPCARTREQUEST',
      'D4WTPRESULT' => 'Axess\\Dci4Wtp\\D4WTPRESULT',
      'D4WTPCREATEEXTORDERNRREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCREATEEXTORDERNRREQUEST',
      'ArrayOfD4WTPEXTORDERGROUP' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPEXTORDERGROUP',
      'D4WTPEXTORDERGROUP' => 'Axess\\Dci4Wtp\\D4WTPEXTORDERGROUP',
      'ArrayOfD4WTPTICKETSERIAL' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETSERIAL',
      'D4WTPTICKETSERIAL' => 'Axess\\Dci4Wtp\\D4WTPTICKETSERIAL',
      'D4WTPCREATEEXTORDERNRRESULT' => 'Axess\\Dci4Wtp\\D4WTPCREATEEXTORDERNRRESULT',
      'ArrayOfD4WTPEXTORDERGROUPRESULT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPEXTORDERGROUPRESULT',
      'D4WTPEXTORDERGROUPRESULT' => 'Axess\\Dci4Wtp\\D4WTPEXTORDERGROUPRESULT',
      'ArrayOfD4WTPTICKETEXTORDERNRRESULT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETEXTORDERNRRESULT',
      'D4WTPTICKETEXTORDERNRRESULT' => 'Axess\\Dci4Wtp\\D4WTPTICKETEXTORDERNRRESULT',
      'D4WTPISSUESHOPCARTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUESHOPCARTREQUEST',
      'D4WTPISSUESHOPCARTRESULT' => 'Axess\\Dci4Wtp\\D4WTPISSUESHOPCARTRESULT',
      'D4WTPASSIGNPERSTOCOMPANYREQ' => 'Axess\\Dci4Wtp\\D4WTPASSIGNPERSTOCOMPANYREQ',
      'D4WTPGETPERSFROMCOMPANYREQ' => 'Axess\\Dci4Wtp\\D4WTPGETPERSFROMCOMPANYREQ',
      'D4WTPGETPERSFROMCMPNYRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETPERSFROMCMPNYRESULT',
      'ArrayOfD4WTPERSONDATA5' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPERSONDATA5',
      'D4WTPERSONDATA5' => 'Axess\\Dci4Wtp\\D4WTPERSONDATA5',
      'D4WTPMANAGEWORKORDERREQUEST' => 'Axess\\Dci4Wtp\\D4WTPMANAGEWORKORDERREQUEST',
      'D4WTPMANAGEEMPLOYEE' => 'Axess\\Dci4Wtp\\D4WTPMANAGEEMPLOYEE',
      'ArrayOfD4WTPEMPLOYEE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPEMPLOYEE',
      'D4WTPEMPLOYEE' => 'Axess\\Dci4Wtp\\D4WTPEMPLOYEE',
      'D4WTPGETWORKORDERSREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETWORKORDERSREQUEST',
      'D4WTGETWORKORDERSRESULT' => 'Axess\\Dci4Wtp\\D4WTGETWORKORDERSRESULT',
      'ArrayOfD4WTPWORKORDER' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWORKORDER',
      'D4WTPWORKORDER' => 'Axess\\Dci4Wtp\\D4WTPWORKORDER',
      'ArrayOfD4WTPEMPLOYEEASSIGNED' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPEMPLOYEEASSIGNED',
      'D4WTPEMPLOYEEASSIGNED' => 'Axess\\Dci4Wtp\\D4WTPEMPLOYEEASSIGNED',
      'D4WTPCONTACTPERSON' => 'Axess\\Dci4Wtp\\D4WTPCONTACTPERSON',
      'D4WTPREFUNDTICKETREQUEST' => 'Axess\\Dci4Wtp\\D4WTPREFUNDTICKETREQUEST',
      'D4WTPREFUNDTICKETRESULT' => 'Axess\\Dci4Wtp\\D4WTPREFUNDTICKETRESULT',
      'ArrayOfD4WTPCANCELARTICLERESULT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCANCELARTICLERESULT',
      'D4WTPCANCELARTICLERESULT' => 'Axess\\Dci4Wtp\\D4WTPCANCELARTICLERESULT',
      'D4WTPREFUNDTICKET2REQUEST' => 'Axess\\Dci4Wtp\\D4WTPREFUNDTICKET2REQUEST',
      'ArrayOfD4WTPTICKET' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKET',
      'D4WTPTICKET' => 'Axess\\Dci4Wtp\\D4WTPTICKET',
      'D4WTPREFUNDTICKET2RESULT' => 'Axess\\Dci4Wtp\\D4WTPREFUNDTICKET2RESULT',
      'ArrayOfD4WTPTICKETTARIFINFO' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETTARIFINFO',
      'D4WTPTICKETTARIFINFO' => 'Axess\\Dci4Wtp\\D4WTPTICKETTARIFINFO',
      'ArrayOfD4WTPCANCELARTICLE2RESULT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCANCELARTICLE2RESULT',
      'D4WTPCANCELARTICLE2RESULT' => 'Axess\\Dci4Wtp\\D4WTPCANCELARTICLE2RESULT',
      'D4WTPMSGREFUNDTICKETREQUEST' => 'Axess\\Dci4Wtp\\D4WTPMSGREFUNDTICKETREQUEST',
      'ArrayOfD4WTPTRANSACTION' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTRANSACTION',
      'D4WTPTRANSACTION' => 'Axess\\Dci4Wtp\\D4WTPTRANSACTION',
      'D4WTPGETTRAVELGRPLISTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETTRAVELGRPLISTREQUEST',
      'D4WTPGETTRAVELGRPLISTRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETTRAVELGRPLISTRESULT',
      'ArrayOfD4WTPTRAVELGROUPLIST' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTRAVELGROUPLIST',
      'D4WTPTRAVELGROUPLIST' => 'Axess\\Dci4Wtp\\D4WTPTRAVELGROUPLIST',
      'ArrayOfD4WTPSUBCONTDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSUBCONTDATA',
      'D4WTPSUBCONTDATA' => 'Axess\\Dci4Wtp\\D4WTPSUBCONTDATA',
      'ArrayOfD4WTPTICKETTYPELIST' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETTYPELIST',
      'D4WTPTICKETTYPELIST' => 'Axess\\Dci4Wtp\\D4WTPTICKETTYPELIST',
      'D4WTPGETTRAVELGRPLISTRES2' => 'Axess\\Dci4Wtp\\D4WTPGETTRAVELGRPLISTRES2',
      'ArrayOfD4WTPTRAVELGROUPLIST2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTRAVELGROUPLIST2',
      'D4WTPTRAVELGROUPLIST2' => 'Axess\\Dci4Wtp\\D4WTPTRAVELGROUPLIST2',
      'ArrayOfD4WTPBOOKTIMESLOTINFOS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPBOOKTIMESLOTINFOS',
      'D4WTPBOOKTIMESLOTINFOS' => 'Axess\\Dci4Wtp\\D4WTPBOOKTIMESLOTINFOS',
      'D4WTPGETGRPSUBCONTINGENTREQ' => 'Axess\\Dci4Wtp\\D4WTPGETGRPSUBCONTINGENTREQ',
      'D4WTPGETGRPSUBCONTINGENTRES' => 'Axess\\Dci4Wtp\\D4WTPGETGRPSUBCONTINGENTRES',
      'ArrayOfD4WTPSUBCONTINGENTRES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSUBCONTINGENTRES',
      'D4WTPSUBCONTINGENTRES' => 'Axess\\Dci4Wtp\\D4WTPSUBCONTINGENTRES',
      'ArrayOfD4WTPTRAVELGROUPRES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTRAVELGROUPRES',
      'D4WTPTRAVELGROUPRES' => 'Axess\\Dci4Wtp\\D4WTPTRAVELGROUPRES',
      'D4WTPGETGRPSUBCONTINGENT2RES' => 'Axess\\Dci4Wtp\\D4WTPGETGRPSUBCONTINGENT2RES',
      'ArrayOfD4WTPSUBCONTINGENTRES2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSUBCONTINGENTRES2',
      'D4WTPSUBCONTINGENTRES2' => 'Axess\\Dci4Wtp\\D4WTPSUBCONTINGENTRES2',
      'ArrayOfD4WTPTRAVELGROUPRES2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTRAVELGROUPRES2',
      'D4WTPTRAVELGROUPRES2' => 'Axess\\Dci4Wtp\\D4WTPTRAVELGROUPRES2',
      'D4WTPGETGRPSUBCONTINGENTREQ2' => 'Axess\\Dci4Wtp\\D4WTPGETGRPSUBCONTINGENTREQ2',
      'ArrayOfDATE' => 'Axess\\Dci4Wtp\\ArrayOfDATE',
      'DATE' => 'Axess\\Dci4Wtp\\DATE',
      'D4WTPGETGRPSUBCONTINGENT3RES' => 'Axess\\Dci4Wtp\\D4WTPGETGRPSUBCONTINGENT3RES',
      'ArrayOfD4WTPSUBCONTINGENTRES3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSUBCONTINGENTRES3',
      'D4WTPSUBCONTINGENTRES3' => 'Axess\\Dci4Wtp\\D4WTPSUBCONTINGENTRES3',
      'D4WTPCHECKBONUSPOINTSREQ' => 'Axess\\Dci4Wtp\\D4WTPCHECKBONUSPOINTSREQ',
      'ArrayOfD4WTPPRODUCTORDER7' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPRODUCTORDER7',
      'D4WTPPRODUCTORDER7' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTORDER7',
      'ArrayOfD4WTPADDARTICLE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPADDARTICLE',
      'D4WTPADDARTICLE' => 'Axess\\Dci4Wtp\\D4WTPADDARTICLE',
      'ArrayOfD4WTPDCCONTENT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPDCCONTENT',
      'D4WTPDCCONTENT' => 'Axess\\Dci4Wtp\\D4WTPDCCONTENT',
      'ArrayOfD4WTPPARKING' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPARKING',
      'D4WTPPARKING' => 'Axess\\Dci4Wtp\\D4WTPPARKING',
      'ArrayOfD4WTPERSONDATA4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPERSONDATA4',
      'D4WTPERSONDATA4' => 'Axess\\Dci4Wtp\\D4WTPERSONDATA4',
      'ArrayOfD4WTPSUBCONTINGENT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSUBCONTINGENT',
      'D4WTPSUBCONTINGENT' => 'Axess\\Dci4Wtp\\D4WTPSUBCONTINGENT',
      'ArrayOfD4WTPWTPNO' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWTPNO',
      'D4WTPWTPNO' => 'Axess\\Dci4Wtp\\D4WTPWTPNO',
      'D4WTPPACKAGEPOSPRODUCT' => 'Axess\\Dci4Wtp\\D4WTPPACKAGEPOSPRODUCT',
      'D4WTPRENTALPRODUCTORDER' => 'Axess\\Dci4Wtp\\D4WTPRENTALPRODUCTORDER',
      'ArrayOfD4WTPRENTALITEMPRODUCT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRENTALITEMPRODUCT',
      'D4WTPCHECKBONUSPOINTSRES' => 'Axess\\Dci4Wtp\\D4WTPCHECKBONUSPOINTSRES',
      'ArrayOfD4WTPBLOCKSATZ' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPBLOCKSATZ',
      'D4WTPBLOCKSATZ' => 'Axess\\Dci4Wtp\\D4WTPBLOCKSATZ',
      'D4WTPDISCOUNT' => 'Axess\\Dci4Wtp\\D4WTPDISCOUNT',
      'D4WTPCONFIRMBONUSPOINTSREQ' => 'Axess\\Dci4Wtp\\D4WTPCONFIRMBONUSPOINTSREQ',
      'D4WTPGETDSGVOFLAGSREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETDSGVOFLAGSREQUEST',
      'ArrayOfD4WTPPERSONTRIPLE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPERSONTRIPLE',
      'D4WTPPERSONTRIPLE' => 'Axess\\Dci4Wtp\\D4WTPPERSONTRIPLE',
      'D4WTPGETDSGVOFLAGSRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETDSGVOFLAGSRESULT',
      'ArrayOfD4WTPDSGVOFLAGS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPDSGVOFLAGS',
      'D4WTPDSGVOFLAGS' => 'Axess\\Dci4Wtp\\D4WTPDSGVOFLAGS',
      'D4WTPDEFAULTDSGVOFLAGS' => 'Axess\\Dci4Wtp\\D4WTPDEFAULTDSGVOFLAGS',
      'D4WTPSETDSGVOFLAGSREQUEST' => 'Axess\\Dci4Wtp\\D4WTPSETDSGVOFLAGSREQUEST',
      'D4WTPUPLOADPRIVACYDOCREQ' => 'Axess\\Dci4Wtp\\D4WTPUPLOADPRIVACYDOCREQ',
      'D4WTPUPLOADPRIVACYDOCRES' => 'Axess\\Dci4Wtp\\D4WTPUPLOADPRIVACYDOCRES',
      'D4WTPCREATEWTPDATAREQ' => 'Axess\\Dci4Wtp\\D4WTPCREATEWTPDATAREQ',
      'ArrayOfD4WTPWTPDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWTPDATA',
      'D4WTPWTPDATA' => 'Axess\\Dci4Wtp\\D4WTPWTPDATA',
      'D4WTPCREATEWTPDATARES' => 'Axess\\Dci4Wtp\\D4WTPCREATEWTPDATARES',
      'D4WTPSENDWTPDATAREQ' => 'Axess\\Dci4Wtp\\D4WTPSENDWTPDATAREQ',
      'D4WTPSENDWTPDATARES' => 'Axess\\Dci4Wtp\\D4WTPSENDWTPDATARES',
      'ArrayOfD4WTPWTPDATAOCC' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWTPDATAOCC',
      'D4WTPWTPDATAOCC' => 'Axess\\Dci4Wtp\\D4WTPWTPDATAOCC',
      'D4WTPADDDTL4FAMILYMEMBERREQ' => 'Axess\\Dci4Wtp\\D4WTPADDDTL4FAMILYMEMBERREQ',
      'ArrayOfD4WTPMEDIA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPMEDIA',
      'D4WTPMEDIA' => 'Axess\\Dci4Wtp\\D4WTPMEDIA',
      'D4WTPADDDTL4FAMILYMEMBERRES' => 'Axess\\Dci4Wtp\\D4WTPADDDTL4FAMILYMEMBERRES',
      'ArrayOfD4WTPMEDIAADDED' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPMEDIAADDED',
      'D4WTPMEDIAADDED' => 'Axess\\Dci4Wtp\\D4WTPMEDIAADDED',
      'D4WTPGETADDDAYSTARIFFSREQ' => 'Axess\\Dci4Wtp\\D4WTPGETADDDAYSTARIFFSREQ',
      'D4WTPGETADDDAYSTARIFFSRES' => 'Axess\\Dci4Wtp\\D4WTPGETADDDAYSTARIFFSRES',
      'ArrayOfD4WTPEXTENSIONTARIFF' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPEXTENSIONTARIFF',
      'D4WTPEXTENSIONTARIFF' => 'Axess\\Dci4Wtp\\D4WTPEXTENSIONTARIFF',
      'D4WTPADDDTL4FAMILYDAYSREQ' => 'Axess\\Dci4Wtp\\D4WTPADDDTL4FAMILYDAYSREQ',
      'D4WTPADDDTL4FAMILYDAYSRES' => 'Axess\\Dci4Wtp\\D4WTPADDDTL4FAMILYDAYSRES',
      'D4WTPTICKETPRODDATA5' => 'Axess\\Dci4Wtp\\D4WTPTICKETPRODDATA5',
      'D4WTPCODINGDATA' => 'Axess\\Dci4Wtp\\D4WTPCODINGDATA',
      'D4WTPPRINTINGDATA' => 'Axess\\Dci4Wtp\\D4WTPPRINTINGDATA',
      'DTL4FAMCODINGREQ' => 'Axess\\Dci4Wtp\\DTL4FAMCODINGREQ',
      'DTL4FAMCODINGRESULT' => 'Axess\\Dci4Wtp\\DTL4FAMCODINGRESULT',
      'ArrayOfDTL4FAMCODINGMASTER' => 'Axess\\Dci4Wtp\\ArrayOfDTL4FAMCODINGMASTER',
      'DTL4FAMCODINGMASTER' => 'Axess\\Dci4Wtp\\DTL4FAMCODINGMASTER',
      'ArrayOfDTL4FAMCODINGMEMBER' => 'Axess\\Dci4Wtp\\ArrayOfDTL4FAMCODINGMEMBER',
      'DTL4FAMCODINGMEMBER' => 'Axess\\Dci4Wtp\\DTL4FAMCODINGMEMBER',
      'ArrayOfDTL4FAMCODINGTICKETUSAGE' => 'Axess\\Dci4Wtp\\ArrayOfDTL4FAMCODINGTICKETUSAGE',
      'DTL4FAMCODINGTICKETUSAGE' => 'Axess\\Dci4Wtp\\DTL4FAMCODINGTICKETUSAGE',
      'BLANKTYPESRES' => 'Axess\\Dci4Wtp\\BLANKTYPESRES',
      'ArrayOfD4WTPBLANKTYPEEN' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPBLANKTYPEEN',
      'D4WTPBLANKTYPEEN' => 'Axess\\Dci4Wtp\\D4WTPBLANKTYPEEN',
      'D4AXCOUNTSTEPREQ' => 'Axess\\Dci4Wtp\\D4AXCOUNTSTEPREQ',
      'D4AXCOUNTSTEPRES' => 'Axess\\Dci4Wtp\\D4AXCOUNTSTEPRES',
      'D4WTPEMONEYACCOUNTDETAILSREQ' => 'Axess\\Dci4Wtp\\D4WTPEMONEYACCOUNTDETAILSREQ',
      'D4WTPEMONEYACCOUNT' => 'Axess\\Dci4Wtp\\D4WTPEMONEYACCOUNT',
      'D4WTPEMONEYACCOUNTDETAILSRES' => 'Axess\\Dci4Wtp\\D4WTPEMONEYACCOUNTDETAILSRES',
      'ArrayOfD4WTPAXCOUNTMEMBER' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPAXCOUNTMEMBER',
      'D4WTPAXCOUNTMEMBER' => 'Axess\\Dci4Wtp\\D4WTPAXCOUNTMEMBER',
      'D4WTPEMONEYACCOUNTREQ' => 'Axess\\Dci4Wtp\\D4WTPEMONEYACCOUNTREQ',
      'D4WTPEMONEYACCOUNTRES' => 'Axess\\Dci4Wtp\\D4WTPEMONEYACCOUNTRES',
      'D4WTPEMONEYACCOUNTREQ2' => 'Axess\\Dci4Wtp\\D4WTPEMONEYACCOUNTREQ2',
      'D4WTPEMONEYPAYINOUTREQ' => 'Axess\\Dci4Wtp\\D4WTPEMONEYPAYINOUTREQ',
      'D4WTPEMONEYPAYINOUTRES' => 'Axess\\Dci4Wtp\\D4WTPEMONEYPAYINOUTRES',
      'D4WTPEMONEYPAYTRANSACTIONREQ' => 'Axess\\Dci4Wtp\\D4WTPEMONEYPAYTRANSACTIONREQ',
      'D4WTPEMONEYPAYTRANSACTIONRES' => 'Axess\\Dci4Wtp\\D4WTPEMONEYPAYTRANSACTIONRES',
      'D4WTPEMONEYPAYTRANS2REQ' => 'Axess\\Dci4Wtp\\D4WTPEMONEYPAYTRANS2REQ',
      'ArrayOfD4WTPPAYMENTTYPE2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPAYMENTTYPE2',
      'D4WTPPAYMENTTYPE2' => 'Axess\\Dci4Wtp\\D4WTPPAYMENTTYPE2',
      'D4WTPEMONEYPAYTRANS2RES' => 'Axess\\Dci4Wtp\\D4WTPEMONEYPAYTRANS2RES',
      'ArrayOfD4WTPEMONEYACCOUNT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPEMONEYACCOUNT',
      'D4WTPHISTORYREQ' => 'Axess\\Dci4Wtp\\D4WTPHISTORYREQ',
      'D4WTPHISTORYRES' => 'Axess\\Dci4Wtp\\D4WTPHISTORYRES',
      'ArrayOfD4WTPROW' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPROW',
      'D4WTPROW' => 'Axess\\Dci4Wtp\\D4WTPROW',
      'D4WTPTRANSATTRIBUTESREQ' => 'Axess\\Dci4Wtp\\D4WTPTRANSATTRIBUTESREQ',
      'ArrayOfATTRIBUTE' => 'Axess\\Dci4Wtp\\ArrayOfATTRIBUTE',
      'ATTRIBUTE' => 'Axess\\Dci4Wtp\\ATTRIBUTE',
      'D4WTPTRANSATTRIBUTESRES' => 'Axess\\Dci4Wtp\\D4WTPTRANSATTRIBUTESRES',
      'D4WTPSWISSPASSREGREQ' => 'Axess\\Dci4Wtp\\D4WTPSWISSPASSREGREQ',
      'D4WTPSWISSPASSREGRES' => 'Axess\\Dci4Wtp\\D4WTPSWISSPASSREGRES',
      'D4WTPSWISSPASSSALESREQ' => 'Axess\\Dci4Wtp\\D4WTPSWISSPASSSALESREQ',
      'D4WTPSWISSPASSSALESRES' => 'Axess\\Dci4Wtp\\D4WTPSWISSPASSSALESRES',
      'ArrayOfSWISSTICKETSALE' => 'Axess\\Dci4Wtp\\ArrayOfSWISSTICKETSALE',
      'SWISSTICKETSALE' => 'Axess\\Dci4Wtp\\SWISSTICKETSALE',
      'D4WTPSAVEVOUCHER' => 'Axess\\Dci4Wtp\\D4WTPSAVEVOUCHER',
      'D4WTPGETFIRSTVALIDDAYREQ' => 'Axess\\Dci4Wtp\\D4WTPGETFIRSTVALIDDAYREQ',
      'ArrayOfD4WTPPACKAGE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPACKAGE',
      'D4WTPPACKAGE' => 'Axess\\Dci4Wtp\\D4WTPPACKAGE',
      'ArrayOfD4WTPPRODUCT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPRODUCT',
      'D4WTPGETFIRSTVALIDDAYRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETFIRSTVALIDDAYRESULT',
      'ArrayOfD4WTPVALIDDAYREC' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPVALIDDAYREC',
      'D4WTPVALIDDAYREC' => 'Axess\\Dci4Wtp\\D4WTPVALIDDAYREC',
      'D4WTPCOMPENSATIONTICKETREQ' => 'Axess\\Dci4Wtp\\D4WTPCOMPENSATIONTICKETREQ',
      'ArrayOfD4WTPPCOMPTICKET' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPCOMPTICKET',
      'D4WTPPCOMPTICKET' => 'Axess\\Dci4Wtp\\D4WTPPCOMPTICKET',
      'ArrayOfD4WTPCOMPENSATIONTICKET' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCOMPENSATIONTICKET',
      'D4WTPCOMPENSATIONTICKET' => 'Axess\\Dci4Wtp\\D4WTPCOMPENSATIONTICKET',
      'D4WTPCOMPENSATIONTICKETRES' => 'Axess\\Dci4Wtp\\D4WTPCOMPENSATIONTICKETRES',
      'D4WTPGETCOMTICREQ' => 'Axess\\Dci4Wtp\\D4WTPGETCOMTICREQ',
      'ArrayOfCOMPTICKET' => 'Axess\\Dci4Wtp\\ArrayOfCOMPTICKET',
      'COMPTICKET' => 'Axess\\Dci4Wtp\\COMPTICKET',
      'D4WTPGETCOMTICRES' => 'Axess\\Dci4Wtp\\D4WTPGETCOMTICRES',
      'D4WTPGETCONTINGENTOCUPACYREQ' => 'Axess\\Dci4Wtp\\D4WTPGETCONTINGENTOCUPACYREQ',
      'D4WTPGETCONTINGENTOCUPACYRES' => 'Axess\\Dci4Wtp\\D4WTPGETCONTINGENTOCUPACYRES',
      'ArrayOfD4WTPCONTINGENTOCCUPACY' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCONTINGENTOCCUPACY',
      'D4WTPCONTINGENTOCCUPACY' => 'Axess\\Dci4Wtp\\D4WTPCONTINGENTOCCUPACY',
      'D4WTPGETCNTNGNTOCUPACYREQ2' => 'Axess\\Dci4Wtp\\D4WTPGETCNTNGNTOCUPACYREQ2',
      'ArrayOfD4WTPPOSLIST' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPOSLIST',
      'D4WTPPOSLIST' => 'Axess\\Dci4Wtp\\D4WTPPOSLIST',
      'D4WTPGETDAYOCUPACYREQ' => 'Axess\\Dci4Wtp\\D4WTPGETDAYOCUPACYREQ',
      'D4WTPGETDAYOCUPACYRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETDAYOCUPACYRESULT',
      'ArrayOfD4WTPDAYOCCUPACY' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPDAYOCCUPACY',
      'D4WTPDAYOCCUPACY' => 'Axess\\Dci4Wtp\\D4WTPDAYOCCUPACY',
      'ArrayOfD4WTPOCCUPACYRESERVATIONS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPOCCUPACYRESERVATIONS',
      'D4WTPOCCUPACYRESERVATIONS' => 'Axess\\Dci4Wtp\\D4WTPOCCUPACYRESERVATIONS',
      'D4WTPGETDAYOCUPACYREQ2' => 'Axess\\Dci4Wtp\\D4WTPGETDAYOCUPACYREQ2',
      'D4WTPGETDAYOCUPACYRESULT2' => 'Axess\\Dci4Wtp\\D4WTPGETDAYOCUPACYRESULT2',
      'ArrayOfD4WTPDAYOCCUPACY2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPDAYOCCUPACY2',
      'D4WTPDAYOCCUPACY2' => 'Axess\\Dci4Wtp\\D4WTPDAYOCCUPACY2',
      'ArrayOfD4WTPOCCUPACYRESERVATIONS2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPOCCUPACYRESERVATIONS2',
      'D4WTPOCCUPACYRESERVATIONS2' => 'Axess\\Dci4Wtp\\D4WTPOCCUPACYRESERVATIONS2',
      'ArrayOfD4WTPOCCUPACYSALES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPOCCUPACYSALES',
      'D4WTPOCCUPACYSALES' => 'Axess\\Dci4Wtp\\D4WTPOCCUPACYSALES',
      'D4WTPGETRMPROFILESREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETRMPROFILESREQUEST',
      'D4WTPGETRMPROFILESRESPONSE' => 'Axess\\Dci4Wtp\\D4WTPGETRMPROFILESRESPONSE',
      'ArrayOfD4WTPNAMEDUSERS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPNAMEDUSERS',
      'D4WTPNAMEDUSERS' => 'Axess\\Dci4Wtp\\D4WTPNAMEDUSERS',
      'ArrayOfD4WTPUSERROLES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPUSERROLES',
      'D4WTPUSERROLES' => 'Axess\\Dci4Wtp\\D4WTPUSERROLES',
      'D4WTPGETELIGIBILITYDISCREQ' => 'Axess\\Dci4Wtp\\D4WTPGETELIGIBILITYDISCREQ',
      'D4WTPGETELIGIBILITYDISCRES' => 'Axess\\Dci4Wtp\\D4WTPGETELIGIBILITYDISCRES',
      'ArrayOfD4WTPDISCOUNTSHEETDEF' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPDISCOUNTSHEETDEF',
      'D4WTPDISCOUNTSHEETDEF' => 'Axess\\Dci4Wtp\\D4WTPDISCOUNTSHEETDEF',
      'ArrayOfD4WTPDISCOUNTDEF' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPDISCOUNTDEF',
      'D4WTPDISCOUNTDEF' => 'Axess\\Dci4Wtp\\D4WTPDISCOUNTDEF',
      'D4WTPGETCOMPANYBALANCEREQ' => 'Axess\\Dci4Wtp\\D4WTPGETCOMPANYBALANCEREQ',
      'D4WTPGETCOMPANYBALANCERES' => 'Axess\\Dci4Wtp\\D4WTPGETCOMPANYBALANCERES',
      'D4WTPGETPROCESSEDTKTDATAREQ' => 'Axess\\Dci4Wtp\\D4WTPGETPROCESSEDTKTDATAREQ',
      'D4WTPGETPROCESSEDTKTDATARES' => 'Axess\\Dci4Wtp\\D4WTPGETPROCESSEDTKTDATARES',
      'ArrayOfD4WTPGETPROCESSEDTKTDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPGETPROCESSEDTKTDATA',
      'D4WTPGETPROCESSEDTKTDATA' => 'Axess\\Dci4Wtp\\D4WTPGETPROCESSEDTKTDATA',
      'D4WTPREPLACETICKETREQ' => 'Axess\\Dci4Wtp\\D4WTPREPLACETICKETREQ',
      'D4WTPREPLACETICKETRES' => 'Axess\\Dci4Wtp\\D4WTPREPLACETICKETRES',
      'D4WTPREPLACEDTICKET' => 'Axess\\Dci4Wtp\\D4WTPREPLACEDTICKET',
      'D4WTPMSGREPLACETICKETREQ' => 'Axess\\Dci4Wtp\\D4WTPMSGREPLACETICKETREQ',
      'D4WTPCHANGEPERSDATAREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCHANGEPERSDATAREQUEST',
      'D4WTPERSDATAFAMILY' => 'Axess\\Dci4Wtp\\D4WTPERSDATAFAMILY',
      'D4WTPCHANGEPERSDATARESULT' => 'Axess\\Dci4Wtp\\D4WTPCHANGEPERSDATARESULT',
      'D4WTPCHANGEPERSDATA2REQUEST' => 'Axess\\Dci4Wtp\\D4WTPCHANGEPERSDATA2REQUEST',
      'D4WTPCHANGEPERSDATA2RESULT' => 'Axess\\Dci4Wtp\\D4WTPCHANGEPERSDATA2RESULT',
      'D4WTPCHANGEPERSDATA3REQUEST' => 'Axess\\Dci4Wtp\\D4WTPCHANGEPERSDATA3REQUEST',
      'D4WTPERSDATA3FAMILY' => 'Axess\\Dci4Wtp\\D4WTPERSDATA3FAMILY',
      'D4WTPCHANGEPERSDATA3RESULT' => 'Axess\\Dci4Wtp\\D4WTPCHANGEPERSDATA3RESULT',
      'D4WTPCHANGEPERSDATA4REQUEST' => 'Axess\\Dci4Wtp\\D4WTPCHANGEPERSDATA4REQUEST',
      'ArrayOfD4WTPCUSTOMERADDRESS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCUSTOMERADDRESS',
      'D4WTPCUSTOMERADDRESS' => 'Axess\\Dci4Wtp\\D4WTPCUSTOMERADDRESS',
      'D4WTPERSDATA4FAMILY' => 'Axess\\Dci4Wtp\\D4WTPERSDATA4FAMILY',
      'D4WTPCHANGEPERSDATA4RESULT' => 'Axess\\Dci4Wtp\\D4WTPCHANGEPERSDATA4RESULT',
      'D4WTPREADTICKETREQUEST' => 'Axess\\Dci4Wtp\\D4WTPREADTICKETREQUEST',
      'D4WTPREADTICKETRESULT' => 'Axess\\Dci4Wtp\\D4WTPREADTICKETRESULT',
      'ArrayOfD4WTPTICKETCONTENT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETCONTENT',
      'D4WTPTICKETCONTENT' => 'Axess\\Dci4Wtp\\D4WTPTICKETCONTENT',
      'D4WTPERSONDATA' => 'Axess\\Dci4Wtp\\D4WTPERSONDATA',
      'D4WTPCREATEWTP64BITRESULT' => 'Axess\\Dci4Wtp\\D4WTPCREATEWTP64BITRESULT',
      'D4WTPBLOCKTICKET' => 'Axess\\Dci4Wtp\\D4WTPBLOCKTICKET',
      'D4WTPBLOCKUNBLOCKRESULT' => 'Axess\\Dci4Wtp\\D4WTPBLOCKUNBLOCKRESULT',
      'D4WTPREPAIDRESULT' => 'Axess\\Dci4Wtp\\D4WTPREPAIDRESULT',
      'ArrayOfD4WTPPREPAIDTICKET' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPREPAIDTICKET',
      'D4WTPPREPAIDTICKET' => 'Axess\\Dci4Wtp\\D4WTPPREPAIDTICKET',
      'D4WTPREPAIDRESULT2' => 'Axess\\Dci4Wtp\\D4WTPREPAIDRESULT2',
      'ArrayOfD4WTPPREPAIDTICKET2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPREPAIDTICKET2',
      'D4WTPPREPAIDTICKET2' => 'Axess\\Dci4Wtp\\D4WTPPREPAIDTICKET2',
      'D4WTPFINDPREPAIDTICKETREQ' => 'Axess\\Dci4Wtp\\D4WTPFINDPREPAIDTICKETREQ',
      'D4WTPREPAIDRESULT3' => 'Axess\\Dci4Wtp\\D4WTPREPAIDRESULT3',
      'ArrayOfD4WTPPREPAIDTICKET3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPREPAIDTICKET3',
      'D4WTPPREPAIDTICKET3' => 'Axess\\Dci4Wtp\\D4WTPPREPAIDTICKET3',
      'ArrayOfD4WTPPREPAIDARTICLES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPREPAIDARTICLES',
      'D4WTPPREPAIDARTICLES' => 'Axess\\Dci4Wtp\\D4WTPPREPAIDARTICLES',
      'D4WTPFINDPREPAIDTICKETREQ2' => 'Axess\\Dci4Wtp\\D4WTPFINDPREPAIDTICKETREQ2',
      'D4WTPREPAIDRESULT4' => 'Axess\\Dci4Wtp\\D4WTPREPAIDRESULT4',
      'D4WTPRODUCEPREPAIDTICKETRES' => 'Axess\\Dci4Wtp\\D4WTPRODUCEPREPAIDTICKETRES',
      'D4WTPPRODUCEPREPAIDREQ' => 'Axess\\Dci4Wtp\\D4WTPPRODUCEPREPAIDREQ',
      'D4WTPRODUCEPREPAIDTICKETRES3' => 'Axess\\Dci4Wtp\\D4WTPRODUCEPREPAIDTICKETRES3',
      'ArrayOfD4WTPPREPAIDARTICLESPRINT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPREPAIDARTICLESPRINT',
      'D4WTPPREPAIDARTICLESPRINT' => 'Axess\\Dci4Wtp\\D4WTPPREPAIDARTICLESPRINT',
      'D4WTPPRODUCEPREPAIDREQ4' => 'Axess\\Dci4Wtp\\D4WTPPRODUCEPREPAIDREQ4',
      'D4WTPRODUCEPREPAIDTICKETRES4' => 'Axess\\Dci4Wtp\\D4WTPRODUCEPREPAIDTICKETRES4',
      'D4WTPBLANKTYPE' => 'Axess\\Dci4Wtp\\D4WTPBLANKTYPE',
      'D4WTPSEGMENT' => 'Axess\\Dci4Wtp\\D4WTPSEGMENT',
      'D4WTPOCCPICKUPSTATEREQ' => 'Axess\\Dci4Wtp\\D4WTPOCCPICKUPSTATEREQ',
      'D4WTPSWITCHPREPSTATUSREQUEST' => 'Axess\\Dci4Wtp\\D4WTPSWITCHPREPSTATUSREQUEST',
      'D4WTPSWITCHPREPSTAT2REQUEST' => 'Axess\\Dci4Wtp\\D4WTPSWITCHPREPSTAT2REQUEST',
      'D4WTPSWITCHPREPAIDRES2' => 'Axess\\Dci4Wtp\\D4WTPSWITCHPREPAIDRES2',
      'D4WTPMNGPERSPHOTOREQUEST' => 'Axess\\Dci4Wtp\\D4WTPMNGPERSPHOTOREQUEST',
      'D4WTPMNGPERSPHOTORESULT' => 'Axess\\Dci4Wtp\\D4WTPMNGPERSPHOTORESULT',
      'D4WTPDECODE64WTPNORESULT' => 'Axess\\Dci4Wtp\\D4WTPDECODE64WTPNORESULT',
      'D4WTPCHKSKICLUBMBRRESULT' => 'Axess\\Dci4Wtp\\D4WTPCHKSKICLUBMBRRESULT',
      'OBI4PSPAUSWEISDATENANFRAGE' => 'Axess\\Dci4Wtp\\OBI4PSPAUSWEISDATENANFRAGE',
      'OBI4PSPUIDANFRAGE' => 'Axess\\Dci4Wtp\\OBI4PSPUIDANFRAGE',
      'OBI4PSPKUNDENDATENANFRAGE' => 'Axess\\Dci4Wtp\\OBI4PSPKUNDENDATENANFRAGE',
      'OBI4PSPHOLEKUNDENDATENRESULT' => 'Axess\\Dci4Wtp\\OBI4PSPHOLEKUNDENDATENRESULT',
      'OBI4PSPAXKUNDENDATEN' => 'Axess\\Dci4Wtp\\OBI4PSPAXKUNDENDATEN',
      'OBI4PSPKUNDENDATEN' => 'Axess\\Dci4Wtp\\OBI4PSPKUNDENDATEN',
      'OBI4PSPKMWNUTZERADRESSE' => 'Axess\\Dci4Wtp\\OBI4PSPKMWNUTZERADRESSE',
      'OBI4PSPSWISSPASSABOSTATUS' => 'Axess\\Dci4Wtp\\OBI4PSPSWISSPASSABOSTATUS',
      'D4WTPWTPNOLISTRESULT' => 'Axess\\Dci4Wtp\\D4WTPWTPNOLISTRESULT',
      'ArrayOfD4WTPWTPNOLIST' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWTPNOLIST',
      'D4WTPWTPNOLIST' => 'Axess\\Dci4Wtp\\D4WTPWTPNOLIST',
      'D4WTPGETREADERTRANSRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETREADERTRANSRESULT',
      'ArrayOfD4WTPGETREADERTRANS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPGETREADERTRANS',
      'D4WTPGETREADERTRANS' => 'Axess\\Dci4Wtp\\D4WTPGETREADERTRANS',
      'D4WTPGETREADERTRANS2RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETREADERTRANS2RESULT',
      'ArrayOfD4WTPGETREADERTRANS2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPGETREADERTRANS2',
      'D4WTPGETREADERTRANS2' => 'Axess\\Dci4Wtp\\D4WTPGETREADERTRANS2',
      'D4WTPCONTINGENTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCONTINGENTREQUEST',
      'D4WTPGETCONTINGENTRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETCONTINGENTRESULT',
      'ArrayOfD4WTPCONTNGNTTICKET' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCONTNGNTTICKET',
      'D4WTPCONTNGNTTICKET' => 'Axess\\Dci4Wtp\\D4WTPCONTNGNTTICKET',
      'D4WTPCONTINGENT2REQUEST' => 'Axess\\Dci4Wtp\\D4WTPCONTINGENT2REQUEST',
      'D4WTPGETCONTINGENT2RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETCONTINGENT2RESULT',
      'ArrayOfD4WTPCONTNGNTTICKET2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCONTNGNTTICKET2',
      'D4WTPCONTNGNTTICKET2' => 'Axess\\Dci4Wtp\\D4WTPCONTNGNTTICKET2',
      'ArrayOfD4WTPSUBCONTNGNTTICKET2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSUBCONTNGNTTICKET2',
      'D4WTPSUBCONTNGNTTICKET2' => 'Axess\\Dci4Wtp\\D4WTPSUBCONTNGNTTICKET2',
      'D4WTPCONTINGENT3REQUEST' => 'Axess\\Dci4Wtp\\D4WTPCONTINGENT3REQUEST',
      'D4WTPGETCONTINGENT3RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETCONTINGENT3RESULT',
      'ArrayOfD4WTPCONTNGNTTICKET3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCONTNGNTTICKET3',
      'D4WTPCONTNGNTTICKET3' => 'Axess\\Dci4Wtp\\D4WTPCONTNGNTTICKET3',
      'ArrayOfD4WTPSUBCONTNGNTTICKET3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSUBCONTNGNTTICKET3',
      'D4WTPSUBCONTNGNTTICKET3' => 'Axess\\Dci4Wtp\\D4WTPSUBCONTNGNTTICKET3',
      'D4WTPGETCONTINGENT4RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETCONTINGENT4RESULT',
      'ArrayOfD4WTPCONTNGNTTICKET4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCONTNGNTTICKET4',
      'D4WTPCONTNGNTTICKET4' => 'Axess\\Dci4Wtp\\D4WTPCONTNGNTTICKET4',
      'ArrayOfD4WTPSUBCONTNGNTTICKET4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSUBCONTNGNTTICKET4',
      'D4WTPSUBCONTNGNTTICKET4' => 'Axess\\Dci4Wtp\\D4WTPSUBCONTNGNTTICKET4',
      'D4WTPCONTINGENT5REQUEST' => 'Axess\\Dci4Wtp\\D4WTPCONTINGENT5REQUEST',
      'D4WTPGETCONTINGENT5RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETCONTINGENT5RESULT',
      'ArrayOfD4WTPDAYCONTINGENTS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPDAYCONTINGENTS',
      'D4WTPDAYCONTINGENTS' => 'Axess\\Dci4Wtp\\D4WTPDAYCONTINGENTS',
      'D4WTPDECCONTINGENTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPDECCONTINGENTREQUEST',
      'D4WTPDECCONTINGENTRESULT' => 'Axess\\Dci4Wtp\\D4WTPDECCONTINGENTRESULT',
      'D4WTPGROUPREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGROUPREQUEST',
      'D4WTPGROUPRESULT' => 'Axess\\Dci4Wtp\\D4WTPGROUPRESULT',
      'D4WTPPERSSALESDATAREQUEST' => 'Axess\\Dci4Wtp\\D4WTPPERSSALESDATAREQUEST',
      'D4WTPPERSSALESDATARESULT' => 'Axess\\Dci4Wtp\\D4WTPPERSSALESDATARESULT',
      'ArrayOfD4WTPPERSSALESDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPERSSALESDATA',
      'D4WTPPERSSALESDATA' => 'Axess\\Dci4Wtp\\D4WTPPERSSALESDATA',
      'D4WTPPERSSALESDATARESULT2' => 'Axess\\Dci4Wtp\\D4WTPPERSSALESDATARESULT2',
      'ArrayOfD4WTPPERSSALESDATA2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPERSSALESDATA2',
      'D4WTPPERSSALESDATA2' => 'Axess\\Dci4Wtp\\D4WTPPERSSALESDATA2',
      'ArrayOfD4WTPADDLICENSEPLATES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPADDLICENSEPLATES',
      'D4WTPADDLICENSEPLATES' => 'Axess\\Dci4Wtp\\D4WTPADDLICENSEPLATES',
      'D4WTPWTPNOSALESDATAREQUEST' => 'Axess\\Dci4Wtp\\D4WTPWTPNOSALESDATAREQUEST',
      'D4WTPWTPNOSALESDATARESULT' => 'Axess\\Dci4Wtp\\D4WTPWTPNOSALESDATARESULT',
      'ArrayOfD4WTPWTPNOSALESDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWTPNOSALESDATA',
      'D4WTPWTPNOSALESDATA' => 'Axess\\Dci4Wtp\\D4WTPWTPNOSALESDATA',
      'D4WTPWTPNOSALESDATA2RESULT' => 'Axess\\Dci4Wtp\\D4WTPWTPNOSALESDATA2RESULT',
      'ArrayOfD4WTPWTPNOSALESDATA2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWTPNOSALESDATA2',
      'D4WTPWTPNOSALESDATA2' => 'Axess\\Dci4Wtp\\D4WTPWTPNOSALESDATA2',
      'D4WTPMEMBERRESULT' => 'Axess\\Dci4Wtp\\D4WTPMEMBERRESULT',
      'D4WTPGETFAMILYREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETFAMILYREQUEST',
      'D4WTPGETFAMILYRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETFAMILYRESULT',
      'ArrayOfD4WTPFAMILYMEMBERDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPFAMILYMEMBERDATA',
      'D4WTPFAMILYMEMBERDATA' => 'Axess\\Dci4Wtp\\D4WTPFAMILYMEMBERDATA',
      'D4WTPSETCTCREFIDREQUEST' => 'Axess\\Dci4Wtp\\D4WTPSETCTCREFIDREQUEST',
      'D4WTPGETCTCREFIDREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETCTCREFIDREQUEST',
      'D4WTPGETCTCREFIDRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETCTCREFIDRESULT',
      'ArrayOfD4WTPCREDITCARDREFDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCREDITCARDREFDATA',
      'D4WTPCREDITCARDREFDATA' => 'Axess\\Dci4Wtp\\D4WTPCREDITCARDREFDATA',
      'D4WTPDELCTCREFIDREQUEST' => 'Axess\\Dci4Wtp\\D4WTPDELCTCREFIDREQUEST',
      'D4WTPGETDTLUSAGESREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETDTLUSAGESREQUEST',
      'D4WTPGETDTLUSAGESRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETDTLUSAGESRESULT',
      'ArrayOfD4WTPDTLUSAGESDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPDTLUSAGESDATA',
      'D4WTPDTLUSAGESDATA' => 'Axess\\Dci4Wtp\\D4WTPDTLUSAGESDATA',
      'D4WTPVERIFYNAMEDUSERREQUEST' => 'Axess\\Dci4Wtp\\D4WTPVERIFYNAMEDUSERREQUEST',
      'D4WTPSETPREPTCKTPLUSREQUEST' => 'Axess\\Dci4Wtp\\D4WTPSETPREPTCKTPLUSREQUEST',
      'D4WTPRESTORETCKTSTATUSREQ' => 'Axess\\Dci4Wtp\\D4WTPRESTORETCKTSTATUSREQ',
      'D4WTPGETARTICLELISTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETARTICLELISTREQUEST',
      'D4WTPGETARTICLELISTRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETARTICLELISTRESULT',
      'ArrayOfD4WTPARTICLELIST' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPARTICLELIST',
      'D4WTPARTICLELIST' => 'Axess\\Dci4Wtp\\D4WTPARTICLELIST',
      'ArrayOfD4WTPARTICLE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPARTICLE',
      'D4WTPARTICLE' => 'Axess\\Dci4Wtp\\D4WTPARTICLE',
      'D4WTPGETARTICLELIST2RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETARTICLELIST2RESULT',
      'ArrayOfD4WTPARTICLELIST2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPARTICLELIST2',
      'D4WTPARTICLELIST2' => 'Axess\\Dci4Wtp\\D4WTPARTICLELIST2',
      'ArrayOfD4WTPARTICLE2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPARTICLE2',
      'D4WTPARTICLE2' => 'Axess\\Dci4Wtp\\D4WTPARTICLE2',
      'D4WTPGETARTICLELIST3RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETARTICLELIST3RESULT',
      'ArrayOfD4WTPARTICLELIST3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPARTICLELIST3',
      'D4WTPARTICLELIST3' => 'Axess\\Dci4Wtp\\D4WTPARTICLELIST3',
      'ArrayOfD4WTPARTICLE3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPARTICLE3',
      'D4WTPARTICLE3' => 'Axess\\Dci4Wtp\\D4WTPARTICLE3',
      'D4WTPGETARTICLELIST4RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETARTICLELIST4RESULT',
      'ArrayOfD4WTPARTICLELIST4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPARTICLELIST4',
      'D4WTPARTICLELIST4' => 'Axess\\Dci4Wtp\\D4WTPARTICLELIST4',
      'ArrayOfD4WTPARTICLE4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPARTICLE4',
      'D4WTPARTICLE4' => 'Axess\\Dci4Wtp\\D4WTPARTICLE4',
      'D4WTPGETWARELISTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETWARELISTREQUEST',
      'D4WTPGETWARELISTRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETWARELISTRESULT',
      'ArrayOfD4WTPWARELIST' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWARELIST',
      'D4WTPWARELIST' => 'Axess\\Dci4Wtp\\D4WTPWARELIST',
      'ArrayOfD4WTPWARE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWARE',
      'D4WTPWARE' => 'Axess\\Dci4Wtp\\D4WTPWARE',
      'D4WTPGETWARELISTRESULT2' => 'Axess\\Dci4Wtp\\D4WTPGETWARELISTRESULT2',
      'ArrayOfD4WTPWARELIST2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWARELIST2',
      'D4WTPWARELIST2' => 'Axess\\Dci4Wtp\\D4WTPWARELIST2',
      'ArrayOfD4WTPWARE2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWARE2',
      'D4WTPWARE2' => 'Axess\\Dci4Wtp\\D4WTPWARE2',
      'D4WTPGETWARELISTRESULT3' => 'Axess\\Dci4Wtp\\D4WTPGETWARELISTRESULT3',
      'ArrayOfD4WTPWARELIST3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWARELIST3',
      'D4WTPWARELIST3' => 'Axess\\Dci4Wtp\\D4WTPWARELIST3',
      'ArrayOfD4WTPWARE3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWARE3',
      'D4WTPWARE3' => 'Axess\\Dci4Wtp\\D4WTPWARE3',
      'ArrayOfD4WTPWAREFEATURES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPWAREFEATURES',
      'D4WTPWAREFEATURES' => 'Axess\\Dci4Wtp\\D4WTPWAREFEATURES',
      'D4WTPGETCURRENCIESREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETCURRENCIESREQUEST',
      'D4WTPGETCURRENCIESRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETCURRENCIESRESULT',
      'ArrayOfD4WTPCURRENCYREC' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCURRENCYREC',
      'D4WTPCURRENCYREC' => 'Axess\\Dci4Wtp\\D4WTPCURRENCYREC',
      'D4WTPGETTCKTBLCKSTATUSREQ' => 'Axess\\Dci4Wtp\\D4WTPGETTCKTBLCKSTATUSREQ',
      'D4WTPGETTCKTBLCKSTATUSRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETTCKTBLCKSTATUSRESULT',
      'ArrayOfD4WTPTICKETBLOCKSTATUS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETBLOCKSTATUS',
      'D4WTPTICKETBLOCKSTATUS' => 'Axess\\Dci4Wtp\\D4WTPTICKETBLOCKSTATUS',
      'D4WTPSETCLIENTSETUPREQUEST' => 'Axess\\Dci4Wtp\\D4WTPSETCLIENTSETUPREQUEST',
      'ArrayOfD4WTPSETUPITEM' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSETUPITEM',
      'D4WTPSETUPITEM' => 'Axess\\Dci4Wtp\\D4WTPSETUPITEM',
      'D4WTPGETCLIENTSETUPREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETCLIENTSETUPREQUEST',
      'D4WTPGETCLIENTSETUPRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETCLIENTSETUPRESULT',
      'D4WTPGETTCKTRIDESDROPSREQ' => 'Axess\\Dci4Wtp\\D4WTPGETTCKTRIDESDROPSREQ',
      'D4WTPGETTCKTRIDESDROPSRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETTCKTRIDESDROPSRESULT',
      'ArrayOfD4WTPRIDESANDDROPS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRIDESANDDROPS',
      'D4WTPRIDESANDDROPS' => 'Axess\\Dci4Wtp\\D4WTPRIDESANDDROPS',
      'D4WTPGETTCKTRIDESDROPSREQ2' => 'Axess\\Dci4Wtp\\D4WTPGETTCKTRIDESDROPSREQ2',
      'D4WTPGETTCKTRIDESDROPSRES2' => 'Axess\\Dci4Wtp\\D4WTPGETTCKTRIDESDROPSRES2',
      'ArrayOfD4WTPRIDESANDDROPS2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRIDESANDDROPS2',
      'D4WTPRIDESANDDROPS2' => 'Axess\\Dci4Wtp\\D4WTPRIDESANDDROPS2',
      'D4WTPCOMPANYREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCOMPANYREQUEST',
      'D4WTPCOMPANYWTPFIELDS' => 'Axess\\Dci4Wtp\\D4WTPCOMPANYWTPFIELDS',
      'ArrayOfCOMPANYCONTACTPERSON' => 'Axess\\Dci4Wtp\\ArrayOfCOMPANYCONTACTPERSON',
      'COMPANYCONTACTPERSON' => 'Axess\\Dci4Wtp\\COMPANYCONTACTPERSON',
      'D4WTPCOMPEXHIBITION' => 'Axess\\Dci4Wtp\\D4WTPCOMPEXHIBITION',
      'ArrayOfSPECIALADDRESS' => 'Axess\\Dci4Wtp\\ArrayOfSPECIALADDRESS',
      'SPECIALADDRESS' => 'Axess\\Dci4Wtp\\SPECIALADDRESS',
      'D4WTPCOMPANYRESULT' => 'Axess\\Dci4Wtp\\D4WTPCOMPANYRESULT',
      'D4WTPDECODEBARCODECRESULT' => 'Axess\\Dci4Wtp\\D4WTPDECODEBARCODECRESULT',
      'D4WTPGETPROMPTLISTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETPROMPTLISTREQUEST',
      'ArrayOfD4WTPPROMPTTYPELIST' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPROMPTTYPELIST',
      'D4WTPPROMPTTYPELIST' => 'Axess\\Dci4Wtp\\D4WTPPROMPTTYPELIST',
      'D4WTPGETPROMPTLISTRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETPROMPTLISTRESULT',
      'ArrayOfD4WTPPROMPTTYPES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPROMPTTYPES',
      'D4WTPPROMPTTYPES' => 'Axess\\Dci4Wtp\\D4WTPPROMPTTYPES',
      'ArrayOfD4WTPPROMPTS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPROMPTS',
      'D4WTPPROMPTS' => 'Axess\\Dci4Wtp\\D4WTPPROMPTS',
      'ArrayOfD4WTPPROMPTELEMENTS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPROMPTELEMENTS',
      'D4WTPPROMPTELEMENTS' => 'Axess\\Dci4Wtp\\D4WTPPROMPTELEMENTS',
      'D4WTPSETPROMPTDATAREQUEST' => 'Axess\\Dci4Wtp\\D4WTPSETPROMPTDATAREQUEST',
      'ArrayOfD4WTPPROMPTDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPROMPTDATA',
      'D4WTPPROMPTDATA' => 'Axess\\Dci4Wtp\\D4WTPPROMPTDATA',
      'D4WTPPROMPTADDITONALDATA' => 'Axess\\Dci4Wtp\\D4WTPPROMPTADDITONALDATA',
      'D4WTPGETPROMPTDATAREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETPROMPTDATAREQUEST',
      'D4WTPGETPROMPTDATARESULT' => 'Axess\\Dci4Wtp\\D4WTPGETPROMPTDATARESULT',
      'D4WTPSETEXTINVOICENOREQUEST' => 'Axess\\Dci4Wtp\\D4WTPSETEXTINVOICENOREQUEST',
      'D4WTPRELEASEPREPAIDTICKETREQ' => 'Axess\\Dci4Wtp\\D4WTPRELEASEPREPAIDTICKETREQ',
      'D4WTPGETRESERVATIONREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETRESERVATIONREQUEST',
      'D4WTPGETRESERVATIONRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETRESERVATIONRESULT',
      'ArrayOfD4WTPGETRESERVATIONDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPGETRESERVATIONDATA',
      'D4WTPGETRESERVATIONDATA' => 'Axess\\Dci4Wtp\\D4WTPGETRESERVATIONDATA',
      'D4WTPMODIFYRESREQUEST' => 'Axess\\Dci4Wtp\\D4WTPMODIFYRESREQUEST',
      'D4WTPMODIFYRES2REQUEST' => 'Axess\\Dci4Wtp\\D4WTPMODIFYRES2REQUEST',
      'D4WTPADDRESERVATIONREQUEST' => 'Axess\\Dci4Wtp\\D4WTPADDRESERVATIONREQUEST',
      'ArrayOfD4WTPADDRESCONTINGENTLIST' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPADDRESCONTINGENTLIST',
      'D4WTPADDRESCONTINGENTLIST' => 'Axess\\Dci4Wtp\\D4WTPADDRESCONTINGENTLIST',
      'D4WTPLNECERTIFICATENORESULT' => 'Axess\\Dci4Wtp\\D4WTPLNECERTIFICATENORESULT',
      'D4WTPISTESTMODEENABLEDRESULT' => 'Axess\\Dci4Wtp\\D4WTPISTESTMODEENABLEDRESULT',
      'D4WTPADDLICENSEPLATEREQ' => 'Axess\\Dci4Wtp\\D4WTPADDLICENSEPLATEREQ',
      'D4WTPADDLICENSEPLATERESULT' => 'Axess\\Dci4Wtp\\D4WTPADDLICENSEPLATERESULT',
      'D4WTPADDADLICENSEPLATEREQ' => 'Axess\\Dci4Wtp\\D4WTPADDADLICENSEPLATEREQ',
      'D4WTPADDADLICENSEPLATERESULT' => 'Axess\\Dci4Wtp\\D4WTPADDADLICENSEPLATERESULT',
      'D4WTPSTARTCLICSREPORTREQ' => 'Axess\\Dci4Wtp\\D4WTPSTARTCLICSREPORTREQ',
      'ArrayOfCLICSVARCHARS' => 'Axess\\Dci4Wtp\\ArrayOfCLICSVARCHARS',
      'CLICSVARCHARS' => 'Axess\\Dci4Wtp\\CLICSVARCHARS',
      'D4WTPSTARTCLICSREPORTRES' => 'Axess\\Dci4Wtp\\D4WTPSTARTCLICSREPORTRES',
      'D4WTPCLICSREPORTSTATUSREQ' => 'Axess\\Dci4Wtp\\D4WTPCLICSREPORTSTATUSREQ',
      'D4WTPCLICSREPORTSTATUSRES' => 'Axess\\Dci4Wtp\\D4WTPCLICSREPORTSTATUSRES',
      'D4WTPGETLPUSAGESRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETLPUSAGESRESULT',
      'ArrayOfD4WTPLPUSAGES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPLPUSAGES',
      'D4WTPLPUSAGES' => 'Axess\\Dci4Wtp\\D4WTPLPUSAGES',
      'D4WTPISLPCHECKINOUTRESULT' => 'Axess\\Dci4Wtp\\D4WTPISLPCHECKINOUTRESULT',
      'D4WTPPAYERREQUEST' => 'Axess\\Dci4Wtp\\D4WTPPAYERREQUEST',
      'D4WTPPAYERRESULT' => 'Axess\\Dci4Wtp\\D4WTPPAYERRESULT',
      'D4WTPPAYER2REQUEST' => 'Axess\\Dci4Wtp\\D4WTPPAYER2REQUEST',
      'D4WTPPAYER2RESULT' => 'Axess\\Dci4Wtp\\D4WTPPAYER2RESULT',
      'D4WTPMANAGESHOPCARTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPMANAGESHOPCARTREQUEST',
      'ArrayOfD4WTPSHOPPINGCARTPOS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTPOS',
      'D4WTPSHOPPINGCARTPOS' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTPOS',
      'ArrayOfD4WTPSHOPCSUBCONTINGENT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCSUBCONTINGENT',
      'D4WTPSHOPCSUBCONTINGENT' => 'Axess\\Dci4Wtp\\D4WTPSHOPCSUBCONTINGENT',
      'ArrayOfD4WTPSHOPPINGCARTPOSDETAIL' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTPOSDETAIL',
      'D4WTPSHOPPINGCARTPOSDETAIL' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTPOSDETAIL',
      'ArrayOfD4WTPSHOPPINGCARTPOSADDART' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTPOSADDART',
      'D4WTPSHOPPINGCARTPOSADDART' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTPOSADDART',
      'D4WTPMANAGESHOPCARTRESULT' => 'Axess\\Dci4Wtp\\D4WTPMANAGESHOPCARTRESULT',
      'ArrayOfD4WTPSHOPPINGCARTPOSRES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTPOSRES',
      'D4WTPSHOPPINGCARTPOSRES' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTPOSRES',
      'ArrayOfD4WTPSHOPPINGCARTPOSDETRES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTPOSDETRES',
      'D4WTPSHOPPINGCARTPOSDETRES' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTPOSDETRES',
      'ArrayOfD4WTPSHOPPINGCARTPOSARTRES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTPOSARTRES',
      'D4WTPSHOPPINGCARTPOSARTRES' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTPOSARTRES',
      'D4WTPMANAGESHOPCARTREQ2' => 'Axess\\Dci4Wtp\\D4WTPMANAGESHOPCARTREQ2',
      'ArrayOfD4WTPSHOPPINGCARTPOS2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTPOS2',
      'D4WTPSHOPPINGCARTPOS2' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTPOS2',
      'ArrayOfSHOPCBOOKTIMESLOT' => 'Axess\\Dci4Wtp\\ArrayOfSHOPCBOOKTIMESLOT',
      'SHOPCBOOKTIMESLOT' => 'Axess\\Dci4Wtp\\SHOPCBOOKTIMESLOT',
      'D4WTPMANAGESHOPCARTREQ3' => 'Axess\\Dci4Wtp\\D4WTPMANAGESHOPCARTREQ3',
      'ArrayOfD4WTPSHOPPINGCARTPOS3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTPOS3',
      'D4WTPSHOPPINGCARTPOS3' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTPOS3',
      'ArrayOfD4WTPSHOPPINGCARTPOSDETAIL2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTPOSDETAIL2',
      'D4WTPSHOPPINGCARTPOSDETAIL2' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTPOSDETAIL2',
      'ArrayOfD4WTPSHOPPINGCARTPOSADDART2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTPOSADDART2',
      'D4WTPSHOPPINGCARTPOSADDART2' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTPOSADDART2',
      'DCI4WTPGETDOWNPAYMENTREQ' => 'Axess\\Dci4Wtp\\DCI4WTPGETDOWNPAYMENTREQ',
      'D4WTPMANAGETRAVELGRPREQUEST' => 'Axess\\Dci4Wtp\\D4WTPMANAGETRAVELGRPREQUEST',
      'D4WTPMANAGETRAVELGRPRESULT' => 'Axess\\Dci4Wtp\\D4WTPMANAGETRAVELGRPRESULT',
      'D4WTPSETSHOPCARTSTATUSREQ' => 'Axess\\Dci4Wtp\\D4WTPSETSHOPCARTSTATUSREQ',
      'D4WTPSETTRAVELGROUPSTATUSREQ' => 'Axess\\Dci4Wtp\\D4WTPSETTRAVELGROUPSTATUSREQ',
      'D4WTPGETSHOPCARTDATAREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETSHOPCARTDATAREQUEST',
      'D4WTPGETSHOPCARTDATARESULT' => 'Axess\\Dci4Wtp\\D4WTPGETSHOPCARTDATARESULT',
      'ArrayOfD4WTPSHOPPINGCARTDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTDATA',
      'D4WTPSHOPPINGCARTDATA' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTDATA',
      'ArrayOfD4WTPSHOPCARTPOSDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCARTPOSDATA',
      'D4WTPSHOPCARTPOSDATA' => 'Axess\\Dci4Wtp\\D4WTPSHOPCARTPOSDATA',
      'ArrayOfD4WTPSHOPCARTPOSDETDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCARTPOSDETDATA',
      'D4WTPSHOPCARTPOSDETDATA' => 'Axess\\Dci4Wtp\\D4WTPSHOPCARTPOSDETDATA',
      'ArrayOfD4WTPSHOPCARTPOSDETARTDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCARTPOSDETARTDATA',
      'D4WTPSHOPCARTPOSDETARTDATA' => 'Axess\\Dci4Wtp\\D4WTPSHOPCARTPOSDETARTDATA',
      'D4WTPGETSHOPCARTDATA2RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETSHOPCARTDATA2RESULT',
      'ArrayOfD4WTPSHOPPINGCARTDATA2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTDATA2',
      'D4WTPSHOPPINGCARTDATA2' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTDATA2',
      'ArrayOfD4WTPSHOPCARTPOSDATA2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCARTPOSDATA2',
      'D4WTPSHOPCARTPOSDATA2' => 'Axess\\Dci4Wtp\\D4WTPSHOPCARTPOSDATA2',
      'ArrayOfD4WTPSHOPCARTPOSDETDATA2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCARTPOSDETDATA2',
      'D4WTPSHOPCARTPOSDETDATA2' => 'Axess\\Dci4Wtp\\D4WTPSHOPCARTPOSDETDATA2',
      'ArrayOfD4WTPSHOPCARTPOSDETARTDATA2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCARTPOSDETARTDATA2',
      'D4WTPSHOPCARTPOSDETARTDATA2' => 'Axess\\Dci4Wtp\\D4WTPSHOPCARTPOSDETARTDATA2',
      'ArrayOfD4WTPSHOPCSUBCONTINGENT2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCSUBCONTINGENT2',
      'D4WTPSHOPCSUBCONTINGENT2' => 'Axess\\Dci4Wtp\\D4WTPSHOPCSUBCONTINGENT2',
      'D4WTPGETSHOPCARTDATA3RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETSHOPCARTDATA3RESULT',
      'ArrayOfD4WTPSHOPPINGCARTDATA3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTDATA3',
      'D4WTPSHOPPINGCARTDATA3' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTDATA3',
      'ArrayOfD4WTPSHOPCARTPOSDATA3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCARTPOSDATA3',
      'D4WTPSHOPCARTPOSDATA3' => 'Axess\\Dci4Wtp\\D4WTPSHOPCARTPOSDATA3',
      'ArrayOfD4WTPSHOPCSUBCONTINGENT3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCSUBCONTINGENT3',
      'D4WTPSHOPCSUBCONTINGENT3' => 'Axess\\Dci4Wtp\\D4WTPSHOPCSUBCONTINGENT3',
      'D4WTPGETSHOPCARTDATA4RES' => 'Axess\\Dci4Wtp\\D4WTPGETSHOPCARTDATA4RES',
      'ArrayOfD4WTPSHOPPINGCARTDATA4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTDATA4',
      'D4WTPSHOPPINGCARTDATA4' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTDATA4',
      'ArrayOfD4WTPSHOPCARTPOSDATA4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCARTPOSDATA4',
      'D4WTPSHOPCARTPOSDATA4' => 'Axess\\Dci4Wtp\\D4WTPSHOPCARTPOSDATA4',
      'D4WTPGETSHOPCARTDATA5RES' => 'Axess\\Dci4Wtp\\D4WTPGETSHOPCARTDATA5RES',
      'ArrayOfD4WTPSHOPPINGCARTDATA5' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPPINGCARTDATA5',
      'D4WTPSHOPPINGCARTDATA5' => 'Axess\\Dci4Wtp\\D4WTPSHOPPINGCARTDATA5',
      'ArrayOfD4WTPSHOPCARTPOSDATA5' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCARTPOSDATA5',
      'D4WTPSHOPCARTPOSDATA5' => 'Axess\\Dci4Wtp\\D4WTPSHOPCARTPOSDATA5',
      'ArrayOfD4WTPSHOPCARTPOSDETDATA3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCARTPOSDETDATA3',
      'D4WTPSHOPCARTPOSDETDATA3' => 'Axess\\Dci4Wtp\\D4WTPSHOPCARTPOSDETDATA3',
      'ArrayOfD4WTPSHOPCARTPOSDETARTDATA3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSHOPCARTPOSDETARTDATA3',
      'D4WTPSHOPCARTPOSDETARTDATA3' => 'Axess\\Dci4Wtp\\D4WTPSHOPCARTPOSDETARTDATA3',
      'D4WTPGETTRAVELGRPDATAREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETTRAVELGRPDATAREQUEST',
      'D4WTPGETTRAVELGRPDATARESULT' => 'Axess\\Dci4Wtp\\D4WTPGETTRAVELGRPDATARESULT',
      'ArrayOfD4WTPTRAVELGROUPDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTRAVELGROUPDATA',
      'D4WTPTRAVELGROUPDATA' => 'Axess\\Dci4Wtp\\D4WTPTRAVELGROUPDATA',
      'D4WTPGETTRAVELGRPDATARESULT2' => 'Axess\\Dci4Wtp\\D4WTPGETTRAVELGRPDATARESULT2',
      'ArrayOfD4WTPTRAVELGROUPDATA2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTRAVELGROUPDATA2',
      'D4WTPTRAVELGROUPDATA2' => 'Axess\\Dci4Wtp\\D4WTPTRAVELGROUPDATA2',
      'D4WTPGETTRAVELGRPSHOPPOSREQ' => 'Axess\\Dci4Wtp\\D4WTPGETTRAVELGRPSHOPPOSREQ',
      'D4WTPGETTRAVELGRPSHOPPOSRES' => 'Axess\\Dci4Wtp\\D4WTPGETTRAVELGRPSHOPPOSRES',
      'D4WTPGETTRAVELGRPSHOPPOSREQ2' => 'Axess\\Dci4Wtp\\D4WTPGETTRAVELGRPSHOPPOSREQ2',
      'D4WTPGETTRAVELGRPSHOPPOSRES2' => 'Axess\\Dci4Wtp\\D4WTPGETTRAVELGRPSHOPPOSRES2',
      'D4WTPPRODSHOPPOSPREPAIDREQ' => 'Axess\\Dci4Wtp\\D4WTPPRODSHOPPOSPREPAIDREQ',
      'D4WTPPRODSHOPPOSPREPAIDRES' => 'Axess\\Dci4Wtp\\D4WTPPRODSHOPPOSPREPAIDRES',
      'D4WTPPRODSHOPPOSPREPAIDREQ2' => 'Axess\\Dci4Wtp\\D4WTPPRODSHOPPOSPREPAIDREQ2',
      'D4WTPFIRMENBZHLARTEXREQUEST' => 'Axess\\Dci4Wtp\\D4WTPFIRMENBZHLARTEXREQUEST',
      'D4WTPFIRMENBZHLARTEXCLRESULT' => 'Axess\\Dci4Wtp\\D4WTPFIRMENBZHLARTEXCLRESULT',
      'ArrayOfD4WTPFIRMENBEZAHLARTENEXCL' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPFIRMENBEZAHLARTENEXCL',
      'D4WTPFIRMENBEZAHLARTENEXCL' => 'Axess\\Dci4Wtp\\D4WTPFIRMENBEZAHLARTENEXCL',
      'D4WTPSTORETEMPTXTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPSTORETEMPTXTREQUEST',
      'D4WTPCASHIERSHIFTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCASHIERSHIFTREQUEST',
      'D4WTPGETOPENSHIFTSRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETOPENSHIFTSRESULT',
      'ArrayOfD4WTPCASHIERSHIFT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCASHIERSHIFT',
      'D4WTPCASHIERSHIFT' => 'Axess\\Dci4Wtp\\D4WTPCASHIERSHIFT',
      'ArrayOfD4WTPSALESSUMMARY' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSALESSUMMARY',
      'D4WTPSALESSUMMARY' => 'Axess\\Dci4Wtp\\D4WTPSALESSUMMARY',
      'D4WTPOPENCASHIERSHIFTRESULT' => 'Axess\\Dci4Wtp\\D4WTPOPENCASHIERSHIFTRESULT',
      'D4WTPCLOSECASHIERSHIFTRESULT' => 'Axess\\Dci4Wtp\\D4WTPCLOSECASHIERSHIFTRESULT',
      'D4WTPCASHIERRIGHTSREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCASHIERRIGHTSREQUEST',
      'D4WTPCASHIERRIGHTSRESULT' => 'Axess\\Dci4Wtp\\D4WTPCASHIERRIGHTSRESULT',
      'D4WTPGETGROUPRESERVATIONNREQ' => 'Axess\\Dci4Wtp\\D4WTPGETGROUPRESERVATIONNREQ',
      'D4WTPGETGRPRESERVATIONRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETGRPRESERVATIONRESULT',
      'ArrayOfD4WTPGETGRPRESERVATIONDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPGETGRPRESERVATIONDATA',
      'D4WTPGETGRPRESERVATIONDATA' => 'Axess\\Dci4Wtp\\D4WTPGETGRPRESERVATIONDATA',
      'D4WTPGETGRPCONTINGENTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETGRPCONTINGENTREQUEST',
      'D4WTPGETGRPCONTINGENTRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETGRPCONTINGENTRESULT',
      'D4WTPGETGRPCONTINGENT2RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETGRPCONTINGENT2RESULT',
      'D4WTPLOCKRESERVATIONREQUEST' => 'Axess\\Dci4Wtp\\D4WTPLOCKRESERVATIONREQUEST',
      'ArrayOfD4WTPLOCKRESERVATION' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPLOCKRESERVATION',
      'D4WTPLOCKRESERVATION' => 'Axess\\Dci4Wtp\\D4WTPLOCKRESERVATION',
      'D4WTPNF525RECEIPTNOREQUEST' => 'Axess\\Dci4Wtp\\D4WTPNF525RECEIPTNOREQUEST',
      'D4WTPNF525RECEIPTNORESULT' => 'Axess\\Dci4Wtp\\D4WTPNF525RECEIPTNORESULT',
      'D4WTPGETRECEIPTDATAREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETRECEIPTDATAREQUEST',
      'D4WTPRECEIPTDATARESULT' => 'Axess\\Dci4Wtp\\D4WTPRECEIPTDATARESULT',
      'D4WTPRECEIPTDATA' => 'Axess\\Dci4Wtp\\D4WTPRECEIPTDATA',
      'ArrayOfD4WTPRECEIPTADDARTICLES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRECEIPTADDARTICLES',
      'D4WTPRECEIPTADDARTICLES' => 'Axess\\Dci4Wtp\\D4WTPRECEIPTADDARTICLES',
      'ArrayOfD4WTPRECEIPTARTICLES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRECEIPTARTICLES',
      'D4WTPRECEIPTARTICLES' => 'Axess\\Dci4Wtp\\D4WTPRECEIPTARTICLES',
      'ArrayOfD4WTPRECEIPTPAYMENT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRECEIPTPAYMENT',
      'D4WTPRECEIPTPAYMENT' => 'Axess\\Dci4Wtp\\D4WTPRECEIPTPAYMENT',
      'ArrayOfD4WTPRECEIPTTICKETS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRECEIPTTICKETS',
      'D4WTPRECEIPTTICKETS' => 'Axess\\Dci4Wtp\\D4WTPRECEIPTTICKETS',
      'D4WTPRECEIPTPAYERDATA' => 'Axess\\Dci4Wtp\\D4WTPRECEIPTPAYERDATA',
      'D4WTPGETRECEIPTDATAREQUEST2' => 'Axess\\Dci4Wtp\\D4WTPGETRECEIPTDATAREQUEST2',
      'D4WTPRECEIPTDATARESULT2' => 'Axess\\Dci4Wtp\\D4WTPRECEIPTDATARESULT2',
      'D4WTPRECEIPTDATA2' => 'Axess\\Dci4Wtp\\D4WTPRECEIPTDATA2',
      'D4WTPGETARCUBESREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETARCUBESREQUEST',
      'D4WTPGETARCUBESRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETARCUBESRESULT',
      'ArrayOfD4WTPARCUBE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPARCUBE',
      'D4WTPARCUBE' => 'Axess\\Dci4Wtp\\D4WTPARCUBE',
      'D4WTPSETARCUBEREQUEST' => 'Axess\\Dci4Wtp\\D4WTPSETARCUBEREQUEST',
      'D4WTPRENTALITEMREQ' => 'Axess\\Dci4Wtp\\D4WTPRENTALITEMREQ',
      'D4WTPRENTALITEMRESULT' => 'Axess\\Dci4Wtp\\D4WTPRENTALITEMRESULT',
      'ArrayOfD4WTPRENTALITEM' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRENTALITEM',
      'D4WTPRENTALITEM' => 'Axess\\Dci4Wtp\\D4WTPRENTALITEM',
      'D4WTPRENTALITEMRESULT2' => 'Axess\\Dci4Wtp\\D4WTPRENTALITEMRESULT2',
      'ArrayOfD4WTPRENTALITEM2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRENTALITEM2',
      'D4WTPRENTALITEM2' => 'Axess\\Dci4Wtp\\D4WTPRENTALITEM2',
      'D4WTPGETRENTALPERSTYPESREQ' => 'Axess\\Dci4Wtp\\D4WTPGETRENTALPERSTYPESREQ',
      'D4WTPRENTALPERSTYPERESULT' => 'Axess\\Dci4Wtp\\D4WTPRENTALPERSTYPERESULT',
      'ArrayOfD4WTPRENTALPERSTYPE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRENTALPERSTYPE',
      'D4WTPRENTALPERSTYPE' => 'Axess\\Dci4Wtp\\D4WTPRENTALPERSTYPE',
      'D4WTPGETARCDATAREQ' => 'Axess\\Dci4Wtp\\D4WTPGETARCDATAREQ',
      'D4WTPGETARCDATARES' => 'Axess\\Dci4Wtp\\D4WTPGETARCDATARES',
      'ArrayOfD4WTPDURATIONINFO' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPDURATIONINFO',
      'D4WTPDURATIONINFO' => 'Axess\\Dci4Wtp\\D4WTPDURATIONINFO',
      'ArrayOfD4WTPLESSONINSTINFO' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPLESSONINSTINFO',
      'D4WTPLESSONINSTINFO' => 'Axess\\Dci4Wtp\\D4WTPLESSONINSTINFO',
      'ArrayOfD4WTPLOCATIONINFO' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPLOCATIONINFO',
      'D4WTPLOCATIONINFO' => 'Axess\\Dci4Wtp\\D4WTPLOCATIONINFO',
      'WEEKSCALE' => 'Axess\\Dci4Wtp\\WEEKSCALE',
      'D4WTPGETROUTESTATIONSREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETROUTESTATIONSREQUEST',
      'ArrayOfD4WTPTICKETTYPENO' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETTYPENO',
      'D4WTPTICKETTYPENO' => 'Axess\\Dci4Wtp\\D4WTPTICKETTYPENO',
      'D4WTPGETROUTESSTATIONSRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETROUTESSTATIONSRESULT',
      'ArrayOfD4WTPROUTESSTATIONS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPROUTESSTATIONS',
      'D4WTPROUTESSTATIONS' => 'Axess\\Dci4Wtp\\D4WTPROUTESSTATIONS',
      'D4WTPGETCOMPANYREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETCOMPANYREQUEST',
      'D4WTPGETCOMPANYRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETCOMPANYRESULT',
      'ArrayOfD4WTPCOMPANYDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCOMPANYDATA',
      'D4WTPCOMPANYDATA' => 'Axess\\Dci4Wtp\\D4WTPCOMPANYDATA',
      'D4WTPGETCOMPANY2RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETCOMPANY2RESULT',
      'ArrayOfD4WTPCOMPANYDATA2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCOMPANYDATA2',
      'D4WTPCOMPANYDATA2' => 'Axess\\Dci4Wtp\\D4WTPCOMPANYDATA2',
      'D4WTPCOMPANY2' => 'Axess\\Dci4Wtp\\D4WTPCOMPANY2',
      'D4WTPGETALLCOMPANYREQ' => 'Axess\\Dci4Wtp\\D4WTPGETALLCOMPANYREQ',
      'D4WTPGETCOMPANY3RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETCOMPANY3RESULT',
      'ArrayOfD4WTPCOMPANYDATA3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCOMPANYDATA3',
      'D4WTPCOMPANYDATA3' => 'Axess\\Dci4Wtp\\D4WTPCOMPANYDATA3',
      'D4WTPCOMPANY3' => 'Axess\\Dci4Wtp\\D4WTPCOMPANY3',
      'D4WTPGETCASH4CASHIERSHIFTRES' => 'Axess\\Dci4Wtp\\D4WTPGETCASH4CASHIERSHIFTRES',
      'D4WTPCASH4CASHIERSHIFTREQ' => 'Axess\\Dci4Wtp\\D4WTPCASH4CASHIERSHIFTREQ',
      'D4WTPMOVCASH4CASHIERSHIFTRES' => 'Axess\\Dci4Wtp\\D4WTPMOVCASH4CASHIERSHIFTRES',
      'D4WTPREDEEMPROMOCODESREQUEST' => 'Axess\\Dci4Wtp\\D4WTPREDEEMPROMOCODESREQUEST',
      'ArrayOfD4WTPREDEEMPROMOCODES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPREDEEMPROMOCODES',
      'D4WTPREDEEMPROMOCODES' => 'Axess\\Dci4Wtp\\D4WTPREDEEMPROMOCODES',
      'D4WTPREDEEMPROMOCODESRESULT' => 'Axess\\Dci4Wtp\\D4WTPREDEEMPROMOCODESRESULT',
      'D4WTPACPXIMAGERESULT' => 'Axess\\Dci4Wtp\\D4WTPACPXIMAGERESULT',
      'D4WTPLOGINREQUEST' => 'Axess\\Dci4Wtp\\D4WTPLOGINREQUEST',
      'D4WTPLOGINRESULT' => 'Axess\\Dci4Wtp\\D4WTPLOGINRESULT',
      'D4WTPLOGIN2RESULT' => 'Axess\\Dci4Wtp\\D4WTPLOGIN2RESULT',
      'ArrayOfD4WTPADDRESSDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPADDRESSDATA',
      'D4WTPADDRESSDATA' => 'Axess\\Dci4Wtp\\D4WTPADDRESSDATA',
      'D4WTPLOGINCASHIERREQUEST' => 'Axess\\Dci4Wtp\\D4WTPLOGINCASHIERREQUEST',
      'D4WTPLOGINCASHIERRESULT' => 'Axess\\Dci4Wtp\\D4WTPLOGINCASHIERRESULT',
      'D4WTPLOGINNAMEDUSERREQUEST' => 'Axess\\Dci4Wtp\\D4WTPLOGINNAMEDUSERREQUEST',
      'D4WTPLOGINNAMEDUSERRESULT' => 'Axess\\Dci4Wtp\\D4WTPLOGINNAMEDUSERRESULT',
      'D4WTPLOGOUTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPLOGOUTREQUEST',
      'D4WTPLOGOUTRESULT' => 'Axess\\Dci4Wtp\\D4WTPLOGOUTRESULT',
      'D4WTPSETCOUNTRYCODEREQUEST' => 'Axess\\Dci4Wtp\\D4WTPSETCOUNTRYCODEREQUEST',
      'D4WTPSETCOUNTRYCODERESULT' => 'Axess\\Dci4Wtp\\D4WTPSETCOUNTRYCODERESULT',
      'D4WTPCHECKSESSIONREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCHECKSESSIONREQUEST',
      'D4WTPCHECKSESSIONRESULT' => 'Axess\\Dci4Wtp\\D4WTPCHECKSESSIONRESULT',
      'D4WTPCONFIGREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCONFIGREQUEST',
      'D4WTPCONFIGRESULT' => 'Axess\\Dci4Wtp\\D4WTPCONFIGRESULT',
      'ArrayOfD4WTPCONFIG' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCONFIG',
      'D4WTPCONFIG' => 'Axess\\Dci4Wtp\\D4WTPCONFIG',
      'D4WTPFORGOTPASSREQ' => 'Axess\\Dci4Wtp\\D4WTPFORGOTPASSREQ',
      'D4WTPFORGOTPASSRES' => 'Axess\\Dci4Wtp\\D4WTPFORGOTPASSRES',
      'D4WTPCUSTOMERPROFILEREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCUSTOMERPROFILEREQUEST',
      'D4WTPCUSTOMERPROFILERESULT' => 'Axess\\Dci4Wtp\\D4WTPCUSTOMERPROFILERESULT',
      'D4WTPCUSTOMERPROFILE2RESULT' => 'Axess\\Dci4Wtp\\D4WTPCUSTOMERPROFILE2RESULT',
      'D4WTPCUSTOMERPROFILE3RESULT' => 'Axess\\Dci4Wtp\\D4WTPCUSTOMERPROFILE3RESULT',
      'ArrayOfD4WTPCUSTOMERPROFSETUPITEM' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCUSTOMERPROFSETUPITEM',
      'D4WTPCUSTOMERPROFSETUPITEM' => 'Axess\\Dci4Wtp\\D4WTPCUSTOMERPROFSETUPITEM',
      'D4WTPB2CACCOUNTSREQUEST' => 'Axess\\Dci4Wtp\\D4WTPB2CACCOUNTSREQUEST',
      'D4WTPB2CACCOUNTSRESULT' => 'Axess\\Dci4Wtp\\D4WTPB2CACCOUNTSRESULT',
      'ArrayOfD4WTPB2CACCOUNT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPB2CACCOUNT',
      'D4WTPB2CACCOUNT' => 'Axess\\Dci4Wtp\\D4WTPB2CACCOUNT',
      'D4WTPSETB2CACCOUNTSREQUEST' => 'Axess\\Dci4Wtp\\D4WTPSETB2CACCOUNTSREQUEST',
      'D4WTPSETB2CACCOUNTSRESULT' => 'Axess\\Dci4Wtp\\D4WTPSETB2CACCOUNTSRESULT',
      'D4WTPCHANGEPWREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCHANGEPWREQUEST',
      'D4WTPCHANGEPWRESULT' => 'Axess\\Dci4Wtp\\D4WTPCHANGEPWRESULT',
      'D4WTPCOMPANYINFOREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCOMPANYINFOREQUEST',
      'D4WTPCOMPANYINFORESULT' => 'Axess\\Dci4Wtp\\D4WTPCOMPANYINFORESULT',
      'D4WTPCOMPANYINFORESULT2' => 'Axess\\Dci4Wtp\\D4WTPCOMPANYINFORESULT2',
      'D4WTPPOOLREQUEST' => 'Axess\\Dci4Wtp\\D4WTPPOOLREQUEST',
      'D4WTPPOOLRESULT' => 'Axess\\Dci4Wtp\\D4WTPPOOLRESULT',
      'ArrayOfD4WTPPOOL' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPOOL',
      'D4WTPPOOL' => 'Axess\\Dci4Wtp\\D4WTPPOOL',
      'D4WTPPOOL2RESULT' => 'Axess\\Dci4Wtp\\D4WTPPOOL2RESULT',
      'ArrayOfD4WTPPOOL2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPOOL2',
      'D4WTPPOOL2' => 'Axess\\Dci4Wtp\\D4WTPPOOL2',
      'D4WTPTICKETTYPEREQUEST' => 'Axess\\Dci4Wtp\\D4WTPTICKETTYPEREQUEST',
      'D4WTPTICKETTYPERESULT' => 'Axess\\Dci4Wtp\\D4WTPTICKETTYPERESULT',
      'ArrayOfD4WTPTICKETTYPE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETTYPE',
      'D4WTPTICKETTYPE' => 'Axess\\Dci4Wtp\\D4WTPTICKETTYPE',
      'D4WTPTICKETTYPE2RESULT' => 'Axess\\Dci4Wtp\\D4WTPTICKETTYPE2RESULT',
      'ArrayOfD4WTPTICKETTYPE2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETTYPE2',
      'D4WTPTICKETTYPE2' => 'Axess\\Dci4Wtp\\D4WTPTICKETTYPE2',
      'D4WTPTICKETTYPE3RESULT' => 'Axess\\Dci4Wtp\\D4WTPTICKETTYPE3RESULT',
      'ArrayOfD4WTPTICKETTYPE3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETTYPE3',
      'D4WTPTICKETTYPE3' => 'Axess\\Dci4Wtp\\D4WTPTICKETTYPE3',
      'D4WTPTICKETTYPE4RESULT' => 'Axess\\Dci4Wtp\\D4WTPTICKETTYPE4RESULT',
      'ArrayOfD4WTPTICKETTYPE4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETTYPE4',
      'D4WTPTICKETTYPE4' => 'Axess\\Dci4Wtp\\D4WTPTICKETTYPE4',
      'D4WTPGROUPSREQ' => 'Axess\\Dci4Wtp\\D4WTPGROUPSREQ',
      'D4WTPGROUPSRES' => 'Axess\\Dci4Wtp\\D4WTPGROUPSRES',
      'ArrayOfD4WTPGROUP' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPGROUP',
      'D4WTPGROUP' => 'Axess\\Dci4Wtp\\D4WTPGROUP',
      'ArrayOfWTPPOOLTICKETTYPE' => 'Axess\\Dci4Wtp\\ArrayOfWTPPOOLTICKETTYPE',
      'WTPPOOLTICKETTYPE' => 'Axess\\Dci4Wtp\\WTPPOOLTICKETTYPE',
      'D4WTPGROUPS2REQ' => 'Axess\\Dci4Wtp\\D4WTPGROUPS2REQ',
      'D4WTPGROUPS2RES' => 'Axess\\Dci4Wtp\\D4WTPGROUPS2RES',
      'ArrayOfD4WTPGROUP2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPGROUP2',
      'D4WTPGROUP2' => 'Axess\\Dci4Wtp\\D4WTPGROUP2',
      'ArrayOfD4WTPSTANDARTICLE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSTANDARTICLE',
      'D4WTPSTANDARTICLE' => 'Axess\\Dci4Wtp\\D4WTPSTANDARTICLE',
      'ArrayOfD4WTPGROUPWARE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPGROUPWARE',
      'D4WTPGROUPWARE' => 'Axess\\Dci4Wtp\\D4WTPGROUPWARE',
      'D4WTPPERSONTYPEREQUEST' => 'Axess\\Dci4Wtp\\D4WTPPERSONTYPEREQUEST',
      'D4WTPPERSONTYPERESULT' => 'Axess\\Dci4Wtp\\D4WTPPERSONTYPERESULT',
      'ArrayOfD4WTPPERSONTYPE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPERSONTYPE',
      'D4WTPPERSONTYPE' => 'Axess\\Dci4Wtp\\D4WTPPERSONTYPE',
      'D4WTPPERSONTYPE2RESULT' => 'Axess\\Dci4Wtp\\D4WTPPERSONTYPE2RESULT',
      'ArrayOfD4WTPPERSONTYPE2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPERSONTYPE2',
      'D4WTPPERSONTYPE2' => 'Axess\\Dci4Wtp\\D4WTPPERSONTYPE2',
      'D4WTPPERSONTYPE3RESULT' => 'Axess\\Dci4Wtp\\D4WTPPERSONTYPE3RESULT',
      'ArrayOfD4WTPPERSONTYPE3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPERSONTYPE3',
      'D4WTPPERSONTYPE3' => 'Axess\\Dci4Wtp\\D4WTPPERSONTYPE3',
      'D4WTPPERSTYPERULEREQUEST' => 'Axess\\Dci4Wtp\\D4WTPPERSTYPERULEREQUEST',
      'D4WTPPERSTYPERULERESULT' => 'Axess\\Dci4Wtp\\D4WTPPERSTYPERULERESULT',
      'ArrayOfD4WTPPERSTYPERULE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPERSTYPERULE',
      'D4WTPPERSTYPERULE' => 'Axess\\Dci4Wtp\\D4WTPPERSTYPERULE',
      'D4WTPADDARTICLEREQUEST' => 'Axess\\Dci4Wtp\\D4WTPADDARTICLEREQUEST',
      'D4WTPADDARTICLERESULT' => 'Axess\\Dci4Wtp\\D4WTPADDARTICLERESULT',
      'D4WTPPACKAGEREQUEST' => 'Axess\\Dci4Wtp\\D4WTPPACKAGEREQUEST',
      'D4WTPPACKAGERESULT' => 'Axess\\Dci4Wtp\\D4WTPPACKAGERESULT',
      'D4WTPPACKAGE2RESULT' => 'Axess\\Dci4Wtp\\D4WTPPACKAGE2RESULT',
      'ArrayOfD4WTPPACKAGE2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPACKAGE2',
      'D4WTPPACKAGE2' => 'Axess\\Dci4Wtp\\D4WTPPACKAGE2',
      'D4WTPPACKAGE3RESULT' => 'Axess\\Dci4Wtp\\D4WTPPACKAGE3RESULT',
      'ArrayOfD4WTPPACKAGE3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPACKAGE3',
      'D4WTPPACKAGE3' => 'Axess\\Dci4Wtp\\D4WTPPACKAGE3',
      'D4WTPPACKAGECONTENTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPPACKAGECONTENTREQUEST',
      'D4WTPPACKAGECONTENTRESULT' => 'Axess\\Dci4Wtp\\D4WTPPACKAGECONTENTRESULT',
      'ArrayOfD4WTPPACKAGECONTENT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPACKAGECONTENT',
      'D4WTPPACKAGECONTENT' => 'Axess\\Dci4Wtp\\D4WTPPACKAGECONTENT',
      'D4WTPPACKAGECONTENT2RESULT' => 'Axess\\Dci4Wtp\\D4WTPPACKAGECONTENT2RESULT',
      'ArrayOfD4WTPPACKAGECONTENT2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPACKAGECONTENT2',
      'D4WTPPACKAGECONTENT2' => 'Axess\\Dci4Wtp\\D4WTPPACKAGECONTENT2',
      'D4WTPPACKAGECONTENT3RESULT' => 'Axess\\Dci4Wtp\\D4WTPPACKAGECONTENT3RESULT',
      'ArrayOfD4WTPPACKAGECONTENT3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPACKAGECONTENT3',
      'D4WTPPACKAGECONTENT3' => 'Axess\\Dci4Wtp\\D4WTPPACKAGECONTENT3',
      'D4WTPPACKAGECONTENT4RESULT' => 'Axess\\Dci4Wtp\\D4WTPPACKAGECONTENT4RESULT',
      'ArrayOfD4WTPPACKAGECONTENT4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPACKAGECONTENT4',
      'D4WTPPACKAGECONTENT4' => 'Axess\\Dci4Wtp\\D4WTPPACKAGECONTENT4',
      'D4WTPPKGTARIFFLISTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLISTREQUEST',
      'D4WTPPKGTARIFFLISTRESULT' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLISTRESULT',
      'ArrayOfD4WTPPKGTARIFFLIST' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLIST',
      'D4WTPPKGTARIFFLIST' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST',
      'ArrayOfD4WTPPKGTARIFFLISTDAY' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLISTDAY',
      'D4WTPPKGTARIFFLISTDAY' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLISTDAY',
      'ArrayOfD4WTPPACKAGELIST' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPACKAGELIST',
      'D4WTPPACKAGELIST' => 'Axess\\Dci4Wtp\\D4WTPPACKAGELIST',
      'D4WTPPACKAGEARTICLE' => 'Axess\\Dci4Wtp\\D4WTPPACKAGEARTICLE',
      'D4WTPPKGTARIFF' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFF',
      'ArrayOfD4WTPADDARTTARIFF' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPADDARTTARIFF',
      'D4WTPADDARTTARIFF' => 'Axess\\Dci4Wtp\\D4WTPADDARTTARIFF',
      'ArrayOfD4WTPCONTINGENTLIST' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCONTINGENTLIST',
      'D4WTPCONTINGENTLIST' => 'Axess\\Dci4Wtp\\D4WTPCONTINGENTLIST',
      'ArrayOfD4WTPDATTRAEGNO' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPDATTRAEGNO',
      'D4WTPDATTRAEGNO' => 'Axess\\Dci4Wtp\\D4WTPDATTRAEGNO',
      'D4WTPPKGTARIFFLIST2RESULT' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST2RESULT',
      'ArrayOfD4WTPPKGTARIFFLIST2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLIST2',
      'D4WTPPKGTARIFFLIST2' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST2',
      'ArrayOfD4WTPPKGTARIFFLISTDAY2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLISTDAY2',
      'D4WTPPKGTARIFFLISTDAY2' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLISTDAY2',
      'D4WTPPKGTARIFF2' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFF2',
      'D4WTPPKGTARIFFLIST3RESULT' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST3RESULT',
      'ArrayOfD4WTPPKGTARIFFLIST3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLIST3',
      'D4WTPPKGTARIFFLIST3' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST3',
      'ArrayOfD4WTPPKGTARIFFLISTDAY3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLISTDAY3',
      'D4WTPPKGTARIFFLISTDAY3' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLISTDAY3',
      'D4WTPPKGTARIFF3' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFF3',
      'ArrayOfD4WTPADDARTTARIFF2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPADDARTTARIFF2',
      'D4WTPADDARTTARIFF2' => 'Axess\\Dci4Wtp\\D4WTPADDARTTARIFF2',
      'ArrayOfD4WTPCONTINGENTLIST3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCONTINGENTLIST3',
      'D4WTPCONTINGENTLIST3' => 'Axess\\Dci4Wtp\\D4WTPCONTINGENTLIST3',
      'ArrayOfD4WTPSUBCONTINGENTLIST2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSUBCONTINGENTLIST2',
      'D4WTPSUBCONTINGENTLIST2' => 'Axess\\Dci4Wtp\\D4WTPSUBCONTINGENTLIST2',
      'D4WTPRESARTICLETARIFF' => 'Axess\\Dci4Wtp\\D4WTPRESARTICLETARIFF',
      'D4WTPPKGTARIFFLIST2REQUEST' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST2REQUEST',
      'D4WTPPKGTARIFFLIST4RESULT' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST4RESULT',
      'ArrayOfD4WTPPKGTARIFFLIST4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLIST4',
      'D4WTPPKGTARIFFLIST4' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST4',
      'ArrayOfD4WTPPKGTARIFFLISTDAY4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLISTDAY4',
      'D4WTPPKGTARIFFLISTDAY4' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLISTDAY4',
      'D4WTPPACKAGEARTICLE2' => 'Axess\\Dci4Wtp\\D4WTPPACKAGEARTICLE2',
      'D4WTPCOMPANYCONDITION' => 'Axess\\Dci4Wtp\\D4WTPCOMPANYCONDITION',
      'D4WTPPKGTARIFF4' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFF4',
      'ArrayOfD4WTPADDARTTARIFF4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPADDARTTARIFF4',
      'D4WTPADDARTTARIFF4' => 'Axess\\Dci4Wtp\\D4WTPADDARTTARIFF4',
      'ArrayOfD4WTPCONTINGENTLIST5' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCONTINGENTLIST5',
      'D4WTPCONTINGENTLIST5' => 'Axess\\Dci4Wtp\\D4WTPCONTINGENTLIST5',
      'ArrayOfD4WTPSUBCONTINGENTLIST3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSUBCONTINGENTLIST3',
      'D4WTPSUBCONTINGENTLIST3' => 'Axess\\Dci4Wtp\\D4WTPSUBCONTINGENTLIST3',
      'D4WTPDYNAMICPRICING' => 'Axess\\Dci4Wtp\\D4WTPDYNAMICPRICING',
      'ArrayOfD4WTPDISCOUNT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPDISCOUNT',
      'D4WTPPKGTARIFFLIST5RESULT' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST5RESULT',
      'ArrayOfD4WTPPKGTARIFFLIST5' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLIST5',
      'D4WTPPKGTARIFFLIST5' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST5',
      'ArrayOfD4WTPPKGTARIFFLISTDAY5' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLISTDAY5',
      'D4WTPPKGTARIFFLISTDAY5' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLISTDAY5',
      'D4WTPPKGTARIFF5' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFF5',
      'ArrayOfD4WTPCONTINGENTLIST6' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCONTINGENTLIST6',
      'D4WTPCONTINGENTLIST6' => 'Axess\\Dci4Wtp\\D4WTPCONTINGENTLIST6',
      'ArrayOfD4WTPSUBCONTINGENTLIST4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSUBCONTINGENTLIST4',
      'D4WTPSUBCONTINGENTLIST4' => 'Axess\\Dci4Wtp\\D4WTPSUBCONTINGENTLIST4',
      'D4WTPPKGTARIFFLIST6REQUEST' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST6REQUEST',
      'D4WTPARCTIMESELECTION' => 'Axess\\Dci4Wtp\\D4WTPARCTIMESELECTION',
      'D4WTPPKGTARIFFLIST6RESULT' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST6RESULT',
      'ArrayOfD4WTPPKGTARIFFLIST6' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLIST6',
      'D4WTPPKGTARIFFLIST6' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST6',
      'ArrayOfD4WTPPKGTARIFFLISTDAY6' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLISTDAY6',
      'D4WTPPKGTARIFFLISTDAY6' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLISTDAY6',
      'D4WTPPKGTARIFFLIST7RESULT' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST7RESULT',
      'ArrayOfD4WTPPKGTARIFFLIST7' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLIST7',
      'D4WTPPKGTARIFFLIST7' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST7',
      'ArrayOfD4WTPPKGTARIFFLISTDAY7' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLISTDAY7',
      'D4WTPPKGTARIFFLISTDAY7' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLISTDAY7',
      'ArrayOfD4WTPPACKAGELIST2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPACKAGELIST2',
      'D4WTPPACKAGELIST2' => 'Axess\\Dci4Wtp\\D4WTPPACKAGELIST2',
      'D4WTPPKGTARIFFLIST8RESULT' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST8RESULT',
      'ArrayOfD4WTPPKGTARIFFLIST8' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLIST8',
      'D4WTPPKGTARIFFLIST8' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST8',
      'ArrayOfD4WTPPKGTARIFFLISTDAY8' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLISTDAY8',
      'D4WTPPKGTARIFFLISTDAY8' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLISTDAY8',
      'ArrayOfD4WTPPACKAGELIST3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPACKAGELIST3',
      'D4WTPPACKAGELIST3' => 'Axess\\Dci4Wtp\\D4WTPPACKAGELIST3',
      'D4WTPPKGTARIFFLIST10RESULT' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST10RESULT',
      'ArrayOfD4WTPPKGTARIFFLIST10' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLIST10',
      'D4WTPPKGTARIFFLIST10' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLIST10',
      'ArrayOfD4WTPPKGTARIFFLISTDAY10' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLISTDAY10',
      'D4WTPPKGTARIFFLISTDAY10' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLISTDAY10',
      'ArrayOfD4WTPPACKAGELIST4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPACKAGELIST4',
      'D4WTPPACKAGELIST4' => 'Axess\\Dci4Wtp\\D4WTPPACKAGELIST4',
      'ArrayOfD4WTPPKGTARIFFLISTDAY9' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPKGTARIFFLISTDAY9',
      'D4WTPPKGTARIFFLISTDAY9' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFFLISTDAY9',
      'D4WTPPKGTARIFF6' => 'Axess\\Dci4Wtp\\D4WTPPKGTARIFF6',
      'D4WTPPACKAGEPOSTARIFREQUEST' => 'Axess\\Dci4Wtp\\D4WTPPACKAGEPOSTARIFREQUEST',
      'D4WTPPACKAGEPOSTARIFRESULT' => 'Axess\\Dci4Wtp\\D4WTPPACKAGEPOSTARIFRESULT',
      'D4WTPPRODUCTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTREQUEST',
      'D4WTPPRODUCTRESULT' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTRESULT',
      'ArrayOfD4WTPPRODUCTTYPES' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPRODUCTTYPES',
      'D4WTPPRODUCTTYPES' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTTYPES',
      'ArrayOfD4WTPPRODUCTS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPRODUCTS',
      'D4WTPPRODUCTS' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTS',
      'D4WTPGETTAXREQ' => 'Axess\\Dci4Wtp\\D4WTPGETTAXREQ',
      'D4WTPGETTAXRES' => 'Axess\\Dci4Wtp\\D4WTPGETTAXRES',
      'ArrayOfTAXARTICLES' => 'Axess\\Dci4Wtp\\ArrayOfTAXARTICLES',
      'TAXARTICLES' => 'Axess\\Dci4Wtp\\TAXARTICLES',
      'ArrayOfMWSTSATZ' => 'Axess\\Dci4Wtp\\ArrayOfMWSTSATZ',
      'MWSTSATZ' => 'Axess\\Dci4Wtp\\MWSTSATZ',
      'ArrayOfRENTALITEMMWST' => 'Axess\\Dci4Wtp\\ArrayOfRENTALITEMMWST',
      'RENTALITEMMWST' => 'Axess\\Dci4Wtp\\RENTALITEMMWST',
      'ArrayOfTICKETTYPEMWST' => 'Axess\\Dci4Wtp\\ArrayOfTICKETTYPEMWST',
      'TICKETTYPEMWST' => 'Axess\\Dci4Wtp\\TICKETTYPEMWST',
      'D4WTPTARIFFREQUEST' => 'Axess\\Dci4Wtp\\D4WTPTARIFFREQUEST',
      'D4WTPTARIFFRESULT' => 'Axess\\Dci4Wtp\\D4WTPTARIFFRESULT',
      'D4WTPTARIFF2REQUEST' => 'Axess\\Dci4Wtp\\D4WTPTARIFF2REQUEST',
      'D4WTPTARIFF2RESULT' => 'Axess\\Dci4Wtp\\D4WTPTARIFF2RESULT',
      'D4WTPTARIFFLISTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLISTREQUEST',
      'D4WTPPRODUCTLIST' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTLIST',
      'ArrayOfD4WTPPERSONTYPENO' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPERSONTYPENO',
      'D4WTPPERSONTYPENO' => 'Axess\\Dci4Wtp\\D4WTPPERSONTYPENO',
      'ArrayOfD4WTPPOOLNO' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPOOLNO',
      'D4WTPPOOLNO' => 'Axess\\Dci4Wtp\\D4WTPPOOLNO',
      'D4WTPTARIFFLISTRESULT' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLISTRESULT',
      'ArrayOfD4WTPTARIFFLIST' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLIST',
      'D4WTPTARIFFLIST' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST',
      'ArrayOfD4WTPTARIFFLISTDAY' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLISTDAY',
      'D4WTPTARIFFLISTDAY' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLISTDAY',
      'D4WTPTARIFFLIST2REQUEST' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST2REQUEST',
      'D4WTPTARIFFLIST2RESULT' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST2RESULT',
      'ArrayOfD4WTPTARIFFLIST2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLIST2',
      'D4WTPTARIFFLIST2' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST2',
      'ArrayOfD4WTPTARIFFLISTDAY2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLISTDAY2',
      'D4WTPTARIFFLISTDAY2' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLISTDAY2',
      'D4WTPTARIFFLIST3RESULT' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST3RESULT',
      'ArrayOfD4WTPTARIFFLIST3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLIST3',
      'D4WTPTARIFFLIST3' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST3',
      'ArrayOfD4WTPTARIFFLISTDAY3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLISTDAY3',
      'D4WTPTARIFFLISTDAY3' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLISTDAY3',
      'D4WTPTARIFFLIST4REQUEST' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST4REQUEST',
      'D4WTPTARIFFLIST4RESULT' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST4RESULT',
      'ArrayOfD4WTPTARIFFLIST4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLIST4',
      'D4WTPTARIFFLIST4' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST4',
      'ArrayOfD4WTPTARIFFLISTDAY4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLISTDAY4',
      'D4WTPTARIFFLISTDAY4' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLISTDAY4',
      'ArrayOfD4WTPCONTINGENTLIST2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCONTINGENTLIST2',
      'D4WTPCONTINGENTLIST2' => 'Axess\\Dci4Wtp\\D4WTPCONTINGENTLIST2',
      'ArrayOfD4WTPSUBCONTINGENTLIST' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSUBCONTINGENTLIST',
      'D4WTPSUBCONTINGENTLIST' => 'Axess\\Dci4Wtp\\D4WTPSUBCONTINGENTLIST',
      'D4WTPTARIFFLIST5RESULT' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST5RESULT',
      'ArrayOfD4WTPTARIFFLIST5' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLIST5',
      'D4WTPTARIFFLIST5' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST5',
      'ArrayOfD4WTPTARIFFLISTDAY5' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLISTDAY5',
      'D4WTPTARIFFLISTDAY5' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLISTDAY5',
      'D4WTPTARIFFLIST6REQUEST' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST6REQUEST',
      'D4WTPTARIFFLIST6RESULT' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST6RESULT',
      'ArrayOfD4WTPTARIFFLIST6' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLIST6',
      'D4WTPTARIFFLIST6' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST6',
      'ArrayOfD4WTPTARIFFLISTDAY6' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLISTDAY6',
      'D4WTPTARIFFLISTDAY6' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLISTDAY6',
      'ArrayOfD4WTPADDARTTARIFF3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPADDARTTARIFF3',
      'D4WTPADDARTTARIFF3' => 'Axess\\Dci4Wtp\\D4WTPADDARTTARIFF3',
      'ArrayOfD4WTPCONTINGENTLIST4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCONTINGENTLIST4',
      'D4WTPCONTINGENTLIST4' => 'Axess\\Dci4Wtp\\D4WTPCONTINGENTLIST4',
      'D4WTPTARIFFLIST7REQUEST' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST7REQUEST',
      'D4WTPTARIFFLIST7RESULT' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST7RESULT',
      'ArrayOfD4WTPTARIFFLIST7' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLIST7',
      'D4WTPTARIFFLIST7' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST7',
      'ArrayOfD4WTPTARIFFLISTDAY7' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLISTDAY7',
      'D4WTPTARIFFLISTDAY7' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLISTDAY7',
      'D4WTPTARIFFLIST8RESULT' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST8RESULT',
      'ArrayOfD4WTPTARIFFLIST8' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLIST8',
      'D4WTPTARIFFLIST8' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST8',
      'ArrayOfD4WTPTARIFFLISTDAY8' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLISTDAY8',
      'D4WTPTARIFFLISTDAY8' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLISTDAY8',
      'D4WTPTARIFFLIST9RESULT' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST9RESULT',
      'ArrayOfD4WTPTARIFFLIST9' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLIST9',
      'D4WTPTARIFFLIST9' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLIST9',
      'ArrayOfD4WTPTARIFFLISTDAY9' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTARIFFLISTDAY9',
      'D4WTPTARIFFLISTDAY9' => 'Axess\\Dci4Wtp\\D4WTPTARIFFLISTDAY9',
      'ArrayOfWTPALTCONTINGENT' => 'Axess\\Dci4Wtp\\ArrayOfWTPALTCONTINGENT',
      'WTPALTCONTINGENT' => 'Axess\\Dci4Wtp\\WTPALTCONTINGENT',
      'D4WTPPACKAGETARIFFSREQUEST' => 'Axess\\Dci4Wtp\\D4WTPPACKAGETARIFFSREQUEST',
      'D4WTPPACKAGERESTRICTION' => 'Axess\\Dci4Wtp\\D4WTPPACKAGERESTRICTION',
      'D4WTPPACKAGETARIFFSRESULT' => 'Axess\\Dci4Wtp\\D4WTPPACKAGETARIFFSRESULT',
      'ArrayOfD4WTPPACKAGETARIFF' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPACKAGETARIFF',
      'D4WTPPACKAGETARIFF' => 'Axess\\Dci4Wtp\\D4WTPPACKAGETARIFF',
      'ArrayOfD4WTPPACKAGEPOSTARIFFCALC' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPACKAGEPOSTARIFFCALC',
      'D4WTPPACKAGEPOSTARIFFCALC' => 'Axess\\Dci4Wtp\\D4WTPPACKAGEPOSTARIFFCALC',
      'D4WTPPACKAGEPOSTARIFF' => 'Axess\\Dci4Wtp\\D4WTPPACKAGEPOSTARIFF',
      'D4WTPISSUETICKETREQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKETREQUEST',
      'ArrayOfD4WTPPRODUCTORDER' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPRODUCTORDER',
      'D4WTPPRODUCTORDER' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTORDER',
      'D4WTPCREDITCARDDATA' => 'Axess\\Dci4Wtp\\D4WTPCREDITCARDDATA',
      'D4WTPISSUETICKETRESULT' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKETRESULT',
      'ArrayOfD4WTPTICKETPRODDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETPRODDATA',
      'D4WTPTICKETPRODDATA' => 'Axess\\Dci4Wtp\\D4WTPTICKETPRODDATA',
      'D4WTPISSUETICKET2RESULT' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET2RESULT',
      'D4WTPISSUETICKET3REQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET3REQUEST',
      'ArrayOfD4WTPPRODUCTORDER3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPRODUCTORDER3',
      'D4WTPPRODUCTORDER3' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTORDER3',
      'ArrayOfD4WTPERSONDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPERSONDATA',
      'D4WTPISSUETICKET3RESULT' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET3RESULT',
      'ArrayOfD4WTPTICKETPRODDATA3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETPRODDATA3',
      'D4WTPTICKETPRODDATA3' => 'Axess\\Dci4Wtp\\D4WTPTICKETPRODDATA3',
      'D4WTPISSUETICKET4REQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET4REQUEST',
      'ArrayOfD4WTPPRODUCTORDER4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPRODUCTORDER4',
      'D4WTPPRODUCTORDER4' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTORDER4',
      'D4WTPISSUETICKET5REQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET5REQUEST',
      'D4WTPISSUETICKET5RESULT' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET5RESULT',
      'ArrayOfD4WTPTICKETPRODDATA5' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETPRODDATA5',
      'D4WTPISSUETICKET6REQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET6REQUEST',
      'ArrayOfD4WTPPRODUCTORDER6' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPRODUCTORDER6',
      'D4WTPPRODUCTORDER6' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTORDER6',
      'D4WTPISSUETICKET7REQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET7REQUEST',
      'D4WTPISSUETICKET8REQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET8REQUEST',
      'ArrayOfD4WTPPAYMENTTYPE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPAYMENTTYPE',
      'D4WTPPAYMENTTYPE' => 'Axess\\Dci4Wtp\\D4WTPPAYMENTTYPE',
      'D4WTPPAYERDATA' => 'Axess\\Dci4Wtp\\D4WTPPAYERDATA',
      'D4WTPISSUETICKET9REQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET9REQUEST',
      'D4WTPISSUETICKET6RESULT' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET6RESULT',
      'ArrayOfD4WTPTICKETPRODDATA6' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETPRODDATA6',
      'D4WTPTICKETPRODDATA6' => 'Axess\\Dci4Wtp\\D4WTPTICKETPRODDATA6',
      'D4WTPISSUETICKET10REQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET10REQUEST',
      'ArrayOfD4WTPPRODUCTORDER8' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPRODUCTORDER8',
      'D4WTPPRODUCTORDER8' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTORDER8',
      'D4WTPISSUETICKET10RESULT' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET10RESULT',
      'ArrayOfD4WTPTICKETPRODDATA10' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETPRODDATA10',
      'D4WTPTICKETPRODDATA10' => 'Axess\\Dci4Wtp\\D4WTPTICKETPRODDATA10',
      'D4WTPISSUETICKET11REQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET11REQUEST',
      'ArrayOfD4WTPPRODUCTORDER9' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPRODUCTORDER9',
      'D4WTPPRODUCTORDER9' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTORDER9',
      'D4WTPRENTALPRODUCTORDER2' => 'Axess\\Dci4Wtp\\D4WTPRENTALPRODUCTORDER2',
      'ArrayOfD4WTPRENTALITEMPRODUCT3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRENTALITEMPRODUCT3',
      'D4WTPRENTALITEMPRODUCT3' => 'Axess\\Dci4Wtp\\D4WTPRENTALITEMPRODUCT3',
      'D4WTPISSUETICKET12REQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET12REQUEST',
      'ArrayOfD4WTPPRODUCTORDER10' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPRODUCTORDER10',
      'D4WTPPRODUCTORDER10' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTORDER10',
      'D4WTPRENTALPRODUCTORDER3' => 'Axess\\Dci4Wtp\\D4WTPRENTALPRODUCTORDER3',
      'ArrayOfD4WTPRENTALITEMPRODUCT4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPRENTALITEMPRODUCT4',
      'D4WTPRENTALITEMPRODUCT4' => 'Axess\\Dci4Wtp\\D4WTPRENTALITEMPRODUCT4',
      'D4WTPISSUETICKET13REQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET13REQUEST',
      'ArrayOfD4WTPPRODUCTORDER11' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPPRODUCTORDER11',
      'D4WTPPRODUCTORDER11' => 'Axess\\Dci4Wtp\\D4WTPPRODUCTORDER11',
      'D4WTPISSUEWARE' => 'Axess\\Dci4Wtp\\D4WTPISSUEWARE',
      'D4WTPISSUETICKET11RESULT' => 'Axess\\Dci4Wtp\\D4WTPISSUETICKET11RESULT',
      'ArrayOfD4WTPTICKETPRODDATA11' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETPRODDATA11',
      'D4WTPTICKETPRODDATA11' => 'Axess\\Dci4Wtp\\D4WTPTICKETPRODDATA11',
      'D4WTPISSUEMODIFYREQ' => 'Axess\\Dci4Wtp\\D4WTPISSUEMODIFYREQ',
      'D4WTPISSUEMODIFYRES' => 'Axess\\Dci4Wtp\\D4WTPISSUEMODIFYRES',
      'D4WTPMSGISSUETICKETREQUEST' => 'Axess\\Dci4Wtp\\D4WTPMSGISSUETICKETREQUEST',
      'D4WTPMSGISSUETICKETRESULT' => 'Axess\\Dci4Wtp\\D4WTPMSGISSUETICKETRESULT',
      'D4WTPMSGISSUETICKETREQUEST2' => 'Axess\\Dci4Wtp\\D4WTPMSGISSUETICKETREQUEST2',
      'D4WTPMSGISSUETICKETRESULT2' => 'Axess\\Dci4Wtp\\D4WTPMSGISSUETICKETRESULT2',
      'D4WTPMSGISSUETICKETREQUEST3' => 'Axess\\Dci4Wtp\\D4WTPMSGISSUETICKETREQUEST3',
      'ArrayOfD4WTPMSGTICKETDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPMSGTICKETDATA',
      'D4WTPMSGTICKETDATA' => 'Axess\\Dci4Wtp\\D4WTPMSGTICKETDATA',
      'D4WTPMSGISSUETICKETRESULT3' => 'Axess\\Dci4Wtp\\D4WTPMSGISSUETICKETRESULT3',
      'ArrayOfD4WTPMSGTICKETRESDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPMSGTICKETRESDATA',
      'D4WTPMSGTICKETRESDATA' => 'Axess\\Dci4Wtp\\D4WTPMSGTICKETRESDATA',
      'D4WTPMSGISSUETICKETREQUEST5' => 'Axess\\Dci4Wtp\\D4WTPMSGISSUETICKETREQUEST5',
      'ArrayOfD4WTPMSGTICKETDATA2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPMSGTICKETDATA2',
      'D4WTPMSGTICKETDATA2' => 'Axess\\Dci4Wtp\\D4WTPMSGTICKETDATA2',
      'D4WTPMSGISSUETICKETREQUEST6' => 'Axess\\Dci4Wtp\\D4WTPMSGISSUETICKETREQUEST6',
      'ArrayOfD4WTPMSGTICKETDATA3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPMSGTICKETDATA3',
      'D4WTPMSGTICKETDATA3' => 'Axess\\Dci4Wtp\\D4WTPMSGTICKETDATA3',
      'D4WTPMSGISSUETICKETREQUEST7' => 'Axess\\Dci4Wtp\\D4WTPMSGISSUETICKETREQUEST7',
      'ArrayOfD4WTPMSGTICKETDATA4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPMSGTICKETDATA4',
      'D4WTPMSGTICKETDATA4' => 'Axess\\Dci4Wtp\\D4WTPMSGTICKETDATA4',
      'D4WTPMSGISSUETICKETRESULT7' => 'Axess\\Dci4Wtp\\D4WTPMSGISSUETICKETRESULT7',
      'ArrayOfD4WTPMSGTICKETRESDATA2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPMSGTICKETRESDATA2',
      'D4WTPMSGTICKETRESDATA2' => 'Axess\\Dci4Wtp\\D4WTPMSGTICKETRESDATA2',
      'D4WTPMSGRENTALRES' => 'Axess\\Dci4Wtp\\D4WTPMSGRENTALRES',
      'ArrayOfD4WTPLOCKERMEDIAIDS' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPLOCKERMEDIAIDS',
      'D4WTPLOCKERMEDIAIDS' => 'Axess\\Dci4Wtp\\D4WTPLOCKERMEDIAIDS',
      'D4WTPMSGISSUETICKETREQUEST8' => 'Axess\\Dci4Wtp\\D4WTPMSGISSUETICKETREQUEST8',
      'ArrayOfD4WTPMSGTICKETDATA5' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPMSGTICKETDATA5',
      'D4WTPMSGTICKETDATA5' => 'Axess\\Dci4Wtp\\D4WTPMSGTICKETDATA5',
      'D4WTPMSGISSUETICKETRESULT8' => 'Axess\\Dci4Wtp\\D4WTPMSGISSUETICKETRESULT8',
      'ArrayOfD4WTPMSGTICKETRESDATA3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPMSGTICKETRESDATA3',
      'D4WTPMSGTICKETRESDATA3' => 'Axess\\Dci4Wtp\\D4WTPMSGTICKETRESDATA3',
      'D4WTPISSUEPACKAGEREQUEST' => 'Axess\\Dci4Wtp\\D4WTPISSUEPACKAGEREQUEST',
      'D4WTPPACKAGEORDER' => 'Axess\\Dci4Wtp\\D4WTPPACKAGEORDER',
      'D4WTPISSUEPACKAGERESULT' => 'Axess\\Dci4Wtp\\D4WTPISSUEPACKAGERESULT',
      'D4WTPCHECK4REBOOKREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCHECK4REBOOKREQUEST',
      'ArrayOfD4WTPTICKETSALE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPTICKETSALE',
      'D4WTPTICKETSALE' => 'Axess\\Dci4Wtp\\D4WTPTICKETSALE',
      'D4WTPCHECK4REBOOKRESULT' => 'Axess\\Dci4Wtp\\D4WTPCHECK4REBOOKRESULT',
      'ArrayOfD4WTPCHECKED4REBOOK' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCHECKED4REBOOK',
      'D4WTPCHECKED4REBOOK' => 'Axess\\Dci4Wtp\\D4WTPCHECKED4REBOOK',
      'D4WTPREBOOKREQUEST' => 'Axess\\Dci4Wtp\\D4WTPREBOOKREQUEST',
      'D4WTPREBOOKRESULT' => 'Axess\\Dci4Wtp\\D4WTPREBOOKRESULT',
      'D4WTPREBOOKREQUEST2' => 'Axess\\Dci4Wtp\\D4WTPREBOOKREQUEST2',
      'D4WTPREBOOKTRANSREQ' => 'Axess\\Dci4Wtp\\D4WTPREBOOKTRANSREQ',
      'D4WTPSPLITPAYMENTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPSPLITPAYMENTREQUEST',
      'D4WTPSPLITPAYMENTRESULT' => 'Axess\\Dci4Wtp\\D4WTPSPLITPAYMENTRESULT',
      'ArrayOfD4WTPSPLITPAYMENT' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSPLITPAYMENT',
      'D4WTPSPLITPAYMENT' => 'Axess\\Dci4Wtp\\D4WTPSPLITPAYMENT',
      'D4WTPRELOADTICKETREQUEST' => 'Axess\\Dci4Wtp\\D4WTPRELOADTICKETREQUEST',
      'D4WTPRELOADTICKETRESULT' => 'Axess\\Dci4Wtp\\D4WTPRELOADTICKETRESULT',
      'D4WTPCANCELTICKETREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCANCELTICKETREQUEST',
      'D4WTPCANCELTICKET2RESULT' => 'Axess\\Dci4Wtp\\D4WTPCANCELTICKET2RESULT',
      'D4WTPCANCELTICKETRESULT' => 'Axess\\Dci4Wtp\\D4WTPCANCELTICKETRESULT',
      'D4WTPCANCELTICKET3REQUEST' => 'Axess\\Dci4Wtp\\D4WTPCANCELTICKET3REQUEST',
      'D4WTPCANCELTICKET3RESULT' => 'Axess\\Dci4Wtp\\D4WTPCANCELTICKET3RESULT',
      'D4WTPCANCELTICKET4REQUEST' => 'Axess\\Dci4Wtp\\D4WTPCANCELTICKET4REQUEST',
      'D4WTPCANCELTICKET4RESULT' => 'Axess\\Dci4Wtp\\D4WTPCANCELTICKET4RESULT',
      'ArrayOfD4WTPCANCELRENTAL' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCANCELRENTAL',
      'D4WTPCANCELRENTAL' => 'Axess\\Dci4Wtp\\D4WTPCANCELRENTAL',
      'D4WTPCANCELTICKET5REQUEST' => 'Axess\\Dci4Wtp\\D4WTPCANCELTICKET5REQUEST',
      'ArrayOfDCI4WTPWARECANCEL' => 'Axess\\Dci4Wtp\\ArrayOfDCI4WTPWARECANCEL',
      'DCI4WTPWARECANCEL' => 'Axess\\Dci4Wtp\\DCI4WTPWARECANCEL',
      'ArrayOfDCI4WTPLISTSERIALNO' => 'Axess\\Dci4Wtp\\ArrayOfDCI4WTPLISTSERIALNO',
      'DCI4WTPLISTSERIALNO' => 'Axess\\Dci4Wtp\\DCI4WTPLISTSERIALNO',
      'D4WTPCANCELTICKET5RESULT' => 'Axess\\Dci4Wtp\\D4WTPCANCELTICKET5RESULT',
      'ArrayOfD4WTPCANCELWARE' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPCANCELWARE',
      'D4WTPCANCELWARE' => 'Axess\\Dci4Wtp\\D4WTPCANCELWARE',
      'D4WTPCANCELTICKET6REQUEST' => 'Axess\\Dci4Wtp\\D4WTPCANCELTICKET6REQUEST',
      'D4WTPMSGCANCELTICKETREQUEST' => 'Axess\\Dci4Wtp\\D4WTPMSGCANCELTICKETREQUEST',
      'D4WTPMSGCANCELTICKETRESULT' => 'Axess\\Dci4Wtp\\D4WTPMSGCANCELTICKETRESULT',
      'D4WTPSALESDATAREQUEST' => 'Axess\\Dci4Wtp\\D4WTPSALESDATAREQUEST',
      'D4WTPSALESDATARESULT' => 'Axess\\Dci4Wtp\\D4WTPSALESDATARESULT',
      'ArrayOfD4WTPSALESDATA' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSALESDATA',
      'D4WTPSALESDATA' => 'Axess\\Dci4Wtp\\D4WTPSALESDATA',
      'D4WTPSALESDATAREQUEST2' => 'Axess\\Dci4Wtp\\D4WTPSALESDATAREQUEST2',
      'D4WTPSALESDATARESULT2' => 'Axess\\Dci4Wtp\\D4WTPSALESDATARESULT2',
      'ArrayOfD4WTPSALESDATA2' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSALESDATA2',
      'D4WTPSALESDATA2' => 'Axess\\Dci4Wtp\\D4WTPSALESDATA2',
      'D4WTPSALESDATAREQUEST3' => 'Axess\\Dci4Wtp\\D4WTPSALESDATAREQUEST3',
      'D4WTPSALESDATARESULT3' => 'Axess\\Dci4Wtp\\D4WTPSALESDATARESULT3',
      'ArrayOfD4WTPSALESDATA3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSALESDATA3',
      'D4WTPSALESDATA3' => 'Axess\\Dci4Wtp\\D4WTPSALESDATA3',
      'D4WTPSALESDATAREQUEST4' => 'Axess\\Dci4Wtp\\D4WTPSALESDATAREQUEST4',
      'D4WTPSALESDATARESULT4' => 'Axess\\Dci4Wtp\\D4WTPSALESDATARESULT4',
      'ArrayOfD4WTPSALESDATA4' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPSALESDATA4',
      'D4WTPSALESDATA4' => 'Axess\\Dci4Wtp\\D4WTPSALESDATA4',
      'D4WTPNONCONSECUTIVEDAYSINFO' => 'Axess\\Dci4Wtp\\D4WTPNONCONSECUTIVEDAYSINFO',
      'D4WTPREPORTREQUEST' => 'Axess\\Dci4Wtp\\D4WTPREPORTREQUEST',
      'D4WTPREPORTRESULT' => 'Axess\\Dci4Wtp\\D4WTPREPORTRESULT',
      'D4WTPTICKETDATAREQUEST' => 'Axess\\Dci4Wtp\\D4WTPTICKETDATAREQUEST',
      'D4WTPTICKETDATARESULT' => 'Axess\\Dci4Wtp\\D4WTPTICKETDATARESULT',
      'D4WTPCHECKWTPNOREQUEST' => 'Axess\\Dci4Wtp\\D4WTPCHECKWTPNOREQUEST',
      'D4WTPCHECKWTPNORESULT' => 'Axess\\Dci4Wtp\\D4WTPCHECKWTPNORESULT',
      'D4WTPCHECKWTPNO2RESULT' => 'Axess\\Dci4Wtp\\D4WTPCHECKWTPNO2RESULT',
      'D4WTPCHECKWTPNO3RESULT' => 'Axess\\Dci4Wtp\\D4WTPCHECKWTPNO3RESULT',
      'D4WTPCHECKWTPNO2REQUEST' => 'Axess\\Dci4Wtp\\D4WTPCHECKWTPNO2REQUEST',
      'D4WTPGETPERSONREQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETPERSONREQUEST',
      'D4WTPGETPERSONRESULT' => 'Axess\\Dci4Wtp\\D4WTPGETPERSONRESULT',
      'D4WTPGETPERSON2REQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETPERSON2REQUEST',
      'D4WTPGETPERSON2RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETPERSON2RESULT',
      'D4WTPGETPERSON3REQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETPERSON3REQUEST',
      'D4WTPGETPERSON3RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETPERSON3RESULT',
      'ArrayOfD4WTPERSONDATA3' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPERSONDATA3',
      'D4WTPGETPERSON4RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETPERSON4RESULT',
      'D4WTPGETPERSON4REQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETPERSON4REQUEST',
      'D4WTPGETPERSON5RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETPERSON5RESULT',
      'ArrayOfD4WTPERSONDATA6' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPERSONDATA6',
      'D4WTPERSONDATA6' => 'Axess\\Dci4Wtp\\D4WTPERSONDATA6',
      'D4WTPGETPERSON5REQUEST' => 'Axess\\Dci4Wtp\\D4WTPGETPERSON5REQUEST',
      'D4WTPGETPERSON6RESULT' => 'Axess\\Dci4Wtp\\D4WTPGETPERSON6RESULT',
      'ArrayOfD4WTPERSONDATA7' => 'Axess\\Dci4Wtp\\ArrayOfD4WTPERSONDATA7',
      'D4WTPERSONDATA7' => 'Axess\\Dci4Wtp\\D4WTPERSONDATA7',
      'CreateReservationRequest' => 'Axess\\Dci4Wtp\\CreateReservationRequest',
      'ArrayOfCreateReservationTicket' => 'Axess\\Dci4Wtp\\ArrayOfCreateReservationTicket',
      'CreateReservationTicket' => 'Axess\\Dci4Wtp\\CreateReservationTicket',
      'CreateBookingRequest' => 'Axess\\Dci4Wtp\\CreateBookingRequest',
      'CancelReservationRequest' => 'Axess\\Dci4Wtp\\CancelReservationRequest',
      'ArrayOfCancelReservationTicket' => 'Axess\\Dci4Wtp\\ArrayOfCancelReservationTicket',
      'CancelReservationTicket' => 'Axess\\Dci4Wtp\\CancelReservationTicket',
      'CheckAvailabilityRequest' => 'Axess\\Dci4Wtp\\CheckAvailabilityRequest',
      'GetDataForTicketRequest' => 'Axess\\Dci4Wtp\\GetDataForTicketRequest',
      'TicketValidity' => 'Axess\\Dci4Wtp\\TicketValidity',
      'ArrayOfTicketBooking' => 'Axess\\Dci4Wtp\\ArrayOfTicketBooking',
      'TicketBooking' => 'Axess\\Dci4Wtp\\TicketBooking',
      'ArrayOfTicketPOELimit' => 'Axess\\Dci4Wtp\\ArrayOfTicketPOELimit',
      'TicketPOELimit' => 'Axess\\Dci4Wtp\\TicketPOELimit',
      'CreatePOEBookingRequest' => 'Axess\\Dci4Wtp\\CreatePOEBookingRequest',
      'ArrayOfPoeBooking' => 'Axess\\Dci4Wtp\\ArrayOfPoeBooking',
      'PoeBooking' => 'Axess\\Dci4Wtp\\PoeBooking',
      'CancelPOEBookingRequest' => 'Axess\\Dci4Wtp\\CancelPOEBookingRequest',
      'CreatePOEBookingWithCertRequest' => 'Axess\\Dci4Wtp\\CreatePOEBookingWithCertRequest',
      'CreatePOEBookingWithManualCertRequest' => 'Axess\\Dci4Wtp\\CreatePOEBookingWithManualCertRequest',
      'ArrayOfstring' => 'Axess\\Dci4Wtp\\ArrayOfstring',
      'ArrayOfint' => 'Axess\\Dci4Wtp\\ArrayOfint',
      'WTPCreateReservationResult' => 'Axess\\Dci4Wtp\\WTPCreateReservationResult',
      'WTPCreateBookingResult' => 'Axess\\Dci4Wtp\\WTPCreateBookingResult',
      'WTPCancelBookingResult' => 'Axess\\Dci4Wtp\\WTPCancelBookingResult',
      'WTPCheckAvailabilityResult' => 'Axess\\Dci4Wtp\\WTPCheckAvailabilityResult',
      'ArrayOfWTPAvailability' => 'Axess\\Dci4Wtp\\ArrayOfWTPAvailability',
      'WTPAvailability' => 'Axess\\Dci4Wtp\\WTPAvailability',
      'ArrayOfWTPProductsToIgnore' => 'Axess\\Dci4Wtp\\ArrayOfWTPProductsToIgnore',
      'WTPProductsToIgnore' => 'Axess\\Dci4Wtp\\WTPProductsToIgnore',
      'WTPTicketDataResult' => 'Axess\\Dci4Wtp\\WTPTicketDataResult',
      'WTPCreatePOEBookingResult' => 'Axess\\Dci4Wtp\\WTPCreatePOEBookingResult',
      'WTPCancelPOEBookingResult' => 'Axess\\Dci4Wtp\\WTPCancelPOEBookingResult',
      'WTPCreatePOEBookingWithCertResult' => 'Axess\\Dci4Wtp\\WTPCreatePOEBookingWithCertResult',
      'WTPCreatePOEBookingWithmanualCertResult' => 'Axess\\Dci4Wtp\\WTPCreatePOEBookingWithmanualCertResult',
      'QRCodeData' => 'Axess\\Dci4Wtp\\QRCodeData',
      'ArrayOfEntryStatePerDay' => 'Axess\\Dci4Wtp\\ArrayOfEntryStatePerDay',
      'EntryStatePerDay' => 'Axess\\Dci4Wtp\\EntryStatePerDay',
      'ArrayOfQRCodeManuallyAddedData' => 'Axess\\Dci4Wtp\\ArrayOfQRCodeManuallyAddedData',
      'QRCodeManuallyAddedData' => 'Axess\\Dci4Wtp\\QRCodeManuallyAddedData',
      'ArrayOfQRCodeRecoveryData' => 'Axess\\Dci4Wtp\\ArrayOfQRCodeRecoveryData',
      'QRCodeRecoveryData' => 'Axess\\Dci4Wtp\\QRCodeRecoveryData',
      'ArrayOfQRCodeTestData' => 'Axess\\Dci4Wtp\\ArrayOfQRCodeTestData',
      'QRCodeTestData' => 'Axess\\Dci4Wtp\\QRCodeTestData',
      'ArrayOfQRCodeVaccineData' => 'Axess\\Dci4Wtp\\ArrayOfQRCodeVaccineData',
      'QRCodeVaccineData' => 'Axess\\Dci4Wtp\\QRCodeVaccineData',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'http://demo-aster.teamaxess.com:16351/DCI4WTP/DCI4WTPService.svc?singleWsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param getConfigFile $parameters
     * @return getConfigFileResponse
     */
    public function getConfigFile(getConfigFile $parameters)
    {
      return $this->__soapCall('getConfigFile', array($parameters));
    }

    /**
     * @param getBOCVersion $parameters
     * @return getBOCVersionResponse
     */
    public function getBOCVersion(getBOCVersion $parameters)
    {
      return $this->__soapCall('getBOCVersion', array($parameters));
    }

    /**
     * @param getBOCUniversalTime $parameters
     * @return getBOCUniversalTimeResponse
     */
    public function getBOCUniversalTime(getBOCUniversalTime $parameters)
    {
      return $this->__soapCall('getBOCUniversalTime', array($parameters));
    }

    /**
     * @param axessForceGarbageColletor $parameters
     * @return axessForceGarbageColletorResponse
     */
    public function axessForceGarbageColletor(axessForceGarbageColletor $parameters)
    {
      return $this->__soapCall('axessForceGarbageColletor', array($parameters));
    }

    /**
     * @param CheckOneTimePwd $parameters
     * @return CheckOneTimePwdResponse
     */
    public function CheckOneTimePwd(CheckOneTimePwd $parameters)
    {
      return $this->__soapCall('CheckOneTimePwd', array($parameters));
    }

    /**
     * @param checkPromoCodes $parameters
     * @return checkPromoCodesResponse
     */
    public function checkPromoCodes(checkPromoCodes $parameters)
    {
      return $this->__soapCall('checkPromoCodes', array($parameters));
    }

    /**
     * @param editOrderHeader $parameters
     * @return editOrderHeaderResponse
     */
    public function editOrderHeader(editOrderHeader $parameters)
    {
      return $this->__soapCall('editOrderHeader', array($parameters));
    }

    /**
     * @param editOrderPositions $parameters
     * @return editOrderPositionsResponse
     */
    public function editOrderPositions(editOrderPositions $parameters)
    {
      return $this->__soapCall('editOrderPositions', array($parameters));
    }

    /**
     * @param getChipcardEntries $parameters
     * @return getChipcardEntriesResponse
     */
    public function getChipcardEntries(getChipcardEntries $parameters)
    {
      return $this->__soapCall('getChipcardEntries', array($parameters));
    }

    /**
     * @param getAdditionalCashiers $parameters
     * @return getAdditionalCashiersResponse
     */
    public function getAdditionalCashiers(getAdditionalCashiers $parameters)
    {
      return $this->__soapCall('getAdditionalCashiers', array($parameters));
    }

    /**
     * @param getSalesTransactions $parameters
     * @return getSalesTransactionsResponse
     */
    public function getSalesTransactions(getSalesTransactions $parameters)
    {
      return $this->__soapCall('getSalesTransactions', array($parameters));
    }

    /**
     * @param getRentalItemTariff $parameters
     * @return getRentalItemTariffResponse
     */
    public function getRentalItemTariff(getRentalItemTariff $parameters)
    {
      return $this->__soapCall('getRentalItemTariff', array($parameters));
    }

    /**
     * @param getRentalItemTariff2 $parameters
     * @return getRentalItemTariff2Response
     */
    public function getRentalItemTariff2(getRentalItemTariff2 $parameters)
    {
      return $this->__soapCall('getRentalItemTariff2', array($parameters));
    }

    /**
     * @param GetRentalItemTariff3 $parameters
     * @return GetRentalItemTariff3Response
     */
    public function GetRentalItemTariff3(GetRentalItemTariff3 $parameters)
    {
      return $this->__soapCall('GetRentalItemTariff3', array($parameters));
    }

    /**
     * @param GetRentalItemTariff4 $parameters
     * @return GetRentalItemTariff4Response
     */
    public function GetRentalItemTariff4(GetRentalItemTariff4 $parameters)
    {
      return $this->__soapCall('GetRentalItemTariff4', array($parameters));
    }

    /**
     * @param getPersRentalSales $parameters
     * @return getPersRentalSalesResponse
     */
    public function getPersRentalSales(getPersRentalSales $parameters)
    {
      return $this->__soapCall('getPersRentalSales', array($parameters));
    }

    /**
     * @param getWebRentalByProperty $parameters
     * @return getWebRentalByPropertyResponse
     */
    public function getWebRentalByProperty(getWebRentalByProperty $parameters)
    {
      return $this->__soapCall('getWebRentalByProperty', array($parameters));
    }

    /**
     * @param findShoppingCarts $parameters
     * @return findShoppingCartsResponse
     */
    public function findShoppingCarts(findShoppingCarts $parameters)
    {
      return $this->__soapCall('findShoppingCarts', array($parameters));
    }

    /**
     * @param lockShoppingCart $parameters
     * @return lockShoppingCartResponse
     */
    public function lockShoppingCart(lockShoppingCart $parameters)
    {
      return $this->__soapCall('lockShoppingCart', array($parameters));
    }

    /**
     * @param createExtOrderNr $parameters
     * @return createExtOrderNrResponse
     */
    public function createExtOrderNr(createExtOrderNr $parameters)
    {
      return $this->__soapCall('createExtOrderNr', array($parameters));
    }

    /**
     * @param issueShoppingCart $parameters
     * @return issueShoppingCartResponse
     */
    public function issueShoppingCart(issueShoppingCart $parameters)
    {
      return $this->__soapCall('issueShoppingCart', array($parameters));
    }

    /**
     * @param assignPersToCompany $parameters
     * @return assignPersToCompanyResponse
     */
    public function assignPersToCompany(assignPersToCompany $parameters)
    {
      return $this->__soapCall('assignPersToCompany', array($parameters));
    }

    /**
     * @param getPersFromCompany $parameters
     * @return getPersFromCompanyResponse
     */
    public function getPersFromCompany(getPersFromCompany $parameters)
    {
      return $this->__soapCall('getPersFromCompany', array($parameters));
    }

    /**
     * @param manageWorkOrder $parameters
     * @return manageWorkOrderResponse
     */
    public function manageWorkOrder(manageWorkOrder $parameters)
    {
      return $this->__soapCall('manageWorkOrder', array($parameters));
    }

    /**
     * @param getWorkOrders $parameters
     * @return getWorkOrdersResponse
     */
    public function getWorkOrders(getWorkOrders $parameters)
    {
      return $this->__soapCall('getWorkOrders', array($parameters));
    }

    /**
     * @param refundTicket $parameters
     * @return refundTicketResponse
     */
    public function refundTicket(refundTicket $parameters)
    {
      return $this->__soapCall('refundTicket', array($parameters));
    }

    /**
     * @param refundTicket2 $parameters
     * @return refundTicket2Response
     */
    public function refundTicket2(refundTicket2 $parameters)
    {
      return $this->__soapCall('refundTicket2', array($parameters));
    }

    /**
     * @param msgRefundTicket $parameters
     * @return msgRefundTicketResponse
     */
    public function msgRefundTicket(msgRefundTicket $parameters)
    {
      return $this->__soapCall('msgRefundTicket', array($parameters));
    }

    /**
     * @param getTravelGroupList $parameters
     * @return getTravelGroupListResponse
     */
    public function getTravelGroupList(getTravelGroupList $parameters)
    {
      return $this->__soapCall('getTravelGroupList', array($parameters));
    }

    /**
     * @param getTravelGroupList2 $parameters
     * @return getTravelGroupList2Response
     */
    public function getTravelGroupList2(getTravelGroupList2 $parameters)
    {
      return $this->__soapCall('getTravelGroupList2', array($parameters));
    }

    /**
     * @param getGroupSubContingent $parameters
     * @return getGroupSubContingentResponse
     */
    public function getGroupSubContingent(getGroupSubContingent $parameters)
    {
      return $this->__soapCall('getGroupSubContingent', array($parameters));
    }

    /**
     * @param getGroupSubContingent2 $parameters
     * @return getGroupSubContingent2Response
     */
    public function getGroupSubContingent2(getGroupSubContingent2 $parameters)
    {
      return $this->__soapCall('getGroupSubContingent2', array($parameters));
    }

    /**
     * @param getGroupSubContingent3 $parameters
     * @return getGroupSubContingent3Response
     */
    public function getGroupSubContingent3(getGroupSubContingent3 $parameters)
    {
      return $this->__soapCall('getGroupSubContingent3', array($parameters));
    }

    /**
     * @param checkBonusPoints $parameters
     * @return checkBonusPointsResponse
     */
    public function checkBonusPoints(checkBonusPoints $parameters)
    {
      return $this->__soapCall('checkBonusPoints', array($parameters));
    }

    /**
     * @param confirmBonusPointsRedeem $parameters
     * @return confirmBonusPointsRedeemResponse
     */
    public function confirmBonusPointsRedeem(confirmBonusPointsRedeem $parameters)
    {
      return $this->__soapCall('confirmBonusPointsRedeem', array($parameters));
    }

    /**
     * @param getDSGVOFlags $parameters
     * @return getDSGVOFlagsResponse
     */
    public function getDSGVOFlags(getDSGVOFlags $parameters)
    {
      return $this->__soapCall('getDSGVOFlags', array($parameters));
    }

    /**
     * @param setDSGVOFlags $parameters
     * @return setDSGVOFlagsResponse
     */
    public function setDSGVOFlags(setDSGVOFlags $parameters)
    {
      return $this->__soapCall('setDSGVOFlags', array($parameters));
    }

    /**
     * @param uploadPrivacyDoc $parameters
     * @return uploadPrivacyDocResponse
     */
    public function uploadPrivacyDoc(uploadPrivacyDoc $parameters)
    {
      return $this->__soapCall('uploadPrivacyDoc', array($parameters));
    }

    /**
     * @param createWTPData $parameters
     * @return createWTPDataResponse
     */
    public function createWTPData(createWTPData $parameters)
    {
      return $this->__soapCall('createWTPData', array($parameters));
    }

    /**
     * @param sendWTPDataOCC $parameters
     * @return sendWTPDataOCCResponse
     */
    public function sendWTPDataOCC(sendWTPDataOCC $parameters)
    {
      return $this->__soapCall('sendWTPDataOCC', array($parameters));
    }

    /**
     * @param addDTL4FamilyMember $parameters
     * @return addDTL4FamilyMemberResponse
     */
    public function addDTL4FamilyMember(addDTL4FamilyMember $parameters)
    {
      return $this->__soapCall('addDTL4FamilyMember', array($parameters));
    }

    /**
     * @param getDTL4FamilyExtendTariffs $parameters
     * @return getDTL4FamilyExtendTariffsResponse
     */
    public function getDTL4FamilyExtendTariffs(getDTL4FamilyExtendTariffs $parameters)
    {
      return $this->__soapCall('getDTL4FamilyExtendTariffs', array($parameters));
    }

    /**
     * @param addDTL4FamilyDays $parameters
     * @return addDTL4FamilyDaysResponse
     */
    public function addDTL4FamilyDays(addDTL4FamilyDays $parameters)
    {
      return $this->__soapCall('addDTL4FamilyDays', array($parameters));
    }

    /**
     * @param getDTL4FamilyData $parameters
     * @return getDTL4FamilyDataResponse
     */
    public function getDTL4FamilyData(getDTL4FamilyData $parameters)
    {
      return $this->__soapCall('getDTL4FamilyData', array($parameters));
    }

    /**
     * @param getBlankTypeList $parameters
     * @return getBlankTypeListResponse
     */
    public function getBlankTypeList(getBlankTypeList $parameters)
    {
      return $this->__soapCall('getBlankTypeList', array($parameters));
    }

    /**
     * @param getEMoneySteps $parameters
     * @return getEMoneyStepsResponse
     */
    public function getEMoneySteps(getEMoneySteps $parameters)
    {
      return $this->__soapCall('getEMoneySteps', array($parameters));
    }

    /**
     * @param getEmoneyAccountDetails $parameters
     * @return getEmoneyAccountDetailsResponse
     */
    public function getEmoneyAccountDetails(getEmoneyAccountDetails $parameters)
    {
      return $this->__soapCall('getEmoneyAccountDetails', array($parameters));
    }

    /**
     * @param changeEmoneyAccount $parameters
     * @return changeEmoneyAccountResponse
     */
    public function changeEmoneyAccount(changeEmoneyAccount $parameters)
    {
      return $this->__soapCall('changeEmoneyAccount', array($parameters));
    }

    /**
     * @param changeEmoneyAccount2 $parameters
     * @return changeEmoneyAccount2Response
     */
    public function changeEmoneyAccount2(changeEmoneyAccount2 $parameters)
    {
      return $this->__soapCall('changeEmoneyAccount2', array($parameters));
    }

    /**
     * @param eMoneyPayInOut $parameters
     * @return eMoneyPayInOutResponse
     */
    public function eMoneyPayInOut(eMoneyPayInOut $parameters)
    {
      return $this->__soapCall('eMoneyPayInOut', array($parameters));
    }

    /**
     * @param eMoneyPayTransaction $parameters
     * @return eMoneyPayTransactionResponse
     */
    public function eMoneyPayTransaction(eMoneyPayTransaction $parameters)
    {
      return $this->__soapCall('eMoneyPayTransaction', array($parameters));
    }

    /**
     * @param eMoneyPayTransaction2 $parameters
     * @return eMoneyPayTransaction2Response
     */
    public function eMoneyPayTransaction2(eMoneyPayTransaction2 $parameters)
    {
      return $this->__soapCall('eMoneyPayTransaction2', array($parameters));
    }

    /**
     * @param eMoneyBalanceHistory $parameters
     * @return eMoneyBalanceHistoryResponse
     */
    public function eMoneyBalanceHistory(eMoneyBalanceHistory $parameters)
    {
      return $this->__soapCall('eMoneyBalanceHistory', array($parameters));
    }

    /**
     * @param getAttributes $parameters
     * @return getAttributesResponse
     */
    public function getAttributes(getAttributes $parameters)
    {
      return $this->__soapCall('getAttributes', array($parameters));
    }

    /**
     * @param setAttributes $parameters
     * @return setAttributesResponse
     */
    public function setAttributes(setAttributes $parameters)
    {
      return $this->__soapCall('setAttributes', array($parameters));
    }

    /**
     * @param registerSwissPassSale $parameters
     * @return registerSwissPassSaleResponse
     */
    public function registerSwissPassSale(registerSwissPassSale $parameters)
    {
      return $this->__soapCall('registerSwissPassSale', array($parameters));
    }

    /**
     * @param isSwissPassSoldonDay $parameters
     * @return isSwissPassSoldonDayResponse
     */
    public function isSwissPassSoldonDay(isSwissPassSoldonDay $parameters)
    {
      return $this->__soapCall('isSwissPassSoldonDay', array($parameters));
    }

    /**
     * @param saveVoucherIDWTPProdsatz $parameters
     * @return saveVoucherIDWTPProdsatzResponse
     */
    public function saveVoucherIDWTPProdsatz(saveVoucherIDWTPProdsatz $parameters)
    {
      return $this->__soapCall('saveVoucherIDWTPProdsatz', array($parameters));
    }

    /**
     * @param getFirstValidDay $parameters
     * @return getFirstValidDayResponse
     */
    public function getFirstValidDay(getFirstValidDay $parameters)
    {
      return $this->__soapCall('getFirstValidDay', array($parameters));
    }

    /**
     * @param setCompensationTicket $parameters
     * @return setCompensationTicketResponse
     */
    public function setCompensationTicket(setCompensationTicket $parameters)
    {
      return $this->__soapCall('setCompensationTicket', array($parameters));
    }

    /**
     * @param getCompensationTicket $parameters
     * @return getCompensationTicketResponse
     */
    public function getCompensationTicket(getCompensationTicket $parameters)
    {
      return $this->__soapCall('getCompensationTicket', array($parameters));
    }

    /**
     * @param getContingentOccupacy $parameters
     * @return getContingentOccupacyResponse
     */
    public function getContingentOccupacy(getContingentOccupacy $parameters)
    {
      return $this->__soapCall('getContingentOccupacy', array($parameters));
    }

    /**
     * @param getContingentOccupacy2 $parameters
     * @return getContingentOccupacy2Response
     */
    public function getContingentOccupacy2(getContingentOccupacy2 $parameters)
    {
      return $this->__soapCall('getContingentOccupacy2', array($parameters));
    }

    /**
     * @param getDayOccupacy $parameters
     * @return getDayOccupacyResponse
     */
    public function getDayOccupacy(getDayOccupacy $parameters)
    {
      return $this->__soapCall('getDayOccupacy', array($parameters));
    }

    /**
     * @param getDayOccupacy2 $parameters
     * @return getDayOccupacy2Response
     */
    public function getDayOccupacy2(getDayOccupacy2 $parameters)
    {
      return $this->__soapCall('getDayOccupacy2', array($parameters));
    }

    /**
     * @param getRMProfiles $parameters
     * @return getRMProfilesResponse
     */
    public function getRMProfiles(getRMProfiles $parameters)
    {
      return $this->__soapCall('getRMProfiles', array($parameters));
    }

    /**
     * @param getEligibilityDiscounts $parameters
     * @return getEligibilityDiscountsResponse
     */
    public function getEligibilityDiscounts(getEligibilityDiscounts $parameters)
    {
      return $this->__soapCall('getEligibilityDiscounts', array($parameters));
    }

    /**
     * @param getCompanyBalance $parameters
     * @return getCompanyBalanceResponse
     */
    public function getCompanyBalance(getCompanyBalance $parameters)
    {
      return $this->__soapCall('getCompanyBalance', array($parameters));
    }

    /**
     * @param getProcessedTicketData $parameters
     * @return getProcessedTicketDataResponse
     */
    public function getProcessedTicketData(getProcessedTicketData $parameters)
    {
      return $this->__soapCall('getProcessedTicketData', array($parameters));
    }

    /**
     * @param replaceTicket $parameters
     * @return replaceTicketResponse
     */
    public function replaceTicket(replaceTicket $parameters)
    {
      return $this->__soapCall('replaceTicket', array($parameters));
    }

    /**
     * @param msgReplaceTicket $parameters
     * @return msgReplaceTicketResponse
     */
    public function msgReplaceTicket(msgReplaceTicket $parameters)
    {
      return $this->__soapCall('msgReplaceTicket', array($parameters));
    }

    /**
     * @param changePersData $parameters
     * @return changePersDataResponse
     */
    public function changePersData(changePersData $parameters)
    {
      return $this->__soapCall('changePersData', array($parameters));
    }

    /**
     * @param changePersData2 $parameters
     * @return changePersData2Response
     */
    public function changePersData2(changePersData2 $parameters)
    {
      return $this->__soapCall('changePersData2', array($parameters));
    }

    /**
     * @param changePersData3 $parameters
     * @return changePersData3Response
     */
    public function changePersData3(changePersData3 $parameters)
    {
      return $this->__soapCall('changePersData3', array($parameters));
    }

    /**
     * @param changePersData4 $parameters
     * @return changePersData4Response
     */
    public function changePersData4(changePersData4 $parameters)
    {
      return $this->__soapCall('changePersData4', array($parameters));
    }

    /**
     * @param readAuthorization $parameters
     * @return readAuthorizationResponse
     */
    public function readAuthorization(readAuthorization $parameters)
    {
      return $this->__soapCall('readAuthorization', array($parameters));
    }

    /**
     * @param createWTP64Bit $parameters
     * @return createWTP64BitResponse
     */
    public function createWTP64Bit(createWTP64Bit $parameters)
    {
      return $this->__soapCall('createWTP64Bit', array($parameters));
    }

    /**
     * @param blockTicketBySerial $parameters
     * @return blockTicketBySerialResponse
     */
    public function blockTicketBySerial(blockTicketBySerial $parameters)
    {
      return $this->__soapCall('blockTicketBySerial', array($parameters));
    }

    /**
     * @param unBlockTicketBySerial $parameters
     * @return unBlockTicketBySerialResponse
     */
    public function unBlockTicketBySerial(unBlockTicketBySerial $parameters)
    {
      return $this->__soapCall('unBlockTicketBySerial', array($parameters));
    }

    /**
     * @param blockUnBlockTicketByJournal $parameters
     * @return blockUnBlockTicketByJournalResponse
     */
    public function blockUnBlockTicketByJournal(blockUnBlockTicketByJournal $parameters)
    {
      return $this->__soapCall('blockUnBlockTicketByJournal', array($parameters));
    }

    /**
     * @param BlockUnblockTicket $parameters
     * @return BlockUnblockTicketResponse
     */
    public function BlockUnblockTicket(BlockUnblockTicket $parameters)
    {
      return $this->__soapCall('BlockUnblockTicket', array($parameters));
    }

    /**
     * @param findPrepaidTicket $parameters
     * @return findPrepaidTicketResponse
     */
    public function findPrepaidTicket(findPrepaidTicket $parameters)
    {
      return $this->__soapCall('findPrepaidTicket', array($parameters));
    }

    /**
     * @param findPrepaidTicket2 $parameters
     * @return findPrepaidTicket2Response
     */
    public function findPrepaidTicket2(findPrepaidTicket2 $parameters)
    {
      return $this->__soapCall('findPrepaidTicket2', array($parameters));
    }

    /**
     * @param findPrepaidTicket3 $parameters
     * @return findPrepaidTicket3Response
     */
    public function findPrepaidTicket3(findPrepaidTicket3 $parameters)
    {
      return $this->__soapCall('findPrepaidTicket3', array($parameters));
    }

    /**
     * @param findPrepaidTicket4 $parameters
     * @return findPrepaidTicket4Response
     */
    public function findPrepaidTicket4(findPrepaidTicket4 $parameters)
    {
      return $this->__soapCall('findPrepaidTicket4', array($parameters));
    }

    /**
     * @param producePrepaidTicket $parameters
     * @return producePrepaidTicketResponse
     */
    public function producePrepaidTicket(producePrepaidTicket $parameters)
    {
      return $this->__soapCall('producePrepaidTicket', array($parameters));
    }

    /**
     * @param producePrepaidTicket2 $parameters
     * @return producePrepaidTicket2Response
     */
    public function producePrepaidTicket2(producePrepaidTicket2 $parameters)
    {
      return $this->__soapCall('producePrepaidTicket2', array($parameters));
    }

    /**
     * @param producePrepaidTicket3 $parameters
     * @return producePrepaidTicket3Response
     */
    public function producePrepaidTicket3(producePrepaidTicket3 $parameters)
    {
      return $this->__soapCall('producePrepaidTicket3', array($parameters));
    }

    /**
     * @param producePrepaidTicket4 $parameters
     * @return producePrepaidTicket4Response
     */
    public function producePrepaidTicket4(producePrepaidTicket4 $parameters)
    {
      return $this->__soapCall('producePrepaidTicket4', array($parameters));
    }

    /**
     * @param setStateOCCPickupProduction $parameters
     * @return setStateOCCPickupProductionResponse
     */
    public function setStateOCCPickupProduction(setStateOCCPickupProduction $parameters)
    {
      return $this->__soapCall('setStateOCCPickupProduction', array($parameters));
    }

    /**
     * @param switchPrepaidTcktStatus $parameters
     * @return switchPrepaidTcktStatusResponse
     */
    public function switchPrepaidTcktStatus(switchPrepaidTcktStatus $parameters)
    {
      return $this->__soapCall('switchPrepaidTcktStatus', array($parameters));
    }

    /**
     * @param switchPrepaidTcktStatus2 $parameters
     * @return switchPrepaidTcktStatus2Response
     */
    public function switchPrepaidTcktStatus2(switchPrepaidTcktStatus2 $parameters)
    {
      return $this->__soapCall('switchPrepaidTcktStatus2', array($parameters));
    }

    /**
     * @param managePERSPhoto $parameters
     * @return managePERSPhotoResponse
     */
    public function managePERSPhoto(managePERSPhoto $parameters)
    {
      return $this->__soapCall('managePERSPhoto', array($parameters));
    }

    /**
     * @param decodeWTPNo64Bit $parameters
     * @return decodeWTPNo64BitResponse
     */
    public function decodeWTPNo64Bit(decodeWTPNo64Bit $parameters)
    {
      return $this->__soapCall('decodeWTPNo64Bit', array($parameters));
    }

    /**
     * @param checkIsSkiClubMember $parameters
     * @return checkIsSkiClubMemberResponse
     */
    public function checkIsSkiClubMember(checkIsSkiClubMember $parameters)
    {
      return $this->__soapCall('checkIsSkiClubMember', array($parameters));
    }

    /**
     * @param holeKundenDatenFromSwissPass $parameters
     * @return holeKundenDatenFromSwissPassResponse
     */
    public function holeKundenDatenFromSwissPass(holeKundenDatenFromSwissPass $parameters)
    {
      return $this->__soapCall('holeKundenDatenFromSwissPass', array($parameters));
    }

    /**
     * @param postLeistungsInfoToSwissPass $parameters
     * @return postLeistungsInfoToSwissPassResponse
     */
    public function postLeistungsInfoToSwissPass(postLeistungsInfoToSwissPass $parameters)
    {
      return $this->__soapCall('postLeistungsInfoToSwissPass', array($parameters));
    }

    /**
     * @param getWTPNoList $parameters
     * @return getWTPNoListResponse
     */
    public function getWTPNoList(getWTPNoList $parameters)
    {
      return $this->__soapCall('getWTPNoList', array($parameters));
    }

    /**
     * @param IsSwissPassEnabled $parameters
     * @return IsSwissPassEnabledResponse
     */
    public function IsSwissPassEnabled(IsSwissPassEnabled $parameters)
    {
      return $this->__soapCall('IsSwissPassEnabled', array($parameters));
    }

    /**
     * @param getReaderTrans $parameters
     * @return getReaderTransResponse
     */
    public function getReaderTrans(getReaderTrans $parameters)
    {
      return $this->__soapCall('getReaderTrans', array($parameters));
    }

    /**
     * @param getReaderTrans2 $parameters
     * @return getReaderTrans2Response
     */
    public function getReaderTrans2(getReaderTrans2 $parameters)
    {
      return $this->__soapCall('getReaderTrans2', array($parameters));
    }

    /**
     * @param getContingent $parameters
     * @return getContingentResponse
     */
    public function getContingent(getContingent $parameters)
    {
      return $this->__soapCall('getContingent', array($parameters));
    }

    /**
     * @param getContingent2 $parameters
     * @return getContingent2Response
     */
    public function getContingent2(getContingent2 $parameters)
    {
      return $this->__soapCall('getContingent2', array($parameters));
    }

    /**
     * @param getContingent3 $parameters
     * @return getContingent3Response
     */
    public function getContingent3(getContingent3 $parameters)
    {
      return $this->__soapCall('getContingent3', array($parameters));
    }

    /**
     * @param getContingent4 $parameters
     * @return getContingent4Response
     */
    public function getContingent4(getContingent4 $parameters)
    {
      return $this->__soapCall('getContingent4', array($parameters));
    }

    /**
     * @param getContingent5 $parameters
     * @return getContingent5Response
     */
    public function getContingent5(getContingent5 $parameters)
    {
      return $this->__soapCall('getContingent5', array($parameters));
    }

    /**
     * @param decContingent $parameters
     * @return decContingentResponse
     */
    public function decContingent(decContingent $parameters)
    {
      return $this->__soapCall('decContingent', array($parameters));
    }

    /**
     * @param nChangeGroup $parameters
     * @return nChangeGroupResponse
     */
    public function nChangeGroup(nChangeGroup $parameters)
    {
      return $this->__soapCall('nChangeGroup', array($parameters));
    }

    /**
     * @param getPersSalesData $parameters
     * @return getPersSalesDataResponse
     */
    public function getPersSalesData(getPersSalesData $parameters)
    {
      return $this->__soapCall('getPersSalesData', array($parameters));
    }

    /**
     * @param getPersSalesData2 $parameters
     * @return getPersSalesData2Response
     */
    public function getPersSalesData2(getPersSalesData2 $parameters)
    {
      return $this->__soapCall('getPersSalesData2', array($parameters));
    }

    /**
     * @param getWTPNoSalesData $parameters
     * @return getWTPNoSalesDataResponse
     */
    public function getWTPNoSalesData(getWTPNoSalesData $parameters)
    {
      return $this->__soapCall('getWTPNoSalesData', array($parameters));
    }

    /**
     * @param getWTPNoSalesData2 $parameters
     * @return getWTPNoSalesData2Response
     */
    public function getWTPNoSalesData2(getWTPNoSalesData2 $parameters)
    {
      return $this->__soapCall('getWTPNoSalesData2', array($parameters));
    }

    /**
     * @param authenticateMember $parameters
     * @return authenticateMemberResponse
     */
    public function authenticateMember(authenticateMember $parameters)
    {
      return $this->__soapCall('authenticateMember', array($parameters));
    }

    /**
     * @param getFamily $parameters
     * @return getFamilyResponse
     */
    public function getFamily(getFamily $parameters)
    {
      return $this->__soapCall('getFamily', array($parameters));
    }

    /**
     * @param setCreditCardReference $parameters
     * @return setCreditCardReferenceResponse
     */
    public function setCreditCardReference(setCreditCardReference $parameters)
    {
      return $this->__soapCall('setCreditCardReference', array($parameters));
    }

    /**
     * @param getCreditCardReference $parameters
     * @return getCreditCardReferenceResponse
     */
    public function getCreditCardReference(getCreditCardReference $parameters)
    {
      return $this->__soapCall('getCreditCardReference', array($parameters));
    }

    /**
     * @param delCreditCardReference $parameters
     * @return delCreditCardReferenceResponse
     */
    public function delCreditCardReference(delCreditCardReference $parameters)
    {
      return $this->__soapCall('delCreditCardReference', array($parameters));
    }

    /**
     * @param getDTLFirstUsages $parameters
     * @return getDTLFirstUsagesResponse
     */
    public function getDTLFirstUsages(getDTLFirstUsages $parameters)
    {
      return $this->__soapCall('getDTLFirstUsages', array($parameters));
    }

    /**
     * @param verifyNamedUser $parameters
     * @return verifyNamedUserResponse
     */
    public function verifyNamedUser(verifyNamedUser $parameters)
    {
      return $this->__soapCall('verifyNamedUser', array($parameters));
    }

    /**
     * @param setPrepaidTcktPlusData $parameters
     * @return setPrepaidTcktPlusDataResponse
     */
    public function setPrepaidTcktPlusData(setPrepaidTcktPlusData $parameters)
    {
      return $this->__soapCall('setPrepaidTcktPlusData', array($parameters));
    }

    /**
     * @param restoreTicketStatus $parameters
     * @return restoreTicketStatusResponse
     */
    public function restoreTicketStatus(restoreTicketStatus $parameters)
    {
      return $this->__soapCall('restoreTicketStatus', array($parameters));
    }

    /**
     * @param getArticleList $parameters
     * @return getArticleListResponse
     */
    public function getArticleList(getArticleList $parameters)
    {
      return $this->__soapCall('getArticleList', array($parameters));
    }

    /**
     * @param getArticleList2 $parameters
     * @return getArticleList2Response
     */
    public function getArticleList2(getArticleList2 $parameters)
    {
      return $this->__soapCall('getArticleList2', array($parameters));
    }

    /**
     * @param getArticleList3 $parameters
     * @return getArticleList3Response
     */
    public function getArticleList3(getArticleList3 $parameters)
    {
      return $this->__soapCall('getArticleList3', array($parameters));
    }

    /**
     * @param getArticleList4 $parameters
     * @return getArticleList4Response
     */
    public function getArticleList4(getArticleList4 $parameters)
    {
      return $this->__soapCall('getArticleList4', array($parameters));
    }

    /**
     * @param getWareList $parameters
     * @return getWareListResponse
     */
    public function getWareList(getWareList $parameters)
    {
      return $this->__soapCall('getWareList', array($parameters));
    }

    /**
     * @param getWareList2 $parameters
     * @return getWareList2Response
     */
    public function getWareList2(getWareList2 $parameters)
    {
      return $this->__soapCall('getWareList2', array($parameters));
    }

    /**
     * @param getWareList3 $parameters
     * @return getWareList3Response
     */
    public function getWareList3(getWareList3 $parameters)
    {
      return $this->__soapCall('getWareList3', array($parameters));
    }

    /**
     * @param getCurrencies $parameters
     * @return getCurrenciesResponse
     */
    public function getCurrencies(getCurrencies $parameters)
    {
      return $this->__soapCall('getCurrencies', array($parameters));
    }

    /**
     * @param getTicketBlockStatus $parameters
     * @return getTicketBlockStatusResponse
     */
    public function getTicketBlockStatus(getTicketBlockStatus $parameters)
    {
      return $this->__soapCall('getTicketBlockStatus', array($parameters));
    }

    /**
     * @param setClientSetup $parameters
     * @return setClientSetupResponse
     */
    public function setClientSetup(setClientSetup $parameters)
    {
      return $this->__soapCall('setClientSetup', array($parameters));
    }

    /**
     * @param getClientSetup $parameters
     * @return getClientSetupResponse
     */
    public function getClientSetup(getClientSetup $parameters)
    {
      return $this->__soapCall('getClientSetup', array($parameters));
    }

    /**
     * @param getTicketRidesAndDrops $parameters
     * @return getTicketRidesAndDropsResponse
     */
    public function getTicketRidesAndDrops(getTicketRidesAndDrops $parameters)
    {
      return $this->__soapCall('getTicketRidesAndDrops', array($parameters));
    }

    /**
     * @param getTicketRidesAndDrops2 $parameters
     * @return getTicketRidesAndDrops2Response
     */
    public function getTicketRidesAndDrops2(getTicketRidesAndDrops2 $parameters)
    {
      return $this->__soapCall('getTicketRidesAndDrops2', array($parameters));
    }

    /**
     * @param ChangeCompany $parameters
     * @return ChangeCompanyResponse
     */
    public function ChangeCompany(ChangeCompany $parameters)
    {
      return $this->__soapCall('ChangeCompany', array($parameters));
    }

    /**
     * @param decodeBarcode $parameters
     * @return decodeBarcodeResponse
     */
    public function decodeBarcode(decodeBarcode $parameters)
    {
      return $this->__soapCall('decodeBarcode', array($parameters));
    }

    /**
     * @param getPromptList $parameters
     * @return getPromptListResponse
     */
    public function getPromptList(getPromptList $parameters)
    {
      return $this->__soapCall('getPromptList', array($parameters));
    }

    /**
     * @param setPromptData $parameters
     * @return setPromptDataResponse
     */
    public function setPromptData(setPromptData $parameters)
    {
      return $this->__soapCall('setPromptData', array($parameters));
    }

    /**
     * @param getPromptData $parameters
     * @return getPromptDataResponse
     */
    public function getPromptData(getPromptData $parameters)
    {
      return $this->__soapCall('getPromptData', array($parameters));
    }

    /**
     * @param setExtInvoiceNo $parameters
     * @return setExtInvoiceNoResponse
     */
    public function setExtInvoiceNo(setExtInvoiceNo $parameters)
    {
      return $this->__soapCall('setExtInvoiceNo', array($parameters));
    }

    /**
     * @param ReleaseGlobalPrepaidTicket $parameters
     * @return ReleaseGlobalPrepaidTicketResponse
     */
    public function ReleaseGlobalPrepaidTicket(ReleaseGlobalPrepaidTicket $parameters)
    {
      return $this->__soapCall('ReleaseGlobalPrepaidTicket', array($parameters));
    }

    /**
     * @param getReservation $parameters
     * @return getReservationResponse
     */
    public function getReservation(getReservation $parameters)
    {
      return $this->__soapCall('getReservation', array($parameters));
    }

    /**
     * @param modifyReservation $parameters
     * @return modifyReservationResponse
     */
    public function modifyReservation(modifyReservation $parameters)
    {
      return $this->__soapCall('modifyReservation', array($parameters));
    }

    /**
     * @param modifyReservation2 $parameters
     * @return modifyReservation2Response
     */
    public function modifyReservation2(modifyReservation2 $parameters)
    {
      return $this->__soapCall('modifyReservation2', array($parameters));
    }

    /**
     * @param addReservation $parameters
     * @return addReservationResponse
     */
    public function addReservation(addReservation $parameters)
    {
      return $this->__soapCall('addReservation', array($parameters));
    }

    /**
     * @param getLNECertificateNumber $parameters
     * @return getLNECertificateNumberResponse
     */
    public function getLNECertificateNumber(getLNECertificateNumber $parameters)
    {
      return $this->__soapCall('getLNECertificateNumber', array($parameters));
    }

    /**
     * @param isNF525TestModeEnabled $parameters
     * @return isNF525TestModeEnabledResponse
     */
    public function isNF525TestModeEnabled(isNF525TestModeEnabled $parameters)
    {
      return $this->__soapCall('isNF525TestModeEnabled', array($parameters));
    }

    /**
     * @param addLicensePlate $parameters
     * @return addLicensePlateResponse
     */
    public function addLicensePlate(addLicensePlate $parameters)
    {
      return $this->__soapCall('addLicensePlate', array($parameters));
    }

    /**
     * @param addAdditionalLicensePlate $parameters
     * @return addAdditionalLicensePlateResponse
     */
    public function addAdditionalLicensePlate(addAdditionalLicensePlate $parameters)
    {
      return $this->__soapCall('addAdditionalLicensePlate', array($parameters));
    }

    /**
     * @param StartCLICSReport $parameters
     * @return StartCLICSReportResponse
     */
    public function StartCLICSReport(StartCLICSReport $parameters)
    {
      return $this->__soapCall('StartCLICSReport', array($parameters));
    }

    /**
     * @param GetCLICSReportStatus $parameters
     * @return GetCLICSReportStatusResponse
     */
    public function GetCLICSReportStatus(GetCLICSReportStatus $parameters)
    {
      return $this->__soapCall('GetCLICSReportStatus', array($parameters));
    }

    /**
     * @param getLicensePlateUsages $parameters
     * @return getLicensePlateUsagesResponse
     */
    public function getLicensePlateUsages(getLicensePlateUsages $parameters)
    {
      return $this->__soapCall('getLicensePlateUsages', array($parameters));
    }

    /**
     * @param bIsLpCheckInOut $parameters
     * @return bIsLpCheckInOutResponse
     */
    public function bIsLpCheckInOut(bIsLpCheckInOut $parameters)
    {
      return $this->__soapCall('bIsLpCheckInOut', array($parameters));
    }

    /**
     * @param setPayer $parameters
     * @return setPayerResponse
     */
    public function setPayer(setPayer $parameters)
    {
      return $this->__soapCall('setPayer', array($parameters));
    }

    /**
     * @param setPayer2 $parameters
     * @return setPayer2Response
     */
    public function setPayer2(setPayer2 $parameters)
    {
      return $this->__soapCall('setPayer2', array($parameters));
    }

    /**
     * @param manageShoppingCart $parameters
     * @return manageShoppingCartResponse
     */
    public function manageShoppingCart(manageShoppingCart $parameters)
    {
      return $this->__soapCall('manageShoppingCart', array($parameters));
    }

    /**
     * @param manageShoppingCart2 $parameters
     * @return manageShoppingCart2Response
     */
    public function manageShoppingCart2(manageShoppingCart2 $parameters)
    {
      return $this->__soapCall('manageShoppingCart2', array($parameters));
    }

    /**
     * @param manageShoppingCart3 $parameters
     * @return manageShoppingCart3Response
     */
    public function manageShoppingCart3(manageShoppingCart3 $parameters)
    {
      return $this->__soapCall('manageShoppingCart3', array($parameters));
    }

    /**
     * @param manageDownPayment $parameters
     * @return manageDownPaymentResponse
     */
    public function manageDownPayment(manageDownPayment $parameters)
    {
      return $this->__soapCall('manageDownPayment', array($parameters));
    }

    /**
     * @param manageTravelGroup $parameters
     * @return manageTravelGroupResponse
     */
    public function manageTravelGroup(manageTravelGroup $parameters)
    {
      return $this->__soapCall('manageTravelGroup', array($parameters));
    }

    /**
     * @param setShoppingCartStatus $parameters
     * @return setShoppingCartStatusResponse
     */
    public function setShoppingCartStatus(setShoppingCartStatus $parameters)
    {
      return $this->__soapCall('setShoppingCartStatus', array($parameters));
    }

    /**
     * @param setTravelGroupStatus $parameters
     * @return setTravelGroupStatusResponse
     */
    public function setTravelGroupStatus(setTravelGroupStatus $parameters)
    {
      return $this->__soapCall('setTravelGroupStatus', array($parameters));
    }

    /**
     * @param getShoppingCartData $parameters
     * @return getShoppingCartDataResponse
     */
    public function getShoppingCartData(getShoppingCartData $parameters)
    {
      return $this->__soapCall('getShoppingCartData', array($parameters));
    }

    /**
     * @param getShoppingCartData2 $parameters
     * @return getShoppingCartData2Response
     */
    public function getShoppingCartData2(getShoppingCartData2 $parameters)
    {
      return $this->__soapCall('getShoppingCartData2', array($parameters));
    }

    /**
     * @param getShoppingCartData3 $parameters
     * @return getShoppingCartData3Response
     */
    public function getShoppingCartData3(getShoppingCartData3 $parameters)
    {
      return $this->__soapCall('getShoppingCartData3', array($parameters));
    }

    /**
     * @param getShoppingCartData4 $parameters
     * @return getShoppingCartData4Response
     */
    public function getShoppingCartData4(getShoppingCartData4 $parameters)
    {
      return $this->__soapCall('getShoppingCartData4', array($parameters));
    }

    /**
     * @param getShoppingCartData5 $parameters
     * @return getShoppingCartData5Response
     */
    public function getShoppingCartData5(getShoppingCartData5 $parameters)
    {
      return $this->__soapCall('getShoppingCartData5', array($parameters));
    }

    /**
     * @param getTravelGroupData $parameters
     * @return getTravelGroupDataResponse
     */
    public function getTravelGroupData(getTravelGroupData $parameters)
    {
      return $this->__soapCall('getTravelGroupData', array($parameters));
    }

    /**
     * @param getTravelGroupData2 $parameters
     * @return getTravelGroupData2Response
     */
    public function getTravelGroupData2(getTravelGroupData2 $parameters)
    {
      return $this->__soapCall('getTravelGroupData2', array($parameters));
    }

    /**
     * @param getTravelGrpShopBasPositions $parameters
     * @return getTravelGrpShopBasPositionsResponse
     */
    public function getTravelGrpShopBasPositions(getTravelGrpShopBasPositions $parameters)
    {
      return $this->__soapCall('getTravelGrpShopBasPositions', array($parameters));
    }

    /**
     * @param getTravelGrpShopBasPositions2 $parameters
     * @return getTravelGrpShopBasPositions2Response
     */
    public function getTravelGrpShopBasPositions2(getTravelGrpShopBasPositions2 $parameters)
    {
      return $this->__soapCall('getTravelGrpShopBasPositions2', array($parameters));
    }

    /**
     * @param produceShoppingBasketPrepaid $parameters
     * @return produceShoppingBasketPrepaidResponse
     */
    public function produceShoppingBasketPrepaid(produceShoppingBasketPrepaid $parameters)
    {
      return $this->__soapCall('produceShoppingBasketPrepaid', array($parameters));
    }

    /**
     * @param produceShoppingBasketPrepaid2 $parameters
     * @return produceShoppingBasketPrepaid2Response
     */
    public function produceShoppingBasketPrepaid2(produceShoppingBasketPrepaid2 $parameters)
    {
      return $this->__soapCall('produceShoppingBasketPrepaid2', array($parameters));
    }

    /**
     * @param getCompanyPaymentTypeExcl $parameters
     * @return getCompanyPaymentTypeExclResponse
     */
    public function getCompanyPaymentTypeExcl(getCompanyPaymentTypeExcl $parameters)
    {
      return $this->__soapCall('getCompanyPaymentTypeExcl', array($parameters));
    }

    /**
     * @param storeTempTXT $parameters
     * @return storeTempTXTResponse
     */
    public function storeTempTXT(storeTempTXT $parameters)
    {
      return $this->__soapCall('storeTempTXT', array($parameters));
    }

    /**
     * @param getOpenCashierShifts $parameters
     * @return getOpenCashierShiftsResponse
     */
    public function getOpenCashierShifts(getOpenCashierShifts $parameters)
    {
      return $this->__soapCall('getOpenCashierShifts', array($parameters));
    }

    /**
     * @param openCashierShift $parameters
     * @return openCashierShiftResponse
     */
    public function openCashierShift(openCashierShift $parameters)
    {
      return $this->__soapCall('openCashierShift', array($parameters));
    }

    /**
     * @param closeCashierShift $parameters
     * @return closeCashierShiftResponse
     */
    public function closeCashierShift(closeCashierShift $parameters)
    {
      return $this->__soapCall('closeCashierShift', array($parameters));
    }

    /**
     * @param getCashierRights $parameters
     * @return getCashierRightsResponse
     */
    public function getCashierRights(getCashierRights $parameters)
    {
      return $this->__soapCall('getCashierRights', array($parameters));
    }

    /**
     * @param getGroupReservation $parameters
     * @return getGroupReservationResponse
     */
    public function getGroupReservation(getGroupReservation $parameters)
    {
      return $this->__soapCall('getGroupReservation', array($parameters));
    }

    /**
     * @param getGroupContingent $parameters
     * @return getGroupContingentResponse
     */
    public function getGroupContingent(getGroupContingent $parameters)
    {
      return $this->__soapCall('getGroupContingent', array($parameters));
    }

    /**
     * @param getGroupContingent2 $parameters
     * @return getGroupContingent2Response
     */
    public function getGroupContingent2(getGroupContingent2 $parameters)
    {
      return $this->__soapCall('getGroupContingent2', array($parameters));
    }

    /**
     * @param lockReservation $parameters
     * @return lockReservationResponse
     */
    public function lockReservation(lockReservation $parameters)
    {
      return $this->__soapCall('lockReservation', array($parameters));
    }

    /**
     * @param getNF525ReceiptNo $parameters
     * @return getNF525ReceiptNoResponse
     */
    public function getNF525ReceiptNo(getNF525ReceiptNo $parameters)
    {
      return $this->__soapCall('getNF525ReceiptNo', array($parameters));
    }

    /**
     * @param getReceiptData $parameters
     * @return getReceiptDataResponse
     */
    public function getReceiptData(getReceiptData $parameters)
    {
      return $this->__soapCall('getReceiptData', array($parameters));
    }

    /**
     * @param getReceiptData2 $parameters
     * @return getReceiptData2Response
     */
    public function getReceiptData2(getReceiptData2 $parameters)
    {
      return $this->__soapCall('getReceiptData2', array($parameters));
    }

    /**
     * @param getARCubes $parameters
     * @return getARCubesResponse
     */
    public function getARCubes(getARCubes $parameters)
    {
      return $this->__soapCall('getARCubes', array($parameters));
    }

    /**
     * @param setARCube $parameters
     * @return setARCubeResponse
     */
    public function setARCube(setARCube $parameters)
    {
      return $this->__soapCall('setARCube', array($parameters));
    }

    /**
     * @param getWTPRentalItems $parameters
     * @return getWTPRentalItemsResponse
     */
    public function getWTPRentalItems(getWTPRentalItems $parameters)
    {
      return $this->__soapCall('getWTPRentalItems', array($parameters));
    }

    /**
     * @param getWTPRentalItems2 $parameters
     * @return getWTPRentalItems2Response
     */
    public function getWTPRentalItems2(getWTPRentalItems2 $parameters)
    {
      return $this->__soapCall('getWTPRentalItems2', array($parameters));
    }

    /**
     * @param getWTPRentalPersTypes $parameters
     * @return getWTPRentalPersTypesResponse
     */
    public function getWTPRentalPersTypes(getWTPRentalPersTypes $parameters)
    {
      return $this->__soapCall('getWTPRentalPersTypes', array($parameters));
    }

    /**
     * @param getARCData $parameters
     * @return getARCDataResponse
     */
    public function getARCData(getARCData $parameters)
    {
      return $this->__soapCall('getARCData', array($parameters));
    }

    /**
     * @param getRoutesStations $parameters
     * @return getRoutesStationsResponse
     */
    public function getRoutesStations(getRoutesStations $parameters)
    {
      return $this->__soapCall('getRoutesStations', array($parameters));
    }

    /**
     * @param getCompany $parameters
     * @return getCompanyResponse
     */
    public function getCompany(getCompany $parameters)
    {
      return $this->__soapCall('getCompany', array($parameters));
    }

    /**
     * @param getCompany2 $parameters
     * @return getCompany2Response
     */
    public function getCompany2(getCompany2 $parameters)
    {
      return $this->__soapCall('getCompany2', array($parameters));
    }

    /**
     * @param getCompany3 $parameters
     * @return getCompany3Response
     */
    public function getCompany3(getCompany3 $parameters)
    {
      return $this->__soapCall('getCompany3', array($parameters));
    }

    /**
     * @param getCash4CashierShift $parameters
     * @return getCash4CashierShiftResponse
     */
    public function getCash4CashierShift(getCash4CashierShift $parameters)
    {
      return $this->__soapCall('getCash4CashierShift', array($parameters));
    }

    /**
     * @param moveCash4CashierShift $parameters
     * @return moveCash4CashierShiftResponse
     */
    public function moveCash4CashierShift(moveCash4CashierShift $parameters)
    {
      return $this->__soapCall('moveCash4CashierShift', array($parameters));
    }

    /**
     * @param redeemPromoCodes $parameters
     * @return redeemPromoCodesResponse
     */
    public function redeemPromoCodes(redeemPromoCodes $parameters)
    {
      return $this->__soapCall('redeemPromoCodes', array($parameters));
    }

    /**
     * @param getACPXImage $parameters
     * @return getACPXImageResponse
     */
    public function getACPXImage(getACPXImage $parameters)
    {
      return $this->__soapCall('getACPXImage', array($parameters));
    }

    /**
     * @param getACPXImageWithImage $parameters
     * @return getACPXImageWithImageResponse
     */
    public function getACPXImageWithImage(getACPXImageWithImage $parameters)
    {
      return $this->__soapCall('getACPXImageWithImage', array($parameters));
    }

    /**
     * @param createBookingReservation $parameters
     * @return createBookingReservationResponse
     */
    public function createBookingReservation(createBookingReservation $parameters)
    {
      return $this->__soapCall('createBookingReservation', array($parameters));
    }

    /**
     * @param createBooking $parameters
     * @return createBookingResponse
     */
    public function createBooking(createBooking $parameters)
    {
      return $this->__soapCall('createBooking', array($parameters));
    }

    /**
     * @param cancelBookingReservation $parameters
     * @return cancelBookingReservationResponse
     */
    public function cancelBookingReservation(cancelBookingReservation $parameters)
    {
      return $this->__soapCall('cancelBookingReservation', array($parameters));
    }

    /**
     * @param getBookingLimits $parameters
     * @return getBookingLimitsResponse
     */
    public function getBookingLimits(getBookingLimits $parameters)
    {
      return $this->__soapCall('getBookingLimits', array($parameters));
    }

    /**
     * @param getDataForTicket $parameters
     * @return getDataForTicketResponse
     */
    public function getDataForTicket(getDataForTicket $parameters)
    {
      return $this->__soapCall('getDataForTicket', array($parameters));
    }

    /**
     * @param createPOEBooking $parameters
     * @return createPOEBookingResponse
     */
    public function createPOEBooking(createPOEBooking $parameters)
    {
      return $this->__soapCall('createPOEBooking', array($parameters));
    }

    /**
     * @param cancelPOEBooking $parameters
     * @return cancelPOEBookingResponse
     */
    public function cancelPOEBooking(cancelPOEBooking $parameters)
    {
      return $this->__soapCall('cancelPOEBooking', array($parameters));
    }

    /**
     * @param createPOEBookingWithCertificate $parameters
     * @return createPOEBookingWithCertificateResponse
     */
    public function createPOEBookingWithCertificate(createPOEBookingWithCertificate $parameters)
    {
      return $this->__soapCall('createPOEBookingWithCertificate', array($parameters));
    }

    /**
     * @param createPOEBookingWithManualCertificate $parameters
     * @return createPOEBookingWithManualCertificateResponse
     */
    public function createPOEBookingWithManualCertificate(createPOEBookingWithManualCertificate $parameters)
    {
      return $this->__soapCall('createPOEBookingWithManualCertificate', array($parameters));
    }

    /**
     * @param subscribeForLimitCheck $parameters
     * @return subscribeForLimitCheckResponse
     */
    public function subscribeForLimitCheck(subscribeForLimitCheck $parameters)
    {
      return $this->__soapCall('subscribeForLimitCheck', array($parameters));
    }

    /**
     * @param subscribeForTariffCheck $parameters
     * @return subscribeForTariffCheckResponse
     */
    public function subscribeForTariffCheck(subscribeForTariffCheck $parameters)
    {
      return $this->__soapCall('subscribeForTariffCheck', array($parameters));
    }

    /**
     * @param getServerVersion $parameters
     * @return getServerVersionResponse
     */
    public function getServerVersion(getServerVersion $parameters)
    {
      return $this->__soapCall('getServerVersion', array($parameters));
    }

    /**
     * @param getModulVersion $parameters
     * @return getModulVersionResponse
     */
    public function getModulVersion(getModulVersion $parameters)
    {
      return $this->__soapCall('getModulVersion', array($parameters));
    }

    /**
     * @param login $parameters
     * @return loginResponse
     */
    public function login(login $parameters)
    {
      return $this->__soapCall('login', array($parameters));
    }

    /**
     * @param login2 $parameters
     * @return login2Response
     */
    public function login2(login2 $parameters)
    {
      return $this->__soapCall('login2', array($parameters));
    }

    /**
     * @param loginCashier $parameters
     * @return loginCashierResponse
     */
    public function loginCashier(loginCashier $parameters)
    {
      return $this->__soapCall('loginCashier', array($parameters));
    }

    /**
     * @param loginNamedUser $parameters
     * @return loginNamedUserResponse
     */
    public function loginNamedUser(loginNamedUser $parameters)
    {
      return $this->__soapCall('loginNamedUser', array($parameters));
    }

    /**
     * @param logout $parameters
     * @return logoutResponse
     */
    public function logout(logout $parameters)
    {
      return $this->__soapCall('logout', array($parameters));
    }

    /**
     * @param setCountryCode $parameters
     * @return setCountryCodeResponse
     */
    public function setCountryCode(setCountryCode $parameters)
    {
      return $this->__soapCall('setCountryCode', array($parameters));
    }

    /**
     * @param checkSession $parameters
     * @return checkSessionResponse
     */
    public function checkSession(checkSession $parameters)
    {
      return $this->__soapCall('checkSession', array($parameters));
    }

    /**
     * @param getWTPConfiguration $parameters
     * @return getWTPConfigurationResponse
     */
    public function getWTPConfiguration(getWTPConfiguration $parameters)
    {
      return $this->__soapCall('getWTPConfiguration', array($parameters));
    }

    /**
     * @param forgotPassword $parameters
     * @return forgotPasswordResponse
     */
    public function forgotPassword(forgotPassword $parameters)
    {
      return $this->__soapCall('forgotPassword', array($parameters));
    }

    /**
     * @param getCustomerProfile $parameters
     * @return getCustomerProfileResponse
     */
    public function getCustomerProfile(getCustomerProfile $parameters)
    {
      return $this->__soapCall('getCustomerProfile', array($parameters));
    }

    /**
     * @param getCustomerProfile2 $parameters
     * @return getCustomerProfile2Response
     */
    public function getCustomerProfile2(getCustomerProfile2 $parameters)
    {
      return $this->__soapCall('getCustomerProfile2', array($parameters));
    }

    /**
     * @param getCustomerProfile3 $parameters
     * @return getCustomerProfile3Response
     */
    public function getCustomerProfile3(getCustomerProfile3 $parameters)
    {
      return $this->__soapCall('getCustomerProfile3', array($parameters));
    }

    /**
     * @param getB2CAccounts $parameters
     * @return getB2CAccountsResponse
     */
    public function getB2CAccounts(getB2CAccounts $parameters)
    {
      return $this->__soapCall('getB2CAccounts', array($parameters));
    }

    /**
     * @param setB2CAccount $parameters
     * @return setB2CAccountResponse
     */
    public function setB2CAccount(setB2CAccount $parameters)
    {
      return $this->__soapCall('setB2CAccount', array($parameters));
    }

    /**
     * @param changePassword $parameters
     * @return changePasswordResponse
     */
    public function changePassword(changePassword $parameters)
    {
      return $this->__soapCall('changePassword', array($parameters));
    }

    /**
     * @param getCompanyInfo $parameters
     * @return getCompanyInfoResponse
     */
    public function getCompanyInfo(getCompanyInfo $parameters)
    {
      return $this->__soapCall('getCompanyInfo', array($parameters));
    }

    /**
     * @param getCompanyInfo2 $parameters
     * @return getCompanyInfo2Response
     */
    public function getCompanyInfo2(getCompanyInfo2 $parameters)
    {
      return $this->__soapCall('getCompanyInfo2', array($parameters));
    }

    /**
     * @param getPools $parameters
     * @return getPoolsResponse
     */
    public function getPools(getPools $parameters)
    {
      return $this->__soapCall('getPools', array($parameters));
    }

    /**
     * @param getPools2 $parameters
     * @return getPools2Response
     */
    public function getPools2(getPools2 $parameters)
    {
      return $this->__soapCall('getPools2', array($parameters));
    }

    /**
     * @param getTicketTypes $parameters
     * @return getTicketTypesResponse
     */
    public function getTicketTypes(getTicketTypes $parameters)
    {
      return $this->__soapCall('getTicketTypes', array($parameters));
    }

    /**
     * @param getTicketTypes2 $parameters
     * @return getTicketTypes2Response
     */
    public function getTicketTypes2(getTicketTypes2 $parameters)
    {
      return $this->__soapCall('getTicketTypes2', array($parameters));
    }

    /**
     * @param getTicketTypes3 $parameters
     * @return getTicketTypes3Response
     */
    public function getTicketTypes3(getTicketTypes3 $parameters)
    {
      return $this->__soapCall('getTicketTypes3', array($parameters));
    }

    /**
     * @param getTicketTypes4 $parameters
     * @return getTicketTypes4Response
     */
    public function getTicketTypes4(getTicketTypes4 $parameters)
    {
      return $this->__soapCall('getTicketTypes4', array($parameters));
    }

    /**
     * @param getWTPGroups $parameters
     * @return getWTPGroupsResponse
     */
    public function getWTPGroups(getWTPGroups $parameters)
    {
      return $this->__soapCall('getWTPGroups', array($parameters));
    }

    /**
     * @param getWTPGroups2 $parameters
     * @return getWTPGroups2Response
     */
    public function getWTPGroups2(getWTPGroups2 $parameters)
    {
      return $this->__soapCall('getWTPGroups2', array($parameters));
    }

    /**
     * @param getPersonTypes $parameters
     * @return getPersonTypesResponse
     */
    public function getPersonTypes(getPersonTypes $parameters)
    {
      return $this->__soapCall('getPersonTypes', array($parameters));
    }

    /**
     * @param getPersonTypes2 $parameters
     * @return getPersonTypes2Response
     */
    public function getPersonTypes2(getPersonTypes2 $parameters)
    {
      return $this->__soapCall('getPersonTypes2', array($parameters));
    }

    /**
     * @param getPersonTypes3 $parameters
     * @return getPersonTypes3Response
     */
    public function getPersonTypes3(getPersonTypes3 $parameters)
    {
      return $this->__soapCall('getPersonTypes3', array($parameters));
    }

    /**
     * @param getPersTypeRules $parameters
     * @return getPersTypeRulesResponse
     */
    public function getPersTypeRules(getPersTypeRules $parameters)
    {
      return $this->__soapCall('getPersTypeRules', array($parameters));
    }

    /**
     * @param getAdditionalArticles $parameters
     * @return getAdditionalArticlesResponse
     */
    public function getAdditionalArticles(getAdditionalArticles $parameters)
    {
      return $this->__soapCall('getAdditionalArticles', array($parameters));
    }

    /**
     * @param getPackages $parameters
     * @return getPackagesResponse
     */
    public function getPackages(getPackages $parameters)
    {
      return $this->__soapCall('getPackages', array($parameters));
    }

    /**
     * @param getPackages2 $parameters
     * @return getPackages2Response
     */
    public function getPackages2(getPackages2 $parameters)
    {
      return $this->__soapCall('getPackages2', array($parameters));
    }

    /**
     * @param getPackages3 $parameters
     * @return getPackages3Response
     */
    public function getPackages3(getPackages3 $parameters)
    {
      return $this->__soapCall('getPackages3', array($parameters));
    }

    /**
     * @param getPackageContent $parameters
     * @return getPackageContentResponse
     */
    public function getPackageContent(getPackageContent $parameters)
    {
      return $this->__soapCall('getPackageContent', array($parameters));
    }

    /**
     * @param getPackageContent2 $parameters
     * @return getPackageContent2Response
     */
    public function getPackageContent2(getPackageContent2 $parameters)
    {
      return $this->__soapCall('getPackageContent2', array($parameters));
    }

    /**
     * @param getPackageContent3 $parameters
     * @return getPackageContent3Response
     */
    public function getPackageContent3(getPackageContent3 $parameters)
    {
      return $this->__soapCall('getPackageContent3', array($parameters));
    }

    /**
     * @param getPackageContent4 $parameters
     * @return getPackageContent4Response
     */
    public function getPackageContent4(getPackageContent4 $parameters)
    {
      return $this->__soapCall('getPackageContent4', array($parameters));
    }

    /**
     * @param getPackageTariffList $parameters
     * @return getPackageTariffListResponse
     */
    public function getPackageTariffList(getPackageTariffList $parameters)
    {
      return $this->__soapCall('getPackageTariffList', array($parameters));
    }

    /**
     * @param getPackageTariffList2 $parameters
     * @return getPackageTariffList2Response
     */
    public function getPackageTariffList2(getPackageTariffList2 $parameters)
    {
      return $this->__soapCall('getPackageTariffList2', array($parameters));
    }

    /**
     * @param getPackageTariffList3 $parameters
     * @return getPackageTariffList3Response
     */
    public function getPackageTariffList3(getPackageTariffList3 $parameters)
    {
      return $this->__soapCall('getPackageTariffList3', array($parameters));
    }

    /**
     * @param getPackageTariffList4 $parameters
     * @return getPackageTariffList4Response
     */
    public function getPackageTariffList4(getPackageTariffList4 $parameters)
    {
      return $this->__soapCall('getPackageTariffList4', array($parameters));
    }

    /**
     * @param getPackageTariffList5 $parameters
     * @return getPackageTariffList5Response
     */
    public function getPackageTariffList5(getPackageTariffList5 $parameters)
    {
      return $this->__soapCall('getPackageTariffList5', array($parameters));
    }

    /**
     * @param getPackageTariffList6 $parameters
     * @return getPackageTariffList6Response
     */
    public function getPackageTariffList6(getPackageTariffList6 $parameters)
    {
      return $this->__soapCall('getPackageTariffList6', array($parameters));
    }

    /**
     * @param getPackageTariffList7 $parameters
     * @return getPackageTariffList7Response
     */
    public function getPackageTariffList7(getPackageTariffList7 $parameters)
    {
      return $this->__soapCall('getPackageTariffList7', array($parameters));
    }

    /**
     * @param getPackageTariffList8 $parameters
     * @return getPackageTariffList8Response
     */
    public function getPackageTariffList8(getPackageTariffList8 $parameters)
    {
      return $this->__soapCall('getPackageTariffList8', array($parameters));
    }

    /**
     * @param getPackageTariffList9 $parameters
     * @return getPackageTariffList9Response
     */
    public function getPackageTariffList9(getPackageTariffList9 $parameters)
    {
      return $this->__soapCall('getPackageTariffList9', array($parameters));
    }

    /**
     * @param getPackagePosTarif $parameters
     * @return getPackagePosTarifResponse
     */
    public function getPackagePosTarif(getPackagePosTarif $parameters)
    {
      return $this->__soapCall('getPackagePosTarif', array($parameters));
    }

    /**
     * @param getProducts $parameters
     * @return getProductsResponse
     */
    public function getProducts(getProducts $parameters)
    {
      return $this->__soapCall('getProducts', array($parameters));
    }

    /**
     * @param getTax $parameters
     * @return getTaxResponse
     */
    public function getTax(getTax $parameters)
    {
      return $this->__soapCall('getTax', array($parameters));
    }

    /**
     * @param getTariff $parameters
     * @return getTariffResponse
     */
    public function getTariff(getTariff $parameters)
    {
      return $this->__soapCall('getTariff', array($parameters));
    }

    /**
     * @param getTariff2 $parameters
     * @return getTariff2Response
     */
    public function getTariff2(getTariff2 $parameters)
    {
      return $this->__soapCall('getTariff2', array($parameters));
    }

    /**
     * @param getTariffList $parameters
     * @return getTariffListResponse
     */
    public function getTariffList(getTariffList $parameters)
    {
      return $this->__soapCall('getTariffList', array($parameters));
    }

    /**
     * @param getTariffList2 $parameters
     * @return getTariffList2Response
     */
    public function getTariffList2(getTariffList2 $parameters)
    {
      return $this->__soapCall('getTariffList2', array($parameters));
    }

    /**
     * @param getTariffList3 $parameters
     * @return getTariffList3Response
     */
    public function getTariffList3(getTariffList3 $parameters)
    {
      return $this->__soapCall('getTariffList3', array($parameters));
    }

    /**
     * @param getTariffList4 $parameters
     * @return getTariffList4Response
     */
    public function getTariffList4(getTariffList4 $parameters)
    {
      return $this->__soapCall('getTariffList4', array($parameters));
    }

    /**
     * @param getTariffList5 $parameters
     * @return getTariffList5Response
     */
    public function getTariffList5(getTariffList5 $parameters)
    {
      return $this->__soapCall('getTariffList5', array($parameters));
    }

    /**
     * @param getTariffList6 $parameters
     * @return getTariffList6Response
     */
    public function getTariffList6(getTariffList6 $parameters)
    {
      return $this->__soapCall('getTariffList6', array($parameters));
    }

    /**
     * @param getTariffList7 $parameters
     * @return getTariffList7Response
     */
    public function getTariffList7(getTariffList7 $parameters)
    {
      return $this->__soapCall('getTariffList7', array($parameters));
    }

    /**
     * @param getTariffList8 $parameters
     * @return getTariffList8Response
     */
    public function getTariffList8(getTariffList8 $parameters)
    {
      return $this->__soapCall('getTariffList8', array($parameters));
    }

    /**
     * @param getTariffList9 $parameters
     * @return getTariffList9Response
     */
    public function getTariffList9(getTariffList9 $parameters)
    {
      return $this->__soapCall('getTariffList9', array($parameters));
    }

    /**
     * @param getPackageTariffs $parameters
     * @return getPackageTariffsResponse
     */
    public function getPackageTariffs(getPackageTariffs $parameters)
    {
      return $this->__soapCall('getPackageTariffs', array($parameters));
    }

    /**
     * @param issueTicket $parameters
     * @return issueTicketResponse
     */
    public function issueTicket(issueTicket $parameters)
    {
      return $this->__soapCall('issueTicket', array($parameters));
    }

    /**
     * @param issueTicket2 $parameters
     * @return issueTicket2Response
     */
    public function issueTicket2(issueTicket2 $parameters)
    {
      return $this->__soapCall('issueTicket2', array($parameters));
    }

    /**
     * @param issueTicket3 $parameters
     * @return issueTicket3Response
     */
    public function issueTicket3(issueTicket3 $parameters)
    {
      return $this->__soapCall('issueTicket3', array($parameters));
    }

    /**
     * @param issueTicket4 $parameters
     * @return issueTicket4Response
     */
    public function issueTicket4(issueTicket4 $parameters)
    {
      return $this->__soapCall('issueTicket4', array($parameters));
    }

    /**
     * @param issueTicket5 $parameters
     * @return issueTicket5Response
     */
    public function issueTicket5(issueTicket5 $parameters)
    {
      return $this->__soapCall('issueTicket5', array($parameters));
    }

    /**
     * @param issueTicket6 $parameters
     * @return issueTicket6Response
     */
    public function issueTicket6(issueTicket6 $parameters)
    {
      return $this->__soapCall('issueTicket6', array($parameters));
    }

    /**
     * @param issueTicket7 $parameters
     * @return issueTicket7Response
     */
    public function issueTicket7(issueTicket7 $parameters)
    {
      return $this->__soapCall('issueTicket7', array($parameters));
    }

    /**
     * @param issueTicket8 $parameters
     * @return issueTicket8Response
     */
    public function issueTicket8(issueTicket8 $parameters)
    {
      return $this->__soapCall('issueTicket8', array($parameters));
    }

    /**
     * @param issueTicket9 $parameters
     * @return issueTicket9Response
     */
    public function issueTicket9(issueTicket9 $parameters)
    {
      return $this->__soapCall('issueTicket9', array($parameters));
    }

    /**
     * @param issueTicket10 $parameters
     * @return issueTicket10Response
     */
    public function issueTicket10(issueTicket10 $parameters)
    {
      return $this->__soapCall('issueTicket10', array($parameters));
    }

    /**
     * @param issueTicket11 $parameters
     * @return issueTicket11Response
     */
    public function issueTicket11(issueTicket11 $parameters)
    {
      return $this->__soapCall('issueTicket11', array($parameters));
    }

    /**
     * @param issueTicket12 $parameters
     * @return issueTicket12Response
     */
    public function issueTicket12(issueTicket12 $parameters)
    {
      return $this->__soapCall('issueTicket12', array($parameters));
    }

    /**
     * @param issueTicket13 $parameters
     * @return issueTicket13Response
     */
    public function issueTicket13(issueTicket13 $parameters)
    {
      return $this->__soapCall('issueTicket13', array($parameters));
    }

    /**
     * @param issueTicketModifyDate $parameters
     * @return issueTicketModifyDateResponse
     */
    public function issueTicketModifyDate(issueTicketModifyDate $parameters)
    {
      return $this->__soapCall('issueTicketModifyDate', array($parameters));
    }

    /**
     * @param msgIssueTicket $parameters
     * @return msgIssueTicketResponse
     */
    public function msgIssueTicket(msgIssueTicket $parameters)
    {
      return $this->__soapCall('msgIssueTicket', array($parameters));
    }

    /**
     * @param msgIssueTicket2 $parameters
     * @return msgIssueTicket2Response
     */
    public function msgIssueTicket2(msgIssueTicket2 $parameters)
    {
      return $this->__soapCall('msgIssueTicket2', array($parameters));
    }

    /**
     * @param msgIssueTicket3 $parameters
     * @return msgIssueTicket3Response
     */
    public function msgIssueTicket3(msgIssueTicket3 $parameters)
    {
      return $this->__soapCall('msgIssueTicket3', array($parameters));
    }

    /**
     * @param msgIssueTicket4 $parameters
     * @return msgIssueTicket4Response
     */
    public function msgIssueTicket4(msgIssueTicket4 $parameters)
    {
      return $this->__soapCall('msgIssueTicket4', array($parameters));
    }

    /**
     * @param msgIssueTicket5 $parameters
     * @return msgIssueTicket5Response
     */
    public function msgIssueTicket5(msgIssueTicket5 $parameters)
    {
      return $this->__soapCall('msgIssueTicket5', array($parameters));
    }

    /**
     * @param msgIssueTicket6 $parameters
     * @return msgIssueTicket6Response
     */
    public function msgIssueTicket6(msgIssueTicket6 $parameters)
    {
      return $this->__soapCall('msgIssueTicket6', array($parameters));
    }

    /**
     * @param msgIssueTicket7 $parameters
     * @return msgIssueTicket7Response
     */
    public function msgIssueTicket7(msgIssueTicket7 $parameters)
    {
      return $this->__soapCall('msgIssueTicket7', array($parameters));
    }

    /**
     * @param msgIssueTicket8 $parameters
     * @return msgIssueTicket8Response
     */
    public function msgIssueTicket8(msgIssueTicket8 $parameters)
    {
      return $this->__soapCall('msgIssueTicket8', array($parameters));
    }

    /**
     * @param issuePackagePos $parameters
     * @return issuePackagePosResponse
     */
    public function issuePackagePos(issuePackagePos $parameters)
    {
      return $this->__soapCall('issuePackagePos', array($parameters));
    }

    /**
     * @param check4Rebook $parameters
     * @return check4RebookResponse
     */
    public function check4Rebook(check4Rebook $parameters)
    {
      return $this->__soapCall('check4Rebook', array($parameters));
    }

    /**
     * @param rebook $parameters
     * @return rebookResponse
     */
    public function rebook(rebook $parameters)
    {
      return $this->__soapCall('rebook', array($parameters));
    }

    /**
     * @param rebook2 $parameters
     * @return rebook2Response
     */
    public function rebook2(rebook2 $parameters)
    {
      return $this->__soapCall('rebook2', array($parameters));
    }

    /**
     * @param rebookTransaction $parameters
     * @return rebookTransactionResponse
     */
    public function rebookTransaction(rebookTransaction $parameters)
    {
      return $this->__soapCall('rebookTransaction', array($parameters));
    }

    /**
     * @param DoSplitPayment $parameters
     * @return DoSplitPaymentResponse
     */
    public function DoSplitPayment(DoSplitPayment $parameters)
    {
      return $this->__soapCall('DoSplitPayment', array($parameters));
    }

    /**
     * @param reloadTicket $parameters
     * @return reloadTicketResponse
     */
    public function reloadTicket(reloadTicket $parameters)
    {
      return $this->__soapCall('reloadTicket', array($parameters));
    }

    /**
     * @param cancelTicket2 $parameters
     * @return cancelTicket2Response
     */
    public function cancelTicket2(cancelTicket2 $parameters)
    {
      return $this->__soapCall('cancelTicket2', array($parameters));
    }

    /**
     * @param cancelTicket $parameters
     * @return cancelTicketResponse
     */
    public function cancelTicket(cancelTicket $parameters)
    {
      return $this->__soapCall('cancelTicket', array($parameters));
    }

    /**
     * @param cancelTicket3 $parameters
     * @return cancelTicket3Response
     */
    public function cancelTicket3(cancelTicket3 $parameters)
    {
      return $this->__soapCall('cancelTicket3', array($parameters));
    }

    /**
     * @param cancelTicket4 $parameters
     * @return cancelTicket4Response
     */
    public function cancelTicket4(cancelTicket4 $parameters)
    {
      return $this->__soapCall('cancelTicket4', array($parameters));
    }

    /**
     * @param cancelTicket5 $parameters
     * @return cancelTicket5Response
     */
    public function cancelTicket5(cancelTicket5 $parameters)
    {
      return $this->__soapCall('cancelTicket5', array($parameters));
    }

    /**
     * @param cancelTicket6 $parameters
     * @return cancelTicket6Response
     */
    public function cancelTicket6(cancelTicket6 $parameters)
    {
      return $this->__soapCall('cancelTicket6', array($parameters));
    }

    /**
     * @param msgCancelTicket $parameters
     * @return msgCancelTicketResponse
     */
    public function msgCancelTicket(msgCancelTicket $parameters)
    {
      return $this->__soapCall('msgCancelTicket', array($parameters));
    }

    /**
     * @param getTicketSalesData $parameters
     * @return getTicketSalesDataResponse
     */
    public function getTicketSalesData(getTicketSalesData $parameters)
    {
      return $this->__soapCall('getTicketSalesData', array($parameters));
    }

    /**
     * @param getTicketSalesData2 $parameters
     * @return getTicketSalesData2Response
     */
    public function getTicketSalesData2(getTicketSalesData2 $parameters)
    {
      return $this->__soapCall('getTicketSalesData2', array($parameters));
    }

    /**
     * @param getTicketSalesData3 $parameters
     * @return getTicketSalesData3Response
     */
    public function getTicketSalesData3(getTicketSalesData3 $parameters)
    {
      return $this->__soapCall('getTicketSalesData3', array($parameters));
    }

    /**
     * @param getTicketSalesData4 $parameters
     * @return getTicketSalesData4Response
     */
    public function getTicketSalesData4(getTicketSalesData4 $parameters)
    {
      return $this->__soapCall('getTicketSalesData4', array($parameters));
    }

    /**
     * @param createReport $parameters
     * @return createReportResponse
     */
    public function createReport(createReport $parameters)
    {
      return $this->__soapCall('createReport', array($parameters));
    }

    /**
     * @param getTicketData $parameters
     * @return getTicketDataResponse
     */
    public function getTicketData(getTicketData $parameters)
    {
      return $this->__soapCall('getTicketData', array($parameters));
    }

    /**
     * @param checkWTPNo $parameters
     * @return checkWTPNoResponse
     */
    public function checkWTPNo(checkWTPNo $parameters)
    {
      return $this->__soapCall('checkWTPNo', array($parameters));
    }

    /**
     * @param checkWTPNo2 $parameters
     * @return checkWTPNo2Response
     */
    public function checkWTPNo2(checkWTPNo2 $parameters)
    {
      return $this->__soapCall('checkWTPNo2', array($parameters));
    }

    /**
     * @param checkWTPNo3 $parameters
     * @return checkWTPNo3Response
     */
    public function checkWTPNo3(checkWTPNo3 $parameters)
    {
      return $this->__soapCall('checkWTPNo3', array($parameters));
    }

    /**
     * @param checkWTPNo4 $parameters
     * @return checkWTPNo4Response
     */
    public function checkWTPNo4(checkWTPNo4 $parameters)
    {
      return $this->__soapCall('checkWTPNo4', array($parameters));
    }

    /**
     * @param getPersonData $parameters
     * @return getPersonDataResponse
     */
    public function getPersonData(getPersonData $parameters)
    {
      return $this->__soapCall('getPersonData', array($parameters));
    }

    /**
     * @param getPersonData2 $parameters
     * @return getPersonData2Response
     */
    public function getPersonData2(getPersonData2 $parameters)
    {
      return $this->__soapCall('getPersonData2', array($parameters));
    }

    /**
     * @param getPersonData3 $parameters
     * @return getPersonData3Response
     */
    public function getPersonData3(getPersonData3 $parameters)
    {
      return $this->__soapCall('getPersonData3', array($parameters));
    }

    /**
     * @param getPersonData4 $parameters
     * @return getPersonData4Response
     */
    public function getPersonData4(getPersonData4 $parameters)
    {
      return $this->__soapCall('getPersonData4', array($parameters));
    }

    /**
     * @param getPersonData5 $parameters
     * @return getPersonData5Response
     */
    public function getPersonData5(getPersonData5 $parameters)
    {
      return $this->__soapCall('getPersonData5', array($parameters));
    }

    /**
     * @param getPersonData6 $parameters
     * @return getPersonData6Response
     */
    public function getPersonData6(getPersonData6 $parameters)
    {
      return $this->__soapCall('getPersonData6', array($parameters));
    }

}
