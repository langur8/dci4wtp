<?php

namespace Axess\Dci4Wtp;

class D4WTPPKGTARIFFLISTRESULT
{

    /**
     * @var ArrayOfD4WTPPKGTARIFFLIST $ACTPKGTARIFFLIST
     */
    protected $ACTPKGTARIFFLIST = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPKGTARIFFLIST
     */
    public function getACTPKGTARIFFLIST()
    {
      return $this->ACTPKGTARIFFLIST;
    }

    /**
     * @param ArrayOfD4WTPPKGTARIFFLIST $ACTPKGTARIFFLIST
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTRESULT
     */
    public function setACTPKGTARIFFLIST($ACTPKGTARIFFLIST)
    {
      $this->ACTPKGTARIFFLIST = $ACTPKGTARIFFLIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
