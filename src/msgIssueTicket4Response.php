<?php

namespace Axess\Dci4Wtp;

class msgIssueTicket4Response
{

    /**
     * @var D4WTPMSGISSUETICKETRESULT3 $msgIssueTicket4Result
     */
    protected $msgIssueTicket4Result = null;

    /**
     * @param D4WTPMSGISSUETICKETRESULT3 $msgIssueTicket4Result
     */
    public function __construct($msgIssueTicket4Result)
    {
      $this->msgIssueTicket4Result = $msgIssueTicket4Result;
    }

    /**
     * @return D4WTPMSGISSUETICKETRESULT3
     */
    public function getMsgIssueTicket4Result()
    {
      return $this->msgIssueTicket4Result;
    }

    /**
     * @param D4WTPMSGISSUETICKETRESULT3 $msgIssueTicket4Result
     * @return \Axess\Dci4Wtp\msgIssueTicket4Response
     */
    public function setMsgIssueTicket4Result($msgIssueTicket4Result)
    {
      $this->msgIssueTicket4Result = $msgIssueTicket4Result;
      return $this;
    }

}
