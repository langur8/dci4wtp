<?php

namespace Axess\Dci4Wtp;

class getAdditionalArticlesResponse
{

    /**
     * @var D4WTPADDARTICLERESULT $getAdditionalArticlesResult
     */
    protected $getAdditionalArticlesResult = null;

    /**
     * @param D4WTPADDARTICLERESULT $getAdditionalArticlesResult
     */
    public function __construct($getAdditionalArticlesResult)
    {
      $this->getAdditionalArticlesResult = $getAdditionalArticlesResult;
    }

    /**
     * @return D4WTPADDARTICLERESULT
     */
    public function getGetAdditionalArticlesResult()
    {
      return $this->getAdditionalArticlesResult;
    }

    /**
     * @param D4WTPADDARTICLERESULT $getAdditionalArticlesResult
     * @return \Axess\Dci4Wtp\getAdditionalArticlesResponse
     */
    public function setGetAdditionalArticlesResult($getAdditionalArticlesResult)
    {
      $this->getAdditionalArticlesResult = $getAdditionalArticlesResult;
      return $this;
    }

}
