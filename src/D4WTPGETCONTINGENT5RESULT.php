<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCONTINGENT5RESULT
{

    /**
     * @var ArrayOfD4WTPDAYCONTINGENTS $ACTDAYCONTINGENTS
     */
    protected $ACTDAYCONTINGENTS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPDAYCONTINGENTS
     */
    public function getACTDAYCONTINGENTS()
    {
      return $this->ACTDAYCONTINGENTS;
    }

    /**
     * @param ArrayOfD4WTPDAYCONTINGENTS $ACTDAYCONTINGENTS
     * @return \Axess\Dci4Wtp\D4WTPGETCONTINGENT5RESULT
     */
    public function setACTDAYCONTINGENTS($ACTDAYCONTINGENTS)
    {
      $this->ACTDAYCONTINGENTS = $ACTDAYCONTINGENTS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETCONTINGENT5RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETCONTINGENT5RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
