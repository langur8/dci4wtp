<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST3RESULT
{

    /**
     * @var ArrayOfD4WTPTARIFFLIST3 $ACTTARIFFLIST3
     */
    protected $ACTTARIFFLIST3 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLIST3
     */
    public function getACTTARIFFLIST3()
    {
      return $this->ACTTARIFFLIST3;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLIST3 $ACTTARIFFLIST3
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST3RESULT
     */
    public function setACTTARIFFLIST3($ACTTARIFFLIST3)
    {
      $this->ACTTARIFFLIST3 = $ACTTARIFFLIST3;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST3RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST3RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
