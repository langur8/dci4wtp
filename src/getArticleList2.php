<?php

namespace Axess\Dci4Wtp;

class getArticleList2
{

    /**
     * @var D4WTPGETARTICLELISTREQUEST $i_ctArticleListReq
     */
    protected $i_ctArticleListReq = null;

    /**
     * @param D4WTPGETARTICLELISTREQUEST $i_ctArticleListReq
     */
    public function __construct($i_ctArticleListReq)
    {
      $this->i_ctArticleListReq = $i_ctArticleListReq;
    }

    /**
     * @return D4WTPGETARTICLELISTREQUEST
     */
    public function getI_ctArticleListReq()
    {
      return $this->i_ctArticleListReq;
    }

    /**
     * @param D4WTPGETARTICLELISTREQUEST $i_ctArticleListReq
     * @return \Axess\Dci4Wtp\getArticleList2
     */
    public function setI_ctArticleListReq($i_ctArticleListReq)
    {
      $this->i_ctArticleListReq = $i_ctArticleListReq;
      return $this;
    }

}
