<?php

namespace Axess\Dci4Wtp;

class D4WTPCOMPANYWTPFIELDS
{

    /**
     * @var float $BISB2CSELECTABLE
     */
    protected $BISB2CSELECTABLE = null;

    /**
     * @var float $NWTPCASHIERNR
     */
    protected $NWTPCASHIERNR = null;

    /**
     * @var float $NWTPCUSTOMERPROFILENR
     */
    protected $NWTPCUSTOMERPROFILENR = null;

    /**
     * @var float $NWTPPOSNR
     */
    protected $NWTPPOSNR = null;

    /**
     * @var string $NWTPPROFILENR
     */
    protected $NWTPPROFILENR = null;

    /**
     * @var float $NXMLUSERID
     */
    protected $NXMLUSERID = null;

    /**
     * @var string $SZWTPLOGINID
     */
    protected $SZWTPLOGINID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISB2CSELECTABLE()
    {
      return $this->BISB2CSELECTABLE;
    }

    /**
     * @param float $BISB2CSELECTABLE
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYWTPFIELDS
     */
    public function setBISB2CSELECTABLE($BISB2CSELECTABLE)
    {
      $this->BISB2CSELECTABLE = $BISB2CSELECTABLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPCASHIERNR()
    {
      return $this->NWTPCASHIERNR;
    }

    /**
     * @param float $NWTPCASHIERNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYWTPFIELDS
     */
    public function setNWTPCASHIERNR($NWTPCASHIERNR)
    {
      $this->NWTPCASHIERNR = $NWTPCASHIERNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPCUSTOMERPROFILENR()
    {
      return $this->NWTPCUSTOMERPROFILENR;
    }

    /**
     * @param float $NWTPCUSTOMERPROFILENR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYWTPFIELDS
     */
    public function setNWTPCUSTOMERPROFILENR($NWTPCUSTOMERPROFILENR)
    {
      $this->NWTPCUSTOMERPROFILENR = $NWTPCUSTOMERPROFILENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPOSNR()
    {
      return $this->NWTPPOSNR;
    }

    /**
     * @param float $NWTPPOSNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYWTPFIELDS
     */
    public function setNWTPPOSNR($NWTPPOSNR)
    {
      $this->NWTPPOSNR = $NWTPPOSNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getNWTPPROFILENR()
    {
      return $this->NWTPPROFILENR;
    }

    /**
     * @param string $NWTPPROFILENR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYWTPFIELDS
     */
    public function setNWTPPROFILENR($NWTPPROFILENR)
    {
      $this->NWTPPROFILENR = $NWTPPROFILENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNXMLUSERID()
    {
      return $this->NXMLUSERID;
    }

    /**
     * @param float $NXMLUSERID
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYWTPFIELDS
     */
    public function setNXMLUSERID($NXMLUSERID)
    {
      $this->NXMLUSERID = $NXMLUSERID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPLOGINID()
    {
      return $this->SZWTPLOGINID;
    }

    /**
     * @param string $SZWTPLOGINID
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYWTPFIELDS
     */
    public function setSZWTPLOGINID($SZWTPLOGINID)
    {
      $this->SZWTPLOGINID = $SZWTPLOGINID;
      return $this;
    }

}
