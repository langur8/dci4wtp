<?php

namespace Axess\Dci4Wtp;

class SWISSTICKETSALE
{

    /**
     * @var float $NPERSONTYPENO
     */
    protected $NPERSONTYPENO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NTARIFFNR
     */
    protected $NTARIFFNR = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var string $SZEXTMATERIALNO
     */
    protected $SZEXTMATERIALNO = null;

    /**
     * @var string $SZPRODUCTIONDATE
     */
    protected $SZPRODUCTIONDATE = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPERSONTYPENO()
    {
      return $this->NPERSONTYPENO;
    }

    /**
     * @param float $NPERSONTYPENO
     * @return \Axess\Dci4Wtp\SWISSTICKETSALE
     */
    public function setNPERSONTYPENO($NPERSONTYPENO)
    {
      $this->NPERSONTYPENO = $NPERSONTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\SWISSTICKETSALE
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\SWISSTICKETSALE
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFFNR()
    {
      return $this->NTARIFFNR;
    }

    /**
     * @param float $NTARIFFNR
     * @return \Axess\Dci4Wtp\SWISSTICKETSALE
     */
    public function setNTARIFFNR($NTARIFFNR)
    {
      $this->NTARIFFNR = $NTARIFFNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\SWISSTICKETSALE
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTMATERIALNO()
    {
      return $this->SZEXTMATERIALNO;
    }

    /**
     * @param string $SZEXTMATERIALNO
     * @return \Axess\Dci4Wtp\SWISSTICKETSALE
     */
    public function setSZEXTMATERIALNO($SZEXTMATERIALNO)
    {
      $this->SZEXTMATERIALNO = $SZEXTMATERIALNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPRODUCTIONDATE()
    {
      return $this->SZPRODUCTIONDATE;
    }

    /**
     * @param string $SZPRODUCTIONDATE
     * @return \Axess\Dci4Wtp\SWISSTICKETSALE
     */
    public function setSZPRODUCTIONDATE($SZPRODUCTIONDATE)
    {
      $this->SZPRODUCTIONDATE = $SZPRODUCTIONDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\SWISSTICKETSALE
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\SWISSTICKETSALE
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
