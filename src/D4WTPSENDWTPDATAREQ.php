<?php

namespace Axess\Dci4Wtp;

class D4WTPSENDWTPDATAREQ
{

    /**
     * @var float $NLOGNRSTART
     */
    protected $NLOGNRSTART = null;

    /**
     * @var float $NMAXENTRIES
     */
    protected $NMAXENTRIES = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNLOGNRSTART()
    {
      return $this->NLOGNRSTART;
    }

    /**
     * @param float $NLOGNRSTART
     * @return \Axess\Dci4Wtp\D4WTPSENDWTPDATAREQ
     */
    public function setNLOGNRSTART($NLOGNRSTART)
    {
      $this->NLOGNRSTART = $NLOGNRSTART;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMAXENTRIES()
    {
      return $this->NMAXENTRIES;
    }

    /**
     * @param float $NMAXENTRIES
     * @return \Axess\Dci4Wtp\D4WTPSENDWTPDATAREQ
     */
    public function setNMAXENTRIES($NMAXENTRIES)
    {
      $this->NMAXENTRIES = $NMAXENTRIES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSENDWTPDATAREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
