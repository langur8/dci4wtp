<?php

namespace Axess\Dci4Wtp;

class createExtOrderNrResponse
{

    /**
     * @var D4WTPCREATEEXTORDERNRRESULT $createExtOrderNrResult
     */
    protected $createExtOrderNrResult = null;

    /**
     * @param D4WTPCREATEEXTORDERNRRESULT $createExtOrderNrResult
     */
    public function __construct($createExtOrderNrResult)
    {
      $this->createExtOrderNrResult = $createExtOrderNrResult;
    }

    /**
     * @return D4WTPCREATEEXTORDERNRRESULT
     */
    public function getCreateExtOrderNrResult()
    {
      return $this->createExtOrderNrResult;
    }

    /**
     * @param D4WTPCREATEEXTORDERNRRESULT $createExtOrderNrResult
     * @return \Axess\Dci4Wtp\createExtOrderNrResponse
     */
    public function setCreateExtOrderNrResult($createExtOrderNrResult)
    {
      $this->createExtOrderNrResult = $createExtOrderNrResult;
      return $this;
    }

}
