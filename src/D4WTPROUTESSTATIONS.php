<?php

namespace Axess\Dci4Wtp;

class D4WTPROUTESSTATIONS
{

    /**
     * @var float $BISCIRCLE
     */
    protected $BISCIRCLE = null;

    /**
     * @var float $NFROMPOENO
     */
    protected $NFROMPOENO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NRETOURTARIFFPOOLNO
     */
    protected $NRETOURTARIFFPOOLNO = null;

    /**
     * @var float $NROUTENO
     */
    protected $NROUTENO = null;

    /**
     * @var float $NSINGLETARIFFPOOLNO
     */
    protected $NSINGLETARIFFPOOLNO = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var float $NTOPOENO
     */
    protected $NTOPOENO = null;

    /**
     * @var float $NVARIANTNO
     */
    protected $NVARIANTNO = null;

    /**
     * @var float $NVIAPOENO
     */
    protected $NVIAPOENO = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZFROMLONGNAME
     */
    protected $SZFROMLONGNAME = null;

    /**
     * @var string $SZFROMMASKNAME
     */
    protected $SZFROMMASKNAME = null;

    /**
     * @var string $SZFROMSHORTNAME
     */
    protected $SZFROMSHORTNAME = null;

    /**
     * @var string $SZFROMSTATIONID
     */
    protected $SZFROMSTATIONID = null;

    /**
     * @var string $SZFROMSTATIONNAME
     */
    protected $SZFROMSTATIONNAME = null;

    /**
     * @var string $SZTOLONGNAME
     */
    protected $SZTOLONGNAME = null;

    /**
     * @var string $SZTOMASKNAME
     */
    protected $SZTOMASKNAME = null;

    /**
     * @var string $SZTOSHORTNAME
     */
    protected $SZTOSHORTNAME = null;

    /**
     * @var string $SZTOSTATIONID
     */
    protected $SZTOSTATIONID = null;

    /**
     * @var string $SZTOSTATIONNAME
     */
    protected $SZTOSTATIONNAME = null;

    /**
     * @var string $SZVARIANTNAME
     */
    protected $SZVARIANTNAME = null;

    /**
     * @var string $SZVIALONGNAME
     */
    protected $SZVIALONGNAME = null;

    /**
     * @var string $SZVIAMASKNAME
     */
    protected $SZVIAMASKNAME = null;

    /**
     * @var string $SZVIASHORTNAME
     */
    protected $SZVIASHORTNAME = null;

    /**
     * @var string $SZVIASTATIONID
     */
    protected $SZVIASTATIONID = null;

    /**
     * @var string $SZVIASTATIONNAME
     */
    protected $SZVIASTATIONNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISCIRCLE()
    {
      return $this->BISCIRCLE;
    }

    /**
     * @param float $BISCIRCLE
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setBISCIRCLE($BISCIRCLE)
    {
      $this->BISCIRCLE = $BISCIRCLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFROMPOENO()
    {
      return $this->NFROMPOENO;
    }

    /**
     * @param float $NFROMPOENO
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setNFROMPOENO($NFROMPOENO)
    {
      $this->NFROMPOENO = $NFROMPOENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRETOURTARIFFPOOLNO()
    {
      return $this->NRETOURTARIFFPOOLNO;
    }

    /**
     * @param float $NRETOURTARIFFPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setNRETOURTARIFFPOOLNO($NRETOURTARIFFPOOLNO)
    {
      $this->NRETOURTARIFFPOOLNO = $NRETOURTARIFFPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNROUTENO()
    {
      return $this->NROUTENO;
    }

    /**
     * @param float $NROUTENO
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setNROUTENO($NROUTENO)
    {
      $this->NROUTENO = $NROUTENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSINGLETARIFFPOOLNO()
    {
      return $this->NSINGLETARIFFPOOLNO;
    }

    /**
     * @param float $NSINGLETARIFFPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setNSINGLETARIFFPOOLNO($NSINGLETARIFFPOOLNO)
    {
      $this->NSINGLETARIFFPOOLNO = $NSINGLETARIFFPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTOPOENO()
    {
      return $this->NTOPOENO;
    }

    /**
     * @param float $NTOPOENO
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setNTOPOENO($NTOPOENO)
    {
      $this->NTOPOENO = $NTOPOENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVARIANTNO()
    {
      return $this->NVARIANTNO;
    }

    /**
     * @param float $NVARIANTNO
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setNVARIANTNO($NVARIANTNO)
    {
      $this->NVARIANTNO = $NVARIANTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVIAPOENO()
    {
      return $this->NVIAPOENO;
    }

    /**
     * @param float $NVIAPOENO
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setNVIAPOENO($NVIAPOENO)
    {
      $this->NVIAPOENO = $NVIAPOENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFROMLONGNAME()
    {
      return $this->SZFROMLONGNAME;
    }

    /**
     * @param string $SZFROMLONGNAME
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZFROMLONGNAME($SZFROMLONGNAME)
    {
      $this->SZFROMLONGNAME = $SZFROMLONGNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFROMMASKNAME()
    {
      return $this->SZFROMMASKNAME;
    }

    /**
     * @param string $SZFROMMASKNAME
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZFROMMASKNAME($SZFROMMASKNAME)
    {
      $this->SZFROMMASKNAME = $SZFROMMASKNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFROMSHORTNAME()
    {
      return $this->SZFROMSHORTNAME;
    }

    /**
     * @param string $SZFROMSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZFROMSHORTNAME($SZFROMSHORTNAME)
    {
      $this->SZFROMSHORTNAME = $SZFROMSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFROMSTATIONID()
    {
      return $this->SZFROMSTATIONID;
    }

    /**
     * @param string $SZFROMSTATIONID
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZFROMSTATIONID($SZFROMSTATIONID)
    {
      $this->SZFROMSTATIONID = $SZFROMSTATIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFROMSTATIONNAME()
    {
      return $this->SZFROMSTATIONNAME;
    }

    /**
     * @param string $SZFROMSTATIONNAME
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZFROMSTATIONNAME($SZFROMSTATIONNAME)
    {
      $this->SZFROMSTATIONNAME = $SZFROMSTATIONNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTOLONGNAME()
    {
      return $this->SZTOLONGNAME;
    }

    /**
     * @param string $SZTOLONGNAME
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZTOLONGNAME($SZTOLONGNAME)
    {
      $this->SZTOLONGNAME = $SZTOLONGNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTOMASKNAME()
    {
      return $this->SZTOMASKNAME;
    }

    /**
     * @param string $SZTOMASKNAME
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZTOMASKNAME($SZTOMASKNAME)
    {
      $this->SZTOMASKNAME = $SZTOMASKNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTOSHORTNAME()
    {
      return $this->SZTOSHORTNAME;
    }

    /**
     * @param string $SZTOSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZTOSHORTNAME($SZTOSHORTNAME)
    {
      $this->SZTOSHORTNAME = $SZTOSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTOSTATIONID()
    {
      return $this->SZTOSTATIONID;
    }

    /**
     * @param string $SZTOSTATIONID
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZTOSTATIONID($SZTOSTATIONID)
    {
      $this->SZTOSTATIONID = $SZTOSTATIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTOSTATIONNAME()
    {
      return $this->SZTOSTATIONNAME;
    }

    /**
     * @param string $SZTOSTATIONNAME
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZTOSTATIONNAME($SZTOSTATIONNAME)
    {
      $this->SZTOSTATIONNAME = $SZTOSTATIONNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVARIANTNAME()
    {
      return $this->SZVARIANTNAME;
    }

    /**
     * @param string $SZVARIANTNAME
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZVARIANTNAME($SZVARIANTNAME)
    {
      $this->SZVARIANTNAME = $SZVARIANTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVIALONGNAME()
    {
      return $this->SZVIALONGNAME;
    }

    /**
     * @param string $SZVIALONGNAME
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZVIALONGNAME($SZVIALONGNAME)
    {
      $this->SZVIALONGNAME = $SZVIALONGNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVIAMASKNAME()
    {
      return $this->SZVIAMASKNAME;
    }

    /**
     * @param string $SZVIAMASKNAME
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZVIAMASKNAME($SZVIAMASKNAME)
    {
      $this->SZVIAMASKNAME = $SZVIAMASKNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVIASHORTNAME()
    {
      return $this->SZVIASHORTNAME;
    }

    /**
     * @param string $SZVIASHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZVIASHORTNAME($SZVIASHORTNAME)
    {
      $this->SZVIASHORTNAME = $SZVIASHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVIASTATIONID()
    {
      return $this->SZVIASTATIONID;
    }

    /**
     * @param string $SZVIASTATIONID
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZVIASTATIONID($SZVIASTATIONID)
    {
      $this->SZVIASTATIONID = $SZVIASTATIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVIASTATIONNAME()
    {
      return $this->SZVIASTATIONNAME;
    }

    /**
     * @param string $SZVIASTATIONNAME
     * @return \Axess\Dci4Wtp\D4WTPROUTESSTATIONS
     */
    public function setSZVIASTATIONNAME($SZVIASTATIONNAME)
    {
      $this->SZVIASTATIONNAME = $SZVIASTATIONNAME;
      return $this;
    }

}
