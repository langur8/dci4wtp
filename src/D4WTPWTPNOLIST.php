<?php

namespace Axess\Dci4Wtp;

class D4WTPWTPNOLIST
{

    /**
     * @var string $SZKARTENNR
     */
    protected $SZKARTENNR = null;

    /**
     * @var string $SZPOSERSTVERWDAT
     */
    protected $SZPOSERSTVERWDAT = null;

    /**
     * @var string $SZPOSLETZTVERWDAT
     */
    protected $SZPOSLETZTVERWDAT = null;

    /**
     * @var string $SZWTPNO
     */
    protected $SZWTPNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZKARTENNR()
    {
      return $this->SZKARTENNR;
    }

    /**
     * @param string $SZKARTENNR
     * @return \Axess\Dci4Wtp\D4WTPWTPNOLIST
     */
    public function setSZKARTENNR($SZKARTENNR)
    {
      $this->SZKARTENNR = $SZKARTENNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOSERSTVERWDAT()
    {
      return $this->SZPOSERSTVERWDAT;
    }

    /**
     * @param string $SZPOSERSTVERWDAT
     * @return \Axess\Dci4Wtp\D4WTPWTPNOLIST
     */
    public function setSZPOSERSTVERWDAT($SZPOSERSTVERWDAT)
    {
      $this->SZPOSERSTVERWDAT = $SZPOSERSTVERWDAT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOSLETZTVERWDAT()
    {
      return $this->SZPOSLETZTVERWDAT;
    }

    /**
     * @param string $SZPOSLETZTVERWDAT
     * @return \Axess\Dci4Wtp\D4WTPWTPNOLIST
     */
    public function setSZPOSLETZTVERWDAT($SZPOSLETZTVERWDAT)
    {
      $this->SZPOSLETZTVERWDAT = $SZPOSLETZTVERWDAT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNO()
    {
      return $this->SZWTPNO;
    }

    /**
     * @param string $SZWTPNO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOLIST
     */
    public function setSZWTPNO($SZWTPNO)
    {
      $this->SZWTPNO = $SZWTPNO;
      return $this;
    }

}
