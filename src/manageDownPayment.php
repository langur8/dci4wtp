<?php

namespace Axess\Dci4Wtp;

class manageDownPayment
{

    /**
     * @var DCI4WTPGETDOWNPAYMENTREQ $i_ctManageDownPayment
     */
    protected $i_ctManageDownPayment = null;

    /**
     * @param DCI4WTPGETDOWNPAYMENTREQ $i_ctManageDownPayment
     */
    public function __construct($i_ctManageDownPayment)
    {
      $this->i_ctManageDownPayment = $i_ctManageDownPayment;
    }

    /**
     * @return DCI4WTPGETDOWNPAYMENTREQ
     */
    public function getI_ctManageDownPayment()
    {
      return $this->i_ctManageDownPayment;
    }

    /**
     * @param DCI4WTPGETDOWNPAYMENTREQ $i_ctManageDownPayment
     * @return \Axess\Dci4Wtp\manageDownPayment
     */
    public function setI_ctManageDownPayment($i_ctManageDownPayment)
    {
      $this->i_ctManageDownPayment = $i_ctManageDownPayment;
      return $this;
    }

}
