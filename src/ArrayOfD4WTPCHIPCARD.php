<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCHIPCARD implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCHIPCARD[] $D4WTPCHIPCARD
     */
    protected $D4WTPCHIPCARD = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCHIPCARD[]
     */
    public function getD4WTPCHIPCARD()
    {
      return $this->D4WTPCHIPCARD;
    }

    /**
     * @param D4WTPCHIPCARD[] $D4WTPCHIPCARD
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCHIPCARD
     */
    public function setD4WTPCHIPCARD(array $D4WTPCHIPCARD = null)
    {
      $this->D4WTPCHIPCARD = $D4WTPCHIPCARD;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCHIPCARD[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCHIPCARD
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCHIPCARD[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCHIPCARD $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCHIPCARD[] = $value;
      } else {
        $this->D4WTPCHIPCARD[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCHIPCARD[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCHIPCARD Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCHIPCARD);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCHIPCARD);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCHIPCARD);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCHIPCARD);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCHIPCARD Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCHIPCARD);
    }

}
