<?php

namespace Axess\Dci4Wtp;

class DCI4WTPORDERPOSITION
{

    /**
     * @var ArrayOfD4WTPADDARTICLEINFO $ACTADDARTICLE
     */
    protected $ACTADDARTICLE = null;

    /**
     * @var float $NORDERNO
     */
    protected $NORDERNO = null;

    /**
     * @var float $NPERSNR
     */
    protected $NPERSNR = null;

    /**
     * @var float $NPERSONTYPENO
     */
    protected $NPERSONTYPENO = null;

    /**
     * @var float $NPERSPOSNR
     */
    protected $NPERSPOSNR = null;

    /**
     * @var float $NPERSPROJNR
     */
    protected $NPERSPROJNR = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPOSITIONNO
     */
    protected $NPOSITIONNO = null;

    /**
     * @var float $NSINGLETARIFF
     */
    protected $NSINGLETARIFF = null;

    /**
     * @var float $NSUBPOSITIONNO
     */
    protected $NSUBPOSITIONNO = null;

    /**
     * @var float $NTICKETSQUANTITY
     */
    protected $NTICKETSQUANTITY = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var string $SZACCEPTANCENO
     */
    protected $SZACCEPTANCENO = null;

    /**
     * @var string $SZCHIPID
     */
    protected $SZCHIPID = null;

    /**
     * @var string $SZCHIPIDCRC
     */
    protected $SZCHIPIDCRC = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDARTICLEINFO
     */
    public function getACTADDARTICLE()
    {
      return $this->ACTADDARTICLE;
    }

    /**
     * @param ArrayOfD4WTPADDARTICLEINFO $ACTADDARTICLE
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setACTADDARTICLE($ACTADDARTICLE)
    {
      $this->ACTADDARTICLE = $ACTADDARTICLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORDERNO()
    {
      return $this->NORDERNO;
    }

    /**
     * @param float $NORDERNO
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setNORDERNO($NORDERNO)
    {
      $this->NORDERNO = $NORDERNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNR()
    {
      return $this->NPERSNR;
    }

    /**
     * @param float $NPERSNR
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setNPERSNR($NPERSNR)
    {
      $this->NPERSNR = $NPERSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSONTYPENO()
    {
      return $this->NPERSONTYPENO;
    }

    /**
     * @param float $NPERSONTYPENO
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setNPERSONTYPENO($NPERSONTYPENO)
    {
      $this->NPERSONTYPENO = $NPERSONTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNR()
    {
      return $this->NPERSPOSNR;
    }

    /**
     * @param float $NPERSPOSNR
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setNPERSPOSNR($NPERSPOSNR)
    {
      $this->NPERSPOSNR = $NPERSPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNR()
    {
      return $this->NPERSPROJNR;
    }

    /**
     * @param float $NPERSPROJNR
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setNPERSPROJNR($NPERSPROJNR)
    {
      $this->NPERSPROJNR = $NPERSPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSITIONNO()
    {
      return $this->NPOSITIONNO;
    }

    /**
     * @param float $NPOSITIONNO
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setNPOSITIONNO($NPOSITIONNO)
    {
      $this->NPOSITIONNO = $NPOSITIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSINGLETARIFF()
    {
      return $this->NSINGLETARIFF;
    }

    /**
     * @param float $NSINGLETARIFF
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setNSINGLETARIFF($NSINGLETARIFF)
    {
      $this->NSINGLETARIFF = $NSINGLETARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBPOSITIONNO()
    {
      return $this->NSUBPOSITIONNO;
    }

    /**
     * @param float $NSUBPOSITIONNO
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setNSUBPOSITIONNO($NSUBPOSITIONNO)
    {
      $this->NSUBPOSITIONNO = $NSUBPOSITIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETSQUANTITY()
    {
      return $this->NTICKETSQUANTITY;
    }

    /**
     * @param float $NTICKETSQUANTITY
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setNTICKETSQUANTITY($NTICKETSQUANTITY)
    {
      $this->NTICKETSQUANTITY = $NTICKETSQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZACCEPTANCENO()
    {
      return $this->SZACCEPTANCENO;
    }

    /**
     * @param string $SZACCEPTANCENO
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setSZACCEPTANCENO($SZACCEPTANCENO)
    {
      $this->SZACCEPTANCENO = $SZACCEPTANCENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCHIPID()
    {
      return $this->SZCHIPID;
    }

    /**
     * @param string $SZCHIPID
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setSZCHIPID($SZCHIPID)
    {
      $this->SZCHIPID = $SZCHIPID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCHIPIDCRC()
    {
      return $this->SZCHIPIDCRC;
    }

    /**
     * @param string $SZCHIPIDCRC
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setSZCHIPIDCRC($SZCHIPIDCRC)
    {
      $this->SZCHIPIDCRC = $SZCHIPIDCRC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\DCI4WTPORDERPOSITION
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
