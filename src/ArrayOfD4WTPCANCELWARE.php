<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCANCELWARE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCANCELWARE[] $D4WTPCANCELWARE
     */
    protected $D4WTPCANCELWARE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCANCELWARE[]
     */
    public function getD4WTPCANCELWARE()
    {
      return $this->D4WTPCANCELWARE;
    }

    /**
     * @param D4WTPCANCELWARE[] $D4WTPCANCELWARE
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCANCELWARE
     */
    public function setD4WTPCANCELWARE(array $D4WTPCANCELWARE = null)
    {
      $this->D4WTPCANCELWARE = $D4WTPCANCELWARE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCANCELWARE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCANCELWARE
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCANCELWARE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCANCELWARE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCANCELWARE[] = $value;
      } else {
        $this->D4WTPCANCELWARE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCANCELWARE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCANCELWARE Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCANCELWARE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCANCELWARE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCANCELWARE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCANCELWARE);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCANCELWARE Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCANCELWARE);
    }

}
