<?php

namespace Axess\Dci4Wtp;

class getPackageContentResponse
{

    /**
     * @var D4WTPPACKAGECONTENTRESULT $getPackageContentResult
     */
    protected $getPackageContentResult = null;

    /**
     * @param D4WTPPACKAGECONTENTRESULT $getPackageContentResult
     */
    public function __construct($getPackageContentResult)
    {
      $this->getPackageContentResult = $getPackageContentResult;
    }

    /**
     * @return D4WTPPACKAGECONTENTRESULT
     */
    public function getGetPackageContentResult()
    {
      return $this->getPackageContentResult;
    }

    /**
     * @param D4WTPPACKAGECONTENTRESULT $getPackageContentResult
     * @return \Axess\Dci4Wtp\getPackageContentResponse
     */
    public function setGetPackageContentResult($getPackageContentResult)
    {
      $this->getPackageContentResult = $getPackageContentResult;
      return $this;
    }

}
