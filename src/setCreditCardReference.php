<?php

namespace Axess\Dci4Wtp;

class setCreditCardReference
{

    /**
     * @var D4WTPSETCTCREFIDREQUEST $i_ctCTCRefIdReq
     */
    protected $i_ctCTCRefIdReq = null;

    /**
     * @param D4WTPSETCTCREFIDREQUEST $i_ctCTCRefIdReq
     */
    public function __construct($i_ctCTCRefIdReq)
    {
      $this->i_ctCTCRefIdReq = $i_ctCTCRefIdReq;
    }

    /**
     * @return D4WTPSETCTCREFIDREQUEST
     */
    public function getI_ctCTCRefIdReq()
    {
      return $this->i_ctCTCRefIdReq;
    }

    /**
     * @param D4WTPSETCTCREFIDREQUEST $i_ctCTCRefIdReq
     * @return \Axess\Dci4Wtp\setCreditCardReference
     */
    public function setI_ctCTCRefIdReq($i_ctCTCRefIdReq)
    {
      $this->i_ctCTCRefIdReq = $i_ctCTCRefIdReq;
      return $this;
    }

}
