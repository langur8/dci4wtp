<?php

namespace Axess\Dci4Wtp;

class D4WTPREPAIDRESULT
{

    /**
     * @var ArrayOfD4WTPPREPAIDTICKET $ACTPREPAIDTICKET
     */
    protected $ACTPREPAIDTICKET = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPREPAIDTICKET
     */
    public function getACTPREPAIDTICKET()
    {
      return $this->ACTPREPAIDTICKET;
    }

    /**
     * @param ArrayOfD4WTPPREPAIDTICKET $ACTPREPAIDTICKET
     * @return \Axess\Dci4Wtp\D4WTPREPAIDRESULT
     */
    public function setACTPREPAIDTICKET($ACTPREPAIDTICKET)
    {
      $this->ACTPREPAIDTICKET = $ACTPREPAIDTICKET;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPREPAIDRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPREPAIDRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
