<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGEARTICLE2
{

    /**
     * @var float $BMANDATORY
     */
    protected $BMANDATORY = null;

    /**
     * @var D4WTPCOMPANYCONDITION $CTCOMPANYCONDITION
     */
    protected $CTCOMPANYCONDITION = null;

    /**
     * @var float $NARTICLECONTENTGROUPNO
     */
    protected $NARTICLECONTENTGROUPNO = null;

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NARTICLETYPENO
     */
    protected $NARTICLETYPENO = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NSORTID
     */
    protected $NSORTID = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NVATAMOUNT
     */
    protected $NVATAMOUNT = null;

    /**
     * @var float $NVATPERCENT
     */
    protected $NVATPERCENT = null;

    /**
     * @var string $SZARTICLECONTENTGROUPNAME
     */
    protected $SZARTICLECONTENTGROUPNAME = null;

    /**
     * @var string $SZARTICLETYPENAME
     */
    protected $SZARTICLETYPENAME = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBMANDATORY()
    {
      return $this->BMANDATORY;
    }

    /**
     * @param float $BMANDATORY
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEARTICLE2
     */
    public function setBMANDATORY($BMANDATORY)
    {
      $this->BMANDATORY = $BMANDATORY;
      return $this;
    }

    /**
     * @return D4WTPCOMPANYCONDITION
     */
    public function getCTCOMPANYCONDITION()
    {
      return $this->CTCOMPANYCONDITION;
    }

    /**
     * @param D4WTPCOMPANYCONDITION $CTCOMPANYCONDITION
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEARTICLE2
     */
    public function setCTCOMPANYCONDITION($CTCOMPANYCONDITION)
    {
      $this->CTCOMPANYCONDITION = $CTCOMPANYCONDITION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLECONTENTGROUPNO()
    {
      return $this->NARTICLECONTENTGROUPNO;
    }

    /**
     * @param float $NARTICLECONTENTGROUPNO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEARTICLE2
     */
    public function setNARTICLECONTENTGROUPNO($NARTICLECONTENTGROUPNO)
    {
      $this->NARTICLECONTENTGROUPNO = $NARTICLECONTENTGROUPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEARTICLE2
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLETYPENO()
    {
      return $this->NARTICLETYPENO;
    }

    /**
     * @param float $NARTICLETYPENO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEARTICLE2
     */
    public function setNARTICLETYPENO($NARTICLETYPENO)
    {
      $this->NARTICLETYPENO = $NARTICLETYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEARTICLE2
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTID()
    {
      return $this->NSORTID;
    }

    /**
     * @param float $NSORTID
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEARTICLE2
     */
    public function setNSORTID($NSORTID)
    {
      $this->NSORTID = $NSORTID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEARTICLE2
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATAMOUNT()
    {
      return $this->NVATAMOUNT;
    }

    /**
     * @param float $NVATAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEARTICLE2
     */
    public function setNVATAMOUNT($NVATAMOUNT)
    {
      $this->NVATAMOUNT = $NVATAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATPERCENT()
    {
      return $this->NVATPERCENT;
    }

    /**
     * @param float $NVATPERCENT
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEARTICLE2
     */
    public function setNVATPERCENT($NVATPERCENT)
    {
      $this->NVATPERCENT = $NVATPERCENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZARTICLECONTENTGROUPNAME()
    {
      return $this->SZARTICLECONTENTGROUPNAME;
    }

    /**
     * @param string $SZARTICLECONTENTGROUPNAME
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEARTICLE2
     */
    public function setSZARTICLECONTENTGROUPNAME($SZARTICLECONTENTGROUPNAME)
    {
      $this->SZARTICLECONTENTGROUPNAME = $SZARTICLECONTENTGROUPNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZARTICLETYPENAME()
    {
      return $this->SZARTICLETYPENAME;
    }

    /**
     * @param string $SZARTICLETYPENAME
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEARTICLE2
     */
    public function setSZARTICLETYPENAME($SZARTICLETYPENAME)
    {
      $this->SZARTICLETYPENAME = $SZARTICLETYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEARTICLE2
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
