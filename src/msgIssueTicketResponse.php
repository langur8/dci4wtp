<?php

namespace Axess\Dci4Wtp;

class msgIssueTicketResponse
{

    /**
     * @var D4WTPMSGISSUETICKETRESULT $msgIssueTicketResult
     */
    protected $msgIssueTicketResult = null;

    /**
     * @param D4WTPMSGISSUETICKETRESULT $msgIssueTicketResult
     */
    public function __construct($msgIssueTicketResult)
    {
      $this->msgIssueTicketResult = $msgIssueTicketResult;
    }

    /**
     * @return D4WTPMSGISSUETICKETRESULT
     */
    public function getMsgIssueTicketResult()
    {
      return $this->msgIssueTicketResult;
    }

    /**
     * @param D4WTPMSGISSUETICKETRESULT $msgIssueTicketResult
     * @return \Axess\Dci4Wtp\msgIssueTicketResponse
     */
    public function setMsgIssueTicketResult($msgIssueTicketResult)
    {
      $this->msgIssueTicketResult = $msgIssueTicketResult;
      return $this;
    }

}
