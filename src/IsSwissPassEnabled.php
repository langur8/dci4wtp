<?php

namespace Axess\Dci4Wtp;

class IsSwissPassEnabled
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @param float $i_nSessionID
     */
    public function __construct($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\IsSwissPassEnabled
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

}
