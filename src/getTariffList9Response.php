<?php

namespace Axess\Dci4Wtp;

class getTariffList9Response
{

    /**
     * @var D4WTPTARIFFLIST9RESULT $getTariffList9Result
     */
    protected $getTariffList9Result = null;

    /**
     * @param D4WTPTARIFFLIST9RESULT $getTariffList9Result
     */
    public function __construct($getTariffList9Result)
    {
      $this->getTariffList9Result = $getTariffList9Result;
    }

    /**
     * @return D4WTPTARIFFLIST9RESULT
     */
    public function getGetTariffList9Result()
    {
      return $this->getTariffList9Result;
    }

    /**
     * @param D4WTPTARIFFLIST9RESULT $getTariffList9Result
     * @return \Axess\Dci4Wtp\getTariffList9Response
     */
    public function setGetTariffList9Result($getTariffList9Result)
    {
      $this->getTariffList9Result = $getTariffList9Result;
      return $this;
    }

}
