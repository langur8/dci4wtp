<?php

namespace Axess\Dci4Wtp;

class manageDownPaymentResponse
{

    /**
     * @var D4WTPRESULT $manageDownPaymentResult
     */
    protected $manageDownPaymentResult = null;

    /**
     * @param D4WTPRESULT $manageDownPaymentResult
     */
    public function __construct($manageDownPaymentResult)
    {
      $this->manageDownPaymentResult = $manageDownPaymentResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getManageDownPaymentResult()
    {
      return $this->manageDownPaymentResult;
    }

    /**
     * @param D4WTPRESULT $manageDownPaymentResult
     * @return \Axess\Dci4Wtp\manageDownPaymentResponse
     */
    public function setManageDownPaymentResult($manageDownPaymentResult)
    {
      $this->manageDownPaymentResult = $manageDownPaymentResult;
      return $this;
    }

}
