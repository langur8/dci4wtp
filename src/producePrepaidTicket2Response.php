<?php

namespace Axess\Dci4Wtp;

class producePrepaidTicket2Response
{

    /**
     * @var D4WTPRODUCEPREPAIDTICKETRES $producePrepaidTicket2Result
     */
    protected $producePrepaidTicket2Result = null;

    /**
     * @param D4WTPRODUCEPREPAIDTICKETRES $producePrepaidTicket2Result
     */
    public function __construct($producePrepaidTicket2Result)
    {
      $this->producePrepaidTicket2Result = $producePrepaidTicket2Result;
    }

    /**
     * @return D4WTPRODUCEPREPAIDTICKETRES
     */
    public function getProducePrepaidTicket2Result()
    {
      return $this->producePrepaidTicket2Result;
    }

    /**
     * @param D4WTPRODUCEPREPAIDTICKETRES $producePrepaidTicket2Result
     * @return \Axess\Dci4Wtp\producePrepaidTicket2Response
     */
    public function setProducePrepaidTicket2Result($producePrepaidTicket2Result)
    {
      $this->producePrepaidTicket2Result = $producePrepaidTicket2Result;
      return $this;
    }

}
