<?php

namespace Axess\Dci4Wtp;

class D4WTPPRODUCEPREPAIDREQ
{

    /**
     * @var ArrayOfD4WTPPREPAIDARTICLES $ACTPREPAIDARTICLES
     */
    protected $ACTPREPAIDARTICLES = null;

    /**
     * @var float $BINCLALLARTICLECARDMASKS
     */
    protected $BINCLALLARTICLECARDMASKS = null;

    /**
     * @var float $NCODINGCOMPNR
     */
    protected $NCODINGCOMPNR = null;

    /**
     * @var float $NCODINGCOMPPOSNR
     */
    protected $NCODINGCOMPPOSNR = null;

    /**
     * @var float $NCODINGCOMPPROJNR
     */
    protected $NCODINGCOMPPROJNR = null;

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var float $NPICKUPBOXNR
     */
    protected $NPICKUPBOXNR = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZCODINGMODE
     */
    protected $SZCODINGMODE = null;

    /**
     * @var string $SZDCCONTENT
     */
    protected $SZDCCONTENT = null;

    /**
     * @var string $SZDRIVERTYPE
     */
    protected $SZDRIVERTYPE = null;

    /**
     * @var string $SZEXTORDERNR
     */
    protected $SZEXTORDERNR = null;

    /**
     * @var string $SZWTPNO
     */
    protected $SZWTPNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPREPAIDARTICLES
     */
    public function getACTPREPAIDARTICLES()
    {
      return $this->ACTPREPAIDARTICLES;
    }

    /**
     * @param ArrayOfD4WTPPREPAIDARTICLES $ACTPREPAIDARTICLES
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setACTPREPAIDARTICLES($ACTPREPAIDARTICLES)
    {
      $this->ACTPREPAIDARTICLES = $ACTPREPAIDARTICLES;
      return $this;
    }

    /**
     * @return float
     */
    public function getBINCLALLARTICLECARDMASKS()
    {
      return $this->BINCLALLARTICLECARDMASKS;
    }

    /**
     * @param float $BINCLALLARTICLECARDMASKS
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setBINCLALLARTICLECARDMASKS($BINCLALLARTICLECARDMASKS)
    {
      $this->BINCLALLARTICLECARDMASKS = $BINCLALLARTICLECARDMASKS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCODINGCOMPNR()
    {
      return $this->NCODINGCOMPNR;
    }

    /**
     * @param float $NCODINGCOMPNR
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setNCODINGCOMPNR($NCODINGCOMPNR)
    {
      $this->NCODINGCOMPNR = $NCODINGCOMPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCODINGCOMPPOSNR()
    {
      return $this->NCODINGCOMPPOSNR;
    }

    /**
     * @param float $NCODINGCOMPPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setNCODINGCOMPPOSNR($NCODINGCOMPPOSNR)
    {
      $this->NCODINGCOMPPOSNR = $NCODINGCOMPPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCODINGCOMPPROJNR()
    {
      return $this->NCODINGCOMPPROJNR;
    }

    /**
     * @param float $NCODINGCOMPPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setNCODINGCOMPPROJNR($NCODINGCOMPPROJNR)
    {
      $this->NCODINGCOMPPROJNR = $NCODINGCOMPPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICKUPBOXNR()
    {
      return $this->NPICKUPBOXNR;
    }

    /**
     * @param float $NPICKUPBOXNR
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setNPICKUPBOXNR($NPICKUPBOXNR)
    {
      $this->NPICKUPBOXNR = $NPICKUPBOXNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGMODE()
    {
      return $this->SZCODINGMODE;
    }

    /**
     * @param string $SZCODINGMODE
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setSZCODINGMODE($SZCODINGMODE)
    {
      $this->SZCODINGMODE = $SZCODINGMODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDCCONTENT()
    {
      return $this->SZDCCONTENT;
    }

    /**
     * @param string $SZDCCONTENT
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setSZDCCONTENT($SZDCCONTENT)
    {
      $this->SZDCCONTENT = $SZDCCONTENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDRIVERTYPE()
    {
      return $this->SZDRIVERTYPE;
    }

    /**
     * @param string $SZDRIVERTYPE
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setSZDRIVERTYPE($SZDRIVERTYPE)
    {
      $this->SZDRIVERTYPE = $SZDRIVERTYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTORDERNR()
    {
      return $this->SZEXTORDERNR;
    }

    /**
     * @param string $SZEXTORDERNR
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setSZEXTORDERNR($SZEXTORDERNR)
    {
      $this->SZEXTORDERNR = $SZEXTORDERNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNO()
    {
      return $this->SZWTPNO;
    }

    /**
     * @param string $SZWTPNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCEPREPAIDREQ
     */
    public function setSZWTPNO($SZWTPNO)
    {
      $this->SZWTPNO = $SZWTPNO;
      return $this;
    }

}
