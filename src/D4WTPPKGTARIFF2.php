<?php

namespace Axess\Dci4Wtp;

class D4WTPPKGTARIFF2
{

    /**
     * @var ArrayOfD4WTPADDARTTARIFF $ACTADDARTTARIFF
     */
    protected $ACTADDARTTARIFF = null;

    /**
     * @var ArrayOfD4WTPCONTINGENTLIST $ACTCONTINGENTLIST
     */
    protected $ACTCONTINGENTLIST = null;

    /**
     * @var ArrayOfD4WTPDATTRAEGNO $ACTDATTRAEGNO
     */
    protected $ACTDATTRAEGNO = null;

    /**
     * @var float $BPARKINGFEATURE
     */
    protected $BPARKINGFEATURE = null;

    /**
     * @var float $BPERSDATEN
     */
    protected $BPERSDATEN = null;

    /**
     * @var float $BPREDATEABLE
     */
    protected $BPREDATEABLE = null;

    /**
     * @var float $BWTPWITHOUTPRINTER
     */
    protected $BWTPWITHOUTPRINTER = null;

    /**
     * @var float $BWTPWITHPRINTER
     */
    protected $BWTPWITHPRINTER = null;

    /**
     * @var float $NBASICCARDTYPENO
     */
    protected $NBASICCARDTYPENO = null;

    /**
     * @var float $NDEFDATTRAEGTYPNR
     */
    protected $NDEFDATTRAEGTYPNR = null;

    /**
     * @var float $NDEFROHLINGSTYPNR
     */
    protected $NDEFROHLINGSTYPNR = null;

    /**
     * @var float $NPERSONTYPENO
     */
    protected $NPERSONTYPENO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSTEUERBETRAG
     */
    protected $NSTEUERBETRAG = null;

    /**
     * @var float $NSTEUERPROZENT
     */
    protected $NSTEUERPROZENT = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var string $SZCURRENCY
     */
    protected $SZCURRENCY = null;

    /**
     * @var string $SZEXTMATERIALNR
     */
    protected $SZEXTMATERIALNR = null;

    /**
     * @var string $SZPERSTYPENAME
     */
    protected $SZPERSTYPENAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZTARIFVALIDTO
     */
    protected $SZTARIFVALIDTO = null;

    /**
     * @var string $SZTICKETTYPENAME
     */
    protected $SZTICKETTYPENAME = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDARTTARIFF
     */
    public function getACTADDARTTARIFF()
    {
      return $this->ACTADDARTTARIFF;
    }

    /**
     * @param ArrayOfD4WTPADDARTTARIFF $ACTADDARTTARIFF
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setACTADDARTTARIFF($ACTADDARTTARIFF)
    {
      $this->ACTADDARTTARIFF = $ACTADDARTTARIFF;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPCONTINGENTLIST
     */
    public function getACTCONTINGENTLIST()
    {
      return $this->ACTCONTINGENTLIST;
    }

    /**
     * @param ArrayOfD4WTPCONTINGENTLIST $ACTCONTINGENTLIST
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setACTCONTINGENTLIST($ACTCONTINGENTLIST)
    {
      $this->ACTCONTINGENTLIST = $ACTCONTINGENTLIST;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPDATTRAEGNO
     */
    public function getACTDATTRAEGNO()
    {
      return $this->ACTDATTRAEGNO;
    }

    /**
     * @param ArrayOfD4WTPDATTRAEGNO $ACTDATTRAEGNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setACTDATTRAEGNO($ACTDATTRAEGNO)
    {
      $this->ACTDATTRAEGNO = $ACTDATTRAEGNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPARKINGFEATURE()
    {
      return $this->BPARKINGFEATURE;
    }

    /**
     * @param float $BPARKINGFEATURE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setBPARKINGFEATURE($BPARKINGFEATURE)
    {
      $this->BPARKINGFEATURE = $BPARKINGFEATURE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPERSDATEN()
    {
      return $this->BPERSDATEN;
    }

    /**
     * @param float $BPERSDATEN
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setBPERSDATEN($BPERSDATEN)
    {
      $this->BPERSDATEN = $BPERSDATEN;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPREDATEABLE()
    {
      return $this->BPREDATEABLE;
    }

    /**
     * @param float $BPREDATEABLE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setBPREDATEABLE($BPREDATEABLE)
    {
      $this->BPREDATEABLE = $BPREDATEABLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBWTPWITHOUTPRINTER()
    {
      return $this->BWTPWITHOUTPRINTER;
    }

    /**
     * @param float $BWTPWITHOUTPRINTER
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setBWTPWITHOUTPRINTER($BWTPWITHOUTPRINTER)
    {
      $this->BWTPWITHOUTPRINTER = $BWTPWITHOUTPRINTER;
      return $this;
    }

    /**
     * @return float
     */
    public function getBWTPWITHPRINTER()
    {
      return $this->BWTPWITHPRINTER;
    }

    /**
     * @param float $BWTPWITHPRINTER
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setBWTPWITHPRINTER($BWTPWITHPRINTER)
    {
      $this->BWTPWITHPRINTER = $BWTPWITHPRINTER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBASICCARDTYPENO()
    {
      return $this->NBASICCARDTYPENO;
    }

    /**
     * @param float $NBASICCARDTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setNBASICCARDTYPENO($NBASICCARDTYPENO)
    {
      $this->NBASICCARDTYPENO = $NBASICCARDTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDEFDATTRAEGTYPNR()
    {
      return $this->NDEFDATTRAEGTYPNR;
    }

    /**
     * @param float $NDEFDATTRAEGTYPNR
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setNDEFDATTRAEGTYPNR($NDEFDATTRAEGTYPNR)
    {
      $this->NDEFDATTRAEGTYPNR = $NDEFDATTRAEGTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDEFROHLINGSTYPNR()
    {
      return $this->NDEFROHLINGSTYPNR;
    }

    /**
     * @param float $NDEFROHLINGSTYPNR
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setNDEFROHLINGSTYPNR($NDEFROHLINGSTYPNR)
    {
      $this->NDEFROHLINGSTYPNR = $NDEFROHLINGSTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSONTYPENO()
    {
      return $this->NPERSONTYPENO;
    }

    /**
     * @param float $NPERSONTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setNPERSONTYPENO($NPERSONTYPENO)
    {
      $this->NPERSONTYPENO = $NPERSONTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTEUERBETRAG()
    {
      return $this->NSTEUERBETRAG;
    }

    /**
     * @param float $NSTEUERBETRAG
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setNSTEUERBETRAG($NSTEUERBETRAG)
    {
      $this->NSTEUERBETRAG = $NSTEUERBETRAG;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTEUERPROZENT()
    {
      return $this->NSTEUERPROZENT;
    }

    /**
     * @param float $NSTEUERPROZENT
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setNSTEUERPROZENT($NSTEUERPROZENT)
    {
      $this->NSTEUERPROZENT = $NSTEUERPROZENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCY()
    {
      return $this->SZCURRENCY;
    }

    /**
     * @param string $SZCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setSZCURRENCY($SZCURRENCY)
    {
      $this->SZCURRENCY = $SZCURRENCY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTMATERIALNR()
    {
      return $this->SZEXTMATERIALNR;
    }

    /**
     * @param string $SZEXTMATERIALNR
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setSZEXTMATERIALNR($SZEXTMATERIALNR)
    {
      $this->SZEXTMATERIALNR = $SZEXTMATERIALNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSTYPENAME()
    {
      return $this->SZPERSTYPENAME;
    }

    /**
     * @param string $SZPERSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setSZPERSTYPENAME($SZPERSTYPENAME)
    {
      $this->SZPERSTYPENAME = $SZPERSTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTARIFVALIDTO()
    {
      return $this->SZTARIFVALIDTO;
    }

    /**
     * @param string $SZTARIFVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setSZTARIFVALIDTO($SZTARIFVALIDTO)
    {
      $this->SZTARIFVALIDTO = $SZTARIFVALIDTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPENAME()
    {
      return $this->SZTICKETTYPENAME;
    }

    /**
     * @param string $SZTICKETTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setSZTICKETTYPENAME($SZTICKETTYPENAME)
    {
      $this->SZTICKETTYPENAME = $SZTICKETTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF2
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
