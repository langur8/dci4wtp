<?php

namespace Axess\Dci4Wtp;

class msgReplaceTicketResponse
{

    /**
     * @var D4WTPRESULT $msgReplaceTicketResult
     */
    protected $msgReplaceTicketResult = null;

    /**
     * @param D4WTPRESULT $msgReplaceTicketResult
     */
    public function __construct($msgReplaceTicketResult)
    {
      $this->msgReplaceTicketResult = $msgReplaceTicketResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getMsgReplaceTicketResult()
    {
      return $this->msgReplaceTicketResult;
    }

    /**
     * @param D4WTPRESULT $msgReplaceTicketResult
     * @return \Axess\Dci4Wtp\msgReplaceTicketResponse
     */
    public function setMsgReplaceTicketResult($msgReplaceTicketResult)
    {
      $this->msgReplaceTicketResult = $msgReplaceTicketResult;
      return $this;
    }

}
