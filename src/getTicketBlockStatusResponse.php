<?php

namespace Axess\Dci4Wtp;

class getTicketBlockStatusResponse
{

    /**
     * @var D4WTPGETTCKTBLCKSTATUSRESULT $getTicketBlockStatusResult
     */
    protected $getTicketBlockStatusResult = null;

    /**
     * @param D4WTPGETTCKTBLCKSTATUSRESULT $getTicketBlockStatusResult
     */
    public function __construct($getTicketBlockStatusResult)
    {
      $this->getTicketBlockStatusResult = $getTicketBlockStatusResult;
    }

    /**
     * @return D4WTPGETTCKTBLCKSTATUSRESULT
     */
    public function getGetTicketBlockStatusResult()
    {
      return $this->getTicketBlockStatusResult;
    }

    /**
     * @param D4WTPGETTCKTBLCKSTATUSRESULT $getTicketBlockStatusResult
     * @return \Axess\Dci4Wtp\getTicketBlockStatusResponse
     */
    public function setGetTicketBlockStatusResult($getTicketBlockStatusResult)
    {
      $this->getTicketBlockStatusResult = $getTicketBlockStatusResult;
      return $this;
    }

}
