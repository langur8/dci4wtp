<?php

namespace Axess\Dci4Wtp;

class D4WTPADDLICENSEPLATERESULT
{

    /**
     * @var float $NCUSTOMERACCOUNTNR
     */
    protected $NCUSTOMERACCOUNTNR = null;

    /**
     * @var float $NCUSTPROJNR
     */
    protected $NCUSTPROJNR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NLFDNR
     */
    protected $NLFDNR = null;

    /**
     * @var float $NPERSKASSANR
     */
    protected $NPERSKASSANR = null;

    /**
     * @var float $NPERSNR
     */
    protected $NPERSNR = null;

    /**
     * @var float $NPERSPROJNR
     */
    protected $NPERSPROJNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZGILTAB
     */
    protected $SZGILTAB = null;

    /**
     * @var string $SZGILTBIS
     */
    protected $SZGILTBIS = null;

    /**
     * @var string $SZLICENSEPLATE
     */
    protected $SZLICENSEPLATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCUSTOMERACCOUNTNR()
    {
      return $this->NCUSTOMERACCOUNTNR;
    }

    /**
     * @param float $NCUSTOMERACCOUNTNR
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATERESULT
     */
    public function setNCUSTOMERACCOUNTNR($NCUSTOMERACCOUNTNR)
    {
      $this->NCUSTOMERACCOUNTNR = $NCUSTOMERACCOUNTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPROJNR()
    {
      return $this->NCUSTPROJNR;
    }

    /**
     * @param float $NCUSTPROJNR
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATERESULT
     */
    public function setNCUSTPROJNR($NCUSTPROJNR)
    {
      $this->NCUSTPROJNR = $NCUSTPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATERESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDNR()
    {
      return $this->NLFDNR;
    }

    /**
     * @param float $NLFDNR
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATERESULT
     */
    public function setNLFDNR($NLFDNR)
    {
      $this->NLFDNR = $NLFDNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSKASSANR()
    {
      return $this->NPERSKASSANR;
    }

    /**
     * @param float $NPERSKASSANR
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATERESULT
     */
    public function setNPERSKASSANR($NPERSKASSANR)
    {
      $this->NPERSKASSANR = $NPERSKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNR()
    {
      return $this->NPERSNR;
    }

    /**
     * @param float $NPERSNR
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATERESULT
     */
    public function setNPERSNR($NPERSNR)
    {
      $this->NPERSNR = $NPERSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNR()
    {
      return $this->NPERSPROJNR;
    }

    /**
     * @param float $NPERSPROJNR
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATERESULT
     */
    public function setNPERSPROJNR($NPERSPROJNR)
    {
      $this->NPERSPROJNR = $NPERSPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATERESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGILTAB()
    {
      return $this->SZGILTAB;
    }

    /**
     * @param string $SZGILTAB
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATERESULT
     */
    public function setSZGILTAB($SZGILTAB)
    {
      $this->SZGILTAB = $SZGILTAB;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGILTBIS()
    {
      return $this->SZGILTBIS;
    }

    /**
     * @param string $SZGILTBIS
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATERESULT
     */
    public function setSZGILTBIS($SZGILTBIS)
    {
      $this->SZGILTBIS = $SZGILTBIS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLICENSEPLATE()
    {
      return $this->SZLICENSEPLATE;
    }

    /**
     * @param string $SZLICENSEPLATE
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATERESULT
     */
    public function setSZLICENSEPLATE($SZLICENSEPLATE)
    {
      $this->SZLICENSEPLATE = $SZLICENSEPLATE;
      return $this;
    }

}
