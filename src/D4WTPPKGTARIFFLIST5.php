<?php

namespace Axess\Dci4Wtp;

class D4WTPPKGTARIFFLIST5
{

    /**
     * @var ArrayOfD4WTPPKGTARIFFLISTDAY5 $ACTPKGTARIFFLISTDAY
     */
    protected $ACTPKGTARIFFLISTDAY = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPKGTARIFFLISTDAY5
     */
    public function getACTPKGTARIFFLISTDAY()
    {
      return $this->ACTPKGTARIFFLISTDAY;
    }

    /**
     * @param ArrayOfD4WTPPKGTARIFFLISTDAY5 $ACTPKGTARIFFLISTDAY
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST5
     */
    public function setACTPKGTARIFFLISTDAY($ACTPKGTARIFFLISTDAY)
    {
      $this->ACTPKGTARIFFLISTDAY = $ACTPKGTARIFFLISTDAY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST5
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
