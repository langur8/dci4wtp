<?php

namespace Axess\Dci4Wtp;

class DoSplitPaymentResponse
{

    /**
     * @var D4WTPSPLITPAYMENTRESULT $DoSplitPaymentResult
     */
    protected $DoSplitPaymentResult = null;

    /**
     * @param D4WTPSPLITPAYMENTRESULT $DoSplitPaymentResult
     */
    public function __construct($DoSplitPaymentResult)
    {
      $this->DoSplitPaymentResult = $DoSplitPaymentResult;
    }

    /**
     * @return D4WTPSPLITPAYMENTRESULT
     */
    public function getDoSplitPaymentResult()
    {
      return $this->DoSplitPaymentResult;
    }

    /**
     * @param D4WTPSPLITPAYMENTRESULT $DoSplitPaymentResult
     * @return \Axess\Dci4Wtp\DoSplitPaymentResponse
     */
    public function setDoSplitPaymentResult($DoSplitPaymentResult)
    {
      $this->DoSplitPaymentResult = $DoSplitPaymentResult;
      return $this;
    }

}
