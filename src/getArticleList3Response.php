<?php

namespace Axess\Dci4Wtp;

class getArticleList3Response
{

    /**
     * @var D4WTPGETARTICLELIST3RESULT $getArticleList3Result
     */
    protected $getArticleList3Result = null;

    /**
     * @param D4WTPGETARTICLELIST3RESULT $getArticleList3Result
     */
    public function __construct($getArticleList3Result)
    {
      $this->getArticleList3Result = $getArticleList3Result;
    }

    /**
     * @return D4WTPGETARTICLELIST3RESULT
     */
    public function getGetArticleList3Result()
    {
      return $this->getArticleList3Result;
    }

    /**
     * @param D4WTPGETARTICLELIST3RESULT $getArticleList3Result
     * @return \Axess\Dci4Wtp\getArticleList3Response
     */
    public function setGetArticleList3Result($getArticleList3Result)
    {
      $this->getArticleList3Result = $getArticleList3Result;
      return $this;
    }

}
