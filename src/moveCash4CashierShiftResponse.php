<?php

namespace Axess\Dci4Wtp;

class moveCash4CashierShiftResponse
{

    /**
     * @var D4WTPMOVCASH4CASHIERSHIFTRES $moveCash4CashierShiftResult
     */
    protected $moveCash4CashierShiftResult = null;

    /**
     * @param D4WTPMOVCASH4CASHIERSHIFTRES $moveCash4CashierShiftResult
     */
    public function __construct($moveCash4CashierShiftResult)
    {
      $this->moveCash4CashierShiftResult = $moveCash4CashierShiftResult;
    }

    /**
     * @return D4WTPMOVCASH4CASHIERSHIFTRES
     */
    public function getMoveCash4CashierShiftResult()
    {
      return $this->moveCash4CashierShiftResult;
    }

    /**
     * @param D4WTPMOVCASH4CASHIERSHIFTRES $moveCash4CashierShiftResult
     * @return \Axess\Dci4Wtp\moveCash4CashierShiftResponse
     */
    public function setMoveCash4CashierShiftResult($moveCash4CashierShiftResult)
    {
      $this->moveCash4CashierShiftResult = $moveCash4CashierShiftResult;
      return $this;
    }

}
