<?php

namespace Axess\Dci4Wtp;

class D4WTPREPAIDRESULT2
{

    /**
     * @var ArrayOfD4WTPPREPAIDTICKET2 $ACTPREPAIDTICKET2
     */
    protected $ACTPREPAIDTICKET2 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPREPAIDTICKET2
     */
    public function getACTPREPAIDTICKET2()
    {
      return $this->ACTPREPAIDTICKET2;
    }

    /**
     * @param ArrayOfD4WTPPREPAIDTICKET2 $ACTPREPAIDTICKET2
     * @return \Axess\Dci4Wtp\D4WTPREPAIDRESULT2
     */
    public function setACTPREPAIDTICKET2($ACTPREPAIDTICKET2)
    {
      $this->ACTPREPAIDTICKET2 = $ACTPREPAIDTICKET2;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPREPAIDRESULT2
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPREPAIDRESULT2
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
