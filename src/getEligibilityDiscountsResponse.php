<?php

namespace Axess\Dci4Wtp;

class getEligibilityDiscountsResponse
{

    /**
     * @var D4WTPGETELIGIBILITYDISCRES $getEligibilityDiscountsResult
     */
    protected $getEligibilityDiscountsResult = null;

    /**
     * @param D4WTPGETELIGIBILITYDISCRES $getEligibilityDiscountsResult
     */
    public function __construct($getEligibilityDiscountsResult)
    {
      $this->getEligibilityDiscountsResult = $getEligibilityDiscountsResult;
    }

    /**
     * @return D4WTPGETELIGIBILITYDISCRES
     */
    public function getGetEligibilityDiscountsResult()
    {
      return $this->getEligibilityDiscountsResult;
    }

    /**
     * @param D4WTPGETELIGIBILITYDISCRES $getEligibilityDiscountsResult
     * @return \Axess\Dci4Wtp\getEligibilityDiscountsResponse
     */
    public function setGetEligibilityDiscountsResult($getEligibilityDiscountsResult)
    {
      $this->getEligibilityDiscountsResult = $getEligibilityDiscountsResult;
      return $this;
    }

}
