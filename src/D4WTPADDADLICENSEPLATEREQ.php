<?php

namespace Axess\Dci4Wtp;

class D4WTPADDADLICENSEPLATEREQ
{

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZFIRSTLICENSEPLATE
     */
    protected $SZFIRSTLICENSEPLATE = null;

    /**
     * @var string $SZFLAG
     */
    protected $SZFLAG = null;

    /**
     * @var string $SZINFO
     */
    protected $SZINFO = null;

    /**
     * @var string $SZNEWLICENSEPLATE
     */
    protected $SZNEWLICENSEPLATE = null;

    /**
     * @var string $SZNEWVALIDFROM
     */
    protected $SZNEWVALIDFROM = null;

    /**
     * @var string $SZNEWVALIDTO
     */
    protected $SZNEWVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPADDADLICENSEPLATEREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTLICENSEPLATE()
    {
      return $this->SZFIRSTLICENSEPLATE;
    }

    /**
     * @param string $SZFIRSTLICENSEPLATE
     * @return \Axess\Dci4Wtp\D4WTPADDADLICENSEPLATEREQ
     */
    public function setSZFIRSTLICENSEPLATE($SZFIRSTLICENSEPLATE)
    {
      $this->SZFIRSTLICENSEPLATE = $SZFIRSTLICENSEPLATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFLAG()
    {
      return $this->SZFLAG;
    }

    /**
     * @param string $SZFLAG
     * @return \Axess\Dci4Wtp\D4WTPADDADLICENSEPLATEREQ
     */
    public function setSZFLAG($SZFLAG)
    {
      $this->SZFLAG = $SZFLAG;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZINFO()
    {
      return $this->SZINFO;
    }

    /**
     * @param string $SZINFO
     * @return \Axess\Dci4Wtp\D4WTPADDADLICENSEPLATEREQ
     */
    public function setSZINFO($SZINFO)
    {
      $this->SZINFO = $SZINFO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNEWLICENSEPLATE()
    {
      return $this->SZNEWLICENSEPLATE;
    }

    /**
     * @param string $SZNEWLICENSEPLATE
     * @return \Axess\Dci4Wtp\D4WTPADDADLICENSEPLATEREQ
     */
    public function setSZNEWLICENSEPLATE($SZNEWLICENSEPLATE)
    {
      $this->SZNEWLICENSEPLATE = $SZNEWLICENSEPLATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNEWVALIDFROM()
    {
      return $this->SZNEWVALIDFROM;
    }

    /**
     * @param string $SZNEWVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPADDADLICENSEPLATEREQ
     */
    public function setSZNEWVALIDFROM($SZNEWVALIDFROM)
    {
      $this->SZNEWVALIDFROM = $SZNEWVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNEWVALIDTO()
    {
      return $this->SZNEWVALIDTO;
    }

    /**
     * @param string $SZNEWVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPADDADLICENSEPLATEREQ
     */
    public function setSZNEWVALIDTO($SZNEWVALIDTO)
    {
      $this->SZNEWVALIDTO = $SZNEWVALIDTO;
      return $this;
    }

}
