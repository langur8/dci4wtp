<?php

namespace Axess\Dci4Wtp;

class changePersData2
{

    /**
     * @var D4WTPCHANGEPERSDATA2REQUEST $i_ctChangePersDataReq
     */
    protected $i_ctChangePersDataReq = null;

    /**
     * @param D4WTPCHANGEPERSDATA2REQUEST $i_ctChangePersDataReq
     */
    public function __construct($i_ctChangePersDataReq)
    {
      $this->i_ctChangePersDataReq = $i_ctChangePersDataReq;
    }

    /**
     * @return D4WTPCHANGEPERSDATA2REQUEST
     */
    public function getI_ctChangePersDataReq()
    {
      return $this->i_ctChangePersDataReq;
    }

    /**
     * @param D4WTPCHANGEPERSDATA2REQUEST $i_ctChangePersDataReq
     * @return \Axess\Dci4Wtp\changePersData2
     */
    public function setI_ctChangePersDataReq($i_ctChangePersDataReq)
    {
      $this->i_ctChangePersDataReq = $i_ctChangePersDataReq;
      return $this;
    }

}
