<?php

namespace Axess\Dci4Wtp;

class D4WTPPAYERDATA
{

    /**
     * @var float $NPAYERCOMPANYNO
     */
    protected $NPAYERCOMPANYNO = null;

    /**
     * @var float $NPAYERCOMPANYPOSNO
     */
    protected $NPAYERCOMPANYPOSNO = null;

    /**
     * @var float $NPAYERCOMPANYPROJNO
     */
    protected $NPAYERCOMPANYPROJNO = null;

    /**
     * @var float $NPAYERPERSNO
     */
    protected $NPAYERPERSNO = null;

    /**
     * @var float $NPAYERPERSPOSNO
     */
    protected $NPAYERPERSPOSNO = null;

    /**
     * @var float $NPAYERPERSPROJNO
     */
    protected $NPAYERPERSPROJNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPAYERCOMPANYNO()
    {
      return $this->NPAYERCOMPANYNO;
    }

    /**
     * @param float $NPAYERCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPPAYERDATA
     */
    public function setNPAYERCOMPANYNO($NPAYERCOMPANYNO)
    {
      $this->NPAYERCOMPANYNO = $NPAYERCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERCOMPANYPOSNO()
    {
      return $this->NPAYERCOMPANYPOSNO;
    }

    /**
     * @param float $NPAYERCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPPAYERDATA
     */
    public function setNPAYERCOMPANYPOSNO($NPAYERCOMPANYPOSNO)
    {
      $this->NPAYERCOMPANYPOSNO = $NPAYERCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERCOMPANYPROJNO()
    {
      return $this->NPAYERCOMPANYPROJNO;
    }

    /**
     * @param float $NPAYERCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPPAYERDATA
     */
    public function setNPAYERCOMPANYPROJNO($NPAYERCOMPANYPROJNO)
    {
      $this->NPAYERCOMPANYPROJNO = $NPAYERCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERPERSNO()
    {
      return $this->NPAYERPERSNO;
    }

    /**
     * @param float $NPAYERPERSNO
     * @return \Axess\Dci4Wtp\D4WTPPAYERDATA
     */
    public function setNPAYERPERSNO($NPAYERPERSNO)
    {
      $this->NPAYERPERSNO = $NPAYERPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERPERSPOSNO()
    {
      return $this->NPAYERPERSPOSNO;
    }

    /**
     * @param float $NPAYERPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPPAYERDATA
     */
    public function setNPAYERPERSPOSNO($NPAYERPERSPOSNO)
    {
      $this->NPAYERPERSPOSNO = $NPAYERPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERPERSPROJNO()
    {
      return $this->NPAYERPERSPROJNO;
    }

    /**
     * @param float $NPAYERPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPPAYERDATA
     */
    public function setNPAYERPERSPROJNO($NPAYERPERSPROJNO)
    {
      $this->NPAYERPERSPROJNO = $NPAYERPERSPROJNO;
      return $this;
    }

}
