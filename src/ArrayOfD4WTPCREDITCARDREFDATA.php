<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCREDITCARDREFDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCREDITCARDREFDATA[] $D4WTPCREDITCARDREFDATA
     */
    protected $D4WTPCREDITCARDREFDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCREDITCARDREFDATA[]
     */
    public function getD4WTPCREDITCARDREFDATA()
    {
      return $this->D4WTPCREDITCARDREFDATA;
    }

    /**
     * @param D4WTPCREDITCARDREFDATA[] $D4WTPCREDITCARDREFDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCREDITCARDREFDATA
     */
    public function setD4WTPCREDITCARDREFDATA(array $D4WTPCREDITCARDREFDATA = null)
    {
      $this->D4WTPCREDITCARDREFDATA = $D4WTPCREDITCARDREFDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCREDITCARDREFDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCREDITCARDREFDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCREDITCARDREFDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCREDITCARDREFDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCREDITCARDREFDATA[] = $value;
      } else {
        $this->D4WTPCREDITCARDREFDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCREDITCARDREFDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCREDITCARDREFDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCREDITCARDREFDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCREDITCARDREFDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCREDITCARDREFDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCREDITCARDREFDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCREDITCARDREFDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCREDITCARDREFDATA);
    }

}
