<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPRENTALSALE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPRENTALSALE[] $D4WTPRENTALSALE
     */
    protected $D4WTPRENTALSALE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPRENTALSALE[]
     */
    public function getD4WTPRENTALSALE()
    {
      return $this->D4WTPRENTALSALE;
    }

    /**
     * @param D4WTPRENTALSALE[] $D4WTPRENTALSALE
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPRENTALSALE
     */
    public function setD4WTPRENTALSALE(array $D4WTPRENTALSALE = null)
    {
      $this->D4WTPRENTALSALE = $D4WTPRENTALSALE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPRENTALSALE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPRENTALSALE
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPRENTALSALE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPRENTALSALE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPRENTALSALE[] = $value;
      } else {
        $this->D4WTPRENTALSALE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPRENTALSALE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPRENTALSALE Return the current element
     */
    public function current()
    {
      return current($this->D4WTPRENTALSALE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPRENTALSALE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPRENTALSALE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPRENTALSALE);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPRENTALSALE Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPRENTALSALE);
    }

}
