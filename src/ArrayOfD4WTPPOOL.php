<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPOOL implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPOOL[] $D4WTPPOOL
     */
    protected $D4WTPPOOL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPOOL[]
     */
    public function getD4WTPPOOL()
    {
      return $this->D4WTPPOOL;
    }

    /**
     * @param D4WTPPOOL[] $D4WTPPOOL
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPOOL
     */
    public function setD4WTPPOOL(array $D4WTPPOOL = null)
    {
      $this->D4WTPPOOL = $D4WTPPOOL;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPOOL[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPOOL
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPOOL[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPOOL $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPOOL[] = $value;
      } else {
        $this->D4WTPPOOL[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPOOL[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPOOL Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPOOL);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPOOL);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPOOL);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPOOL);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPOOL Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPOOL);
    }

}
