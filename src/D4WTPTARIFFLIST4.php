<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST4
{

    /**
     * @var ArrayOfD4WTPTARIFFLISTDAY4 $ACTTARIFFLISTDAY4
     */
    protected $ACTTARIFFLISTDAY4 = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLISTDAY4
     */
    public function getACTTARIFFLISTDAY4()
    {
      return $this->ACTTARIFFLISTDAY4;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLISTDAY4 $ACTTARIFFLISTDAY4
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST4
     */
    public function setACTTARIFFLISTDAY4($ACTTARIFFLISTDAY4)
    {
      $this->ACTTARIFFLISTDAY4 = $ACTTARIFFLISTDAY4;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST4
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
