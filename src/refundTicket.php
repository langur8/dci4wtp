<?php

namespace Axess\Dci4Wtp;

class refundTicket
{

    /**
     * @var D4WTPREFUNDTICKETREQUEST $i_ctRefundTicketReq
     */
    protected $i_ctRefundTicketReq = null;

    /**
     * @param D4WTPREFUNDTICKETREQUEST $i_ctRefundTicketReq
     */
    public function __construct($i_ctRefundTicketReq)
    {
      $this->i_ctRefundTicketReq = $i_ctRefundTicketReq;
    }

    /**
     * @return D4WTPREFUNDTICKETREQUEST
     */
    public function getI_ctRefundTicketReq()
    {
      return $this->i_ctRefundTicketReq;
    }

    /**
     * @param D4WTPREFUNDTICKETREQUEST $i_ctRefundTicketReq
     * @return \Axess\Dci4Wtp\refundTicket
     */
    public function setI_ctRefundTicketReq($i_ctRefundTicketReq)
    {
      $this->i_ctRefundTicketReq = $i_ctRefundTicketReq;
      return $this;
    }

}
