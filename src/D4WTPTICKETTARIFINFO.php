<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETTARIFINFO
{

    /**
     * @var ArrayOfD4WTPCANCELARTICLE2RESULT $ACTCANCELARTICLERESULT
     */
    protected $ACTCANCELARTICLERESULT = null;

    /**
     * @var float $FTAXAMOUNT
     */
    protected $FTAXAMOUNT = null;

    /**
     * @var float $FTAXPERCENT
     */
    protected $FTAXPERCENT = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NNETTOTARIFF
     */
    protected $NNETTOTARIFF = null;

    /**
     * @var float $NPERSONTYPNO
     */
    protected $NPERSONTYPNO = null;

    /**
     * @var float $NPOOLNR
     */
    protected $NPOOLNR = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NREFUNDTARIF
     */
    protected $NREFUNDTARIF = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var string $SZPERSONTYPNAME
     */
    protected $SZPERSONTYPNAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZTAXNAME
     */
    protected $SZTAXNAME = null;

    /**
     * @var string $SZTICKETTYPNAME
     */
    protected $SZTICKETTYPNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCANCELARTICLE2RESULT
     */
    public function getACTCANCELARTICLERESULT()
    {
      return $this->ACTCANCELARTICLERESULT;
    }

    /**
     * @param ArrayOfD4WTPCANCELARTICLE2RESULT $ACTCANCELARTICLERESULT
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setACTCANCELARTICLERESULT($ACTCANCELARTICLERESULT)
    {
      $this->ACTCANCELARTICLERESULT = $ACTCANCELARTICLERESULT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFTAXAMOUNT()
    {
      return $this->FTAXAMOUNT;
    }

    /**
     * @param float $FTAXAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setFTAXAMOUNT($FTAXAMOUNT)
    {
      $this->FTAXAMOUNT = $FTAXAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFTAXPERCENT()
    {
      return $this->FTAXPERCENT;
    }

    /**
     * @param float $FTAXPERCENT
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setFTAXPERCENT($FTAXPERCENT)
    {
      $this->FTAXPERCENT = $FTAXPERCENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNETTOTARIFF()
    {
      return $this->NNETTOTARIFF;
    }

    /**
     * @param float $NNETTOTARIFF
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setNNETTOTARIFF($NNETTOTARIFF)
    {
      $this->NNETTOTARIFF = $NNETTOTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSONTYPNO()
    {
      return $this->NPERSONTYPNO;
    }

    /**
     * @param float $NPERSONTYPNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setNPERSONTYPNO($NPERSONTYPNO)
    {
      $this->NPERSONTYPNO = $NPERSONTYPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNR()
    {
      return $this->NPOOLNR;
    }

    /**
     * @param float $NPOOLNR
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setNPOOLNR($NPOOLNR)
    {
      $this->NPOOLNR = $NPOOLNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNREFUNDTARIF()
    {
      return $this->NREFUNDTARIF;
    }

    /**
     * @param float $NREFUNDTARIF
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setNREFUNDTARIF($NREFUNDTARIF)
    {
      $this->NREFUNDTARIF = $NREFUNDTARIF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSONTYPNAME()
    {
      return $this->SZPERSONTYPNAME;
    }

    /**
     * @param string $SZPERSONTYPNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setSZPERSONTYPNAME($SZPERSONTYPNAME)
    {
      $this->SZPERSONTYPNAME = $SZPERSONTYPNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTAXNAME()
    {
      return $this->SZTAXNAME;
    }

    /**
     * @param string $SZTAXNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setSZTAXNAME($SZTAXNAME)
    {
      $this->SZTAXNAME = $SZTAXNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPNAME()
    {
      return $this->SZTICKETTYPNAME;
    }

    /**
     * @param string $SZTICKETTYPNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETTARIFINFO
     */
    public function setSZTICKETTYPNAME($SZTICKETTYPNAME)
    {
      $this->SZTICKETTYPNAME = $SZTICKETTYPNAME;
      return $this;
    }

}
