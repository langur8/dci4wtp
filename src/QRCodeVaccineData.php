<?php

namespace Axess\Dci4Wtp;

class QRCodeVaccineData
{

    /**
     * @var string $CertId
     */
    protected $CertId = null;

    /**
     * @var string $CertIssuer
     */
    protected $CertIssuer = null;

    /**
     * @var string $CountryOfVaccination
     */
    protected $CountryOfVaccination = null;

    /**
     * @var string $DateOfVaccination
     */
    protected $DateOfVaccination = null;

    /**
     * @var int $TotalSeriesOfDoses
     */
    protected $TotalSeriesOfDoses = null;

    /**
     * @var string $Vaccine
     */
    protected $Vaccine = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCertId()
    {
      return $this->CertId;
    }

    /**
     * @param string $CertId
     * @return \Axess\Dci4Wtp\QRCodeVaccineData
     */
    public function setCertId($CertId)
    {
      $this->CertId = $CertId;
      return $this;
    }

    /**
     * @return string
     */
    public function getCertIssuer()
    {
      return $this->CertIssuer;
    }

    /**
     * @param string $CertIssuer
     * @return \Axess\Dci4Wtp\QRCodeVaccineData
     */
    public function setCertIssuer($CertIssuer)
    {
      $this->CertIssuer = $CertIssuer;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryOfVaccination()
    {
      return $this->CountryOfVaccination;
    }

    /**
     * @param string $CountryOfVaccination
     * @return \Axess\Dci4Wtp\QRCodeVaccineData
     */
    public function setCountryOfVaccination($CountryOfVaccination)
    {
      $this->CountryOfVaccination = $CountryOfVaccination;
      return $this;
    }

    /**
     * @return string
     */
    public function getDateOfVaccination()
    {
      return $this->DateOfVaccination;
    }

    /**
     * @param string $DateOfVaccination
     * @return \Axess\Dci4Wtp\QRCodeVaccineData
     */
    public function setDateOfVaccination($DateOfVaccination)
    {
      $this->DateOfVaccination = $DateOfVaccination;
      return $this;
    }

    /**
     * @return int
     */
    public function getTotalSeriesOfDoses()
    {
      return $this->TotalSeriesOfDoses;
    }

    /**
     * @param int $TotalSeriesOfDoses
     * @return \Axess\Dci4Wtp\QRCodeVaccineData
     */
    public function setTotalSeriesOfDoses($TotalSeriesOfDoses)
    {
      $this->TotalSeriesOfDoses = $TotalSeriesOfDoses;
      return $this;
    }

    /**
     * @return string
     */
    public function getVaccine()
    {
      return $this->Vaccine;
    }

    /**
     * @param string $Vaccine
     * @return \Axess\Dci4Wtp\QRCodeVaccineData
     */
    public function setVaccine($Vaccine)
    {
      $this->Vaccine = $Vaccine;
      return $this;
    }

}
