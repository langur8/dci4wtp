<?php

namespace Axess\Dci4Wtp;

class getWorkOrdersResponse
{

    /**
     * @var D4WTGETWORKORDERSRESULT $getWorkOrdersResult
     */
    protected $getWorkOrdersResult = null;

    /**
     * @param D4WTGETWORKORDERSRESULT $getWorkOrdersResult
     */
    public function __construct($getWorkOrdersResult)
    {
      $this->getWorkOrdersResult = $getWorkOrdersResult;
    }

    /**
     * @return D4WTGETWORKORDERSRESULT
     */
    public function getGetWorkOrdersResult()
    {
      return $this->getWorkOrdersResult;
    }

    /**
     * @param D4WTGETWORKORDERSRESULT $getWorkOrdersResult
     * @return \Axess\Dci4Wtp\getWorkOrdersResponse
     */
    public function setGetWorkOrdersResult($getWorkOrdersResult)
    {
      $this->getWorkOrdersResult = $getWorkOrdersResult;
      return $this;
    }

}
