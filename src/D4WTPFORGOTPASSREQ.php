<?php

namespace Axess\Dci4Wtp;

class D4WTPFORGOTPASSREQ
{

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NVALIDTITYINMINUTES
     */
    protected $NVALIDTITYINMINUTES = null;

    /**
     * @var string $SZCOMPANYLOGINID
     */
    protected $SZCOMPANYLOGINID = null;

    /**
     * @var string $SZPASSWORD
     */
    protected $SZPASSWORD = null;

    /**
     * @var string $SZTOKEN
     */
    protected $SZTOKEN = null;

    /**
     * @var string $SZUSERNAME
     */
    protected $SZUSERNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPFORGOTPASSREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVALIDTITYINMINUTES()
    {
      return $this->NVALIDTITYINMINUTES;
    }

    /**
     * @param float $NVALIDTITYINMINUTES
     * @return \Axess\Dci4Wtp\D4WTPFORGOTPASSREQ
     */
    public function setNVALIDTITYINMINUTES($NVALIDTITYINMINUTES)
    {
      $this->NVALIDTITYINMINUTES = $NVALIDTITYINMINUTES;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYLOGINID()
    {
      return $this->SZCOMPANYLOGINID;
    }

    /**
     * @param string $SZCOMPANYLOGINID
     * @return \Axess\Dci4Wtp\D4WTPFORGOTPASSREQ
     */
    public function setSZCOMPANYLOGINID($SZCOMPANYLOGINID)
    {
      $this->SZCOMPANYLOGINID = $SZCOMPANYLOGINID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPASSWORD()
    {
      return $this->SZPASSWORD;
    }

    /**
     * @param string $SZPASSWORD
     * @return \Axess\Dci4Wtp\D4WTPFORGOTPASSREQ
     */
    public function setSZPASSWORD($SZPASSWORD)
    {
      $this->SZPASSWORD = $SZPASSWORD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTOKEN()
    {
      return $this->SZTOKEN;
    }

    /**
     * @param string $SZTOKEN
     * @return \Axess\Dci4Wtp\D4WTPFORGOTPASSREQ
     */
    public function setSZTOKEN($SZTOKEN)
    {
      $this->SZTOKEN = $SZTOKEN;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUSERNAME()
    {
      return $this->SZUSERNAME;
    }

    /**
     * @param string $SZUSERNAME
     * @return \Axess\Dci4Wtp\D4WTPFORGOTPASSREQ
     */
    public function setSZUSERNAME($SZUSERNAME)
    {
      $this->SZUSERNAME = $SZUSERNAME;
      return $this;
    }

}
