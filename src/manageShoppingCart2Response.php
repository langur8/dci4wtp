<?php

namespace Axess\Dci4Wtp;

class manageShoppingCart2Response
{

    /**
     * @var D4WTPMANAGESHOPCARTRESULT $manageShoppingCart2Result
     */
    protected $manageShoppingCart2Result = null;

    /**
     * @param D4WTPMANAGESHOPCARTRESULT $manageShoppingCart2Result
     */
    public function __construct($manageShoppingCart2Result)
    {
      $this->manageShoppingCart2Result = $manageShoppingCart2Result;
    }

    /**
     * @return D4WTPMANAGESHOPCARTRESULT
     */
    public function getManageShoppingCart2Result()
    {
      return $this->manageShoppingCart2Result;
    }

    /**
     * @param D4WTPMANAGESHOPCARTRESULT $manageShoppingCart2Result
     * @return \Axess\Dci4Wtp\manageShoppingCart2Response
     */
    public function setManageShoppingCart2Result($manageShoppingCart2Result)
    {
      $this->manageShoppingCart2Result = $manageShoppingCart2Result;
      return $this;
    }

}
