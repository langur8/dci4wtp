<?php

namespace Axess\Dci4Wtp;

class WTPCheckAvailabilityResult
{

    /**
     * @var ArrayOfWTPAvailability $Availabilities
     */
    protected $Availabilities = null;

    /**
     * @var int $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var ArrayOfWTPAvailability $POEAvailabilities
     */
    protected $POEAvailabilities = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var boolean $bFeatureEnabled
     */
    protected $bFeatureEnabled = null;

    /**
     * @var ArrayOfint $tabKGTs
     */
    protected $tabKGTs = null;

    /**
     * @var ArrayOfWTPProductsToIgnore $tabProductsToIgnore
     */
    protected $tabProductsToIgnore = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfWTPAvailability
     */
    public function getAvailabilities()
    {
      return $this->Availabilities;
    }

    /**
     * @param ArrayOfWTPAvailability $Availabilities
     * @return \Axess\Dci4Wtp\WTPCheckAvailabilityResult
     */
    public function setAvailabilities($Availabilities)
    {
      $this->Availabilities = $Availabilities;
      return $this;
    }

    /**
     * @return int
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param int $NERRORNO
     * @return \Axess\Dci4Wtp\WTPCheckAvailabilityResult
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return ArrayOfWTPAvailability
     */
    public function getPOEAvailabilities()
    {
      return $this->POEAvailabilities;
    }

    /**
     * @param ArrayOfWTPAvailability $POEAvailabilities
     * @return \Axess\Dci4Wtp\WTPCheckAvailabilityResult
     */
    public function setPOEAvailabilities($POEAvailabilities)
    {
      $this->POEAvailabilities = $POEAvailabilities;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\WTPCheckAvailabilityResult
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBFeatureEnabled()
    {
      return $this->bFeatureEnabled;
    }

    /**
     * @param boolean $bFeatureEnabled
     * @return \Axess\Dci4Wtp\WTPCheckAvailabilityResult
     */
    public function setBFeatureEnabled($bFeatureEnabled)
    {
      $this->bFeatureEnabled = $bFeatureEnabled;
      return $this;
    }

    /**
     * @return ArrayOfint
     */
    public function getTabKGTs()
    {
      return $this->tabKGTs;
    }

    /**
     * @param ArrayOfint $tabKGTs
     * @return \Axess\Dci4Wtp\WTPCheckAvailabilityResult
     */
    public function setTabKGTs($tabKGTs)
    {
      $this->tabKGTs = $tabKGTs;
      return $this;
    }

    /**
     * @return ArrayOfWTPProductsToIgnore
     */
    public function getTabProductsToIgnore()
    {
      return $this->tabProductsToIgnore;
    }

    /**
     * @param ArrayOfWTPProductsToIgnore $tabProductsToIgnore
     * @return \Axess\Dci4Wtp\WTPCheckAvailabilityResult
     */
    public function setTabProductsToIgnore($tabProductsToIgnore)
    {
      $this->tabProductsToIgnore = $tabProductsToIgnore;
      return $this;
    }

}
