<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST9RESULT
{

    /**
     * @var ArrayOfD4WTPTARIFFLIST9 $ACTTARIFFLIST9
     */
    protected $ACTTARIFFLIST9 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLIST9
     */
    public function getACTTARIFFLIST9()
    {
      return $this->ACTTARIFFLIST9;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLIST9 $ACTTARIFFLIST9
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST9RESULT
     */
    public function setACTTARIFFLIST9($ACTTARIFFLIST9)
    {
      $this->ACTTARIFFLIST9 = $ACTTARIFFLIST9;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST9RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST9RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
