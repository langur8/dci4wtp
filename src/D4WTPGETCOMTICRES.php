<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCOMTICRES
{

    /**
     * @var ArrayOfD4WTPCOMPENSATIONTICKET $ACTCOMPENSATIONOTICKET
     */
    protected $ACTCOMPENSATIONOTICKET = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCOMPENSATIONTICKET
     */
    public function getACTCOMPENSATIONOTICKET()
    {
      return $this->ACTCOMPENSATIONOTICKET;
    }

    /**
     * @param ArrayOfD4WTPCOMPENSATIONTICKET $ACTCOMPENSATIONOTICKET
     * @return \Axess\Dci4Wtp\D4WTPGETCOMTICRES
     */
    public function setACTCOMPENSATIONOTICKET($ACTCOMPENSATIONOTICKET)
    {
      $this->ACTCOMPENSATIONOTICKET = $ACTCOMPENSATIONOTICKET;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETCOMTICRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETCOMTICRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
