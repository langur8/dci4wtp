<?php

namespace Axess\Dci4Wtp;

class getAttributes
{

    /**
     * @var D4WTPTRANSATTRIBUTESREQ $i_transAttributesReq
     */
    protected $i_transAttributesReq = null;

    /**
     * @param D4WTPTRANSATTRIBUTESREQ $i_transAttributesReq
     */
    public function __construct($i_transAttributesReq)
    {
      $this->i_transAttributesReq = $i_transAttributesReq;
    }

    /**
     * @return D4WTPTRANSATTRIBUTESREQ
     */
    public function getI_transAttributesReq()
    {
      return $this->i_transAttributesReq;
    }

    /**
     * @param D4WTPTRANSATTRIBUTESREQ $i_transAttributesReq
     * @return \Axess\Dci4Wtp\getAttributes
     */
    public function setI_transAttributesReq($i_transAttributesReq)
    {
      $this->i_transAttributesReq = $i_transAttributesReq;
      return $this;
    }

}
