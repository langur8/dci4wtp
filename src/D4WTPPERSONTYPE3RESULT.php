<?php

namespace Axess\Dci4Wtp;

class D4WTPPERSONTYPE3RESULT
{

    /**
     * @var ArrayOfD4WTPPERSONTYPE3 $ACTPERSONTYPES3
     */
    protected $ACTPERSONTYPES3 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPERSONTYPE3
     */
    public function getACTPERSONTYPES3()
    {
      return $this->ACTPERSONTYPES3;
    }

    /**
     * @param ArrayOfD4WTPPERSONTYPE3 $ACTPERSONTYPES3
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3RESULT
     */
    public function setACTPERSONTYPES3($ACTPERSONTYPES3)
    {
      $this->ACTPERSONTYPES3 = $ACTPERSONTYPES3;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
