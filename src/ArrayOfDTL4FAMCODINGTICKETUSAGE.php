<?php

namespace Axess\Dci4Wtp;

class ArrayOfDTL4FAMCODINGTICKETUSAGE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DTL4FAMCODINGTICKETUSAGE[] $DTL4FAMCODINGTICKETUSAGE
     */
    protected $DTL4FAMCODINGTICKETUSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DTL4FAMCODINGTICKETUSAGE[]
     */
    public function getDTL4FAMCODINGTICKETUSAGE()
    {
      return $this->DTL4FAMCODINGTICKETUSAGE;
    }

    /**
     * @param DTL4FAMCODINGTICKETUSAGE[] $DTL4FAMCODINGTICKETUSAGE
     * @return \Axess\Dci4Wtp\ArrayOfDTL4FAMCODINGTICKETUSAGE
     */
    public function setDTL4FAMCODINGTICKETUSAGE(array $DTL4FAMCODINGTICKETUSAGE = null)
    {
      $this->DTL4FAMCODINGTICKETUSAGE = $DTL4FAMCODINGTICKETUSAGE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DTL4FAMCODINGTICKETUSAGE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DTL4FAMCODINGTICKETUSAGE
     */
    public function offsetGet($offset)
    {
      return $this->DTL4FAMCODINGTICKETUSAGE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DTL4FAMCODINGTICKETUSAGE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DTL4FAMCODINGTICKETUSAGE[] = $value;
      } else {
        $this->DTL4FAMCODINGTICKETUSAGE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DTL4FAMCODINGTICKETUSAGE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DTL4FAMCODINGTICKETUSAGE Return the current element
     */
    public function current()
    {
      return current($this->DTL4FAMCODINGTICKETUSAGE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DTL4FAMCODINGTICKETUSAGE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DTL4FAMCODINGTICKETUSAGE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DTL4FAMCODINGTICKETUSAGE);
    }

    /**
     * Countable implementation
     *
     * @return DTL4FAMCODINGTICKETUSAGE Return count of elements
     */
    public function count()
    {
      return count($this->DTL4FAMCODINGTICKETUSAGE);
    }

}
