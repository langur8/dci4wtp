<?php

namespace Axess\Dci4Wtp;

class checkWTPNo2
{

    /**
     * @var D4WTPCHECKWTPNOREQUEST $i_ctCheckWTPNoReq
     */
    protected $i_ctCheckWTPNoReq = null;

    /**
     * @param D4WTPCHECKWTPNOREQUEST $i_ctCheckWTPNoReq
     */
    public function __construct($i_ctCheckWTPNoReq)
    {
      $this->i_ctCheckWTPNoReq = $i_ctCheckWTPNoReq;
    }

    /**
     * @return D4WTPCHECKWTPNOREQUEST
     */
    public function getI_ctCheckWTPNoReq()
    {
      return $this->i_ctCheckWTPNoReq;
    }

    /**
     * @param D4WTPCHECKWTPNOREQUEST $i_ctCheckWTPNoReq
     * @return \Axess\Dci4Wtp\checkWTPNo2
     */
    public function setI_ctCheckWTPNoReq($i_ctCheckWTPNoReq)
    {
      $this->i_ctCheckWTPNoReq = $i_ctCheckWTPNoReq;
      return $this;
    }

}
