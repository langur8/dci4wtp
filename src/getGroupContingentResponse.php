<?php

namespace Axess\Dci4Wtp;

class getGroupContingentResponse
{

    /**
     * @var D4WTPGETGRPCONTINGENTRESULT $getGroupContingentResult
     */
    protected $getGroupContingentResult = null;

    /**
     * @param D4WTPGETGRPCONTINGENTRESULT $getGroupContingentResult
     */
    public function __construct($getGroupContingentResult)
    {
      $this->getGroupContingentResult = $getGroupContingentResult;
    }

    /**
     * @return D4WTPGETGRPCONTINGENTRESULT
     */
    public function getGetGroupContingentResult()
    {
      return $this->getGroupContingentResult;
    }

    /**
     * @param D4WTPGETGRPCONTINGENTRESULT $getGroupContingentResult
     * @return \Axess\Dci4Wtp\getGroupContingentResponse
     */
    public function setGetGroupContingentResult($getGroupContingentResult)
    {
      $this->getGroupContingentResult = $getGroupContingentResult;
      return $this;
    }

}
