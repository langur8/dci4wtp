<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST5RESULT
{

    /**
     * @var ArrayOfD4WTPTARIFFLIST5 $ACTTARIFFLIST5
     */
    protected $ACTTARIFFLIST5 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLIST5
     */
    public function getACTTARIFFLIST5()
    {
      return $this->ACTTARIFFLIST5;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLIST5 $ACTTARIFFLIST5
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST5RESULT
     */
    public function setACTTARIFFLIST5($ACTTARIFFLIST5)
    {
      $this->ACTTARIFFLIST5 = $ACTTARIFFLIST5;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST5RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST5RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
