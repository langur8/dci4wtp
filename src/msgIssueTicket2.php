<?php

namespace Axess\Dci4Wtp;

class msgIssueTicket2
{

    /**
     * @var D4WTPMSGISSUETICKETREQUEST2 $i_ctMsgTicketReq
     */
    protected $i_ctMsgTicketReq = null;

    /**
     * @param D4WTPMSGISSUETICKETREQUEST2 $i_ctMsgTicketReq
     */
    public function __construct($i_ctMsgTicketReq)
    {
      $this->i_ctMsgTicketReq = $i_ctMsgTicketReq;
    }

    /**
     * @return D4WTPMSGISSUETICKETREQUEST2
     */
    public function getI_ctMsgTicketReq()
    {
      return $this->i_ctMsgTicketReq;
    }

    /**
     * @param D4WTPMSGISSUETICKETREQUEST2 $i_ctMsgTicketReq
     * @return \Axess\Dci4Wtp\msgIssueTicket2
     */
    public function setI_ctMsgTicketReq($i_ctMsgTicketReq)
    {
      $this->i_ctMsgTicketReq = $i_ctMsgTicketReq;
      return $this;
    }

}
