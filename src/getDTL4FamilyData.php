<?php

namespace Axess\Dci4Wtp;

class getDTL4FamilyData
{

    /**
     * @var DTL4FAMCODINGREQ $i_ctgetDTL4FamilyDataReq
     */
    protected $i_ctgetDTL4FamilyDataReq = null;

    /**
     * @param DTL4FAMCODINGREQ $i_ctgetDTL4FamilyDataReq
     */
    public function __construct($i_ctgetDTL4FamilyDataReq)
    {
      $this->i_ctgetDTL4FamilyDataReq = $i_ctgetDTL4FamilyDataReq;
    }

    /**
     * @return DTL4FAMCODINGREQ
     */
    public function getI_ctgetDTL4FamilyDataReq()
    {
      return $this->i_ctgetDTL4FamilyDataReq;
    }

    /**
     * @param DTL4FAMCODINGREQ $i_ctgetDTL4FamilyDataReq
     * @return \Axess\Dci4Wtp\getDTL4FamilyData
     */
    public function setI_ctgetDTL4FamilyDataReq($i_ctgetDTL4FamilyDataReq)
    {
      $this->i_ctgetDTL4FamilyDataReq = $i_ctgetDTL4FamilyDataReq;
      return $this;
    }

}
