<?php

namespace Axess\Dci4Wtp;

class D4WTPCUSTOMERPROFSETUPITEM
{

    /**
     * @var float $BVALUE
     */
    protected $BVALUE = null;

    /**
     * @var float $FVALUE
     */
    protected $FVALUE = null;

    /**
     * @var float $NDATATYPE
     */
    protected $NDATATYPE = null;

    /**
     * @var float $NSETUPITEMNR
     */
    protected $NSETUPITEMNR = null;

    /**
     * @var float $NVALUE
     */
    protected $NVALUE = null;

    /**
     * @var string $SZBESCH
     */
    protected $SZBESCH = null;

    /**
     * @var string $SZDATEVALUE
     */
    protected $SZDATEVALUE = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZVALUE
     */
    protected $SZVALUE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBVALUE()
    {
      return $this->BVALUE;
    }

    /**
     * @param float $BVALUE
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFSETUPITEM
     */
    public function setBVALUE($BVALUE)
    {
      $this->BVALUE = $BVALUE;
      return $this;
    }

    /**
     * @return float
     */
    public function getFVALUE()
    {
      return $this->FVALUE;
    }

    /**
     * @param float $FVALUE
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFSETUPITEM
     */
    public function setFVALUE($FVALUE)
    {
      $this->FVALUE = $FVALUE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATATYPE()
    {
      return $this->NDATATYPE;
    }

    /**
     * @param float $NDATATYPE
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFSETUPITEM
     */
    public function setNDATATYPE($NDATATYPE)
    {
      $this->NDATATYPE = $NDATATYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSETUPITEMNR()
    {
      return $this->NSETUPITEMNR;
    }

    /**
     * @param float $NSETUPITEMNR
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFSETUPITEM
     */
    public function setNSETUPITEMNR($NSETUPITEMNR)
    {
      $this->NSETUPITEMNR = $NSETUPITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVALUE()
    {
      return $this->NVALUE;
    }

    /**
     * @param float $NVALUE
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFSETUPITEM
     */
    public function setNVALUE($NVALUE)
    {
      $this->NVALUE = $NVALUE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBESCH()
    {
      return $this->SZBESCH;
    }

    /**
     * @param string $SZBESCH
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFSETUPITEM
     */
    public function setSZBESCH($SZBESCH)
    {
      $this->SZBESCH = $SZBESCH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEVALUE()
    {
      return $this->SZDATEVALUE;
    }

    /**
     * @param string $SZDATEVALUE
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFSETUPITEM
     */
    public function setSZDATEVALUE($SZDATEVALUE)
    {
      $this->SZDATEVALUE = $SZDATEVALUE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFSETUPITEM
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALUE()
    {
      return $this->SZVALUE;
    }

    /**
     * @param string $SZVALUE
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFSETUPITEM
     */
    public function setSZVALUE($SZVALUE)
    {
      $this->SZVALUE = $SZVALUE;
      return $this;
    }

}
