<?php

namespace Axess\Dci4Wtp;

class getPromptListResponse
{

    /**
     * @var D4WTPGETPROMPTLISTRESULT $getPromptListResult
     */
    protected $getPromptListResult = null;

    /**
     * @param D4WTPGETPROMPTLISTRESULT $getPromptListResult
     */
    public function __construct($getPromptListResult)
    {
      $this->getPromptListResult = $getPromptListResult;
    }

    /**
     * @return D4WTPGETPROMPTLISTRESULT
     */
    public function getGetPromptListResult()
    {
      return $this->getPromptListResult;
    }

    /**
     * @param D4WTPGETPROMPTLISTRESULT $getPromptListResult
     * @return \Axess\Dci4Wtp\getPromptListResponse
     */
    public function setGetPromptListResult($getPromptListResult)
    {
      $this->getPromptListResult = $getPromptListResult;
      return $this;
    }

}
