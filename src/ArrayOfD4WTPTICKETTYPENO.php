<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTICKETTYPENO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTICKETTYPENO[] $D4WTPTICKETTYPENO
     */
    protected $D4WTPTICKETTYPENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTICKETTYPENO[]
     */
    public function getD4WTPTICKETTYPENO()
    {
      return $this->D4WTPTICKETTYPENO;
    }

    /**
     * @param D4WTPTICKETTYPENO[] $D4WTPTICKETTYPENO
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTICKETTYPENO
     */
    public function setD4WTPTICKETTYPENO(array $D4WTPTICKETTYPENO = null)
    {
      $this->D4WTPTICKETTYPENO = $D4WTPTICKETTYPENO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTICKETTYPENO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTICKETTYPENO
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTICKETTYPENO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTICKETTYPENO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTICKETTYPENO[] = $value;
      } else {
        $this->D4WTPTICKETTYPENO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTICKETTYPENO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTICKETTYPENO Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTICKETTYPENO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTICKETTYPENO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTICKETTYPENO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTICKETTYPENO);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTICKETTYPENO Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTICKETTYPENO);
    }

}
