<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTICKETTARIFINFO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTICKETTARIFINFO[] $D4WTPTICKETTARIFINFO
     */
    protected $D4WTPTICKETTARIFINFO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTICKETTARIFINFO[]
     */
    public function getD4WTPTICKETTARIFINFO()
    {
      return $this->D4WTPTICKETTARIFINFO;
    }

    /**
     * @param D4WTPTICKETTARIFINFO[] $D4WTPTICKETTARIFINFO
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTICKETTARIFINFO
     */
    public function setD4WTPTICKETTARIFINFO(array $D4WTPTICKETTARIFINFO = null)
    {
      $this->D4WTPTICKETTARIFINFO = $D4WTPTICKETTARIFINFO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTICKETTARIFINFO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTICKETTARIFINFO
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTICKETTARIFINFO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTICKETTARIFINFO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTICKETTARIFINFO[] = $value;
      } else {
        $this->D4WTPTICKETTARIFINFO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTICKETTARIFINFO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTICKETTARIFINFO Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTICKETTARIFINFO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTICKETTARIFINFO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTICKETTARIFINFO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTICKETTARIFINFO);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTICKETTARIFINFO Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTICKETTARIFINFO);
    }

}
