<?php

namespace Axess\Dci4Wtp;

class ReleaseGlobalPrepaidTicketResponse
{

    /**
     * @var D4WTPRESULT $ReleaseGlobalPrepaidTicketResult
     */
    protected $ReleaseGlobalPrepaidTicketResult = null;

    /**
     * @param D4WTPRESULT $ReleaseGlobalPrepaidTicketResult
     */
    public function __construct($ReleaseGlobalPrepaidTicketResult)
    {
      $this->ReleaseGlobalPrepaidTicketResult = $ReleaseGlobalPrepaidTicketResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getReleaseGlobalPrepaidTicketResult()
    {
      return $this->ReleaseGlobalPrepaidTicketResult;
    }

    /**
     * @param D4WTPRESULT $ReleaseGlobalPrepaidTicketResult
     * @return \Axess\Dci4Wtp\ReleaseGlobalPrepaidTicketResponse
     */
    public function setReleaseGlobalPrepaidTicketResult($ReleaseGlobalPrepaidTicketResult)
    {
      $this->ReleaseGlobalPrepaidTicketResult = $ReleaseGlobalPrepaidTicketResult;
      return $this;
    }

}
