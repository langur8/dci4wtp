<?php

namespace Axess\Dci4Wtp;

class D4WTPPERSSALESDATARESULT
{

    /**
     * @var ArrayOfD4WTPPERSSALESDATA $ACTPERSSALESDATA
     */
    protected $ACTPERSSALESDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPERSSALESDATA
     */
    public function getACTPERSSALESDATA()
    {
      return $this->ACTPERSSALESDATA;
    }

    /**
     * @param ArrayOfD4WTPPERSSALESDATA $ACTPERSSALESDATA
     * @return \Axess\Dci4Wtp\D4WTPPERSSALESDATARESULT
     */
    public function setACTPERSSALESDATA($ACTPERSSALESDATA)
    {
      $this->ACTPERSSALESDATA = $ACTPERSSALESDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPERSSALESDATARESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPERSSALESDATARESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
