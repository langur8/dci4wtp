<?php

namespace Axess\Dci4Wtp;

class manageShoppingCartResponse
{

    /**
     * @var D4WTPMANAGESHOPCARTRESULT $manageShoppingCartResult
     */
    protected $manageShoppingCartResult = null;

    /**
     * @param D4WTPMANAGESHOPCARTRESULT $manageShoppingCartResult
     */
    public function __construct($manageShoppingCartResult)
    {
      $this->manageShoppingCartResult = $manageShoppingCartResult;
    }

    /**
     * @return D4WTPMANAGESHOPCARTRESULT
     */
    public function getManageShoppingCartResult()
    {
      return $this->manageShoppingCartResult;
    }

    /**
     * @param D4WTPMANAGESHOPCARTRESULT $manageShoppingCartResult
     * @return \Axess\Dci4Wtp\manageShoppingCartResponse
     */
    public function setManageShoppingCartResult($manageShoppingCartResult)
    {
      $this->manageShoppingCartResult = $manageShoppingCartResult;
      return $this;
    }

}
