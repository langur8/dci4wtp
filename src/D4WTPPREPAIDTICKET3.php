<?php

namespace Axess\Dci4Wtp;

class D4WTPPREPAIDTICKET3
{

    /**
     * @var ArrayOfD4WTPPREPAIDARTICLES $ACTPREPAIDARTICLES
     */
    protected $ACTPREPAIDARTICLES = null;

    /**
     * @var float $NCODINGCASHIERID
     */
    protected $NCODINGCASHIERID = null;

    /**
     * @var float $NCODINGPOSNR
     */
    protected $NCODINGPOSNR = null;

    /**
     * @var float $NCODINGPROJNR
     */
    protected $NCODINGPROJNR = null;

    /**
     * @var float $NDATACARRIERTYPE
     */
    protected $NDATACARRIERTYPE = null;

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var string $NKARTENNR
     */
    protected $NKARTENNR = null;

    /**
     * @var string $NMEDIAID
     */
    protected $NMEDIAID = null;

    /**
     * @var float $NNAMEDUSERID
     */
    protected $NNAMEDUSERID = null;

    /**
     * @var float $NORDERSTATUSNR
     */
    protected $NORDERSTATUSNR = null;

    /**
     * @var float $NPERSTYPENO
     */
    protected $NPERSTYPENO = null;

    /**
     * @var float $NPOOLNR
     */
    protected $NPOOLNR = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var D4WTPERSONDATA3 $PERSONDATA
     */
    protected $PERSONDATA = null;

    /**
     * @var string $SZCODINGCASHIERNAME
     */
    protected $SZCODINGCASHIERNAME = null;

    /**
     * @var string $SZCODINGDATE
     */
    protected $SZCODINGDATE = null;

    /**
     * @var string $SZCODINGPOSNAME
     */
    protected $SZCODINGPOSNAME = null;

    /**
     * @var string $SZEXTORDERNR
     */
    protected $SZEXTORDERNR = null;

    /**
     * @var string $SZEXTSERIALNO
     */
    protected $SZEXTSERIALNO = null;

    /**
     * @var string $SZEXTVOUCHERID
     */
    protected $SZEXTVOUCHERID = null;

    /**
     * @var string $SZINTERNALORDERNR
     */
    protected $SZINTERNALORDERNR = null;

    /**
     * @var string $SZORDERSTATUSNAME
     */
    protected $SZORDERSTATUSNAME = null;

    /**
     * @var string $SZPERSONTYPENAME
     */
    protected $SZPERSONTYPENAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZPRODTIMESTAMP
     */
    protected $SZPRODTIMESTAMP = null;

    /**
     * @var string $SZTICKETTYPENAME
     */
    protected $SZTICKETTYPENAME = null;

    /**
     * @var string $SZUPDATE
     */
    protected $SZUPDATE = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    /**
     * @var string $SZWTPNR32
     */
    protected $SZWTPNR32 = null;

    /**
     * @var string $SZWTPNR64
     */
    protected $SZWTPNR64 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPREPAIDARTICLES
     */
    public function getACTPREPAIDARTICLES()
    {
      return $this->ACTPREPAIDARTICLES;
    }

    /**
     * @param ArrayOfD4WTPPREPAIDARTICLES $ACTPREPAIDARTICLES
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setACTPREPAIDARTICLES($ACTPREPAIDARTICLES)
    {
      $this->ACTPREPAIDARTICLES = $ACTPREPAIDARTICLES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCODINGCASHIERID()
    {
      return $this->NCODINGCASHIERID;
    }

    /**
     * @param float $NCODINGCASHIERID
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNCODINGCASHIERID($NCODINGCASHIERID)
    {
      $this->NCODINGCASHIERID = $NCODINGCASHIERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCODINGPOSNR()
    {
      return $this->NCODINGPOSNR;
    }

    /**
     * @param float $NCODINGPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNCODINGPOSNR($NCODINGPOSNR)
    {
      $this->NCODINGPOSNR = $NCODINGPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCODINGPROJNR()
    {
      return $this->NCODINGPROJNR;
    }

    /**
     * @param float $NCODINGPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNCODINGPROJNR($NCODINGPROJNR)
    {
      $this->NCODINGPROJNR = $NCODINGPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATACARRIERTYPE()
    {
      return $this->NDATACARRIERTYPE;
    }

    /**
     * @param float $NDATACARRIERTYPE
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNDATACARRIERTYPE($NDATACARRIERTYPE)
    {
      $this->NDATACARRIERTYPE = $NDATACARRIERTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getNKARTENNR()
    {
      return $this->NKARTENNR;
    }

    /**
     * @param string $NKARTENNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNKARTENNR($NKARTENNR)
    {
      $this->NKARTENNR = $NKARTENNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getNMEDIAID()
    {
      return $this->NMEDIAID;
    }

    /**
     * @param string $NMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNMEDIAID($NMEDIAID)
    {
      $this->NMEDIAID = $NMEDIAID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNAMEDUSERID()
    {
      return $this->NNAMEDUSERID;
    }

    /**
     * @param float $NNAMEDUSERID
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNNAMEDUSERID($NNAMEDUSERID)
    {
      $this->NNAMEDUSERID = $NNAMEDUSERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORDERSTATUSNR()
    {
      return $this->NORDERSTATUSNR;
    }

    /**
     * @param float $NORDERSTATUSNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNORDERSTATUSNR($NORDERSTATUSNR)
    {
      $this->NORDERSTATUSNR = $NORDERSTATUSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSTYPENO()
    {
      return $this->NPERSTYPENO;
    }

    /**
     * @param float $NPERSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNPERSTYPENO($NPERSTYPENO)
    {
      $this->NPERSTYPENO = $NPERSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNR()
    {
      return $this->NPOOLNR;
    }

    /**
     * @param float $NPOOLNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNPOOLNR($NPOOLNR)
    {
      $this->NPOOLNR = $NPOOLNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return D4WTPERSONDATA3
     */
    public function getPERSONDATA()
    {
      return $this->PERSONDATA;
    }

    /**
     * @param D4WTPERSONDATA3 $PERSONDATA
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setPERSONDATA($PERSONDATA)
    {
      $this->PERSONDATA = $PERSONDATA;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGCASHIERNAME()
    {
      return $this->SZCODINGCASHIERNAME;
    }

    /**
     * @param string $SZCODINGCASHIERNAME
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZCODINGCASHIERNAME($SZCODINGCASHIERNAME)
    {
      $this->SZCODINGCASHIERNAME = $SZCODINGCASHIERNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGDATE()
    {
      return $this->SZCODINGDATE;
    }

    /**
     * @param string $SZCODINGDATE
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZCODINGDATE($SZCODINGDATE)
    {
      $this->SZCODINGDATE = $SZCODINGDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGPOSNAME()
    {
      return $this->SZCODINGPOSNAME;
    }

    /**
     * @param string $SZCODINGPOSNAME
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZCODINGPOSNAME($SZCODINGPOSNAME)
    {
      $this->SZCODINGPOSNAME = $SZCODINGPOSNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTORDERNR()
    {
      return $this->SZEXTORDERNR;
    }

    /**
     * @param string $SZEXTORDERNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZEXTORDERNR($SZEXTORDERNR)
    {
      $this->SZEXTORDERNR = $SZEXTORDERNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTSERIALNO()
    {
      return $this->SZEXTSERIALNO;
    }

    /**
     * @param string $SZEXTSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZEXTSERIALNO($SZEXTSERIALNO)
    {
      $this->SZEXTSERIALNO = $SZEXTSERIALNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTVOUCHERID()
    {
      return $this->SZEXTVOUCHERID;
    }

    /**
     * @param string $SZEXTVOUCHERID
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZEXTVOUCHERID($SZEXTVOUCHERID)
    {
      $this->SZEXTVOUCHERID = $SZEXTVOUCHERID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZINTERNALORDERNR()
    {
      return $this->SZINTERNALORDERNR;
    }

    /**
     * @param string $SZINTERNALORDERNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZINTERNALORDERNR($SZINTERNALORDERNR)
    {
      $this->SZINTERNALORDERNR = $SZINTERNALORDERNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZORDERSTATUSNAME()
    {
      return $this->SZORDERSTATUSNAME;
    }

    /**
     * @param string $SZORDERSTATUSNAME
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZORDERSTATUSNAME($SZORDERSTATUSNAME)
    {
      $this->SZORDERSTATUSNAME = $SZORDERSTATUSNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSONTYPENAME()
    {
      return $this->SZPERSONTYPENAME;
    }

    /**
     * @param string $SZPERSONTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZPERSONTYPENAME($SZPERSONTYPENAME)
    {
      $this->SZPERSONTYPENAME = $SZPERSONTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPRODTIMESTAMP()
    {
      return $this->SZPRODTIMESTAMP;
    }

    /**
     * @param string $SZPRODTIMESTAMP
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZPRODTIMESTAMP($SZPRODTIMESTAMP)
    {
      $this->SZPRODTIMESTAMP = $SZPRODTIMESTAMP;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPENAME()
    {
      return $this->SZTICKETTYPENAME;
    }

    /**
     * @param string $SZTICKETTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZTICKETTYPENAME($SZTICKETTYPENAME)
    {
      $this->SZTICKETTYPENAME = $SZTICKETTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUPDATE()
    {
      return $this->SZUPDATE;
    }

    /**
     * @param string $SZUPDATE
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZUPDATE($SZUPDATE)
    {
      $this->SZUPDATE = $SZUPDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNR32()
    {
      return $this->SZWTPNR32;
    }

    /**
     * @param string $SZWTPNR32
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZWTPNR32($SZWTPNR32)
    {
      $this->SZWTPNR32 = $SZWTPNR32;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNR64()
    {
      return $this->SZWTPNR64;
    }

    /**
     * @param string $SZWTPNR64
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDTICKET3
     */
    public function setSZWTPNR64($SZWTPNR64)
    {
      $this->SZWTPNR64 = $SZWTPNR64;
      return $this;
    }

}
