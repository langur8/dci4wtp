<?php

namespace Axess\Dci4Wtp;

class D4WTPRECEIPTPAYMENT
{

    /**
     * @var float $NAMOUNT
     */
    protected $NAMOUNT = null;

    /**
     * @var float $NBASPAYMENTTYPENO
     */
    protected $NBASPAYMENTTYPENO = null;

    /**
     * @var float $NCUSTPAYMENTTYPENO
     */
    protected $NCUSTPAYMENTTYPENO = null;

    /**
     * @var string $SZBASPAYMENTTYPENAME
     */
    protected $SZBASPAYMENTTYPENAME = null;

    /**
     * @var string $SZCUSTPAYMENTTYPENAME
     */
    protected $SZCUSTPAYMENTTYPENAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNAMOUNT()
    {
      return $this->NAMOUNT;
    }

    /**
     * @param float $NAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYMENT
     */
    public function setNAMOUNT($NAMOUNT)
    {
      $this->NAMOUNT = $NAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBASPAYMENTTYPENO()
    {
      return $this->NBASPAYMENTTYPENO;
    }

    /**
     * @param float $NBASPAYMENTTYPENO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYMENT
     */
    public function setNBASPAYMENTTYPENO($NBASPAYMENTTYPENO)
    {
      $this->NBASPAYMENTTYPENO = $NBASPAYMENTTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPAYMENTTYPENO()
    {
      return $this->NCUSTPAYMENTTYPENO;
    }

    /**
     * @param float $NCUSTPAYMENTTYPENO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYMENT
     */
    public function setNCUSTPAYMENTTYPENO($NCUSTPAYMENTTYPENO)
    {
      $this->NCUSTPAYMENTTYPENO = $NCUSTPAYMENTTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBASPAYMENTTYPENAME()
    {
      return $this->SZBASPAYMENTTYPENAME;
    }

    /**
     * @param string $SZBASPAYMENTTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYMENT
     */
    public function setSZBASPAYMENTTYPENAME($SZBASPAYMENTTYPENAME)
    {
      $this->SZBASPAYMENTTYPENAME = $SZBASPAYMENTTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTPAYMENTTYPENAME()
    {
      return $this->SZCUSTPAYMENTTYPENAME;
    }

    /**
     * @param string $SZCUSTPAYMENTTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYMENT
     */
    public function setSZCUSTPAYMENTTYPENAME($SZCUSTPAYMENTTYPENAME)
    {
      $this->SZCUSTPAYMENTTYPENAME = $SZCUSTPAYMENTTYPENAME;
      return $this;
    }

}
