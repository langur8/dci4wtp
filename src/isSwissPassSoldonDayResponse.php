<?php

namespace Axess\Dci4Wtp;

class isSwissPassSoldonDayResponse
{

    /**
     * @var D4WTPSWISSPASSSALESRES $isSwissPassSoldonDayResult
     */
    protected $isSwissPassSoldonDayResult = null;

    /**
     * @param D4WTPSWISSPASSSALESRES $isSwissPassSoldonDayResult
     */
    public function __construct($isSwissPassSoldonDayResult)
    {
      $this->isSwissPassSoldonDayResult = $isSwissPassSoldonDayResult;
    }

    /**
     * @return D4WTPSWISSPASSSALESRES
     */
    public function getIsSwissPassSoldonDayResult()
    {
      return $this->isSwissPassSoldonDayResult;
    }

    /**
     * @param D4WTPSWISSPASSSALESRES $isSwissPassSoldonDayResult
     * @return \Axess\Dci4Wtp\isSwissPassSoldonDayResponse
     */
    public function setIsSwissPassSoldonDayResult($isSwissPassSoldonDayResult)
    {
      $this->isSwissPassSoldonDayResult = $isSwissPassSoldonDayResult;
      return $this;
    }

}
