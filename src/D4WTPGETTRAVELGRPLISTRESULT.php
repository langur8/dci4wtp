<?php

namespace Axess\Dci4Wtp;

class D4WTPGETTRAVELGRPLISTRESULT
{

    /**
     * @var ArrayOfD4WTPTRAVELGROUPLIST $ACTTRAVELGROUPLIST
     */
    protected $ACTTRAVELGROUPLIST = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NROWCOUNT
     */
    protected $NROWCOUNT = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTRAVELGROUPLIST
     */
    public function getACTTRAVELGROUPLIST()
    {
      return $this->ACTTRAVELGROUPLIST;
    }

    /**
     * @param ArrayOfD4WTPTRAVELGROUPLIST $ACTTRAVELGROUPLIST
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTRESULT
     */
    public function setACTTRAVELGROUPLIST($ACTTRAVELGROUPLIST)
    {
      $this->ACTTRAVELGROUPLIST = $ACTTRAVELGROUPLIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNROWCOUNT()
    {
      return $this->NROWCOUNT;
    }

    /**
     * @param float $NROWCOUNT
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTRESULT
     */
    public function setNROWCOUNT($NROWCOUNT)
    {
      $this->NROWCOUNT = $NROWCOUNT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPLISTRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
