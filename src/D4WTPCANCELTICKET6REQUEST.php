<?php

namespace Axess\Dci4Wtp;

class D4WTPCANCELTICKET6REQUEST
{

    /**
     * @var ArrayOfDCI4WTPWARECANCEL $ACTWARECANCEL
     */
    protected $ACTWARECANCEL = null;

    /**
     * @var float $BADDINCIDENTSTORNO
     */
    protected $BADDINCIDENTSTORNO = null;

    /**
     * @var float $BCANCELALLTICKET
     */
    protected $BCANCELALLTICKET = null;

    /**
     * @var ArrayOfDCI4WTPLISTSERIALNO $LISTSERIALNO
     */
    protected $LISTSERIALNO = null;

    /**
     * @var float $NAPPTRANSLOGID
     */
    protected $NAPPTRANSLOGID = null;

    /**
     * @var float $NARTICLEBLOCKNO
     */
    protected $NARTICLEBLOCKNO = null;

    /**
     * @var float $NARTICLEPOSNO
     */
    protected $NARTICLEPOSNO = null;

    /**
     * @var float $NARTICLEPROJNO
     */
    protected $NARTICLEPROJNO = null;

    /**
     * @var float $NARTICLETRANSNO
     */
    protected $NARTICLETRANSNO = null;

    /**
     * @var float $NCANCELMODE
     */
    protected $NCANCELMODE = null;

    /**
     * @var float $NRENTALPOSNO
     */
    protected $NRENTALPOSNO = null;

    /**
     * @var float $NRENTALPROJNO
     */
    protected $NRENTALPROJNO = null;

    /**
     * @var float $NRENTALTRANSNO
     */
    protected $NRENTALTRANSNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTICKETPOSNO
     */
    protected $NTICKETPOSNO = null;

    /**
     * @var float $NTICKETPROJNO
     */
    protected $NTICKETPROJNO = null;

    /**
     * @var float $NTICKETSERIALNO
     */
    protected $NTICKETSERIALNO = null;

    /**
     * @var float $NTICKETTRANSNO
     */
    protected $NTICKETTRANSNO = null;

    /**
     * @var float $NTICKETUNICODENO
     */
    protected $NTICKETUNICODENO = null;

    /**
     * @var string $SZCODINGMODE
     */
    protected $SZCODINGMODE = null;

    /**
     * @var string $SZDCCONTENT
     */
    protected $SZDCCONTENT = null;

    /**
     * @var string $SZDRIVERTYPE
     */
    protected $SZDRIVERTYPE = null;

    /**
     * @var string $SZTICKETDATE
     */
    protected $SZTICKETDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfDCI4WTPWARECANCEL
     */
    public function getACTWARECANCEL()
    {
      return $this->ACTWARECANCEL;
    }

    /**
     * @param ArrayOfDCI4WTPWARECANCEL $ACTWARECANCEL
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setACTWARECANCEL($ACTWARECANCEL)
    {
      $this->ACTWARECANCEL = $ACTWARECANCEL;
      return $this;
    }

    /**
     * @return float
     */
    public function getBADDINCIDENTSTORNO()
    {
      return $this->BADDINCIDENTSTORNO;
    }

    /**
     * @param float $BADDINCIDENTSTORNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setBADDINCIDENTSTORNO($BADDINCIDENTSTORNO)
    {
      $this->BADDINCIDENTSTORNO = $BADDINCIDENTSTORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getBCANCELALLTICKET()
    {
      return $this->BCANCELALLTICKET;
    }

    /**
     * @param float $BCANCELALLTICKET
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setBCANCELALLTICKET($BCANCELALLTICKET)
    {
      $this->BCANCELALLTICKET = $BCANCELALLTICKET;
      return $this;
    }

    /**
     * @return ArrayOfDCI4WTPLISTSERIALNO
     */
    public function getLISTSERIALNO()
    {
      return $this->LISTSERIALNO;
    }

    /**
     * @param ArrayOfDCI4WTPLISTSERIALNO $LISTSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setLISTSERIALNO($LISTSERIALNO)
    {
      $this->LISTSERIALNO = $LISTSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAPPTRANSLOGID()
    {
      return $this->NAPPTRANSLOGID;
    }

    /**
     * @param float $NAPPTRANSLOGID
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNAPPTRANSLOGID($NAPPTRANSLOGID)
    {
      $this->NAPPTRANSLOGID = $NAPPTRANSLOGID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLEBLOCKNO()
    {
      return $this->NARTICLEBLOCKNO;
    }

    /**
     * @param float $NARTICLEBLOCKNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNARTICLEBLOCKNO($NARTICLEBLOCKNO)
    {
      $this->NARTICLEBLOCKNO = $NARTICLEBLOCKNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLEPOSNO()
    {
      return $this->NARTICLEPOSNO;
    }

    /**
     * @param float $NARTICLEPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNARTICLEPOSNO($NARTICLEPOSNO)
    {
      $this->NARTICLEPOSNO = $NARTICLEPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLEPROJNO()
    {
      return $this->NARTICLEPROJNO;
    }

    /**
     * @param float $NARTICLEPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNARTICLEPROJNO($NARTICLEPROJNO)
    {
      $this->NARTICLEPROJNO = $NARTICLEPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLETRANSNO()
    {
      return $this->NARTICLETRANSNO;
    }

    /**
     * @param float $NARTICLETRANSNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNARTICLETRANSNO($NARTICLETRANSNO)
    {
      $this->NARTICLETRANSNO = $NARTICLETRANSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCANCELMODE()
    {
      return $this->NCANCELMODE;
    }

    /**
     * @param float $NCANCELMODE
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNCANCELMODE($NCANCELMODE)
    {
      $this->NCANCELMODE = $NCANCELMODE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPOSNO()
    {
      return $this->NRENTALPOSNO;
    }

    /**
     * @param float $NRENTALPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNRENTALPOSNO($NRENTALPOSNO)
    {
      $this->NRENTALPOSNO = $NRENTALPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPROJNO()
    {
      return $this->NRENTALPROJNO;
    }

    /**
     * @param float $NRENTALPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNRENTALPROJNO($NRENTALPROJNO)
    {
      $this->NRENTALPROJNO = $NRENTALPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALTRANSNO()
    {
      return $this->NRENTALTRANSNO;
    }

    /**
     * @param float $NRENTALTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNRENTALTRANSNO($NRENTALTRANSNO)
    {
      $this->NRENTALTRANSNO = $NRENTALTRANSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETPOSNO()
    {
      return $this->NTICKETPOSNO;
    }

    /**
     * @param float $NTICKETPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNTICKETPOSNO($NTICKETPOSNO)
    {
      $this->NTICKETPOSNO = $NTICKETPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETPROJNO()
    {
      return $this->NTICKETPROJNO;
    }

    /**
     * @param float $NTICKETPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNTICKETPROJNO($NTICKETPROJNO)
    {
      $this->NTICKETPROJNO = $NTICKETPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETSERIALNO()
    {
      return $this->NTICKETSERIALNO;
    }

    /**
     * @param float $NTICKETSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNTICKETSERIALNO($NTICKETSERIALNO)
    {
      $this->NTICKETSERIALNO = $NTICKETSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTRANSNO()
    {
      return $this->NTICKETTRANSNO;
    }

    /**
     * @param float $NTICKETTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNTICKETTRANSNO($NTICKETTRANSNO)
    {
      $this->NTICKETTRANSNO = $NTICKETTRANSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETUNICODENO()
    {
      return $this->NTICKETUNICODENO;
    }

    /**
     * @param float $NTICKETUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setNTICKETUNICODENO($NTICKETUNICODENO)
    {
      $this->NTICKETUNICODENO = $NTICKETUNICODENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGMODE()
    {
      return $this->SZCODINGMODE;
    }

    /**
     * @param string $SZCODINGMODE
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setSZCODINGMODE($SZCODINGMODE)
    {
      $this->SZCODINGMODE = $SZCODINGMODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDCCONTENT()
    {
      return $this->SZDCCONTENT;
    }

    /**
     * @param string $SZDCCONTENT
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setSZDCCONTENT($SZDCCONTENT)
    {
      $this->SZDCCONTENT = $SZDCCONTENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDRIVERTYPE()
    {
      return $this->SZDRIVERTYPE;
    }

    /**
     * @param string $SZDRIVERTYPE
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setSZDRIVERTYPE($SZDRIVERTYPE)
    {
      $this->SZDRIVERTYPE = $SZDRIVERTYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETDATE()
    {
      return $this->SZTICKETDATE;
    }

    /**
     * @param string $SZTICKETDATE
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET6REQUEST
     */
    public function setSZTICKETDATE($SZTICKETDATE)
    {
      $this->SZTICKETDATE = $SZTICKETDATE;
      return $this;
    }

}
