<?php

namespace Axess\Dci4Wtp;

class getTicketRidesAndDrops
{

    /**
     * @var D4WTPGETTCKTRIDESDROPSREQ $i_ctGetTcktRidesDropsReq
     */
    protected $i_ctGetTcktRidesDropsReq = null;

    /**
     * @param D4WTPGETTCKTRIDESDROPSREQ $i_ctGetTcktRidesDropsReq
     */
    public function __construct($i_ctGetTcktRidesDropsReq)
    {
      $this->i_ctGetTcktRidesDropsReq = $i_ctGetTcktRidesDropsReq;
    }

    /**
     * @return D4WTPGETTCKTRIDESDROPSREQ
     */
    public function getI_ctGetTcktRidesDropsReq()
    {
      return $this->i_ctGetTcktRidesDropsReq;
    }

    /**
     * @param D4WTPGETTCKTRIDESDROPSREQ $i_ctGetTcktRidesDropsReq
     * @return \Axess\Dci4Wtp\getTicketRidesAndDrops
     */
    public function setI_ctGetTcktRidesDropsReq($i_ctGetTcktRidesDropsReq)
    {
      $this->i_ctGetTcktRidesDropsReq = $i_ctGetTcktRidesDropsReq;
      return $this;
    }

}
