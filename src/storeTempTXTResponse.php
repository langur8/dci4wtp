<?php

namespace Axess\Dci4Wtp;

class storeTempTXTResponse
{

    /**
     * @var D4WTPRESULT $storeTempTXTResult
     */
    protected $storeTempTXTResult = null;

    /**
     * @param D4WTPRESULT $storeTempTXTResult
     */
    public function __construct($storeTempTXTResult)
    {
      $this->storeTempTXTResult = $storeTempTXTResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getStoreTempTXTResult()
    {
      return $this->storeTempTXTResult;
    }

    /**
     * @param D4WTPRESULT $storeTempTXTResult
     * @return \Axess\Dci4Wtp\storeTempTXTResponse
     */
    public function setStoreTempTXTResult($storeTempTXTResult)
    {
      $this->storeTempTXTResult = $storeTempTXTResult;
      return $this;
    }

}
