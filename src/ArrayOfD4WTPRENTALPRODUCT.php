<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPRENTALPRODUCT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPRENTALPRODUCT[] $D4WTPRENTALPRODUCT
     */
    protected $D4WTPRENTALPRODUCT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPRENTALPRODUCT[]
     */
    public function getD4WTPRENTALPRODUCT()
    {
      return $this->D4WTPRENTALPRODUCT;
    }

    /**
     * @param D4WTPRENTALPRODUCT[] $D4WTPRENTALPRODUCT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPRENTALPRODUCT
     */
    public function setD4WTPRENTALPRODUCT(array $D4WTPRENTALPRODUCT = null)
    {
      $this->D4WTPRENTALPRODUCT = $D4WTPRENTALPRODUCT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPRENTALPRODUCT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPRENTALPRODUCT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPRENTALPRODUCT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPRENTALPRODUCT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPRENTALPRODUCT[] = $value;
      } else {
        $this->D4WTPRENTALPRODUCT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPRENTALPRODUCT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPRENTALPRODUCT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPRENTALPRODUCT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPRENTALPRODUCT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPRENTALPRODUCT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPRENTALPRODUCT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPRENTALPRODUCT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPRENTALPRODUCT);
    }

}
