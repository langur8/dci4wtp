<?php

namespace Axess\Dci4Wtp;

class D4WTPDAYOCCUPACY2
{

    /**
     * @var float $NFREECOUNT
     */
    protected $NFREECOUNT = null;

    /**
     * @var float $NRESERVEDCOUNT
     */
    protected $NRESERVEDCOUNT = null;

    /**
     * @var float $NRESERVEDPERPOSCOUNT
     */
    protected $NRESERVEDPERPOSCOUNT = null;

    /**
     * @var float $NSOLDCOUNT
     */
    protected $NSOLDCOUNT = null;

    /**
     * @var float $NSUBCONTINGENTNO
     */
    protected $NSUBCONTINGENTNO = null;

    /**
     * @var float $NTOTALCOUNT
     */
    protected $NTOTALCOUNT = null;

    /**
     * @var ArrayOfD4WTPOCCUPACYRESERVATIONS2 $RESERVATIONS
     */
    protected $RESERVATIONS = null;

    /**
     * @var ArrayOfD4WTPOCCUPACYSALES $SALES
     */
    protected $SALES = null;

    /**
     * @var string $SZSUBCONTINGENTNAME
     */
    protected $SZSUBCONTINGENTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNFREECOUNT()
    {
      return $this->NFREECOUNT;
    }

    /**
     * @param float $NFREECOUNT
     * @return \Axess\Dci4Wtp\D4WTPDAYOCCUPACY2
     */
    public function setNFREECOUNT($NFREECOUNT)
    {
      $this->NFREECOUNT = $NFREECOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRESERVEDCOUNT()
    {
      return $this->NRESERVEDCOUNT;
    }

    /**
     * @param float $NRESERVEDCOUNT
     * @return \Axess\Dci4Wtp\D4WTPDAYOCCUPACY2
     */
    public function setNRESERVEDCOUNT($NRESERVEDCOUNT)
    {
      $this->NRESERVEDCOUNT = $NRESERVEDCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRESERVEDPERPOSCOUNT()
    {
      return $this->NRESERVEDPERPOSCOUNT;
    }

    /**
     * @param float $NRESERVEDPERPOSCOUNT
     * @return \Axess\Dci4Wtp\D4WTPDAYOCCUPACY2
     */
    public function setNRESERVEDPERPOSCOUNT($NRESERVEDPERPOSCOUNT)
    {
      $this->NRESERVEDPERPOSCOUNT = $NRESERVEDPERPOSCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSOLDCOUNT()
    {
      return $this->NSOLDCOUNT;
    }

    /**
     * @param float $NSOLDCOUNT
     * @return \Axess\Dci4Wtp\D4WTPDAYOCCUPACY2
     */
    public function setNSOLDCOUNT($NSOLDCOUNT)
    {
      $this->NSOLDCOUNT = $NSOLDCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTINGENTNO()
    {
      return $this->NSUBCONTINGENTNO;
    }

    /**
     * @param float $NSUBCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPDAYOCCUPACY2
     */
    public function setNSUBCONTINGENTNO($NSUBCONTINGENTNO)
    {
      $this->NSUBCONTINGENTNO = $NSUBCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTOTALCOUNT()
    {
      return $this->NTOTALCOUNT;
    }

    /**
     * @param float $NTOTALCOUNT
     * @return \Axess\Dci4Wtp\D4WTPDAYOCCUPACY2
     */
    public function setNTOTALCOUNT($NTOTALCOUNT)
    {
      $this->NTOTALCOUNT = $NTOTALCOUNT;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPOCCUPACYRESERVATIONS2
     */
    public function getRESERVATIONS()
    {
      return $this->RESERVATIONS;
    }

    /**
     * @param ArrayOfD4WTPOCCUPACYRESERVATIONS2 $RESERVATIONS
     * @return \Axess\Dci4Wtp\D4WTPDAYOCCUPACY2
     */
    public function setRESERVATIONS($RESERVATIONS)
    {
      $this->RESERVATIONS = $RESERVATIONS;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPOCCUPACYSALES
     */
    public function getSALES()
    {
      return $this->SALES;
    }

    /**
     * @param ArrayOfD4WTPOCCUPACYSALES $SALES
     * @return \Axess\Dci4Wtp\D4WTPDAYOCCUPACY2
     */
    public function setSALES($SALES)
    {
      $this->SALES = $SALES;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBCONTINGENTNAME()
    {
      return $this->SZSUBCONTINGENTNAME;
    }

    /**
     * @param string $SZSUBCONTINGENTNAME
     * @return \Axess\Dci4Wtp\D4WTPDAYOCCUPACY2
     */
    public function setSZSUBCONTINGENTNAME($SZSUBCONTINGENTNAME)
    {
      $this->SZSUBCONTINGENTNAME = $SZSUBCONTINGENTNAME;
      return $this;
    }

}
