<?php

namespace Axess\Dci4Wtp;

class D4WTPCODINGDATA
{

    /**
     * @var float $NDWAMOUNT
     */
    protected $NDWAMOUNT = null;

    /**
     * @var float $NEXPIRYDATE
     */
    protected $NEXPIRYDATE = null;

    /**
     * @var float $NNULLINFOBIT
     */
    protected $NNULLINFOBIT = null;

    /**
     * @var string $SZIP2CODE
     */
    protected $SZIP2CODE = null;

    /**
     * @var string $SZPERMISSIONCODE
     */
    protected $SZPERMISSIONCODE = null;

    /**
     * @var string $SZSEGMENTS
     */
    protected $SZSEGMENTS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNDWAMOUNT()
    {
      return $this->NDWAMOUNT;
    }

    /**
     * @param float $NDWAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPCODINGDATA
     */
    public function setNDWAMOUNT($NDWAMOUNT)
    {
      $this->NDWAMOUNT = $NDWAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEXPIRYDATE()
    {
      return $this->NEXPIRYDATE;
    }

    /**
     * @param float $NEXPIRYDATE
     * @return \Axess\Dci4Wtp\D4WTPCODINGDATA
     */
    public function setNEXPIRYDATE($NEXPIRYDATE)
    {
      $this->NEXPIRYDATE = $NEXPIRYDATE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNULLINFOBIT()
    {
      return $this->NNULLINFOBIT;
    }

    /**
     * @param float $NNULLINFOBIT
     * @return \Axess\Dci4Wtp\D4WTPCODINGDATA
     */
    public function setNNULLINFOBIT($NNULLINFOBIT)
    {
      $this->NNULLINFOBIT = $NNULLINFOBIT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZIP2CODE()
    {
      return $this->SZIP2CODE;
    }

    /**
     * @param string $SZIP2CODE
     * @return \Axess\Dci4Wtp\D4WTPCODINGDATA
     */
    public function setSZIP2CODE($SZIP2CODE)
    {
      $this->SZIP2CODE = $SZIP2CODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERMISSIONCODE()
    {
      return $this->SZPERMISSIONCODE;
    }

    /**
     * @param string $SZPERMISSIONCODE
     * @return \Axess\Dci4Wtp\D4WTPCODINGDATA
     */
    public function setSZPERMISSIONCODE($SZPERMISSIONCODE)
    {
      $this->SZPERMISSIONCODE = $SZPERMISSIONCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSEGMENTS()
    {
      return $this->SZSEGMENTS;
    }

    /**
     * @param string $SZSEGMENTS
     * @return \Axess\Dci4Wtp\D4WTPCODINGDATA
     */
    public function setSZSEGMENTS($SZSEGMENTS)
    {
      $this->SZSEGMENTS = $SZSEGMENTS;
      return $this;
    }

}
