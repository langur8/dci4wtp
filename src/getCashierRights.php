<?php

namespace Axess\Dci4Wtp;

class getCashierRights
{

    /**
     * @var D4WTPCASHIERRIGHTSREQUEST $i_ctCashierRightsReq
     */
    protected $i_ctCashierRightsReq = null;

    /**
     * @param D4WTPCASHIERRIGHTSREQUEST $i_ctCashierRightsReq
     */
    public function __construct($i_ctCashierRightsReq)
    {
      $this->i_ctCashierRightsReq = $i_ctCashierRightsReq;
    }

    /**
     * @return D4WTPCASHIERRIGHTSREQUEST
     */
    public function getI_ctCashierRightsReq()
    {
      return $this->i_ctCashierRightsReq;
    }

    /**
     * @param D4WTPCASHIERRIGHTSREQUEST $i_ctCashierRightsReq
     * @return \Axess\Dci4Wtp\getCashierRights
     */
    public function setI_ctCashierRightsReq($i_ctCashierRightsReq)
    {
      $this->i_ctCashierRightsReq = $i_ctCashierRightsReq;
      return $this;
    }

}
