<?php

namespace Axess\Dci4Wtp;

class getARCDataResponse
{

    /**
     * @var D4WTPGETARCDATARES $getARCDataResult
     */
    protected $getARCDataResult = null;

    /**
     * @param D4WTPGETARCDATARES $getARCDataResult
     */
    public function __construct($getARCDataResult)
    {
      $this->getARCDataResult = $getARCDataResult;
    }

    /**
     * @return D4WTPGETARCDATARES
     */
    public function getGetARCDataResult()
    {
      return $this->getARCDataResult;
    }

    /**
     * @param D4WTPGETARCDATARES $getARCDataResult
     * @return \Axess\Dci4Wtp\getARCDataResponse
     */
    public function setGetARCDataResult($getARCDataResult)
    {
      $this->getARCDataResult = $getARCDataResult;
      return $this;
    }

}
