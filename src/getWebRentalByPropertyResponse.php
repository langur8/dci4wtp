<?php

namespace Axess\Dci4Wtp;

class getWebRentalByPropertyResponse
{

    /**
     * @var D4WTPWEBRENTALBYPROPERTYRES $getWebRentalByPropertyResult
     */
    protected $getWebRentalByPropertyResult = null;

    /**
     * @param D4WTPWEBRENTALBYPROPERTYRES $getWebRentalByPropertyResult
     */
    public function __construct($getWebRentalByPropertyResult)
    {
      $this->getWebRentalByPropertyResult = $getWebRentalByPropertyResult;
    }

    /**
     * @return D4WTPWEBRENTALBYPROPERTYRES
     */
    public function getGetWebRentalByPropertyResult()
    {
      return $this->getWebRentalByPropertyResult;
    }

    /**
     * @param D4WTPWEBRENTALBYPROPERTYRES $getWebRentalByPropertyResult
     * @return \Axess\Dci4Wtp\getWebRentalByPropertyResponse
     */
    public function setGetWebRentalByPropertyResult($getWebRentalByPropertyResult)
    {
      $this->getWebRentalByPropertyResult = $getWebRentalByPropertyResult;
      return $this;
    }

}
