<?php

namespace Axess\Dci4Wtp;

class isNF525TestModeEnabledResponse
{

    /**
     * @var D4WTPISTESTMODEENABLEDRESULT $isNF525TestModeEnabledResult
     */
    protected $isNF525TestModeEnabledResult = null;

    /**
     * @param D4WTPISTESTMODEENABLEDRESULT $isNF525TestModeEnabledResult
     */
    public function __construct($isNF525TestModeEnabledResult)
    {
      $this->isNF525TestModeEnabledResult = $isNF525TestModeEnabledResult;
    }

    /**
     * @return D4WTPISTESTMODEENABLEDRESULT
     */
    public function getIsNF525TestModeEnabledResult()
    {
      return $this->isNF525TestModeEnabledResult;
    }

    /**
     * @param D4WTPISTESTMODEENABLEDRESULT $isNF525TestModeEnabledResult
     * @return \Axess\Dci4Wtp\isNF525TestModeEnabledResponse
     */
    public function setIsNF525TestModeEnabledResult($isNF525TestModeEnabledResult)
    {
      $this->isNF525TestModeEnabledResult = $isNF525TestModeEnabledResult;
      return $this;
    }

}
