<?php

namespace Axess\Dci4Wtp;

class getContingentOccupacy2Response
{

    /**
     * @var D4WTPGETCONTINGENTOCUPACYRES $getContingentOccupacy2Result
     */
    protected $getContingentOccupacy2Result = null;

    /**
     * @param D4WTPGETCONTINGENTOCUPACYRES $getContingentOccupacy2Result
     */
    public function __construct($getContingentOccupacy2Result)
    {
      $this->getContingentOccupacy2Result = $getContingentOccupacy2Result;
    }

    /**
     * @return D4WTPGETCONTINGENTOCUPACYRES
     */
    public function getGetContingentOccupacy2Result()
    {
      return $this->getContingentOccupacy2Result;
    }

    /**
     * @param D4WTPGETCONTINGENTOCUPACYRES $getContingentOccupacy2Result
     * @return \Axess\Dci4Wtp\getContingentOccupacy2Response
     */
    public function setGetContingentOccupacy2Result($getContingentOccupacy2Result)
    {
      $this->getContingentOccupacy2Result = $getContingentOccupacy2Result;
      return $this;
    }

}
