<?php

namespace Axess\Dci4Wtp;

class D4WTPGROUPREQUEST
{

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var float $NCOMPANYNR
     */
    protected $NCOMPANYNR = null;

    /**
     * @var float $NCOMPANYPOSNR
     */
    protected $NCOMPANYPOSNR = null;

    /**
     * @var float $NCOMPANYPROJNR
     */
    protected $NCOMPANYPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZCITY
     */
    protected $SZCITY = null;

    /**
     * @var string $SZCO
     */
    protected $SZCO = null;

    /**
     * @var string $SZCOUNTRY
     */
    protected $SZCOUNTRY = null;

    /**
     * @var string $SZDATEOFBIRTH
     */
    protected $SZDATEOFBIRTH = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZFLAG
     */
    protected $SZFLAG = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZNAME1
     */
    protected $SZNAME1 = null;

    /**
     * @var string $SZNAME2
     */
    protected $SZNAME2 = null;

    /**
     * @var string $SZNAME3
     */
    protected $SZNAME3 = null;

    /**
     * @var string $SZSPECIALCODE
     */
    protected $SZSPECIALCODE = null;

    /**
     * @var string $SZSTREET1
     */
    protected $SZSTREET1 = null;

    /**
     * @var string $SZSTREET2
     */
    protected $SZSTREET2 = null;

    /**
     * @var string $SZSTREET3
     */
    protected $SZSTREET3 = null;

    /**
     * @var string $SZZIPCODE
     */
    protected $SZZIPCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNR()
    {
      return $this->NCOMPANYNR;
    }

    /**
     * @param float $NCOMPANYNR
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setNCOMPANYNR($NCOMPANYNR)
    {
      $this->NCOMPANYNR = $NCOMPANYNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNR()
    {
      return $this->NCOMPANYPOSNR;
    }

    /**
     * @param float $NCOMPANYPOSNR
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setNCOMPANYPOSNR($NCOMPANYPOSNR)
    {
      $this->NCOMPANYPOSNR = $NCOMPANYPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNR()
    {
      return $this->NCOMPANYPROJNR;
    }

    /**
     * @param float $NCOMPANYPROJNR
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setNCOMPANYPROJNR($NCOMPANYPROJNR)
    {
      $this->NCOMPANYPROJNR = $NCOMPANYPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCITY()
    {
      return $this->SZCITY;
    }

    /**
     * @param string $SZCITY
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZCITY($SZCITY)
    {
      $this->SZCITY = $SZCITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCO()
    {
      return $this->SZCO;
    }

    /**
     * @param string $SZCO
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZCO($SZCO)
    {
      $this->SZCO = $SZCO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRY()
    {
      return $this->SZCOUNTRY;
    }

    /**
     * @param string $SZCOUNTRY
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZCOUNTRY($SZCOUNTRY)
    {
      $this->SZCOUNTRY = $SZCOUNTRY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEOFBIRTH()
    {
      return $this->SZDATEOFBIRTH;
    }

    /**
     * @param string $SZDATEOFBIRTH
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZDATEOFBIRTH($SZDATEOFBIRTH)
    {
      $this->SZDATEOFBIRTH = $SZDATEOFBIRTH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFLAG()
    {
      return $this->SZFLAG;
    }

    /**
     * @param string $SZFLAG
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZFLAG($SZFLAG)
    {
      $this->SZFLAG = $SZFLAG;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME1()
    {
      return $this->SZNAME1;
    }

    /**
     * @param string $SZNAME1
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZNAME1($SZNAME1)
    {
      $this->SZNAME1 = $SZNAME1;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME2()
    {
      return $this->SZNAME2;
    }

    /**
     * @param string $SZNAME2
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZNAME2($SZNAME2)
    {
      $this->SZNAME2 = $SZNAME2;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME3()
    {
      return $this->SZNAME3;
    }

    /**
     * @param string $SZNAME3
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZNAME3($SZNAME3)
    {
      $this->SZNAME3 = $SZNAME3;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSPECIALCODE()
    {
      return $this->SZSPECIALCODE;
    }

    /**
     * @param string $SZSPECIALCODE
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZSPECIALCODE($SZSPECIALCODE)
    {
      $this->SZSPECIALCODE = $SZSPECIALCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREET1()
    {
      return $this->SZSTREET1;
    }

    /**
     * @param string $SZSTREET1
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZSTREET1($SZSTREET1)
    {
      $this->SZSTREET1 = $SZSTREET1;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREET2()
    {
      return $this->SZSTREET2;
    }

    /**
     * @param string $SZSTREET2
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZSTREET2($SZSTREET2)
    {
      $this->SZSTREET2 = $SZSTREET2;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREET3()
    {
      return $this->SZSTREET3;
    }

    /**
     * @param string $SZSTREET3
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZSTREET3($SZSTREET3)
    {
      $this->SZSTREET3 = $SZSTREET3;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZZIPCODE()
    {
      return $this->SZZIPCODE;
    }

    /**
     * @param string $SZZIPCODE
     * @return \Axess\Dci4Wtp\D4WTPGROUPREQUEST
     */
    public function setSZZIPCODE($SZZIPCODE)
    {
      $this->SZZIPCODE = $SZZIPCODE;
      return $this;
    }

}
