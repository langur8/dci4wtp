<?php

namespace Axess\Dci4Wtp;

class D4WTPPERSRENTAL
{

    /**
     * @var float $FHEIGHT
     */
    protected $FHEIGHT = null;

    /**
     * @var float $FSHOESIZE
     */
    protected $FSHOESIZE = null;

    /**
     * @var float $FSOLELENGTH
     */
    protected $FSOLELENGTH = null;

    /**
     * @var float $FWEIGHT
     */
    protected $FWEIGHT = null;

    /**
     * @var float $NHEIGHTUNIT
     */
    protected $NHEIGHTUNIT = null;

    /**
     * @var float $NSHOESIZEUNIT
     */
    protected $NSHOESIZEUNIT = null;

    /**
     * @var float $NSKILEVEL
     */
    protected $NSKILEVEL = null;

    /**
     * @var float $NSOLELENGTHUNIT
     */
    protected $NSOLELENGTHUNIT = null;

    /**
     * @var float $NWEIGHTUNIT
     */
    protected $NWEIGHTUNIT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFHEIGHT()
    {
      return $this->FHEIGHT;
    }

    /**
     * @param float $FHEIGHT
     * @return \Axess\Dci4Wtp\D4WTPPERSRENTAL
     */
    public function setFHEIGHT($FHEIGHT)
    {
      $this->FHEIGHT = $FHEIGHT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFSHOESIZE()
    {
      return $this->FSHOESIZE;
    }

    /**
     * @param float $FSHOESIZE
     * @return \Axess\Dci4Wtp\D4WTPPERSRENTAL
     */
    public function setFSHOESIZE($FSHOESIZE)
    {
      $this->FSHOESIZE = $FSHOESIZE;
      return $this;
    }

    /**
     * @return float
     */
    public function getFSOLELENGTH()
    {
      return $this->FSOLELENGTH;
    }

    /**
     * @param float $FSOLELENGTH
     * @return \Axess\Dci4Wtp\D4WTPPERSRENTAL
     */
    public function setFSOLELENGTH($FSOLELENGTH)
    {
      $this->FSOLELENGTH = $FSOLELENGTH;
      return $this;
    }

    /**
     * @return float
     */
    public function getFWEIGHT()
    {
      return $this->FWEIGHT;
    }

    /**
     * @param float $FWEIGHT
     * @return \Axess\Dci4Wtp\D4WTPPERSRENTAL
     */
    public function setFWEIGHT($FWEIGHT)
    {
      $this->FWEIGHT = $FWEIGHT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNHEIGHTUNIT()
    {
      return $this->NHEIGHTUNIT;
    }

    /**
     * @param float $NHEIGHTUNIT
     * @return \Axess\Dci4Wtp\D4WTPPERSRENTAL
     */
    public function setNHEIGHTUNIT($NHEIGHTUNIT)
    {
      $this->NHEIGHTUNIT = $NHEIGHTUNIT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOESIZEUNIT()
    {
      return $this->NSHOESIZEUNIT;
    }

    /**
     * @param float $NSHOESIZEUNIT
     * @return \Axess\Dci4Wtp\D4WTPPERSRENTAL
     */
    public function setNSHOESIZEUNIT($NSHOESIZEUNIT)
    {
      $this->NSHOESIZEUNIT = $NSHOESIZEUNIT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSKILEVEL()
    {
      return $this->NSKILEVEL;
    }

    /**
     * @param float $NSKILEVEL
     * @return \Axess\Dci4Wtp\D4WTPPERSRENTAL
     */
    public function setNSKILEVEL($NSKILEVEL)
    {
      $this->NSKILEVEL = $NSKILEVEL;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSOLELENGTHUNIT()
    {
      return $this->NSOLELENGTHUNIT;
    }

    /**
     * @param float $NSOLELENGTHUNIT
     * @return \Axess\Dci4Wtp\D4WTPPERSRENTAL
     */
    public function setNSOLELENGTHUNIT($NSOLELENGTHUNIT)
    {
      $this->NSOLELENGTHUNIT = $NSOLELENGTHUNIT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWEIGHTUNIT()
    {
      return $this->NWEIGHTUNIT;
    }

    /**
     * @param float $NWEIGHTUNIT
     * @return \Axess\Dci4Wtp\D4WTPPERSRENTAL
     */
    public function setNWEIGHTUNIT($NWEIGHTUNIT)
    {
      $this->NWEIGHTUNIT = $NWEIGHTUNIT;
      return $this;
    }

}
