<?php

namespace Axess\Dci4Wtp;

class getCompanyInfo
{

    /**
     * @var D4WTPCOMPANYINFOREQUEST $i_ctCompInfoReq
     */
    protected $i_ctCompInfoReq = null;

    /**
     * @param D4WTPCOMPANYINFOREQUEST $i_ctCompInfoReq
     */
    public function __construct($i_ctCompInfoReq)
    {
      $this->i_ctCompInfoReq = $i_ctCompInfoReq;
    }

    /**
     * @return D4WTPCOMPANYINFOREQUEST
     */
    public function getI_ctCompInfoReq()
    {
      return $this->i_ctCompInfoReq;
    }

    /**
     * @param D4WTPCOMPANYINFOREQUEST $i_ctCompInfoReq
     * @return \Axess\Dci4Wtp\getCompanyInfo
     */
    public function setI_ctCompInfoReq($i_ctCompInfoReq)
    {
      $this->i_ctCompInfoReq = $i_ctCompInfoReq;
      return $this;
    }

}
