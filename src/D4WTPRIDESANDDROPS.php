<?php

namespace Axess\Dci4Wtp;

class D4WTPRIDESANDDROPS
{

    /**
     * @var float $NALTITUDE
     */
    protected $NALTITUDE = null;

    /**
     * @var float $NVERTICALFEET
     */
    protected $NVERTICALFEET = null;

    /**
     * @var string $SZDATEOFRIDE
     */
    protected $SZDATEOFRIDE = null;

    /**
     * @var string $SZDURATIONOFRIDE
     */
    protected $SZDURATIONOFRIDE = null;

    /**
     * @var string $SZPOENAME
     */
    protected $SZPOENAME = null;

    /**
     * @var string $SZTIMEOFRIDE
     */
    protected $SZTIMEOFRIDE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNALTITUDE()
    {
      return $this->NALTITUDE;
    }

    /**
     * @param float $NALTITUDE
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS
     */
    public function setNALTITUDE($NALTITUDE)
    {
      $this->NALTITUDE = $NALTITUDE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVERTICALFEET()
    {
      return $this->NVERTICALFEET;
    }

    /**
     * @param float $NVERTICALFEET
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS
     */
    public function setNVERTICALFEET($NVERTICALFEET)
    {
      $this->NVERTICALFEET = $NVERTICALFEET;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEOFRIDE()
    {
      return $this->SZDATEOFRIDE;
    }

    /**
     * @param string $SZDATEOFRIDE
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS
     */
    public function setSZDATEOFRIDE($SZDATEOFRIDE)
    {
      $this->SZDATEOFRIDE = $SZDATEOFRIDE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDURATIONOFRIDE()
    {
      return $this->SZDURATIONOFRIDE;
    }

    /**
     * @param string $SZDURATIONOFRIDE
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS
     */
    public function setSZDURATIONOFRIDE($SZDURATIONOFRIDE)
    {
      $this->SZDURATIONOFRIDE = $SZDURATIONOFRIDE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOENAME()
    {
      return $this->SZPOENAME;
    }

    /**
     * @param string $SZPOENAME
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS
     */
    public function setSZPOENAME($SZPOENAME)
    {
      $this->SZPOENAME = $SZPOENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTIMEOFRIDE()
    {
      return $this->SZTIMEOFRIDE;
    }

    /**
     * @param string $SZTIMEOFRIDE
     * @return \Axess\Dci4Wtp\D4WTPRIDESANDDROPS
     */
    public function setSZTIMEOFRIDE($SZTIMEOFRIDE)
    {
      $this->SZTIMEOFRIDE = $SZTIMEOFRIDE;
      return $this;
    }

}
