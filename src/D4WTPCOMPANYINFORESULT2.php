<?php

namespace Axess\Dci4Wtp;

class D4WTPCOMPANYINFORESULT2
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZBOCNAME
     */
    protected $SZBOCNAME = null;

    /**
     * @var string $SZCITY
     */
    protected $SZCITY = null;

    /**
     * @var string $SZDVRNO
     */
    protected $SZDVRNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZFAXNO
     */
    protected $SZFAXNO = null;

    /**
     * @var string $SZNAF
     */
    protected $SZNAF = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZPHONENO
     */
    protected $SZPHONENO = null;

    /**
     * @var string $SZSTREET
     */
    protected $SZSTREET = null;

    /**
     * @var string $SZVATNO
     */
    protected $SZVATNO = null;

    /**
     * @var string $SZZIPCODE
     */
    protected $SZZIPCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYINFORESULT2
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBOCNAME()
    {
      return $this->SZBOCNAME;
    }

    /**
     * @param string $SZBOCNAME
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYINFORESULT2
     */
    public function setSZBOCNAME($SZBOCNAME)
    {
      $this->SZBOCNAME = $SZBOCNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCITY()
    {
      return $this->SZCITY;
    }

    /**
     * @param string $SZCITY
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYINFORESULT2
     */
    public function setSZCITY($SZCITY)
    {
      $this->SZCITY = $SZCITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDVRNO()
    {
      return $this->SZDVRNO;
    }

    /**
     * @param string $SZDVRNO
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYINFORESULT2
     */
    public function setSZDVRNO($SZDVRNO)
    {
      $this->SZDVRNO = $SZDVRNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYINFORESULT2
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFAXNO()
    {
      return $this->SZFAXNO;
    }

    /**
     * @param string $SZFAXNO
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYINFORESULT2
     */
    public function setSZFAXNO($SZFAXNO)
    {
      $this->SZFAXNO = $SZFAXNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAF()
    {
      return $this->SZNAF;
    }

    /**
     * @param string $SZNAF
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYINFORESULT2
     */
    public function setSZNAF($SZNAF)
    {
      $this->SZNAF = $SZNAF;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYINFORESULT2
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPHONENO()
    {
      return $this->SZPHONENO;
    }

    /**
     * @param string $SZPHONENO
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYINFORESULT2
     */
    public function setSZPHONENO($SZPHONENO)
    {
      $this->SZPHONENO = $SZPHONENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREET()
    {
      return $this->SZSTREET;
    }

    /**
     * @param string $SZSTREET
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYINFORESULT2
     */
    public function setSZSTREET($SZSTREET)
    {
      $this->SZSTREET = $SZSTREET;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVATNO()
    {
      return $this->SZVATNO;
    }

    /**
     * @param string $SZVATNO
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYINFORESULT2
     */
    public function setSZVATNO($SZVATNO)
    {
      $this->SZVATNO = $SZVATNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZZIPCODE()
    {
      return $this->SZZIPCODE;
    }

    /**
     * @param string $SZZIPCODE
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYINFORESULT2
     */
    public function setSZZIPCODE($SZZIPCODE)
    {
      $this->SZZIPCODE = $SZZIPCODE;
      return $this;
    }

}
