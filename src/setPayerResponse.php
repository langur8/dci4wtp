<?php

namespace Axess\Dci4Wtp;

class setPayerResponse
{

    /**
     * @var D4WTPPAYERRESULT $setPayerResult
     */
    protected $setPayerResult = null;

    /**
     * @param D4WTPPAYERRESULT $setPayerResult
     */
    public function __construct($setPayerResult)
    {
      $this->setPayerResult = $setPayerResult;
    }

    /**
     * @return D4WTPPAYERRESULT
     */
    public function getSetPayerResult()
    {
      return $this->setPayerResult;
    }

    /**
     * @param D4WTPPAYERRESULT $setPayerResult
     * @return \Axess\Dci4Wtp\setPayerResponse
     */
    public function setSetPayerResult($setPayerResult)
    {
      $this->setPayerResult = $setPayerResult;
      return $this;
    }

}
