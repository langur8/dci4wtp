<?php

namespace Axess\Dci4Wtp;

class createReportResponse
{

    /**
     * @var D4WTPREPORTRESULT $createReportResult
     */
    protected $createReportResult = null;

    /**
     * @param D4WTPREPORTRESULT $createReportResult
     */
    public function __construct($createReportResult)
    {
      $this->createReportResult = $createReportResult;
    }

    /**
     * @return D4WTPREPORTRESULT
     */
    public function getCreateReportResult()
    {
      return $this->createReportResult;
    }

    /**
     * @param D4WTPREPORTRESULT $createReportResult
     * @return \Axess\Dci4Wtp\createReportResponse
     */
    public function setCreateReportResult($createReportResult)
    {
      $this->createReportResult = $createReportResult;
      return $this;
    }

}
