<?php

namespace Axess\Dci4Wtp;

class getTax
{

    /**
     * @var D4WTPGETTAXREQ $i_ctTax
     */
    protected $i_ctTax = null;

    /**
     * @param D4WTPGETTAXREQ $i_ctTax
     */
    public function __construct($i_ctTax)
    {
      $this->i_ctTax = $i_ctTax;
    }

    /**
     * @return D4WTPGETTAXREQ
     */
    public function getI_ctTax()
    {
      return $this->i_ctTax;
    }

    /**
     * @param D4WTPGETTAXREQ $i_ctTax
     * @return \Axess\Dci4Wtp\getTax
     */
    public function setI_ctTax($i_ctTax)
    {
      $this->i_ctTax = $i_ctTax;
      return $this;
    }

}
