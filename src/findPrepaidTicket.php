<?php

namespace Axess\Dci4Wtp;

class findPrepaidTicket
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var string $i_szExtOrderNr
     */
    protected $i_szExtOrderNr = null;

    /**
     * @var float $i_NPROJNR
     */
    protected $i_NPROJNR = null;

    /**
     * @var float $i_nPOSNr
     */
    protected $i_nPOSNr = null;

    /**
     * @var float $i_nJournalNr
     */
    protected $i_nJournalNr = null;

    /**
     * @var float $i_nOrderStatusNr
     */
    protected $i_nOrderStatusNr = null;

    /**
     * @param float $i_nSessionID
     * @param string $i_szExtOrderNr
     * @param float $i_NPROJNR
     * @param float $i_nPOSNr
     * @param float $i_nJournalNr
     * @param float $i_nOrderStatusNr
     */
    public function __construct($i_nSessionID, $i_szExtOrderNr, $i_NPROJNR, $i_nPOSNr, $i_nJournalNr, $i_nOrderStatusNr)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_szExtOrderNr = $i_szExtOrderNr;
      $this->i_NPROJNR = $i_NPROJNR;
      $this->i_nPOSNr = $i_nPOSNr;
      $this->i_nJournalNr = $i_nJournalNr;
      $this->i_nOrderStatusNr = $i_nOrderStatusNr;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\findPrepaidTicket
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szExtOrderNr()
    {
      return $this->i_szExtOrderNr;
    }

    /**
     * @param string $i_szExtOrderNr
     * @return \Axess\Dci4Wtp\findPrepaidTicket
     */
    public function setI_szExtOrderNr($i_szExtOrderNr)
    {
      $this->i_szExtOrderNr = $i_szExtOrderNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_NPROJNR()
    {
      return $this->i_NPROJNR;
    }

    /**
     * @param float $i_NPROJNR
     * @return \Axess\Dci4Wtp\findPrepaidTicket
     */
    public function setI_NPROJNR($i_NPROJNR)
    {
      $this->i_NPROJNR = $i_NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nPOSNr()
    {
      return $this->i_nPOSNr;
    }

    /**
     * @param float $i_nPOSNr
     * @return \Axess\Dci4Wtp\findPrepaidTicket
     */
    public function setI_nPOSNr($i_nPOSNr)
    {
      $this->i_nPOSNr = $i_nPOSNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nJournalNr()
    {
      return $this->i_nJournalNr;
    }

    /**
     * @param float $i_nJournalNr
     * @return \Axess\Dci4Wtp\findPrepaidTicket
     */
    public function setI_nJournalNr($i_nJournalNr)
    {
      $this->i_nJournalNr = $i_nJournalNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nOrderStatusNr()
    {
      return $this->i_nOrderStatusNr;
    }

    /**
     * @param float $i_nOrderStatusNr
     * @return \Axess\Dci4Wtp\findPrepaidTicket
     */
    public function setI_nOrderStatusNr($i_nOrderStatusNr)
    {
      $this->i_nOrderStatusNr = $i_nOrderStatusNr;
      return $this;
    }

}
