<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCOMPANYBALANCERES
{

    /**
     * @var float $FBALANCE
     */
    protected $FBALANCE = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFBALANCE()
    {
      return $this->FBALANCE;
    }

    /**
     * @param float $FBALANCE
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANYBALANCERES
     */
    public function setFBALANCE($FBALANCE)
    {
      $this->FBALANCE = $FBALANCE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANYBALANCERES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANYBALANCERES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
