<?php

namespace Axess\Dci4Wtp;

class TicketValidity
{

    /**
     * @var boolean $IsValid
     */
    protected $IsValid = null;

    /**
     * @var int $KGTNo
     */
    protected $KGTNo = null;

    /**
     * @var ArrayOfTicketBooking $POEDates
     */
    protected $POEDates = null;

    /**
     * @var ArrayOfTicketBooking $POSDates
     */
    protected $POSDates = null;

    /**
     * @var int $POSNo
     */
    protected $POSNo = null;

    /**
     * @var int $ProjNo
     */
    protected $ProjNo = null;

    /**
     * @var int $SerialNo
     */
    protected $SerialNo = null;

    /**
     * @var int $UnicodeNr
     */
    protected $UnicodeNr = null;

    /**
     * @var string $ValidityEnd
     */
    protected $ValidityEnd = null;

    /**
     * @var string $ValidityStart
     */
    protected $ValidityStart = null;

    /**
     * @var ArrayOfTicketPOELimit $availablePOELimits
     */
    protected $availablePOELimits = null;

    /**
     * @var boolean $bNeedGreenPass
     */
    protected $bNeedGreenPass = null;

    /**
     * @var boolean $bNeedPoeLimit
     */
    protected $bNeedPoeLimit = null;

    /**
     * @var boolean $bNeedPosLimit
     */
    protected $bNeedPosLimit = null;

    /**
     * @var boolean $bNeedTimeSlot
     */
    protected $bNeedTimeSlot = null;

    /**
     * @var string $dtGreenPassValidTo
     */
    protected $dtGreenPassValidTo = null;

    /**
     * @var QRCodeData $greenPassQRCode
     */
    protected $greenPassQRCode = null;

    /**
     * @var int $nKundenKartenTypNr
     */
    protected $nKundenKartenTypNr = null;

    /**
     * @var int $nPersTypNr
     */
    protected $nPersTypNr = null;

    /**
     * @var int $nTicketPoolNr
     */
    protected $nTicketPoolNr = null;

    /**
     * @var string $szMediaID
     */
    protected $szMediaID = null;

    /**
     * @var string $szOwnerFirstName
     */
    protected $szOwnerFirstName = null;

    /**
     * @var string $szOwnerLastName
     */
    protected $szOwnerLastName = null;

    /**
     * @var string $szPersTypName
     */
    protected $szPersTypName = null;

    /**
     * @var string $szTicketTypeName
     */
    protected $szTicketTypeName = null;

    /**
     * @var string $szUID
     */
    protected $szUID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getIsValid()
    {
      return $this->IsValid;
    }

    /**
     * @param boolean $IsValid
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setIsValid($IsValid)
    {
      $this->IsValid = $IsValid;
      return $this;
    }

    /**
     * @return int
     */
    public function getKGTNo()
    {
      return $this->KGTNo;
    }

    /**
     * @param int $KGTNo
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setKGTNo($KGTNo)
    {
      $this->KGTNo = $KGTNo;
      return $this;
    }

    /**
     * @return ArrayOfTicketBooking
     */
    public function getPOEDates()
    {
      return $this->POEDates;
    }

    /**
     * @param ArrayOfTicketBooking $POEDates
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setPOEDates($POEDates)
    {
      $this->POEDates = $POEDates;
      return $this;
    }

    /**
     * @return ArrayOfTicketBooking
     */
    public function getPOSDates()
    {
      return $this->POSDates;
    }

    /**
     * @param ArrayOfTicketBooking $POSDates
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setPOSDates($POSDates)
    {
      $this->POSDates = $POSDates;
      return $this;
    }

    /**
     * @return int
     */
    public function getPOSNo()
    {
      return $this->POSNo;
    }

    /**
     * @param int $POSNo
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setPOSNo($POSNo)
    {
      $this->POSNo = $POSNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getProjNo()
    {
      return $this->ProjNo;
    }

    /**
     * @param int $ProjNo
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setProjNo($ProjNo)
    {
      $this->ProjNo = $ProjNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getSerialNo()
    {
      return $this->SerialNo;
    }

    /**
     * @param int $SerialNo
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setSerialNo($SerialNo)
    {
      $this->SerialNo = $SerialNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getUnicodeNr()
    {
      return $this->UnicodeNr;
    }

    /**
     * @param int $UnicodeNr
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setUnicodeNr($UnicodeNr)
    {
      $this->UnicodeNr = $UnicodeNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getValidityEnd()
    {
      return $this->ValidityEnd;
    }

    /**
     * @param string $ValidityEnd
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setValidityEnd($ValidityEnd)
    {
      $this->ValidityEnd = $ValidityEnd;
      return $this;
    }

    /**
     * @return string
     */
    public function getValidityStart()
    {
      return $this->ValidityStart;
    }

    /**
     * @param string $ValidityStart
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setValidityStart($ValidityStart)
    {
      $this->ValidityStart = $ValidityStart;
      return $this;
    }

    /**
     * @return ArrayOfTicketPOELimit
     */
    public function getAvailablePOELimits()
    {
      return $this->availablePOELimits;
    }

    /**
     * @param ArrayOfTicketPOELimit $availablePOELimits
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setAvailablePOELimits($availablePOELimits)
    {
      $this->availablePOELimits = $availablePOELimits;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBNeedGreenPass()
    {
      return $this->bNeedGreenPass;
    }

    /**
     * @param boolean $bNeedGreenPass
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setBNeedGreenPass($bNeedGreenPass)
    {
      $this->bNeedGreenPass = $bNeedGreenPass;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBNeedPoeLimit()
    {
      return $this->bNeedPoeLimit;
    }

    /**
     * @param boolean $bNeedPoeLimit
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setBNeedPoeLimit($bNeedPoeLimit)
    {
      $this->bNeedPoeLimit = $bNeedPoeLimit;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBNeedPosLimit()
    {
      return $this->bNeedPosLimit;
    }

    /**
     * @param boolean $bNeedPosLimit
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setBNeedPosLimit($bNeedPosLimit)
    {
      $this->bNeedPosLimit = $bNeedPosLimit;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBNeedTimeSlot()
    {
      return $this->bNeedTimeSlot;
    }

    /**
     * @param boolean $bNeedTimeSlot
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setBNeedTimeSlot($bNeedTimeSlot)
    {
      $this->bNeedTimeSlot = $bNeedTimeSlot;
      return $this;
    }

    /**
     * @return string
     */
    public function getDtGreenPassValidTo()
    {
      return $this->dtGreenPassValidTo;
    }

    /**
     * @param string $dtGreenPassValidTo
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setDtGreenPassValidTo($dtGreenPassValidTo)
    {
      $this->dtGreenPassValidTo = $dtGreenPassValidTo;
      return $this;
    }

    /**
     * @return QRCodeData
     */
    public function getGreenPassQRCode()
    {
      return $this->greenPassQRCode;
    }

    /**
     * @param QRCodeData $greenPassQRCode
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setGreenPassQRCode($greenPassQRCode)
    {
      $this->greenPassQRCode = $greenPassQRCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getNKundenKartenTypNr()
    {
      return $this->nKundenKartenTypNr;
    }

    /**
     * @param int $nKundenKartenTypNr
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setNKundenKartenTypNr($nKundenKartenTypNr)
    {
      $this->nKundenKartenTypNr = $nKundenKartenTypNr;
      return $this;
    }

    /**
     * @return int
     */
    public function getNPersTypNr()
    {
      return $this->nPersTypNr;
    }

    /**
     * @param int $nPersTypNr
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setNPersTypNr($nPersTypNr)
    {
      $this->nPersTypNr = $nPersTypNr;
      return $this;
    }

    /**
     * @return int
     */
    public function getNTicketPoolNr()
    {
      return $this->nTicketPoolNr;
    }

    /**
     * @param int $nTicketPoolNr
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setNTicketPoolNr($nTicketPoolNr)
    {
      $this->nTicketPoolNr = $nTicketPoolNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzMediaID()
    {
      return $this->szMediaID;
    }

    /**
     * @param string $szMediaID
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setSzMediaID($szMediaID)
    {
      $this->szMediaID = $szMediaID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzOwnerFirstName()
    {
      return $this->szOwnerFirstName;
    }

    /**
     * @param string $szOwnerFirstName
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setSzOwnerFirstName($szOwnerFirstName)
    {
      $this->szOwnerFirstName = $szOwnerFirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzOwnerLastName()
    {
      return $this->szOwnerLastName;
    }

    /**
     * @param string $szOwnerLastName
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setSzOwnerLastName($szOwnerLastName)
    {
      $this->szOwnerLastName = $szOwnerLastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzPersTypName()
    {
      return $this->szPersTypName;
    }

    /**
     * @param string $szPersTypName
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setSzPersTypName($szPersTypName)
    {
      $this->szPersTypName = $szPersTypName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzTicketTypeName()
    {
      return $this->szTicketTypeName;
    }

    /**
     * @param string $szTicketTypeName
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setSzTicketTypeName($szTicketTypeName)
    {
      $this->szTicketTypeName = $szTicketTypeName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzUID()
    {
      return $this->szUID;
    }

    /**
     * @param string $szUID
     * @return \Axess\Dci4Wtp\TicketValidity
     */
    public function setSzUID($szUID)
    {
      $this->szUID = $szUID;
      return $this;
    }

}
