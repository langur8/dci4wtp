<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGECONTENT2
{

    /**
     * @var float $BDURATIONCANBEMODIFIED
     */
    protected $BDURATIONCANBEMODIFIED = null;

    /**
     * @var float $BQUANTITYFIX
     */
    protected $BQUANTITYFIX = null;

    /**
     * @var float $NARTICLENR
     */
    protected $NARTICLENR = null;

    /**
     * @var float $NCUSTOMERTICKETTYPENR
     */
    protected $NCUSTOMERTICKETTYPENR = null;

    /**
     * @var float $NMAXQUANTITY
     */
    protected $NMAXQUANTITY = null;

    /**
     * @var float $NMINQUANTITY
     */
    protected $NMINQUANTITY = null;

    /**
     * @var float $NPACKNR
     */
    protected $NPACKNR = null;

    /**
     * @var float $NPACKPOS
     */
    protected $NPACKPOS = null;

    /**
     * @var float $NPACKTARIFFSHEETNR
     */
    protected $NPACKTARIFFSHEETNR = null;

    /**
     * @var float $NPERSTYPENR
     */
    protected $NPERSTYPENR = null;

    /**
     * @var float $NPOOLNR
     */
    protected $NPOOLNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NRENTALITEMDURATION
     */
    protected $NRENTALITEMDURATION = null;

    /**
     * @var float $NRENTALITEMNR
     */
    protected $NRENTALITEMNR = null;

    /**
     * @var float $NRENTALPERSTYPENR
     */
    protected $NRENTALPERSTYPENR = null;

    /**
     * @var float $NSORTNR
     */
    protected $NSORTNR = null;

    /**
     * @var float $NWARESALEITEMNR
     */
    protected $NWARESALEITEMNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBDURATIONCANBEMODIFIED()
    {
      return $this->BDURATIONCANBEMODIFIED;
    }

    /**
     * @param float $BDURATIONCANBEMODIFIED
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setBDURATIONCANBEMODIFIED($BDURATIONCANBEMODIFIED)
    {
      $this->BDURATIONCANBEMODIFIED = $BDURATIONCANBEMODIFIED;
      return $this;
    }

    /**
     * @return float
     */
    public function getBQUANTITYFIX()
    {
      return $this->BQUANTITYFIX;
    }

    /**
     * @param float $BQUANTITYFIX
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setBQUANTITYFIX($BQUANTITYFIX)
    {
      $this->BQUANTITYFIX = $BQUANTITYFIX;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENR()
    {
      return $this->NARTICLENR;
    }

    /**
     * @param float $NARTICLENR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNARTICLENR($NARTICLENR)
    {
      $this->NARTICLENR = $NARTICLENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTOMERTICKETTYPENR()
    {
      return $this->NCUSTOMERTICKETTYPENR;
    }

    /**
     * @param float $NCUSTOMERTICKETTYPENR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNCUSTOMERTICKETTYPENR($NCUSTOMERTICKETTYPENR)
    {
      $this->NCUSTOMERTICKETTYPENR = $NCUSTOMERTICKETTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMAXQUANTITY()
    {
      return $this->NMAXQUANTITY;
    }

    /**
     * @param float $NMAXQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNMAXQUANTITY($NMAXQUANTITY)
    {
      $this->NMAXQUANTITY = $NMAXQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMINQUANTITY()
    {
      return $this->NMINQUANTITY;
    }

    /**
     * @param float $NMINQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNMINQUANTITY($NMINQUANTITY)
    {
      $this->NMINQUANTITY = $NMINQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKNR()
    {
      return $this->NPACKNR;
    }

    /**
     * @param float $NPACKNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNPACKNR($NPACKNR)
    {
      $this->NPACKNR = $NPACKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKPOS()
    {
      return $this->NPACKPOS;
    }

    /**
     * @param float $NPACKPOS
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNPACKPOS($NPACKPOS)
    {
      $this->NPACKPOS = $NPACKPOS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKTARIFFSHEETNR()
    {
      return $this->NPACKTARIFFSHEETNR;
    }

    /**
     * @param float $NPACKTARIFFSHEETNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNPACKTARIFFSHEETNR($NPACKTARIFFSHEETNR)
    {
      $this->NPACKTARIFFSHEETNR = $NPACKTARIFFSHEETNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSTYPENR()
    {
      return $this->NPERSTYPENR;
    }

    /**
     * @param float $NPERSTYPENR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNPERSTYPENR($NPERSTYPENR)
    {
      $this->NPERSTYPENR = $NPERSTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNR()
    {
      return $this->NPOOLNR;
    }

    /**
     * @param float $NPOOLNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNPOOLNR($NPOOLNR)
    {
      $this->NPOOLNR = $NPOOLNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMDURATION()
    {
      return $this->NRENTALITEMDURATION;
    }

    /**
     * @param float $NRENTALITEMDURATION
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNRENTALITEMDURATION($NRENTALITEMDURATION)
    {
      $this->NRENTALITEMDURATION = $NRENTALITEMDURATION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNR()
    {
      return $this->NRENTALITEMNR;
    }

    /**
     * @param float $NRENTALITEMNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNRENTALITEMNR($NRENTALITEMNR)
    {
      $this->NRENTALITEMNR = $NRENTALITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPERSTYPENR()
    {
      return $this->NRENTALPERSTYPENR;
    }

    /**
     * @param float $NRENTALPERSTYPENR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNRENTALPERSTYPENR($NRENTALPERSTYPENR)
    {
      $this->NRENTALPERSTYPENR = $NRENTALPERSTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNR()
    {
      return $this->NSORTNR;
    }

    /**
     * @param float $NSORTNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNSORTNR($NSORTNR)
    {
      $this->NSORTNR = $NSORTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARESALEITEMNR()
    {
      return $this->NWARESALEITEMNR;
    }

    /**
     * @param float $NWARESALEITEMNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT2
     */
    public function setNWARESALEITEMNR($NWARESALEITEMNR)
    {
      $this->NWARESALEITEMNR = $NWARESALEITEMNR;
      return $this;
    }

}
