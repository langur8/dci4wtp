<?php

namespace Axess\Dci4Wtp;

class D4WTPPRINTINGDATA
{

    /**
     * @var float $NYMAX
     */
    protected $NYMAX = null;

    /**
     * @var float $NYMIN
     */
    protected $NYMIN = null;

    /**
     * @var string $SZPRINTDATA
     */
    protected $SZPRINTDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNYMAX()
    {
      return $this->NYMAX;
    }

    /**
     * @param float $NYMAX
     * @return \Axess\Dci4Wtp\D4WTPPRINTINGDATA
     */
    public function setNYMAX($NYMAX)
    {
      $this->NYMAX = $NYMAX;
      return $this;
    }

    /**
     * @return float
     */
    public function getNYMIN()
    {
      return $this->NYMIN;
    }

    /**
     * @param float $NYMIN
     * @return \Axess\Dci4Wtp\D4WTPPRINTINGDATA
     */
    public function setNYMIN($NYMIN)
    {
      $this->NYMIN = $NYMIN;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPRINTDATA()
    {
      return $this->SZPRINTDATA;
    }

    /**
     * @param string $SZPRINTDATA
     * @return \Axess\Dci4Wtp\D4WTPPRINTINGDATA
     */
    public function setSZPRINTDATA($SZPRINTDATA)
    {
      $this->SZPRINTDATA = $SZPRINTDATA;
      return $this;
    }

}
