<?php

namespace Axess\Dci4Wtp;

class getWorkOrders
{

    /**
     * @var D4WTPGETWORKORDERSREQUEST $i_ctGetWorkOrderReq
     */
    protected $i_ctGetWorkOrderReq = null;

    /**
     * @param D4WTPGETWORKORDERSREQUEST $i_ctGetWorkOrderReq
     */
    public function __construct($i_ctGetWorkOrderReq)
    {
      $this->i_ctGetWorkOrderReq = $i_ctGetWorkOrderReq;
    }

    /**
     * @return D4WTPGETWORKORDERSREQUEST
     */
    public function getI_ctGetWorkOrderReq()
    {
      return $this->i_ctGetWorkOrderReq;
    }

    /**
     * @param D4WTPGETWORKORDERSREQUEST $i_ctGetWorkOrderReq
     * @return \Axess\Dci4Wtp\getWorkOrders
     */
    public function setI_ctGetWorkOrderReq($i_ctGetWorkOrderReq)
    {
      $this->i_ctGetWorkOrderReq = $i_ctGetWorkOrderReq;
      return $this;
    }

}
