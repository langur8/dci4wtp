<?php

namespace Axess\Dci4Wtp;

class modifyReservationResponse
{

    /**
     * @var D4WTPRESULT $modifyReservationResult
     */
    protected $modifyReservationResult = null;

    /**
     * @param D4WTPRESULT $modifyReservationResult
     */
    public function __construct($modifyReservationResult)
    {
      $this->modifyReservationResult = $modifyReservationResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getModifyReservationResult()
    {
      return $this->modifyReservationResult;
    }

    /**
     * @param D4WTPRESULT $modifyReservationResult
     * @return \Axess\Dci4Wtp\modifyReservationResponse
     */
    public function setModifyReservationResult($modifyReservationResult)
    {
      $this->modifyReservationResult = $modifyReservationResult;
      return $this;
    }

}
