<?php

namespace Axess\Dci4Wtp;

class D4WTPSWITCHPREPAIDRES2
{

    /**
     * @var ArrayOfD4WTPPREPAIDARTICLESPRINT $ACTPREPAIDARTICLESPRINT
     */
    protected $ACTPREPAIDARTICLESPRINT = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPREPAIDARTICLESPRINT
     */
    public function getACTPREPAIDARTICLESPRINT()
    {
      return $this->ACTPREPAIDARTICLESPRINT;
    }

    /**
     * @param ArrayOfD4WTPPREPAIDARTICLESPRINT $ACTPREPAIDARTICLESPRINT
     * @return \Axess\Dci4Wtp\D4WTPSWITCHPREPAIDRES2
     */
    public function setACTPREPAIDARTICLESPRINT($ACTPREPAIDARTICLESPRINT)
    {
      $this->ACTPREPAIDARTICLESPRINT = $ACTPREPAIDARTICLESPRINT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPSWITCHPREPAIDRES2
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPSWITCHPREPAIDRES2
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
