<?php

namespace Axess\Dci4Wtp;

class D4WTPREBOOKTRANSREQ
{

    /**
     * @var ArrayOfD4WTPTICKETSALE $ACTTICKETSALE
     */
    protected $ACTTICKETSALE = null;

    /**
     * @var D4WTPCREDITCARDDATA $CTCREDITCARDDATA
     */
    protected $CTCREDITCARDDATA = null;

    /**
     * @var float $NAPPTRANSLOGID
     */
    protected $NAPPTRANSLOGID = null;

    /**
     * @var float $NCOREPAYMENTNRTO
     */
    protected $NCOREPAYMENTNRTO = null;

    /**
     * @var float $NPAYMENTNRTO
     */
    protected $NPAYMENTNRTO = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNR
     */
    protected $NTRANSNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKETSALE
     */
    public function getACTTICKETSALE()
    {
      return $this->ACTTICKETSALE;
    }

    /**
     * @param ArrayOfD4WTPTICKETSALE $ACTTICKETSALE
     * @return \Axess\Dci4Wtp\D4WTPREBOOKTRANSREQ
     */
    public function setACTTICKETSALE($ACTTICKETSALE)
    {
      $this->ACTTICKETSALE = $ACTTICKETSALE;
      return $this;
    }

    /**
     * @return D4WTPCREDITCARDDATA
     */
    public function getCTCREDITCARDDATA()
    {
      return $this->CTCREDITCARDDATA;
    }

    /**
     * @param D4WTPCREDITCARDDATA $CTCREDITCARDDATA
     * @return \Axess\Dci4Wtp\D4WTPREBOOKTRANSREQ
     */
    public function setCTCREDITCARDDATA($CTCREDITCARDDATA)
    {
      $this->CTCREDITCARDDATA = $CTCREDITCARDDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAPPTRANSLOGID()
    {
      return $this->NAPPTRANSLOGID;
    }

    /**
     * @param float $NAPPTRANSLOGID
     * @return \Axess\Dci4Wtp\D4WTPREBOOKTRANSREQ
     */
    public function setNAPPTRANSLOGID($NAPPTRANSLOGID)
    {
      $this->NAPPTRANSLOGID = $NAPPTRANSLOGID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOREPAYMENTNRTO()
    {
      return $this->NCOREPAYMENTNRTO;
    }

    /**
     * @param float $NCOREPAYMENTNRTO
     * @return \Axess\Dci4Wtp\D4WTPREBOOKTRANSREQ
     */
    public function setNCOREPAYMENTNRTO($NCOREPAYMENTNRTO)
    {
      $this->NCOREPAYMENTNRTO = $NCOREPAYMENTNRTO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYMENTNRTO()
    {
      return $this->NPAYMENTNRTO;
    }

    /**
     * @param float $NPAYMENTNRTO
     * @return \Axess\Dci4Wtp\D4WTPREBOOKTRANSREQ
     */
    public function setNPAYMENTNRTO($NPAYMENTNRTO)
    {
      $this->NPAYMENTNRTO = $NPAYMENTNRTO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPREBOOKTRANSREQ
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPREBOOKTRANSREQ
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPREBOOKTRANSREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNR()
    {
      return $this->NTRANSNR;
    }

    /**
     * @param float $NTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPREBOOKTRANSREQ
     */
    public function setNTRANSNR($NTRANSNR)
    {
      $this->NTRANSNR = $NTRANSNR;
      return $this;
    }

}
