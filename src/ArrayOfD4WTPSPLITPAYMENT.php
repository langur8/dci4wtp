<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSPLITPAYMENT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSPLITPAYMENT[] $D4WTPSPLITPAYMENT
     */
    protected $D4WTPSPLITPAYMENT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSPLITPAYMENT[]
     */
    public function getD4WTPSPLITPAYMENT()
    {
      return $this->D4WTPSPLITPAYMENT;
    }

    /**
     * @param D4WTPSPLITPAYMENT[] $D4WTPSPLITPAYMENT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSPLITPAYMENT
     */
    public function setD4WTPSPLITPAYMENT(array $D4WTPSPLITPAYMENT = null)
    {
      $this->D4WTPSPLITPAYMENT = $D4WTPSPLITPAYMENT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSPLITPAYMENT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSPLITPAYMENT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSPLITPAYMENT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSPLITPAYMENT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSPLITPAYMENT[] = $value;
      } else {
        $this->D4WTPSPLITPAYMENT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSPLITPAYMENT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSPLITPAYMENT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSPLITPAYMENT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSPLITPAYMENT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSPLITPAYMENT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSPLITPAYMENT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSPLITPAYMENT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSPLITPAYMENT);
    }

}
