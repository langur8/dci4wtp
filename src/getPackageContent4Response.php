<?php

namespace Axess\Dci4Wtp;

class getPackageContent4Response
{

    /**
     * @var D4WTPPACKAGECONTENT4RESULT $getPackageContent4Result
     */
    protected $getPackageContent4Result = null;

    /**
     * @param D4WTPPACKAGECONTENT4RESULT $getPackageContent4Result
     */
    public function __construct($getPackageContent4Result)
    {
      $this->getPackageContent4Result = $getPackageContent4Result;
    }

    /**
     * @return D4WTPPACKAGECONTENT4RESULT
     */
    public function getGetPackageContent4Result()
    {
      return $this->getPackageContent4Result;
    }

    /**
     * @param D4WTPPACKAGECONTENT4RESULT $getPackageContent4Result
     * @return \Axess\Dci4Wtp\getPackageContent4Response
     */
    public function setGetPackageContent4Result($getPackageContent4Result)
    {
      $this->getPackageContent4Result = $getPackageContent4Result;
      return $this;
    }

}
