<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST4RESULT
{

    /**
     * @var ArrayOfD4WTPTARIFFLIST4 $ACTTARIFFLIST4
     */
    protected $ACTTARIFFLIST4 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLIST4
     */
    public function getACTTARIFFLIST4()
    {
      return $this->ACTTARIFFLIST4;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLIST4 $ACTTARIFFLIST4
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST4RESULT
     */
    public function setACTTARIFFLIST4($ACTTARIFFLIST4)
    {
      $this->ACTTARIFFLIST4 = $ACTTARIFFLIST4;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST4RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST4RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
