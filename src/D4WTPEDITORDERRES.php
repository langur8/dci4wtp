<?php

namespace Axess\Dci4Wtp;

class D4WTPEDITORDERRES
{

    /**
     * @var ADDRESS $CTDELIVERYADDRESS
     */
    protected $CTDELIVERYADDRESS = null;

    /**
     * @var float $NCOMPNR
     */
    protected $NCOMPNR = null;

    /**
     * @var float $NCOMPPOSNR
     */
    protected $NCOMPPOSNR = null;

    /**
     * @var float $NCOMPPROJNR
     */
    protected $NCOMPPROJNR = null;

    /**
     * @var float $NCOREPAYMENTTYPENR
     */
    protected $NCOREPAYMENTTYPENR = null;

    /**
     * @var float $NCUSTOMERPAYMENTTYPENR
     */
    protected $NCUSTOMERPAYMENTTYPENR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $NNAMEDUSERNAME
     */
    protected $NNAMEDUSERNAME = null;

    /**
     * @var float $NNAMEDUSERNR
     */
    protected $NNAMEDUSERNR = null;

    /**
     * @var float $NORDERLISTNR
     */
    protected $NORDERLISTNR = null;

    /**
     * @var float $NORDERSTATENR
     */
    protected $NORDERSTATENR = null;

    /**
     * @var float $NORDERTYPENR
     */
    protected $NORDERTYPENR = null;

    /**
     * @var float $NPERSNR
     */
    protected $NPERSNR = null;

    /**
     * @var float $NPERSPOSNR
     */
    protected $NPERSPOSNR = null;

    /**
     * @var float $NPERSPROJNR
     */
    protected $NPERSPROJNR = null;

    /**
     * @var float $NPRODPOSNR
     */
    protected $NPRODPOSNR = null;

    /**
     * @var float $NPRODPOSPROJNR
     */
    protected $NPRODPOSPROJNR = null;

    /**
     * @var float $NPRODTRANSNR
     */
    protected $NPRODTRANSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSWITCHGESNR
     */
    protected $NSWITCHGESNR = null;

    /**
     * @var float $NSWITCHPROJNR
     */
    protected $NSWITCHPROJNR = null;

    /**
     * @var float $NSWITCHSUBPROJNR
     */
    protected $NSWITCHSUBPROJNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZEXTORDERREFERENCE
     */
    protected $SZEXTORDERREFERENCE = null;

    /**
     * @var string $SZORDERSTATENAME
     */
    protected $SZORDERSTATENAME = null;

    /**
     * @var string $SZORDERTYPENAME
     */
    protected $SZORDERTYPENAME = null;

    /**
     * @var string $SZOWNERCOMPANYNAME
     */
    protected $SZOWNERCOMPANYNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ADDRESS
     */
    public function getCTDELIVERYADDRESS()
    {
      return $this->CTDELIVERYADDRESS;
    }

    /**
     * @param ADDRESS $CTDELIVERYADDRESS
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setCTDELIVERYADDRESS($CTDELIVERYADDRESS)
    {
      $this->CTDELIVERYADDRESS = $CTDELIVERYADDRESS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPNR()
    {
      return $this->NCOMPNR;
    }

    /**
     * @param float $NCOMPNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNCOMPNR($NCOMPNR)
    {
      $this->NCOMPNR = $NCOMPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPPOSNR()
    {
      return $this->NCOMPPOSNR;
    }

    /**
     * @param float $NCOMPPOSNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNCOMPPOSNR($NCOMPPOSNR)
    {
      $this->NCOMPPOSNR = $NCOMPPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPPROJNR()
    {
      return $this->NCOMPPROJNR;
    }

    /**
     * @param float $NCOMPPROJNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNCOMPPROJNR($NCOMPPROJNR)
    {
      $this->NCOMPPROJNR = $NCOMPPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOREPAYMENTTYPENR()
    {
      return $this->NCOREPAYMENTTYPENR;
    }

    /**
     * @param float $NCOREPAYMENTTYPENR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNCOREPAYMENTTYPENR($NCOREPAYMENTTYPENR)
    {
      $this->NCOREPAYMENTTYPENR = $NCOREPAYMENTTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTOMERPAYMENTTYPENR()
    {
      return $this->NCUSTOMERPAYMENTTYPENR;
    }

    /**
     * @param float $NCUSTOMERPAYMENTTYPENR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNCUSTOMERPAYMENTTYPENR($NCUSTOMERPAYMENTTYPENR)
    {
      $this->NCUSTOMERPAYMENTTYPENR = $NCUSTOMERPAYMENTTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getNNAMEDUSERNAME()
    {
      return $this->NNAMEDUSERNAME;
    }

    /**
     * @param string $NNAMEDUSERNAME
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNNAMEDUSERNAME($NNAMEDUSERNAME)
    {
      $this->NNAMEDUSERNAME = $NNAMEDUSERNAME;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNAMEDUSERNR()
    {
      return $this->NNAMEDUSERNR;
    }

    /**
     * @param float $NNAMEDUSERNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNNAMEDUSERNR($NNAMEDUSERNR)
    {
      $this->NNAMEDUSERNR = $NNAMEDUSERNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORDERLISTNR()
    {
      return $this->NORDERLISTNR;
    }

    /**
     * @param float $NORDERLISTNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNORDERLISTNR($NORDERLISTNR)
    {
      $this->NORDERLISTNR = $NORDERLISTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORDERSTATENR()
    {
      return $this->NORDERSTATENR;
    }

    /**
     * @param float $NORDERSTATENR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNORDERSTATENR($NORDERSTATENR)
    {
      $this->NORDERSTATENR = $NORDERSTATENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORDERTYPENR()
    {
      return $this->NORDERTYPENR;
    }

    /**
     * @param float $NORDERTYPENR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNORDERTYPENR($NORDERTYPENR)
    {
      $this->NORDERTYPENR = $NORDERTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNR()
    {
      return $this->NPERSNR;
    }

    /**
     * @param float $NPERSNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNPERSNR($NPERSNR)
    {
      $this->NPERSNR = $NPERSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNR()
    {
      return $this->NPERSPOSNR;
    }

    /**
     * @param float $NPERSPOSNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNPERSPOSNR($NPERSPOSNR)
    {
      $this->NPERSPOSNR = $NPERSPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNR()
    {
      return $this->NPERSPROJNR;
    }

    /**
     * @param float $NPERSPROJNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNPERSPROJNR($NPERSPROJNR)
    {
      $this->NPERSPROJNR = $NPERSPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPRODPOSNR()
    {
      return $this->NPRODPOSNR;
    }

    /**
     * @param float $NPRODPOSNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNPRODPOSNR($NPRODPOSNR)
    {
      $this->NPRODPOSNR = $NPRODPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPRODPOSPROJNR()
    {
      return $this->NPRODPOSPROJNR;
    }

    /**
     * @param float $NPRODPOSPROJNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNPRODPOSPROJNR($NPRODPOSPROJNR)
    {
      $this->NPRODPOSPROJNR = $NPRODPOSPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPRODTRANSNR()
    {
      return $this->NPRODTRANSNR;
    }

    /**
     * @param float $NPRODTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNPRODTRANSNR($NPRODTRANSNR)
    {
      $this->NPRODTRANSNR = $NPRODTRANSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSWITCHGESNR()
    {
      return $this->NSWITCHGESNR;
    }

    /**
     * @param float $NSWITCHGESNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNSWITCHGESNR($NSWITCHGESNR)
    {
      $this->NSWITCHGESNR = $NSWITCHGESNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSWITCHPROJNR()
    {
      return $this->NSWITCHPROJNR;
    }

    /**
     * @param float $NSWITCHPROJNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNSWITCHPROJNR($NSWITCHPROJNR)
    {
      $this->NSWITCHPROJNR = $NSWITCHPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSWITCHSUBPROJNR()
    {
      return $this->NSWITCHSUBPROJNR;
    }

    /**
     * @param float $NSWITCHSUBPROJNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setNSWITCHSUBPROJNR($NSWITCHSUBPROJNR)
    {
      $this->NSWITCHSUBPROJNR = $NSWITCHSUBPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTORDERREFERENCE()
    {
      return $this->SZEXTORDERREFERENCE;
    }

    /**
     * @param string $SZEXTORDERREFERENCE
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setSZEXTORDERREFERENCE($SZEXTORDERREFERENCE)
    {
      $this->SZEXTORDERREFERENCE = $SZEXTORDERREFERENCE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZORDERSTATENAME()
    {
      return $this->SZORDERSTATENAME;
    }

    /**
     * @param string $SZORDERSTATENAME
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setSZORDERSTATENAME($SZORDERSTATENAME)
    {
      $this->SZORDERSTATENAME = $SZORDERSTATENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZORDERTYPENAME()
    {
      return $this->SZORDERTYPENAME;
    }

    /**
     * @param string $SZORDERTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setSZORDERTYPENAME($SZORDERTYPENAME)
    {
      $this->SZORDERTYPENAME = $SZORDERTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZOWNERCOMPANYNAME()
    {
      return $this->SZOWNERCOMPANYNAME;
    }

    /**
     * @param string $SZOWNERCOMPANYNAME
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERRES
     */
    public function setSZOWNERCOMPANYNAME($SZOWNERCOMPANYNAME)
    {
      $this->SZOWNERCOMPANYNAME = $SZOWNERCOMPANYNAME;
      return $this;
    }

}
