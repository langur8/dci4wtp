<?php

namespace Axess\Dci4Wtp;

class D4WTPPKGTARIFFLIST2REQUEST
{

    /**
     * @var ArrayOfD4WTPPACKAGE $ACTPACKAGE
     */
    protected $ACTPACKAGE = null;

    /**
     * @var float $BINCLCONTINGENTCNT
     */
    protected $BINCLCONTINGENTCNT = null;

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NPERSOWNERNO
     */
    protected $NPERSOWNERNO = null;

    /**
     * @var float $NPERSOWNERPOSNO
     */
    protected $NPERSOWNERPOSNO = null;

    /**
     * @var float $NPERSOWNERPROJNO
     */
    protected $NPERSOWNERPROJNO = null;

    /**
     * @var float $NPERSPAYERNO
     */
    protected $NPERSPAYERNO = null;

    /**
     * @var float $NPERSPAYERPOSNO
     */
    protected $NPERSPAYERPOSNO = null;

    /**
     * @var float $NPERSPAYERPROJNO
     */
    protected $NPERSPAYERPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    /**
     * @var string $SZVALIDFROMEND
     */
    protected $SZVALIDFROMEND = null;

    /**
     * @var string $SZVALIDFROMSTART
     */
    protected $SZVALIDFROMSTART = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPACKAGE
     */
    public function getACTPACKAGE()
    {
      return $this->ACTPACKAGE;
    }

    /**
     * @param ArrayOfD4WTPPACKAGE $ACTPACKAGE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setACTPACKAGE($ACTPACKAGE)
    {
      $this->ACTPACKAGE = $ACTPACKAGE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBINCLCONTINGENTCNT()
    {
      return $this->BINCLCONTINGENTCNT;
    }

    /**
     * @param float $BINCLCONTINGENTCNT
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setBINCLCONTINGENTCNT($BINCLCONTINGENTCNT)
    {
      $this->BINCLCONTINGENTCNT = $BINCLCONTINGENTCNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSOWNERNO()
    {
      return $this->NPERSOWNERNO;
    }

    /**
     * @param float $NPERSOWNERNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setNPERSOWNERNO($NPERSOWNERNO)
    {
      $this->NPERSOWNERNO = $NPERSOWNERNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSOWNERPOSNO()
    {
      return $this->NPERSOWNERPOSNO;
    }

    /**
     * @param float $NPERSOWNERPOSNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setNPERSOWNERPOSNO($NPERSOWNERPOSNO)
    {
      $this->NPERSOWNERPOSNO = $NPERSOWNERPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSOWNERPROJNO()
    {
      return $this->NPERSOWNERPROJNO;
    }

    /**
     * @param float $NPERSOWNERPROJNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setNPERSOWNERPROJNO($NPERSOWNERPROJNO)
    {
      $this->NPERSOWNERPROJNO = $NPERSOWNERPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPAYERNO()
    {
      return $this->NPERSPAYERNO;
    }

    /**
     * @param float $NPERSPAYERNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setNPERSPAYERNO($NPERSPAYERNO)
    {
      $this->NPERSPAYERNO = $NPERSPAYERNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPAYERPOSNO()
    {
      return $this->NPERSPAYERPOSNO;
    }

    /**
     * @param float $NPERSPAYERPOSNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setNPERSPAYERPOSNO($NPERSPAYERPOSNO)
    {
      $this->NPERSPAYERPOSNO = $NPERSPAYERPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPAYERPROJNO()
    {
      return $this->NPERSPAYERPROJNO;
    }

    /**
     * @param float $NPERSPAYERPROJNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setNPERSPAYERPROJNO($NPERSPAYERPROJNO)
    {
      $this->NPERSPAYERPROJNO = $NPERSPAYERPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROMEND()
    {
      return $this->SZVALIDFROMEND;
    }

    /**
     * @param string $SZVALIDFROMEND
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setSZVALIDFROMEND($SZVALIDFROMEND)
    {
      $this->SZVALIDFROMEND = $SZVALIDFROMEND;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROMSTART()
    {
      return $this->SZVALIDFROMSTART;
    }

    /**
     * @param string $SZVALIDFROMSTART
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2REQUEST
     */
    public function setSZVALIDFROMSTART($SZVALIDFROMSTART)
    {
      $this->SZVALIDFROMSTART = $SZVALIDFROMSTART;
      return $this;
    }

}
