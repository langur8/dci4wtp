<?php

namespace Axess\Dci4Wtp;

class D4WTPGETRENTALITEMTARIFFREQ
{

    /**
     * @var float $BCHECKAVAILCOUNT
     */
    protected $BCHECKAVAILCOUNT = null;

    /**
     * @var D4WTPRENTALITEMPRODUCT $CTRENTALITEMPRODUCT
     */
    protected $CTRENTALITEMPRODUCT = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZFROM
     */
    protected $SZFROM = null;

    /**
     * @var string $SZTO
     */
    protected $SZTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBCHECKAVAILCOUNT()
    {
      return $this->BCHECKAVAILCOUNT;
    }

    /**
     * @param float $BCHECKAVAILCOUNT
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFFREQ
     */
    public function setBCHECKAVAILCOUNT($BCHECKAVAILCOUNT)
    {
      $this->BCHECKAVAILCOUNT = $BCHECKAVAILCOUNT;
      return $this;
    }

    /**
     * @return D4WTPRENTALITEMPRODUCT
     */
    public function getCTRENTALITEMPRODUCT()
    {
      return $this->CTRENTALITEMPRODUCT;
    }

    /**
     * @param D4WTPRENTALITEMPRODUCT $CTRENTALITEMPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFFREQ
     */
    public function setCTRENTALITEMPRODUCT($CTRENTALITEMPRODUCT)
    {
      $this->CTRENTALITEMPRODUCT = $CTRENTALITEMPRODUCT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFFREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFROM()
    {
      return $this->SZFROM;
    }

    /**
     * @param string $SZFROM
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFFREQ
     */
    public function setSZFROM($SZFROM)
    {
      $this->SZFROM = $SZFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTO()
    {
      return $this->SZTO;
    }

    /**
     * @param string $SZTO
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFFREQ
     */
    public function setSZTO($SZTO)
    {
      $this->SZTO = $SZTO;
      return $this;
    }

}
