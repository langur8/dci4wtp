<?php

namespace Axess\Dci4Wtp;

class D4WTPSHOPCSUBCONTINGENT2
{

    /**
     * @var float $NCONTINGENTNO
     */
    protected $NCONTINGENTNO = null;

    /**
     * @var float $NSUBCONTINGENTNO
     */
    protected $NSUBCONTINGENTNO = null;

    /**
     * @var string $SZCONTINGENTNAME
     */
    protected $SZCONTINGENTNAME = null;

    /**
     * @var string $SZCONTINGETSHORTNAME
     */
    protected $SZCONTINGETSHORTNAME = null;

    /**
     * @var string $SZRESERVATIONDATE
     */
    protected $SZRESERVATIONDATE = null;

    /**
     * @var string $SZSTARTTIME
     */
    protected $SZSTARTTIME = null;

    /**
     * @var string $SZSUBCONTINGENTMATCHCODE
     */
    protected $SZSUBCONTINGENTMATCHCODE = null;

    /**
     * @var string $SZSUBCONTINGENTNAME
     */
    protected $SZSUBCONTINGENTNAME = null;

    /**
     * @var string $SZSUBCONTINGETSHORTNAME
     */
    protected $SZSUBCONTINGETSHORTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNO()
    {
      return $this->NCONTINGENTNO;
    }

    /**
     * @param float $NCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCSUBCONTINGENT2
     */
    public function setNCONTINGENTNO($NCONTINGENTNO)
    {
      $this->NCONTINGENTNO = $NCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTINGENTNO()
    {
      return $this->NSUBCONTINGENTNO;
    }

    /**
     * @param float $NSUBCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCSUBCONTINGENT2
     */
    public function setNSUBCONTINGENTNO($NSUBCONTINGENTNO)
    {
      $this->NSUBCONTINGENTNO = $NSUBCONTINGENTNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCONTINGENTNAME()
    {
      return $this->SZCONTINGENTNAME;
    }

    /**
     * @param string $SZCONTINGENTNAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCSUBCONTINGENT2
     */
    public function setSZCONTINGENTNAME($SZCONTINGENTNAME)
    {
      $this->SZCONTINGENTNAME = $SZCONTINGENTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCONTINGETSHORTNAME()
    {
      return $this->SZCONTINGETSHORTNAME;
    }

    /**
     * @param string $SZCONTINGETSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCSUBCONTINGENT2
     */
    public function setSZCONTINGETSHORTNAME($SZCONTINGETSHORTNAME)
    {
      $this->SZCONTINGETSHORTNAME = $SZCONTINGETSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRESERVATIONDATE()
    {
      return $this->SZRESERVATIONDATE;
    }

    /**
     * @param string $SZRESERVATIONDATE
     * @return \Axess\Dci4Wtp\D4WTPSHOPCSUBCONTINGENT2
     */
    public function setSZRESERVATIONDATE($SZRESERVATIONDATE)
    {
      $this->SZRESERVATIONDATE = $SZRESERVATIONDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTARTTIME()
    {
      return $this->SZSTARTTIME;
    }

    /**
     * @param string $SZSTARTTIME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCSUBCONTINGENT2
     */
    public function setSZSTARTTIME($SZSTARTTIME)
    {
      $this->SZSTARTTIME = $SZSTARTTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBCONTINGENTMATCHCODE()
    {
      return $this->SZSUBCONTINGENTMATCHCODE;
    }

    /**
     * @param string $SZSUBCONTINGENTMATCHCODE
     * @return \Axess\Dci4Wtp\D4WTPSHOPCSUBCONTINGENT2
     */
    public function setSZSUBCONTINGENTMATCHCODE($SZSUBCONTINGENTMATCHCODE)
    {
      $this->SZSUBCONTINGENTMATCHCODE = $SZSUBCONTINGENTMATCHCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBCONTINGENTNAME()
    {
      return $this->SZSUBCONTINGENTNAME;
    }

    /**
     * @param string $SZSUBCONTINGENTNAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCSUBCONTINGENT2
     */
    public function setSZSUBCONTINGENTNAME($SZSUBCONTINGENTNAME)
    {
      $this->SZSUBCONTINGENTNAME = $SZSUBCONTINGENTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBCONTINGETSHORTNAME()
    {
      return $this->SZSUBCONTINGETSHORTNAME;
    }

    /**
     * @param string $SZSUBCONTINGETSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCSUBCONTINGENT2
     */
    public function setSZSUBCONTINGETSHORTNAME($SZSUBCONTINGETSHORTNAME)
    {
      $this->SZSUBCONTINGETSHORTNAME = $SZSUBCONTINGETSHORTNAME;
      return $this;
    }

}
