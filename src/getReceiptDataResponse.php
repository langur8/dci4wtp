<?php

namespace Axess\Dci4Wtp;

class getReceiptDataResponse
{

    /**
     * @var D4WTPRECEIPTDATARESULT $getReceiptDataResult
     */
    protected $getReceiptDataResult = null;

    /**
     * @param D4WTPRECEIPTDATARESULT $getReceiptDataResult
     */
    public function __construct($getReceiptDataResult)
    {
      $this->getReceiptDataResult = $getReceiptDataResult;
    }

    /**
     * @return D4WTPRECEIPTDATARESULT
     */
    public function getGetReceiptDataResult()
    {
      return $this->getReceiptDataResult;
    }

    /**
     * @param D4WTPRECEIPTDATARESULT $getReceiptDataResult
     * @return \Axess\Dci4Wtp\getReceiptDataResponse
     */
    public function setGetReceiptDataResult($getReceiptDataResult)
    {
      $this->getReceiptDataResult = $getReceiptDataResult;
      return $this;
    }

}
