<?php

namespace Axess\Dci4Wtp;

class issueTicket10Response
{

    /**
     * @var D4WTPISSUETICKET10RESULT $issueTicket10Result
     */
    protected $issueTicket10Result = null;

    /**
     * @param D4WTPISSUETICKET10RESULT $issueTicket10Result
     */
    public function __construct($issueTicket10Result)
    {
      $this->issueTicket10Result = $issueTicket10Result;
    }

    /**
     * @return D4WTPISSUETICKET10RESULT
     */
    public function getIssueTicket10Result()
    {
      return $this->issueTicket10Result;
    }

    /**
     * @param D4WTPISSUETICKET10RESULT $issueTicket10Result
     * @return \Axess\Dci4Wtp\issueTicket10Response
     */
    public function setIssueTicket10Result($issueTicket10Result)
    {
      $this->issueTicket10Result = $issueTicket10Result;
      return $this;
    }

}
