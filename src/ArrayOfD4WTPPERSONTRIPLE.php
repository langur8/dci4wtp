<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPERSONTRIPLE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPERSONTRIPLE[] $D4WTPPERSONTRIPLE
     */
    protected $D4WTPPERSONTRIPLE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPERSONTRIPLE[]
     */
    public function getD4WTPPERSONTRIPLE()
    {
      return $this->D4WTPPERSONTRIPLE;
    }

    /**
     * @param D4WTPPERSONTRIPLE[] $D4WTPPERSONTRIPLE
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPERSONTRIPLE
     */
    public function setD4WTPPERSONTRIPLE(array $D4WTPPERSONTRIPLE = null)
    {
      $this->D4WTPPERSONTRIPLE = $D4WTPPERSONTRIPLE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPERSONTRIPLE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPERSONTRIPLE
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPERSONTRIPLE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPERSONTRIPLE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPERSONTRIPLE[] = $value;
      } else {
        $this->D4WTPPERSONTRIPLE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPERSONTRIPLE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPERSONTRIPLE Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPERSONTRIPLE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPERSONTRIPLE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPERSONTRIPLE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPERSONTRIPLE);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPERSONTRIPLE Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPERSONTRIPLE);
    }

}
