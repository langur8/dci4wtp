<?php

namespace Axess\Dci4Wtp;

class D4WTPLOCKSHOPCARTREQUEST
{

    /**
     * @var float $NLOCKPOSNO
     */
    protected $NLOCKPOSNO = null;

    /**
     * @var float $NLOCKPROJNO
     */
    protected $NLOCKPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var float $NSHOPPINGCARTPOSNO
     */
    protected $NSHOPPINGCARTPOSNO = null;

    /**
     * @var float $NSHOPPINGCARTPROJNO
     */
    protected $NSHOPPINGCARTPROJNO = null;

    /**
     * @var float $NTIMEOUTINMINUTES
     */
    protected $NTIMEOUTINMINUTES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNLOCKPOSNO()
    {
      return $this->NLOCKPOSNO;
    }

    /**
     * @param float $NLOCKPOSNO
     * @return \Axess\Dci4Wtp\D4WTPLOCKSHOPCARTREQUEST
     */
    public function setNLOCKPOSNO($NLOCKPOSNO)
    {
      $this->NLOCKPOSNO = $NLOCKPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLOCKPROJNO()
    {
      return $this->NLOCKPROJNO;
    }

    /**
     * @param float $NLOCKPROJNO
     * @return \Axess\Dci4Wtp\D4WTPLOCKSHOPCARTREQUEST
     */
    public function setNLOCKPROJNO($NLOCKPROJNO)
    {
      $this->NLOCKPROJNO = $NLOCKPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPLOCKSHOPCARTREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPLOCKSHOPCARTREQUEST
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPOSNO()
    {
      return $this->NSHOPPINGCARTPOSNO;
    }

    /**
     * @param float $NSHOPPINGCARTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPLOCKSHOPCARTREQUEST
     */
    public function setNSHOPPINGCARTPOSNO($NSHOPPINGCARTPOSNO)
    {
      $this->NSHOPPINGCARTPOSNO = $NSHOPPINGCARTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPROJNO()
    {
      return $this->NSHOPPINGCARTPROJNO;
    }

    /**
     * @param float $NSHOPPINGCARTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPLOCKSHOPCARTREQUEST
     */
    public function setNSHOPPINGCARTPROJNO($NSHOPPINGCARTPROJNO)
    {
      $this->NSHOPPINGCARTPROJNO = $NSHOPPINGCARTPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTIMEOUTINMINUTES()
    {
      return $this->NTIMEOUTINMINUTES;
    }

    /**
     * @param float $NTIMEOUTINMINUTES
     * @return \Axess\Dci4Wtp\D4WTPLOCKSHOPCARTREQUEST
     */
    public function setNTIMEOUTINMINUTES($NTIMEOUTINMINUTES)
    {
      $this->NTIMEOUTINMINUTES = $NTIMEOUTINMINUTES;
      return $this;
    }

}
