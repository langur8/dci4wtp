<?php

namespace Axess\Dci4Wtp;

class D4WTPMSGRENTALRES
{

    /**
     * @var ArrayOfD4WTPLOCKERMEDIAIDS $CTLOCKERMEDIAIDS
     */
    protected $CTLOCKERMEDIAIDS = null;

    /**
     * @var float $NARCNR
     */
    protected $NARCNR = null;

    /**
     * @var float $NINTORDERNR
     */
    protected $NINTORDERNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPLOCKERMEDIAIDS
     */
    public function getCTLOCKERMEDIAIDS()
    {
      return $this->CTLOCKERMEDIAIDS;
    }

    /**
     * @param ArrayOfD4WTPLOCKERMEDIAIDS $CTLOCKERMEDIAIDS
     * @return \Axess\Dci4Wtp\D4WTPMSGRENTALRES
     */
    public function setCTLOCKERMEDIAIDS($CTLOCKERMEDIAIDS)
    {
      $this->CTLOCKERMEDIAIDS = $CTLOCKERMEDIAIDS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARCNR()
    {
      return $this->NARCNR;
    }

    /**
     * @param float $NARCNR
     * @return \Axess\Dci4Wtp\D4WTPMSGRENTALRES
     */
    public function setNARCNR($NARCNR)
    {
      $this->NARCNR = $NARCNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNINTORDERNR()
    {
      return $this->NINTORDERNR;
    }

    /**
     * @param float $NINTORDERNR
     * @return \Axess\Dci4Wtp\D4WTPMSGRENTALRES
     */
    public function setNINTORDERNR($NINTORDERNR)
    {
      $this->NINTORDERNR = $NINTORDERNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPMSGRENTALRES
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

}
