<?php

namespace Axess\Dci4Wtp;

class createPOEBookingWithManualCertificate
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var CreatePOEBookingWithManualCertRequest $request
     */
    protected $request = null;

    /**
     * @param float $i_nSessionID
     * @param CreatePOEBookingWithManualCertRequest $request
     */
    public function __construct($i_nSessionID, $request)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->request = $request;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\createPOEBookingWithManualCertificate
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return CreatePOEBookingWithManualCertRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param CreatePOEBookingWithManualCertRequest $request
     * @return \Axess\Dci4Wtp\createPOEBookingWithManualCertificate
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
