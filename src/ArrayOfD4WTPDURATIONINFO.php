<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPDURATIONINFO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPDURATIONINFO[] $D4WTPDURATIONINFO
     */
    protected $D4WTPDURATIONINFO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPDURATIONINFO[]
     */
    public function getD4WTPDURATIONINFO()
    {
      return $this->D4WTPDURATIONINFO;
    }

    /**
     * @param D4WTPDURATIONINFO[] $D4WTPDURATIONINFO
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPDURATIONINFO
     */
    public function setD4WTPDURATIONINFO(array $D4WTPDURATIONINFO = null)
    {
      $this->D4WTPDURATIONINFO = $D4WTPDURATIONINFO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPDURATIONINFO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPDURATIONINFO
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPDURATIONINFO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPDURATIONINFO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPDURATIONINFO[] = $value;
      } else {
        $this->D4WTPDURATIONINFO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPDURATIONINFO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPDURATIONINFO Return the current element
     */
    public function current()
    {
      return current($this->D4WTPDURATIONINFO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPDURATIONINFO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPDURATIONINFO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPDURATIONINFO);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPDURATIONINFO Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPDURATIONINFO);
    }

}
