<?php

namespace Axess\Dci4Wtp;

class D4WTPDCCONTENT
{

    /**
     * @var string $SZDCCONTENT
     */
    protected $SZDCCONTENT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZDCCONTENT()
    {
      return $this->SZDCCONTENT;
    }

    /**
     * @param string $SZDCCONTENT
     * @return \Axess\Dci4Wtp\D4WTPDCCONTENT
     */
    public function setSZDCCONTENT($SZDCCONTENT)
    {
      $this->SZDCCONTENT = $SZDCCONTENT;
      return $this;
    }

}
