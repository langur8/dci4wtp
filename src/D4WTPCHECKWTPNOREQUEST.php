<?php

namespace Axess\Dci4Wtp;

class D4WTPCHECKWTPNOREQUEST
{

    /**
     * @var D4WTPPRODUCT $CTPRODUCT
     */
    protected $CTPRODUCT = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZACCEPTANCENO
     */
    protected $SZACCEPTANCENO = null;

    /**
     * @var string $SZCHECKSUM
     */
    protected $SZCHECKSUM = null;

    /**
     * @var string $SZCHIPID
     */
    protected $SZCHIPID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPRODUCT
     */
    public function getCTPRODUCT()
    {
      return $this->CTPRODUCT;
    }

    /**
     * @param D4WTPPRODUCT $CTPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNOREQUEST
     */
    public function setCTPRODUCT($CTPRODUCT)
    {
      $this->CTPRODUCT = $CTPRODUCT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNOREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNOREQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZACCEPTANCENO()
    {
      return $this->SZACCEPTANCENO;
    }

    /**
     * @param string $SZACCEPTANCENO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNOREQUEST
     */
    public function setSZACCEPTANCENO($SZACCEPTANCENO)
    {
      $this->SZACCEPTANCENO = $SZACCEPTANCENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCHECKSUM()
    {
      return $this->SZCHECKSUM;
    }

    /**
     * @param string $SZCHECKSUM
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNOREQUEST
     */
    public function setSZCHECKSUM($SZCHECKSUM)
    {
      $this->SZCHECKSUM = $SZCHECKSUM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCHIPID()
    {
      return $this->SZCHIPID;
    }

    /**
     * @param string $SZCHIPID
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNOREQUEST
     */
    public function setSZCHIPID($SZCHIPID)
    {
      $this->SZCHIPID = $SZCHIPID;
      return $this;
    }

}
