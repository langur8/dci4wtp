<?php

namespace Axess\Dci4Wtp;

class D4WTPCREDITCARDREFDATA
{

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var float $NCTCARDTYPENO
     */
    protected $NCTCARDTYPENO = null;

    /**
     * @var float $NMEMBERSHIPTYPENO
     */
    protected $NMEMBERSHIPTYPENO = null;

    /**
     * @var string $SZCARDOWNER
     */
    protected $SZCARDOWNER = null;

    /**
     * @var string $SZCTCARDREFID
     */
    protected $SZCTCARDREFID = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZEXPIRYDATE
     */
    protected $SZEXPIRYDATE = null;

    /**
     * @var string $SZMASKEDCTCARDNO
     */
    protected $SZMASKEDCTCARDNO = null;

    /**
     * @var string $SZUPDATEDATE
     */
    protected $SZUPDATEDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPCREDITCARDREFDATA
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCTCARDTYPENO()
    {
      return $this->NCTCARDTYPENO;
    }

    /**
     * @param float $NCTCARDTYPENO
     * @return \Axess\Dci4Wtp\D4WTPCREDITCARDREFDATA
     */
    public function setNCTCARDTYPENO($NCTCARDTYPENO)
    {
      $this->NCTCARDTYPENO = $NCTCARDTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMEMBERSHIPTYPENO()
    {
      return $this->NMEMBERSHIPTYPENO;
    }

    /**
     * @param float $NMEMBERSHIPTYPENO
     * @return \Axess\Dci4Wtp\D4WTPCREDITCARDREFDATA
     */
    public function setNMEMBERSHIPTYPENO($NMEMBERSHIPTYPENO)
    {
      $this->NMEMBERSHIPTYPENO = $NMEMBERSHIPTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDOWNER()
    {
      return $this->SZCARDOWNER;
    }

    /**
     * @param string $SZCARDOWNER
     * @return \Axess\Dci4Wtp\D4WTPCREDITCARDREFDATA
     */
    public function setSZCARDOWNER($SZCARDOWNER)
    {
      $this->SZCARDOWNER = $SZCARDOWNER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCTCARDREFID()
    {
      return $this->SZCTCARDREFID;
    }

    /**
     * @param string $SZCTCARDREFID
     * @return \Axess\Dci4Wtp\D4WTPCREDITCARDREFDATA
     */
    public function setSZCTCARDREFID($SZCTCARDREFID)
    {
      $this->SZCTCARDREFID = $SZCTCARDREFID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPCREDITCARDREFDATA
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXPIRYDATE()
    {
      return $this->SZEXPIRYDATE;
    }

    /**
     * @param string $SZEXPIRYDATE
     * @return \Axess\Dci4Wtp\D4WTPCREDITCARDREFDATA
     */
    public function setSZEXPIRYDATE($SZEXPIRYDATE)
    {
      $this->SZEXPIRYDATE = $SZEXPIRYDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASKEDCTCARDNO()
    {
      return $this->SZMASKEDCTCARDNO;
    }

    /**
     * @param string $SZMASKEDCTCARDNO
     * @return \Axess\Dci4Wtp\D4WTPCREDITCARDREFDATA
     */
    public function setSZMASKEDCTCARDNO($SZMASKEDCTCARDNO)
    {
      $this->SZMASKEDCTCARDNO = $SZMASKEDCTCARDNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUPDATEDATE()
    {
      return $this->SZUPDATEDATE;
    }

    /**
     * @param string $SZUPDATEDATE
     * @return \Axess\Dci4Wtp\D4WTPCREDITCARDREFDATA
     */
    public function setSZUPDATEDATE($SZUPDATEDATE)
    {
      $this->SZUPDATEDATE = $SZUPDATEDATE;
      return $this;
    }

}
