<?php

namespace Axess\Dci4Wtp;

class D4WTPFORGOTPASSRES
{

    /**
     * @var float $NERRORNR
     */
    protected $NERRORNR = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZEXPIRESAT
     */
    protected $SZEXPIRESAT = null;

    /**
     * @var string $SZTOKEN
     */
    protected $SZTOKEN = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNR()
    {
      return $this->NERRORNR;
    }

    /**
     * @param float $NERRORNR
     * @return \Axess\Dci4Wtp\D4WTPFORGOTPASSRES
     */
    public function setNERRORNR($NERRORNR)
    {
      $this->NERRORNR = $NERRORNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\D4WTPFORGOTPASSRES
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPFORGOTPASSRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXPIRESAT()
    {
      return $this->SZEXPIRESAT;
    }

    /**
     * @param string $SZEXPIRESAT
     * @return \Axess\Dci4Wtp\D4WTPFORGOTPASSRES
     */
    public function setSZEXPIRESAT($SZEXPIRESAT)
    {
      $this->SZEXPIRESAT = $SZEXPIRESAT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTOKEN()
    {
      return $this->SZTOKEN;
    }

    /**
     * @param string $SZTOKEN
     * @return \Axess\Dci4Wtp\D4WTPFORGOTPASSRES
     */
    public function setSZTOKEN($SZTOKEN)
    {
      $this->SZTOKEN = $SZTOKEN;
      return $this;
    }

}
