<?php

namespace Axess\Dci4Wtp;

class D4WTPGETPROMPTLISTRESULT
{

    /**
     * @var ArrayOfD4WTPPROMPTTYPES $ACTPROMPTTYPES
     */
    protected $ACTPROMPTTYPES = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPROMPTTYPES
     */
    public function getACTPROMPTTYPES()
    {
      return $this->ACTPROMPTTYPES;
    }

    /**
     * @param ArrayOfD4WTPPROMPTTYPES $ACTPROMPTTYPES
     * @return \Axess\Dci4Wtp\D4WTPGETPROMPTLISTRESULT
     */
    public function setACTPROMPTTYPES($ACTPROMPTTYPES)
    {
      $this->ACTPROMPTTYPES = $ACTPROMPTTYPES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETPROMPTLISTRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETPROMPTLISTRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
