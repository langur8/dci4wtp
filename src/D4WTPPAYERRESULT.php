<?php

namespace Axess\Dci4Wtp;

class D4WTPPAYERRESULT
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPAYERPERSNR
     */
    protected $NPAYERPERSNR = null;

    /**
     * @var float $NPAYERPERSPOSNR
     */
    protected $NPAYERPERSPOSNR = null;

    /**
     * @var float $NPAYERPERSPROJNR
     */
    protected $NPAYERPERSPROJNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPAYERRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERPERSNR()
    {
      return $this->NPAYERPERSNR;
    }

    /**
     * @param float $NPAYERPERSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYERRESULT
     */
    public function setNPAYERPERSNR($NPAYERPERSNR)
    {
      $this->NPAYERPERSNR = $NPAYERPERSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERPERSPOSNR()
    {
      return $this->NPAYERPERSPOSNR;
    }

    /**
     * @param float $NPAYERPERSPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYERRESULT
     */
    public function setNPAYERPERSPOSNR($NPAYERPERSPOSNR)
    {
      $this->NPAYERPERSPOSNR = $NPAYERPERSPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERPERSPROJNR()
    {
      return $this->NPAYERPERSPROJNR;
    }

    /**
     * @param float $NPAYERPERSPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPAYERRESULT
     */
    public function setNPAYERPERSPROJNR($NPAYERPERSPROJNR)
    {
      $this->NPAYERPERSPROJNR = $NPAYERPERSPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPAYERRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
