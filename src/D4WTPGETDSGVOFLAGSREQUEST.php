<?php

namespace Axess\Dci4Wtp;

class D4WTPGETDSGVOFLAGSREQUEST
{

    /**
     * @var ArrayOfD4WTPPERSONTRIPLE $ACTPERSONTRIPLE
     */
    protected $ACTPERSONTRIPLE = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPERSONTRIPLE
     */
    public function getACTPERSONTRIPLE()
    {
      return $this->ACTPERSONTRIPLE;
    }

    /**
     * @param ArrayOfD4WTPPERSONTRIPLE $ACTPERSONTRIPLE
     * @return \Axess\Dci4Wtp\D4WTPGETDSGVOFLAGSREQUEST
     */
    public function setACTPERSONTRIPLE($ACTPERSONTRIPLE)
    {
      $this->ACTPERSONTRIPLE = $ACTPERSONTRIPLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETDSGVOFLAGSREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
