<?php

namespace Axess\Dci4Wtp;

class getModulVersionResponse
{

    /**
     * @var string $getModulVersionResult
     */
    protected $getModulVersionResult = null;

    /**
     * @param string $getModulVersionResult
     */
    public function __construct($getModulVersionResult)
    {
      $this->getModulVersionResult = $getModulVersionResult;
    }

    /**
     * @return string
     */
    public function getGetModulVersionResult()
    {
      return $this->getModulVersionResult;
    }

    /**
     * @param string $getModulVersionResult
     * @return \Axess\Dci4Wtp\getModulVersionResponse
     */
    public function setGetModulVersionResult($getModulVersionResult)
    {
      $this->getModulVersionResult = $getModulVersionResult;
      return $this;
    }

}
