<?php

namespace Axess\Dci4Wtp;

class setPromptData
{

    /**
     * @var D4WTPSETPROMPTDATAREQUEST $i_ctSetPromptDataReq
     */
    protected $i_ctSetPromptDataReq = null;

    /**
     * @param D4WTPSETPROMPTDATAREQUEST $i_ctSetPromptDataReq
     */
    public function __construct($i_ctSetPromptDataReq)
    {
      $this->i_ctSetPromptDataReq = $i_ctSetPromptDataReq;
    }

    /**
     * @return D4WTPSETPROMPTDATAREQUEST
     */
    public function getI_ctSetPromptDataReq()
    {
      return $this->i_ctSetPromptDataReq;
    }

    /**
     * @param D4WTPSETPROMPTDATAREQUEST $i_ctSetPromptDataReq
     * @return \Axess\Dci4Wtp\setPromptData
     */
    public function setI_ctSetPromptDataReq($i_ctSetPromptDataReq)
    {
      $this->i_ctSetPromptDataReq = $i_ctSetPromptDataReq;
      return $this;
    }

}
