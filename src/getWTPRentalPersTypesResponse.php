<?php

namespace Axess\Dci4Wtp;

class getWTPRentalPersTypesResponse
{

    /**
     * @var D4WTPRENTALPERSTYPERESULT $getWTPRentalPersTypesResult
     */
    protected $getWTPRentalPersTypesResult = null;

    /**
     * @param D4WTPRENTALPERSTYPERESULT $getWTPRentalPersTypesResult
     */
    public function __construct($getWTPRentalPersTypesResult)
    {
      $this->getWTPRentalPersTypesResult = $getWTPRentalPersTypesResult;
    }

    /**
     * @return D4WTPRENTALPERSTYPERESULT
     */
    public function getGetWTPRentalPersTypesResult()
    {
      return $this->getWTPRentalPersTypesResult;
    }

    /**
     * @param D4WTPRENTALPERSTYPERESULT $getWTPRentalPersTypesResult
     * @return \Axess\Dci4Wtp\getWTPRentalPersTypesResponse
     */
    public function setGetWTPRentalPersTypesResult($getWTPRentalPersTypesResult)
    {
      $this->getWTPRentalPersTypesResult = $getWTPRentalPersTypesResult;
      return $this;
    }

}
