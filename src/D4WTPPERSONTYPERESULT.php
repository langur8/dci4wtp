<?php

namespace Axess\Dci4Wtp;

class D4WTPPERSONTYPERESULT
{

    /**
     * @var ArrayOfD4WTPPERSONTYPE $ACTPERSONTYPES
     */
    protected $ACTPERSONTYPES = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPERSONTYPE
     */
    public function getACTPERSONTYPES()
    {
      return $this->ACTPERSONTYPES;
    }

    /**
     * @param ArrayOfD4WTPPERSONTYPE $ACTPERSONTYPES
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPERESULT
     */
    public function setACTPERSONTYPES($ACTPERSONTYPES)
    {
      $this->ACTPERSONTYPES = $ACTPERSONTYPES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPERESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPERESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
