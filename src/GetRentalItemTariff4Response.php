<?php

namespace Axess\Dci4Wtp;

class GetRentalItemTariff4Response
{

    /**
     * @var D4WTPGETRENTALITEMTARIFF3RES $GetRentalItemTariff4Result
     */
    protected $GetRentalItemTariff4Result = null;

    /**
     * @param D4WTPGETRENTALITEMTARIFF3RES $GetRentalItemTariff4Result
     */
    public function __construct($GetRentalItemTariff4Result)
    {
      $this->GetRentalItemTariff4Result = $GetRentalItemTariff4Result;
    }

    /**
     * @return D4WTPGETRENTALITEMTARIFF3RES
     */
    public function getGetRentalItemTariff4Result()
    {
      return $this->GetRentalItemTariff4Result;
    }

    /**
     * @param D4WTPGETRENTALITEMTARIFF3RES $GetRentalItemTariff4Result
     * @return \Axess\Dci4Wtp\GetRentalItemTariff4Response
     */
    public function setGetRentalItemTariff4Result($GetRentalItemTariff4Result)
    {
      $this->GetRentalItemTariff4Result = $GetRentalItemTariff4Result;
      return $this;
    }

}
