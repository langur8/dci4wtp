<?php

namespace Axess\Dci4Wtp;

class D4WTPPERSTYPERULERESULT
{

    /**
     * @var ArrayOfD4WTPPERSTYPERULE $ACTPERSTYPERULES
     */
    protected $ACTPERSTYPERULES = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPERSTYPERULE
     */
    public function getACTPERSTYPERULES()
    {
      return $this->ACTPERSTYPERULES;
    }

    /**
     * @param ArrayOfD4WTPPERSTYPERULE $ACTPERSTYPERULES
     * @return \Axess\Dci4Wtp\D4WTPPERSTYPERULERESULT
     */
    public function setACTPERSTYPERULES($ACTPERSTYPERULES)
    {
      $this->ACTPERSTYPERULES = $ACTPERSTYPERULES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPERSTYPERULERESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPERSTYPERULERESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
