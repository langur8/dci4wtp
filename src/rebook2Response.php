<?php

namespace Axess\Dci4Wtp;

class rebook2Response
{

    /**
     * @var D4WTPREBOOKRESULT $rebook2Result
     */
    protected $rebook2Result = null;

    /**
     * @param D4WTPREBOOKRESULT $rebook2Result
     */
    public function __construct($rebook2Result)
    {
      $this->rebook2Result = $rebook2Result;
    }

    /**
     * @return D4WTPREBOOKRESULT
     */
    public function getRebook2Result()
    {
      return $this->rebook2Result;
    }

    /**
     * @param D4WTPREBOOKRESULT $rebook2Result
     * @return \Axess\Dci4Wtp\rebook2Response
     */
    public function setRebook2Result($rebook2Result)
    {
      $this->rebook2Result = $rebook2Result;
      return $this;
    }

}
