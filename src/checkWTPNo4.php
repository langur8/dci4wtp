<?php

namespace Axess\Dci4Wtp;

class checkWTPNo4
{

    /**
     * @var D4WTPCHECKWTPNO2REQUEST $i_ctCheckWTPNo2Req
     */
    protected $i_ctCheckWTPNo2Req = null;

    /**
     * @param D4WTPCHECKWTPNO2REQUEST $i_ctCheckWTPNo2Req
     */
    public function __construct($i_ctCheckWTPNo2Req)
    {
      $this->i_ctCheckWTPNo2Req = $i_ctCheckWTPNo2Req;
    }

    /**
     * @return D4WTPCHECKWTPNO2REQUEST
     */
    public function getI_ctCheckWTPNo2Req()
    {
      return $this->i_ctCheckWTPNo2Req;
    }

    /**
     * @param D4WTPCHECKWTPNO2REQUEST $i_ctCheckWTPNo2Req
     * @return \Axess\Dci4Wtp\checkWTPNo4
     */
    public function setI_ctCheckWTPNo2Req($i_ctCheckWTPNo2Req)
    {
      $this->i_ctCheckWTPNo2Req = $i_ctCheckWTPNo2Req;
      return $this;
    }

}
