<?php

namespace Axess\Dci4Wtp;

class D4WTPCOMPANYREQUEST
{

    /**
     * @var D4WTPCOMPANYWTPFIELDS $COMPANYWTPFIELDS
     */
    protected $COMPANYWTPFIELDS = null;

    /**
     * @var ArrayOfCOMPANYCONTACTPERSON $COMPCONTACTPERSON
     */
    protected $COMPCONTACTPERSON = null;

    /**
     * @var D4WTPCOMPEXHIBITION $COMPEXHIBITION
     */
    protected $COMPEXHIBITION = null;

    /**
     * @var float $NCOMPNR
     */
    protected $NCOMPNR = null;

    /**
     * @var float $NCOMPPOSNR
     */
    protected $NCOMPPOSNR = null;

    /**
     * @var float $NCOMPPROJNR
     */
    protected $NCOMPPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var ArrayOfSPECIALADDRESS $SPECIALADDRESS
     */
    protected $SPECIALADDRESS = null;

    /**
     * @var string $SZIUD
     */
    protected $SZIUD = null;

    /**
     * @var D4WTPCOMPANY $WTPCOMPANY
     */
    protected $WTPCOMPANY = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCOMPANYWTPFIELDS
     */
    public function getCOMPANYWTPFIELDS()
    {
      return $this->COMPANYWTPFIELDS;
    }

    /**
     * @param D4WTPCOMPANYWTPFIELDS $COMPANYWTPFIELDS
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYREQUEST
     */
    public function setCOMPANYWTPFIELDS($COMPANYWTPFIELDS)
    {
      $this->COMPANYWTPFIELDS = $COMPANYWTPFIELDS;
      return $this;
    }

    /**
     * @return ArrayOfCOMPANYCONTACTPERSON
     */
    public function getCOMPCONTACTPERSON()
    {
      return $this->COMPCONTACTPERSON;
    }

    /**
     * @param ArrayOfCOMPANYCONTACTPERSON $COMPCONTACTPERSON
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYREQUEST
     */
    public function setCOMPCONTACTPERSON($COMPCONTACTPERSON)
    {
      $this->COMPCONTACTPERSON = $COMPCONTACTPERSON;
      return $this;
    }

    /**
     * @return D4WTPCOMPEXHIBITION
     */
    public function getCOMPEXHIBITION()
    {
      return $this->COMPEXHIBITION;
    }

    /**
     * @param D4WTPCOMPEXHIBITION $COMPEXHIBITION
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYREQUEST
     */
    public function setCOMPEXHIBITION($COMPEXHIBITION)
    {
      $this->COMPEXHIBITION = $COMPEXHIBITION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPNR()
    {
      return $this->NCOMPNR;
    }

    /**
     * @param float $NCOMPNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYREQUEST
     */
    public function setNCOMPNR($NCOMPNR)
    {
      $this->NCOMPNR = $NCOMPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPPOSNR()
    {
      return $this->NCOMPPOSNR;
    }

    /**
     * @param float $NCOMPPOSNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYREQUEST
     */
    public function setNCOMPPOSNR($NCOMPPOSNR)
    {
      $this->NCOMPPOSNR = $NCOMPPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPPROJNR()
    {
      return $this->NCOMPPROJNR;
    }

    /**
     * @param float $NCOMPPROJNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYREQUEST
     */
    public function setNCOMPPROJNR($NCOMPPROJNR)
    {
      $this->NCOMPPROJNR = $NCOMPPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return ArrayOfSPECIALADDRESS
     */
    public function getSPECIALADDRESS()
    {
      return $this->SPECIALADDRESS;
    }

    /**
     * @param ArrayOfSPECIALADDRESS $SPECIALADDRESS
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYREQUEST
     */
    public function setSPECIALADDRESS($SPECIALADDRESS)
    {
      $this->SPECIALADDRESS = $SPECIALADDRESS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZIUD()
    {
      return $this->SZIUD;
    }

    /**
     * @param string $SZIUD
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYREQUEST
     */
    public function setSZIUD($SZIUD)
    {
      $this->SZIUD = $SZIUD;
      return $this;
    }

    /**
     * @return D4WTPCOMPANY
     */
    public function getWTPCOMPANY()
    {
      return $this->WTPCOMPANY;
    }

    /**
     * @param D4WTPCOMPANY $WTPCOMPANY
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYREQUEST
     */
    public function setWTPCOMPANY($WTPCOMPANY)
    {
      $this->WTPCOMPANY = $WTPCOMPANY;
      return $this;
    }

}
