<?php

namespace Axess\Dci4Wtp;

class getEmoneyAccountDetailsResponse
{

    /**
     * @var D4WTPEMONEYACCOUNTDETAILSRES $getEmoneyAccountDetailsResult
     */
    protected $getEmoneyAccountDetailsResult = null;

    /**
     * @param D4WTPEMONEYACCOUNTDETAILSRES $getEmoneyAccountDetailsResult
     */
    public function __construct($getEmoneyAccountDetailsResult)
    {
      $this->getEmoneyAccountDetailsResult = $getEmoneyAccountDetailsResult;
    }

    /**
     * @return D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function getGetEmoneyAccountDetailsResult()
    {
      return $this->getEmoneyAccountDetailsResult;
    }

    /**
     * @param D4WTPEMONEYACCOUNTDETAILSRES $getEmoneyAccountDetailsResult
     * @return \Axess\Dci4Wtp\getEmoneyAccountDetailsResponse
     */
    public function setGetEmoneyAccountDetailsResult($getEmoneyAccountDetailsResult)
    {
      $this->getEmoneyAccountDetailsResult = $getEmoneyAccountDetailsResult;
      return $this;
    }

}
