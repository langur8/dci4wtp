<?php

namespace Axess\Dci4Wtp;

class D4WTPGETSHOPCARTDATA5RES
{

    /**
     * @var ArrayOfD4WTPSHOPPINGCARTDATA5 $ACTSHOPPINGCARTDATA
     */
    protected $ACTSHOPPINGCARTDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSHOPPINGCARTDATA5
     */
    public function getACTSHOPPINGCARTDATA()
    {
      return $this->ACTSHOPPINGCARTDATA;
    }

    /**
     * @param ArrayOfD4WTPSHOPPINGCARTDATA5 $ACTSHOPPINGCARTDATA
     * @return \Axess\Dci4Wtp\D4WTPGETSHOPCARTDATA5RES
     */
    public function setACTSHOPPINGCARTDATA($ACTSHOPPINGCARTDATA)
    {
      $this->ACTSHOPPINGCARTDATA = $ACTSHOPPINGCARTDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETSHOPCARTDATA5RES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETSHOPCARTDATA5RES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
