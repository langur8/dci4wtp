<?php

namespace Axess\Dci4Wtp;

class OBI4PSPSWISSPASSABOSTATUS
{

    /**
     * @var float $BISGUELTIGGA
     */
    protected $BISGUELTIGGA = null;

    /**
     * @var float $BISGUELTIGGA_1
     */
    protected $BISGUELTIGGA_1 = null;

    /**
     * @var float $BISGUELTIGGA_2
     */
    protected $BISGUELTIGGA_2 = null;

    /**
     * @var float $BISGUELTIGHTA
     */
    protected $BISGUELTIGHTA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISGUELTIGGA()
    {
      return $this->BISGUELTIGGA;
    }

    /**
     * @param float $BISGUELTIGGA
     * @return \Axess\Dci4Wtp\OBI4PSPSWISSPASSABOSTATUS
     */
    public function setBISGUELTIGGA($BISGUELTIGGA)
    {
      $this->BISGUELTIGGA = $BISGUELTIGGA;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISGUELTIGGA_1()
    {
      return $this->BISGUELTIGGA_1;
    }

    /**
     * @param float $BISGUELTIGGA_1
     * @return \Axess\Dci4Wtp\OBI4PSPSWISSPASSABOSTATUS
     */
    public function setBISGUELTIGGA_1($BISGUELTIGGA_1)
    {
      $this->BISGUELTIGGA_1 = $BISGUELTIGGA_1;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISGUELTIGGA_2()
    {
      return $this->BISGUELTIGGA_2;
    }

    /**
     * @param float $BISGUELTIGGA_2
     * @return \Axess\Dci4Wtp\OBI4PSPSWISSPASSABOSTATUS
     */
    public function setBISGUELTIGGA_2($BISGUELTIGGA_2)
    {
      $this->BISGUELTIGGA_2 = $BISGUELTIGGA_2;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISGUELTIGHTA()
    {
      return $this->BISGUELTIGHTA;
    }

    /**
     * @param float $BISGUELTIGHTA
     * @return \Axess\Dci4Wtp\OBI4PSPSWISSPASSABOSTATUS
     */
    public function setBISGUELTIGHTA($BISGUELTIGHTA)
    {
      $this->BISGUELTIGHTA = $BISGUELTIGHTA;
      return $this;
    }

}
