<?php

namespace Axess\Dci4Wtp;

class msgIssueTicket7Response
{

    /**
     * @var D4WTPMSGISSUETICKETRESULT7 $msgIssueTicket7Result
     */
    protected $msgIssueTicket7Result = null;

    /**
     * @param D4WTPMSGISSUETICKETRESULT7 $msgIssueTicket7Result
     */
    public function __construct($msgIssueTicket7Result)
    {
      $this->msgIssueTicket7Result = $msgIssueTicket7Result;
    }

    /**
     * @return D4WTPMSGISSUETICKETRESULT7
     */
    public function getMsgIssueTicket7Result()
    {
      return $this->msgIssueTicket7Result;
    }

    /**
     * @param D4WTPMSGISSUETICKETRESULT7 $msgIssueTicket7Result
     * @return \Axess\Dci4Wtp\msgIssueTicket7Response
     */
    public function setMsgIssueTicket7Result($msgIssueTicket7Result)
    {
      $this->msgIssueTicket7Result = $msgIssueTicket7Result;
      return $this;
    }

}
