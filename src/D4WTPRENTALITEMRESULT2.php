<?php

namespace Axess\Dci4Wtp;

class D4WTPRENTALITEMRESULT2
{

    /**
     * @var ArrayOfD4WTPRENTALITEM2 $ACTWTPRENTALITEM
     */
    protected $ACTWTPRENTALITEM = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPRENTALITEM2
     */
    public function getACTWTPRENTALITEM()
    {
      return $this->ACTWTPRENTALITEM;
    }

    /**
     * @param ArrayOfD4WTPRENTALITEM2 $ACTWTPRENTALITEM
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMRESULT2
     */
    public function setACTWTPRENTALITEM($ACTWTPRENTALITEM)
    {
      $this->ACTWTPRENTALITEM = $ACTWTPRENTALITEM;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMRESULT2
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMRESULT2
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
