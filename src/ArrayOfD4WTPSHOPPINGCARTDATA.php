<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSHOPPINGCARTDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSHOPPINGCARTDATA[] $D4WTPSHOPPINGCARTDATA
     */
    protected $D4WTPSHOPPINGCARTDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSHOPPINGCARTDATA[]
     */
    public function getD4WTPSHOPPINGCARTDATA()
    {
      return $this->D4WTPSHOPPINGCARTDATA;
    }

    /**
     * @param D4WTPSHOPPINGCARTDATA[] $D4WTPSHOPPINGCARTDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSHOPPINGCARTDATA
     */
    public function setD4WTPSHOPPINGCARTDATA(array $D4WTPSHOPPINGCARTDATA = null)
    {
      $this->D4WTPSHOPPINGCARTDATA = $D4WTPSHOPPINGCARTDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSHOPPINGCARTDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSHOPPINGCARTDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSHOPPINGCARTDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSHOPPINGCARTDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSHOPPINGCARTDATA[] = $value;
      } else {
        $this->D4WTPSHOPPINGCARTDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSHOPPINGCARTDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSHOPPINGCARTDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSHOPPINGCARTDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSHOPPINGCARTDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSHOPPINGCARTDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSHOPPINGCARTDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSHOPPINGCARTDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSHOPPINGCARTDATA);
    }

}
