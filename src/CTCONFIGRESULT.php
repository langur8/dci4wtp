<?php

namespace Axess\Dci4Wtp;

class CTCONFIGRESULT
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NORAERRORNO
     */
    protected $NORAERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZORAERRORMESSAGE
     */
    protected $SZORAERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\CTCONFIGRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORAERRORNO()
    {
      return $this->NORAERRORNO;
    }

    /**
     * @param float $NORAERRORNO
     * @return \Axess\Dci4Wtp\CTCONFIGRESULT
     */
    public function setNORAERRORNO($NORAERRORNO)
    {
      $this->NORAERRORNO = $NORAERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\CTCONFIGRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZORAERRORMESSAGE()
    {
      return $this->SZORAERRORMESSAGE;
    }

    /**
     * @param string $SZORAERRORMESSAGE
     * @return \Axess\Dci4Wtp\CTCONFIGRESULT
     */
    public function setSZORAERRORMESSAGE($SZORAERRORMESSAGE)
    {
      $this->SZORAERRORMESSAGE = $SZORAERRORMESSAGE;
      return $this;
    }

}
