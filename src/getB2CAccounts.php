<?php

namespace Axess\Dci4Wtp;

class getB2CAccounts
{

    /**
     * @var D4WTPB2CACCOUNTSREQUEST $i_ctB2CAccReq
     */
    protected $i_ctB2CAccReq = null;

    /**
     * @param D4WTPB2CACCOUNTSREQUEST $i_ctB2CAccReq
     */
    public function __construct($i_ctB2CAccReq)
    {
      $this->i_ctB2CAccReq = $i_ctB2CAccReq;
    }

    /**
     * @return D4WTPB2CACCOUNTSREQUEST
     */
    public function getI_ctB2CAccReq()
    {
      return $this->i_ctB2CAccReq;
    }

    /**
     * @param D4WTPB2CACCOUNTSREQUEST $i_ctB2CAccReq
     * @return \Axess\Dci4Wtp\getB2CAccounts
     */
    public function setI_ctB2CAccReq($i_ctB2CAccReq)
    {
      $this->i_ctB2CAccReq = $i_ctB2CAccReq;
      return $this;
    }

}
