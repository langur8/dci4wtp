<?php

namespace Axess\Dci4Wtp;

class OBI4PSPAXKUNDENDATEN
{

    /**
     * @var float $NPERSKASSANR
     */
    protected $NPERSKASSANR = null;

    /**
     * @var float $NPERSNR
     */
    protected $NPERSNR = null;

    /**
     * @var float $NPERSPROJNR
     */
    protected $NPERSPROJNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPERSKASSANR()
    {
      return $this->NPERSKASSANR;
    }

    /**
     * @param float $NPERSKASSANR
     * @return \Axess\Dci4Wtp\OBI4PSPAXKUNDENDATEN
     */
    public function setNPERSKASSANR($NPERSKASSANR)
    {
      $this->NPERSKASSANR = $NPERSKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNR()
    {
      return $this->NPERSNR;
    }

    /**
     * @param float $NPERSNR
     * @return \Axess\Dci4Wtp\OBI4PSPAXKUNDENDATEN
     */
    public function setNPERSNR($NPERSNR)
    {
      $this->NPERSNR = $NPERSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNR()
    {
      return $this->NPERSPROJNR;
    }

    /**
     * @param float $NPERSPROJNR
     * @return \Axess\Dci4Wtp\OBI4PSPAXKUNDENDATEN
     */
    public function setNPERSPROJNR($NPERSPROJNR)
    {
      $this->NPERSPROJNR = $NPERSPROJNR;
      return $this;
    }

}
