<?php

namespace Axess\Dci4Wtp;

class D4WTPGETGRPCONTINGENT2RESULT
{

    /**
     * @var ArrayOfD4WTPCONTNGNTTICKET4 $ACTCONTNGNTTICKET4
     */
    protected $ACTCONTNGNTTICKET4 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCONTNGNTTICKET4
     */
    public function getACTCONTNGNTTICKET4()
    {
      return $this->ACTCONTNGNTTICKET4;
    }

    /**
     * @param ArrayOfD4WTPCONTNGNTTICKET4 $ACTCONTNGNTTICKET4
     * @return \Axess\Dci4Wtp\D4WTPGETGRPCONTINGENT2RESULT
     */
    public function setACTCONTNGNTTICKET4($ACTCONTNGNTTICKET4)
    {
      $this->ACTCONTNGNTTICKET4 = $ACTCONTNGNTTICKET4;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETGRPCONTINGENT2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETGRPCONTINGENT2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
