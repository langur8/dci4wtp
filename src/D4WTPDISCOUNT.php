<?php

namespace Axess\Dci4Wtp;

class D4WTPDISCOUNT
{

    /**
     * @var float $NDISCOUNTNO
     */
    protected $NDISCOUNTNO = null;

    /**
     * @var float $NDISCOUNTPERCENT
     */
    protected $NDISCOUNTPERCENT = null;

    /**
     * @var float $NDISCOUNTSHEETNO
     */
    protected $NDISCOUNTSHEETNO = null;

    /**
     * @var float $NFIXEDTARIFF
     */
    protected $NFIXEDTARIFF = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var string $SZDISCOUNTNAME
     */
    protected $SZDISCOUNTNAME = null;

    /**
     * @var string $SZDISCOUNTSHEETNAME
     */
    protected $SZDISCOUNTSHEETNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNDISCOUNTNO()
    {
      return $this->NDISCOUNTNO;
    }

    /**
     * @param float $NDISCOUNTNO
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNT
     */
    public function setNDISCOUNTNO($NDISCOUNTNO)
    {
      $this->NDISCOUNTNO = $NDISCOUNTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDISCOUNTPERCENT()
    {
      return $this->NDISCOUNTPERCENT;
    }

    /**
     * @param float $NDISCOUNTPERCENT
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNT
     */
    public function setNDISCOUNTPERCENT($NDISCOUNTPERCENT)
    {
      $this->NDISCOUNTPERCENT = $NDISCOUNTPERCENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDISCOUNTSHEETNO()
    {
      return $this->NDISCOUNTSHEETNO;
    }

    /**
     * @param float $NDISCOUNTSHEETNO
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNT
     */
    public function setNDISCOUNTSHEETNO($NDISCOUNTSHEETNO)
    {
      $this->NDISCOUNTSHEETNO = $NDISCOUNTSHEETNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIXEDTARIFF()
    {
      return $this->NFIXEDTARIFF;
    }

    /**
     * @param float $NFIXEDTARIFF
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNT
     */
    public function setNFIXEDTARIFF($NFIXEDTARIFF)
    {
      $this->NFIXEDTARIFF = $NFIXEDTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNT
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDISCOUNTNAME()
    {
      return $this->SZDISCOUNTNAME;
    }

    /**
     * @param string $SZDISCOUNTNAME
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNT
     */
    public function setSZDISCOUNTNAME($SZDISCOUNTNAME)
    {
      $this->SZDISCOUNTNAME = $SZDISCOUNTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDISCOUNTSHEETNAME()
    {
      return $this->SZDISCOUNTSHEETNAME;
    }

    /**
     * @param string $SZDISCOUNTSHEETNAME
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNT
     */
    public function setSZDISCOUNTSHEETNAME($SZDISCOUNTSHEETNAME)
    {
      $this->SZDISCOUNTSHEETNAME = $SZDISCOUNTSHEETNAME;
      return $this;
    }

}
