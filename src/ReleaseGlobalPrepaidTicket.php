<?php

namespace Axess\Dci4Wtp;

class ReleaseGlobalPrepaidTicket
{

    /**
     * @var D4WTPRELEASEPREPAIDTICKETREQ $i_ctReleasePrepaidReq
     */
    protected $i_ctReleasePrepaidReq = null;

    /**
     * @param D4WTPRELEASEPREPAIDTICKETREQ $i_ctReleasePrepaidReq
     */
    public function __construct($i_ctReleasePrepaidReq)
    {
      $this->i_ctReleasePrepaidReq = $i_ctReleasePrepaidReq;
    }

    /**
     * @return D4WTPRELEASEPREPAIDTICKETREQ
     */
    public function getI_ctReleasePrepaidReq()
    {
      return $this->i_ctReleasePrepaidReq;
    }

    /**
     * @param D4WTPRELEASEPREPAIDTICKETREQ $i_ctReleasePrepaidReq
     * @return \Axess\Dci4Wtp\ReleaseGlobalPrepaidTicket
     */
    public function setI_ctReleasePrepaidReq($i_ctReleasePrepaidReq)
    {
      $this->i_ctReleasePrepaidReq = $i_ctReleasePrepaidReq;
      return $this;
    }

}
