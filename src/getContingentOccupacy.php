<?php

namespace Axess\Dci4Wtp;

class getContingentOccupacy
{

    /**
     * @var D4WTPGETCONTINGENTOCUPACYREQ $i_request
     */
    protected $i_request = null;

    /**
     * @param D4WTPGETCONTINGENTOCUPACYREQ $i_request
     */
    public function __construct($i_request)
    {
      $this->i_request = $i_request;
    }

    /**
     * @return D4WTPGETCONTINGENTOCUPACYREQ
     */
    public function getI_request()
    {
      return $this->i_request;
    }

    /**
     * @param D4WTPGETCONTINGENTOCUPACYREQ $i_request
     * @return \Axess\Dci4Wtp\getContingentOccupacy
     */
    public function setI_request($i_request)
    {
      $this->i_request = $i_request;
      return $this;
    }

}
