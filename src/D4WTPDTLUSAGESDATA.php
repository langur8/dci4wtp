<?php

namespace Axess\Dci4Wtp;

class D4WTPDTLUSAGESDATA
{

    /**
     * @var float $BISCHARGED
     */
    protected $BISCHARGED = null;

    /**
     * @var float $NAUTHPOSNO
     */
    protected $NAUTHPOSNO = null;

    /**
     * @var float $NAUTHPROJNO
     */
    protected $NAUTHPROJNO = null;

    /**
     * @var float $NAUTHSERIALNO
     */
    protected $NAUTHSERIALNO = null;

    /**
     * @var float $NAUTHTARIFF
     */
    protected $NAUTHTARIFF = null;

    /**
     * @var float $NAUTHUNICODENO
     */
    protected $NAUTHUNICODENO = null;

    /**
     * @var float $NDTLPOSNO
     */
    protected $NDTLPOSNO = null;

    /**
     * @var float $NDTLPROJNO
     */
    protected $NDTLPROJNO = null;

    /**
     * @var float $NDTLSERIALNO
     */
    protected $NDTLSERIALNO = null;

    /**
     * @var float $NDTLUNICODENO
     */
    protected $NDTLUNICODENO = null;

    /**
     * @var string $SZAUTHVALIDFROMDATE
     */
    protected $SZAUTHVALIDFROMDATE = null;

    /**
     * @var string $SZAUTHVALIDTODATE
     */
    protected $SZAUTHVALIDTODATE = null;

    /**
     * @var string $SZCARDNO
     */
    protected $SZCARDNO = null;

    /**
     * @var string $SZCLEARINGDATE
     */
    protected $SZCLEARINGDATE = null;

    /**
     * @var string $SZCUSTOMERCARDNAME
     */
    protected $SZCUSTOMERCARDNAME = null;

    /**
     * @var string $SZCUSTOMERCARDSHORTNAME
     */
    protected $SZCUSTOMERCARDSHORTNAME = null;

    /**
     * @var string $SZFIRSTUSAGEDATE
     */
    protected $SZFIRSTUSAGEDATE = null;

    /**
     * @var string $SZLASTUSAGEDATE
     */
    protected $SZLASTUSAGEDATE = null;

    /**
     * @var string $SZTARIFFVALIDITYNAME
     */
    protected $SZTARIFFVALIDITYNAME = null;

    /**
     * @var string $SZTIMESCALENAME
     */
    protected $SZTIMESCALENAME = null;

    /**
     * @var string $SZUSAGEDATE
     */
    protected $SZUSAGEDATE = null;

    /**
     * @var string $SZVALIDFROMDATE
     */
    protected $SZVALIDFROMDATE = null;

    /**
     * @var string $SZVALIDTODATE
     */
    protected $SZVALIDTODATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISCHARGED()
    {
      return $this->BISCHARGED;
    }

    /**
     * @param float $BISCHARGED
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setBISCHARGED($BISCHARGED)
    {
      $this->BISCHARGED = $BISCHARGED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAUTHPOSNO()
    {
      return $this->NAUTHPOSNO;
    }

    /**
     * @param float $NAUTHPOSNO
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setNAUTHPOSNO($NAUTHPOSNO)
    {
      $this->NAUTHPOSNO = $NAUTHPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAUTHPROJNO()
    {
      return $this->NAUTHPROJNO;
    }

    /**
     * @param float $NAUTHPROJNO
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setNAUTHPROJNO($NAUTHPROJNO)
    {
      $this->NAUTHPROJNO = $NAUTHPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAUTHSERIALNO()
    {
      return $this->NAUTHSERIALNO;
    }

    /**
     * @param float $NAUTHSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setNAUTHSERIALNO($NAUTHSERIALNO)
    {
      $this->NAUTHSERIALNO = $NAUTHSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAUTHTARIFF()
    {
      return $this->NAUTHTARIFF;
    }

    /**
     * @param float $NAUTHTARIFF
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setNAUTHTARIFF($NAUTHTARIFF)
    {
      $this->NAUTHTARIFF = $NAUTHTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAUTHUNICODENO()
    {
      return $this->NAUTHUNICODENO;
    }

    /**
     * @param float $NAUTHUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setNAUTHUNICODENO($NAUTHUNICODENO)
    {
      $this->NAUTHUNICODENO = $NAUTHUNICODENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDTLPOSNO()
    {
      return $this->NDTLPOSNO;
    }

    /**
     * @param float $NDTLPOSNO
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setNDTLPOSNO($NDTLPOSNO)
    {
      $this->NDTLPOSNO = $NDTLPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDTLPROJNO()
    {
      return $this->NDTLPROJNO;
    }

    /**
     * @param float $NDTLPROJNO
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setNDTLPROJNO($NDTLPROJNO)
    {
      $this->NDTLPROJNO = $NDTLPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDTLSERIALNO()
    {
      return $this->NDTLSERIALNO;
    }

    /**
     * @param float $NDTLSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setNDTLSERIALNO($NDTLSERIALNO)
    {
      $this->NDTLSERIALNO = $NDTLSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDTLUNICODENO()
    {
      return $this->NDTLUNICODENO;
    }

    /**
     * @param float $NDTLUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setNDTLUNICODENO($NDTLUNICODENO)
    {
      $this->NDTLUNICODENO = $NDTLUNICODENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAUTHVALIDFROMDATE()
    {
      return $this->SZAUTHVALIDFROMDATE;
    }

    /**
     * @param string $SZAUTHVALIDFROMDATE
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setSZAUTHVALIDFROMDATE($SZAUTHVALIDFROMDATE)
    {
      $this->SZAUTHVALIDFROMDATE = $SZAUTHVALIDFROMDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAUTHVALIDTODATE()
    {
      return $this->SZAUTHVALIDTODATE;
    }

    /**
     * @param string $SZAUTHVALIDTODATE
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setSZAUTHVALIDTODATE($SZAUTHVALIDTODATE)
    {
      $this->SZAUTHVALIDTODATE = $SZAUTHVALIDTODATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDNO()
    {
      return $this->SZCARDNO;
    }

    /**
     * @param string $SZCARDNO
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setSZCARDNO($SZCARDNO)
    {
      $this->SZCARDNO = $SZCARDNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCLEARINGDATE()
    {
      return $this->SZCLEARINGDATE;
    }

    /**
     * @param string $SZCLEARINGDATE
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setSZCLEARINGDATE($SZCLEARINGDATE)
    {
      $this->SZCLEARINGDATE = $SZCLEARINGDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTOMERCARDNAME()
    {
      return $this->SZCUSTOMERCARDNAME;
    }

    /**
     * @param string $SZCUSTOMERCARDNAME
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setSZCUSTOMERCARDNAME($SZCUSTOMERCARDNAME)
    {
      $this->SZCUSTOMERCARDNAME = $SZCUSTOMERCARDNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTOMERCARDSHORTNAME()
    {
      return $this->SZCUSTOMERCARDSHORTNAME;
    }

    /**
     * @param string $SZCUSTOMERCARDSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setSZCUSTOMERCARDSHORTNAME($SZCUSTOMERCARDSHORTNAME)
    {
      $this->SZCUSTOMERCARDSHORTNAME = $SZCUSTOMERCARDSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTUSAGEDATE()
    {
      return $this->SZFIRSTUSAGEDATE;
    }

    /**
     * @param string $SZFIRSTUSAGEDATE
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setSZFIRSTUSAGEDATE($SZFIRSTUSAGEDATE)
    {
      $this->SZFIRSTUSAGEDATE = $SZFIRSTUSAGEDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTUSAGEDATE()
    {
      return $this->SZLASTUSAGEDATE;
    }

    /**
     * @param string $SZLASTUSAGEDATE
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setSZLASTUSAGEDATE($SZLASTUSAGEDATE)
    {
      $this->SZLASTUSAGEDATE = $SZLASTUSAGEDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTARIFFVALIDITYNAME()
    {
      return $this->SZTARIFFVALIDITYNAME;
    }

    /**
     * @param string $SZTARIFFVALIDITYNAME
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setSZTARIFFVALIDITYNAME($SZTARIFFVALIDITYNAME)
    {
      $this->SZTARIFFVALIDITYNAME = $SZTARIFFVALIDITYNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTIMESCALENAME()
    {
      return $this->SZTIMESCALENAME;
    }

    /**
     * @param string $SZTIMESCALENAME
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setSZTIMESCALENAME($SZTIMESCALENAME)
    {
      $this->SZTIMESCALENAME = $SZTIMESCALENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUSAGEDATE()
    {
      return $this->SZUSAGEDATE;
    }

    /**
     * @param string $SZUSAGEDATE
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setSZUSAGEDATE($SZUSAGEDATE)
    {
      $this->SZUSAGEDATE = $SZUSAGEDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROMDATE()
    {
      return $this->SZVALIDFROMDATE;
    }

    /**
     * @param string $SZVALIDFROMDATE
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setSZVALIDFROMDATE($SZVALIDFROMDATE)
    {
      $this->SZVALIDFROMDATE = $SZVALIDFROMDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTODATE()
    {
      return $this->SZVALIDTODATE;
    }

    /**
     * @param string $SZVALIDTODATE
     * @return \Axess\Dci4Wtp\D4WTPDTLUSAGESDATA
     */
    public function setSZVALIDTODATE($SZVALIDTODATE)
    {
      $this->SZVALIDTODATE = $SZVALIDTODATE;
      return $this;
    }

}
