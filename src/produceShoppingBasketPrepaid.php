<?php

namespace Axess\Dci4Wtp;

class produceShoppingBasketPrepaid
{

    /**
     * @var D4WTPPRODSHOPPOSPREPAIDREQ $i_ctProdShopPosPrepaidREQ
     */
    protected $i_ctProdShopPosPrepaidREQ = null;

    /**
     * @param D4WTPPRODSHOPPOSPREPAIDREQ $i_ctProdShopPosPrepaidREQ
     */
    public function __construct($i_ctProdShopPosPrepaidREQ)
    {
      $this->i_ctProdShopPosPrepaidREQ = $i_ctProdShopPosPrepaidREQ;
    }

    /**
     * @return D4WTPPRODSHOPPOSPREPAIDREQ
     */
    public function getI_ctProdShopPosPrepaidREQ()
    {
      return $this->i_ctProdShopPosPrepaidREQ;
    }

    /**
     * @param D4WTPPRODSHOPPOSPREPAIDREQ $i_ctProdShopPosPrepaidREQ
     * @return \Axess\Dci4Wtp\produceShoppingBasketPrepaid
     */
    public function setI_ctProdShopPosPrepaidREQ($i_ctProdShopPosPrepaidREQ)
    {
      $this->i_ctProdShopPosPrepaidREQ = $i_ctProdShopPosPrepaidREQ;
      return $this;
    }

}
