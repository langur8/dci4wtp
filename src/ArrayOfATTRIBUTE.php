<?php

namespace Axess\Dci4Wtp;

class ArrayOfATTRIBUTE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ATTRIBUTE[] $ATTRIBUTE
     */
    protected $ATTRIBUTE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ATTRIBUTE[]
     */
    public function getATTRIBUTE()
    {
      return $this->ATTRIBUTE;
    }

    /**
     * @param ATTRIBUTE[] $ATTRIBUTE
     * @return \Axess\Dci4Wtp\ArrayOfATTRIBUTE
     */
    public function setATTRIBUTE(array $ATTRIBUTE = null)
    {
      $this->ATTRIBUTE = $ATTRIBUTE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ATTRIBUTE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ATTRIBUTE
     */
    public function offsetGet($offset)
    {
      return $this->ATTRIBUTE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ATTRIBUTE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ATTRIBUTE[] = $value;
      } else {
        $this->ATTRIBUTE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ATTRIBUTE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ATTRIBUTE Return the current element
     */
    public function current()
    {
      return current($this->ATTRIBUTE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ATTRIBUTE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ATTRIBUTE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ATTRIBUTE);
    }

    /**
     * Countable implementation
     *
     * @return ATTRIBUTE Return count of elements
     */
    public function count()
    {
      return count($this->ATTRIBUTE);
    }

}
