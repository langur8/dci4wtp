<?php

namespace Axess\Dci4Wtp;

class D4WTPGETPROCESSEDTKTDATAREQ
{

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    /**
     * @var string $SZDATEFROM
     */
    protected $SZDATEFROM = null;

    /**
     * @var string $SZDATETO
     */
    protected $SZDATETO = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATAREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATAREQ
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATAREQ
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEFROM()
    {
      return $this->SZDATEFROM;
    }

    /**
     * @param string $SZDATEFROM
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATAREQ
     */
    public function setSZDATEFROM($SZDATEFROM)
    {
      $this->SZDATEFROM = $SZDATEFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATETO()
    {
      return $this->SZDATETO;
    }

    /**
     * @param string $SZDATETO
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATAREQ
     */
    public function setSZDATETO($SZDATETO)
    {
      $this->SZDATETO = $SZDATETO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPGETPROCESSEDTKTDATAREQ
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

}
