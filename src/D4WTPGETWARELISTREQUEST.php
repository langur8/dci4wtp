<?php

namespace Axess\Dci4Wtp;

class D4WTPGETWARELISTREQUEST
{

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZLANGUAGECODE
     */
    protected $SZLANGUAGECODE = null;

    /**
     * @var string $SZVALIDFROMEND
     */
    protected $SZVALIDFROMEND = null;

    /**
     * @var string $SZVALIDFROMSTART
     */
    protected $SZVALIDFROMSTART = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETWARELISTREQUEST
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETWARELISTREQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETWARELISTREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPGETWARELISTREQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLANGUAGECODE()
    {
      return $this->SZLANGUAGECODE;
    }

    /**
     * @param string $SZLANGUAGECODE
     * @return \Axess\Dci4Wtp\D4WTPGETWARELISTREQUEST
     */
    public function setSZLANGUAGECODE($SZLANGUAGECODE)
    {
      $this->SZLANGUAGECODE = $SZLANGUAGECODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROMEND()
    {
      return $this->SZVALIDFROMEND;
    }

    /**
     * @param string $SZVALIDFROMEND
     * @return \Axess\Dci4Wtp\D4WTPGETWARELISTREQUEST
     */
    public function setSZVALIDFROMEND($SZVALIDFROMEND)
    {
      $this->SZVALIDFROMEND = $SZVALIDFROMEND;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROMSTART()
    {
      return $this->SZVALIDFROMSTART;
    }

    /**
     * @param string $SZVALIDFROMSTART
     * @return \Axess\Dci4Wtp\D4WTPGETWARELISTREQUEST
     */
    public function setSZVALIDFROMSTART($SZVALIDFROMSTART)
    {
      $this->SZVALIDFROMSTART = $SZVALIDFROMSTART;
      return $this;
    }

}
