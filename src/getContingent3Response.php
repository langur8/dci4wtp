<?php

namespace Axess\Dci4Wtp;

class getContingent3Response
{

    /**
     * @var D4WTPGETCONTINGENT3RESULT $getContingent3Result
     */
    protected $getContingent3Result = null;

    /**
     * @param D4WTPGETCONTINGENT3RESULT $getContingent3Result
     */
    public function __construct($getContingent3Result)
    {
      $this->getContingent3Result = $getContingent3Result;
    }

    /**
     * @return D4WTPGETCONTINGENT3RESULT
     */
    public function getGetContingent3Result()
    {
      return $this->getContingent3Result;
    }

    /**
     * @param D4WTPGETCONTINGENT3RESULT $getContingent3Result
     * @return \Axess\Dci4Wtp\getContingent3Response
     */
    public function setGetContingent3Result($getContingent3Result)
    {
      $this->getContingent3Result = $getContingent3Result;
      return $this;
    }

}
