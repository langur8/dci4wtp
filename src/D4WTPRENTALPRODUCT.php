<?php

namespace Axess\Dci4Wtp;

class D4WTPRENTALPRODUCT
{

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NRENTALBLOCKNR
     */
    protected $NRENTALBLOCKNR = null;

    /**
     * @var float $NRENTALITEMNR
     */
    protected $NRENTALITEMNR = null;

    /**
     * @var float $NRENTALITEMTYPENR
     */
    protected $NRENTALITEMTYPENR = null;

    /**
     * @var float $NRENTALPERSTYPENR
     */
    protected $NRENTALPERSTYPENR = null;

    /**
     * @var float $NRENTALPOSNR
     */
    protected $NRENTALPOSNR = null;

    /**
     * @var float $NRENTALPROJNR
     */
    protected $NRENTALPROJNR = null;

    /**
     * @var float $NRENTALTRANSNR
     */
    protected $NRENTALTRANSNR = null;

    /**
     * @var string $SZRENTALITEMNAME
     */
    protected $SZRENTALITEMNAME = null;

    /**
     * @var string $SZRENTALITEMTYPENAME
     */
    protected $SZRENTALITEMTYPENAME = null;

    /**
     * @var string $SZRENTALPERSTYPENAME
     */
    protected $SZRENTALPERSTYPENAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCT
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALBLOCKNR()
    {
      return $this->NRENTALBLOCKNR;
    }

    /**
     * @param float $NRENTALBLOCKNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCT
     */
    public function setNRENTALBLOCKNR($NRENTALBLOCKNR)
    {
      $this->NRENTALBLOCKNR = $NRENTALBLOCKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNR()
    {
      return $this->NRENTALITEMNR;
    }

    /**
     * @param float $NRENTALITEMNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCT
     */
    public function setNRENTALITEMNR($NRENTALITEMNR)
    {
      $this->NRENTALITEMNR = $NRENTALITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMTYPENR()
    {
      return $this->NRENTALITEMTYPENR;
    }

    /**
     * @param float $NRENTALITEMTYPENR
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCT
     */
    public function setNRENTALITEMTYPENR($NRENTALITEMTYPENR)
    {
      $this->NRENTALITEMTYPENR = $NRENTALITEMTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPERSTYPENR()
    {
      return $this->NRENTALPERSTYPENR;
    }

    /**
     * @param float $NRENTALPERSTYPENR
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCT
     */
    public function setNRENTALPERSTYPENR($NRENTALPERSTYPENR)
    {
      $this->NRENTALPERSTYPENR = $NRENTALPERSTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPOSNR()
    {
      return $this->NRENTALPOSNR;
    }

    /**
     * @param float $NRENTALPOSNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCT
     */
    public function setNRENTALPOSNR($NRENTALPOSNR)
    {
      $this->NRENTALPOSNR = $NRENTALPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPROJNR()
    {
      return $this->NRENTALPROJNR;
    }

    /**
     * @param float $NRENTALPROJNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCT
     */
    public function setNRENTALPROJNR($NRENTALPROJNR)
    {
      $this->NRENTALPROJNR = $NRENTALPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALTRANSNR()
    {
      return $this->NRENTALTRANSNR;
    }

    /**
     * @param float $NRENTALTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCT
     */
    public function setNRENTALTRANSNR($NRENTALTRANSNR)
    {
      $this->NRENTALTRANSNR = $NRENTALTRANSNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRENTALITEMNAME()
    {
      return $this->SZRENTALITEMNAME;
    }

    /**
     * @param string $SZRENTALITEMNAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCT
     */
    public function setSZRENTALITEMNAME($SZRENTALITEMNAME)
    {
      $this->SZRENTALITEMNAME = $SZRENTALITEMNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRENTALITEMTYPENAME()
    {
      return $this->SZRENTALITEMTYPENAME;
    }

    /**
     * @param string $SZRENTALITEMTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCT
     */
    public function setSZRENTALITEMTYPENAME($SZRENTALITEMTYPENAME)
    {
      $this->SZRENTALITEMTYPENAME = $SZRENTALITEMTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRENTALPERSTYPENAME()
    {
      return $this->SZRENTALPERSTYPENAME;
    }

    /**
     * @param string $SZRENTALPERSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCT
     */
    public function setSZRENTALPERSTYPENAME($SZRENTALPERSTYPENAME)
    {
      $this->SZRENTALPERSTYPENAME = $SZRENTALPERSTYPENAME;
      return $this;
    }

}
