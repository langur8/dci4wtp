<?php

namespace Axess\Dci4Wtp;

class createReport
{

    /**
     * @var D4WTPREPORTREQUEST $i_ctReportReq
     */
    protected $i_ctReportReq = null;

    /**
     * @param D4WTPREPORTREQUEST $i_ctReportReq
     */
    public function __construct($i_ctReportReq)
    {
      $this->i_ctReportReq = $i_ctReportReq;
    }

    /**
     * @return D4WTPREPORTREQUEST
     */
    public function getI_ctReportReq()
    {
      return $this->i_ctReportReq;
    }

    /**
     * @param D4WTPREPORTREQUEST $i_ctReportReq
     * @return \Axess\Dci4Wtp\createReport
     */
    public function setI_ctReportReq($i_ctReportReq)
    {
      $this->i_ctReportReq = $i_ctReportReq;
      return $this;
    }

}
