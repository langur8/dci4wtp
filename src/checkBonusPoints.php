<?php

namespace Axess\Dci4Wtp;

class checkBonusPoints
{

    /**
     * @var D4WTPCHECKBONUSPOINTSREQ $i_ctCheckBonusPointsReq
     */
    protected $i_ctCheckBonusPointsReq = null;

    /**
     * @param D4WTPCHECKBONUSPOINTSREQ $i_ctCheckBonusPointsReq
     */
    public function __construct($i_ctCheckBonusPointsReq)
    {
      $this->i_ctCheckBonusPointsReq = $i_ctCheckBonusPointsReq;
    }

    /**
     * @return D4WTPCHECKBONUSPOINTSREQ
     */
    public function getI_ctCheckBonusPointsReq()
    {
      return $this->i_ctCheckBonusPointsReq;
    }

    /**
     * @param D4WTPCHECKBONUSPOINTSREQ $i_ctCheckBonusPointsReq
     * @return \Axess\Dci4Wtp\checkBonusPoints
     */
    public function setI_ctCheckBonusPointsReq($i_ctCheckBonusPointsReq)
    {
      $this->i_ctCheckBonusPointsReq = $i_ctCheckBonusPointsReq;
      return $this;
    }

}
