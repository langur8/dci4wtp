<?php

namespace Axess\Dci4Wtp;

class D4WTPBLOCKUNBLOCKRESULT
{

    /**
     * @var string $NBLOCKREASONNAME
     */
    protected $NBLOCKREASONNAME = null;

    /**
     * @var float $NBLOCKREASONNO
     */
    protected $NBLOCKREASONNO = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NSTATUSTICKETNO
     */
    protected $NSTATUSTICKETNO = null;

    /**
     * @var float $NUNICODNO
     */
    protected $NUNICODNO = null;

    /**
     * @var string $SZENDOFBLOCKINGDATE
     */
    protected $SZENDOFBLOCKINGDATE = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZVALIDDATE
     */
    protected $SZVALIDDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getNBLOCKREASONNAME()
    {
      return $this->NBLOCKREASONNAME;
    }

    /**
     * @param string $NBLOCKREASONNAME
     * @return \Axess\Dci4Wtp\D4WTPBLOCKUNBLOCKRESULT
     */
    public function setNBLOCKREASONNAME($NBLOCKREASONNAME)
    {
      $this->NBLOCKREASONNAME = $NBLOCKREASONNAME;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKREASONNO()
    {
      return $this->NBLOCKREASONNO;
    }

    /**
     * @param float $NBLOCKREASONNO
     * @return \Axess\Dci4Wtp\D4WTPBLOCKUNBLOCKRESULT
     */
    public function setNBLOCKREASONNO($NBLOCKREASONNO)
    {
      $this->NBLOCKREASONNO = $NBLOCKREASONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPBLOCKUNBLOCKRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPBLOCKUNBLOCKRESULT
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPBLOCKUNBLOCKRESULT
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPBLOCKUNBLOCKRESULT
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPBLOCKUNBLOCKRESULT
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTATUSTICKETNO()
    {
      return $this->NSTATUSTICKETNO;
    }

    /**
     * @param float $NSTATUSTICKETNO
     * @return \Axess\Dci4Wtp\D4WTPBLOCKUNBLOCKRESULT
     */
    public function setNSTATUSTICKETNO($NSTATUSTICKETNO)
    {
      $this->NSTATUSTICKETNO = $NSTATUSTICKETNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODNO()
    {
      return $this->NUNICODNO;
    }

    /**
     * @param float $NUNICODNO
     * @return \Axess\Dci4Wtp\D4WTPBLOCKUNBLOCKRESULT
     */
    public function setNUNICODNO($NUNICODNO)
    {
      $this->NUNICODNO = $NUNICODNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZENDOFBLOCKINGDATE()
    {
      return $this->SZENDOFBLOCKINGDATE;
    }

    /**
     * @param string $SZENDOFBLOCKINGDATE
     * @return \Axess\Dci4Wtp\D4WTPBLOCKUNBLOCKRESULT
     */
    public function setSZENDOFBLOCKINGDATE($SZENDOFBLOCKINGDATE)
    {
      $this->SZENDOFBLOCKINGDATE = $SZENDOFBLOCKINGDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPBLOCKUNBLOCKRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDDATE()
    {
      return $this->SZVALIDDATE;
    }

    /**
     * @param string $SZVALIDDATE
     * @return \Axess\Dci4Wtp\D4WTPBLOCKUNBLOCKRESULT
     */
    public function setSZVALIDDATE($SZVALIDDATE)
    {
      $this->SZVALIDDATE = $SZVALIDDATE;
      return $this;
    }

}
