<?php

namespace Axess\Dci4Wtp;

class getBookingLimitsResponse
{

    /**
     * @var WTPCheckAvailabilityResult $getBookingLimitsResult
     */
    protected $getBookingLimitsResult = null;

    /**
     * @param WTPCheckAvailabilityResult $getBookingLimitsResult
     */
    public function __construct($getBookingLimitsResult)
    {
      $this->getBookingLimitsResult = $getBookingLimitsResult;
    }

    /**
     * @return WTPCheckAvailabilityResult
     */
    public function getGetBookingLimitsResult()
    {
      return $this->getBookingLimitsResult;
    }

    /**
     * @param WTPCheckAvailabilityResult $getBookingLimitsResult
     * @return \Axess\Dci4Wtp\getBookingLimitsResponse
     */
    public function setGetBookingLimitsResult($getBookingLimitsResult)
    {
      $this->getBookingLimitsResult = $getBookingLimitsResult;
      return $this;
    }

}
