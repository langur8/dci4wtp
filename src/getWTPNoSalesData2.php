<?php

namespace Axess\Dci4Wtp;

class getWTPNoSalesData2
{

    /**
     * @var D4WTPWTPNOSALESDATAREQUEST $i_ctWTPNoSalesDataReq
     */
    protected $i_ctWTPNoSalesDataReq = null;

    /**
     * @param D4WTPWTPNOSALESDATAREQUEST $i_ctWTPNoSalesDataReq
     */
    public function __construct($i_ctWTPNoSalesDataReq)
    {
      $this->i_ctWTPNoSalesDataReq = $i_ctWTPNoSalesDataReq;
    }

    /**
     * @return D4WTPWTPNOSALESDATAREQUEST
     */
    public function getI_ctWTPNoSalesDataReq()
    {
      return $this->i_ctWTPNoSalesDataReq;
    }

    /**
     * @param D4WTPWTPNOSALESDATAREQUEST $i_ctWTPNoSalesDataReq
     * @return \Axess\Dci4Wtp\getWTPNoSalesData2
     */
    public function setI_ctWTPNoSalesDataReq($i_ctWTPNoSalesDataReq)
    {
      $this->i_ctWTPNoSalesDataReq = $i_ctWTPNoSalesDataReq;
      return $this;
    }

}
