<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSUBCONTINGENTRES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSUBCONTINGENTRES[] $D4WTPSUBCONTINGENTRES
     */
    protected $D4WTPSUBCONTINGENTRES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSUBCONTINGENTRES[]
     */
    public function getD4WTPSUBCONTINGENTRES()
    {
      return $this->D4WTPSUBCONTINGENTRES;
    }

    /**
     * @param D4WTPSUBCONTINGENTRES[] $D4WTPSUBCONTINGENTRES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSUBCONTINGENTRES
     */
    public function setD4WTPSUBCONTINGENTRES(array $D4WTPSUBCONTINGENTRES = null)
    {
      $this->D4WTPSUBCONTINGENTRES = $D4WTPSUBCONTINGENTRES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSUBCONTINGENTRES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSUBCONTINGENTRES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSUBCONTINGENTRES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSUBCONTINGENTRES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSUBCONTINGENTRES[] = $value;
      } else {
        $this->D4WTPSUBCONTINGENTRES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSUBCONTINGENTRES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSUBCONTINGENTRES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSUBCONTINGENTRES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSUBCONTINGENTRES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSUBCONTINGENTRES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSUBCONTINGENTRES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSUBCONTINGENTRES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSUBCONTINGENTRES);
    }

}
