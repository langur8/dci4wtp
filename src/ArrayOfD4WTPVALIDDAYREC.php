<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPVALIDDAYREC implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPVALIDDAYREC[] $D4WTPVALIDDAYREC
     */
    protected $D4WTPVALIDDAYREC = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPVALIDDAYREC[]
     */
    public function getD4WTPVALIDDAYREC()
    {
      return $this->D4WTPVALIDDAYREC;
    }

    /**
     * @param D4WTPVALIDDAYREC[] $D4WTPVALIDDAYREC
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPVALIDDAYREC
     */
    public function setD4WTPVALIDDAYREC(array $D4WTPVALIDDAYREC = null)
    {
      $this->D4WTPVALIDDAYREC = $D4WTPVALIDDAYREC;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPVALIDDAYREC[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPVALIDDAYREC
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPVALIDDAYREC[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPVALIDDAYREC $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPVALIDDAYREC[] = $value;
      } else {
        $this->D4WTPVALIDDAYREC[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPVALIDDAYREC[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPVALIDDAYREC Return the current element
     */
    public function current()
    {
      return current($this->D4WTPVALIDDAYREC);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPVALIDDAYREC);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPVALIDDAYREC);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPVALIDDAYREC);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPVALIDDAYREC Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPVALIDDAYREC);
    }

}
