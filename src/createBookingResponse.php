<?php

namespace Axess\Dci4Wtp;

class createBookingResponse
{

    /**
     * @var WTPCreateBookingResult $createBookingResult
     */
    protected $createBookingResult = null;

    /**
     * @param WTPCreateBookingResult $createBookingResult
     */
    public function __construct($createBookingResult)
    {
      $this->createBookingResult = $createBookingResult;
    }

    /**
     * @return WTPCreateBookingResult
     */
    public function getCreateBookingResult()
    {
      return $this->createBookingResult;
    }

    /**
     * @param WTPCreateBookingResult $createBookingResult
     * @return \Axess\Dci4Wtp\createBookingResponse
     */
    public function setCreateBookingResult($createBookingResult)
    {
      $this->createBookingResult = $createBookingResult;
      return $this;
    }

}
