<?php

namespace Axess\Dci4Wtp;

class refundTicket2Response
{

    /**
     * @var D4WTPREFUNDTICKET2RESULT $refundTicket2Result
     */
    protected $refundTicket2Result = null;

    /**
     * @param D4WTPREFUNDTICKET2RESULT $refundTicket2Result
     */
    public function __construct($refundTicket2Result)
    {
      $this->refundTicket2Result = $refundTicket2Result;
    }

    /**
     * @return D4WTPREFUNDTICKET2RESULT
     */
    public function getRefundTicket2Result()
    {
      return $this->refundTicket2Result;
    }

    /**
     * @param D4WTPREFUNDTICKET2RESULT $refundTicket2Result
     * @return \Axess\Dci4Wtp\refundTicket2Response
     */
    public function setRefundTicket2Result($refundTicket2Result)
    {
      $this->refundTicket2Result = $refundTicket2Result;
      return $this;
    }

}
