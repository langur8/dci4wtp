<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLISTRESULT
{

    /**
     * @var ArrayOfD4WTPTARIFFLIST $ACTTARIFFLIST
     */
    protected $ACTTARIFFLIST = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLIST
     */
    public function getACTTARIFFLIST()
    {
      return $this->ACTTARIFFLIST;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLIST $ACTTARIFFLIST
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTRESULT
     */
    public function setACTTARIFFLIST($ACTTARIFFLIST)
    {
      $this->ACTTARIFFLIST = $ACTTARIFFLIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
