<?php

namespace Axess\Dci4Wtp;

class D4WTPEDITORDERPOSRES
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var ArrayOfD4WTPEDITORDERPOSITION $ORDERPOSITIONS
     */
    protected $ORDERPOSITIONS = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERPOSRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPEDITORDERPOSITION
     */
    public function getORDERPOSITIONS()
    {
      return $this->ORDERPOSITIONS;
    }

    /**
     * @param ArrayOfD4WTPEDITORDERPOSITION $ORDERPOSITIONS
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERPOSRES
     */
    public function setORDERPOSITIONS($ORDERPOSITIONS)
    {
      $this->ORDERPOSITIONS = $ORDERPOSITIONS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERPOSRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
