<?php

namespace Axess\Dci4Wtp;

class D4WTPHISTORYREQ
{

    /**
     * @var float $NAXCOUNTKONTONR
     */
    protected $NAXCOUNTKONTONR = null;

    /**
     * @var float $NBANKNR
     */
    protected $NBANKNR = null;

    /**
     * @var float $NBRANCHNR
     */
    protected $NBRANCHNR = null;

    /**
     * @var float $NCORPNR
     */
    protected $NCORPNR = null;

    /**
     * @var float $NDESKNR
     */
    protected $NDESKNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNAXCOUNTKONTONR()
    {
      return $this->NAXCOUNTKONTONR;
    }

    /**
     * @param float $NAXCOUNTKONTONR
     * @return \Axess\Dci4Wtp\D4WTPHISTORYREQ
     */
    public function setNAXCOUNTKONTONR($NAXCOUNTKONTONR)
    {
      $this->NAXCOUNTKONTONR = $NAXCOUNTKONTONR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBANKNR()
    {
      return $this->NBANKNR;
    }

    /**
     * @param float $NBANKNR
     * @return \Axess\Dci4Wtp\D4WTPHISTORYREQ
     */
    public function setNBANKNR($NBANKNR)
    {
      $this->NBANKNR = $NBANKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBRANCHNR()
    {
      return $this->NBRANCHNR;
    }

    /**
     * @param float $NBRANCHNR
     * @return \Axess\Dci4Wtp\D4WTPHISTORYREQ
     */
    public function setNBRANCHNR($NBRANCHNR)
    {
      $this->NBRANCHNR = $NBRANCHNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCORPNR()
    {
      return $this->NCORPNR;
    }

    /**
     * @param float $NCORPNR
     * @return \Axess\Dci4Wtp\D4WTPHISTORYREQ
     */
    public function setNCORPNR($NCORPNR)
    {
      $this->NCORPNR = $NCORPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDESKNR()
    {
      return $this->NDESKNR;
    }

    /**
     * @param float $NDESKNR
     * @return \Axess\Dci4Wtp\D4WTPHISTORYREQ
     */
    public function setNDESKNR($NDESKNR)
    {
      $this->NDESKNR = $NDESKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPHISTORYREQ
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPHISTORYREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPHISTORYREQ
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPHISTORYREQ
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
