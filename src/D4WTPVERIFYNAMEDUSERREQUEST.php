<?php

namespace Axess\Dci4Wtp;

class D4WTPVERIFYNAMEDUSERREQUEST
{

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZPASSWORD
     */
    protected $SZPASSWORD = null;

    /**
     * @var string $SZUSERNAME
     */
    protected $SZUSERNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPVERIFYNAMEDUSERREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPASSWORD()
    {
      return $this->SZPASSWORD;
    }

    /**
     * @param string $SZPASSWORD
     * @return \Axess\Dci4Wtp\D4WTPVERIFYNAMEDUSERREQUEST
     */
    public function setSZPASSWORD($SZPASSWORD)
    {
      $this->SZPASSWORD = $SZPASSWORD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUSERNAME()
    {
      return $this->SZUSERNAME;
    }

    /**
     * @param string $SZUSERNAME
     * @return \Axess\Dci4Wtp\D4WTPVERIFYNAMEDUSERREQUEST
     */
    public function setSZUSERNAME($SZUSERNAME)
    {
      $this->SZUSERNAME = $SZUSERNAME;
      return $this;
    }

}
