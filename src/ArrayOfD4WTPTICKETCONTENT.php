<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTICKETCONTENT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTICKETCONTENT[] $D4WTPTICKETCONTENT
     */
    protected $D4WTPTICKETCONTENT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTICKETCONTENT[]
     */
    public function getD4WTPTICKETCONTENT()
    {
      return $this->D4WTPTICKETCONTENT;
    }

    /**
     * @param D4WTPTICKETCONTENT[] $D4WTPTICKETCONTENT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTICKETCONTENT
     */
    public function setD4WTPTICKETCONTENT(array $D4WTPTICKETCONTENT = null)
    {
      $this->D4WTPTICKETCONTENT = $D4WTPTICKETCONTENT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTICKETCONTENT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTICKETCONTENT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTICKETCONTENT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTICKETCONTENT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTICKETCONTENT[] = $value;
      } else {
        $this->D4WTPTICKETCONTENT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTICKETCONTENT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTICKETCONTENT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTICKETCONTENT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTICKETCONTENT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTICKETCONTENT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTICKETCONTENT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTICKETCONTENT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTICKETCONTENT);
    }

}
