<?php

namespace Axess\Dci4Wtp;

class D4WTPRECEIPTDATARESULT2
{

    /**
     * @var D4WTPRECEIPTDATA2 $CTRECEIPTDATA2
     */
    protected $CTRECEIPTDATA2 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPRECEIPTDATA2
     */
    public function getCTRECEIPTDATA2()
    {
      return $this->CTRECEIPTDATA2;
    }

    /**
     * @param D4WTPRECEIPTDATA2 $CTRECEIPTDATA2
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATARESULT2
     */
    public function setCTRECEIPTDATA2($CTRECEIPTDATA2)
    {
      $this->CTRECEIPTDATA2 = $CTRECEIPTDATA2;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATARESULT2
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATARESULT2
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
