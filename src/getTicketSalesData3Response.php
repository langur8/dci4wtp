<?php

namespace Axess\Dci4Wtp;

class getTicketSalesData3Response
{

    /**
     * @var D4WTPSALESDATARESULT3 $getTicketSalesData3Result
     */
    protected $getTicketSalesData3Result = null;

    /**
     * @param D4WTPSALESDATARESULT3 $getTicketSalesData3Result
     */
    public function __construct($getTicketSalesData3Result)
    {
      $this->getTicketSalesData3Result = $getTicketSalesData3Result;
    }

    /**
     * @return D4WTPSALESDATARESULT3
     */
    public function getGetTicketSalesData3Result()
    {
      return $this->getTicketSalesData3Result;
    }

    /**
     * @param D4WTPSALESDATARESULT3 $getTicketSalesData3Result
     * @return \Axess\Dci4Wtp\getTicketSalesData3Response
     */
    public function setGetTicketSalesData3Result($getTicketSalesData3Result)
    {
      $this->getTicketSalesData3Result = $getTicketSalesData3Result;
      return $this;
    }

}
