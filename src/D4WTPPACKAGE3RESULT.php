<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGE3RESULT
{

    /**
     * @var ArrayOfD4WTPPACKAGE3 $ACTPACKAGE3
     */
    protected $ACTPACKAGE3 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPACKAGE3
     */
    public function getACTPACKAGE3()
    {
      return $this->ACTPACKAGE3;
    }

    /**
     * @param ArrayOfD4WTPPACKAGE3 $ACTPACKAGE3
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE3RESULT
     */
    public function setACTPACKAGE3($ACTPACKAGE3)
    {
      $this->ACTPACKAGE3 = $ACTPACKAGE3;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE3RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE3RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
