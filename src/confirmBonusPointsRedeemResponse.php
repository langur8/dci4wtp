<?php

namespace Axess\Dci4Wtp;

class confirmBonusPointsRedeemResponse
{

    /**
     * @var D4WTPRESULT $confirmBonusPointsRedeemResult
     */
    protected $confirmBonusPointsRedeemResult = null;

    /**
     * @param D4WTPRESULT $confirmBonusPointsRedeemResult
     */
    public function __construct($confirmBonusPointsRedeemResult)
    {
      $this->confirmBonusPointsRedeemResult = $confirmBonusPointsRedeemResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getConfirmBonusPointsRedeemResult()
    {
      return $this->confirmBonusPointsRedeemResult;
    }

    /**
     * @param D4WTPRESULT $confirmBonusPointsRedeemResult
     * @return \Axess\Dci4Wtp\confirmBonusPointsRedeemResponse
     */
    public function setConfirmBonusPointsRedeemResult($confirmBonusPointsRedeemResult)
    {
      $this->confirmBonusPointsRedeemResult = $confirmBonusPointsRedeemResult;
      return $this;
    }

}
