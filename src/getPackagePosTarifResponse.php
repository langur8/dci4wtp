<?php

namespace Axess\Dci4Wtp;

class getPackagePosTarifResponse
{

    /**
     * @var D4WTPPACKAGEPOSTARIFRESULT $getPackagePosTarifResult
     */
    protected $getPackagePosTarifResult = null;

    /**
     * @param D4WTPPACKAGEPOSTARIFRESULT $getPackagePosTarifResult
     */
    public function __construct($getPackagePosTarifResult)
    {
      $this->getPackagePosTarifResult = $getPackagePosTarifResult;
    }

    /**
     * @return D4WTPPACKAGEPOSTARIFRESULT
     */
    public function getGetPackagePosTarifResult()
    {
      return $this->getPackagePosTarifResult;
    }

    /**
     * @param D4WTPPACKAGEPOSTARIFRESULT $getPackagePosTarifResult
     * @return \Axess\Dci4Wtp\getPackagePosTarifResponse
     */
    public function setGetPackagePosTarifResult($getPackagePosTarifResult)
    {
      $this->getPackagePosTarifResult = $getPackagePosTarifResult;
      return $this;
    }

}
