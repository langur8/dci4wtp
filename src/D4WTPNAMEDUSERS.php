<?php

namespace Axess\Dci4Wtp;

class D4WTPNAMEDUSERS
{

    /**
     * @var float $NNAMEDUSERID
     */
    protected $NNAMEDUSERID = null;

    /**
     * @var ArrayOfD4WTPUSERROLES $RMUSERROLES
     */
    protected $RMUSERROLES = null;

    /**
     * @var string $SZUSERNAME
     */
    protected $SZUSERNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNNAMEDUSERID()
    {
      return $this->NNAMEDUSERID;
    }

    /**
     * @param float $NNAMEDUSERID
     * @return \Axess\Dci4Wtp\D4WTPNAMEDUSERS
     */
    public function setNNAMEDUSERID($NNAMEDUSERID)
    {
      $this->NNAMEDUSERID = $NNAMEDUSERID;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPUSERROLES
     */
    public function getRMUSERROLES()
    {
      return $this->RMUSERROLES;
    }

    /**
     * @param ArrayOfD4WTPUSERROLES $RMUSERROLES
     * @return \Axess\Dci4Wtp\D4WTPNAMEDUSERS
     */
    public function setRMUSERROLES($RMUSERROLES)
    {
      $this->RMUSERROLES = $RMUSERROLES;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUSERNAME()
    {
      return $this->SZUSERNAME;
    }

    /**
     * @param string $SZUSERNAME
     * @return \Axess\Dci4Wtp\D4WTPNAMEDUSERS
     */
    public function setSZUSERNAME($SZUSERNAME)
    {
      $this->SZUSERNAME = $SZUSERNAME;
      return $this;
    }

}
