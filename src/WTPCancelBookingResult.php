<?php

namespace Axess\Dci4Wtp;

class WTPCancelBookingResult
{

    /**
     * @var int $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var int $bOpenWeb
     */
    protected $bOpenWeb = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param int $NERRORNO
     * @return \Axess\Dci4Wtp\WTPCancelBookingResult
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\WTPCancelBookingResult
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return int
     */
    public function getBOpenWeb()
    {
      return $this->bOpenWeb;
    }

    /**
     * @param int $bOpenWeb
     * @return \Axess\Dci4Wtp\WTPCancelBookingResult
     */
    public function setBOpenWeb($bOpenWeb)
    {
      $this->bOpenWeb = $bOpenWeb;
      return $this;
    }

}
