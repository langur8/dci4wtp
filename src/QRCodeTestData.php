<?php

namespace Axess\Dci4Wtp;

class QRCodeTestData
{

    /**
     * @var string $CertId
     */
    protected $CertId = null;

    /**
     * @var string $CertIssuer
     */
    protected $CertIssuer = null;

    /**
     * @var string $CountryOfTest
     */
    protected $CountryOfTest = null;

    /**
     * @var string $DateOfTest
     */
    protected $DateOfTest = null;

    /**
     * @var string $TestCenter
     */
    protected $TestCenter = null;

    /**
     * @var string $TestResult
     */
    protected $TestResult = null;

    /**
     * @var string $TestType
     */
    protected $TestType = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCertId()
    {
      return $this->CertId;
    }

    /**
     * @param string $CertId
     * @return \Axess\Dci4Wtp\QRCodeTestData
     */
    public function setCertId($CertId)
    {
      $this->CertId = $CertId;
      return $this;
    }

    /**
     * @return string
     */
    public function getCertIssuer()
    {
      return $this->CertIssuer;
    }

    /**
     * @param string $CertIssuer
     * @return \Axess\Dci4Wtp\QRCodeTestData
     */
    public function setCertIssuer($CertIssuer)
    {
      $this->CertIssuer = $CertIssuer;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryOfTest()
    {
      return $this->CountryOfTest;
    }

    /**
     * @param string $CountryOfTest
     * @return \Axess\Dci4Wtp\QRCodeTestData
     */
    public function setCountryOfTest($CountryOfTest)
    {
      $this->CountryOfTest = $CountryOfTest;
      return $this;
    }

    /**
     * @return string
     */
    public function getDateOfTest()
    {
      return $this->DateOfTest;
    }

    /**
     * @param string $DateOfTest
     * @return \Axess\Dci4Wtp\QRCodeTestData
     */
    public function setDateOfTest($DateOfTest)
    {
      $this->DateOfTest = $DateOfTest;
      return $this;
    }

    /**
     * @return string
     */
    public function getTestCenter()
    {
      return $this->TestCenter;
    }

    /**
     * @param string $TestCenter
     * @return \Axess\Dci4Wtp\QRCodeTestData
     */
    public function setTestCenter($TestCenter)
    {
      $this->TestCenter = $TestCenter;
      return $this;
    }

    /**
     * @return string
     */
    public function getTestResult()
    {
      return $this->TestResult;
    }

    /**
     * @param string $TestResult
     * @return \Axess\Dci4Wtp\QRCodeTestData
     */
    public function setTestResult($TestResult)
    {
      $this->TestResult = $TestResult;
      return $this;
    }

    /**
     * @return string
     */
    public function getTestType()
    {
      return $this->TestType;
    }

    /**
     * @param string $TestType
     * @return \Axess\Dci4Wtp\QRCodeTestData
     */
    public function setTestType($TestType)
    {
      $this->TestType = $TestType;
      return $this;
    }

}
