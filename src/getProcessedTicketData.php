<?php

namespace Axess\Dci4Wtp;

class getProcessedTicketData
{

    /**
     * @var D4WTPGETPROCESSEDTKTDATAREQ $i_ctProcessedTicketDataReq
     */
    protected $i_ctProcessedTicketDataReq = null;

    /**
     * @param D4WTPGETPROCESSEDTKTDATAREQ $i_ctProcessedTicketDataReq
     */
    public function __construct($i_ctProcessedTicketDataReq)
    {
      $this->i_ctProcessedTicketDataReq = $i_ctProcessedTicketDataReq;
    }

    /**
     * @return D4WTPGETPROCESSEDTKTDATAREQ
     */
    public function getI_ctProcessedTicketDataReq()
    {
      return $this->i_ctProcessedTicketDataReq;
    }

    /**
     * @param D4WTPGETPROCESSEDTKTDATAREQ $i_ctProcessedTicketDataReq
     * @return \Axess\Dci4Wtp\getProcessedTicketData
     */
    public function setI_ctProcessedTicketDataReq($i_ctProcessedTicketDataReq)
    {
      $this->i_ctProcessedTicketDataReq = $i_ctProcessedTicketDataReq;
      return $this;
    }

}
