<?php

namespace Axess\Dci4Wtp;

class getARCubes
{

    /**
     * @var D4WTPGETARCUBESREQUEST $i_ctGetARCubesReq
     */
    protected $i_ctGetARCubesReq = null;

    /**
     * @param D4WTPGETARCUBESREQUEST $i_ctGetARCubesReq
     */
    public function __construct($i_ctGetARCubesReq)
    {
      $this->i_ctGetARCubesReq = $i_ctGetARCubesReq;
    }

    /**
     * @return D4WTPGETARCUBESREQUEST
     */
    public function getI_ctGetARCubesReq()
    {
      return $this->i_ctGetARCubesReq;
    }

    /**
     * @param D4WTPGETARCUBESREQUEST $i_ctGetARCubesReq
     * @return \Axess\Dci4Wtp\getARCubes
     */
    public function setI_ctGetARCubesReq($i_ctGetARCubesReq)
    {
      $this->i_ctGetARCubesReq = $i_ctGetARCubesReq;
      return $this;
    }

}
