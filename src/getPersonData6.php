<?php

namespace Axess\Dci4Wtp;

class getPersonData6
{

    /**
     * @var D4WTPGETPERSON5REQUEST $i_ctPersDataReq
     */
    protected $i_ctPersDataReq = null;

    /**
     * @param D4WTPGETPERSON5REQUEST $i_ctPersDataReq
     */
    public function __construct($i_ctPersDataReq)
    {
      $this->i_ctPersDataReq = $i_ctPersDataReq;
    }

    /**
     * @return D4WTPGETPERSON5REQUEST
     */
    public function getI_ctPersDataReq()
    {
      return $this->i_ctPersDataReq;
    }

    /**
     * @param D4WTPGETPERSON5REQUEST $i_ctPersDataReq
     * @return \Axess\Dci4Wtp\getPersonData6
     */
    public function setI_ctPersDataReq($i_ctPersDataReq)
    {
      $this->i_ctPersDataReq = $i_ctPersDataReq;
      return $this;
    }

}
