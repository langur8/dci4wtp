<?php

namespace Axess\Dci4Wtp;

class getTariff
{

    /**
     * @var D4WTPTARIFFREQUEST $i_ctTariffReq
     */
    protected $i_ctTariffReq = null;

    /**
     * @param D4WTPTARIFFREQUEST $i_ctTariffReq
     */
    public function __construct($i_ctTariffReq)
    {
      $this->i_ctTariffReq = $i_ctTariffReq;
    }

    /**
     * @return D4WTPTARIFFREQUEST
     */
    public function getI_ctTariffReq()
    {
      return $this->i_ctTariffReq;
    }

    /**
     * @param D4WTPTARIFFREQUEST $i_ctTariffReq
     * @return \Axess\Dci4Wtp\getTariff
     */
    public function setI_ctTariffReq($i_ctTariffReq)
    {
      $this->i_ctTariffReq = $i_ctTariffReq;
      return $this;
    }

}
