<?php

namespace Axess\Dci4Wtp;

class getContingent
{

    /**
     * @var D4WTPCONTINGENTREQUEST $i_ctContingentReq
     */
    protected $i_ctContingentReq = null;

    /**
     * @param D4WTPCONTINGENTREQUEST $i_ctContingentReq
     */
    public function __construct($i_ctContingentReq)
    {
      $this->i_ctContingentReq = $i_ctContingentReq;
    }

    /**
     * @return D4WTPCONTINGENTREQUEST
     */
    public function getI_ctContingentReq()
    {
      return $this->i_ctContingentReq;
    }

    /**
     * @param D4WTPCONTINGENTREQUEST $i_ctContingentReq
     * @return \Axess\Dci4Wtp\getContingent
     */
    public function setI_ctContingentReq($i_ctContingentReq)
    {
      $this->i_ctContingentReq = $i_ctContingentReq;
      return $this;
    }

}
