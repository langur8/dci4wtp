<?php

namespace Axess\Dci4Wtp;

class changeEmoneyAccountResponse
{

    /**
     * @var D4WTPEMONEYACCOUNTRES $changeEmoneyAccountResult
     */
    protected $changeEmoneyAccountResult = null;

    /**
     * @param D4WTPEMONEYACCOUNTRES $changeEmoneyAccountResult
     */
    public function __construct($changeEmoneyAccountResult)
    {
      $this->changeEmoneyAccountResult = $changeEmoneyAccountResult;
    }

    /**
     * @return D4WTPEMONEYACCOUNTRES
     */
    public function getChangeEmoneyAccountResult()
    {
      return $this->changeEmoneyAccountResult;
    }

    /**
     * @param D4WTPEMONEYACCOUNTRES $changeEmoneyAccountResult
     * @return \Axess\Dci4Wtp\changeEmoneyAccountResponse
     */
    public function setChangeEmoneyAccountResult($changeEmoneyAccountResult)
    {
      $this->changeEmoneyAccountResult = $changeEmoneyAccountResult;
      return $this;
    }

}
