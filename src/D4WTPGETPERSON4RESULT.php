<?php

namespace Axess\Dci4Wtp;

class D4WTPGETPERSON4RESULT
{

    /**
     * @var ArrayOfD4WTPERSONDATA5 $ACTPERSONDATA5
     */
    protected $ACTPERSONDATA5 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NROWCOUNT
     */
    protected $NROWCOUNT = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPERSONDATA5
     */
    public function getACTPERSONDATA5()
    {
      return $this->ACTPERSONDATA5;
    }

    /**
     * @param ArrayOfD4WTPERSONDATA5 $ACTPERSONDATA5
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON4RESULT
     */
    public function setACTPERSONDATA5($ACTPERSONDATA5)
    {
      $this->ACTPERSONDATA5 = $ACTPERSONDATA5;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON4RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNROWCOUNT()
    {
      return $this->NROWCOUNT;
    }

    /**
     * @param float $NROWCOUNT
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON4RESULT
     */
    public function setNROWCOUNT($NROWCOUNT)
    {
      $this->NROWCOUNT = $NROWCOUNT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON4RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
