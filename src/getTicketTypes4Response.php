<?php

namespace Axess\Dci4Wtp;

class getTicketTypes4Response
{

    /**
     * @var D4WTPTICKETTYPE4RESULT $getTicketTypes4Result
     */
    protected $getTicketTypes4Result = null;

    /**
     * @param D4WTPTICKETTYPE4RESULT $getTicketTypes4Result
     */
    public function __construct($getTicketTypes4Result)
    {
      $this->getTicketTypes4Result = $getTicketTypes4Result;
    }

    /**
     * @return D4WTPTICKETTYPE4RESULT
     */
    public function getGetTicketTypes4Result()
    {
      return $this->getTicketTypes4Result;
    }

    /**
     * @param D4WTPTICKETTYPE4RESULT $getTicketTypes4Result
     * @return \Axess\Dci4Wtp\getTicketTypes4Response
     */
    public function setGetTicketTypes4Result($getTicketTypes4Result)
    {
      $this->getTicketTypes4Result = $getTicketTypes4Result;
      return $this;
    }

}
