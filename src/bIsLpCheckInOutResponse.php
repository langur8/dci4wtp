<?php

namespace Axess\Dci4Wtp;

class bIsLpCheckInOutResponse
{

    /**
     * @var D4WTPISLPCHECKINOUTRESULT $bIsLpCheckInOutResult
     */
    protected $bIsLpCheckInOutResult = null;

    /**
     * @param D4WTPISLPCHECKINOUTRESULT $bIsLpCheckInOutResult
     */
    public function __construct($bIsLpCheckInOutResult)
    {
      $this->bIsLpCheckInOutResult = $bIsLpCheckInOutResult;
    }

    /**
     * @return D4WTPISLPCHECKINOUTRESULT
     */
    public function getBIsLpCheckInOutResult()
    {
      return $this->bIsLpCheckInOutResult;
    }

    /**
     * @param D4WTPISLPCHECKINOUTRESULT $bIsLpCheckInOutResult
     * @return \Axess\Dci4Wtp\bIsLpCheckInOutResponse
     */
    public function setBIsLpCheckInOutResult($bIsLpCheckInOutResult)
    {
      $this->bIsLpCheckInOutResult = $bIsLpCheckInOutResult;
      return $this;
    }

}
