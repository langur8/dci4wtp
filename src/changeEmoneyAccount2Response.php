<?php

namespace Axess\Dci4Wtp;

class changeEmoneyAccount2Response
{

    /**
     * @var D4WTPEMONEYACCOUNTRES $changeEmoneyAccount2Result
     */
    protected $changeEmoneyAccount2Result = null;

    /**
     * @param D4WTPEMONEYACCOUNTRES $changeEmoneyAccount2Result
     */
    public function __construct($changeEmoneyAccount2Result)
    {
      $this->changeEmoneyAccount2Result = $changeEmoneyAccount2Result;
    }

    /**
     * @return D4WTPEMONEYACCOUNTRES
     */
    public function getChangeEmoneyAccount2Result()
    {
      return $this->changeEmoneyAccount2Result;
    }

    /**
     * @param D4WTPEMONEYACCOUNTRES $changeEmoneyAccount2Result
     * @return \Axess\Dci4Wtp\changeEmoneyAccount2Response
     */
    public function setChangeEmoneyAccount2Result($changeEmoneyAccount2Result)
    {
      $this->changeEmoneyAccount2Result = $changeEmoneyAccount2Result;
      return $this;
    }

}
