<?php

namespace Axess\Dci4Wtp;

class D4WTPMEMBERDETAILS
{

    /**
     * @var float $NCUSTOMERLANGUAGEID
     */
    protected $NCUSTOMERLANGUAGEID = null;

    /**
     * @var string $SZACTIVATIONDATE
     */
    protected $SZACTIVATIONDATE = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZKEY
     */
    protected $SZKEY = null;

    /**
     * @var string $SZKEYVALIDITYDATE
     */
    protected $SZKEYVALIDITYDATE = null;

    /**
     * @var string $SZLASTCHANGEDATE
     */
    protected $SZLASTCHANGEDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCUSTOMERLANGUAGEID()
    {
      return $this->NCUSTOMERLANGUAGEID;
    }

    /**
     * @param float $NCUSTOMERLANGUAGEID
     * @return \Axess\Dci4Wtp\D4WTPMEMBERDETAILS
     */
    public function setNCUSTOMERLANGUAGEID($NCUSTOMERLANGUAGEID)
    {
      $this->NCUSTOMERLANGUAGEID = $NCUSTOMERLANGUAGEID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZACTIVATIONDATE()
    {
      return $this->SZACTIVATIONDATE;
    }

    /**
     * @param string $SZACTIVATIONDATE
     * @return \Axess\Dci4Wtp\D4WTPMEMBERDETAILS
     */
    public function setSZACTIVATIONDATE($SZACTIVATIONDATE)
    {
      $this->SZACTIVATIONDATE = $SZACTIVATIONDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPMEMBERDETAILS
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZKEY()
    {
      return $this->SZKEY;
    }

    /**
     * @param string $SZKEY
     * @return \Axess\Dci4Wtp\D4WTPMEMBERDETAILS
     */
    public function setSZKEY($SZKEY)
    {
      $this->SZKEY = $SZKEY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZKEYVALIDITYDATE()
    {
      return $this->SZKEYVALIDITYDATE;
    }

    /**
     * @param string $SZKEYVALIDITYDATE
     * @return \Axess\Dci4Wtp\D4WTPMEMBERDETAILS
     */
    public function setSZKEYVALIDITYDATE($SZKEYVALIDITYDATE)
    {
      $this->SZKEYVALIDITYDATE = $SZKEYVALIDITYDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTCHANGEDATE()
    {
      return $this->SZLASTCHANGEDATE;
    }

    /**
     * @param string $SZLASTCHANGEDATE
     * @return \Axess\Dci4Wtp\D4WTPMEMBERDETAILS
     */
    public function setSZLASTCHANGEDATE($SZLASTCHANGEDATE)
    {
      $this->SZLASTCHANGEDATE = $SZLASTCHANGEDATE;
      return $this;
    }

}
