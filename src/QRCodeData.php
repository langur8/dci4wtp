<?php

namespace Axess\Dci4Wtp;

class QRCodeData
{

    /**
     * @var string $DateOfBirth
     */
    protected $DateOfBirth = null;

    /**
     * @var ArrayOfEntryStatePerDay $EntryStateList
     */
    protected $EntryStateList = null;

    /**
     * @var string $ExpirationDate
     */
    protected $ExpirationDate = null;

    /**
     * @var string $FirstName
     */
    protected $FirstName = null;

    /**
     * @var string $IssuedDate
     */
    protected $IssuedDate = null;

    /**
     * @var string $IssuingCountry
     */
    protected $IssuingCountry = null;

    /**
     * @var string $LastName
     */
    protected $LastName = null;

    /**
     * @var ArrayOfQRCodeManuallyAddedData $ManuallyAdded
     */
    protected $ManuallyAdded = null;

    /**
     * @var ArrayOfQRCodeRecoveryData $Recoveries
     */
    protected $Recoveries = null;

    /**
     * @var ArrayOfQRCodeTestData $Tests
     */
    protected $Tests = null;

    /**
     * @var ArrayOfQRCodeVaccineData $Vaccines
     */
    protected $Vaccines = null;

    /**
     * @var string $Validated
     */
    protected $Validated = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDateOfBirth()
    {
      return $this->DateOfBirth;
    }

    /**
     * @param string $DateOfBirth
     * @return \Axess\Dci4Wtp\QRCodeData
     */
    public function setDateOfBirth($DateOfBirth)
    {
      $this->DateOfBirth = $DateOfBirth;
      return $this;
    }

    /**
     * @return ArrayOfEntryStatePerDay
     */
    public function getEntryStateList()
    {
      return $this->EntryStateList;
    }

    /**
     * @param ArrayOfEntryStatePerDay $EntryStateList
     * @return \Axess\Dci4Wtp\QRCodeData
     */
    public function setEntryStateList($EntryStateList)
    {
      $this->EntryStateList = $EntryStateList;
      return $this;
    }

    /**
     * @return string
     */
    public function getExpirationDate()
    {
      return $this->ExpirationDate;
    }

    /**
     * @param string $ExpirationDate
     * @return \Axess\Dci4Wtp\QRCodeData
     */
    public function setExpirationDate($ExpirationDate)
    {
      $this->ExpirationDate = $ExpirationDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
      return $this->FirstName;
    }

    /**
     * @param string $FirstName
     * @return \Axess\Dci4Wtp\QRCodeData
     */
    public function setFirstName($FirstName)
    {
      $this->FirstName = $FirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getIssuedDate()
    {
      return $this->IssuedDate;
    }

    /**
     * @param string $IssuedDate
     * @return \Axess\Dci4Wtp\QRCodeData
     */
    public function setIssuedDate($IssuedDate)
    {
      $this->IssuedDate = $IssuedDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getIssuingCountry()
    {
      return $this->IssuingCountry;
    }

    /**
     * @param string $IssuingCountry
     * @return \Axess\Dci4Wtp\QRCodeData
     */
    public function setIssuingCountry($IssuingCountry)
    {
      $this->IssuingCountry = $IssuingCountry;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
      return $this->LastName;
    }

    /**
     * @param string $LastName
     * @return \Axess\Dci4Wtp\QRCodeData
     */
    public function setLastName($LastName)
    {
      $this->LastName = $LastName;
      return $this;
    }

    /**
     * @return ArrayOfQRCodeManuallyAddedData
     */
    public function getManuallyAdded()
    {
      return $this->ManuallyAdded;
    }

    /**
     * @param ArrayOfQRCodeManuallyAddedData $ManuallyAdded
     * @return \Axess\Dci4Wtp\QRCodeData
     */
    public function setManuallyAdded($ManuallyAdded)
    {
      $this->ManuallyAdded = $ManuallyAdded;
      return $this;
    }

    /**
     * @return ArrayOfQRCodeRecoveryData
     */
    public function getRecoveries()
    {
      return $this->Recoveries;
    }

    /**
     * @param ArrayOfQRCodeRecoveryData $Recoveries
     * @return \Axess\Dci4Wtp\QRCodeData
     */
    public function setRecoveries($Recoveries)
    {
      $this->Recoveries = $Recoveries;
      return $this;
    }

    /**
     * @return ArrayOfQRCodeTestData
     */
    public function getTests()
    {
      return $this->Tests;
    }

    /**
     * @param ArrayOfQRCodeTestData $Tests
     * @return \Axess\Dci4Wtp\QRCodeData
     */
    public function setTests($Tests)
    {
      $this->Tests = $Tests;
      return $this;
    }

    /**
     * @return ArrayOfQRCodeVaccineData
     */
    public function getVaccines()
    {
      return $this->Vaccines;
    }

    /**
     * @param ArrayOfQRCodeVaccineData $Vaccines
     * @return \Axess\Dci4Wtp\QRCodeData
     */
    public function setVaccines($Vaccines)
    {
      $this->Vaccines = $Vaccines;
      return $this;
    }

    /**
     * @return string
     */
    public function getValidated()
    {
      return $this->Validated;
    }

    /**
     * @param string $Validated
     * @return \Axess\Dci4Wtp\QRCodeData
     */
    public function setValidated($Validated)
    {
      $this->Validated = $Validated;
      return $this;
    }

}
