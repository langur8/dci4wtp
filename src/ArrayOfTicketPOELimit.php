<?php

namespace Axess\Dci4Wtp;

class ArrayOfTicketPOELimit implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var TicketPOELimit[] $TicketPOELimit
     */
    protected $TicketPOELimit = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return TicketPOELimit[]
     */
    public function getTicketPOELimit()
    {
      return $this->TicketPOELimit;
    }

    /**
     * @param TicketPOELimit[] $TicketPOELimit
     * @return \Axess\Dci4Wtp\ArrayOfTicketPOELimit
     */
    public function setTicketPOELimit(array $TicketPOELimit = null)
    {
      $this->TicketPOELimit = $TicketPOELimit;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->TicketPOELimit[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return TicketPOELimit
     */
    public function offsetGet($offset)
    {
      return $this->TicketPOELimit[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param TicketPOELimit $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->TicketPOELimit[] = $value;
      } else {
        $this->TicketPOELimit[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->TicketPOELimit[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return TicketPOELimit Return the current element
     */
    public function current()
    {
      return current($this->TicketPOELimit);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->TicketPOELimit);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->TicketPOELimit);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->TicketPOELimit);
    }

    /**
     * Countable implementation
     *
     * @return TicketPOELimit Return count of elements
     */
    public function count()
    {
      return count($this->TicketPOELimit);
    }

}
