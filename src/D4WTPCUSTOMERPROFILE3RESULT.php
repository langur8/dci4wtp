<?php

namespace Axess\Dci4Wtp;

class D4WTPCUSTOMERPROFILE3RESULT
{

    /**
     * @var ArrayOfD4WTPCUSTOMERPROFSETUPITEM $ACTSETUPITEM
     */
    protected $ACTSETUPITEM = null;

    /**
     * @var float $BCREATEPHOTO
     */
    protected $BCREATEPHOTO = null;

    /**
     * @var float $BCREDITCARDALLOWED
     */
    protected $BCREDITCARDALLOWED = null;

    /**
     * @var float $BPERSONDATAENABLED
     */
    protected $BPERSONDATAENABLED = null;

    /**
     * @var float $BPERSONDATAREQUIRED
     */
    protected $BPERSONDATAREQUIRED = null;

    /**
     * @var float $BPOOLSELECTION
     */
    protected $BPOOLSELECTION = null;

    /**
     * @var float $BREBOOKALLOWED
     */
    protected $BREBOOKALLOWED = null;

    /**
     * @var float $BUSEPRINTERMODE
     */
    protected $BUSEPRINTERMODE = null;

    /**
     * @var float $BUSEWTPMODE
     */
    protected $BUSEWTPMODE = null;

    /**
     * @var float $NDEFAULTREPORTID
     */
    protected $NDEFAULTREPORTID = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZCLEARING
     */
    protected $SZCLEARING = null;

    /**
     * @var string $SZD1TIME
     */
    protected $SZD1TIME = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCUSTOMERPROFSETUPITEM
     */
    public function getACTSETUPITEM()
    {
      return $this->ACTSETUPITEM;
    }

    /**
     * @param ArrayOfD4WTPCUSTOMERPROFSETUPITEM $ACTSETUPITEM
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setACTSETUPITEM($ACTSETUPITEM)
    {
      $this->ACTSETUPITEM = $ACTSETUPITEM;
      return $this;
    }

    /**
     * @return float
     */
    public function getBCREATEPHOTO()
    {
      return $this->BCREATEPHOTO;
    }

    /**
     * @param float $BCREATEPHOTO
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setBCREATEPHOTO($BCREATEPHOTO)
    {
      $this->BCREATEPHOTO = $BCREATEPHOTO;
      return $this;
    }

    /**
     * @return float
     */
    public function getBCREDITCARDALLOWED()
    {
      return $this->BCREDITCARDALLOWED;
    }

    /**
     * @param float $BCREDITCARDALLOWED
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setBCREDITCARDALLOWED($BCREDITCARDALLOWED)
    {
      $this->BCREDITCARDALLOWED = $BCREDITCARDALLOWED;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPERSONDATAENABLED()
    {
      return $this->BPERSONDATAENABLED;
    }

    /**
     * @param float $BPERSONDATAENABLED
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setBPERSONDATAENABLED($BPERSONDATAENABLED)
    {
      $this->BPERSONDATAENABLED = $BPERSONDATAENABLED;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPERSONDATAREQUIRED()
    {
      return $this->BPERSONDATAREQUIRED;
    }

    /**
     * @param float $BPERSONDATAREQUIRED
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setBPERSONDATAREQUIRED($BPERSONDATAREQUIRED)
    {
      $this->BPERSONDATAREQUIRED = $BPERSONDATAREQUIRED;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPOOLSELECTION()
    {
      return $this->BPOOLSELECTION;
    }

    /**
     * @param float $BPOOLSELECTION
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setBPOOLSELECTION($BPOOLSELECTION)
    {
      $this->BPOOLSELECTION = $BPOOLSELECTION;
      return $this;
    }

    /**
     * @return float
     */
    public function getBREBOOKALLOWED()
    {
      return $this->BREBOOKALLOWED;
    }

    /**
     * @param float $BREBOOKALLOWED
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setBREBOOKALLOWED($BREBOOKALLOWED)
    {
      $this->BREBOOKALLOWED = $BREBOOKALLOWED;
      return $this;
    }

    /**
     * @return float
     */
    public function getBUSEPRINTERMODE()
    {
      return $this->BUSEPRINTERMODE;
    }

    /**
     * @param float $BUSEPRINTERMODE
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setBUSEPRINTERMODE($BUSEPRINTERMODE)
    {
      $this->BUSEPRINTERMODE = $BUSEPRINTERMODE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBUSEWTPMODE()
    {
      return $this->BUSEWTPMODE;
    }

    /**
     * @param float $BUSEWTPMODE
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setBUSEWTPMODE($BUSEWTPMODE)
    {
      $this->BUSEWTPMODE = $BUSEWTPMODE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDEFAULTREPORTID()
    {
      return $this->NDEFAULTREPORTID;
    }

    /**
     * @param float $NDEFAULTREPORTID
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setNDEFAULTREPORTID($NDEFAULTREPORTID)
    {
      $this->NDEFAULTREPORTID = $NDEFAULTREPORTID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCLEARING()
    {
      return $this->SZCLEARING;
    }

    /**
     * @param string $SZCLEARING
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setSZCLEARING($SZCLEARING)
    {
      $this->SZCLEARING = $SZCLEARING;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZD1TIME()
    {
      return $this->SZD1TIME;
    }

    /**
     * @param string $SZD1TIME
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setSZD1TIME($SZD1TIME)
    {
      $this->SZD1TIME = $SZD1TIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERPROFILE3RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
