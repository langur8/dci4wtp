<?php

namespace Axess\Dci4Wtp;

class D4WTPHISTORYRES
{

    /**
     * @var ArrayOfD4WTPROW $AXCOUNTROWS
     */
    protected $AXCOUNTROWS = null;

    /**
     * @var float $NAXCOUNTKONTONR
     */
    protected $NAXCOUNTKONTONR = null;

    /**
     * @var float $NBANKNR
     */
    protected $NBANKNR = null;

    /**
     * @var float $NBRANCHNR
     */
    protected $NBRANCHNR = null;

    /**
     * @var float $NCORPNR
     */
    protected $NCORPNR = null;

    /**
     * @var float $NDESKNR
     */
    protected $NDESKNR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPROW
     */
    public function getAXCOUNTROWS()
    {
      return $this->AXCOUNTROWS;
    }

    /**
     * @param ArrayOfD4WTPROW $AXCOUNTROWS
     * @return \Axess\Dci4Wtp\D4WTPHISTORYRES
     */
    public function setAXCOUNTROWS($AXCOUNTROWS)
    {
      $this->AXCOUNTROWS = $AXCOUNTROWS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAXCOUNTKONTONR()
    {
      return $this->NAXCOUNTKONTONR;
    }

    /**
     * @param float $NAXCOUNTKONTONR
     * @return \Axess\Dci4Wtp\D4WTPHISTORYRES
     */
    public function setNAXCOUNTKONTONR($NAXCOUNTKONTONR)
    {
      $this->NAXCOUNTKONTONR = $NAXCOUNTKONTONR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBANKNR()
    {
      return $this->NBANKNR;
    }

    /**
     * @param float $NBANKNR
     * @return \Axess\Dci4Wtp\D4WTPHISTORYRES
     */
    public function setNBANKNR($NBANKNR)
    {
      $this->NBANKNR = $NBANKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBRANCHNR()
    {
      return $this->NBRANCHNR;
    }

    /**
     * @param float $NBRANCHNR
     * @return \Axess\Dci4Wtp\D4WTPHISTORYRES
     */
    public function setNBRANCHNR($NBRANCHNR)
    {
      $this->NBRANCHNR = $NBRANCHNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCORPNR()
    {
      return $this->NCORPNR;
    }

    /**
     * @param float $NCORPNR
     * @return \Axess\Dci4Wtp\D4WTPHISTORYRES
     */
    public function setNCORPNR($NCORPNR)
    {
      $this->NCORPNR = $NCORPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDESKNR()
    {
      return $this->NDESKNR;
    }

    /**
     * @param float $NDESKNR
     * @return \Axess\Dci4Wtp\D4WTPHISTORYRES
     */
    public function setNDESKNR($NDESKNR)
    {
      $this->NDESKNR = $NDESKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPHISTORYRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPHISTORYRES
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPHISTORYRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
