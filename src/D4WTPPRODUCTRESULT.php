<?php

namespace Axess\Dci4Wtp;

class D4WTPPRODUCTRESULT
{

    /**
     * @var ArrayOfD4WTPPRODUCTTYPES $ACTPRODUCTTYPES
     */
    protected $ACTPRODUCTTYPES = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPRODUCTTYPES
     */
    public function getACTPRODUCTTYPES()
    {
      return $this->ACTPRODUCTTYPES;
    }

    /**
     * @param ArrayOfD4WTPPRODUCTTYPES $ACTPRODUCTTYPES
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTRESULT
     */
    public function setACTPRODUCTTYPES($ACTPRODUCTTYPES)
    {
      $this->ACTPRODUCTTYPES = $ACTPRODUCTTYPES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
