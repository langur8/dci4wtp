<?php

namespace Axess\Dci4Wtp;

class changePersData4Response
{

    /**
     * @var D4WTPCHANGEPERSDATA4RESULT $changePersData4Result
     */
    protected $changePersData4Result = null;

    /**
     * @param D4WTPCHANGEPERSDATA4RESULT $changePersData4Result
     */
    public function __construct($changePersData4Result)
    {
      $this->changePersData4Result = $changePersData4Result;
    }

    /**
     * @return D4WTPCHANGEPERSDATA4RESULT
     */
    public function getChangePersData4Result()
    {
      return $this->changePersData4Result;
    }

    /**
     * @param D4WTPCHANGEPERSDATA4RESULT $changePersData4Result
     * @return \Axess\Dci4Wtp\changePersData4Response
     */
    public function setChangePersData4Result($changePersData4Result)
    {
      $this->changePersData4Result = $changePersData4Result;
      return $this;
    }

}
