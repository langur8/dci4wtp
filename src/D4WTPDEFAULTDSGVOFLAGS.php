<?php

namespace Axess\Dci4Wtp;

class D4WTPDEFAULTDSGVOFLAGS
{

    /**
     * @var float $BMARKETINGDEFAULT
     */
    protected $BMARKETINGDEFAULT = null;

    /**
     * @var float $BRETRANSMISSION3RDPARTYDEFAULT
     */
    protected $BRETRANSMISSION3RDPARTYDEFAULT = null;

    /**
     * @var float $BSTORAGEDEFAULT
     */
    protected $BSTORAGEDEFAULT = null;

    /**
     * @var float $BTRANSACTRETRANSMISSIONDEFAULT
     */
    protected $BTRANSACTRETRANSMISSIONDEFAULT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBMARKETINGDEFAULT()
    {
      return $this->BMARKETINGDEFAULT;
    }

    /**
     * @param float $BMARKETINGDEFAULT
     * @return \Axess\Dci4Wtp\D4WTPDEFAULTDSGVOFLAGS
     */
    public function setBMARKETINGDEFAULT($BMARKETINGDEFAULT)
    {
      $this->BMARKETINGDEFAULT = $BMARKETINGDEFAULT;
      return $this;
    }

    /**
     * @return float
     */
    public function getBRETRANSMISSION3RDPARTYDEFAULT()
    {
      return $this->BRETRANSMISSION3RDPARTYDEFAULT;
    }

    /**
     * @param float $BRETRANSMISSION3RDPARTYDEFAULT
     * @return \Axess\Dci4Wtp\D4WTPDEFAULTDSGVOFLAGS
     */
    public function setBRETRANSMISSION3RDPARTYDEFAULT($BRETRANSMISSION3RDPARTYDEFAULT)
    {
      $this->BRETRANSMISSION3RDPARTYDEFAULT = $BRETRANSMISSION3RDPARTYDEFAULT;
      return $this;
    }

    /**
     * @return float
     */
    public function getBSTORAGEDEFAULT()
    {
      return $this->BSTORAGEDEFAULT;
    }

    /**
     * @param float $BSTORAGEDEFAULT
     * @return \Axess\Dci4Wtp\D4WTPDEFAULTDSGVOFLAGS
     */
    public function setBSTORAGEDEFAULT($BSTORAGEDEFAULT)
    {
      $this->BSTORAGEDEFAULT = $BSTORAGEDEFAULT;
      return $this;
    }

    /**
     * @return float
     */
    public function getBTRANSACTRETRANSMISSIONDEFAULT()
    {
      return $this->BTRANSACTRETRANSMISSIONDEFAULT;
    }

    /**
     * @param float $BTRANSACTRETRANSMISSIONDEFAULT
     * @return \Axess\Dci4Wtp\D4WTPDEFAULTDSGVOFLAGS
     */
    public function setBTRANSACTRETRANSMISSIONDEFAULT($BTRANSACTRETRANSMISSIONDEFAULT)
    {
      $this->BTRANSACTRETRANSMISSIONDEFAULT = $BTRANSACTRETRANSMISSIONDEFAULT;
      return $this;
    }

}
