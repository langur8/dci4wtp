<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCOMPANY3RESULT
{

    /**
     * @var ArrayOfD4WTPCOMPANYDATA3 $ACTALLCOMPANYDATA
     */
    protected $ACTALLCOMPANYDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCOMPANYDATA3
     */
    public function getACTALLCOMPANYDATA()
    {
      return $this->ACTALLCOMPANYDATA;
    }

    /**
     * @param ArrayOfD4WTPCOMPANYDATA3 $ACTALLCOMPANYDATA
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANY3RESULT
     */
    public function setACTALLCOMPANYDATA($ACTALLCOMPANYDATA)
    {
      $this->ACTALLCOMPANYDATA = $ACTALLCOMPANYDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANY3RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANY3RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
