<?php

namespace Axess\Dci4Wtp;

class D4WTPLOGIN2RESULT
{

    /**
     * @var ArrayOfD4WTPADDRESSDATA $ACTADDRESSDATA
     */
    protected $ACTADDRESSDATA = null;

    /**
     * @var ArrayOfCOMPANYCONTACTPERSON $ACTCONTACTPERSON
     */
    protected $ACTCONTACTPERSON = null;

    /**
     * @var float $BCUSTACTIVE
     */
    protected $BCUSTACTIVE = null;

    /**
     * @var float $BCUSTPAYMENTTYPEFIX
     */
    protected $BCUSTPAYMENTTYPEFIX = null;

    /**
     * @var float $BISB2CSELECTABLE
     */
    protected $BISB2CSELECTABLE = null;

    /**
     * @var float $NCASHIERID
     */
    protected $NCASHIERID = null;

    /**
     * @var float $NCUSTDEADLINEINDAYS
     */
    protected $NCUSTDEADLINEINDAYS = null;

    /**
     * @var float $NCUSTNO
     */
    protected $NCUSTNO = null;

    /**
     * @var float $NCUSTPAYMENTTYPENO
     */
    protected $NCUSTPAYMENTTYPENO = null;

    /**
     * @var float $NCUSTPOSNO
     */
    protected $NCUSTPOSNO = null;

    /**
     * @var float $NCUSTPROFILENO
     */
    protected $NCUSTPROFILENO = null;

    /**
     * @var float $NCUSTPROJNO
     */
    protected $NCUSTPROJNO = null;

    /**
     * @var float $NCUSTTYPENO
     */
    protected $NCUSTTYPENO = null;

    /**
     * @var float $NEMPLOYEENO
     */
    protected $NEMPLOYEENO = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPCASHIERID
     */
    protected $NWTPCASHIERID = null;

    /**
     * @var float $NWTPPACKLISTNO
     */
    protected $NWTPPACKLISTNO = null;

    /**
     * @var float $NWTPPOSNO
     */
    protected $NWTPPOSNO = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var float $NWTPSORTNO
     */
    protected $NWTPSORTNO = null;

    /**
     * @var string $SZCASHIERFIRSTNAME
     */
    protected $SZCASHIERFIRSTNAME = null;

    /**
     * @var string $SZCASHIERLASTNAME
     */
    protected $SZCASHIERLASTNAME = null;

    /**
     * @var string $SZCUSTACCOUNTNO
     */
    protected $SZCUSTACCOUNTNO = null;

    /**
     * @var string $SZCUSTDESCRIPTION
     */
    protected $SZCUSTDESCRIPTION = null;

    /**
     * @var string $SZCUSTEMAIL
     */
    protected $SZCUSTEMAIL = null;

    /**
     * @var string $SZCUSTMASKNAME
     */
    protected $SZCUSTMASKNAME = null;

    /**
     * @var string $SZCUSTNAME
     */
    protected $SZCUSTNAME = null;

    /**
     * @var string $SZCUSTPROFILENAME
     */
    protected $SZCUSTPROFILENAME = null;

    /**
     * @var string $SZCUSTTYPENAME
     */
    protected $SZCUSTTYPENAME = null;

    /**
     * @var string $SZCUSTUID
     */
    protected $SZCUSTUID = null;

    /**
     * @var string $SZCUSTURL
     */
    protected $SZCUSTURL = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZSEARCHNAME
     */
    protected $SZSEARCHNAME = null;

    /**
     * @var string $SZWTPLOGINID
     */
    protected $SZWTPLOGINID = null;

    /**
     * @var string $SZWTPPACKLISTNAME
     */
    protected $SZWTPPACKLISTNAME = null;

    /**
     * @var string $SZWTPPOSNAME
     */
    protected $SZWTPPOSNAME = null;

    /**
     * @var string $SZWTPPROFILENAME
     */
    protected $SZWTPPROFILENAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDRESSDATA
     */
    public function getACTADDRESSDATA()
    {
      return $this->ACTADDRESSDATA;
    }

    /**
     * @param ArrayOfD4WTPADDRESSDATA $ACTADDRESSDATA
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setACTADDRESSDATA($ACTADDRESSDATA)
    {
      $this->ACTADDRESSDATA = $ACTADDRESSDATA;
      return $this;
    }

    /**
     * @return ArrayOfCOMPANYCONTACTPERSON
     */
    public function getACTCONTACTPERSON()
    {
      return $this->ACTCONTACTPERSON;
    }

    /**
     * @param ArrayOfCOMPANYCONTACTPERSON $ACTCONTACTPERSON
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setACTCONTACTPERSON($ACTCONTACTPERSON)
    {
      $this->ACTCONTACTPERSON = $ACTCONTACTPERSON;
      return $this;
    }

    /**
     * @return float
     */
    public function getBCUSTACTIVE()
    {
      return $this->BCUSTACTIVE;
    }

    /**
     * @param float $BCUSTACTIVE
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setBCUSTACTIVE($BCUSTACTIVE)
    {
      $this->BCUSTACTIVE = $BCUSTACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBCUSTPAYMENTTYPEFIX()
    {
      return $this->BCUSTPAYMENTTYPEFIX;
    }

    /**
     * @param float $BCUSTPAYMENTTYPEFIX
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setBCUSTPAYMENTTYPEFIX($BCUSTPAYMENTTYPEFIX)
    {
      $this->BCUSTPAYMENTTYPEFIX = $BCUSTPAYMENTTYPEFIX;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISB2CSELECTABLE()
    {
      return $this->BISB2CSELECTABLE;
    }

    /**
     * @param float $BISB2CSELECTABLE
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setBISB2CSELECTABLE($BISB2CSELECTABLE)
    {
      $this->BISB2CSELECTABLE = $BISB2CSELECTABLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCASHIERID()
    {
      return $this->NCASHIERID;
    }

    /**
     * @param float $NCASHIERID
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNCASHIERID($NCASHIERID)
    {
      $this->NCASHIERID = $NCASHIERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTDEADLINEINDAYS()
    {
      return $this->NCUSTDEADLINEINDAYS;
    }

    /**
     * @param float $NCUSTDEADLINEINDAYS
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNCUSTDEADLINEINDAYS($NCUSTDEADLINEINDAYS)
    {
      $this->NCUSTDEADLINEINDAYS = $NCUSTDEADLINEINDAYS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTNO()
    {
      return $this->NCUSTNO;
    }

    /**
     * @param float $NCUSTNO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNCUSTNO($NCUSTNO)
    {
      $this->NCUSTNO = $NCUSTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPAYMENTTYPENO()
    {
      return $this->NCUSTPAYMENTTYPENO;
    }

    /**
     * @param float $NCUSTPAYMENTTYPENO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNCUSTPAYMENTTYPENO($NCUSTPAYMENTTYPENO)
    {
      $this->NCUSTPAYMENTTYPENO = $NCUSTPAYMENTTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPOSNO()
    {
      return $this->NCUSTPOSNO;
    }

    /**
     * @param float $NCUSTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNCUSTPOSNO($NCUSTPOSNO)
    {
      $this->NCUSTPOSNO = $NCUSTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPROFILENO()
    {
      return $this->NCUSTPROFILENO;
    }

    /**
     * @param float $NCUSTPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNCUSTPROFILENO($NCUSTPROFILENO)
    {
      $this->NCUSTPROFILENO = $NCUSTPROFILENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPROJNO()
    {
      return $this->NCUSTPROJNO;
    }

    /**
     * @param float $NCUSTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNCUSTPROJNO($NCUSTPROJNO)
    {
      $this->NCUSTPROJNO = $NCUSTPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTTYPENO()
    {
      return $this->NCUSTTYPENO;
    }

    /**
     * @param float $NCUSTTYPENO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNCUSTTYPENO($NCUSTTYPENO)
    {
      $this->NCUSTTYPENO = $NCUSTTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEMPLOYEENO()
    {
      return $this->NEMPLOYEENO;
    }

    /**
     * @param float $NEMPLOYEENO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNEMPLOYEENO($NEMPLOYEENO)
    {
      $this->NEMPLOYEENO = $NEMPLOYEENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPCASHIERID()
    {
      return $this->NWTPCASHIERID;
    }

    /**
     * @param float $NWTPCASHIERID
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNWTPCASHIERID($NWTPCASHIERID)
    {
      $this->NWTPCASHIERID = $NWTPCASHIERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPACKLISTNO()
    {
      return $this->NWTPPACKLISTNO;
    }

    /**
     * @param float $NWTPPACKLISTNO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNWTPPACKLISTNO($NWTPPACKLISTNO)
    {
      $this->NWTPPACKLISTNO = $NWTPPACKLISTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPOSNO()
    {
      return $this->NWTPPOSNO;
    }

    /**
     * @param float $NWTPPOSNO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNWTPPOSNO($NWTPPOSNO)
    {
      $this->NWTPPOSNO = $NWTPPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPSORTNO()
    {
      return $this->NWTPSORTNO;
    }

    /**
     * @param float $NWTPSORTNO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setNWTPSORTNO($NWTPSORTNO)
    {
      $this->NWTPSORTNO = $NWTPSORTNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCASHIERFIRSTNAME()
    {
      return $this->SZCASHIERFIRSTNAME;
    }

    /**
     * @param string $SZCASHIERFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZCASHIERFIRSTNAME($SZCASHIERFIRSTNAME)
    {
      $this->SZCASHIERFIRSTNAME = $SZCASHIERFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCASHIERLASTNAME()
    {
      return $this->SZCASHIERLASTNAME;
    }

    /**
     * @param string $SZCASHIERLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZCASHIERLASTNAME($SZCASHIERLASTNAME)
    {
      $this->SZCASHIERLASTNAME = $SZCASHIERLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTACCOUNTNO()
    {
      return $this->SZCUSTACCOUNTNO;
    }

    /**
     * @param string $SZCUSTACCOUNTNO
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZCUSTACCOUNTNO($SZCUSTACCOUNTNO)
    {
      $this->SZCUSTACCOUNTNO = $SZCUSTACCOUNTNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTDESCRIPTION()
    {
      return $this->SZCUSTDESCRIPTION;
    }

    /**
     * @param string $SZCUSTDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZCUSTDESCRIPTION($SZCUSTDESCRIPTION)
    {
      $this->SZCUSTDESCRIPTION = $SZCUSTDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTEMAIL()
    {
      return $this->SZCUSTEMAIL;
    }

    /**
     * @param string $SZCUSTEMAIL
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZCUSTEMAIL($SZCUSTEMAIL)
    {
      $this->SZCUSTEMAIL = $SZCUSTEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTMASKNAME()
    {
      return $this->SZCUSTMASKNAME;
    }

    /**
     * @param string $SZCUSTMASKNAME
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZCUSTMASKNAME($SZCUSTMASKNAME)
    {
      $this->SZCUSTMASKNAME = $SZCUSTMASKNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTNAME()
    {
      return $this->SZCUSTNAME;
    }

    /**
     * @param string $SZCUSTNAME
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZCUSTNAME($SZCUSTNAME)
    {
      $this->SZCUSTNAME = $SZCUSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTPROFILENAME()
    {
      return $this->SZCUSTPROFILENAME;
    }

    /**
     * @param string $SZCUSTPROFILENAME
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZCUSTPROFILENAME($SZCUSTPROFILENAME)
    {
      $this->SZCUSTPROFILENAME = $SZCUSTPROFILENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTTYPENAME()
    {
      return $this->SZCUSTTYPENAME;
    }

    /**
     * @param string $SZCUSTTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZCUSTTYPENAME($SZCUSTTYPENAME)
    {
      $this->SZCUSTTYPENAME = $SZCUSTTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTUID()
    {
      return $this->SZCUSTUID;
    }

    /**
     * @param string $SZCUSTUID
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZCUSTUID($SZCUSTUID)
    {
      $this->SZCUSTUID = $SZCUSTUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTURL()
    {
      return $this->SZCUSTURL;
    }

    /**
     * @param string $SZCUSTURL
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZCUSTURL($SZCUSTURL)
    {
      $this->SZCUSTURL = $SZCUSTURL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSEARCHNAME()
    {
      return $this->SZSEARCHNAME;
    }

    /**
     * @param string $SZSEARCHNAME
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZSEARCHNAME($SZSEARCHNAME)
    {
      $this->SZSEARCHNAME = $SZSEARCHNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPLOGINID()
    {
      return $this->SZWTPLOGINID;
    }

    /**
     * @param string $SZWTPLOGINID
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZWTPLOGINID($SZWTPLOGINID)
    {
      $this->SZWTPLOGINID = $SZWTPLOGINID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPPACKLISTNAME()
    {
      return $this->SZWTPPACKLISTNAME;
    }

    /**
     * @param string $SZWTPPACKLISTNAME
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZWTPPACKLISTNAME($SZWTPPACKLISTNAME)
    {
      $this->SZWTPPACKLISTNAME = $SZWTPPACKLISTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPPOSNAME()
    {
      return $this->SZWTPPOSNAME;
    }

    /**
     * @param string $SZWTPPOSNAME
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZWTPPOSNAME($SZWTPPOSNAME)
    {
      $this->SZWTPPOSNAME = $SZWTPPOSNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPPROFILENAME()
    {
      return $this->SZWTPPROFILENAME;
    }

    /**
     * @param string $SZWTPPROFILENAME
     * @return \Axess\Dci4Wtp\D4WTPLOGIN2RESULT
     */
    public function setSZWTPPROFILENAME($SZWTPPROFILENAME)
    {
      $this->SZWTPPROFILENAME = $SZWTPPROFILENAME;
      return $this;
    }

}
