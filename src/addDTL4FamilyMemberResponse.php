<?php

namespace Axess\Dci4Wtp;

class addDTL4FamilyMemberResponse
{

    /**
     * @var D4WTPADDDTL4FAMILYMEMBERRES $addDTL4FamilyMemberResult
     */
    protected $addDTL4FamilyMemberResult = null;

    /**
     * @param D4WTPADDDTL4FAMILYMEMBERRES $addDTL4FamilyMemberResult
     */
    public function __construct($addDTL4FamilyMemberResult)
    {
      $this->addDTL4FamilyMemberResult = $addDTL4FamilyMemberResult;
    }

    /**
     * @return D4WTPADDDTL4FAMILYMEMBERRES
     */
    public function getAddDTL4FamilyMemberResult()
    {
      return $this->addDTL4FamilyMemberResult;
    }

    /**
     * @param D4WTPADDDTL4FAMILYMEMBERRES $addDTL4FamilyMemberResult
     * @return \Axess\Dci4Wtp\addDTL4FamilyMemberResponse
     */
    public function setAddDTL4FamilyMemberResult($addDTL4FamilyMemberResult)
    {
      $this->addDTL4FamilyMemberResult = $addDTL4FamilyMemberResult;
      return $this;
    }

}
