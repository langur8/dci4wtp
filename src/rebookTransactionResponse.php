<?php

namespace Axess\Dci4Wtp;

class rebookTransactionResponse
{

    /**
     * @var D4WTPREBOOKRESULT $rebookTransactionResult
     */
    protected $rebookTransactionResult = null;

    /**
     * @param D4WTPREBOOKRESULT $rebookTransactionResult
     */
    public function __construct($rebookTransactionResult)
    {
      $this->rebookTransactionResult = $rebookTransactionResult;
    }

    /**
     * @return D4WTPREBOOKRESULT
     */
    public function getRebookTransactionResult()
    {
      return $this->rebookTransactionResult;
    }

    /**
     * @param D4WTPREBOOKRESULT $rebookTransactionResult
     * @return \Axess\Dci4Wtp\rebookTransactionResponse
     */
    public function setRebookTransactionResult($rebookTransactionResult)
    {
      $this->rebookTransactionResult = $rebookTransactionResult;
      return $this;
    }

}
