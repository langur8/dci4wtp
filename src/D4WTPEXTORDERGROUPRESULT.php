<?php

namespace Axess\Dci4Wtp;

class D4WTPEXTORDERGROUPRESULT
{

    /**
     * @var ArrayOfD4WTPTICKETEXTORDERNRRESULT $ACTTICKETEXTORDERNRRESULT
     */
    protected $ACTTICKETEXTORDERNRRESULT = null;

    /**
     * @var string $SZGROUPID
     */
    protected $SZGROUPID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKETEXTORDERNRRESULT
     */
    public function getACTTICKETEXTORDERNRRESULT()
    {
      return $this->ACTTICKETEXTORDERNRRESULT;
    }

    /**
     * @param ArrayOfD4WTPTICKETEXTORDERNRRESULT $ACTTICKETEXTORDERNRRESULT
     * @return \Axess\Dci4Wtp\D4WTPEXTORDERGROUPRESULT
     */
    public function setACTTICKETEXTORDERNRRESULT($ACTTICKETEXTORDERNRRESULT)
    {
      $this->ACTTICKETEXTORDERNRRESULT = $ACTTICKETEXTORDERNRRESULT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGROUPID()
    {
      return $this->SZGROUPID;
    }

    /**
     * @param string $SZGROUPID
     * @return \Axess\Dci4Wtp\D4WTPEXTORDERGROUPRESULT
     */
    public function setSZGROUPID($SZGROUPID)
    {
      $this->SZGROUPID = $SZGROUPID;
      return $this;
    }

}
