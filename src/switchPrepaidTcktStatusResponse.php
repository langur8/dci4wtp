<?php

namespace Axess\Dci4Wtp;

class switchPrepaidTcktStatusResponse
{

    /**
     * @var D4WTPRESULT $switchPrepaidTcktStatusResult
     */
    protected $switchPrepaidTcktStatusResult = null;

    /**
     * @param D4WTPRESULT $switchPrepaidTcktStatusResult
     */
    public function __construct($switchPrepaidTcktStatusResult)
    {
      $this->switchPrepaidTcktStatusResult = $switchPrepaidTcktStatusResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSwitchPrepaidTcktStatusResult()
    {
      return $this->switchPrepaidTcktStatusResult;
    }

    /**
     * @param D4WTPRESULT $switchPrepaidTcktStatusResult
     * @return \Axess\Dci4Wtp\switchPrepaidTcktStatusResponse
     */
    public function setSwitchPrepaidTcktStatusResult($switchPrepaidTcktStatusResult)
    {
      $this->switchPrepaidTcktStatusResult = $switchPrepaidTcktStatusResult;
      return $this;
    }

}
