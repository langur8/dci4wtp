<?php

namespace Axess\Dci4Wtp;

class getTravelGroupList2Response
{

    /**
     * @var D4WTPGETTRAVELGRPLISTRES2 $getTravelGroupList2Result
     */
    protected $getTravelGroupList2Result = null;

    /**
     * @param D4WTPGETTRAVELGRPLISTRES2 $getTravelGroupList2Result
     */
    public function __construct($getTravelGroupList2Result)
    {
      $this->getTravelGroupList2Result = $getTravelGroupList2Result;
    }

    /**
     * @return D4WTPGETTRAVELGRPLISTRES2
     */
    public function getGetTravelGroupList2Result()
    {
      return $this->getTravelGroupList2Result;
    }

    /**
     * @param D4WTPGETTRAVELGRPLISTRES2 $getTravelGroupList2Result
     * @return \Axess\Dci4Wtp\getTravelGroupList2Response
     */
    public function setGetTravelGroupList2Result($getTravelGroupList2Result)
    {
      $this->getTravelGroupList2Result = $getTravelGroupList2Result;
      return $this;
    }

}
