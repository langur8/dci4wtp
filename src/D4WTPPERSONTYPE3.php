<?php

namespace Axess\Dci4Wtp;

class D4WTPPERSONTYPE3
{

    /**
     * @var float $NCONTRACTNO
     */
    protected $NCONTRACTNO = null;

    /**
     * @var float $NICONSYSNR
     */
    protected $NICONSYSNR = null;

    /**
     * @var float $NICONTYPNR
     */
    protected $NICONTYPNR = null;

    /**
     * @var float $NPERSONTYPENO
     */
    protected $NPERSONTYPENO = null;

    /**
     * @var string $SZCARDMASKFIELD1
     */
    protected $SZCARDMASKFIELD1 = null;

    /**
     * @var string $SZCARDMASKFIELD10
     */
    protected $SZCARDMASKFIELD10 = null;

    /**
     * @var string $SZCARDMASKFIELD2
     */
    protected $SZCARDMASKFIELD2 = null;

    /**
     * @var string $SZCARDMASKFIELD3
     */
    protected $SZCARDMASKFIELD3 = null;

    /**
     * @var string $SZCARDMASKFIELD4
     */
    protected $SZCARDMASKFIELD4 = null;

    /**
     * @var string $SZCARDMASKFIELD5
     */
    protected $SZCARDMASKFIELD5 = null;

    /**
     * @var string $SZCARDMASKFIELD6
     */
    protected $SZCARDMASKFIELD6 = null;

    /**
     * @var string $SZCARDMASKFIELD7
     */
    protected $SZCARDMASKFIELD7 = null;

    /**
     * @var string $SZCARDMASKFIELD8
     */
    protected $SZCARDMASKFIELD8 = null;

    /**
     * @var string $SZCARDMASKFIELD9
     */
    protected $SZCARDMASKFIELD9 = null;

    /**
     * @var string $SZICONNAME
     */
    protected $SZICONNAME = null;

    /**
     * @var string $SZMASKNAME
     */
    protected $SZMASKNAME = null;

    /**
     * @var string $SZMASKSHORTNAME
     */
    protected $SZMASKSHORTNAME = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSHORTNAME
     */
    protected $SZSHORTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCONTRACTNO()
    {
      return $this->NCONTRACTNO;
    }

    /**
     * @param float $NCONTRACTNO
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setNCONTRACTNO($NCONTRACTNO)
    {
      $this->NCONTRACTNO = $NCONTRACTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNICONSYSNR()
    {
      return $this->NICONSYSNR;
    }

    /**
     * @param float $NICONSYSNR
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setNICONSYSNR($NICONSYSNR)
    {
      $this->NICONSYSNR = $NICONSYSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNICONTYPNR()
    {
      return $this->NICONTYPNR;
    }

    /**
     * @param float $NICONTYPNR
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setNICONTYPNR($NICONTYPNR)
    {
      $this->NICONTYPNR = $NICONTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSONTYPENO()
    {
      return $this->NPERSONTYPENO;
    }

    /**
     * @param float $NPERSONTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setNPERSONTYPENO($NPERSONTYPENO)
    {
      $this->NPERSONTYPENO = $NPERSONTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDMASKFIELD1()
    {
      return $this->SZCARDMASKFIELD1;
    }

    /**
     * @param string $SZCARDMASKFIELD1
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZCARDMASKFIELD1($SZCARDMASKFIELD1)
    {
      $this->SZCARDMASKFIELD1 = $SZCARDMASKFIELD1;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDMASKFIELD10()
    {
      return $this->SZCARDMASKFIELD10;
    }

    /**
     * @param string $SZCARDMASKFIELD10
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZCARDMASKFIELD10($SZCARDMASKFIELD10)
    {
      $this->SZCARDMASKFIELD10 = $SZCARDMASKFIELD10;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDMASKFIELD2()
    {
      return $this->SZCARDMASKFIELD2;
    }

    /**
     * @param string $SZCARDMASKFIELD2
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZCARDMASKFIELD2($SZCARDMASKFIELD2)
    {
      $this->SZCARDMASKFIELD2 = $SZCARDMASKFIELD2;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDMASKFIELD3()
    {
      return $this->SZCARDMASKFIELD3;
    }

    /**
     * @param string $SZCARDMASKFIELD3
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZCARDMASKFIELD3($SZCARDMASKFIELD3)
    {
      $this->SZCARDMASKFIELD3 = $SZCARDMASKFIELD3;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDMASKFIELD4()
    {
      return $this->SZCARDMASKFIELD4;
    }

    /**
     * @param string $SZCARDMASKFIELD4
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZCARDMASKFIELD4($SZCARDMASKFIELD4)
    {
      $this->SZCARDMASKFIELD4 = $SZCARDMASKFIELD4;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDMASKFIELD5()
    {
      return $this->SZCARDMASKFIELD5;
    }

    /**
     * @param string $SZCARDMASKFIELD5
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZCARDMASKFIELD5($SZCARDMASKFIELD5)
    {
      $this->SZCARDMASKFIELD5 = $SZCARDMASKFIELD5;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDMASKFIELD6()
    {
      return $this->SZCARDMASKFIELD6;
    }

    /**
     * @param string $SZCARDMASKFIELD6
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZCARDMASKFIELD6($SZCARDMASKFIELD6)
    {
      $this->SZCARDMASKFIELD6 = $SZCARDMASKFIELD6;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDMASKFIELD7()
    {
      return $this->SZCARDMASKFIELD7;
    }

    /**
     * @param string $SZCARDMASKFIELD7
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZCARDMASKFIELD7($SZCARDMASKFIELD7)
    {
      $this->SZCARDMASKFIELD7 = $SZCARDMASKFIELD7;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDMASKFIELD8()
    {
      return $this->SZCARDMASKFIELD8;
    }

    /**
     * @param string $SZCARDMASKFIELD8
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZCARDMASKFIELD8($SZCARDMASKFIELD8)
    {
      $this->SZCARDMASKFIELD8 = $SZCARDMASKFIELD8;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDMASKFIELD9()
    {
      return $this->SZCARDMASKFIELD9;
    }

    /**
     * @param string $SZCARDMASKFIELD9
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZCARDMASKFIELD9($SZCARDMASKFIELD9)
    {
      $this->SZCARDMASKFIELD9 = $SZCARDMASKFIELD9;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZICONNAME()
    {
      return $this->SZICONNAME;
    }

    /**
     * @param string $SZICONNAME
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZICONNAME($SZICONNAME)
    {
      $this->SZICONNAME = $SZICONNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASKNAME()
    {
      return $this->SZMASKNAME;
    }

    /**
     * @param string $SZMASKNAME
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZMASKNAME($SZMASKNAME)
    {
      $this->SZMASKNAME = $SZMASKNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASKSHORTNAME()
    {
      return $this->SZMASKSHORTNAME;
    }

    /**
     * @param string $SZMASKSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZMASKSHORTNAME($SZMASKSHORTNAME)
    {
      $this->SZMASKSHORTNAME = $SZMASKSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHORTNAME()
    {
      return $this->SZSHORTNAME;
    }

    /**
     * @param string $SZSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE3
     */
    public function setSZSHORTNAME($SZSHORTNAME)
    {
      $this->SZSHORTNAME = $SZSHORTNAME;
      return $this;
    }

}
