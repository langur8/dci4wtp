<?php

namespace Axess\Dci4Wtp;

class D4WTPCHECKBONUSPOINTSRES
{

    /**
     * @var float $BBLOCKSATZSPLITTED
     */
    protected $BBLOCKSATZSPLITTED = null;

    /**
     * @var float $BCONDITIONFOUND
     */
    protected $BCONDITIONFOUND = null;

    /**
     * @var float $FBONUSPOINT
     */
    protected $FBONUSPOINT = null;

    /**
     * @var float $FRESTPOINT
     */
    protected $FRESTPOINT = null;

    /**
     * @var float $NCUSTOMERACCOUNTNO
     */
    protected $NCUSTOMERACCOUNTNO = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var ArrayOfD4WTPBLOCKSATZ $TABBLOCKSATZ
     */
    protected $TABBLOCKSATZ = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBBLOCKSATZSPLITTED()
    {
      return $this->BBLOCKSATZSPLITTED;
    }

    /**
     * @param float $BBLOCKSATZSPLITTED
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSRES
     */
    public function setBBLOCKSATZSPLITTED($BBLOCKSATZSPLITTED)
    {
      $this->BBLOCKSATZSPLITTED = $BBLOCKSATZSPLITTED;
      return $this;
    }

    /**
     * @return float
     */
    public function getBCONDITIONFOUND()
    {
      return $this->BCONDITIONFOUND;
    }

    /**
     * @param float $BCONDITIONFOUND
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSRES
     */
    public function setBCONDITIONFOUND($BCONDITIONFOUND)
    {
      $this->BCONDITIONFOUND = $BCONDITIONFOUND;
      return $this;
    }

    /**
     * @return float
     */
    public function getFBONUSPOINT()
    {
      return $this->FBONUSPOINT;
    }

    /**
     * @param float $FBONUSPOINT
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSRES
     */
    public function setFBONUSPOINT($FBONUSPOINT)
    {
      $this->FBONUSPOINT = $FBONUSPOINT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFRESTPOINT()
    {
      return $this->FRESTPOINT;
    }

    /**
     * @param float $FRESTPOINT
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSRES
     */
    public function setFRESTPOINT($FRESTPOINT)
    {
      $this->FRESTPOINT = $FRESTPOINT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTOMERACCOUNTNO()
    {
      return $this->NCUSTOMERACCOUNTNO;
    }

    /**
     * @param float $NCUSTOMERACCOUNTNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSRES
     */
    public function setNCUSTOMERACCOUNTNO($NCUSTOMERACCOUNTNO)
    {
      $this->NCUSTOMERACCOUNTNO = $NCUSTOMERACCOUNTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSRES
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSRES
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPBLOCKSATZ
     */
    public function getTABBLOCKSATZ()
    {
      return $this->TABBLOCKSATZ;
    }

    /**
     * @param ArrayOfD4WTPBLOCKSATZ $TABBLOCKSATZ
     * @return \Axess\Dci4Wtp\D4WTPCHECKBONUSPOINTSRES
     */
    public function setTABBLOCKSATZ($TABBLOCKSATZ)
    {
      $this->TABBLOCKSATZ = $TABBLOCKSATZ;
      return $this;
    }

}
