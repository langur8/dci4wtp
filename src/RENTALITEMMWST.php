<?php

namespace Axess\Dci4Wtp;

class RENTALITEMMWST
{

    /**
     * @var float $NRENTALITEMNO
     */
    protected $NRENTALITEMNO = null;

    /**
     * @var float $NTAXNO
     */
    protected $NTAXNO = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNO()
    {
      return $this->NRENTALITEMNO;
    }

    /**
     * @param float $NRENTALITEMNO
     * @return \Axess\Dci4Wtp\RENTALITEMMWST
     */
    public function setNRENTALITEMNO($NRENTALITEMNO)
    {
      $this->NRENTALITEMNO = $NRENTALITEMNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTAXNO()
    {
      return $this->NTAXNO;
    }

    /**
     * @param float $NTAXNO
     * @return \Axess\Dci4Wtp\RENTALITEMMWST
     */
    public function setNTAXNO($NTAXNO)
    {
      $this->NTAXNO = $NTAXNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\RENTALITEMMWST
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
