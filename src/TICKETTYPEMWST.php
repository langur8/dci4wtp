<?php

namespace Axess\Dci4Wtp;

class TICKETTYPEMWST
{

    /**
     * @var float $NTAXNO
     */
    protected $NTAXNO = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNTAXNO()
    {
      return $this->NTAXNO;
    }

    /**
     * @param float $NTAXNO
     * @return \Axess\Dci4Wtp\TICKETTYPEMWST
     */
    public function setNTAXNO($NTAXNO)
    {
      $this->NTAXNO = $NTAXNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\TICKETTYPEMWST
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\TICKETTYPEMWST
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
