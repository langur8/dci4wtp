<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETTYPE2RESULT
{

    /**
     * @var ArrayOfD4WTPTICKETTYPE2 $ACTTICKETTYPES2
     */
    protected $ACTTICKETTYPES2 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKETTYPE2
     */
    public function getACTTICKETTYPES2()
    {
      return $this->ACTTICKETTYPES2;
    }

    /**
     * @param ArrayOfD4WTPTICKETTYPE2 $ACTTICKETTYPES2
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE2RESULT
     */
    public function setACTTICKETTYPES2($ACTTICKETTYPES2)
    {
      $this->ACTTICKETTYPES2 = $ACTTICKETTYPES2;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
