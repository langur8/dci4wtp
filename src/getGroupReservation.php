<?php

namespace Axess\Dci4Wtp;

class getGroupReservation
{

    /**
     * @var D4WTPGETGROUPRESERVATIONNREQ $i_ctgetGroupReservationReq
     */
    protected $i_ctgetGroupReservationReq = null;

    /**
     * @param D4WTPGETGROUPRESERVATIONNREQ $i_ctgetGroupReservationReq
     */
    public function __construct($i_ctgetGroupReservationReq)
    {
      $this->i_ctgetGroupReservationReq = $i_ctgetGroupReservationReq;
    }

    /**
     * @return D4WTPGETGROUPRESERVATIONNREQ
     */
    public function getI_ctgetGroupReservationReq()
    {
      return $this->i_ctgetGroupReservationReq;
    }

    /**
     * @param D4WTPGETGROUPRESERVATIONNREQ $i_ctgetGroupReservationReq
     * @return \Axess\Dci4Wtp\getGroupReservation
     */
    public function setI_ctgetGroupReservationReq($i_ctgetGroupReservationReq)
    {
      $this->i_ctgetGroupReservationReq = $i_ctgetGroupReservationReq;
      return $this;
    }

}
