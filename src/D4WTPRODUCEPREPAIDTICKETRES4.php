<?php

namespace Axess\Dci4Wtp;

class D4WTPRODUCEPREPAIDTICKETRES4
{

    /**
     * @var ArrayOfD4WTPPREPAIDARTICLESPRINT $ACTPREPAIDARTICLESPRINT
     */
    protected $ACTPREPAIDARTICLESPRINT = null;

    /**
     * @var base64Binary $BLPHOTODATA
     */
    protected $BLPHOTODATA = null;

    /**
     * @var D4WTPBLANKTYPE $CTBLANKTYPE
     */
    protected $CTBLANKTYPE = null;

    /**
     * @var D4WTPCODINGDATA $CTCODINGDATA
     */
    protected $CTCODINGDATA = null;

    /**
     * @var D4WTPSEGMENT $CTSEGMENT
     */
    protected $CTSEGMENT = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var string $SZBINCODE
     */
    protected $SZBINCODE = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZEXTORDERNR
     */
    protected $SZEXTORDERNR = null;

    /**
     * @var string $SZPRINTDATA
     */
    protected $SZPRINTDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPREPAIDARTICLESPRINT
     */
    public function getACTPREPAIDARTICLESPRINT()
    {
      return $this->ACTPREPAIDARTICLESPRINT;
    }

    /**
     * @param ArrayOfD4WTPPREPAIDARTICLESPRINT $ACTPREPAIDARTICLESPRINT
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function setACTPREPAIDARTICLESPRINT($ACTPREPAIDARTICLESPRINT)
    {
      $this->ACTPREPAIDARTICLESPRINT = $ACTPREPAIDARTICLESPRINT;
      return $this;
    }

    /**
     * @return base64Binary
     */
    public function getBLPHOTODATA()
    {
      return $this->BLPHOTODATA;
    }

    /**
     * @param base64Binary $BLPHOTODATA
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function setBLPHOTODATA($BLPHOTODATA)
    {
      $this->BLPHOTODATA = $BLPHOTODATA;
      return $this;
    }

    /**
     * @return D4WTPBLANKTYPE
     */
    public function getCTBLANKTYPE()
    {
      return $this->CTBLANKTYPE;
    }

    /**
     * @param D4WTPBLANKTYPE $CTBLANKTYPE
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function setCTBLANKTYPE($CTBLANKTYPE)
    {
      $this->CTBLANKTYPE = $CTBLANKTYPE;
      return $this;
    }

    /**
     * @return D4WTPCODINGDATA
     */
    public function getCTCODINGDATA()
    {
      return $this->CTCODINGDATA;
    }

    /**
     * @param D4WTPCODINGDATA $CTCODINGDATA
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function setCTCODINGDATA($CTCODINGDATA)
    {
      $this->CTCODINGDATA = $CTCODINGDATA;
      return $this;
    }

    /**
     * @return D4WTPSEGMENT
     */
    public function getCTSEGMENT()
    {
      return $this->CTSEGMENT;
    }

    /**
     * @param D4WTPSEGMENT $CTSEGMENT
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function setCTSEGMENT($CTSEGMENT)
    {
      $this->CTSEGMENT = $CTSEGMENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBINCODE()
    {
      return $this->SZBINCODE;
    }

    /**
     * @param string $SZBINCODE
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function setSZBINCODE($SZBINCODE)
    {
      $this->SZBINCODE = $SZBINCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTORDERNR()
    {
      return $this->SZEXTORDERNR;
    }

    /**
     * @param string $SZEXTORDERNR
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function setSZEXTORDERNR($SZEXTORDERNR)
    {
      $this->SZEXTORDERNR = $SZEXTORDERNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPRINTDATA()
    {
      return $this->SZPRINTDATA;
    }

    /**
     * @param string $SZPRINTDATA
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function setSZPRINTDATA($SZPRINTDATA)
    {
      $this->SZPRINTDATA = $SZPRINTDATA;
      return $this;
    }

}
