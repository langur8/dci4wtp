<?php

namespace Axess\Dci4Wtp;

class D4WTPLOGOUTREQUEST
{

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZSOAPPASSWORD
     */
    protected $SZSOAPPASSWORD = null;

    /**
     * @var string $SZSOAPUSERNAME
     */
    protected $SZSOAPUSERNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPLOGOUTREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSOAPPASSWORD()
    {
      return $this->SZSOAPPASSWORD;
    }

    /**
     * @param string $SZSOAPPASSWORD
     * @return \Axess\Dci4Wtp\D4WTPLOGOUTREQUEST
     */
    public function setSZSOAPPASSWORD($SZSOAPPASSWORD)
    {
      $this->SZSOAPPASSWORD = $SZSOAPPASSWORD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSOAPUSERNAME()
    {
      return $this->SZSOAPUSERNAME;
    }

    /**
     * @param string $SZSOAPUSERNAME
     * @return \Axess\Dci4Wtp\D4WTPLOGOUTREQUEST
     */
    public function setSZSOAPUSERNAME($SZSOAPUSERNAME)
    {
      $this->SZSOAPUSERNAME = $SZSOAPUSERNAME;
      return $this;
    }

}
