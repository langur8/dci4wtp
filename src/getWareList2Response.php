<?php

namespace Axess\Dci4Wtp;

class getWareList2Response
{

    /**
     * @var D4WTPGETWARELISTRESULT2 $getWareList2Result
     */
    protected $getWareList2Result = null;

    /**
     * @param D4WTPGETWARELISTRESULT2 $getWareList2Result
     */
    public function __construct($getWareList2Result)
    {
      $this->getWareList2Result = $getWareList2Result;
    }

    /**
     * @return D4WTPGETWARELISTRESULT2
     */
    public function getGetWareList2Result()
    {
      return $this->getWareList2Result;
    }

    /**
     * @param D4WTPGETWARELISTRESULT2 $getWareList2Result
     * @return \Axess\Dci4Wtp\getWareList2Response
     */
    public function setGetWareList2Result($getWareList2Result)
    {
      $this->getWareList2Result = $getWareList2Result;
      return $this;
    }

}
