<?php

namespace Axess\Dci4Wtp;

class D4WTPRENTALITEMPRODUCT5
{

    /**
     * @var ArrayOfDCI4WTPGETWEBRENTALBYPROP $ACTWEBRENTALPROPS
     */
    protected $ACTWEBRENTALPROPS = null;

    /**
     * @var float $NARCNR
     */
    protected $NARCNR = null;

    /**
     * @var float $NLESSONARCNR
     */
    protected $NLESSONARCNR = null;

    /**
     * @var float $NLESSONNR
     */
    protected $NLESSONNR = null;

    /**
     * @var float $NLESSONPROJNR
     */
    protected $NLESSONPROJNR = null;

    /**
     * @var float $NPACKNR
     */
    protected $NPACKNR = null;

    /**
     * @var float $NPACKPOSNR
     */
    protected $NPACKPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NRENTALITEMNR
     */
    protected $NRENTALITEMNR = null;

    /**
     * @var float $NRENTALPERSTYPENR
     */
    protected $NRENTALPERSTYPENR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfDCI4WTPGETWEBRENTALBYPROP
     */
    public function getACTWEBRENTALPROPS()
    {
      return $this->ACTWEBRENTALPROPS;
    }

    /**
     * @param ArrayOfDCI4WTPGETWEBRENTALBYPROP $ACTWEBRENTALPROPS
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT5
     */
    public function setACTWEBRENTALPROPS($ACTWEBRENTALPROPS)
    {
      $this->ACTWEBRENTALPROPS = $ACTWEBRENTALPROPS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARCNR()
    {
      return $this->NARCNR;
    }

    /**
     * @param float $NARCNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT5
     */
    public function setNARCNR($NARCNR)
    {
      $this->NARCNR = $NARCNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLESSONARCNR()
    {
      return $this->NLESSONARCNR;
    }

    /**
     * @param float $NLESSONARCNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT5
     */
    public function setNLESSONARCNR($NLESSONARCNR)
    {
      $this->NLESSONARCNR = $NLESSONARCNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLESSONNR()
    {
      return $this->NLESSONNR;
    }

    /**
     * @param float $NLESSONNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT5
     */
    public function setNLESSONNR($NLESSONNR)
    {
      $this->NLESSONNR = $NLESSONNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLESSONPROJNR()
    {
      return $this->NLESSONPROJNR;
    }

    /**
     * @param float $NLESSONPROJNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT5
     */
    public function setNLESSONPROJNR($NLESSONPROJNR)
    {
      $this->NLESSONPROJNR = $NLESSONPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKNR()
    {
      return $this->NPACKNR;
    }

    /**
     * @param float $NPACKNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT5
     */
    public function setNPACKNR($NPACKNR)
    {
      $this->NPACKNR = $NPACKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKPOSNR()
    {
      return $this->NPACKPOSNR;
    }

    /**
     * @param float $NPACKPOSNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT5
     */
    public function setNPACKPOSNR($NPACKPOSNR)
    {
      $this->NPACKPOSNR = $NPACKPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT5
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT5
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNR()
    {
      return $this->NRENTALITEMNR;
    }

    /**
     * @param float $NRENTALITEMNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT5
     */
    public function setNRENTALITEMNR($NRENTALITEMNR)
    {
      $this->NRENTALITEMNR = $NRENTALITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPERSTYPENR()
    {
      return $this->NRENTALPERSTYPENR;
    }

    /**
     * @param float $NRENTALPERSTYPENR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT5
     */
    public function setNRENTALPERSTYPENR($NRENTALPERSTYPENR)
    {
      $this->NRENTALPERSTYPENR = $NRENTALPERSTYPENR;
      return $this;
    }

}
