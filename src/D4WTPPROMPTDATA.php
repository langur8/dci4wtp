<?php

namespace Axess\Dci4Wtp;

class D4WTPPROMPTDATA
{

    /**
     * @var ArrayOfD4WTPPROMPTELEMENTS $ACTPROMPTELEMENTS
     */
    protected $ACTPROMPTELEMENTS = null;

    /**
     * @var D4WTPPROMPTADDITONALDATA $CTPROMPTADDITONALDATA
     */
    protected $CTPROMPTADDITONALDATA = null;

    /**
     * @var float $NBLOCKNO
     */
    protected $NBLOCKNO = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPROMPTNO
     */
    protected $NPROMPTNO = null;

    /**
     * @var float $NPROMPTTYPENO
     */
    protected $NPROMPTTYPENO = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPROMPTELEMENTS
     */
    public function getACTPROMPTELEMENTS()
    {
      return $this->ACTPROMPTELEMENTS;
    }

    /**
     * @param ArrayOfD4WTPPROMPTELEMENTS $ACTPROMPTELEMENTS
     * @return \Axess\Dci4Wtp\D4WTPPROMPTDATA
     */
    public function setACTPROMPTELEMENTS($ACTPROMPTELEMENTS)
    {
      $this->ACTPROMPTELEMENTS = $ACTPROMPTELEMENTS;
      return $this;
    }

    /**
     * @return D4WTPPROMPTADDITONALDATA
     */
    public function getCTPROMPTADDITONALDATA()
    {
      return $this->CTPROMPTADDITONALDATA;
    }

    /**
     * @param D4WTPPROMPTADDITONALDATA $CTPROMPTADDITONALDATA
     * @return \Axess\Dci4Wtp\D4WTPPROMPTDATA
     */
    public function setCTPROMPTADDITONALDATA($CTPROMPTADDITONALDATA)
    {
      $this->CTPROMPTADDITONALDATA = $CTPROMPTADDITONALDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKNO()
    {
      return $this->NBLOCKNO;
    }

    /**
     * @param float $NBLOCKNO
     * @return \Axess\Dci4Wtp\D4WTPPROMPTDATA
     */
    public function setNBLOCKNO($NBLOCKNO)
    {
      $this->NBLOCKNO = $NBLOCKNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPPROMPTDATA
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROMPTNO()
    {
      return $this->NPROMPTNO;
    }

    /**
     * @param float $NPROMPTNO
     * @return \Axess\Dci4Wtp\D4WTPPROMPTDATA
     */
    public function setNPROMPTNO($NPROMPTNO)
    {
      $this->NPROMPTNO = $NPROMPTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROMPTTYPENO()
    {
      return $this->NPROMPTTYPENO;
    }

    /**
     * @param float $NPROMPTTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPROMPTDATA
     */
    public function setNPROMPTTYPENO($NPROMPTTYPENO)
    {
      $this->NPROMPTTYPENO = $NPROMPTTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPPROMPTDATA
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

}
