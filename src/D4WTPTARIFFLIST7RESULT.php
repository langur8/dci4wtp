<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST7RESULT
{

    /**
     * @var ArrayOfD4WTPTARIFFLIST7 $ACTTARIFFLIST7
     */
    protected $ACTTARIFFLIST7 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLIST7
     */
    public function getACTTARIFFLIST7()
    {
      return $this->ACTTARIFFLIST7;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLIST7 $ACTTARIFFLIST7
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7RESULT
     */
    public function setACTTARIFFLIST7($ACTTARIFFLIST7)
    {
      $this->ACTTARIFFLIST7 = $ACTTARIFFLIST7;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
