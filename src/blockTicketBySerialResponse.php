<?php

namespace Axess\Dci4Wtp;

class blockTicketBySerialResponse
{

    /**
     * @var D4WTPRESULT $blockTicketBySerialResult
     */
    protected $blockTicketBySerialResult = null;

    /**
     * @param D4WTPRESULT $blockTicketBySerialResult
     */
    public function __construct($blockTicketBySerialResult)
    {
      $this->blockTicketBySerialResult = $blockTicketBySerialResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getBlockTicketBySerialResult()
    {
      return $this->blockTicketBySerialResult;
    }

    /**
     * @param D4WTPRESULT $blockTicketBySerialResult
     * @return \Axess\Dci4Wtp\blockTicketBySerialResponse
     */
    public function setBlockTicketBySerialResult($blockTicketBySerialResult)
    {
      $this->blockTicketBySerialResult = $blockTicketBySerialResult;
      return $this;
    }

}
