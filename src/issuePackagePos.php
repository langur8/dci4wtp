<?php

namespace Axess\Dci4Wtp;

class issuePackagePos
{

    /**
     * @var D4WTPISSUEPACKAGEREQUEST $i_ctIssuePackageReq
     */
    protected $i_ctIssuePackageReq = null;

    /**
     * @param D4WTPISSUEPACKAGEREQUEST $i_ctIssuePackageReq
     */
    public function __construct($i_ctIssuePackageReq)
    {
      $this->i_ctIssuePackageReq = $i_ctIssuePackageReq;
    }

    /**
     * @return D4WTPISSUEPACKAGEREQUEST
     */
    public function getI_ctIssuePackageReq()
    {
      return $this->i_ctIssuePackageReq;
    }

    /**
     * @param D4WTPISSUEPACKAGEREQUEST $i_ctIssuePackageReq
     * @return \Axess\Dci4Wtp\issuePackagePos
     */
    public function setI_ctIssuePackageReq($i_ctIssuePackageReq)
    {
      $this->i_ctIssuePackageReq = $i_ctIssuePackageReq;
      return $this;
    }

}
