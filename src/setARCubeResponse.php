<?php

namespace Axess\Dci4Wtp;

class setARCubeResponse
{

    /**
     * @var D4WTPRESULT $setARCubeResult
     */
    protected $setARCubeResult = null;

    /**
     * @param D4WTPRESULT $setARCubeResult
     */
    public function __construct($setARCubeResult)
    {
      $this->setARCubeResult = $setARCubeResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSetARCubeResult()
    {
      return $this->setARCubeResult;
    }

    /**
     * @param D4WTPRESULT $setARCubeResult
     * @return \Axess\Dci4Wtp\setARCubeResponse
     */
    public function setSetARCubeResult($setARCubeResult)
    {
      $this->setARCubeResult = $setARCubeResult;
      return $this;
    }

}
