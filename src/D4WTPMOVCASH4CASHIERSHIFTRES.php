<?php

namespace Axess\Dci4Wtp;

class D4WTPMOVCASH4CASHIERSHIFTRES
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NNEWSUM
     */
    protected $NNEWSUM = null;

    /**
     * @var string $SZCASHIERCONF
     */
    protected $SZCASHIERCONF = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPMOVCASH4CASHIERSHIFTRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNEWSUM()
    {
      return $this->NNEWSUM;
    }

    /**
     * @param float $NNEWSUM
     * @return \Axess\Dci4Wtp\D4WTPMOVCASH4CASHIERSHIFTRES
     */
    public function setNNEWSUM($NNEWSUM)
    {
      $this->NNEWSUM = $NNEWSUM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCASHIERCONF()
    {
      return $this->SZCASHIERCONF;
    }

    /**
     * @param string $SZCASHIERCONF
     * @return \Axess\Dci4Wtp\D4WTPMOVCASH4CASHIERSHIFTRES
     */
    public function setSZCASHIERCONF($SZCASHIERCONF)
    {
      $this->SZCASHIERCONF = $SZCASHIERCONF;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPMOVCASH4CASHIERSHIFTRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
