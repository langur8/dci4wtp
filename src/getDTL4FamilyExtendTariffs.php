<?php

namespace Axess\Dci4Wtp;

class getDTL4FamilyExtendTariffs
{

    /**
     * @var D4WTPGETADDDAYSTARIFFSREQ $i_ctGetAddDaysTariffsReq
     */
    protected $i_ctGetAddDaysTariffsReq = null;

    /**
     * @param D4WTPGETADDDAYSTARIFFSREQ $i_ctGetAddDaysTariffsReq
     */
    public function __construct($i_ctGetAddDaysTariffsReq)
    {
      $this->i_ctGetAddDaysTariffsReq = $i_ctGetAddDaysTariffsReq;
    }

    /**
     * @return D4WTPGETADDDAYSTARIFFSREQ
     */
    public function getI_ctGetAddDaysTariffsReq()
    {
      return $this->i_ctGetAddDaysTariffsReq;
    }

    /**
     * @param D4WTPGETADDDAYSTARIFFSREQ $i_ctGetAddDaysTariffsReq
     * @return \Axess\Dci4Wtp\getDTL4FamilyExtendTariffs
     */
    public function setI_ctGetAddDaysTariffsReq($i_ctGetAddDaysTariffsReq)
    {
      $this->i_ctGetAddDaysTariffsReq = $i_ctGetAddDaysTariffsReq;
      return $this;
    }

}
