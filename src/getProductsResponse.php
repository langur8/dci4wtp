<?php

namespace Axess\Dci4Wtp;

class getProductsResponse
{

    /**
     * @var D4WTPPRODUCTRESULT $getProductsResult
     */
    protected $getProductsResult = null;

    /**
     * @param D4WTPPRODUCTRESULT $getProductsResult
     */
    public function __construct($getProductsResult)
    {
      $this->getProductsResult = $getProductsResult;
    }

    /**
     * @return D4WTPPRODUCTRESULT
     */
    public function getGetProductsResult()
    {
      return $this->getProductsResult;
    }

    /**
     * @param D4WTPPRODUCTRESULT $getProductsResult
     * @return \Axess\Dci4Wtp\getProductsResponse
     */
    public function setGetProductsResult($getProductsResult)
    {
      $this->getProductsResult = $getProductsResult;
      return $this;
    }

}
