<?php

namespace Axess\Dci4Wtp;

class getWareListResponse
{

    /**
     * @var D4WTPGETWARELISTRESULT $getWareListResult
     */
    protected $getWareListResult = null;

    /**
     * @param D4WTPGETWARELISTRESULT $getWareListResult
     */
    public function __construct($getWareListResult)
    {
      $this->getWareListResult = $getWareListResult;
    }

    /**
     * @return D4WTPGETWARELISTRESULT
     */
    public function getGetWareListResult()
    {
      return $this->getWareListResult;
    }

    /**
     * @param D4WTPGETWARELISTRESULT $getWareListResult
     * @return \Axess\Dci4Wtp\getWareListResponse
     */
    public function setGetWareListResult($getWareListResult)
    {
      $this->getWareListResult = $getWareListResult;
      return $this;
    }

}
