<?php

namespace Axess\Dci4Wtp;

class findPrepaidTicket3Response
{

    /**
     * @var D4WTPREPAIDRESULT3 $findPrepaidTicket3Result
     */
    protected $findPrepaidTicket3Result = null;

    /**
     * @param D4WTPREPAIDRESULT3 $findPrepaidTicket3Result
     */
    public function __construct($findPrepaidTicket3Result)
    {
      $this->findPrepaidTicket3Result = $findPrepaidTicket3Result;
    }

    /**
     * @return D4WTPREPAIDRESULT3
     */
    public function getFindPrepaidTicket3Result()
    {
      return $this->findPrepaidTicket3Result;
    }

    /**
     * @param D4WTPREPAIDRESULT3 $findPrepaidTicket3Result
     * @return \Axess\Dci4Wtp\findPrepaidTicket3Response
     */
    public function setFindPrepaidTicket3Result($findPrepaidTicket3Result)
    {
      $this->findPrepaidTicket3Result = $findPrepaidTicket3Result;
      return $this;
    }

}
