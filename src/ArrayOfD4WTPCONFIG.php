<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCONFIG implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCONFIG[] $D4WTPCONFIG
     */
    protected $D4WTPCONFIG = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCONFIG[]
     */
    public function getD4WTPCONFIG()
    {
      return $this->D4WTPCONFIG;
    }

    /**
     * @param D4WTPCONFIG[] $D4WTPCONFIG
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCONFIG
     */
    public function setD4WTPCONFIG(array $D4WTPCONFIG = null)
    {
      $this->D4WTPCONFIG = $D4WTPCONFIG;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCONFIG[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCONFIG
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCONFIG[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCONFIG $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCONFIG[] = $value;
      } else {
        $this->D4WTPCONFIG[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCONFIG[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCONFIG Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCONFIG);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCONFIG);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCONFIG);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCONFIG);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCONFIG Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCONFIG);
    }

}
