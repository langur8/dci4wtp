<?php

namespace Axess\Dci4Wtp;

class CancelPOEBookingRequest
{

    /**
     * @var ArrayOfPoeBooking $PoeBookings
     */
    protected $PoeBookings = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfPoeBooking
     */
    public function getPoeBookings()
    {
      return $this->PoeBookings;
    }

    /**
     * @param ArrayOfPoeBooking $PoeBookings
     * @return \Axess\Dci4Wtp\CancelPOEBookingRequest
     */
    public function setPoeBookings($PoeBookings)
    {
      $this->PoeBookings = $PoeBookings;
      return $this;
    }

}
