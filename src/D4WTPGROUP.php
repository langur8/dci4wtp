<?php

namespace Axess\Dci4Wtp;

class D4WTPGROUP
{

    /**
     * @var ArrayOfWTPPOOLTICKETTYPE $ACTPOOLTICKETTYPE
     */
    protected $ACTPOOLTICKETTYPE = null;

    /**
     * @var ArrayOfD4WTPPACKAGE $ACTPPACKAGE
     */
    protected $ACTPPACKAGE = null;

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var base64Binary $BLOBPICTURE
     */
    protected $BLOBPICTURE = null;

    /**
     * @var float $NID
     */
    protected $NID = null;

    /**
     * @var float $NSORTNR
     */
    protected $NSORTNR = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSHORTNAME
     */
    protected $SZSHORTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfWTPPOOLTICKETTYPE
     */
    public function getACTPOOLTICKETTYPE()
    {
      return $this->ACTPOOLTICKETTYPE;
    }

    /**
     * @param ArrayOfWTPPOOLTICKETTYPE $ACTPOOLTICKETTYPE
     * @return \Axess\Dci4Wtp\D4WTPGROUP
     */
    public function setACTPOOLTICKETTYPE($ACTPOOLTICKETTYPE)
    {
      $this->ACTPOOLTICKETTYPE = $ACTPOOLTICKETTYPE;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPPACKAGE
     */
    public function getACTPPACKAGE()
    {
      return $this->ACTPPACKAGE;
    }

    /**
     * @param ArrayOfD4WTPPACKAGE $ACTPPACKAGE
     * @return \Axess\Dci4Wtp\D4WTPGROUP
     */
    public function setACTPPACKAGE($ACTPPACKAGE)
    {
      $this->ACTPPACKAGE = $ACTPPACKAGE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPGROUP
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return base64Binary
     */
    public function getBLOBPICTURE()
    {
      return $this->BLOBPICTURE;
    }

    /**
     * @param base64Binary $BLOBPICTURE
     * @return \Axess\Dci4Wtp\D4WTPGROUP
     */
    public function setBLOBPICTURE($BLOBPICTURE)
    {
      $this->BLOBPICTURE = $BLOBPICTURE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNID()
    {
      return $this->NID;
    }

    /**
     * @param float $NID
     * @return \Axess\Dci4Wtp\D4WTPGROUP
     */
    public function setNID($NID)
    {
      $this->NID = $NID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNR()
    {
      return $this->NSORTNR;
    }

    /**
     * @param float $NSORTNR
     * @return \Axess\Dci4Wtp\D4WTPGROUP
     */
    public function setNSORTNR($NSORTNR)
    {
      $this->NSORTNR = $NSORTNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPGROUP
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPGROUP
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHORTNAME()
    {
      return $this->SZSHORTNAME;
    }

    /**
     * @param string $SZSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPGROUP
     */
    public function setSZSHORTNAME($SZSHORTNAME)
    {
      $this->SZSHORTNAME = $SZSHORTNAME;
      return $this;
    }

}
