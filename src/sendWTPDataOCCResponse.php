<?php

namespace Axess\Dci4Wtp;

class sendWTPDataOCCResponse
{

    /**
     * @var D4WTPSENDWTPDATARES $sendWTPDataOCCResult
     */
    protected $sendWTPDataOCCResult = null;

    /**
     * @param D4WTPSENDWTPDATARES $sendWTPDataOCCResult
     */
    public function __construct($sendWTPDataOCCResult)
    {
      $this->sendWTPDataOCCResult = $sendWTPDataOCCResult;
    }

    /**
     * @return D4WTPSENDWTPDATARES
     */
    public function getSendWTPDataOCCResult()
    {
      return $this->sendWTPDataOCCResult;
    }

    /**
     * @param D4WTPSENDWTPDATARES $sendWTPDataOCCResult
     * @return \Axess\Dci4Wtp\sendWTPDataOCCResponse
     */
    public function setSendWTPDataOCCResult($sendWTPDataOCCResult)
    {
      $this->sendWTPDataOCCResult = $sendWTPDataOCCResult;
      return $this;
    }

}
