<?php

namespace Axess\Dci4Wtp;

class getChipcardEntries
{

    /**
     * @var D4WTPCHIPCARDENTRIESREQ $i_ctChipcardEntriesReq
     */
    protected $i_ctChipcardEntriesReq = null;

    /**
     * @param D4WTPCHIPCARDENTRIESREQ $i_ctChipcardEntriesReq
     */
    public function __construct($i_ctChipcardEntriesReq)
    {
      $this->i_ctChipcardEntriesReq = $i_ctChipcardEntriesReq;
    }

    /**
     * @return D4WTPCHIPCARDENTRIESREQ
     */
    public function getI_ctChipcardEntriesReq()
    {
      return $this->i_ctChipcardEntriesReq;
    }

    /**
     * @param D4WTPCHIPCARDENTRIESREQ $i_ctChipcardEntriesReq
     * @return \Axess\Dci4Wtp\getChipcardEntries
     */
    public function setI_ctChipcardEntriesReq($i_ctChipcardEntriesReq)
    {
      $this->i_ctChipcardEntriesReq = $i_ctChipcardEntriesReq;
      return $this;
    }

}
