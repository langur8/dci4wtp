<?php

namespace Axess\Dci4Wtp;

class D4WTPGETELIGIBILITYDISCREQ
{

    /**
     * @var float $BCHECKORGANISATIONAFFILIATION
     */
    protected $BCHECKORGANISATIONAFFILIATION = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBCHECKORGANISATIONAFFILIATION()
    {
      return $this->BCHECKORGANISATIONAFFILIATION;
    }

    /**
     * @param float $BCHECKORGANISATIONAFFILIATION
     * @return \Axess\Dci4Wtp\D4WTPGETELIGIBILITYDISCREQ
     */
    public function setBCHECKORGANISATIONAFFILIATION($BCHECKORGANISATIONAFFILIATION)
    {
      $this->BCHECKORGANISATIONAFFILIATION = $BCHECKORGANISATIONAFFILIATION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETELIGIBILITYDISCREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
