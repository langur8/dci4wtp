<?php

namespace Axess\Dci4Wtp;

class D4WTPSPLITPAYMENTRESULT
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var ArrayOfD4WTPSPLITPAYMENT $SPLITPAYMENTS
     */
    protected $SPLITPAYMENTS = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENTRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPSPLITPAYMENT
     */
    public function getSPLITPAYMENTS()
    {
      return $this->SPLITPAYMENTS;
    }

    /**
     * @param ArrayOfD4WTPSPLITPAYMENT $SPLITPAYMENTS
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENTRESULT
     */
    public function setSPLITPAYMENTS($SPLITPAYMENTS)
    {
      $this->SPLITPAYMENTS = $SPLITPAYMENTS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENTRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
