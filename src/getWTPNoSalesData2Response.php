<?php

namespace Axess\Dci4Wtp;

class getWTPNoSalesData2Response
{

    /**
     * @var D4WTPWTPNOSALESDATA2RESULT $getWTPNoSalesData2Result
     */
    protected $getWTPNoSalesData2Result = null;

    /**
     * @param D4WTPWTPNOSALESDATA2RESULT $getWTPNoSalesData2Result
     */
    public function __construct($getWTPNoSalesData2Result)
    {
      $this->getWTPNoSalesData2Result = $getWTPNoSalesData2Result;
    }

    /**
     * @return D4WTPWTPNOSALESDATA2RESULT
     */
    public function getGetWTPNoSalesData2Result()
    {
      return $this->getWTPNoSalesData2Result;
    }

    /**
     * @param D4WTPWTPNOSALESDATA2RESULT $getWTPNoSalesData2Result
     * @return \Axess\Dci4Wtp\getWTPNoSalesData2Response
     */
    public function setGetWTPNoSalesData2Result($getWTPNoSalesData2Result)
    {
      $this->getWTPNoSalesData2Result = $getWTPNoSalesData2Result;
      return $this;
    }

}
