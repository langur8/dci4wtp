<?php

namespace Axess\Dci4Wtp;

class ArrayOfQRCodeTestData implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var QRCodeTestData[] $QRCodeTestData
     */
    protected $QRCodeTestData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return QRCodeTestData[]
     */
    public function getQRCodeTestData()
    {
      return $this->QRCodeTestData;
    }

    /**
     * @param QRCodeTestData[] $QRCodeTestData
     * @return \Axess\Dci4Wtp\ArrayOfQRCodeTestData
     */
    public function setQRCodeTestData(array $QRCodeTestData = null)
    {
      $this->QRCodeTestData = $QRCodeTestData;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->QRCodeTestData[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return QRCodeTestData
     */
    public function offsetGet($offset)
    {
      return $this->QRCodeTestData[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param QRCodeTestData $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->QRCodeTestData[] = $value;
      } else {
        $this->QRCodeTestData[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->QRCodeTestData[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return QRCodeTestData Return the current element
     */
    public function current()
    {
      return current($this->QRCodeTestData);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->QRCodeTestData);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->QRCodeTestData);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->QRCodeTestData);
    }

    /**
     * Countable implementation
     *
     * @return QRCodeTestData Return count of elements
     */
    public function count()
    {
      return count($this->QRCodeTestData);
    }

}
