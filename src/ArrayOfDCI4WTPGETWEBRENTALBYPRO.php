<?php

namespace Axess\Dci4Wtp;

class ArrayOfDCI4WTPGETWEBRENTALBYPRO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DCI4WTPGETWEBRENTALBYPRO[] $DCI4WTPGETWEBRENTALBYPRO
     */
    protected $DCI4WTPGETWEBRENTALBYPRO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DCI4WTPGETWEBRENTALBYPRO[]
     */
    public function getDCI4WTPGETWEBRENTALBYPRO()
    {
      return $this->DCI4WTPGETWEBRENTALBYPRO;
    }

    /**
     * @param DCI4WTPGETWEBRENTALBYPRO[] $DCI4WTPGETWEBRENTALBYPRO
     * @return \Axess\Dci4Wtp\ArrayOfDCI4WTPGETWEBRENTALBYPRO
     */
    public function setDCI4WTPGETWEBRENTALBYPRO(array $DCI4WTPGETWEBRENTALBYPRO = null)
    {
      $this->DCI4WTPGETWEBRENTALBYPRO = $DCI4WTPGETWEBRENTALBYPRO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DCI4WTPGETWEBRENTALBYPRO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DCI4WTPGETWEBRENTALBYPRO
     */
    public function offsetGet($offset)
    {
      return $this->DCI4WTPGETWEBRENTALBYPRO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DCI4WTPGETWEBRENTALBYPRO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DCI4WTPGETWEBRENTALBYPRO[] = $value;
      } else {
        $this->DCI4WTPGETWEBRENTALBYPRO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DCI4WTPGETWEBRENTALBYPRO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DCI4WTPGETWEBRENTALBYPRO Return the current element
     */
    public function current()
    {
      return current($this->DCI4WTPGETWEBRENTALBYPRO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DCI4WTPGETWEBRENTALBYPRO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DCI4WTPGETWEBRENTALBYPRO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DCI4WTPGETWEBRENTALBYPRO);
    }

    /**
     * Countable implementation
     *
     * @return DCI4WTPGETWEBRENTALBYPRO Return count of elements
     */
    public function count()
    {
      return count($this->DCI4WTPGETWEBRENTALBYPRO);
    }

}
