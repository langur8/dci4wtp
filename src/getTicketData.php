<?php

namespace Axess\Dci4Wtp;

class getTicketData
{

    /**
     * @var D4WTPTICKETDATAREQUEST $i_ctTicketDataReq
     */
    protected $i_ctTicketDataReq = null;

    /**
     * @param D4WTPTICKETDATAREQUEST $i_ctTicketDataReq
     */
    public function __construct($i_ctTicketDataReq)
    {
      $this->i_ctTicketDataReq = $i_ctTicketDataReq;
    }

    /**
     * @return D4WTPTICKETDATAREQUEST
     */
    public function getI_ctTicketDataReq()
    {
      return $this->i_ctTicketDataReq;
    }

    /**
     * @param D4WTPTICKETDATAREQUEST $i_ctTicketDataReq
     * @return \Axess\Dci4Wtp\getTicketData
     */
    public function setI_ctTicketDataReq($i_ctTicketDataReq)
    {
      $this->i_ctTicketDataReq = $i_ctTicketDataReq;
      return $this;
    }

}
