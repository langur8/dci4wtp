<?php

namespace Axess\Dci4Wtp;

class getTravelGrpShopBasPositions2
{

    /**
     * @var D4WTPGETTRAVELGRPSHOPPOSREQ2 $i_ctGETTRAVELGRPShopPosREQ
     */
    protected $i_ctGETTRAVELGRPShopPosREQ = null;

    /**
     * @param D4WTPGETTRAVELGRPSHOPPOSREQ2 $i_ctGETTRAVELGRPShopPosREQ
     */
    public function __construct($i_ctGETTRAVELGRPShopPosREQ)
    {
      $this->i_ctGETTRAVELGRPShopPosREQ = $i_ctGETTRAVELGRPShopPosREQ;
    }

    /**
     * @return D4WTPGETTRAVELGRPSHOPPOSREQ2
     */
    public function getI_ctGETTRAVELGRPShopPosREQ()
    {
      return $this->i_ctGETTRAVELGRPShopPosREQ;
    }

    /**
     * @param D4WTPGETTRAVELGRPSHOPPOSREQ2 $i_ctGETTRAVELGRPShopPosREQ
     * @return \Axess\Dci4Wtp\getTravelGrpShopBasPositions2
     */
    public function setI_ctGETTRAVELGRPShopPosREQ($i_ctGETTRAVELGRPShopPosREQ)
    {
      $this->i_ctGETTRAVELGRPShopPosREQ = $i_ctGETTRAVELGRPShopPosREQ;
      return $this;
    }

}
