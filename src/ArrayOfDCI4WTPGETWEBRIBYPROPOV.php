<?php

namespace Axess\Dci4Wtp;

class ArrayOfDCI4WTPGETWEBRIBYPROPOV implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DCI4WTPGETWEBRIBYPROPOV[] $DCI4WTPGETWEBRIBYPROPOV
     */
    protected $DCI4WTPGETWEBRIBYPROPOV = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DCI4WTPGETWEBRIBYPROPOV[]
     */
    public function getDCI4WTPGETWEBRIBYPROPOV()
    {
      return $this->DCI4WTPGETWEBRIBYPROPOV;
    }

    /**
     * @param DCI4WTPGETWEBRIBYPROPOV[] $DCI4WTPGETWEBRIBYPROPOV
     * @return \Axess\Dci4Wtp\ArrayOfDCI4WTPGETWEBRIBYPROPOV
     */
    public function setDCI4WTPGETWEBRIBYPROPOV(array $DCI4WTPGETWEBRIBYPROPOV = null)
    {
      $this->DCI4WTPGETWEBRIBYPROPOV = $DCI4WTPGETWEBRIBYPROPOV;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DCI4WTPGETWEBRIBYPROPOV[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DCI4WTPGETWEBRIBYPROPOV
     */
    public function offsetGet($offset)
    {
      return $this->DCI4WTPGETWEBRIBYPROPOV[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DCI4WTPGETWEBRIBYPROPOV $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DCI4WTPGETWEBRIBYPROPOV[] = $value;
      } else {
        $this->DCI4WTPGETWEBRIBYPROPOV[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DCI4WTPGETWEBRIBYPROPOV[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DCI4WTPGETWEBRIBYPROPOV Return the current element
     */
    public function current()
    {
      return current($this->DCI4WTPGETWEBRIBYPROPOV);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DCI4WTPGETWEBRIBYPROPOV);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DCI4WTPGETWEBRIBYPROPOV);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DCI4WTPGETWEBRIBYPROPOV);
    }

    /**
     * Countable implementation
     *
     * @return DCI4WTPGETWEBRIBYPROPOV Return count of elements
     */
    public function count()
    {
      return count($this->DCI4WTPGETWEBRIBYPROPOV);
    }

}
