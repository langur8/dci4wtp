<?php

namespace Axess\Dci4Wtp;

class setPromptDataResponse
{

    /**
     * @var D4WTPRESULT $setPromptDataResult
     */
    protected $setPromptDataResult = null;

    /**
     * @param D4WTPRESULT $setPromptDataResult
     */
    public function __construct($setPromptDataResult)
    {
      $this->setPromptDataResult = $setPromptDataResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSetPromptDataResult()
    {
      return $this->setPromptDataResult;
    }

    /**
     * @param D4WTPRESULT $setPromptDataResult
     * @return \Axess\Dci4Wtp\setPromptDataResponse
     */
    public function setSetPromptDataResult($setPromptDataResult)
    {
      $this->setPromptDataResult = $setPromptDataResult;
      return $this;
    }

}
