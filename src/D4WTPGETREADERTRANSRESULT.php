<?php

namespace Axess\Dci4Wtp;

class D4WTPGETREADERTRANSRESULT
{

    /**
     * @var ArrayOfD4WTPGETREADERTRANS $ACTGETREADERTRANS
     */
    protected $ACTGETREADERTRANS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPGETREADERTRANS
     */
    public function getACTGETREADERTRANS()
    {
      return $this->ACTGETREADERTRANS;
    }

    /**
     * @param ArrayOfD4WTPGETREADERTRANS $ACTGETREADERTRANS
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANSRESULT
     */
    public function setACTGETREADERTRANS($ACTGETREADERTRANS)
    {
      $this->ACTGETREADERTRANS = $ACTGETREADERTRANS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANSRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANSRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
