<?php

namespace Axess\Dci4Wtp;

class D4WTPMSGISSUETICKETREQUEST3
{

    /**
     * @var ArrayOfD4WTPMSGTICKETDATA $ACTMSGTICKETDATA
     */
    protected $ACTMSGTICKETDATA = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPMSGTICKETDATA
     */
    public function getACTMSGTICKETDATA()
    {
      return $this->ACTMSGTICKETDATA;
    }

    /**
     * @param ArrayOfD4WTPMSGTICKETDATA $ACTMSGTICKETDATA
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETREQUEST3
     */
    public function setACTMSGTICKETDATA($ACTMSGTICKETDATA)
    {
      $this->ACTMSGTICKETDATA = $ACTMSGTICKETDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETREQUEST3
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
