<?php

namespace Axess\Dci4Wtp;

class D4WTPSETB2CACCOUNTSREQUEST
{

    /**
     * @var D4WTPB2CACCOUNT $CTB2CACCOUNT
     */
    protected $CTB2CACCOUNT = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPB2CACCOUNT
     */
    public function getCTB2CACCOUNT()
    {
      return $this->CTB2CACCOUNT;
    }

    /**
     * @param D4WTPB2CACCOUNT $CTB2CACCOUNT
     * @return \Axess\Dci4Wtp\D4WTPSETB2CACCOUNTSREQUEST
     */
    public function setCTB2CACCOUNT($CTB2CACCOUNT)
    {
      $this->CTB2CACCOUNT = $CTB2CACCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSETB2CACCOUNTSREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
