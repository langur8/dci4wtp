<?php

namespace Axess\Dci4Wtp;

class D4WTPGETWARELISTRESULT
{

    /**
     * @var ArrayOfD4WTPWARELIST $ACTWARELIST
     */
    protected $ACTWARELIST = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPWARELIST
     */
    public function getACTWARELIST()
    {
      return $this->ACTWARELIST;
    }

    /**
     * @param ArrayOfD4WTPWARELIST $ACTWARELIST
     * @return \Axess\Dci4Wtp\D4WTPGETWARELISTRESULT
     */
    public function setACTWARELIST($ACTWARELIST)
    {
      $this->ACTWARELIST = $ACTWARELIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETWARELISTRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETWARELISTRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
