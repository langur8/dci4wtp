<?php

namespace Axess\Dci4Wtp;

class D4WTPWTPNOSALESDATAREQUEST
{

    /**
     * @var float $NCARDTYPENO
     */
    protected $NCARDTYPENO = null;

    /**
     * @var float $NPOSTYPENO
     */
    protected $NPOSTYPENO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZACCEPTANCENO
     */
    protected $SZACCEPTANCENO = null;

    /**
     * @var string $SZCHECKSUM
     */
    protected $SZCHECKSUM = null;

    /**
     * @var string $SZCHIPID
     */
    protected $SZCHIPID = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    /**
     * @var string $SZDATEFROM
     */
    protected $SZDATEFROM = null;

    /**
     * @var string $SZDATETO
     */
    protected $SZDATETO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCARDTYPENO()
    {
      return $this->NCARDTYPENO;
    }

    /**
     * @param float $NCARDTYPENO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATAREQUEST
     */
    public function setNCARDTYPENO($NCARDTYPENO)
    {
      $this->NCARDTYPENO = $NCARDTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSTYPENO()
    {
      return $this->NPOSTYPENO;
    }

    /**
     * @param float $NPOSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATAREQUEST
     */
    public function setNPOSTYPENO($NPOSTYPENO)
    {
      $this->NPOSTYPENO = $NPOSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATAREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATAREQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZACCEPTANCENO()
    {
      return $this->SZACCEPTANCENO;
    }

    /**
     * @param string $SZACCEPTANCENO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATAREQUEST
     */
    public function setSZACCEPTANCENO($SZACCEPTANCENO)
    {
      $this->SZACCEPTANCENO = $SZACCEPTANCENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCHECKSUM()
    {
      return $this->SZCHECKSUM;
    }

    /**
     * @param string $SZCHECKSUM
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATAREQUEST
     */
    public function setSZCHECKSUM($SZCHECKSUM)
    {
      $this->SZCHECKSUM = $SZCHECKSUM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCHIPID()
    {
      return $this->SZCHIPID;
    }

    /**
     * @param string $SZCHIPID
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATAREQUEST
     */
    public function setSZCHIPID($SZCHIPID)
    {
      $this->SZCHIPID = $SZCHIPID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATAREQUEST
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEFROM()
    {
      return $this->SZDATEFROM;
    }

    /**
     * @param string $SZDATEFROM
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATAREQUEST
     */
    public function setSZDATEFROM($SZDATEFROM)
    {
      $this->SZDATEFROM = $SZDATEFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATETO()
    {
      return $this->SZDATETO;
    }

    /**
     * @param string $SZDATETO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATAREQUEST
     */
    public function setSZDATETO($SZDATETO)
    {
      $this->SZDATETO = $SZDATETO;
      return $this;
    }

}
