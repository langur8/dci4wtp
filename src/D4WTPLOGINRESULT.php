<?php

namespace Axess\Dci4Wtp;

class D4WTPLOGINRESULT
{

    /**
     * @var float $NCASHIERID
     */
    protected $NCASHIERID = null;

    /**
     * @var float $NCUSTOMERNO
     */
    protected $NCUSTOMERNO = null;

    /**
     * @var float $NCUSTOMERPOSNO
     */
    protected $NCUSTOMERPOSNO = null;

    /**
     * @var float $NCUSTOMERPROJNO
     */
    protected $NCUSTOMERPROJNO = null;

    /**
     * @var float $NEMPLOYEENO
     */
    protected $NEMPLOYEENO = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZCUSTOMERNAME
     */
    protected $SZCUSTOMERNAME = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCASHIERID()
    {
      return $this->NCASHIERID;
    }

    /**
     * @param float $NCASHIERID
     * @return \Axess\Dci4Wtp\D4WTPLOGINRESULT
     */
    public function setNCASHIERID($NCASHIERID)
    {
      $this->NCASHIERID = $NCASHIERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTOMERNO()
    {
      return $this->NCUSTOMERNO;
    }

    /**
     * @param float $NCUSTOMERNO
     * @return \Axess\Dci4Wtp\D4WTPLOGINRESULT
     */
    public function setNCUSTOMERNO($NCUSTOMERNO)
    {
      $this->NCUSTOMERNO = $NCUSTOMERNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTOMERPOSNO()
    {
      return $this->NCUSTOMERPOSNO;
    }

    /**
     * @param float $NCUSTOMERPOSNO
     * @return \Axess\Dci4Wtp\D4WTPLOGINRESULT
     */
    public function setNCUSTOMERPOSNO($NCUSTOMERPOSNO)
    {
      $this->NCUSTOMERPOSNO = $NCUSTOMERPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTOMERPROJNO()
    {
      return $this->NCUSTOMERPROJNO;
    }

    /**
     * @param float $NCUSTOMERPROJNO
     * @return \Axess\Dci4Wtp\D4WTPLOGINRESULT
     */
    public function setNCUSTOMERPROJNO($NCUSTOMERPROJNO)
    {
      $this->NCUSTOMERPROJNO = $NCUSTOMERPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEMPLOYEENO()
    {
      return $this->NEMPLOYEENO;
    }

    /**
     * @param float $NEMPLOYEENO
     * @return \Axess\Dci4Wtp\D4WTPLOGINRESULT
     */
    public function setNEMPLOYEENO($NEMPLOYEENO)
    {
      $this->NEMPLOYEENO = $NEMPLOYEENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPLOGINRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPLOGINRESULT
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPLOGINRESULT
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPLOGINRESULT
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPLOGINRESULT
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTOMERNAME()
    {
      return $this->SZCUSTOMERNAME;
    }

    /**
     * @param string $SZCUSTOMERNAME
     * @return \Axess\Dci4Wtp\D4WTPLOGINRESULT
     */
    public function setSZCUSTOMERNAME($SZCUSTOMERNAME)
    {
      $this->SZCUSTOMERNAME = $SZCUSTOMERNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPLOGINRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
