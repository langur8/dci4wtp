<?php

namespace Axess\Dci4Wtp;

class verifyNamedUser
{

    /**
     * @var D4WTPVERIFYNAMEDUSERREQUEST $i_ctVerifyNamedUserReq
     */
    protected $i_ctVerifyNamedUserReq = null;

    /**
     * @param D4WTPVERIFYNAMEDUSERREQUEST $i_ctVerifyNamedUserReq
     */
    public function __construct($i_ctVerifyNamedUserReq)
    {
      $this->i_ctVerifyNamedUserReq = $i_ctVerifyNamedUserReq;
    }

    /**
     * @return D4WTPVERIFYNAMEDUSERREQUEST
     */
    public function getI_ctVerifyNamedUserReq()
    {
      return $this->i_ctVerifyNamedUserReq;
    }

    /**
     * @param D4WTPVERIFYNAMEDUSERREQUEST $i_ctVerifyNamedUserReq
     * @return \Axess\Dci4Wtp\verifyNamedUser
     */
    public function setI_ctVerifyNamedUserReq($i_ctVerifyNamedUserReq)
    {
      $this->i_ctVerifyNamedUserReq = $i_ctVerifyNamedUserReq;
      return $this;
    }

}
