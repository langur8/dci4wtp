<?php

namespace Axess\Dci4Wtp;

class getTariffList2Response
{

    /**
     * @var D4WTPTARIFFLIST2RESULT $getTariffList2Result
     */
    protected $getTariffList2Result = null;

    /**
     * @param D4WTPTARIFFLIST2RESULT $getTariffList2Result
     */
    public function __construct($getTariffList2Result)
    {
      $this->getTariffList2Result = $getTariffList2Result;
    }

    /**
     * @return D4WTPTARIFFLIST2RESULT
     */
    public function getGetTariffList2Result()
    {
      return $this->getTariffList2Result;
    }

    /**
     * @param D4WTPTARIFFLIST2RESULT $getTariffList2Result
     * @return \Axess\Dci4Wtp\getTariffList2Response
     */
    public function setGetTariffList2Result($getTariffList2Result)
    {
      $this->getTariffList2Result = $getTariffList2Result;
      return $this;
    }

}
