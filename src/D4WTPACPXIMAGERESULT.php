<?php

namespace Axess\Dci4Wtp;

class D4WTPACPXIMAGERESULT
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPOSX
     */
    protected $NPOSX = null;

    /**
     * @var float $NPOSY
     */
    protected $NPOSY = null;

    /**
     * @var string $SZBARCODE
     */
    protected $SZBARCODE = null;

    /**
     * @var string $SZBASE64IMAGE
     */
    protected $SZBASE64IMAGE = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPACPXIMAGERESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSX()
    {
      return $this->NPOSX;
    }

    /**
     * @param float $NPOSX
     * @return \Axess\Dci4Wtp\D4WTPACPXIMAGERESULT
     */
    public function setNPOSX($NPOSX)
    {
      $this->NPOSX = $NPOSX;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSY()
    {
      return $this->NPOSY;
    }

    /**
     * @param float $NPOSY
     * @return \Axess\Dci4Wtp\D4WTPACPXIMAGERESULT
     */
    public function setNPOSY($NPOSY)
    {
      $this->NPOSY = $NPOSY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBARCODE()
    {
      return $this->SZBARCODE;
    }

    /**
     * @param string $SZBARCODE
     * @return \Axess\Dci4Wtp\D4WTPACPXIMAGERESULT
     */
    public function setSZBARCODE($SZBARCODE)
    {
      $this->SZBARCODE = $SZBARCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBASE64IMAGE()
    {
      return $this->SZBASE64IMAGE;
    }

    /**
     * @param string $SZBASE64IMAGE
     * @return \Axess\Dci4Wtp\D4WTPACPXIMAGERESULT
     */
    public function setSZBASE64IMAGE($SZBASE64IMAGE)
    {
      $this->SZBASE64IMAGE = $SZBASE64IMAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPACPXIMAGERESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
