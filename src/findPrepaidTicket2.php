<?php

namespace Axess\Dci4Wtp;

class findPrepaidTicket2
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var string $i_szExtOrderNr
     */
    protected $i_szExtOrderNr = null;

    /**
     * @var float $i_nProjNr
     */
    protected $i_nProjNr = null;

    /**
     * @var float $i_nPOSNr
     */
    protected $i_nPOSNr = null;

    /**
     * @var float $i_nJournalNr
     */
    protected $i_nJournalNr = null;

    /**
     * @var float $i_nOrderStatusNr
     */
    protected $i_nOrderStatusNr = null;

    /**
     * @var string $i_szDateFrom
     */
    protected $i_szDateFrom = null;

    /**
     * @var string $i_szDateTo
     */
    protected $i_szDateTo = null;

    /**
     * @param float $i_nSessionID
     * @param string $i_szExtOrderNr
     * @param float $i_nProjNr
     * @param float $i_nPOSNr
     * @param float $i_nJournalNr
     * @param float $i_nOrderStatusNr
     * @param string $i_szDateFrom
     * @param string $i_szDateTo
     */
    public function __construct($i_nSessionID, $i_szExtOrderNr, $i_nProjNr, $i_nPOSNr, $i_nJournalNr, $i_nOrderStatusNr, $i_szDateFrom, $i_szDateTo)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_szExtOrderNr = $i_szExtOrderNr;
      $this->i_nProjNr = $i_nProjNr;
      $this->i_nPOSNr = $i_nPOSNr;
      $this->i_nJournalNr = $i_nJournalNr;
      $this->i_nOrderStatusNr = $i_nOrderStatusNr;
      $this->i_szDateFrom = $i_szDateFrom;
      $this->i_szDateTo = $i_szDateTo;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\findPrepaidTicket2
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szExtOrderNr()
    {
      return $this->i_szExtOrderNr;
    }

    /**
     * @param string $i_szExtOrderNr
     * @return \Axess\Dci4Wtp\findPrepaidTicket2
     */
    public function setI_szExtOrderNr($i_szExtOrderNr)
    {
      $this->i_szExtOrderNr = $i_szExtOrderNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nProjNr()
    {
      return $this->i_nProjNr;
    }

    /**
     * @param float $i_nProjNr
     * @return \Axess\Dci4Wtp\findPrepaidTicket2
     */
    public function setI_nProjNr($i_nProjNr)
    {
      $this->i_nProjNr = $i_nProjNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nPOSNr()
    {
      return $this->i_nPOSNr;
    }

    /**
     * @param float $i_nPOSNr
     * @return \Axess\Dci4Wtp\findPrepaidTicket2
     */
    public function setI_nPOSNr($i_nPOSNr)
    {
      $this->i_nPOSNr = $i_nPOSNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nJournalNr()
    {
      return $this->i_nJournalNr;
    }

    /**
     * @param float $i_nJournalNr
     * @return \Axess\Dci4Wtp\findPrepaidTicket2
     */
    public function setI_nJournalNr($i_nJournalNr)
    {
      $this->i_nJournalNr = $i_nJournalNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nOrderStatusNr()
    {
      return $this->i_nOrderStatusNr;
    }

    /**
     * @param float $i_nOrderStatusNr
     * @return \Axess\Dci4Wtp\findPrepaidTicket2
     */
    public function setI_nOrderStatusNr($i_nOrderStatusNr)
    {
      $this->i_nOrderStatusNr = $i_nOrderStatusNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szDateFrom()
    {
      return $this->i_szDateFrom;
    }

    /**
     * @param string $i_szDateFrom
     * @return \Axess\Dci4Wtp\findPrepaidTicket2
     */
    public function setI_szDateFrom($i_szDateFrom)
    {
      $this->i_szDateFrom = $i_szDateFrom;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szDateTo()
    {
      return $this->i_szDateTo;
    }

    /**
     * @param string $i_szDateTo
     * @return \Axess\Dci4Wtp\findPrepaidTicket2
     */
    public function setI_szDateTo($i_szDateTo)
    {
      $this->i_szDateTo = $i_szDateTo;
      return $this;
    }

}
