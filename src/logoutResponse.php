<?php

namespace Axess\Dci4Wtp;

class logoutResponse
{

    /**
     * @var D4WTPLOGOUTRESULT $logoutResult
     */
    protected $logoutResult = null;

    /**
     * @param D4WTPLOGOUTRESULT $logoutResult
     */
    public function __construct($logoutResult)
    {
      $this->logoutResult = $logoutResult;
    }

    /**
     * @return D4WTPLOGOUTRESULT
     */
    public function getLogoutResult()
    {
      return $this->logoutResult;
    }

    /**
     * @param D4WTPLOGOUTRESULT $logoutResult
     * @return \Axess\Dci4Wtp\logoutResponse
     */
    public function setLogoutResult($logoutResult)
    {
      $this->logoutResult = $logoutResult;
      return $this;
    }

}
