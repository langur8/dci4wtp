<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCTCREFIDRESULT
{

    /**
     * @var ArrayOfD4WTPCREDITCARDREFDATA $ACTCREDITCARDREFDATA
     */
    protected $ACTCREDITCARDREFDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCREDITCARDREFDATA
     */
    public function getACTCREDITCARDREFDATA()
    {
      return $this->ACTCREDITCARDREFDATA;
    }

    /**
     * @param ArrayOfD4WTPCREDITCARDREFDATA $ACTCREDITCARDREFDATA
     * @return \Axess\Dci4Wtp\D4WTPGETCTCREFIDRESULT
     */
    public function setACTCREDITCARDREFDATA($ACTCREDITCARDREFDATA)
    {
      $this->ACTCREDITCARDREFDATA = $ACTCREDITCARDREFDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETCTCREFIDRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETCTCREFIDRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
