<?php

namespace Axess\Dci4Wtp;

class TicketBooking
{

    /**
     * @var string $dtDate
     */
    protected $dtDate = null;

    /**
     * @var int $nConfigNr
     */
    protected $nConfigNr = null;

    /**
     * @var int $nPoolNr
     */
    protected $nPoolNr = null;

    /**
     * @var string $szName
     */
    protected $szName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDtDate()
    {
      return $this->dtDate;
    }

    /**
     * @param string $dtDate
     * @return \Axess\Dci4Wtp\TicketBooking
     */
    public function setDtDate($dtDate)
    {
      $this->dtDate = $dtDate;
      return $this;
    }

    /**
     * @return int
     */
    public function getNConfigNr()
    {
      return $this->nConfigNr;
    }

    /**
     * @param int $nConfigNr
     * @return \Axess\Dci4Wtp\TicketBooking
     */
    public function setNConfigNr($nConfigNr)
    {
      $this->nConfigNr = $nConfigNr;
      return $this;
    }

    /**
     * @return int
     */
    public function getNPoolNr()
    {
      return $this->nPoolNr;
    }

    /**
     * @param int $nPoolNr
     * @return \Axess\Dci4Wtp\TicketBooking
     */
    public function setNPoolNr($nPoolNr)
    {
      $this->nPoolNr = $nPoolNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzName()
    {
      return $this->szName;
    }

    /**
     * @param string $szName
     * @return \Axess\Dci4Wtp\TicketBooking
     */
    public function setSzName($szName)
    {
      $this->szName = $szName;
      return $this;
    }

}
