<?php

namespace Axess\Dci4Wtp;

class getTariffList5Response
{

    /**
     * @var D4WTPTARIFFLIST5RESULT $getTariffList5Result
     */
    protected $getTariffList5Result = null;

    /**
     * @param D4WTPTARIFFLIST5RESULT $getTariffList5Result
     */
    public function __construct($getTariffList5Result)
    {
      $this->getTariffList5Result = $getTariffList5Result;
    }

    /**
     * @return D4WTPTARIFFLIST5RESULT
     */
    public function getGetTariffList5Result()
    {
      return $this->getTariffList5Result;
    }

    /**
     * @param D4WTPTARIFFLIST5RESULT $getTariffList5Result
     * @return \Axess\Dci4Wtp\getTariffList5Response
     */
    public function setGetTariffList5Result($getTariffList5Result)
    {
      $this->getTariffList5Result = $getTariffList5Result;
      return $this;
    }

}
