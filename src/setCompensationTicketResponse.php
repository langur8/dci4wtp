<?php

namespace Axess\Dci4Wtp;

class setCompensationTicketResponse
{

    /**
     * @var D4WTPCOMPENSATIONTICKETRES $setCompensationTicketResult
     */
    protected $setCompensationTicketResult = null;

    /**
     * @param D4WTPCOMPENSATIONTICKETRES $setCompensationTicketResult
     */
    public function __construct($setCompensationTicketResult)
    {
      $this->setCompensationTicketResult = $setCompensationTicketResult;
    }

    /**
     * @return D4WTPCOMPENSATIONTICKETRES
     */
    public function getSetCompensationTicketResult()
    {
      return $this->setCompensationTicketResult;
    }

    /**
     * @param D4WTPCOMPENSATIONTICKETRES $setCompensationTicketResult
     * @return \Axess\Dci4Wtp\setCompensationTicketResponse
     */
    public function setSetCompensationTicketResult($setCompensationTicketResult)
    {
      $this->setCompensationTicketResult = $setCompensationTicketResult;
      return $this;
    }

}
