<?php

namespace Axess\Dci4Wtp;

class findShoppingCarts
{

    /**
     * @var D4WTPFINDSHOPCARTSREQUEST $i_ctFindShoppingCartsReq
     */
    protected $i_ctFindShoppingCartsReq = null;

    /**
     * @param D4WTPFINDSHOPCARTSREQUEST $i_ctFindShoppingCartsReq
     */
    public function __construct($i_ctFindShoppingCartsReq)
    {
      $this->i_ctFindShoppingCartsReq = $i_ctFindShoppingCartsReq;
    }

    /**
     * @return D4WTPFINDSHOPCARTSREQUEST
     */
    public function getI_ctFindShoppingCartsReq()
    {
      return $this->i_ctFindShoppingCartsReq;
    }

    /**
     * @param D4WTPFINDSHOPCARTSREQUEST $i_ctFindShoppingCartsReq
     * @return \Axess\Dci4Wtp\findShoppingCarts
     */
    public function setI_ctFindShoppingCartsReq($i_ctFindShoppingCartsReq)
    {
      $this->i_ctFindShoppingCartsReq = $i_ctFindShoppingCartsReq;
      return $this;
    }

}
