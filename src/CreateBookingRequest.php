<?php

namespace Axess\Dci4Wtp;

class CreateBookingRequest
{

    /**
     * @var int $ReservationNo
     */
    protected $ReservationNo = null;

    /**
     * @var ArrayOfCreateReservationTicket $Tickets
     */
    protected $Tickets = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getReservationNo()
    {
      return $this->ReservationNo;
    }

    /**
     * @param int $ReservationNo
     * @return \Axess\Dci4Wtp\CreateBookingRequest
     */
    public function setReservationNo($ReservationNo)
    {
      $this->ReservationNo = $ReservationNo;
      return $this;
    }

    /**
     * @return ArrayOfCreateReservationTicket
     */
    public function getTickets()
    {
      return $this->Tickets;
    }

    /**
     * @param ArrayOfCreateReservationTicket $Tickets
     * @return \Axess\Dci4Wtp\CreateBookingRequest
     */
    public function setTickets($Tickets)
    {
      $this->Tickets = $Tickets;
      return $this;
    }

}
