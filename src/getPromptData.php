<?php

namespace Axess\Dci4Wtp;

class getPromptData
{

    /**
     * @var D4WTPGETPROMPTDATAREQUEST $i_ctGetPromptDataReq
     */
    protected $i_ctGetPromptDataReq = null;

    /**
     * @param D4WTPGETPROMPTDATAREQUEST $i_ctGetPromptDataReq
     */
    public function __construct($i_ctGetPromptDataReq)
    {
      $this->i_ctGetPromptDataReq = $i_ctGetPromptDataReq;
    }

    /**
     * @return D4WTPGETPROMPTDATAREQUEST
     */
    public function getI_ctGetPromptDataReq()
    {
      return $this->i_ctGetPromptDataReq;
    }

    /**
     * @param D4WTPGETPROMPTDATAREQUEST $i_ctGetPromptDataReq
     * @return \Axess\Dci4Wtp\getPromptData
     */
    public function setI_ctGetPromptDataReq($i_ctGetPromptDataReq)
    {
      $this->i_ctGetPromptDataReq = $i_ctGetPromptDataReq;
      return $this;
    }

}
