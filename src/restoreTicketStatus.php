<?php

namespace Axess\Dci4Wtp;

class restoreTicketStatus
{

    /**
     * @var D4WTPRESTORETCKTSTATUSREQ $i_ctRestoreTcktStatusReq
     */
    protected $i_ctRestoreTcktStatusReq = null;

    /**
     * @param D4WTPRESTORETCKTSTATUSREQ $i_ctRestoreTcktStatusReq
     */
    public function __construct($i_ctRestoreTcktStatusReq)
    {
      $this->i_ctRestoreTcktStatusReq = $i_ctRestoreTcktStatusReq;
    }

    /**
     * @return D4WTPRESTORETCKTSTATUSREQ
     */
    public function getI_ctRestoreTcktStatusReq()
    {
      return $this->i_ctRestoreTcktStatusReq;
    }

    /**
     * @param D4WTPRESTORETCKTSTATUSREQ $i_ctRestoreTcktStatusReq
     * @return \Axess\Dci4Wtp\restoreTicketStatus
     */
    public function setI_ctRestoreTcktStatusReq($i_ctRestoreTcktStatusReq)
    {
      $this->i_ctRestoreTcktStatusReq = $i_ctRestoreTcktStatusReq;
      return $this;
    }

}
