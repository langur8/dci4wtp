<?php

namespace Axess\Dci4Wtp;

class OBI4PSPKMWNUTZERADRESSE
{

    /**
     * @var string $SZADRESSZUSATZ
     */
    protected $SZADRESSZUSATZ = null;

    /**
     * @var string $SZLAND
     */
    protected $SZLAND = null;

    /**
     * @var string $SZORT
     */
    protected $SZORT = null;

    /**
     * @var string $SZPOSTLEITZAHL
     */
    protected $SZPOSTLEITZAHL = null;

    /**
     * @var string $SZSTRASSEUNDHAUSNUMMER
     */
    protected $SZSTRASSEUNDHAUSNUMMER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZADRESSZUSATZ()
    {
      return $this->SZADRESSZUSATZ;
    }

    /**
     * @param string $SZADRESSZUSATZ
     * @return \Axess\Dci4Wtp\OBI4PSPKMWNUTZERADRESSE
     */
    public function setSZADRESSZUSATZ($SZADRESSZUSATZ)
    {
      $this->SZADRESSZUSATZ = $SZADRESSZUSATZ;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLAND()
    {
      return $this->SZLAND;
    }

    /**
     * @param string $SZLAND
     * @return \Axess\Dci4Wtp\OBI4PSPKMWNUTZERADRESSE
     */
    public function setSZLAND($SZLAND)
    {
      $this->SZLAND = $SZLAND;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZORT()
    {
      return $this->SZORT;
    }

    /**
     * @param string $SZORT
     * @return \Axess\Dci4Wtp\OBI4PSPKMWNUTZERADRESSE
     */
    public function setSZORT($SZORT)
    {
      $this->SZORT = $SZORT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOSTLEITZAHL()
    {
      return $this->SZPOSTLEITZAHL;
    }

    /**
     * @param string $SZPOSTLEITZAHL
     * @return \Axess\Dci4Wtp\OBI4PSPKMWNUTZERADRESSE
     */
    public function setSZPOSTLEITZAHL($SZPOSTLEITZAHL)
    {
      $this->SZPOSTLEITZAHL = $SZPOSTLEITZAHL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTRASSEUNDHAUSNUMMER()
    {
      return $this->SZSTRASSEUNDHAUSNUMMER;
    }

    /**
     * @param string $SZSTRASSEUNDHAUSNUMMER
     * @return \Axess\Dci4Wtp\OBI4PSPKMWNUTZERADRESSE
     */
    public function setSZSTRASSEUNDHAUSNUMMER($SZSTRASSEUNDHAUSNUMMER)
    {
      $this->SZSTRASSEUNDHAUSNUMMER = $SZSTRASSEUNDHAUSNUMMER;
      return $this;
    }

}
