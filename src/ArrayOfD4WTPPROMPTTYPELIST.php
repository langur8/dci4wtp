<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPROMPTTYPELIST implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPROMPTTYPELIST[] $D4WTPPROMPTTYPELIST
     */
    protected $D4WTPPROMPTTYPELIST = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPROMPTTYPELIST[]
     */
    public function getD4WTPPROMPTTYPELIST()
    {
      return $this->D4WTPPROMPTTYPELIST;
    }

    /**
     * @param D4WTPPROMPTTYPELIST[] $D4WTPPROMPTTYPELIST
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPROMPTTYPELIST
     */
    public function setD4WTPPROMPTTYPELIST(array $D4WTPPROMPTTYPELIST = null)
    {
      $this->D4WTPPROMPTTYPELIST = $D4WTPPROMPTTYPELIST;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPROMPTTYPELIST[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPROMPTTYPELIST
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPROMPTTYPELIST[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPROMPTTYPELIST $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPROMPTTYPELIST[] = $value;
      } else {
        $this->D4WTPPROMPTTYPELIST[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPROMPTTYPELIST[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPROMPTTYPELIST Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPROMPTTYPELIST);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPROMPTTYPELIST);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPROMPTTYPELIST);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPROMPTTYPELIST);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPROMPTTYPELIST Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPROMPTTYPELIST);
    }

}
