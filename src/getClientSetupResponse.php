<?php

namespace Axess\Dci4Wtp;

class getClientSetupResponse
{

    /**
     * @var D4WTPGETCLIENTSETUPRESULT $getClientSetupResult
     */
    protected $getClientSetupResult = null;

    /**
     * @param D4WTPGETCLIENTSETUPRESULT $getClientSetupResult
     */
    public function __construct($getClientSetupResult)
    {
      $this->getClientSetupResult = $getClientSetupResult;
    }

    /**
     * @return D4WTPGETCLIENTSETUPRESULT
     */
    public function getGetClientSetupResult()
    {
      return $this->getClientSetupResult;
    }

    /**
     * @param D4WTPGETCLIENTSETUPRESULT $getClientSetupResult
     * @return \Axess\Dci4Wtp\getClientSetupResponse
     */
    public function setGetClientSetupResult($getClientSetupResult)
    {
      $this->getClientSetupResult = $getClientSetupResult;
      return $this;
    }

}
