<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPADDARTICLEINFO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPADDARTICLEINFO[] $D4WTPADDARTICLEINFO
     */
    protected $D4WTPADDARTICLEINFO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPADDARTICLEINFO[]
     */
    public function getD4WTPADDARTICLEINFO()
    {
      return $this->D4WTPADDARTICLEINFO;
    }

    /**
     * @param D4WTPADDARTICLEINFO[] $D4WTPADDARTICLEINFO
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPADDARTICLEINFO
     */
    public function setD4WTPADDARTICLEINFO(array $D4WTPADDARTICLEINFO = null)
    {
      $this->D4WTPADDARTICLEINFO = $D4WTPADDARTICLEINFO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPADDARTICLEINFO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPADDARTICLEINFO
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPADDARTICLEINFO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPADDARTICLEINFO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPADDARTICLEINFO[] = $value;
      } else {
        $this->D4WTPADDARTICLEINFO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPADDARTICLEINFO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPADDARTICLEINFO Return the current element
     */
    public function current()
    {
      return current($this->D4WTPADDARTICLEINFO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPADDARTICLEINFO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPADDARTICLEINFO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPADDARTICLEINFO);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPADDARTICLEINFO Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPADDARTICLEINFO);
    }

}
