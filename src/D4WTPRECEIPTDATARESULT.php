<?php

namespace Axess\Dci4Wtp;

class D4WTPRECEIPTDATARESULT
{

    /**
     * @var D4WTPRECEIPTDATA $CTRECEIPTDATA
     */
    protected $CTRECEIPTDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPRECEIPTDATA
     */
    public function getCTRECEIPTDATA()
    {
      return $this->CTRECEIPTDATA;
    }

    /**
     * @param D4WTPRECEIPTDATA $CTRECEIPTDATA
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATARESULT
     */
    public function setCTRECEIPTDATA($CTRECEIPTDATA)
    {
      $this->CTRECEIPTDATA = $CTRECEIPTDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATARESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATARESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
