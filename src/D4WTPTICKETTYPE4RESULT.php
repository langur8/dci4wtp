<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETTYPE4RESULT
{

    /**
     * @var ArrayOfD4WTPTICKETTYPE4 $ACTTICKETTYPES4
     */
    protected $ACTTICKETTYPES4 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKETTYPE4
     */
    public function getACTTICKETTYPES4()
    {
      return $this->ACTTICKETTYPES4;
    }

    /**
     * @param ArrayOfD4WTPTICKETTYPE4 $ACTTICKETTYPES4
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4RESULT
     */
    public function setACTTICKETTYPES4($ACTTICKETTYPES4)
    {
      $this->ACTTICKETTYPES4 = $ACTTICKETTYPES4;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
