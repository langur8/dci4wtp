<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSUBCONTINGENT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSUBCONTINGENT[] $D4WTPSUBCONTINGENT
     */
    protected $D4WTPSUBCONTINGENT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSUBCONTINGENT[]
     */
    public function getD4WTPSUBCONTINGENT()
    {
      return $this->D4WTPSUBCONTINGENT;
    }

    /**
     * @param D4WTPSUBCONTINGENT[] $D4WTPSUBCONTINGENT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSUBCONTINGENT
     */
    public function setD4WTPSUBCONTINGENT(array $D4WTPSUBCONTINGENT = null)
    {
      $this->D4WTPSUBCONTINGENT = $D4WTPSUBCONTINGENT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSUBCONTINGENT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSUBCONTINGENT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSUBCONTINGENT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSUBCONTINGENT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSUBCONTINGENT[] = $value;
      } else {
        $this->D4WTPSUBCONTINGENT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSUBCONTINGENT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSUBCONTINGENT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSUBCONTINGENT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSUBCONTINGENT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSUBCONTINGENT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSUBCONTINGENT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSUBCONTINGENT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSUBCONTINGENT);
    }

}
