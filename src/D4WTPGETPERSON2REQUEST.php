<?php

namespace Axess\Dci4Wtp;

class D4WTPGETPERSON2REQUEST
{

    /**
     * @var D4WTPERSONDATA $CTPERSONDATA
     */
    protected $CTPERSONDATA = null;

    /**
     * @var float $NFIRSTNROWS
     */
    protected $NFIRSTNROWS = null;

    /**
     * @var float $NMAXROW2FETCH
     */
    protected $NMAXROW2FETCH = null;

    /**
     * @var float $NMINROW2FETCH
     */
    protected $NMINROW2FETCH = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZSORTORDER
     */
    protected $SZSORTORDER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPERSONDATA
     */
    public function getCTPERSONDATA()
    {
      return $this->CTPERSONDATA;
    }

    /**
     * @param D4WTPERSONDATA $CTPERSONDATA
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON2REQUEST
     */
    public function setCTPERSONDATA($CTPERSONDATA)
    {
      $this->CTPERSONDATA = $CTPERSONDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRSTNROWS()
    {
      return $this->NFIRSTNROWS;
    }

    /**
     * @param float $NFIRSTNROWS
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON2REQUEST
     */
    public function setNFIRSTNROWS($NFIRSTNROWS)
    {
      $this->NFIRSTNROWS = $NFIRSTNROWS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMAXROW2FETCH()
    {
      return $this->NMAXROW2FETCH;
    }

    /**
     * @param float $NMAXROW2FETCH
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON2REQUEST
     */
    public function setNMAXROW2FETCH($NMAXROW2FETCH)
    {
      $this->NMAXROW2FETCH = $NMAXROW2FETCH;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMINROW2FETCH()
    {
      return $this->NMINROW2FETCH;
    }

    /**
     * @param float $NMINROW2FETCH
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON2REQUEST
     */
    public function setNMINROW2FETCH($NMINROW2FETCH)
    {
      $this->NMINROW2FETCH = $NMINROW2FETCH;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON2REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSORTORDER()
    {
      return $this->SZSORTORDER;
    }

    /**
     * @param string $SZSORTORDER
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON2REQUEST
     */
    public function setSZSORTORDER($SZSORTORDER)
    {
      $this->SZSORTORDER = $SZSORTORDER;
      return $this;
    }

}
