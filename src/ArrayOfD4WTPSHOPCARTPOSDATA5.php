<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSHOPCARTPOSDATA5 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSHOPCARTPOSDATA5[] $D4WTPSHOPCARTPOSDATA5
     */
    protected $D4WTPSHOPCARTPOSDATA5 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSHOPCARTPOSDATA5[]
     */
    public function getD4WTPSHOPCARTPOSDATA5()
    {
      return $this->D4WTPSHOPCARTPOSDATA5;
    }

    /**
     * @param D4WTPSHOPCARTPOSDATA5[] $D4WTPSHOPCARTPOSDATA5
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSHOPCARTPOSDATA5
     */
    public function setD4WTPSHOPCARTPOSDATA5(array $D4WTPSHOPCARTPOSDATA5 = null)
    {
      $this->D4WTPSHOPCARTPOSDATA5 = $D4WTPSHOPCARTPOSDATA5;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSHOPCARTPOSDATA5[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSHOPCARTPOSDATA5
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSHOPCARTPOSDATA5[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSHOPCARTPOSDATA5 $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSHOPCARTPOSDATA5[] = $value;
      } else {
        $this->D4WTPSHOPCARTPOSDATA5[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSHOPCARTPOSDATA5[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSHOPCARTPOSDATA5 Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSHOPCARTPOSDATA5);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSHOPCARTPOSDATA5);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSHOPCARTPOSDATA5);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSHOPCARTPOSDATA5);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSHOPCARTPOSDATA5 Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSHOPCARTPOSDATA5);
    }

}
