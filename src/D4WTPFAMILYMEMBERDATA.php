<?php

namespace Axess\Dci4Wtp;

class D4WTPFAMILYMEMBERDATA
{

    /**
     * @var ArrayOfD4WTPMEMBERTYPE $ACTMEMBERTYPE
     */
    protected $ACTMEMBERTYPE = null;

    /**
     * @var float $BLOCKAKTIV
     */
    protected $BLOCKAKTIV = null;

    /**
     * @var float $BMEMBERSHIPACTIVE
     */
    protected $BMEMBERSHIPACTIVE = null;

    /**
     * @var float $NFAMPERSNO
     */
    protected $NFAMPERSNO = null;

    /**
     * @var float $NFAMPOSNO
     */
    protected $NFAMPOSNO = null;

    /**
     * @var float $NFAMPROJNO
     */
    protected $NFAMPROJNO = null;

    /**
     * @var float $NFAMSORTNO
     */
    protected $NFAMSORTNO = null;

    /**
     * @var float $NLFDCOUNTER
     */
    protected $NLFDCOUNTER = null;

    /**
     * @var float $NLFDLOCKNO
     */
    protected $NLFDLOCKNO = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NPERSTYPENO
     */
    protected $NPERSTYPENO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var float $NUNICODENO
     */
    protected $NUNICODENO = null;

    /**
     * @var string $SZDATEOFBIRTH
     */
    protected $SZDATEOFBIRTH = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZGENDER
     */
    protected $SZGENDER = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZLOCKVALIDTO
     */
    protected $SZLOCKVALIDTO = null;

    /**
     * @var string $SZMEMACTIVATIONDATE
     */
    protected $SZMEMACTIVATIONDATE = null;

    /**
     * @var string $SZMOBILE
     */
    protected $SZMOBILE = null;

    /**
     * @var string $SZPERSTYPENAME
     */
    protected $SZPERSTYPENAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZSALUTATION
     */
    protected $SZSALUTATION = null;

    /**
     * @var string $SZTICKETTYPENAME
     */
    protected $SZTICKETTYPENAME = null;

    /**
     * @var string $SZTIMESCALENAME
     */
    protected $SZTIMESCALENAME = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPMEMBERTYPE
     */
    public function getACTMEMBERTYPE()
    {
      return $this->ACTMEMBERTYPE;
    }

    /**
     * @param ArrayOfD4WTPMEMBERTYPE $ACTMEMBERTYPE
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setACTMEMBERTYPE($ACTMEMBERTYPE)
    {
      $this->ACTMEMBERTYPE = $ACTMEMBERTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBLOCKAKTIV()
    {
      return $this->BLOCKAKTIV;
    }

    /**
     * @param float $BLOCKAKTIV
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setBLOCKAKTIV($BLOCKAKTIV)
    {
      $this->BLOCKAKTIV = $BLOCKAKTIV;
      return $this;
    }

    /**
     * @return float
     */
    public function getBMEMBERSHIPACTIVE()
    {
      return $this->BMEMBERSHIPACTIVE;
    }

    /**
     * @param float $BMEMBERSHIPACTIVE
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setBMEMBERSHIPACTIVE($BMEMBERSHIPACTIVE)
    {
      $this->BMEMBERSHIPACTIVE = $BMEMBERSHIPACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMPERSNO()
    {
      return $this->NFAMPERSNO;
    }

    /**
     * @param float $NFAMPERSNO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNFAMPERSNO($NFAMPERSNO)
    {
      $this->NFAMPERSNO = $NFAMPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMPOSNO()
    {
      return $this->NFAMPOSNO;
    }

    /**
     * @param float $NFAMPOSNO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNFAMPOSNO($NFAMPOSNO)
    {
      $this->NFAMPOSNO = $NFAMPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMPROJNO()
    {
      return $this->NFAMPROJNO;
    }

    /**
     * @param float $NFAMPROJNO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNFAMPROJNO($NFAMPROJNO)
    {
      $this->NFAMPROJNO = $NFAMPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMSORTNO()
    {
      return $this->NFAMSORTNO;
    }

    /**
     * @param float $NFAMSORTNO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNFAMSORTNO($NFAMSORTNO)
    {
      $this->NFAMSORTNO = $NFAMSORTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDCOUNTER()
    {
      return $this->NLFDCOUNTER;
    }

    /**
     * @param float $NLFDCOUNTER
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNLFDCOUNTER($NLFDCOUNTER)
    {
      $this->NLFDCOUNTER = $NLFDCOUNTER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDLOCKNO()
    {
      return $this->NLFDLOCKNO;
    }

    /**
     * @param float $NLFDLOCKNO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNLFDLOCKNO($NLFDLOCKNO)
    {
      $this->NLFDLOCKNO = $NLFDLOCKNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSTYPENO()
    {
      return $this->NPERSTYPENO;
    }

    /**
     * @param float $NPERSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNPERSTYPENO($NPERSTYPENO)
    {
      $this->NPERSTYPENO = $NPERSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENO()
    {
      return $this->NUNICODENO;
    }

    /**
     * @param float $NUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setNUNICODENO($NUNICODENO)
    {
      $this->NUNICODENO = $NUNICODENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEOFBIRTH()
    {
      return $this->SZDATEOFBIRTH;
    }

    /**
     * @param string $SZDATEOFBIRTH
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZDATEOFBIRTH($SZDATEOFBIRTH)
    {
      $this->SZDATEOFBIRTH = $SZDATEOFBIRTH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGENDER()
    {
      return $this->SZGENDER;
    }

    /**
     * @param string $SZGENDER
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZGENDER($SZGENDER)
    {
      $this->SZGENDER = $SZGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOCKVALIDTO()
    {
      return $this->SZLOCKVALIDTO;
    }

    /**
     * @param string $SZLOCKVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZLOCKVALIDTO($SZLOCKVALIDTO)
    {
      $this->SZLOCKVALIDTO = $SZLOCKVALIDTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEMACTIVATIONDATE()
    {
      return $this->SZMEMACTIVATIONDATE;
    }

    /**
     * @param string $SZMEMACTIVATIONDATE
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZMEMACTIVATIONDATE($SZMEMACTIVATIONDATE)
    {
      $this->SZMEMACTIVATIONDATE = $SZMEMACTIVATIONDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMOBILE()
    {
      return $this->SZMOBILE;
    }

    /**
     * @param string $SZMOBILE
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZMOBILE($SZMOBILE)
    {
      $this->SZMOBILE = $SZMOBILE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSTYPENAME()
    {
      return $this->SZPERSTYPENAME;
    }

    /**
     * @param string $SZPERSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZPERSTYPENAME($SZPERSTYPENAME)
    {
      $this->SZPERSTYPENAME = $SZPERSTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSALUTATION()
    {
      return $this->SZSALUTATION;
    }

    /**
     * @param string $SZSALUTATION
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZSALUTATION($SZSALUTATION)
    {
      $this->SZSALUTATION = $SZSALUTATION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPENAME()
    {
      return $this->SZTICKETTYPENAME;
    }

    /**
     * @param string $SZTICKETTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZTICKETTYPENAME($SZTICKETTYPENAME)
    {
      $this->SZTICKETTYPENAME = $SZTICKETTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTIMESCALENAME()
    {
      return $this->SZTIMESCALENAME;
    }

    /**
     * @param string $SZTIMESCALENAME
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZTIMESCALENAME($SZTIMESCALENAME)
    {
      $this->SZTIMESCALENAME = $SZTIMESCALENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPFAMILYMEMBERDATA
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
