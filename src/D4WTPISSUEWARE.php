<?php

namespace Axess\Dci4Wtp;

class D4WTPISSUEWARE
{

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NSORTID
     */
    protected $NSORTID = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NWARESALEITEMNR
     */
    protected $NWARESALEITEMNR = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPISSUEWARE
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTID()
    {
      return $this->NSORTID;
    }

    /**
     * @param float $NSORTID
     * @return \Axess\Dci4Wtp\D4WTPISSUEWARE
     */
    public function setNSORTID($NSORTID)
    {
      $this->NSORTID = $NSORTID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPISSUEWARE
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARESALEITEMNR()
    {
      return $this->NWARESALEITEMNR;
    }

    /**
     * @param float $NWARESALEITEMNR
     * @return \Axess\Dci4Wtp\D4WTPISSUEWARE
     */
    public function setNWARESALEITEMNR($NWARESALEITEMNR)
    {
      $this->NWARESALEITEMNR = $NWARESALEITEMNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPISSUEWARE
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
