<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPBLANKTYPEEN implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPBLANKTYPEEN[] $D4WTPBLANKTYPEEN
     */
    protected $D4WTPBLANKTYPEEN = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPBLANKTYPEEN[]
     */
    public function getD4WTPBLANKTYPEEN()
    {
      return $this->D4WTPBLANKTYPEEN;
    }

    /**
     * @param D4WTPBLANKTYPEEN[] $D4WTPBLANKTYPEEN
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPBLANKTYPEEN
     */
    public function setD4WTPBLANKTYPEEN(array $D4WTPBLANKTYPEEN = null)
    {
      $this->D4WTPBLANKTYPEEN = $D4WTPBLANKTYPEEN;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPBLANKTYPEEN[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPBLANKTYPEEN
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPBLANKTYPEEN[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPBLANKTYPEEN $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPBLANKTYPEEN[] = $value;
      } else {
        $this->D4WTPBLANKTYPEEN[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPBLANKTYPEEN[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPBLANKTYPEEN Return the current element
     */
    public function current()
    {
      return current($this->D4WTPBLANKTYPEEN);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPBLANKTYPEEN);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPBLANKTYPEEN);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPBLANKTYPEEN);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPBLANKTYPEEN Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPBLANKTYPEEN);
    }

}
