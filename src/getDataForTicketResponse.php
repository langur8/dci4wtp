<?php

namespace Axess\Dci4Wtp;

class getDataForTicketResponse
{

    /**
     * @var WTPTicketDataResult $getDataForTicketResult
     */
    protected $getDataForTicketResult = null;

    /**
     * @param WTPTicketDataResult $getDataForTicketResult
     */
    public function __construct($getDataForTicketResult)
    {
      $this->getDataForTicketResult = $getDataForTicketResult;
    }

    /**
     * @return WTPTicketDataResult
     */
    public function getGetDataForTicketResult()
    {
      return $this->getDataForTicketResult;
    }

    /**
     * @param WTPTicketDataResult $getDataForTicketResult
     * @return \Axess\Dci4Wtp\getDataForTicketResponse
     */
    public function setGetDataForTicketResult($getDataForTicketResult)
    {
      $this->getDataForTicketResult = $getDataForTicketResult;
      return $this;
    }

}
