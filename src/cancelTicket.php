<?php

namespace Axess\Dci4Wtp;

class cancelTicket
{

    /**
     * @var D4WTPCANCELTICKETREQUEST $i_ctCancelTicketReq
     */
    protected $i_ctCancelTicketReq = null;

    /**
     * @param D4WTPCANCELTICKETREQUEST $i_ctCancelTicketReq
     */
    public function __construct($i_ctCancelTicketReq)
    {
      $this->i_ctCancelTicketReq = $i_ctCancelTicketReq;
    }

    /**
     * @return D4WTPCANCELTICKETREQUEST
     */
    public function getI_ctCancelTicketReq()
    {
      return $this->i_ctCancelTicketReq;
    }

    /**
     * @param D4WTPCANCELTICKETREQUEST $i_ctCancelTicketReq
     * @return \Axess\Dci4Wtp\cancelTicket
     */
    public function setI_ctCancelTicketReq($i_ctCancelTicketReq)
    {
      $this->i_ctCancelTicketReq = $i_ctCancelTicketReq;
      return $this;
    }

}
