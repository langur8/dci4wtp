<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPMEMBERTYPE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPMEMBERTYPE[] $D4WTPMEMBERTYPE
     */
    protected $D4WTPMEMBERTYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPMEMBERTYPE[]
     */
    public function getD4WTPMEMBERTYPE()
    {
      return $this->D4WTPMEMBERTYPE;
    }

    /**
     * @param D4WTPMEMBERTYPE[] $D4WTPMEMBERTYPE
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPMEMBERTYPE
     */
    public function setD4WTPMEMBERTYPE(array $D4WTPMEMBERTYPE = null)
    {
      $this->D4WTPMEMBERTYPE = $D4WTPMEMBERTYPE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPMEMBERTYPE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPMEMBERTYPE
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPMEMBERTYPE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPMEMBERTYPE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPMEMBERTYPE[] = $value;
      } else {
        $this->D4WTPMEMBERTYPE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPMEMBERTYPE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPMEMBERTYPE Return the current element
     */
    public function current()
    {
      return current($this->D4WTPMEMBERTYPE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPMEMBERTYPE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPMEMBERTYPE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPMEMBERTYPE);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPMEMBERTYPE Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPMEMBERTYPE);
    }

}
