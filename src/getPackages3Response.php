<?php

namespace Axess\Dci4Wtp;

class getPackages3Response
{

    /**
     * @var D4WTPPACKAGE3RESULT $getPackages3Result
     */
    protected $getPackages3Result = null;

    /**
     * @param D4WTPPACKAGE3RESULT $getPackages3Result
     */
    public function __construct($getPackages3Result)
    {
      $this->getPackages3Result = $getPackages3Result;
    }

    /**
     * @return D4WTPPACKAGE3RESULT
     */
    public function getGetPackages3Result()
    {
      return $this->getPackages3Result;
    }

    /**
     * @param D4WTPPACKAGE3RESULT $getPackages3Result
     * @return \Axess\Dci4Wtp\getPackages3Response
     */
    public function setGetPackages3Result($getPackages3Result)
    {
      $this->getPackages3Result = $getPackages3Result;
      return $this;
    }

}
