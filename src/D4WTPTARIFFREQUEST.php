<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFREQUEST
{

    /**
     * @var D4WTPPRODUCT $CTPRODUCT
     */
    protected $CTPRODUCT = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPRODUCT
     */
    public function getCTPRODUCT()
    {
      return $this->CTPRODUCT;
    }

    /**
     * @param D4WTPPRODUCT $CTPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPTARIFFREQUEST
     */
    public function setCTPRODUCT($CTPRODUCT)
    {
      $this->CTPRODUCT = $CTPRODUCT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPTARIFFREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFREQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

}
