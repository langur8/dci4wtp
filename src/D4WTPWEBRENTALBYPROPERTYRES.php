<?php

namespace Axess\Dci4Wtp;

class D4WTPWEBRENTALBYPROPERTYRES
{

    /**
     * @var float $NARCNR
     */
    protected $NARCNR = null;

    /**
     * @var float $NAVAILABLEITEMS
     */
    protected $NAVAILABLEITEMS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NRENTALITEMNR
     */
    protected $NRENTALITEMNR = null;

    /**
     * @var ArrayOfDCI4WTPGETWEBRENTALBYPRO $SKI_PROPERTY
     */
    protected $SKI_PROPERTY = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZRENTALEND
     */
    protected $SZRENTALEND = null;

    /**
     * @var string $SZRENTALSTART
     */
    protected $SZRENTALSTART = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNARCNR()
    {
      return $this->NARCNR;
    }

    /**
     * @param float $NARCNR
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYRES
     */
    public function setNARCNR($NARCNR)
    {
      $this->NARCNR = $NARCNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAVAILABLEITEMS()
    {
      return $this->NAVAILABLEITEMS;
    }

    /**
     * @param float $NAVAILABLEITEMS
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYRES
     */
    public function setNAVAILABLEITEMS($NAVAILABLEITEMS)
    {
      $this->NAVAILABLEITEMS = $NAVAILABLEITEMS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYRES
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNR()
    {
      return $this->NRENTALITEMNR;
    }

    /**
     * @param float $NRENTALITEMNR
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYRES
     */
    public function setNRENTALITEMNR($NRENTALITEMNR)
    {
      $this->NRENTALITEMNR = $NRENTALITEMNR;
      return $this;
    }

    /**
     * @return ArrayOfDCI4WTPGETWEBRENTALBYPRO
     */
    public function getSKI_PROPERTY()
    {
      return $this->SKI_PROPERTY;
    }

    /**
     * @param ArrayOfDCI4WTPGETWEBRENTALBYPRO $SKI_PROPERTY
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYRES
     */
    public function setSKI_PROPERTY($SKI_PROPERTY)
    {
      $this->SKI_PROPERTY = $SKI_PROPERTY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRENTALEND()
    {
      return $this->SZRENTALEND;
    }

    /**
     * @param string $SZRENTALEND
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYRES
     */
    public function setSZRENTALEND($SZRENTALEND)
    {
      $this->SZRENTALEND = $SZRENTALEND;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRENTALSTART()
    {
      return $this->SZRENTALSTART;
    }

    /**
     * @param string $SZRENTALSTART
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYRES
     */
    public function setSZRENTALSTART($SZRENTALSTART)
    {
      $this->SZRENTALSTART = $SZRENTALSTART;
      return $this;
    }

}
