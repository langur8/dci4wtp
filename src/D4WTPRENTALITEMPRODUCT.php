<?php

namespace Axess\Dci4Wtp;

class D4WTPRENTALITEMPRODUCT
{

    /**
     * @var float $NPACKNR
     */
    protected $NPACKNR = null;

    /**
     * @var float $NPACKPOSNR
     */
    protected $NPACKPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NRENTALITEMNR
     */
    protected $NRENTALITEMNR = null;

    /**
     * @var float $NRENTALPERSTYPENR
     */
    protected $NRENTALPERSTYPENR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPACKNR()
    {
      return $this->NPACKNR;
    }

    /**
     * @param float $NPACKNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT
     */
    public function setNPACKNR($NPACKNR)
    {
      $this->NPACKNR = $NPACKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKPOSNR()
    {
      return $this->NPACKPOSNR;
    }

    /**
     * @param float $NPACKPOSNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT
     */
    public function setNPACKPOSNR($NPACKPOSNR)
    {
      $this->NPACKPOSNR = $NPACKPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNR()
    {
      return $this->NRENTALITEMNR;
    }

    /**
     * @param float $NRENTALITEMNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT
     */
    public function setNRENTALITEMNR($NRENTALITEMNR)
    {
      $this->NRENTALITEMNR = $NRENTALITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPERSTYPENR()
    {
      return $this->NRENTALPERSTYPENR;
    }

    /**
     * @param float $NRENTALPERSTYPENR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMPRODUCT
     */
    public function setNRENTALPERSTYPENR($NRENTALPERSTYPENR)
    {
      $this->NRENTALPERSTYPENR = $NRENTALPERSTYPENR;
      return $this;
    }

}
