<?php

namespace Axess\Dci4Wtp;

class D4WTPCURRENCYREC
{

    /**
     * @var float $BFOREIGNCURR1
     */
    protected $BFOREIGNCURR1 = null;

    /**
     * @var float $BFOREIGNCURR2
     */
    protected $BFOREIGNCURR2 = null;

    /**
     * @var float $BINVOICEFOREIGNCURR1
     */
    protected $BINVOICEFOREIGNCURR1 = null;

    /**
     * @var float $BINVOICEFOREIGNCURR2
     */
    protected $BINVOICEFOREIGNCURR2 = null;

    /**
     * @var float $BISFIRSTOWNCURRENCY
     */
    protected $BISFIRSTOWNCURRENCY = null;

    /**
     * @var float $BOWNCURRENCY
     */
    protected $BOWNCURRENCY = null;

    /**
     * @var float $BROUNTTO5
     */
    protected $BROUNTTO5 = null;

    /**
     * @var float $FCONVERSATIONFACTOR
     */
    protected $FCONVERSATIONFACTOR = null;

    /**
     * @var float $NCURRENCYGROUPNO
     */
    protected $NCURRENCYGROUPNO = null;

    /**
     * @var float $NCURRENCYNO
     */
    protected $NCURRENCYNO = null;

    /**
     * @var float $NCURRENCYSECTIONNO
     */
    protected $NCURRENCYSECTIONNO = null;

    /**
     * @var float $NCURRENCYSECTIONNOALT
     */
    protected $NCURRENCYSECTIONNOALT = null;

    /**
     * @var float $NOWNCURRENCYNOALT
     */
    protected $NOWNCURRENCYNOALT = null;

    /**
     * @var float $NROUNDIGSTYPE
     */
    protected $NROUNDIGSTYPE = null;

    /**
     * @var float $NROUNDTO
     */
    protected $NROUNDTO = null;

    /**
     * @var string $SZCURRENCYNAME
     */
    protected $SZCURRENCYNAME = null;

    /**
     * @var string $SZCURRENCYSHORTNAME
     */
    protected $SZCURRENCYSHORTNAME = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZISEURO
     */
    protected $SZISEURO = null;

    /**
     * @var string $SZROUNDINGSTYPENAME
     */
    protected $SZROUNDINGSTYPENAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBFOREIGNCURR1()
    {
      return $this->BFOREIGNCURR1;
    }

    /**
     * @param float $BFOREIGNCURR1
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setBFOREIGNCURR1($BFOREIGNCURR1)
    {
      $this->BFOREIGNCURR1 = $BFOREIGNCURR1;
      return $this;
    }

    /**
     * @return float
     */
    public function getBFOREIGNCURR2()
    {
      return $this->BFOREIGNCURR2;
    }

    /**
     * @param float $BFOREIGNCURR2
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setBFOREIGNCURR2($BFOREIGNCURR2)
    {
      $this->BFOREIGNCURR2 = $BFOREIGNCURR2;
      return $this;
    }

    /**
     * @return float
     */
    public function getBINVOICEFOREIGNCURR1()
    {
      return $this->BINVOICEFOREIGNCURR1;
    }

    /**
     * @param float $BINVOICEFOREIGNCURR1
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setBINVOICEFOREIGNCURR1($BINVOICEFOREIGNCURR1)
    {
      $this->BINVOICEFOREIGNCURR1 = $BINVOICEFOREIGNCURR1;
      return $this;
    }

    /**
     * @return float
     */
    public function getBINVOICEFOREIGNCURR2()
    {
      return $this->BINVOICEFOREIGNCURR2;
    }

    /**
     * @param float $BINVOICEFOREIGNCURR2
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setBINVOICEFOREIGNCURR2($BINVOICEFOREIGNCURR2)
    {
      $this->BINVOICEFOREIGNCURR2 = $BINVOICEFOREIGNCURR2;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISFIRSTOWNCURRENCY()
    {
      return $this->BISFIRSTOWNCURRENCY;
    }

    /**
     * @param float $BISFIRSTOWNCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setBISFIRSTOWNCURRENCY($BISFIRSTOWNCURRENCY)
    {
      $this->BISFIRSTOWNCURRENCY = $BISFIRSTOWNCURRENCY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBOWNCURRENCY()
    {
      return $this->BOWNCURRENCY;
    }

    /**
     * @param float $BOWNCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setBOWNCURRENCY($BOWNCURRENCY)
    {
      $this->BOWNCURRENCY = $BOWNCURRENCY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBROUNTTO5()
    {
      return $this->BROUNTTO5;
    }

    /**
     * @param float $BROUNTTO5
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setBROUNTTO5($BROUNTTO5)
    {
      $this->BROUNTTO5 = $BROUNTTO5;
      return $this;
    }

    /**
     * @return float
     */
    public function getFCONVERSATIONFACTOR()
    {
      return $this->FCONVERSATIONFACTOR;
    }

    /**
     * @param float $FCONVERSATIONFACTOR
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setFCONVERSATIONFACTOR($FCONVERSATIONFACTOR)
    {
      $this->FCONVERSATIONFACTOR = $FCONVERSATIONFACTOR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCURRENCYGROUPNO()
    {
      return $this->NCURRENCYGROUPNO;
    }

    /**
     * @param float $NCURRENCYGROUPNO
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setNCURRENCYGROUPNO($NCURRENCYGROUPNO)
    {
      $this->NCURRENCYGROUPNO = $NCURRENCYGROUPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCURRENCYNO()
    {
      return $this->NCURRENCYNO;
    }

    /**
     * @param float $NCURRENCYNO
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setNCURRENCYNO($NCURRENCYNO)
    {
      $this->NCURRENCYNO = $NCURRENCYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCURRENCYSECTIONNO()
    {
      return $this->NCURRENCYSECTIONNO;
    }

    /**
     * @param float $NCURRENCYSECTIONNO
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setNCURRENCYSECTIONNO($NCURRENCYSECTIONNO)
    {
      $this->NCURRENCYSECTIONNO = $NCURRENCYSECTIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCURRENCYSECTIONNOALT()
    {
      return $this->NCURRENCYSECTIONNOALT;
    }

    /**
     * @param float $NCURRENCYSECTIONNOALT
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setNCURRENCYSECTIONNOALT($NCURRENCYSECTIONNOALT)
    {
      $this->NCURRENCYSECTIONNOALT = $NCURRENCYSECTIONNOALT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNOWNCURRENCYNOALT()
    {
      return $this->NOWNCURRENCYNOALT;
    }

    /**
     * @param float $NOWNCURRENCYNOALT
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setNOWNCURRENCYNOALT($NOWNCURRENCYNOALT)
    {
      $this->NOWNCURRENCYNOALT = $NOWNCURRENCYNOALT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNROUNDIGSTYPE()
    {
      return $this->NROUNDIGSTYPE;
    }

    /**
     * @param float $NROUNDIGSTYPE
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setNROUNDIGSTYPE($NROUNDIGSTYPE)
    {
      $this->NROUNDIGSTYPE = $NROUNDIGSTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNROUNDTO()
    {
      return $this->NROUNDTO;
    }

    /**
     * @param float $NROUNDTO
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setNROUNDTO($NROUNDTO)
    {
      $this->NROUNDTO = $NROUNDTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCYNAME()
    {
      return $this->SZCURRENCYNAME;
    }

    /**
     * @param string $SZCURRENCYNAME
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setSZCURRENCYNAME($SZCURRENCYNAME)
    {
      $this->SZCURRENCYNAME = $SZCURRENCYNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCYSHORTNAME()
    {
      return $this->SZCURRENCYSHORTNAME;
    }

    /**
     * @param string $SZCURRENCYSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setSZCURRENCYSHORTNAME($SZCURRENCYSHORTNAME)
    {
      $this->SZCURRENCYSHORTNAME = $SZCURRENCYSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZISEURO()
    {
      return $this->SZISEURO;
    }

    /**
     * @param string $SZISEURO
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setSZISEURO($SZISEURO)
    {
      $this->SZISEURO = $SZISEURO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZROUNDINGSTYPENAME()
    {
      return $this->SZROUNDINGSTYPENAME;
    }

    /**
     * @param string $SZROUNDINGSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPCURRENCYREC
     */
    public function setSZROUNDINGSTYPENAME($SZROUNDINGSTYPENAME)
    {
      $this->SZROUNDINGSTYPENAME = $SZROUNDINGSTYPENAME;
      return $this;
    }

}
