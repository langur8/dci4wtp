<?php

namespace Axess\Dci4Wtp;

class manageShoppingCart3
{

    /**
     * @var D4WTPMANAGESHOPCARTREQ3 $i_ctManageShoppingCartReq
     */
    protected $i_ctManageShoppingCartReq = null;

    /**
     * @param D4WTPMANAGESHOPCARTREQ3 $i_ctManageShoppingCartReq
     */
    public function __construct($i_ctManageShoppingCartReq)
    {
      $this->i_ctManageShoppingCartReq = $i_ctManageShoppingCartReq;
    }

    /**
     * @return D4WTPMANAGESHOPCARTREQ3
     */
    public function getI_ctManageShoppingCartReq()
    {
      return $this->i_ctManageShoppingCartReq;
    }

    /**
     * @param D4WTPMANAGESHOPCARTREQ3 $i_ctManageShoppingCartReq
     * @return \Axess\Dci4Wtp\manageShoppingCart3
     */
    public function setI_ctManageShoppingCartReq($i_ctManageShoppingCartReq)
    {
      $this->i_ctManageShoppingCartReq = $i_ctManageShoppingCartReq;
      return $this;
    }

}
