<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPRODUCTTYPES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPRODUCTTYPES[] $D4WTPPRODUCTTYPES
     */
    protected $D4WTPPRODUCTTYPES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPRODUCTTYPES[]
     */
    public function getD4WTPPRODUCTTYPES()
    {
      return $this->D4WTPPRODUCTTYPES;
    }

    /**
     * @param D4WTPPRODUCTTYPES[] $D4WTPPRODUCTTYPES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPRODUCTTYPES
     */
    public function setD4WTPPRODUCTTYPES(array $D4WTPPRODUCTTYPES = null)
    {
      $this->D4WTPPRODUCTTYPES = $D4WTPPRODUCTTYPES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPRODUCTTYPES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPRODUCTTYPES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPRODUCTTYPES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPRODUCTTYPES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPRODUCTTYPES[] = $value;
      } else {
        $this->D4WTPPRODUCTTYPES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPRODUCTTYPES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPRODUCTTYPES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPRODUCTTYPES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPRODUCTTYPES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPRODUCTTYPES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPRODUCTTYPES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPRODUCTTYPES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPRODUCTTYPES);
    }

}
