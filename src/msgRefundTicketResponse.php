<?php

namespace Axess\Dci4Wtp;

class msgRefundTicketResponse
{

    /**
     * @var D4WTPRESULT $msgRefundTicketResult
     */
    protected $msgRefundTicketResult = null;

    /**
     * @param D4WTPRESULT $msgRefundTicketResult
     */
    public function __construct($msgRefundTicketResult)
    {
      $this->msgRefundTicketResult = $msgRefundTicketResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getMsgRefundTicketResult()
    {
      return $this->msgRefundTicketResult;
    }

    /**
     * @param D4WTPRESULT $msgRefundTicketResult
     * @return \Axess\Dci4Wtp\msgRefundTicketResponse
     */
    public function setMsgRefundTicketResult($msgRefundTicketResult)
    {
      $this->msgRefundTicketResult = $msgRefundTicketResult;
      return $this;
    }

}
