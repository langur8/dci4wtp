<?php

namespace Axess\Dci4Wtp;

class D4WTPISSUETICKET3REQUEST
{

    /**
     * @var ArrayOfD4WTPPRODUCTORDER3 $ACTPRODUCTORDER
     */
    protected $ACTPRODUCTORDER = null;

    /**
     * @var D4WTPCREDITCARDDATA $CTCCDATA
     */
    protected $CTCCDATA = null;

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZDRIVERTYPE
     */
    protected $SZDRIVERTYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPRODUCTORDER3
     */
    public function getACTPRODUCTORDER()
    {
      return $this->ACTPRODUCTORDER;
    }

    /**
     * @param ArrayOfD4WTPPRODUCTORDER3 $ACTPRODUCTORDER
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET3REQUEST
     */
    public function setACTPRODUCTORDER($ACTPRODUCTORDER)
    {
      $this->ACTPRODUCTORDER = $ACTPRODUCTORDER;
      return $this;
    }

    /**
     * @return D4WTPCREDITCARDDATA
     */
    public function getCTCCDATA()
    {
      return $this->CTCCDATA;
    }

    /**
     * @param D4WTPCREDITCARDDATA $CTCCDATA
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET3REQUEST
     */
    public function setCTCCDATA($CTCCDATA)
    {
      $this->CTCCDATA = $CTCCDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET3REQUEST
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET3REQUEST
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET3REQUEST
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET3REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDRIVERTYPE()
    {
      return $this->SZDRIVERTYPE;
    }

    /**
     * @param string $SZDRIVERTYPE
     * @return \Axess\Dci4Wtp\D4WTPISSUETICKET3REQUEST
     */
    public function setSZDRIVERTYPE($SZDRIVERTYPE)
    {
      $this->SZDRIVERTYPE = $SZDRIVERTYPE;
      return $this;
    }

}
