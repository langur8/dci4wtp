<?php

namespace Axess\Dci4Wtp;

class eMoneyBalanceHistory
{

    /**
     * @var D4WTPHISTORYREQ $i_ctEmoneyBalanceHistory
     */
    protected $i_ctEmoneyBalanceHistory = null;

    /**
     * @param D4WTPHISTORYREQ $i_ctEmoneyBalanceHistory
     */
    public function __construct($i_ctEmoneyBalanceHistory)
    {
      $this->i_ctEmoneyBalanceHistory = $i_ctEmoneyBalanceHistory;
    }

    /**
     * @return D4WTPHISTORYREQ
     */
    public function getI_ctEmoneyBalanceHistory()
    {
      return $this->i_ctEmoneyBalanceHistory;
    }

    /**
     * @param D4WTPHISTORYREQ $i_ctEmoneyBalanceHistory
     * @return \Axess\Dci4Wtp\eMoneyBalanceHistory
     */
    public function setI_ctEmoneyBalanceHistory($i_ctEmoneyBalanceHistory)
    {
      $this->i_ctEmoneyBalanceHistory = $i_ctEmoneyBalanceHistory;
      return $this;
    }

}
