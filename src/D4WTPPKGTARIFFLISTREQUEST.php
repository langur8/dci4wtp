<?php

namespace Axess\Dci4Wtp;

class D4WTPPKGTARIFFLISTREQUEST
{

    /**
     * @var ArrayOfD4WTPPACKAGE $ACTPACKAGE
     */
    protected $ACTPACKAGE = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    /**
     * @var string $SZVALIDFROMEND
     */
    protected $SZVALIDFROMEND = null;

    /**
     * @var string $SZVALIDFROMSTART
     */
    protected $SZVALIDFROMSTART = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPACKAGE
     */
    public function getACTPACKAGE()
    {
      return $this->ACTPACKAGE;
    }

    /**
     * @param ArrayOfD4WTPPACKAGE $ACTPACKAGE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTREQUEST
     */
    public function setACTPACKAGE($ACTPACKAGE)
    {
      $this->ACTPACKAGE = $ACTPACKAGE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTREQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTREQUEST
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROMEND()
    {
      return $this->SZVALIDFROMEND;
    }

    /**
     * @param string $SZVALIDFROMEND
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTREQUEST
     */
    public function setSZVALIDFROMEND($SZVALIDFROMEND)
    {
      $this->SZVALIDFROMEND = $SZVALIDFROMEND;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROMSTART()
    {
      return $this->SZVALIDFROMSTART;
    }

    /**
     * @param string $SZVALIDFROMSTART
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLISTREQUEST
     */
    public function setSZVALIDFROMSTART($SZVALIDFROMSTART)
    {
      $this->SZVALIDFROMSTART = $SZVALIDFROMSTART;
      return $this;
    }

}
