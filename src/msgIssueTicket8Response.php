<?php

namespace Axess\Dci4Wtp;

class msgIssueTicket8Response
{

    /**
     * @var D4WTPMSGISSUETICKETRESULT8 $msgIssueTicket8Result
     */
    protected $msgIssueTicket8Result = null;

    /**
     * @param D4WTPMSGISSUETICKETRESULT8 $msgIssueTicket8Result
     */
    public function __construct($msgIssueTicket8Result)
    {
      $this->msgIssueTicket8Result = $msgIssueTicket8Result;
    }

    /**
     * @return D4WTPMSGISSUETICKETRESULT8
     */
    public function getMsgIssueTicket8Result()
    {
      return $this->msgIssueTicket8Result;
    }

    /**
     * @param D4WTPMSGISSUETICKETRESULT8 $msgIssueTicket8Result
     * @return \Axess\Dci4Wtp\msgIssueTicket8Response
     */
    public function setMsgIssueTicket8Result($msgIssueTicket8Result)
    {
      $this->msgIssueTicket8Result = $msgIssueTicket8Result;
      return $this;
    }

}
