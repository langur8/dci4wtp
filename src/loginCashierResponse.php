<?php

namespace Axess\Dci4Wtp;

class loginCashierResponse
{

    /**
     * @var D4WTPLOGINCASHIERRESULT $loginCashierResult
     */
    protected $loginCashierResult = null;

    /**
     * @param D4WTPLOGINCASHIERRESULT $loginCashierResult
     */
    public function __construct($loginCashierResult)
    {
      $this->loginCashierResult = $loginCashierResult;
    }

    /**
     * @return D4WTPLOGINCASHIERRESULT
     */
    public function getLoginCashierResult()
    {
      return $this->loginCashierResult;
    }

    /**
     * @param D4WTPLOGINCASHIERRESULT $loginCashierResult
     * @return \Axess\Dci4Wtp\loginCashierResponse
     */
    public function setLoginCashierResult($loginCashierResult)
    {
      $this->loginCashierResult = $loginCashierResult;
      return $this;
    }

}
