<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTICKETSALE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTICKETSALE[] $D4WTPTICKETSALE
     */
    protected $D4WTPTICKETSALE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTICKETSALE[]
     */
    public function getD4WTPTICKETSALE()
    {
      return $this->D4WTPTICKETSALE;
    }

    /**
     * @param D4WTPTICKETSALE[] $D4WTPTICKETSALE
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTICKETSALE
     */
    public function setD4WTPTICKETSALE(array $D4WTPTICKETSALE = null)
    {
      $this->D4WTPTICKETSALE = $D4WTPTICKETSALE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTICKETSALE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTICKETSALE
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTICKETSALE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTICKETSALE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTICKETSALE[] = $value;
      } else {
        $this->D4WTPTICKETSALE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTICKETSALE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTICKETSALE Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTICKETSALE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTICKETSALE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTICKETSALE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTICKETSALE);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTICKETSALE Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTICKETSALE);
    }

}
