<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPACKAGETARIFF implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPACKAGETARIFF[] $D4WTPPACKAGETARIFF
     */
    protected $D4WTPPACKAGETARIFF = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPACKAGETARIFF[]
     */
    public function getD4WTPPACKAGETARIFF()
    {
      return $this->D4WTPPACKAGETARIFF;
    }

    /**
     * @param D4WTPPACKAGETARIFF[] $D4WTPPACKAGETARIFF
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPACKAGETARIFF
     */
    public function setD4WTPPACKAGETARIFF(array $D4WTPPACKAGETARIFF = null)
    {
      $this->D4WTPPACKAGETARIFF = $D4WTPPACKAGETARIFF;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPACKAGETARIFF[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPACKAGETARIFF
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPACKAGETARIFF[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPACKAGETARIFF $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPACKAGETARIFF[] = $value;
      } else {
        $this->D4WTPPACKAGETARIFF[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPACKAGETARIFF[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPACKAGETARIFF Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPACKAGETARIFF);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPACKAGETARIFF);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPACKAGETARIFF);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPACKAGETARIFF);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPACKAGETARIFF Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPACKAGETARIFF);
    }

}
