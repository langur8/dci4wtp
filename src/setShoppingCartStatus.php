<?php

namespace Axess\Dci4Wtp;

class setShoppingCartStatus
{

    /**
     * @var D4WTPSETSHOPCARTSTATUSREQ $i_ctSetShoppingCartStatusReq
     */
    protected $i_ctSetShoppingCartStatusReq = null;

    /**
     * @param D4WTPSETSHOPCARTSTATUSREQ $i_ctSetShoppingCartStatusReq
     */
    public function __construct($i_ctSetShoppingCartStatusReq)
    {
      $this->i_ctSetShoppingCartStatusReq = $i_ctSetShoppingCartStatusReq;
    }

    /**
     * @return D4WTPSETSHOPCARTSTATUSREQ
     */
    public function getI_ctSetShoppingCartStatusReq()
    {
      return $this->i_ctSetShoppingCartStatusReq;
    }

    /**
     * @param D4WTPSETSHOPCARTSTATUSREQ $i_ctSetShoppingCartStatusReq
     * @return \Axess\Dci4Wtp\setShoppingCartStatus
     */
    public function setI_ctSetShoppingCartStatusReq($i_ctSetShoppingCartStatusReq)
    {
      $this->i_ctSetShoppingCartStatusReq = $i_ctSetShoppingCartStatusReq;
      return $this;
    }

}
