<?php

namespace Axess\Dci4Wtp;

class setStateOCCPickupProduction
{

    /**
     * @var D4WTPOCCPICKUPSTATEREQ $i_setOCCPickupStateReq
     */
    protected $i_setOCCPickupStateReq = null;

    /**
     * @param D4WTPOCCPICKUPSTATEREQ $i_setOCCPickupStateReq
     */
    public function __construct($i_setOCCPickupStateReq)
    {
      $this->i_setOCCPickupStateReq = $i_setOCCPickupStateReq;
    }

    /**
     * @return D4WTPOCCPICKUPSTATEREQ
     */
    public function getI_setOCCPickupStateReq()
    {
      return $this->i_setOCCPickupStateReq;
    }

    /**
     * @param D4WTPOCCPICKUPSTATEREQ $i_setOCCPickupStateReq
     * @return \Axess\Dci4Wtp\setStateOCCPickupProduction
     */
    public function setI_setOCCPickupStateReq($i_setOCCPickupStateReq)
    {
      $this->i_setOCCPickupStateReq = $i_setOCCPickupStateReq;
      return $this;
    }

}
