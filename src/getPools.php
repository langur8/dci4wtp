<?php

namespace Axess\Dci4Wtp;

class getPools
{

    /**
     * @var D4WTPPOOLREQUEST $i_ctPoolReq
     */
    protected $i_ctPoolReq = null;

    /**
     * @param D4WTPPOOLREQUEST $i_ctPoolReq
     */
    public function __construct($i_ctPoolReq)
    {
      $this->i_ctPoolReq = $i_ctPoolReq;
    }

    /**
     * @return D4WTPPOOLREQUEST
     */
    public function getI_ctPoolReq()
    {
      return $this->i_ctPoolReq;
    }

    /**
     * @param D4WTPPOOLREQUEST $i_ctPoolReq
     * @return \Axess\Dci4Wtp\getPools
     */
    public function setI_ctPoolReq($i_ctPoolReq)
    {
      $this->i_ctPoolReq = $i_ctPoolReq;
      return $this;
    }

}
