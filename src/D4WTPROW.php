<?php

namespace Axess\Dci4Wtp;

class D4WTPROW
{

    /**
     * @var float $BISAXCOUNTBANKTRANSFER
     */
    protected $BISAXCOUNTBANKTRANSFER = null;

    /**
     * @var float $BISAXCOUNTTRANSFER
     */
    protected $BISAXCOUNTTRANSFER = null;

    /**
     * @var float $FAMOUNT
     */
    protected $FAMOUNT = null;

    /**
     * @var float $FBALANCEAFTER
     */
    protected $FBALANCEAFTER = null;

    /**
     * @var float $FBALANCEBEFORE
     */
    protected $FBALANCEBEFORE = null;

    /**
     * @var float $NAXCOUNTKONTONR
     */
    protected $NAXCOUNTKONTONR = null;

    /**
     * @var float $NAXCOUNTKONTOZEILENNRREF
     */
    protected $NAXCOUNTKONTOZEILENNRREF = null;

    /**
     * @var float $NAXCOUNTKONTOZEILENTYP
     */
    protected $NAXCOUNTKONTOZEILENTYP = null;

    /**
     * @var float $NAXCOUNTMEMBERNR
     */
    protected $NAXCOUNTMEMBERNR = null;

    /**
     * @var float $NAXCOUNTROWNR
     */
    protected $NAXCOUNTROWNR = null;

    /**
     * @var float $NBANKNR
     */
    protected $NBANKNR = null;

    /**
     * @var float $NBRANCHNR
     */
    protected $NBRANCHNR = null;

    /**
     * @var float $NCORPNR
     */
    protected $NCORPNR = null;

    /**
     * @var float $NDESKNR
     */
    protected $NDESKNR = null;

    /**
     * @var float $NKONTOZEILENNRBEFORE
     */
    protected $NKONTOZEILENNRBEFORE = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var string $SZACCOUNTROWTEXT
     */
    protected $SZACCOUNTROWTEXT = null;

    /**
     * @var string $SZAXCOUNTBIC
     */
    protected $SZAXCOUNTBIC = null;

    /**
     * @var string $SZAXCOUNTIBAN
     */
    protected $SZAXCOUNTIBAN = null;

    /**
     * @var string $SZAXCOUNTROWTYPE
     */
    protected $SZAXCOUNTROWTYPE = null;

    /**
     * @var string $SZPOSTINGTEXT
     */
    protected $SZPOSTINGTEXT = null;

    /**
     * @var string $SZTRANSDATEDATE
     */
    protected $SZTRANSDATEDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISAXCOUNTBANKTRANSFER()
    {
      return $this->BISAXCOUNTBANKTRANSFER;
    }

    /**
     * @param float $BISAXCOUNTBANKTRANSFER
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setBISAXCOUNTBANKTRANSFER($BISAXCOUNTBANKTRANSFER)
    {
      $this->BISAXCOUNTBANKTRANSFER = $BISAXCOUNTBANKTRANSFER;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISAXCOUNTTRANSFER()
    {
      return $this->BISAXCOUNTTRANSFER;
    }

    /**
     * @param float $BISAXCOUNTTRANSFER
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setBISAXCOUNTTRANSFER($BISAXCOUNTTRANSFER)
    {
      $this->BISAXCOUNTTRANSFER = $BISAXCOUNTTRANSFER;
      return $this;
    }

    /**
     * @return float
     */
    public function getFAMOUNT()
    {
      return $this->FAMOUNT;
    }

    /**
     * @param float $FAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setFAMOUNT($FAMOUNT)
    {
      $this->FAMOUNT = $FAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFBALANCEAFTER()
    {
      return $this->FBALANCEAFTER;
    }

    /**
     * @param float $FBALANCEAFTER
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setFBALANCEAFTER($FBALANCEAFTER)
    {
      $this->FBALANCEAFTER = $FBALANCEAFTER;
      return $this;
    }

    /**
     * @return float
     */
    public function getFBALANCEBEFORE()
    {
      return $this->FBALANCEBEFORE;
    }

    /**
     * @param float $FBALANCEBEFORE
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setFBALANCEBEFORE($FBALANCEBEFORE)
    {
      $this->FBALANCEBEFORE = $FBALANCEBEFORE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAXCOUNTKONTONR()
    {
      return $this->NAXCOUNTKONTONR;
    }

    /**
     * @param float $NAXCOUNTKONTONR
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setNAXCOUNTKONTONR($NAXCOUNTKONTONR)
    {
      $this->NAXCOUNTKONTONR = $NAXCOUNTKONTONR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAXCOUNTKONTOZEILENNRREF()
    {
      return $this->NAXCOUNTKONTOZEILENNRREF;
    }

    /**
     * @param float $NAXCOUNTKONTOZEILENNRREF
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setNAXCOUNTKONTOZEILENNRREF($NAXCOUNTKONTOZEILENNRREF)
    {
      $this->NAXCOUNTKONTOZEILENNRREF = $NAXCOUNTKONTOZEILENNRREF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAXCOUNTKONTOZEILENTYP()
    {
      return $this->NAXCOUNTKONTOZEILENTYP;
    }

    /**
     * @param float $NAXCOUNTKONTOZEILENTYP
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setNAXCOUNTKONTOZEILENTYP($NAXCOUNTKONTOZEILENTYP)
    {
      $this->NAXCOUNTKONTOZEILENTYP = $NAXCOUNTKONTOZEILENTYP;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAXCOUNTMEMBERNR()
    {
      return $this->NAXCOUNTMEMBERNR;
    }

    /**
     * @param float $NAXCOUNTMEMBERNR
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setNAXCOUNTMEMBERNR($NAXCOUNTMEMBERNR)
    {
      $this->NAXCOUNTMEMBERNR = $NAXCOUNTMEMBERNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAXCOUNTROWNR()
    {
      return $this->NAXCOUNTROWNR;
    }

    /**
     * @param float $NAXCOUNTROWNR
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setNAXCOUNTROWNR($NAXCOUNTROWNR)
    {
      $this->NAXCOUNTROWNR = $NAXCOUNTROWNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBANKNR()
    {
      return $this->NBANKNR;
    }

    /**
     * @param float $NBANKNR
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setNBANKNR($NBANKNR)
    {
      $this->NBANKNR = $NBANKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBRANCHNR()
    {
      return $this->NBRANCHNR;
    }

    /**
     * @param float $NBRANCHNR
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setNBRANCHNR($NBRANCHNR)
    {
      $this->NBRANCHNR = $NBRANCHNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCORPNR()
    {
      return $this->NCORPNR;
    }

    /**
     * @param float $NCORPNR
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setNCORPNR($NCORPNR)
    {
      $this->NCORPNR = $NCORPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDESKNR()
    {
      return $this->NDESKNR;
    }

    /**
     * @param float $NDESKNR
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setNDESKNR($NDESKNR)
    {
      $this->NDESKNR = $NDESKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKONTOZEILENNRBEFORE()
    {
      return $this->NKONTOZEILENNRBEFORE;
    }

    /**
     * @param float $NKONTOZEILENNRBEFORE
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setNKONTOZEILENNRBEFORE($NKONTOZEILENNRBEFORE)
    {
      $this->NKONTOZEILENNRBEFORE = $NKONTOZEILENNRBEFORE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZACCOUNTROWTEXT()
    {
      return $this->SZACCOUNTROWTEXT;
    }

    /**
     * @param string $SZACCOUNTROWTEXT
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setSZACCOUNTROWTEXT($SZACCOUNTROWTEXT)
    {
      $this->SZACCOUNTROWTEXT = $SZACCOUNTROWTEXT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAXCOUNTBIC()
    {
      return $this->SZAXCOUNTBIC;
    }

    /**
     * @param string $SZAXCOUNTBIC
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setSZAXCOUNTBIC($SZAXCOUNTBIC)
    {
      $this->SZAXCOUNTBIC = $SZAXCOUNTBIC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAXCOUNTIBAN()
    {
      return $this->SZAXCOUNTIBAN;
    }

    /**
     * @param string $SZAXCOUNTIBAN
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setSZAXCOUNTIBAN($SZAXCOUNTIBAN)
    {
      $this->SZAXCOUNTIBAN = $SZAXCOUNTIBAN;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAXCOUNTROWTYPE()
    {
      return $this->SZAXCOUNTROWTYPE;
    }

    /**
     * @param string $SZAXCOUNTROWTYPE
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setSZAXCOUNTROWTYPE($SZAXCOUNTROWTYPE)
    {
      $this->SZAXCOUNTROWTYPE = $SZAXCOUNTROWTYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOSTINGTEXT()
    {
      return $this->SZPOSTINGTEXT;
    }

    /**
     * @param string $SZPOSTINGTEXT
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setSZPOSTINGTEXT($SZPOSTINGTEXT)
    {
      $this->SZPOSTINGTEXT = $SZPOSTINGTEXT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTRANSDATEDATE()
    {
      return $this->SZTRANSDATEDATE;
    }

    /**
     * @param string $SZTRANSDATEDATE
     * @return \Axess\Dci4Wtp\D4WTPROW
     */
    public function setSZTRANSDATEDATE($SZTRANSDATEDATE)
    {
      $this->SZTRANSDATEDATE = $SZTRANSDATEDATE;
      return $this;
    }

}
