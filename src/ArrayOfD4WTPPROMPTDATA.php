<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPROMPTDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPROMPTDATA[] $D4WTPPROMPTDATA
     */
    protected $D4WTPPROMPTDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPROMPTDATA[]
     */
    public function getD4WTPPROMPTDATA()
    {
      return $this->D4WTPPROMPTDATA;
    }

    /**
     * @param D4WTPPROMPTDATA[] $D4WTPPROMPTDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPROMPTDATA
     */
    public function setD4WTPPROMPTDATA(array $D4WTPPROMPTDATA = null)
    {
      $this->D4WTPPROMPTDATA = $D4WTPPROMPTDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPROMPTDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPROMPTDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPROMPTDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPROMPTDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPROMPTDATA[] = $value;
      } else {
        $this->D4WTPPROMPTDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPROMPTDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPROMPTDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPROMPTDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPROMPTDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPROMPTDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPROMPTDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPROMPTDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPROMPTDATA);
    }

}
