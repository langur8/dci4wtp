<?php

namespace Axess\Dci4Wtp;

class getTariffListResponse
{

    /**
     * @var D4WTPTARIFFLISTRESULT $getTariffListResult
     */
    protected $getTariffListResult = null;

    /**
     * @param D4WTPTARIFFLISTRESULT $getTariffListResult
     */
    public function __construct($getTariffListResult)
    {
      $this->getTariffListResult = $getTariffListResult;
    }

    /**
     * @return D4WTPTARIFFLISTRESULT
     */
    public function getGetTariffListResult()
    {
      return $this->getTariffListResult;
    }

    /**
     * @param D4WTPTARIFFLISTRESULT $getTariffListResult
     * @return \Axess\Dci4Wtp\getTariffListResponse
     */
    public function setGetTariffListResult($getTariffListResult)
    {
      $this->getTariffListResult = $getTariffListResult;
      return $this;
    }

}
