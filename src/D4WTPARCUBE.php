<?php

namespace Axess\Dci4Wtp;

class D4WTPARCUBE
{

    /**
     * @var float $NARCNO
     */
    protected $NARCNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNARCNO()
    {
      return $this->NARCNO;
    }

    /**
     * @param float $NARCNO
     * @return \Axess\Dci4Wtp\D4WTPARCUBE
     */
    public function setNARCNO($NARCNO)
    {
      $this->NARCNO = $NARCNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPARCUBE
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPARCUBE
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
