<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTICKETEXTORDERNRRESULT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTICKETEXTORDERNRRESULT[] $D4WTPTICKETEXTORDERNRRESULT
     */
    protected $D4WTPTICKETEXTORDERNRRESULT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTICKETEXTORDERNRRESULT[]
     */
    public function getD4WTPTICKETEXTORDERNRRESULT()
    {
      return $this->D4WTPTICKETEXTORDERNRRESULT;
    }

    /**
     * @param D4WTPTICKETEXTORDERNRRESULT[] $D4WTPTICKETEXTORDERNRRESULT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTICKETEXTORDERNRRESULT
     */
    public function setD4WTPTICKETEXTORDERNRRESULT(array $D4WTPTICKETEXTORDERNRRESULT = null)
    {
      $this->D4WTPTICKETEXTORDERNRRESULT = $D4WTPTICKETEXTORDERNRRESULT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTICKETEXTORDERNRRESULT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTICKETEXTORDERNRRESULT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTICKETEXTORDERNRRESULT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTICKETEXTORDERNRRESULT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTICKETEXTORDERNRRESULT[] = $value;
      } else {
        $this->D4WTPTICKETEXTORDERNRRESULT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTICKETEXTORDERNRRESULT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTICKETEXTORDERNRRESULT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTICKETEXTORDERNRRESULT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTICKETEXTORDERNRRESULT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTICKETEXTORDERNRRESULT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTICKETEXTORDERNRRESULT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTICKETEXTORDERNRRESULT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTICKETEXTORDERNRRESULT);
    }

}
