<?php

namespace Axess\Dci4Wtp;

class D4WTPCHECKWTPNO3RESULT
{

    /**
     * @var float $BISCANCELED
     */
    protected $BISCANCELED = null;

    /**
     * @var float $BTICKETALLOWED
     */
    protected $BTICKETALLOWED = null;

    /**
     * @var float $BTICKETFOUND
     */
    protected $BTICKETFOUND = null;

    /**
     * @var float $NBLOCKNO
     */
    protected $NBLOCKNO = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NOWNERPERSNO
     */
    protected $NOWNERPERSNO = null;

    /**
     * @var float $NOWNERPERSPOSNO
     */
    protected $NOWNERPERSPOSNO = null;

    /**
     * @var float $NOWNERPERSPROJNO
     */
    protected $NOWNERPERSPROJNO = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NPERSTYPENO
     */
    protected $NPERSTYPENO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    /**
     * @var float $NUNICODENO
     */
    protected $NUNICODENO = null;

    /**
     * @var string $SZCHIPID
     */
    protected $SZCHIPID = null;

    /**
     * @var string $SZDATEOFBIRTH
     */
    protected $SZDATEOFBIRTH = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZGENDER
     */
    protected $SZGENDER = null;

    /**
     * @var string $SZISSUEDATE
     */
    protected $SZISSUEDATE = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    /**
     * @var string $SZOWNERDATEOFBIRTH
     */
    protected $SZOWNERDATEOFBIRTH = null;

    /**
     * @var string $SZOWNERFIRSTNAME
     */
    protected $SZOWNERFIRSTNAME = null;

    /**
     * @var string $SZOWNERGENDER
     */
    protected $SZOWNERGENDER = null;

    /**
     * @var string $SZOWNERLASTNAME
     */
    protected $SZOWNERLASTNAME = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISCANCELED()
    {
      return $this->BISCANCELED;
    }

    /**
     * @param float $BISCANCELED
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setBISCANCELED($BISCANCELED)
    {
      $this->BISCANCELED = $BISCANCELED;
      return $this;
    }

    /**
     * @return float
     */
    public function getBTICKETALLOWED()
    {
      return $this->BTICKETALLOWED;
    }

    /**
     * @param float $BTICKETALLOWED
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setBTICKETALLOWED($BTICKETALLOWED)
    {
      $this->BTICKETALLOWED = $BTICKETALLOWED;
      return $this;
    }

    /**
     * @return float
     */
    public function getBTICKETFOUND()
    {
      return $this->BTICKETFOUND;
    }

    /**
     * @param float $BTICKETFOUND
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setBTICKETFOUND($BTICKETFOUND)
    {
      $this->BTICKETFOUND = $BTICKETFOUND;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKNO()
    {
      return $this->NBLOCKNO;
    }

    /**
     * @param float $NBLOCKNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNBLOCKNO($NBLOCKNO)
    {
      $this->NBLOCKNO = $NBLOCKNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNOWNERPERSNO()
    {
      return $this->NOWNERPERSNO;
    }

    /**
     * @param float $NOWNERPERSNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNOWNERPERSNO($NOWNERPERSNO)
    {
      $this->NOWNERPERSNO = $NOWNERPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNOWNERPERSPOSNO()
    {
      return $this->NOWNERPERSPOSNO;
    }

    /**
     * @param float $NOWNERPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNOWNERPERSPOSNO($NOWNERPERSPOSNO)
    {
      $this->NOWNERPERSPOSNO = $NOWNERPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNOWNERPERSPROJNO()
    {
      return $this->NOWNERPERSPROJNO;
    }

    /**
     * @param float $NOWNERPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNOWNERPERSPROJNO($NOWNERPERSPROJNO)
    {
      $this->NOWNERPERSPROJNO = $NOWNERPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSTYPENO()
    {
      return $this->NPERSTYPENO;
    }

    /**
     * @param float $NPERSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNPERSTYPENO($NPERSTYPENO)
    {
      $this->NPERSTYPENO = $NPERSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENO()
    {
      return $this->NUNICODENO;
    }

    /**
     * @param float $NUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setNUNICODENO($NUNICODENO)
    {
      $this->NUNICODENO = $NUNICODENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCHIPID()
    {
      return $this->SZCHIPID;
    }

    /**
     * @param string $SZCHIPID
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZCHIPID($SZCHIPID)
    {
      $this->SZCHIPID = $SZCHIPID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEOFBIRTH()
    {
      return $this->SZDATEOFBIRTH;
    }

    /**
     * @param string $SZDATEOFBIRTH
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZDATEOFBIRTH($SZDATEOFBIRTH)
    {
      $this->SZDATEOFBIRTH = $SZDATEOFBIRTH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGENDER()
    {
      return $this->SZGENDER;
    }

    /**
     * @param string $SZGENDER
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZGENDER($SZGENDER)
    {
      $this->SZGENDER = $SZGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZISSUEDATE()
    {
      return $this->SZISSUEDATE;
    }

    /**
     * @param string $SZISSUEDATE
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZISSUEDATE($SZISSUEDATE)
    {
      $this->SZISSUEDATE = $SZISSUEDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZOWNERDATEOFBIRTH()
    {
      return $this->SZOWNERDATEOFBIRTH;
    }

    /**
     * @param string $SZOWNERDATEOFBIRTH
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZOWNERDATEOFBIRTH($SZOWNERDATEOFBIRTH)
    {
      $this->SZOWNERDATEOFBIRTH = $SZOWNERDATEOFBIRTH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZOWNERFIRSTNAME()
    {
      return $this->SZOWNERFIRSTNAME;
    }

    /**
     * @param string $SZOWNERFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZOWNERFIRSTNAME($SZOWNERFIRSTNAME)
    {
      $this->SZOWNERFIRSTNAME = $SZOWNERFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZOWNERGENDER()
    {
      return $this->SZOWNERGENDER;
    }

    /**
     * @param string $SZOWNERGENDER
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZOWNERGENDER($SZOWNERGENDER)
    {
      $this->SZOWNERGENDER = $SZOWNERGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZOWNERLASTNAME()
    {
      return $this->SZOWNERLASTNAME;
    }

    /**
     * @param string $SZOWNERLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZOWNERLASTNAME($SZOWNERLASTNAME)
    {
      $this->SZOWNERLASTNAME = $SZOWNERLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPCHECKWTPNO3RESULT
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
