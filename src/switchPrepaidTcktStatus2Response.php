<?php

namespace Axess\Dci4Wtp;

class switchPrepaidTcktStatus2Response
{

    /**
     * @var D4WTPSWITCHPREPAIDRES2 $switchPrepaidTcktStatus2Result
     */
    protected $switchPrepaidTcktStatus2Result = null;

    /**
     * @param D4WTPSWITCHPREPAIDRES2 $switchPrepaidTcktStatus2Result
     */
    public function __construct($switchPrepaidTcktStatus2Result)
    {
      $this->switchPrepaidTcktStatus2Result = $switchPrepaidTcktStatus2Result;
    }

    /**
     * @return D4WTPSWITCHPREPAIDRES2
     */
    public function getSwitchPrepaidTcktStatus2Result()
    {
      return $this->switchPrepaidTcktStatus2Result;
    }

    /**
     * @param D4WTPSWITCHPREPAIDRES2 $switchPrepaidTcktStatus2Result
     * @return \Axess\Dci4Wtp\switchPrepaidTcktStatus2Response
     */
    public function setSwitchPrepaidTcktStatus2Result($switchPrepaidTcktStatus2Result)
    {
      $this->switchPrepaidTcktStatus2Result = $switchPrepaidTcktStatus2Result;
      return $this;
    }

}
