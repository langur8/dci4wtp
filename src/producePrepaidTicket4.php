<?php

namespace Axess\Dci4Wtp;

class producePrepaidTicket4
{

    /**
     * @var D4WTPPRODUCEPREPAIDREQ4 $i_producePrepaidReq
     */
    protected $i_producePrepaidReq = null;

    /**
     * @param D4WTPPRODUCEPREPAIDREQ4 $i_producePrepaidReq
     */
    public function __construct($i_producePrepaidReq)
    {
      $this->i_producePrepaidReq = $i_producePrepaidReq;
    }

    /**
     * @return D4WTPPRODUCEPREPAIDREQ4
     */
    public function getI_producePrepaidReq()
    {
      return $this->i_producePrepaidReq;
    }

    /**
     * @param D4WTPPRODUCEPREPAIDREQ4 $i_producePrepaidReq
     * @return \Axess\Dci4Wtp\producePrepaidTicket4
     */
    public function setI_producePrepaidReq($i_producePrepaidReq)
    {
      $this->i_producePrepaidReq = $i_producePrepaidReq;
      return $this;
    }

}
