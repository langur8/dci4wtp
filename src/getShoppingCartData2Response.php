<?php

namespace Axess\Dci4Wtp;

class getShoppingCartData2Response
{

    /**
     * @var D4WTPGETSHOPCARTDATA2RESULT $getShoppingCartData2Result
     */
    protected $getShoppingCartData2Result = null;

    /**
     * @param D4WTPGETSHOPCARTDATA2RESULT $getShoppingCartData2Result
     */
    public function __construct($getShoppingCartData2Result)
    {
      $this->getShoppingCartData2Result = $getShoppingCartData2Result;
    }

    /**
     * @return D4WTPGETSHOPCARTDATA2RESULT
     */
    public function getGetShoppingCartData2Result()
    {
      return $this->getShoppingCartData2Result;
    }

    /**
     * @param D4WTPGETSHOPCARTDATA2RESULT $getShoppingCartData2Result
     * @return \Axess\Dci4Wtp\getShoppingCartData2Response
     */
    public function setGetShoppingCartData2Result($getShoppingCartData2Result)
    {
      $this->getShoppingCartData2Result = $getShoppingCartData2Result;
      return $this;
    }

}
