<?php

namespace Axess\Dci4Wtp;

class decodeBarcode
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var string $i_szBarCode
     */
    protected $i_szBarCode = null;

    /**
     * @param float $i_nSessionID
     * @param string $i_szBarCode
     */
    public function __construct($i_nSessionID, $i_szBarCode)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_szBarCode = $i_szBarCode;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\decodeBarcode
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szBarCode()
    {
      return $this->i_szBarCode;
    }

    /**
     * @param string $i_szBarCode
     * @return \Axess\Dci4Wtp\decodeBarcode
     */
    public function setI_szBarCode($i_szBarCode)
    {
      $this->i_szBarCode = $i_szBarCode;
      return $this;
    }

}
