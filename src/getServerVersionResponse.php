<?php

namespace Axess\Dci4Wtp;

class getServerVersionResponse
{

    /**
     * @var string $getServerVersionResult
     */
    protected $getServerVersionResult = null;

    /**
     * @param string $getServerVersionResult
     */
    public function __construct($getServerVersionResult)
    {
      $this->getServerVersionResult = $getServerVersionResult;
    }

    /**
     * @return string
     */
    public function getGetServerVersionResult()
    {
      return $this->getServerVersionResult;
    }

    /**
     * @param string $getServerVersionResult
     * @return \Axess\Dci4Wtp\getServerVersionResponse
     */
    public function setGetServerVersionResult($getServerVersionResult)
    {
      $this->getServerVersionResult = $getServerVersionResult;
      return $this;
    }

}
