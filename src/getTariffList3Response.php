<?php

namespace Axess\Dci4Wtp;

class getTariffList3Response
{

    /**
     * @var D4WTPTARIFFLIST3RESULT $getTariffList3Result
     */
    protected $getTariffList3Result = null;

    /**
     * @param D4WTPTARIFFLIST3RESULT $getTariffList3Result
     */
    public function __construct($getTariffList3Result)
    {
      $this->getTariffList3Result = $getTariffList3Result;
    }

    /**
     * @return D4WTPTARIFFLIST3RESULT
     */
    public function getGetTariffList3Result()
    {
      return $this->getTariffList3Result;
    }

    /**
     * @param D4WTPTARIFFLIST3RESULT $getTariffList3Result
     * @return \Axess\Dci4Wtp\getTariffList3Response
     */
    public function setGetTariffList3Result($getTariffList3Result)
    {
      $this->getTariffList3Result = $getTariffList3Result;
      return $this;
    }

}
