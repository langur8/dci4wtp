<?php

namespace Axess\Dci4Wtp;

class getPersonTypes2Response
{

    /**
     * @var D4WTPPERSONTYPE2RESULT $getPersonTypes2Result
     */
    protected $getPersonTypes2Result = null;

    /**
     * @param D4WTPPERSONTYPE2RESULT $getPersonTypes2Result
     */
    public function __construct($getPersonTypes2Result)
    {
      $this->getPersonTypes2Result = $getPersonTypes2Result;
    }

    /**
     * @return D4WTPPERSONTYPE2RESULT
     */
    public function getGetPersonTypes2Result()
    {
      return $this->getPersonTypes2Result;
    }

    /**
     * @param D4WTPPERSONTYPE2RESULT $getPersonTypes2Result
     * @return \Axess\Dci4Wtp\getPersonTypes2Response
     */
    public function setGetPersonTypes2Result($getPersonTypes2Result)
    {
      $this->getPersonTypes2Result = $getPersonTypes2Result;
      return $this;
    }

}
