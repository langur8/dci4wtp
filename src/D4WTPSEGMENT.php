<?php

namespace Axess\Dci4Wtp;

class D4WTPSEGMENT
{

    /**
     * @var string $SZSEGMENTCONTENT
     */
    protected $SZSEGMENTCONTENT = null;

    /**
     * @var string $SZSEGMENTNAME
     */
    protected $SZSEGMENTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZSEGMENTCONTENT()
    {
      return $this->SZSEGMENTCONTENT;
    }

    /**
     * @param string $SZSEGMENTCONTENT
     * @return \Axess\Dci4Wtp\D4WTPSEGMENT
     */
    public function setSZSEGMENTCONTENT($SZSEGMENTCONTENT)
    {
      $this->SZSEGMENTCONTENT = $SZSEGMENTCONTENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSEGMENTNAME()
    {
      return $this->SZSEGMENTNAME;
    }

    /**
     * @param string $SZSEGMENTNAME
     * @return \Axess\Dci4Wtp\D4WTPSEGMENT
     */
    public function setSZSEGMENTNAME($SZSEGMENTNAME)
    {
      $this->SZSEGMENTNAME = $SZSEGMENTNAME;
      return $this;
    }

}
