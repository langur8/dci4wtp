<?php

namespace Axess\Dci4Wtp;

class D4WTPREPORTREQUEST
{

    /**
     * @var float $NREPORTID
     */
    protected $NREPORTID = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZENDDATE
     */
    protected $SZENDDATE = null;

    /**
     * @var string $SZSTARTDATE
     */
    protected $SZSTARTDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNREPORTID()
    {
      return $this->NREPORTID;
    }

    /**
     * @param float $NREPORTID
     * @return \Axess\Dci4Wtp\D4WTPREPORTREQUEST
     */
    public function setNREPORTID($NREPORTID)
    {
      $this->NREPORTID = $NREPORTID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPREPORTREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZENDDATE()
    {
      return $this->SZENDDATE;
    }

    /**
     * @param string $SZENDDATE
     * @return \Axess\Dci4Wtp\D4WTPREPORTREQUEST
     */
    public function setSZENDDATE($SZENDDATE)
    {
      $this->SZENDDATE = $SZENDDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTARTDATE()
    {
      return $this->SZSTARTDATE;
    }

    /**
     * @param string $SZSTARTDATE
     * @return \Axess\Dci4Wtp\D4WTPREPORTREQUEST
     */
    public function setSZSTARTDATE($SZSTARTDATE)
    {
      $this->SZSTARTDATE = $SZSTARTDATE;
      return $this;
    }

}
