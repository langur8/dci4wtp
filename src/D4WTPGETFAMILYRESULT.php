<?php

namespace Axess\Dci4Wtp;

class D4WTPGETFAMILYRESULT
{

    /**
     * @var ArrayOfD4WTPFAMILYMEMBERDATA $ACTFAMILYMEMBERDATA
     */
    protected $ACTFAMILYMEMBERDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPFAMILYMEMBERDATA
     */
    public function getACTFAMILYMEMBERDATA()
    {
      return $this->ACTFAMILYMEMBERDATA;
    }

    /**
     * @param ArrayOfD4WTPFAMILYMEMBERDATA $ACTFAMILYMEMBERDATA
     * @return \Axess\Dci4Wtp\D4WTPGETFAMILYRESULT
     */
    public function setACTFAMILYMEMBERDATA($ACTFAMILYMEMBERDATA)
    {
      $this->ACTFAMILYMEMBERDATA = $ACTFAMILYMEMBERDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETFAMILYRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETFAMILYRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
