<?php

namespace Axess\Dci4Wtp;

class lockShoppingCart
{

    /**
     * @var D4WTPLOCKSHOPCARTREQUEST $i_ctLockShoppingCartReq
     */
    protected $i_ctLockShoppingCartReq = null;

    /**
     * @param D4WTPLOCKSHOPCARTREQUEST $i_ctLockShoppingCartReq
     */
    public function __construct($i_ctLockShoppingCartReq)
    {
      $this->i_ctLockShoppingCartReq = $i_ctLockShoppingCartReq;
    }

    /**
     * @return D4WTPLOCKSHOPCARTREQUEST
     */
    public function getI_ctLockShoppingCartReq()
    {
      return $this->i_ctLockShoppingCartReq;
    }

    /**
     * @param D4WTPLOCKSHOPCARTREQUEST $i_ctLockShoppingCartReq
     * @return \Axess\Dci4Wtp\lockShoppingCart
     */
    public function setI_ctLockShoppingCartReq($i_ctLockShoppingCartReq)
    {
      $this->i_ctLockShoppingCartReq = $i_ctLockShoppingCartReq;
      return $this;
    }

}
