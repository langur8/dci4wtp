<?php

namespace Axess\Dci4Wtp;

class D4WTPMSGISSUETICKETRESULT7
{

    /**
     * @var ArrayOfD4WTPMSGTICKETRESDATA2 $ACTMSGTICKETRESDATA
     */
    protected $ACTMSGTICKETRESDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPMSGTICKETRESDATA2
     */
    public function getACTMSGTICKETRESDATA()
    {
      return $this->ACTMSGTICKETRESDATA;
    }

    /**
     * @param ArrayOfD4WTPMSGTICKETRESDATA2 $ACTMSGTICKETRESDATA
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETRESULT7
     */
    public function setACTMSGTICKETRESDATA($ACTMSGTICKETRESDATA)
    {
      $this->ACTMSGTICKETRESDATA = $ACTMSGTICKETRESDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETRESULT7
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPMSGISSUETICKETRESULT7
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
