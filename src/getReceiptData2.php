<?php

namespace Axess\Dci4Wtp;

class getReceiptData2
{

    /**
     * @var D4WTPGETRECEIPTDATAREQUEST2 $i_ctReceiptDataReq
     */
    protected $i_ctReceiptDataReq = null;

    /**
     * @param D4WTPGETRECEIPTDATAREQUEST2 $i_ctReceiptDataReq
     */
    public function __construct($i_ctReceiptDataReq)
    {
      $this->i_ctReceiptDataReq = $i_ctReceiptDataReq;
    }

    /**
     * @return D4WTPGETRECEIPTDATAREQUEST2
     */
    public function getI_ctReceiptDataReq()
    {
      return $this->i_ctReceiptDataReq;
    }

    /**
     * @param D4WTPGETRECEIPTDATAREQUEST2 $i_ctReceiptDataReq
     * @return \Axess\Dci4Wtp\getReceiptData2
     */
    public function setI_ctReceiptDataReq($i_ctReceiptDataReq)
    {
      $this->i_ctReceiptDataReq = $i_ctReceiptDataReq;
      return $this;
    }

}
