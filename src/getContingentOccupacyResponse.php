<?php

namespace Axess\Dci4Wtp;

class getContingentOccupacyResponse
{

    /**
     * @var D4WTPGETCONTINGENTOCUPACYRES $getContingentOccupacyResult
     */
    protected $getContingentOccupacyResult = null;

    /**
     * @param D4WTPGETCONTINGENTOCUPACYRES $getContingentOccupacyResult
     */
    public function __construct($getContingentOccupacyResult)
    {
      $this->getContingentOccupacyResult = $getContingentOccupacyResult;
    }

    /**
     * @return D4WTPGETCONTINGENTOCUPACYRES
     */
    public function getGetContingentOccupacyResult()
    {
      return $this->getContingentOccupacyResult;
    }

    /**
     * @param D4WTPGETCONTINGENTOCUPACYRES $getContingentOccupacyResult
     * @return \Axess\Dci4Wtp\getContingentOccupacyResponse
     */
    public function setGetContingentOccupacyResult($getContingentOccupacyResult)
    {
      $this->getContingentOccupacyResult = $getContingentOccupacyResult;
      return $this;
    }

}
