<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGECONTENT3RESULT
{

    /**
     * @var ArrayOfD4WTPPACKAGECONTENT3 $ACTPACKAGECONTENT
     */
    protected $ACTPACKAGECONTENT = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPACKAGECONTENT3
     */
    public function getACTPACKAGECONTENT()
    {
      return $this->ACTPACKAGECONTENT;
    }

    /**
     * @param ArrayOfD4WTPPACKAGECONTENT3 $ACTPACKAGECONTENT
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT3RESULT
     */
    public function setACTPACKAGECONTENT($ACTPACKAGECONTENT)
    {
      $this->ACTPACKAGECONTENT = $ACTPACKAGECONTENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT3RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPACKAGECONTENT3RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
