<?php

namespace Axess\Dci4Wtp;

class D4WTGETWORKORDERSRESULT
{

    /**
     * @var ArrayOfD4WTPWORKORDER $ACTWORKORDER
     */
    protected $ACTWORKORDER = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPWORKORDER
     */
    public function getACTWORKORDER()
    {
      return $this->ACTWORKORDER;
    }

    /**
     * @param ArrayOfD4WTPWORKORDER $ACTWORKORDER
     * @return \Axess\Dci4Wtp\D4WTGETWORKORDERSRESULT
     */
    public function setACTWORKORDER($ACTWORKORDER)
    {
      $this->ACTWORKORDER = $ACTWORKORDER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTGETWORKORDERSRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTGETWORKORDERSRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
