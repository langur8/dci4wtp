<?php

namespace Axess\Dci4Wtp;

class D4WTPSALESDATARESULT
{

    /**
     * @var ArrayOfD4WTPSALESDATA $ACTSALESDATA
     */
    protected $ACTSALESDATA = null;

    /**
     * @var D4WTPCREDITCARDDATA $CTCREDITCARDDATA
     */
    protected $CTCREDITCARDDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NRECEIPTID
     */
    protected $NRECEIPTID = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSALESDATA
     */
    public function getACTSALESDATA()
    {
      return $this->ACTSALESDATA;
    }

    /**
     * @param ArrayOfD4WTPSALESDATA $ACTSALESDATA
     * @return \Axess\Dci4Wtp\D4WTPSALESDATARESULT
     */
    public function setACTSALESDATA($ACTSALESDATA)
    {
      $this->ACTSALESDATA = $ACTSALESDATA;
      return $this;
    }

    /**
     * @return D4WTPCREDITCARDDATA
     */
    public function getCTCREDITCARDDATA()
    {
      return $this->CTCREDITCARDDATA;
    }

    /**
     * @param D4WTPCREDITCARDDATA $CTCREDITCARDDATA
     * @return \Axess\Dci4Wtp\D4WTPSALESDATARESULT
     */
    public function setCTCREDITCARDDATA($CTCREDITCARDDATA)
    {
      $this->CTCREDITCARDDATA = $CTCREDITCARDDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATARESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRECEIPTID()
    {
      return $this->NRECEIPTID;
    }

    /**
     * @param float $NRECEIPTID
     * @return \Axess\Dci4Wtp\D4WTPSALESDATARESULT
     */
    public function setNRECEIPTID($NRECEIPTID)
    {
      $this->NRECEIPTID = $NRECEIPTID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPSALESDATARESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
