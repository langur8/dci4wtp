<?php

namespace Axess\Dci4Wtp;

class D4WTPPREPAIDARTICLES
{

    /**
     * @var float $NARTICLELFDNR
     */
    protected $NARTICLELFDNR = null;

    /**
     * @var float $NORDERSTATENR
     */
    protected $NORDERSTATENR = null;

    /**
     * @var float $NPICKUPBOXNR
     */
    protected $NPICKUPBOXNR = null;

    /**
     * @var string $SZCHANGED
     */
    protected $SZCHANGED = null;

    /**
     * @var string $SZCODINGDATE
     */
    protected $SZCODINGDATE = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNARTICLELFDNR()
    {
      return $this->NARTICLELFDNR;
    }

    /**
     * @param float $NARTICLELFDNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDARTICLES
     */
    public function setNARTICLELFDNR($NARTICLELFDNR)
    {
      $this->NARTICLELFDNR = $NARTICLELFDNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORDERSTATENR()
    {
      return $this->NORDERSTATENR;
    }

    /**
     * @param float $NORDERSTATENR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDARTICLES
     */
    public function setNORDERSTATENR($NORDERSTATENR)
    {
      $this->NORDERSTATENR = $NORDERSTATENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICKUPBOXNR()
    {
      return $this->NPICKUPBOXNR;
    }

    /**
     * @param float $NPICKUPBOXNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDARTICLES
     */
    public function setNPICKUPBOXNR($NPICKUPBOXNR)
    {
      $this->NPICKUPBOXNR = $NPICKUPBOXNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCHANGED()
    {
      return $this->SZCHANGED;
    }

    /**
     * @param string $SZCHANGED
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDARTICLES
     */
    public function setSZCHANGED($SZCHANGED)
    {
      $this->SZCHANGED = $SZCHANGED;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGDATE()
    {
      return $this->SZCODINGDATE;
    }

    /**
     * @param string $SZCODINGDATE
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDARTICLES
     */
    public function setSZCODINGDATE($SZCODINGDATE)
    {
      $this->SZCODINGDATE = $SZCODINGDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDARTICLES
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

}
