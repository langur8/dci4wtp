<?php

namespace Axess\Dci4Wtp;

class D4WTPLOCKERMEDIAIDS
{

    /**
     * @var float $NBLOCKNR
     */
    protected $NBLOCKNR = null;

    /**
     * @var float $NCHIPPROJECTNUMBER
     */
    protected $NCHIPPROJECTNUMBER = null;

    /**
     * @var float $NCOMAITEMNR
     */
    protected $NCOMAITEMNR = null;

    /**
     * @var float $NREGISTRATIONNR
     */
    protected $NREGISTRATIONNR = null;

    /**
     * @var float $NSEQUENCENR
     */
    protected $NSEQUENCENR = null;

    /**
     * @var string $SZITEMNAME
     */
    protected $SZITEMNAME = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    /**
     * @var string $SZSECURITYCODE
     */
    protected $SZSECURITYCODE = null;

    /**
     * @var string $SZWTPNR
     */
    protected $SZWTPNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNBLOCKNR()
    {
      return $this->NBLOCKNR;
    }

    /**
     * @param float $NBLOCKNR
     * @return \Axess\Dci4Wtp\D4WTPLOCKERMEDIAIDS
     */
    public function setNBLOCKNR($NBLOCKNR)
    {
      $this->NBLOCKNR = $NBLOCKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCHIPPROJECTNUMBER()
    {
      return $this->NCHIPPROJECTNUMBER;
    }

    /**
     * @param float $NCHIPPROJECTNUMBER
     * @return \Axess\Dci4Wtp\D4WTPLOCKERMEDIAIDS
     */
    public function setNCHIPPROJECTNUMBER($NCHIPPROJECTNUMBER)
    {
      $this->NCHIPPROJECTNUMBER = $NCHIPPROJECTNUMBER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMAITEMNR()
    {
      return $this->NCOMAITEMNR;
    }

    /**
     * @param float $NCOMAITEMNR
     * @return \Axess\Dci4Wtp\D4WTPLOCKERMEDIAIDS
     */
    public function setNCOMAITEMNR($NCOMAITEMNR)
    {
      $this->NCOMAITEMNR = $NCOMAITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNREGISTRATIONNR()
    {
      return $this->NREGISTRATIONNR;
    }

    /**
     * @param float $NREGISTRATIONNR
     * @return \Axess\Dci4Wtp\D4WTPLOCKERMEDIAIDS
     */
    public function setNREGISTRATIONNR($NREGISTRATIONNR)
    {
      $this->NREGISTRATIONNR = $NREGISTRATIONNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSEQUENCENR()
    {
      return $this->NSEQUENCENR;
    }

    /**
     * @param float $NSEQUENCENR
     * @return \Axess\Dci4Wtp\D4WTPLOCKERMEDIAIDS
     */
    public function setNSEQUENCENR($NSEQUENCENR)
    {
      $this->NSEQUENCENR = $NSEQUENCENR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZITEMNAME()
    {
      return $this->SZITEMNAME;
    }

    /**
     * @param string $SZITEMNAME
     * @return \Axess\Dci4Wtp\D4WTPLOCKERMEDIAIDS
     */
    public function setSZITEMNAME($SZITEMNAME)
    {
      $this->SZITEMNAME = $SZITEMNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPLOCKERMEDIAIDS
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSECURITYCODE()
    {
      return $this->SZSECURITYCODE;
    }

    /**
     * @param string $SZSECURITYCODE
     * @return \Axess\Dci4Wtp\D4WTPLOCKERMEDIAIDS
     */
    public function setSZSECURITYCODE($SZSECURITYCODE)
    {
      $this->SZSECURITYCODE = $SZSECURITYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNR()
    {
      return $this->SZWTPNR;
    }

    /**
     * @param string $SZWTPNR
     * @return \Axess\Dci4Wtp\D4WTPLOCKERMEDIAIDS
     */
    public function setSZWTPNR($SZWTPNR)
    {
      $this->SZWTPNR = $SZWTPNR;
      return $this;
    }

}
