<?php

namespace Axess\Dci4Wtp;

class addAdditionalLicensePlateResponse
{

    /**
     * @var D4WTPADDADLICENSEPLATERESULT $addAdditionalLicensePlateResult
     */
    protected $addAdditionalLicensePlateResult = null;

    /**
     * @param D4WTPADDADLICENSEPLATERESULT $addAdditionalLicensePlateResult
     */
    public function __construct($addAdditionalLicensePlateResult)
    {
      $this->addAdditionalLicensePlateResult = $addAdditionalLicensePlateResult;
    }

    /**
     * @return D4WTPADDADLICENSEPLATERESULT
     */
    public function getAddAdditionalLicensePlateResult()
    {
      return $this->addAdditionalLicensePlateResult;
    }

    /**
     * @param D4WTPADDADLICENSEPLATERESULT $addAdditionalLicensePlateResult
     * @return \Axess\Dci4Wtp\addAdditionalLicensePlateResponse
     */
    public function setAddAdditionalLicensePlateResult($addAdditionalLicensePlateResult)
    {
      $this->addAdditionalLicensePlateResult = $addAdditionalLicensePlateResult;
      return $this;
    }

}
