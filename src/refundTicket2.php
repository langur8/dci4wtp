<?php

namespace Axess\Dci4Wtp;

class refundTicket2
{

    /**
     * @var D4WTPREFUNDTICKET2REQUEST $i_ctRefundTicket2Req
     */
    protected $i_ctRefundTicket2Req = null;

    /**
     * @param D4WTPREFUNDTICKET2REQUEST $i_ctRefundTicket2Req
     */
    public function __construct($i_ctRefundTicket2Req)
    {
      $this->i_ctRefundTicket2Req = $i_ctRefundTicket2Req;
    }

    /**
     * @return D4WTPREFUNDTICKET2REQUEST
     */
    public function getI_ctRefundTicket2Req()
    {
      return $this->i_ctRefundTicket2Req;
    }

    /**
     * @param D4WTPREFUNDTICKET2REQUEST $i_ctRefundTicket2Req
     * @return \Axess\Dci4Wtp\refundTicket2
     */
    public function setI_ctRefundTicket2Req($i_ctRefundTicket2Req)
    {
      $this->i_ctRefundTicket2Req = $i_ctRefundTicket2Req;
      return $this;
    }

}
