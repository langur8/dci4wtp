<?php

namespace Axess\Dci4Wtp;

class getPersonData3Response
{

    /**
     * @var D4WTPGETPERSON3RESULT $getPersonData3Result
     */
    protected $getPersonData3Result = null;

    /**
     * @param D4WTPGETPERSON3RESULT $getPersonData3Result
     */
    public function __construct($getPersonData3Result)
    {
      $this->getPersonData3Result = $getPersonData3Result;
    }

    /**
     * @return D4WTPGETPERSON3RESULT
     */
    public function getGetPersonData3Result()
    {
      return $this->getPersonData3Result;
    }

    /**
     * @param D4WTPGETPERSON3RESULT $getPersonData3Result
     * @return \Axess\Dci4Wtp\getPersonData3Response
     */
    public function setGetPersonData3Result($getPersonData3Result)
    {
      $this->getPersonData3Result = $getPersonData3Result;
      return $this;
    }

}
