<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST9
{

    /**
     * @var ArrayOfD4WTPTARIFFLISTDAY9 $ACTTARIFFLISTDAY9
     */
    protected $ACTTARIFFLISTDAY9 = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLISTDAY9
     */
    public function getACTTARIFFLISTDAY9()
    {
      return $this->ACTTARIFFLISTDAY9;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLISTDAY9 $ACTTARIFFLISTDAY9
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST9
     */
    public function setACTTARIFFLISTDAY9($ACTTARIFFLISTDAY9)
    {
      $this->ACTTARIFFLISTDAY9 = $ACTTARIFFLISTDAY9;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST9
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
