<?php

namespace Axess\Dci4Wtp;

class D4WTPISLPCHECKINOUTRESULT
{

    /**
     * @var float $BISCHECKINOUT
     */
    protected $BISCHECKINOUT = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZTIMESTAMP
     */
    protected $SZTIMESTAMP = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISCHECKINOUT()
    {
      return $this->BISCHECKINOUT;
    }

    /**
     * @param float $BISCHECKINOUT
     * @return \Axess\Dci4Wtp\D4WTPISLPCHECKINOUTRESULT
     */
    public function setBISCHECKINOUT($BISCHECKINOUT)
    {
      $this->BISCHECKINOUT = $BISCHECKINOUT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPISLPCHECKINOUTRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPISLPCHECKINOUTRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTIMESTAMP()
    {
      return $this->SZTIMESTAMP;
    }

    /**
     * @param string $SZTIMESTAMP
     * @return \Axess\Dci4Wtp\D4WTPISLPCHECKINOUTRESULT
     */
    public function setSZTIMESTAMP($SZTIMESTAMP)
    {
      $this->SZTIMESTAMP = $SZTIMESTAMP;
      return $this;
    }

}
