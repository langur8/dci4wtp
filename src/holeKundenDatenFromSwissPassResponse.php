<?php

namespace Axess\Dci4Wtp;

class holeKundenDatenFromSwissPassResponse
{

    /**
     * @var OBI4PSPHOLEKUNDENDATENRESULT $holeKundenDatenFromSwissPassResult
     */
    protected $holeKundenDatenFromSwissPassResult = null;

    /**
     * @param OBI4PSPHOLEKUNDENDATENRESULT $holeKundenDatenFromSwissPassResult
     */
    public function __construct($holeKundenDatenFromSwissPassResult)
    {
      $this->holeKundenDatenFromSwissPassResult = $holeKundenDatenFromSwissPassResult;
    }

    /**
     * @return OBI4PSPHOLEKUNDENDATENRESULT
     */
    public function getHoleKundenDatenFromSwissPassResult()
    {
      return $this->holeKundenDatenFromSwissPassResult;
    }

    /**
     * @param OBI4PSPHOLEKUNDENDATENRESULT $holeKundenDatenFromSwissPassResult
     * @return \Axess\Dci4Wtp\holeKundenDatenFromSwissPassResponse
     */
    public function setHoleKundenDatenFromSwissPassResult($holeKundenDatenFromSwissPassResult)
    {
      $this->holeKundenDatenFromSwissPassResult = $holeKundenDatenFromSwissPassResult;
      return $this;
    }

}
