<?php

namespace Axess\Dci4Wtp;

class saveVoucherIDWTPProdsatz
{

    /**
     * @var D4WTPSAVEVOUCHER $i_SaveVoucherIDWTPProdsatz
     */
    protected $i_SaveVoucherIDWTPProdsatz = null;

    /**
     * @param D4WTPSAVEVOUCHER $i_SaveVoucherIDWTPProdsatz
     */
    public function __construct($i_SaveVoucherIDWTPProdsatz)
    {
      $this->i_SaveVoucherIDWTPProdsatz = $i_SaveVoucherIDWTPProdsatz;
    }

    /**
     * @return D4WTPSAVEVOUCHER
     */
    public function getI_SaveVoucherIDWTPProdsatz()
    {
      return $this->i_SaveVoucherIDWTPProdsatz;
    }

    /**
     * @param D4WTPSAVEVOUCHER $i_SaveVoucherIDWTPProdsatz
     * @return \Axess\Dci4Wtp\saveVoucherIDWTPProdsatz
     */
    public function setI_SaveVoucherIDWTPProdsatz($i_SaveVoucherIDWTPProdsatz)
    {
      $this->i_SaveVoucherIDWTPProdsatz = $i_SaveVoucherIDWTPProdsatz;
      return $this;
    }

}
