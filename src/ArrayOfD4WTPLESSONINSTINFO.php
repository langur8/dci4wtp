<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPLESSONINSTINFO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPLESSONINSTINFO[] $D4WTPLESSONINSTINFO
     */
    protected $D4WTPLESSONINSTINFO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPLESSONINSTINFO[]
     */
    public function getD4WTPLESSONINSTINFO()
    {
      return $this->D4WTPLESSONINSTINFO;
    }

    /**
     * @param D4WTPLESSONINSTINFO[] $D4WTPLESSONINSTINFO
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPLESSONINSTINFO
     */
    public function setD4WTPLESSONINSTINFO(array $D4WTPLESSONINSTINFO = null)
    {
      $this->D4WTPLESSONINSTINFO = $D4WTPLESSONINSTINFO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPLESSONINSTINFO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPLESSONINSTINFO
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPLESSONINSTINFO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPLESSONINSTINFO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPLESSONINSTINFO[] = $value;
      } else {
        $this->D4WTPLESSONINSTINFO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPLESSONINSTINFO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPLESSONINSTINFO Return the current element
     */
    public function current()
    {
      return current($this->D4WTPLESSONINSTINFO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPLESSONINSTINFO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPLESSONINSTINFO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPLESSONINSTINFO);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPLESSONINSTINFO Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPLESSONINSTINFO);
    }

}
