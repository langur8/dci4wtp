<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPROW implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPROW[] $D4WTPROW
     */
    protected $D4WTPROW = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPROW[]
     */
    public function getD4WTPROW()
    {
      return $this->D4WTPROW;
    }

    /**
     * @param D4WTPROW[] $D4WTPROW
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPROW
     */
    public function setD4WTPROW(array $D4WTPROW = null)
    {
      $this->D4WTPROW = $D4WTPROW;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPROW[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPROW
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPROW[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPROW $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPROW[] = $value;
      } else {
        $this->D4WTPROW[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPROW[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPROW Return the current element
     */
    public function current()
    {
      return current($this->D4WTPROW);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPROW);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPROW);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPROW);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPROW Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPROW);
    }

}
