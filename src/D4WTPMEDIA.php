<?php

namespace Axess\Dci4Wtp;

class D4WTPMEDIA
{

    /**
     * @var float $BDELETE
     */
    protected $BDELETE = null;

    /**
     * @var D4WTPPERSONTRIPLE $CTPERSONTRIPLE
     */
    protected $CTPERSONTRIPLE = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    /**
     * @var string $SZWTPNR64
     */
    protected $SZWTPNR64 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBDELETE()
    {
      return $this->BDELETE;
    }

    /**
     * @param float $BDELETE
     * @return \Axess\Dci4Wtp\D4WTPMEDIA
     */
    public function setBDELETE($BDELETE)
    {
      $this->BDELETE = $BDELETE;
      return $this;
    }

    /**
     * @return D4WTPPERSONTRIPLE
     */
    public function getCTPERSONTRIPLE()
    {
      return $this->CTPERSONTRIPLE;
    }

    /**
     * @param D4WTPPERSONTRIPLE $CTPERSONTRIPLE
     * @return \Axess\Dci4Wtp\D4WTPMEDIA
     */
    public function setCTPERSONTRIPLE($CTPERSONTRIPLE)
    {
      $this->CTPERSONTRIPLE = $CTPERSONTRIPLE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPMEDIA
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNR64()
    {
      return $this->SZWTPNR64;
    }

    /**
     * @param string $SZWTPNR64
     * @return \Axess\Dci4Wtp\D4WTPMEDIA
     */
    public function setSZWTPNR64($SZWTPNR64)
    {
      $this->SZWTPNR64 = $SZWTPNR64;
      return $this;
    }

}
