<?php

namespace Axess\Dci4Wtp;

class getGroupSubContingent3
{

    /**
     * @var D4WTPGETGRPSUBCONTINGENTREQ2 $i_ctgetGroupSubContingentReq
     */
    protected $i_ctgetGroupSubContingentReq = null;

    /**
     * @param D4WTPGETGRPSUBCONTINGENTREQ2 $i_ctgetGroupSubContingentReq
     */
    public function __construct($i_ctgetGroupSubContingentReq)
    {
      $this->i_ctgetGroupSubContingentReq = $i_ctgetGroupSubContingentReq;
    }

    /**
     * @return D4WTPGETGRPSUBCONTINGENTREQ2
     */
    public function getI_ctgetGroupSubContingentReq()
    {
      return $this->i_ctgetGroupSubContingentReq;
    }

    /**
     * @param D4WTPGETGRPSUBCONTINGENTREQ2 $i_ctgetGroupSubContingentReq
     * @return \Axess\Dci4Wtp\getGroupSubContingent3
     */
    public function setI_ctgetGroupSubContingentReq($i_ctgetGroupSubContingentReq)
    {
      $this->i_ctgetGroupSubContingentReq = $i_ctgetGroupSubContingentReq;
      return $this;
    }

}
