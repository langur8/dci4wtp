<?php

namespace Axess\Dci4Wtp;

class D4WTPGETROUTESSTATIONSRESULT
{

    /**
     * @var ArrayOfD4WTPROUTESSTATIONS $ACTROUTESSTATIONS
     */
    protected $ACTROUTESSTATIONS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPROUTESSTATIONS
     */
    public function getACTROUTESSTATIONS()
    {
      return $this->ACTROUTESSTATIONS;
    }

    /**
     * @param ArrayOfD4WTPROUTESSTATIONS $ACTROUTESSTATIONS
     * @return \Axess\Dci4Wtp\D4WTPGETROUTESSTATIONSRESULT
     */
    public function setACTROUTESSTATIONS($ACTROUTESSTATIONS)
    {
      $this->ACTROUTESSTATIONS = $ACTROUTESSTATIONS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETROUTESSTATIONSRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETROUTESSTATIONSRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
