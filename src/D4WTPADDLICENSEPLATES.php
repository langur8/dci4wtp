<?php

namespace Axess\Dci4Wtp;

class D4WTPADDLICENSEPLATES
{

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var float $NNEWLFDNR
     */
    protected $NNEWLFDNR = null;

    /**
     * @var string $SZINFO
     */
    protected $SZINFO = null;

    /**
     * @var string $SZNEWLICENSEPLATE
     */
    protected $SZNEWLICENSEPLATE = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATES
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNEWLFDNR()
    {
      return $this->NNEWLFDNR;
    }

    /**
     * @param float $NNEWLFDNR
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATES
     */
    public function setNNEWLFDNR($NNEWLFDNR)
    {
      $this->NNEWLFDNR = $NNEWLFDNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZINFO()
    {
      return $this->SZINFO;
    }

    /**
     * @param string $SZINFO
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATES
     */
    public function setSZINFO($SZINFO)
    {
      $this->SZINFO = $SZINFO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNEWLICENSEPLATE()
    {
      return $this->SZNEWLICENSEPLATE;
    }

    /**
     * @param string $SZNEWLICENSEPLATE
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATES
     */
    public function setSZNEWLICENSEPLATE($SZNEWLICENSEPLATE)
    {
      $this->SZNEWLICENSEPLATE = $SZNEWLICENSEPLATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATES
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATES
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
