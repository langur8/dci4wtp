<?php

namespace Axess\Dci4Wtp;

class cancelTicket2Response
{

    /**
     * @var D4WTPCANCELTICKET2RESULT $cancelTicket2Result
     */
    protected $cancelTicket2Result = null;

    /**
     * @param D4WTPCANCELTICKET2RESULT $cancelTicket2Result
     */
    public function __construct($cancelTicket2Result)
    {
      $this->cancelTicket2Result = $cancelTicket2Result;
    }

    /**
     * @return D4WTPCANCELTICKET2RESULT
     */
    public function getCancelTicket2Result()
    {
      return $this->cancelTicket2Result;
    }

    /**
     * @param D4WTPCANCELTICKET2RESULT $cancelTicket2Result
     * @return \Axess\Dci4Wtp\cancelTicket2Response
     */
    public function setCancelTicket2Result($cancelTicket2Result)
    {
      $this->cancelTicket2Result = $cancelTicket2Result;
      return $this;
    }

}
