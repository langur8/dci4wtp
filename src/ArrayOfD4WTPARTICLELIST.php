<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPARTICLELIST implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPARTICLELIST[] $D4WTPARTICLELIST
     */
    protected $D4WTPARTICLELIST = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPARTICLELIST[]
     */
    public function getD4WTPARTICLELIST()
    {
      return $this->D4WTPARTICLELIST;
    }

    /**
     * @param D4WTPARTICLELIST[] $D4WTPARTICLELIST
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPARTICLELIST
     */
    public function setD4WTPARTICLELIST(array $D4WTPARTICLELIST = null)
    {
      $this->D4WTPARTICLELIST = $D4WTPARTICLELIST;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPARTICLELIST[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPARTICLELIST
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPARTICLELIST[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPARTICLELIST $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPARTICLELIST[] = $value;
      } else {
        $this->D4WTPARTICLELIST[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPARTICLELIST[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPARTICLELIST Return the current element
     */
    public function current()
    {
      return current($this->D4WTPARTICLELIST);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPARTICLELIST);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPARTICLELIST);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPARTICLELIST);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPARTICLELIST Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPARTICLELIST);
    }

}
