<?php

namespace Axess\Dci4Wtp;

class issueTicket5Response
{

    /**
     * @var D4WTPISSUETICKET5RESULT $issueTicket5Result
     */
    protected $issueTicket5Result = null;

    /**
     * @param D4WTPISSUETICKET5RESULT $issueTicket5Result
     */
    public function __construct($issueTicket5Result)
    {
      $this->issueTicket5Result = $issueTicket5Result;
    }

    /**
     * @return D4WTPISSUETICKET5RESULT
     */
    public function getIssueTicket5Result()
    {
      return $this->issueTicket5Result;
    }

    /**
     * @param D4WTPISSUETICKET5RESULT $issueTicket5Result
     * @return \Axess\Dci4Wtp\issueTicket5Response
     */
    public function setIssueTicket5Result($issueTicket5Result)
    {
      $this->issueTicket5Result = $issueTicket5Result;
      return $this;
    }

}
