<?php

namespace Axess\Dci4Wtp;

class D4WTPUSERROLES
{

    /**
     * @var float $NROLEID
     */
    protected $NROLEID = null;

    /**
     * @var string $SZROLENAME
     */
    protected $SZROLENAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNROLEID()
    {
      return $this->NROLEID;
    }

    /**
     * @param float $NROLEID
     * @return \Axess\Dci4Wtp\D4WTPUSERROLES
     */
    public function setNROLEID($NROLEID)
    {
      $this->NROLEID = $NROLEID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZROLENAME()
    {
      return $this->SZROLENAME;
    }

    /**
     * @param string $SZROLENAME
     * @return \Axess\Dci4Wtp\D4WTPUSERROLES
     */
    public function setSZROLENAME($SZROLENAME)
    {
      $this->SZROLENAME = $SZROLENAME;
      return $this;
    }

}
