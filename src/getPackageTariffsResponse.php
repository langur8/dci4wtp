<?php

namespace Axess\Dci4Wtp;

class getPackageTariffsResponse
{

    /**
     * @var D4WTPPACKAGETARIFFSRESULT $getPackageTariffsResult
     */
    protected $getPackageTariffsResult = null;

    /**
     * @param D4WTPPACKAGETARIFFSRESULT $getPackageTariffsResult
     */
    public function __construct($getPackageTariffsResult)
    {
      $this->getPackageTariffsResult = $getPackageTariffsResult;
    }

    /**
     * @return D4WTPPACKAGETARIFFSRESULT
     */
    public function getGetPackageTariffsResult()
    {
      return $this->getPackageTariffsResult;
    }

    /**
     * @param D4WTPPACKAGETARIFFSRESULT $getPackageTariffsResult
     * @return \Axess\Dci4Wtp\getPackageTariffsResponse
     */
    public function setGetPackageTariffsResult($getPackageTariffsResult)
    {
      $this->getPackageTariffsResult = $getPackageTariffsResult;
      return $this;
    }

}
