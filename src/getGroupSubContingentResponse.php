<?php

namespace Axess\Dci4Wtp;

class getGroupSubContingentResponse
{

    /**
     * @var D4WTPGETGRPSUBCONTINGENTRES $getGroupSubContingentResult
     */
    protected $getGroupSubContingentResult = null;

    /**
     * @param D4WTPGETGRPSUBCONTINGENTRES $getGroupSubContingentResult
     */
    public function __construct($getGroupSubContingentResult)
    {
      $this->getGroupSubContingentResult = $getGroupSubContingentResult;
    }

    /**
     * @return D4WTPGETGRPSUBCONTINGENTRES
     */
    public function getGetGroupSubContingentResult()
    {
      return $this->getGroupSubContingentResult;
    }

    /**
     * @param D4WTPGETGRPSUBCONTINGENTRES $getGroupSubContingentResult
     * @return \Axess\Dci4Wtp\getGroupSubContingentResponse
     */
    public function setGetGroupSubContingentResult($getGroupSubContingentResult)
    {
      $this->getGroupSubContingentResult = $getGroupSubContingentResult;
      return $this;
    }

}
