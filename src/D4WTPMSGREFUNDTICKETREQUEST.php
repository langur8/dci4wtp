<?php

namespace Axess\Dci4Wtp;

class D4WTPMSGREFUNDTICKETREQUEST
{

    /**
     * @var ArrayOfD4WTPTRANSACTION $ACTTRANSACTION
     */
    protected $ACTTRANSACTION = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZMESSAGETYPE
     */
    protected $SZMESSAGETYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTRANSACTION
     */
    public function getACTTRANSACTION()
    {
      return $this->ACTTRANSACTION;
    }

    /**
     * @param ArrayOfD4WTPTRANSACTION $ACTTRANSACTION
     * @return \Axess\Dci4Wtp\D4WTPMSGREFUNDTICKETREQUEST
     */
    public function setACTTRANSACTION($ACTTRANSACTION)
    {
      $this->ACTTRANSACTION = $ACTTRANSACTION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPMSGREFUNDTICKETREQUEST
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPMSGREFUNDTICKETREQUEST
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPMSGREFUNDTICKETREQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPMSGREFUNDTICKETREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMESSAGETYPE()
    {
      return $this->SZMESSAGETYPE;
    }

    /**
     * @param string $SZMESSAGETYPE
     * @return \Axess\Dci4Wtp\D4WTPMSGREFUNDTICKETREQUEST
     */
    public function setSZMESSAGETYPE($SZMESSAGETYPE)
    {
      $this->SZMESSAGETYPE = $SZMESSAGETYPE;
      return $this;
    }

}
