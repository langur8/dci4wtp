<?php

namespace Axess\Dci4Wtp;

class postLeistungsInfoToSwissPassResponse
{

    /**
     * @var D4WTPRESULT $postLeistungsInfoToSwissPassResult
     */
    protected $postLeistungsInfoToSwissPassResult = null;

    /**
     * @param D4WTPRESULT $postLeistungsInfoToSwissPassResult
     */
    public function __construct($postLeistungsInfoToSwissPassResult)
    {
      $this->postLeistungsInfoToSwissPassResult = $postLeistungsInfoToSwissPassResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getPostLeistungsInfoToSwissPassResult()
    {
      return $this->postLeistungsInfoToSwissPassResult;
    }

    /**
     * @param D4WTPRESULT $postLeistungsInfoToSwissPassResult
     * @return \Axess\Dci4Wtp\postLeistungsInfoToSwissPassResponse
     */
    public function setPostLeistungsInfoToSwissPassResult($postLeistungsInfoToSwissPassResult)
    {
      $this->postLeistungsInfoToSwissPassResult = $postLeistungsInfoToSwissPassResult;
      return $this;
    }

}
