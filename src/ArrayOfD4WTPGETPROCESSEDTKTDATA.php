<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPGETPROCESSEDTKTDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPGETPROCESSEDTKTDATA[] $D4WTPGETPROCESSEDTKTDATA
     */
    protected $D4WTPGETPROCESSEDTKTDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPGETPROCESSEDTKTDATA[]
     */
    public function getD4WTPGETPROCESSEDTKTDATA()
    {
      return $this->D4WTPGETPROCESSEDTKTDATA;
    }

    /**
     * @param D4WTPGETPROCESSEDTKTDATA[] $D4WTPGETPROCESSEDTKTDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPGETPROCESSEDTKTDATA
     */
    public function setD4WTPGETPROCESSEDTKTDATA(array $D4WTPGETPROCESSEDTKTDATA = null)
    {
      $this->D4WTPGETPROCESSEDTKTDATA = $D4WTPGETPROCESSEDTKTDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPGETPROCESSEDTKTDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPGETPROCESSEDTKTDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPGETPROCESSEDTKTDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPGETPROCESSEDTKTDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPGETPROCESSEDTKTDATA[] = $value;
      } else {
        $this->D4WTPGETPROCESSEDTKTDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPGETPROCESSEDTKTDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPGETPROCESSEDTKTDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPGETPROCESSEDTKTDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPGETPROCESSEDTKTDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPGETPROCESSEDTKTDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPGETPROCESSEDTKTDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPGETPROCESSEDTKTDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPGETPROCESSEDTKTDATA);
    }

}
