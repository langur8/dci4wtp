<?php

namespace Axess\Dci4Wtp;

class msgIssueTicket6Response
{

    /**
     * @var D4WTPMSGISSUETICKETRESULT3 $msgIssueTicket6Result
     */
    protected $msgIssueTicket6Result = null;

    /**
     * @param D4WTPMSGISSUETICKETRESULT3 $msgIssueTicket6Result
     */
    public function __construct($msgIssueTicket6Result)
    {
      $this->msgIssueTicket6Result = $msgIssueTicket6Result;
    }

    /**
     * @return D4WTPMSGISSUETICKETRESULT3
     */
    public function getMsgIssueTicket6Result()
    {
      return $this->msgIssueTicket6Result;
    }

    /**
     * @param D4WTPMSGISSUETICKETRESULT3 $msgIssueTicket6Result
     * @return \Axess\Dci4Wtp\msgIssueTicket6Response
     */
    public function setMsgIssueTicket6Result($msgIssueTicket6Result)
    {
      $this->msgIssueTicket6Result = $msgIssueTicket6Result;
      return $this;
    }

}
