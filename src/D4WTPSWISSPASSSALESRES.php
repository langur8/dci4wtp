<?php

namespace Axess\Dci4Wtp;

class D4WTPSWISSPASSSALESRES
{

    /**
     * @var ArrayOfSWISSTICKETSALE $ACTTICKETS
     */
    protected $ACTTICKETS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfSWISSTICKETSALE
     */
    public function getACTTICKETS()
    {
      return $this->ACTTICKETS;
    }

    /**
     * @param ArrayOfSWISSTICKETSALE $ACTTICKETS
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSSALESRES
     */
    public function setACTTICKETS($ACTTICKETS)
    {
      $this->ACTTICKETS = $ACTTICKETS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSSALESRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSSALESRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
