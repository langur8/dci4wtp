<?php

namespace Axess\Dci4Wtp;

class readAuthorizationResponse
{

    /**
     * @var D4WTPREADTICKETRESULT $readAuthorizationResult
     */
    protected $readAuthorizationResult = null;

    /**
     * @param D4WTPREADTICKETRESULT $readAuthorizationResult
     */
    public function __construct($readAuthorizationResult)
    {
      $this->readAuthorizationResult = $readAuthorizationResult;
    }

    /**
     * @return D4WTPREADTICKETRESULT
     */
    public function getReadAuthorizationResult()
    {
      return $this->readAuthorizationResult;
    }

    /**
     * @param D4WTPREADTICKETRESULT $readAuthorizationResult
     * @return \Axess\Dci4Wtp\readAuthorizationResponse
     */
    public function setReadAuthorizationResult($readAuthorizationResult)
    {
      $this->readAuthorizationResult = $readAuthorizationResult;
      return $this;
    }

}
