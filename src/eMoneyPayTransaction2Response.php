<?php

namespace Axess\Dci4Wtp;

class eMoneyPayTransaction2Response
{

    /**
     * @var D4WTPEMONEYPAYTRANS2RES $eMoneyPayTransaction2Result
     */
    protected $eMoneyPayTransaction2Result = null;

    /**
     * @param D4WTPEMONEYPAYTRANS2RES $eMoneyPayTransaction2Result
     */
    public function __construct($eMoneyPayTransaction2Result)
    {
      $this->eMoneyPayTransaction2Result = $eMoneyPayTransaction2Result;
    }

    /**
     * @return D4WTPEMONEYPAYTRANS2RES
     */
    public function getEMoneyPayTransaction2Result()
    {
      return $this->eMoneyPayTransaction2Result;
    }

    /**
     * @param D4WTPEMONEYPAYTRANS2RES $eMoneyPayTransaction2Result
     * @return \Axess\Dci4Wtp\eMoneyPayTransaction2Response
     */
    public function setEMoneyPayTransaction2Result($eMoneyPayTransaction2Result)
    {
      $this->eMoneyPayTransaction2Result = $eMoneyPayTransaction2Result;
      return $this;
    }

}
