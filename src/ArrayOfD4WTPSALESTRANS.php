<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSALESTRANS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSALESTRANS[] $D4WTPSALESTRANS
     */
    protected $D4WTPSALESTRANS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSALESTRANS[]
     */
    public function getD4WTPSALESTRANS()
    {
      return $this->D4WTPSALESTRANS;
    }

    /**
     * @param D4WTPSALESTRANS[] $D4WTPSALESTRANS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSALESTRANS
     */
    public function setD4WTPSALESTRANS(array $D4WTPSALESTRANS = null)
    {
      $this->D4WTPSALESTRANS = $D4WTPSALESTRANS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSALESTRANS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSALESTRANS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSALESTRANS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSALESTRANS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSALESTRANS[] = $value;
      } else {
        $this->D4WTPSALESTRANS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSALESTRANS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSALESTRANS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSALESTRANS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSALESTRANS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSALESTRANS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSALESTRANS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSALESTRANS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSALESTRANS);
    }

}
