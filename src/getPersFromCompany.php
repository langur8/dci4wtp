<?php

namespace Axess\Dci4Wtp;

class getPersFromCompany
{

    /**
     * @var D4WTPGETPERSFROMCOMPANYREQ $i_ctGetPersFromCompanyReq
     */
    protected $i_ctGetPersFromCompanyReq = null;

    /**
     * @param D4WTPGETPERSFROMCOMPANYREQ $i_ctGetPersFromCompanyReq
     */
    public function __construct($i_ctGetPersFromCompanyReq)
    {
      $this->i_ctGetPersFromCompanyReq = $i_ctGetPersFromCompanyReq;
    }

    /**
     * @return D4WTPGETPERSFROMCOMPANYREQ
     */
    public function getI_ctGetPersFromCompanyReq()
    {
      return $this->i_ctGetPersFromCompanyReq;
    }

    /**
     * @param D4WTPGETPERSFROMCOMPANYREQ $i_ctGetPersFromCompanyReq
     * @return \Axess\Dci4Wtp\getPersFromCompany
     */
    public function setI_ctGetPersFromCompanyReq($i_ctGetPersFromCompanyReq)
    {
      $this->i_ctGetPersFromCompanyReq = $i_ctGetPersFromCompanyReq;
      return $this;
    }

}
