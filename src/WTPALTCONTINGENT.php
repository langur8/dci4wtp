<?php

namespace Axess\Dci4Wtp;

class WTPALTCONTINGENT
{

    /**
     * @var float $NALTERNALTIVECONTINGENTNO
     */
    protected $NALTERNALTIVECONTINGENTNO = null;

    /**
     * @var float $NCONTINGENTNO
     */
    protected $NCONTINGENTNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNALTERNALTIVECONTINGENTNO()
    {
      return $this->NALTERNALTIVECONTINGENTNO;
    }

    /**
     * @param float $NALTERNALTIVECONTINGENTNO
     * @return \Axess\Dci4Wtp\WTPALTCONTINGENT
     */
    public function setNALTERNALTIVECONTINGENTNO($NALTERNALTIVECONTINGENTNO)
    {
      $this->NALTERNALTIVECONTINGENTNO = $NALTERNALTIVECONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNO()
    {
      return $this->NCONTINGENTNO;
    }

    /**
     * @param float $NCONTINGENTNO
     * @return \Axess\Dci4Wtp\WTPALTCONTINGENT
     */
    public function setNCONTINGENTNO($NCONTINGENTNO)
    {
      $this->NCONTINGENTNO = $NCONTINGENTNO;
      return $this;
    }

}
