<?php

namespace Axess\Dci4Wtp;

class decodeBarcodeResponse
{

    /**
     * @var D4WTPDECODEBARCODECRESULT $decodeBarcodeResult
     */
    protected $decodeBarcodeResult = null;

    /**
     * @param D4WTPDECODEBARCODECRESULT $decodeBarcodeResult
     */
    public function __construct($decodeBarcodeResult)
    {
      $this->decodeBarcodeResult = $decodeBarcodeResult;
    }

    /**
     * @return D4WTPDECODEBARCODECRESULT
     */
    public function getDecodeBarcodeResult()
    {
      return $this->decodeBarcodeResult;
    }

    /**
     * @param D4WTPDECODEBARCODECRESULT $decodeBarcodeResult
     * @return \Axess\Dci4Wtp\decodeBarcodeResponse
     */
    public function setDecodeBarcodeResult($decodeBarcodeResult)
    {
      $this->decodeBarcodeResult = $decodeBarcodeResult;
      return $this;
    }

}
