<?php

namespace Axess\Dci4Wtp;

class getRoutesStations
{

    /**
     * @var D4WTPGETROUTESTATIONSREQUEST $i_ctGetRouteStationsReq
     */
    protected $i_ctGetRouteStationsReq = null;

    /**
     * @param D4WTPGETROUTESTATIONSREQUEST $i_ctGetRouteStationsReq
     */
    public function __construct($i_ctGetRouteStationsReq)
    {
      $this->i_ctGetRouteStationsReq = $i_ctGetRouteStationsReq;
    }

    /**
     * @return D4WTPGETROUTESTATIONSREQUEST
     */
    public function getI_ctGetRouteStationsReq()
    {
      return $this->i_ctGetRouteStationsReq;
    }

    /**
     * @param D4WTPGETROUTESTATIONSREQUEST $i_ctGetRouteStationsReq
     * @return \Axess\Dci4Wtp\getRoutesStations
     */
    public function setI_ctGetRouteStationsReq($i_ctGetRouteStationsReq)
    {
      $this->i_ctGetRouteStationsReq = $i_ctGetRouteStationsReq;
      return $this;
    }

}
