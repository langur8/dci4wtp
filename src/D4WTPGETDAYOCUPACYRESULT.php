<?php

namespace Axess\Dci4Wtp;

class D4WTPGETDAYOCUPACYRESULT
{

    /**
     * @var ArrayOfD4WTPDAYOCCUPACY $DAYOCCUPACY
     */
    protected $DAYOCCUPACY = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPDAYOCCUPACY
     */
    public function getDAYOCCUPACY()
    {
      return $this->DAYOCCUPACY;
    }

    /**
     * @param ArrayOfD4WTPDAYOCCUPACY $DAYOCCUPACY
     * @return \Axess\Dci4Wtp\D4WTPGETDAYOCUPACYRESULT
     */
    public function setDAYOCCUPACY($DAYOCCUPACY)
    {
      $this->DAYOCCUPACY = $DAYOCCUPACY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETDAYOCUPACYRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETDAYOCUPACYRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
