<?php

namespace Axess\Dci4Wtp;

class openCashierShift
{

    /**
     * @var D4WTPCASHIERSHIFTREQUEST $i_ctCashierShiftReq
     */
    protected $i_ctCashierShiftReq = null;

    /**
     * @param D4WTPCASHIERSHIFTREQUEST $i_ctCashierShiftReq
     */
    public function __construct($i_ctCashierShiftReq)
    {
      $this->i_ctCashierShiftReq = $i_ctCashierShiftReq;
    }

    /**
     * @return D4WTPCASHIERSHIFTREQUEST
     */
    public function getI_ctCashierShiftReq()
    {
      return $this->i_ctCashierShiftReq;
    }

    /**
     * @param D4WTPCASHIERSHIFTREQUEST $i_ctCashierShiftReq
     * @return \Axess\Dci4Wtp\openCashierShift
     */
    public function setI_ctCashierShiftReq($i_ctCashierShiftReq)
    {
      $this->i_ctCashierShiftReq = $i_ctCashierShiftReq;
      return $this;
    }

}
