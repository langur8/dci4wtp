<?php

namespace Axess\Dci4Wtp;

class D4WTPSHOPCARTPOSDATA2
{

    /**
     * @var ArrayOfD4WTPSHOPCARTPOSDETDATA2 $ACTSHOPCARTPOSDETDATA
     */
    protected $ACTSHOPCARTPOSDETDATA = null;

    /**
     * @var ArrayOfD4WTPSHOPCSUBCONTINGENT2 $ACTSHOPCSUBCONTINGENT
     */
    protected $ACTSHOPCSUBCONTINGENT = null;

    /**
     * @var float $BPRODUCED
     */
    protected $BPRODUCED = null;

    /**
     * @var float $FUNITPRICE
     */
    protected $FUNITPRICE = null;

    /**
     * @var float $FWAREAMOUNT
     */
    protected $FWAREAMOUNT = null;

    /**
     * @var float $FWARETARIFF
     */
    protected $FWARETARIFF = null;

    /**
     * @var float $NALLDISCOUNTNO
     */
    protected $NALLDISCOUNTNO = null;

    /**
     * @var float $NALLDISCOUNTSTAFNO
     */
    protected $NALLDISCOUNTSTAFNO = null;

    /**
     * @var float $NARCNO
     */
    protected $NARCNO = null;

    /**
     * @var float $NARCPROJNO
     */
    protected $NARCPROJNO = null;

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NCURRSEASONNO
     */
    protected $NCURRSEASONNO = null;

    /**
     * @var float $NCUSTCARDTYPENO
     */
    protected $NCUSTCARDTYPENO = null;

    /**
     * @var float $NDATACARRIERTYPENO
     */
    protected $NDATACARRIERTYPENO = null;

    /**
     * @var float $NITEMLOCATIONNO
     */
    protected $NITEMLOCATIONNO = null;

    /**
     * @var float $NPACKAGENO
     */
    protected $NPACKAGENO = null;

    /**
     * @var float $NPACKPOSITION
     */
    protected $NPACKPOSITION = null;

    /**
     * @var float $NPERSTYPENO
     */
    protected $NPERSTYPENO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPOSITIONNO
     */
    protected $NPOSITIONNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NREFPOSITIONNO
     */
    protected $NREFPOSITIONNO = null;

    /**
     * @var float $NRENTALITEMNO
     */
    protected $NRENTALITEMNO = null;

    /**
     * @var float $NRENTALPERSTYPENO
     */
    protected $NRENTALPERSTYPENO = null;

    /**
     * @var float $NROHLINGSTYPENO
     */
    protected $NROHLINGSTYPENO = null;

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var float $NSHOPPINGCARTPOSNO
     */
    protected $NSHOPPINGCARTPOSNO = null;

    /**
     * @var float $NSHOPPINGCARTPROJNO
     */
    protected $NSHOPPINGCARTPROJNO = null;

    /**
     * @var float $NTARIFSHEETNO
     */
    protected $NTARIFSHEETNO = null;

    /**
     * @var float $NTARIFSHEETVALIDNO
     */
    protected $NTARIFSHEETVALIDNO = null;

    /**
     * @var float $NTIMESCALENO
     */
    protected $NTIMESCALENO = null;

    /**
     * @var float $NWARESALEITEMNO
     */
    protected $NWARESALEITEMNO = null;

    /**
     * @var float $NWARESALEQUANTITY
     */
    protected $NWARESALEQUANTITY = null;

    /**
     * @var string $SZALLDISCOUNTNAME
     */
    protected $SZALLDISCOUNTNAME = null;

    /**
     * @var string $SZARCNAME
     */
    protected $SZARCNAME = null;

    /**
     * @var string $SZARTICLENAME
     */
    protected $SZARTICLENAME = null;

    /**
     * @var string $SZCUSTCARDTYPENAME
     */
    protected $SZCUSTCARDTYPENAME = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZEXTMATERIALNO
     */
    protected $SZEXTMATERIALNO = null;

    /**
     * @var string $SZPACKAGENAME
     */
    protected $SZPACKAGENAME = null;

    /**
     * @var string $SZPERSTYPENAME
     */
    protected $SZPERSTYPENAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZRENTALITEMNAME
     */
    protected $SZRENTALITEMNAME = null;

    /**
     * @var string $SZSHOPPINGCARTPOSCODE
     */
    protected $SZSHOPPINGCARTPOSCODE = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    /**
     * @var string $SZWARESALEITEMNAME
     */
    protected $SZWARESALEITEMNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSHOPCARTPOSDETDATA2
     */
    public function getACTSHOPCARTPOSDETDATA()
    {
      return $this->ACTSHOPCARTPOSDETDATA;
    }

    /**
     * @param ArrayOfD4WTPSHOPCARTPOSDETDATA2 $ACTSHOPCARTPOSDETDATA
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setACTSHOPCARTPOSDETDATA($ACTSHOPCARTPOSDETDATA)
    {
      $this->ACTSHOPCARTPOSDETDATA = $ACTSHOPCARTPOSDETDATA;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPSHOPCSUBCONTINGENT2
     */
    public function getACTSHOPCSUBCONTINGENT()
    {
      return $this->ACTSHOPCSUBCONTINGENT;
    }

    /**
     * @param ArrayOfD4WTPSHOPCSUBCONTINGENT2 $ACTSHOPCSUBCONTINGENT
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setACTSHOPCSUBCONTINGENT($ACTSHOPCSUBCONTINGENT)
    {
      $this->ACTSHOPCSUBCONTINGENT = $ACTSHOPCSUBCONTINGENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPRODUCED()
    {
      return $this->BPRODUCED;
    }

    /**
     * @param float $BPRODUCED
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setBPRODUCED($BPRODUCED)
    {
      $this->BPRODUCED = $BPRODUCED;
      return $this;
    }

    /**
     * @return float
     */
    public function getFUNITPRICE()
    {
      return $this->FUNITPRICE;
    }

    /**
     * @param float $FUNITPRICE
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setFUNITPRICE($FUNITPRICE)
    {
      $this->FUNITPRICE = $FUNITPRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getFWAREAMOUNT()
    {
      return $this->FWAREAMOUNT;
    }

    /**
     * @param float $FWAREAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setFWAREAMOUNT($FWAREAMOUNT)
    {
      $this->FWAREAMOUNT = $FWAREAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFWARETARIFF()
    {
      return $this->FWARETARIFF;
    }

    /**
     * @param float $FWARETARIFF
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setFWARETARIFF($FWARETARIFF)
    {
      $this->FWARETARIFF = $FWARETARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNALLDISCOUNTNO()
    {
      return $this->NALLDISCOUNTNO;
    }

    /**
     * @param float $NALLDISCOUNTNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNALLDISCOUNTNO($NALLDISCOUNTNO)
    {
      $this->NALLDISCOUNTNO = $NALLDISCOUNTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNALLDISCOUNTSTAFNO()
    {
      return $this->NALLDISCOUNTSTAFNO;
    }

    /**
     * @param float $NALLDISCOUNTSTAFNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNALLDISCOUNTSTAFNO($NALLDISCOUNTSTAFNO)
    {
      $this->NALLDISCOUNTSTAFNO = $NALLDISCOUNTSTAFNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARCNO()
    {
      return $this->NARCNO;
    }

    /**
     * @param float $NARCNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNARCNO($NARCNO)
    {
      $this->NARCNO = $NARCNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARCPROJNO()
    {
      return $this->NARCPROJNO;
    }

    /**
     * @param float $NARCPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNARCPROJNO($NARCPROJNO)
    {
      $this->NARCPROJNO = $NARCPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCURRSEASONNO()
    {
      return $this->NCURRSEASONNO;
    }

    /**
     * @param float $NCURRSEASONNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNCURRSEASONNO($NCURRSEASONNO)
    {
      $this->NCURRSEASONNO = $NCURRSEASONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTCARDTYPENO()
    {
      return $this->NCUSTCARDTYPENO;
    }

    /**
     * @param float $NCUSTCARDTYPENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNCUSTCARDTYPENO($NCUSTCARDTYPENO)
    {
      $this->NCUSTCARDTYPENO = $NCUSTCARDTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATACARRIERTYPENO()
    {
      return $this->NDATACARRIERTYPENO;
    }

    /**
     * @param float $NDATACARRIERTYPENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNDATACARRIERTYPENO($NDATACARRIERTYPENO)
    {
      $this->NDATACARRIERTYPENO = $NDATACARRIERTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNITEMLOCATIONNO()
    {
      return $this->NITEMLOCATIONNO;
    }

    /**
     * @param float $NITEMLOCATIONNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNITEMLOCATIONNO($NITEMLOCATIONNO)
    {
      $this->NITEMLOCATIONNO = $NITEMLOCATIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKAGENO()
    {
      return $this->NPACKAGENO;
    }

    /**
     * @param float $NPACKAGENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNPACKAGENO($NPACKAGENO)
    {
      $this->NPACKAGENO = $NPACKAGENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKPOSITION()
    {
      return $this->NPACKPOSITION;
    }

    /**
     * @param float $NPACKPOSITION
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNPACKPOSITION($NPACKPOSITION)
    {
      $this->NPACKPOSITION = $NPACKPOSITION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSTYPENO()
    {
      return $this->NPERSTYPENO;
    }

    /**
     * @param float $NPERSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNPERSTYPENO($NPERSTYPENO)
    {
      $this->NPERSTYPENO = $NPERSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSITIONNO()
    {
      return $this->NPOSITIONNO;
    }

    /**
     * @param float $NPOSITIONNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNPOSITIONNO($NPOSITIONNO)
    {
      $this->NPOSITIONNO = $NPOSITIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNREFPOSITIONNO()
    {
      return $this->NREFPOSITIONNO;
    }

    /**
     * @param float $NREFPOSITIONNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNREFPOSITIONNO($NREFPOSITIONNO)
    {
      $this->NREFPOSITIONNO = $NREFPOSITIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNO()
    {
      return $this->NRENTALITEMNO;
    }

    /**
     * @param float $NRENTALITEMNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNRENTALITEMNO($NRENTALITEMNO)
    {
      $this->NRENTALITEMNO = $NRENTALITEMNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPERSTYPENO()
    {
      return $this->NRENTALPERSTYPENO;
    }

    /**
     * @param float $NRENTALPERSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNRENTALPERSTYPENO($NRENTALPERSTYPENO)
    {
      $this->NRENTALPERSTYPENO = $NRENTALPERSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNROHLINGSTYPENO()
    {
      return $this->NROHLINGSTYPENO;
    }

    /**
     * @param float $NROHLINGSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNROHLINGSTYPENO($NROHLINGSTYPENO)
    {
      $this->NROHLINGSTYPENO = $NROHLINGSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPOSNO()
    {
      return $this->NSHOPPINGCARTPOSNO;
    }

    /**
     * @param float $NSHOPPINGCARTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNSHOPPINGCARTPOSNO($NSHOPPINGCARTPOSNO)
    {
      $this->NSHOPPINGCARTPOSNO = $NSHOPPINGCARTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPROJNO()
    {
      return $this->NSHOPPINGCARTPROJNO;
    }

    /**
     * @param float $NSHOPPINGCARTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNSHOPPINGCARTPROJNO($NSHOPPINGCARTPROJNO)
    {
      $this->NSHOPPINGCARTPROJNO = $NSHOPPINGCARTPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFSHEETNO()
    {
      return $this->NTARIFSHEETNO;
    }

    /**
     * @param float $NTARIFSHEETNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNTARIFSHEETNO($NTARIFSHEETNO)
    {
      $this->NTARIFSHEETNO = $NTARIFSHEETNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFSHEETVALIDNO()
    {
      return $this->NTARIFSHEETVALIDNO;
    }

    /**
     * @param float $NTARIFSHEETVALIDNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNTARIFSHEETVALIDNO($NTARIFSHEETVALIDNO)
    {
      $this->NTARIFSHEETVALIDNO = $NTARIFSHEETVALIDNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTIMESCALENO()
    {
      return $this->NTIMESCALENO;
    }

    /**
     * @param float $NTIMESCALENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNTIMESCALENO($NTIMESCALENO)
    {
      $this->NTIMESCALENO = $NTIMESCALENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARESALEITEMNO()
    {
      return $this->NWARESALEITEMNO;
    }

    /**
     * @param float $NWARESALEITEMNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNWARESALEITEMNO($NWARESALEITEMNO)
    {
      $this->NWARESALEITEMNO = $NWARESALEITEMNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARESALEQUANTITY()
    {
      return $this->NWARESALEQUANTITY;
    }

    /**
     * @param float $NWARESALEQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setNWARESALEQUANTITY($NWARESALEQUANTITY)
    {
      $this->NWARESALEQUANTITY = $NWARESALEQUANTITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZALLDISCOUNTNAME()
    {
      return $this->SZALLDISCOUNTNAME;
    }

    /**
     * @param string $SZALLDISCOUNTNAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZALLDISCOUNTNAME($SZALLDISCOUNTNAME)
    {
      $this->SZALLDISCOUNTNAME = $SZALLDISCOUNTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZARCNAME()
    {
      return $this->SZARCNAME;
    }

    /**
     * @param string $SZARCNAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZARCNAME($SZARCNAME)
    {
      $this->SZARCNAME = $SZARCNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZARTICLENAME()
    {
      return $this->SZARTICLENAME;
    }

    /**
     * @param string $SZARTICLENAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZARTICLENAME($SZARTICLENAME)
    {
      $this->SZARTICLENAME = $SZARTICLENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCUSTCARDTYPENAME()
    {
      return $this->SZCUSTCARDTYPENAME;
    }

    /**
     * @param string $SZCUSTCARDTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZCUSTCARDTYPENAME($SZCUSTCARDTYPENAME)
    {
      $this->SZCUSTCARDTYPENAME = $SZCUSTCARDTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTMATERIALNO()
    {
      return $this->SZEXTMATERIALNO;
    }

    /**
     * @param string $SZEXTMATERIALNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZEXTMATERIALNO($SZEXTMATERIALNO)
    {
      $this->SZEXTMATERIALNO = $SZEXTMATERIALNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPACKAGENAME()
    {
      return $this->SZPACKAGENAME;
    }

    /**
     * @param string $SZPACKAGENAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZPACKAGENAME($SZPACKAGENAME)
    {
      $this->SZPACKAGENAME = $SZPACKAGENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSTYPENAME()
    {
      return $this->SZPERSTYPENAME;
    }

    /**
     * @param string $SZPERSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZPERSTYPENAME($SZPERSTYPENAME)
    {
      $this->SZPERSTYPENAME = $SZPERSTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRENTALITEMNAME()
    {
      return $this->SZRENTALITEMNAME;
    }

    /**
     * @param string $SZRENTALITEMNAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZRENTALITEMNAME($SZRENTALITEMNAME)
    {
      $this->SZRENTALITEMNAME = $SZRENTALITEMNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHOPPINGCARTPOSCODE()
    {
      return $this->SZSHOPPINGCARTPOSCODE;
    }

    /**
     * @param string $SZSHOPPINGCARTPOSCODE
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZSHOPPINGCARTPOSCODE($SZSHOPPINGCARTPOSCODE)
    {
      $this->SZSHOPPINGCARTPOSCODE = $SZSHOPPINGCARTPOSCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWARESALEITEMNAME()
    {
      return $this->SZWARESALEITEMNAME;
    }

    /**
     * @param string $SZWARESALEITEMNAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDATA2
     */
    public function setSZWARESALEITEMNAME($SZWARESALEITEMNAME)
    {
      $this->SZWARESALEITEMNAME = $SZWARESALEITEMNAME;
      return $this;
    }

}
