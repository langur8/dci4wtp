<?php

namespace Axess\Dci4Wtp;

class getContingentResponse
{

    /**
     * @var D4WTPGETCONTINGENTRESULT $getContingentResult
     */
    protected $getContingentResult = null;

    /**
     * @param D4WTPGETCONTINGENTRESULT $getContingentResult
     */
    public function __construct($getContingentResult)
    {
      $this->getContingentResult = $getContingentResult;
    }

    /**
     * @return D4WTPGETCONTINGENTRESULT
     */
    public function getGetContingentResult()
    {
      return $this->getContingentResult;
    }

    /**
     * @param D4WTPGETCONTINGENTRESULT $getContingentResult
     * @return \Axess\Dci4Wtp\getContingentResponse
     */
    public function setGetContingentResult($getContingentResult)
    {
      $this->getContingentResult = $getContingentResult;
      return $this;
    }

}
