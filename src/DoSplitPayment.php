<?php

namespace Axess\Dci4Wtp;

class DoSplitPayment
{

    /**
     * @var D4WTPSPLITPAYMENTREQUEST $i_ctSplitPaymentReq
     */
    protected $i_ctSplitPaymentReq = null;

    /**
     * @param D4WTPSPLITPAYMENTREQUEST $i_ctSplitPaymentReq
     */
    public function __construct($i_ctSplitPaymentReq)
    {
      $this->i_ctSplitPaymentReq = $i_ctSplitPaymentReq;
    }

    /**
     * @return D4WTPSPLITPAYMENTREQUEST
     */
    public function getI_ctSplitPaymentReq()
    {
      return $this->i_ctSplitPaymentReq;
    }

    /**
     * @param D4WTPSPLITPAYMENTREQUEST $i_ctSplitPaymentReq
     * @return \Axess\Dci4Wtp\DoSplitPayment
     */
    public function setI_ctSplitPaymentReq($i_ctSplitPaymentReq)
    {
      $this->i_ctSplitPaymentReq = $i_ctSplitPaymentReq;
      return $this;
    }

}
