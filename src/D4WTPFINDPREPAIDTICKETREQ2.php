<?php

namespace Axess\Dci4Wtp;

class D4WTPFINDPREPAIDTICKETREQ2
{

    /**
     * @var float $BINCLARTICLECARDMASKS
     */
    protected $BINCLARTICLECARDMASKS = null;

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var float $NORDERSTATENR
     */
    protected $NORDERSTATENR = null;

    /**
     * @var float $NPICKUPBOXNR
     */
    protected $NPICKUPBOXNR = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZDATEFROM
     */
    protected $SZDATEFROM = null;

    /**
     * @var string $SZDATETO
     */
    protected $SZDATETO = null;

    /**
     * @var string $SZEXTORDERNR
     */
    protected $SZEXTORDERNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBINCLARTICLECARDMASKS()
    {
      return $this->BINCLARTICLECARDMASKS;
    }

    /**
     * @param float $BINCLARTICLECARDMASKS
     * @return \Axess\Dci4Wtp\D4WTPFINDPREPAIDTICKETREQ2
     */
    public function setBINCLARTICLECARDMASKS($BINCLARTICLECARDMASKS)
    {
      $this->BINCLARTICLECARDMASKS = $BINCLARTICLECARDMASKS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPFINDPREPAIDTICKETREQ2
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORDERSTATENR()
    {
      return $this->NORDERSTATENR;
    }

    /**
     * @param float $NORDERSTATENR
     * @return \Axess\Dci4Wtp\D4WTPFINDPREPAIDTICKETREQ2
     */
    public function setNORDERSTATENR($NORDERSTATENR)
    {
      $this->NORDERSTATENR = $NORDERSTATENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICKUPBOXNR()
    {
      return $this->NPICKUPBOXNR;
    }

    /**
     * @param float $NPICKUPBOXNR
     * @return \Axess\Dci4Wtp\D4WTPFINDPREPAIDTICKETREQ2
     */
    public function setNPICKUPBOXNR($NPICKUPBOXNR)
    {
      $this->NPICKUPBOXNR = $NPICKUPBOXNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPFINDPREPAIDTICKETREQ2
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPFINDPREPAIDTICKETREQ2
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPFINDPREPAIDTICKETREQ2
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEFROM()
    {
      return $this->SZDATEFROM;
    }

    /**
     * @param string $SZDATEFROM
     * @return \Axess\Dci4Wtp\D4WTPFINDPREPAIDTICKETREQ2
     */
    public function setSZDATEFROM($SZDATEFROM)
    {
      $this->SZDATEFROM = $SZDATEFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATETO()
    {
      return $this->SZDATETO;
    }

    /**
     * @param string $SZDATETO
     * @return \Axess\Dci4Wtp\D4WTPFINDPREPAIDTICKETREQ2
     */
    public function setSZDATETO($SZDATETO)
    {
      $this->SZDATETO = $SZDATETO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTORDERNR()
    {
      return $this->SZEXTORDERNR;
    }

    /**
     * @param string $SZEXTORDERNR
     * @return \Axess\Dci4Wtp\D4WTPFINDPREPAIDTICKETREQ2
     */
    public function setSZEXTORDERNR($SZEXTORDERNR)
    {
      $this->SZEXTORDERNR = $SZEXTORDERNR;
      return $this;
    }

}
