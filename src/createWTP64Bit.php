<?php

namespace Axess\Dci4Wtp;

class createWTP64Bit
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var float $i_NPROJNR
     */
    protected $i_NPROJNR = null;

    /**
     * @var float $i_nMediaIDDez
     */
    protected $i_nMediaIDDez = null;

    /**
     * @var string $i_szMediaIDHex
     */
    protected $i_szMediaIDHex = null;

    /**
     * @var string $i_szDTANr
     */
    protected $i_szDTANr = null;

    /**
     * @param float $i_nSessionID
     * @param float $i_NPROJNR
     * @param float $i_nMediaIDDez
     * @param string $i_szMediaIDHex
     * @param string $i_szDTANr
     */
    public function __construct($i_nSessionID, $i_NPROJNR, $i_nMediaIDDez, $i_szMediaIDHex, $i_szDTANr)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_NPROJNR = $i_NPROJNR;
      $this->i_nMediaIDDez = $i_nMediaIDDez;
      $this->i_szMediaIDHex = $i_szMediaIDHex;
      $this->i_szDTANr = $i_szDTANr;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\createWTP64Bit
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_NPROJNR()
    {
      return $this->i_NPROJNR;
    }

    /**
     * @param float $i_NPROJNR
     * @return \Axess\Dci4Wtp\createWTP64Bit
     */
    public function setI_NPROJNR($i_NPROJNR)
    {
      $this->i_NPROJNR = $i_NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nMediaIDDez()
    {
      return $this->i_nMediaIDDez;
    }

    /**
     * @param float $i_nMediaIDDez
     * @return \Axess\Dci4Wtp\createWTP64Bit
     */
    public function setI_nMediaIDDez($i_nMediaIDDez)
    {
      $this->i_nMediaIDDez = $i_nMediaIDDez;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szMediaIDHex()
    {
      return $this->i_szMediaIDHex;
    }

    /**
     * @param string $i_szMediaIDHex
     * @return \Axess\Dci4Wtp\createWTP64Bit
     */
    public function setI_szMediaIDHex($i_szMediaIDHex)
    {
      $this->i_szMediaIDHex = $i_szMediaIDHex;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szDTANr()
    {
      return $this->i_szDTANr;
    }

    /**
     * @param string $i_szDTANr
     * @return \Axess\Dci4Wtp\createWTP64Bit
     */
    public function setI_szDTANr($i_szDTANr)
    {
      $this->i_szDTANr = $i_szDTANr;
      return $this;
    }

}
