<?php

namespace Axess\Dci4Wtp;

class D4WTPLNECERTIFICATENORESULT
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZLNECERTIFICATENO
     */
    protected $SZLNECERTIFICATENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPLNECERTIFICATENORESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPLNECERTIFICATENORESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLNECERTIFICATENO()
    {
      return $this->SZLNECERTIFICATENO;
    }

    /**
     * @param string $SZLNECERTIFICATENO
     * @return \Axess\Dci4Wtp\D4WTPLNECERTIFICATENORESULT
     */
    public function setSZLNECERTIFICATENO($SZLNECERTIFICATENO)
    {
      $this->SZLNECERTIFICATENO = $SZLNECERTIFICATENO;
      return $this;
    }

}
