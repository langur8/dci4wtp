<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPROMPTTYPES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPROMPTTYPES[] $D4WTPPROMPTTYPES
     */
    protected $D4WTPPROMPTTYPES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPROMPTTYPES[]
     */
    public function getD4WTPPROMPTTYPES()
    {
      return $this->D4WTPPROMPTTYPES;
    }

    /**
     * @param D4WTPPROMPTTYPES[] $D4WTPPROMPTTYPES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPROMPTTYPES
     */
    public function setD4WTPPROMPTTYPES(array $D4WTPPROMPTTYPES = null)
    {
      $this->D4WTPPROMPTTYPES = $D4WTPPROMPTTYPES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPROMPTTYPES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPROMPTTYPES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPROMPTTYPES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPROMPTTYPES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPROMPTTYPES[] = $value;
      } else {
        $this->D4WTPPROMPTTYPES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPROMPTTYPES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPROMPTTYPES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPROMPTTYPES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPROMPTTYPES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPROMPTTYPES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPROMPTTYPES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPROMPTTYPES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPROMPTTYPES);
    }

}
