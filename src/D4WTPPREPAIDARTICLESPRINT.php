<?php

namespace Axess\Dci4Wtp;

class D4WTPPREPAIDARTICLESPRINT
{

    /**
     * @var float $NARTICLELFDNR
     */
    protected $NARTICLELFDNR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NORDERSTATUSNR
     */
    protected $NORDERSTATUSNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZORDERSTATUSNAME
     */
    protected $SZORDERSTATUSNAME = null;

    /**
     * @var string $SZPRINTDATA
     */
    protected $SZPRINTDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNARTICLELFDNR()
    {
      return $this->NARTICLELFDNR;
    }

    /**
     * @param float $NARTICLELFDNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDARTICLESPRINT
     */
    public function setNARTICLELFDNR($NARTICLELFDNR)
    {
      $this->NARTICLELFDNR = $NARTICLELFDNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDARTICLESPRINT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORDERSTATUSNR()
    {
      return $this->NORDERSTATUSNR;
    }

    /**
     * @param float $NORDERSTATUSNR
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDARTICLESPRINT
     */
    public function setNORDERSTATUSNR($NORDERSTATUSNR)
    {
      $this->NORDERSTATUSNR = $NORDERSTATUSNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDARTICLESPRINT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZORDERSTATUSNAME()
    {
      return $this->SZORDERSTATUSNAME;
    }

    /**
     * @param string $SZORDERSTATUSNAME
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDARTICLESPRINT
     */
    public function setSZORDERSTATUSNAME($SZORDERSTATUSNAME)
    {
      $this->SZORDERSTATUSNAME = $SZORDERSTATUSNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPRINTDATA()
    {
      return $this->SZPRINTDATA;
    }

    /**
     * @param string $SZPRINTDATA
     * @return \Axess\Dci4Wtp\D4WTPPREPAIDARTICLESPRINT
     */
    public function setSZPRINTDATA($SZPRINTDATA)
    {
      $this->SZPRINTDATA = $SZPRINTDATA;
      return $this;
    }

}
