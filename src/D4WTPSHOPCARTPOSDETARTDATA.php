<?php

namespace Axess\Dci4Wtp;

class D4WTPSHOPCARTPOSDETARTDATA
{

    /**
     * @var float $FUNITPRICE
     */
    protected $FUNITPRICE = null;

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NARTICLEPACKAGENO
     */
    protected $NARTICLEPACKAGENO = null;

    /**
     * @var float $NLFDPOSNO
     */
    protected $NLFDPOSNO = null;

    /**
     * @var float $NPOSITIONNO
     */
    protected $NPOSITIONNO = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var float $NSHOPPINGCARTPOSNO
     */
    protected $NSHOPPINGCARTPOSNO = null;

    /**
     * @var float $NSHOPPINGCARTPROJNO
     */
    protected $NSHOPPINGCARTPROJNO = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFUNITPRICE()
    {
      return $this->FUNITPRICE;
    }

    /**
     * @param float $FUNITPRICE
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETARTDATA
     */
    public function setFUNITPRICE($FUNITPRICE)
    {
      $this->FUNITPRICE = $FUNITPRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETARTDATA
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLEPACKAGENO()
    {
      return $this->NARTICLEPACKAGENO;
    }

    /**
     * @param float $NARTICLEPACKAGENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETARTDATA
     */
    public function setNARTICLEPACKAGENO($NARTICLEPACKAGENO)
    {
      $this->NARTICLEPACKAGENO = $NARTICLEPACKAGENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDPOSNO()
    {
      return $this->NLFDPOSNO;
    }

    /**
     * @param float $NLFDPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETARTDATA
     */
    public function setNLFDPOSNO($NLFDPOSNO)
    {
      $this->NLFDPOSNO = $NLFDPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSITIONNO()
    {
      return $this->NPOSITIONNO;
    }

    /**
     * @param float $NPOSITIONNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETARTDATA
     */
    public function setNPOSITIONNO($NPOSITIONNO)
    {
      $this->NPOSITIONNO = $NPOSITIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETARTDATA
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETARTDATA
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPOSNO()
    {
      return $this->NSHOPPINGCARTPOSNO;
    }

    /**
     * @param float $NSHOPPINGCARTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETARTDATA
     */
    public function setNSHOPPINGCARTPOSNO($NSHOPPINGCARTPOSNO)
    {
      $this->NSHOPPINGCARTPOSNO = $NSHOPPINGCARTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPROJNO()
    {
      return $this->NSHOPPINGCARTPROJNO;
    }

    /**
     * @param float $NSHOPPINGCARTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETARTDATA
     */
    public function setNSHOPPINGCARTPROJNO($NSHOPPINGCARTPROJNO)
    {
      $this->NSHOPPINGCARTPROJNO = $NSHOPPINGCARTPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETARTDATA
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

}
