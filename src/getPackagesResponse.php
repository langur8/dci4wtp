<?php

namespace Axess\Dci4Wtp;

class getPackagesResponse
{

    /**
     * @var D4WTPPACKAGERESULT $getPackagesResult
     */
    protected $getPackagesResult = null;

    /**
     * @param D4WTPPACKAGERESULT $getPackagesResult
     */
    public function __construct($getPackagesResult)
    {
      $this->getPackagesResult = $getPackagesResult;
    }

    /**
     * @return D4WTPPACKAGERESULT
     */
    public function getGetPackagesResult()
    {
      return $this->getPackagesResult;
    }

    /**
     * @param D4WTPPACKAGERESULT $getPackagesResult
     * @return \Axess\Dci4Wtp\getPackagesResponse
     */
    public function setGetPackagesResult($getPackagesResult)
    {
      $this->getPackagesResult = $getPackagesResult;
      return $this;
    }

}
