<?php

namespace Axess\Dci4Wtp;

class issueTicket11Response
{

    /**
     * @var D4WTPISSUETICKET10RESULT $issueTicket11Result
     */
    protected $issueTicket11Result = null;

    /**
     * @param D4WTPISSUETICKET10RESULT $issueTicket11Result
     */
    public function __construct($issueTicket11Result)
    {
      $this->issueTicket11Result = $issueTicket11Result;
    }

    /**
     * @return D4WTPISSUETICKET10RESULT
     */
    public function getIssueTicket11Result()
    {
      return $this->issueTicket11Result;
    }

    /**
     * @param D4WTPISSUETICKET10RESULT $issueTicket11Result
     * @return \Axess\Dci4Wtp\issueTicket11Response
     */
    public function setIssueTicket11Result($issueTicket11Result)
    {
      $this->issueTicket11Result = $issueTicket11Result;
      return $this;
    }

}
