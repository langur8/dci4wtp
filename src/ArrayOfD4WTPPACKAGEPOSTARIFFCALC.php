<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPACKAGEPOSTARIFFCALC implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPACKAGEPOSTARIFFCALC[] $D4WTPPACKAGEPOSTARIFFCALC
     */
    protected $D4WTPPACKAGEPOSTARIFFCALC = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPACKAGEPOSTARIFFCALC[]
     */
    public function getD4WTPPACKAGEPOSTARIFFCALC()
    {
      return $this->D4WTPPACKAGEPOSTARIFFCALC;
    }

    /**
     * @param D4WTPPACKAGEPOSTARIFFCALC[] $D4WTPPACKAGEPOSTARIFFCALC
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPACKAGEPOSTARIFFCALC
     */
    public function setD4WTPPACKAGEPOSTARIFFCALC(array $D4WTPPACKAGEPOSTARIFFCALC = null)
    {
      $this->D4WTPPACKAGEPOSTARIFFCALC = $D4WTPPACKAGEPOSTARIFFCALC;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPACKAGEPOSTARIFFCALC[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPACKAGEPOSTARIFFCALC
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPACKAGEPOSTARIFFCALC[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPACKAGEPOSTARIFFCALC $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPACKAGEPOSTARIFFCALC[] = $value;
      } else {
        $this->D4WTPPACKAGEPOSTARIFFCALC[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPACKAGEPOSTARIFFCALC[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPACKAGEPOSTARIFFCALC Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPACKAGEPOSTARIFFCALC);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPACKAGEPOSTARIFFCALC);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPACKAGEPOSTARIFFCALC);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPACKAGEPOSTARIFFCALC);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPACKAGEPOSTARIFFCALC Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPACKAGEPOSTARIFFCALC);
    }

}
