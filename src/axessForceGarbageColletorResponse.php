<?php

namespace Axess\Dci4Wtp;

class axessForceGarbageColletorResponse
{

    /**
     * @var string $axessForceGarbageColletorResult
     */
    protected $axessForceGarbageColletorResult = null;

    /**
     * @param string $axessForceGarbageColletorResult
     */
    public function __construct($axessForceGarbageColletorResult)
    {
      $this->axessForceGarbageColletorResult = $axessForceGarbageColletorResult;
    }

    /**
     * @return string
     */
    public function getAxessForceGarbageColletorResult()
    {
      return $this->axessForceGarbageColletorResult;
    }

    /**
     * @param string $axessForceGarbageColletorResult
     * @return \Axess\Dci4Wtp\axessForceGarbageColletorResponse
     */
    public function setAxessForceGarbageColletorResult($axessForceGarbageColletorResult)
    {
      $this->axessForceGarbageColletorResult = $axessForceGarbageColletorResult;
      return $this;
    }

}
