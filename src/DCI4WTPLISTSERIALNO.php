<?php

namespace Axess\Dci4Wtp;

class DCI4WTPLISTSERIALNO
{

    /**
     * @var float $NTICKETPOSNO
     */
    protected $NTICKETPOSNO = null;

    /**
     * @var float $NTICKETPROJNO
     */
    protected $NTICKETPROJNO = null;

    /**
     * @var float $NTICKETSERIALNO
     */
    protected $NTICKETSERIALNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNTICKETPOSNO()
    {
      return $this->NTICKETPOSNO;
    }

    /**
     * @param float $NTICKETPOSNO
     * @return \Axess\Dci4Wtp\DCI4WTPLISTSERIALNO
     */
    public function setNTICKETPOSNO($NTICKETPOSNO)
    {
      $this->NTICKETPOSNO = $NTICKETPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETPROJNO()
    {
      return $this->NTICKETPROJNO;
    }

    /**
     * @param float $NTICKETPROJNO
     * @return \Axess\Dci4Wtp\DCI4WTPLISTSERIALNO
     */
    public function setNTICKETPROJNO($NTICKETPROJNO)
    {
      $this->NTICKETPROJNO = $NTICKETPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETSERIALNO()
    {
      return $this->NTICKETSERIALNO;
    }

    /**
     * @param float $NTICKETSERIALNO
     * @return \Axess\Dci4Wtp\DCI4WTPLISTSERIALNO
     */
    public function setNTICKETSERIALNO($NTICKETSERIALNO)
    {
      $this->NTICKETSERIALNO = $NTICKETSERIALNO;
      return $this;
    }

}
