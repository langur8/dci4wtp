<?php

namespace Axess\Dci4Wtp;

class D4WTPCANCELRENTAL
{

    /**
     * @var float $NRENTALBLOCKNR
     */
    protected $NRENTALBLOCKNR = null;

    /**
     * @var float $NRENTALITEMNR
     */
    protected $NRENTALITEMNR = null;

    /**
     * @var float $NRENTALPERSTYPENR
     */
    protected $NRENTALPERSTYPENR = null;

    /**
     * @var float $NRENTALPOSNR
     */
    protected $NRENTALPOSNR = null;

    /**
     * @var float $NRENTALPROJNR
     */
    protected $NRENTALPROJNR = null;

    /**
     * @var float $NRENTALTRANSNR
     */
    protected $NRENTALTRANSNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNRENTALBLOCKNR()
    {
      return $this->NRENTALBLOCKNR;
    }

    /**
     * @param float $NRENTALBLOCKNR
     * @return \Axess\Dci4Wtp\D4WTPCANCELRENTAL
     */
    public function setNRENTALBLOCKNR($NRENTALBLOCKNR)
    {
      $this->NRENTALBLOCKNR = $NRENTALBLOCKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNR()
    {
      return $this->NRENTALITEMNR;
    }

    /**
     * @param float $NRENTALITEMNR
     * @return \Axess\Dci4Wtp\D4WTPCANCELRENTAL
     */
    public function setNRENTALITEMNR($NRENTALITEMNR)
    {
      $this->NRENTALITEMNR = $NRENTALITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPERSTYPENR()
    {
      return $this->NRENTALPERSTYPENR;
    }

    /**
     * @param float $NRENTALPERSTYPENR
     * @return \Axess\Dci4Wtp\D4WTPCANCELRENTAL
     */
    public function setNRENTALPERSTYPENR($NRENTALPERSTYPENR)
    {
      $this->NRENTALPERSTYPENR = $NRENTALPERSTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPOSNR()
    {
      return $this->NRENTALPOSNR;
    }

    /**
     * @param float $NRENTALPOSNR
     * @return \Axess\Dci4Wtp\D4WTPCANCELRENTAL
     */
    public function setNRENTALPOSNR($NRENTALPOSNR)
    {
      $this->NRENTALPOSNR = $NRENTALPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPROJNR()
    {
      return $this->NRENTALPROJNR;
    }

    /**
     * @param float $NRENTALPROJNR
     * @return \Axess\Dci4Wtp\D4WTPCANCELRENTAL
     */
    public function setNRENTALPROJNR($NRENTALPROJNR)
    {
      $this->NRENTALPROJNR = $NRENTALPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALTRANSNR()
    {
      return $this->NRENTALTRANSNR;
    }

    /**
     * @param float $NRENTALTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPCANCELRENTAL
     */
    public function setNRENTALTRANSNR($NRENTALTRANSNR)
    {
      $this->NRENTALTRANSNR = $NRENTALTRANSNR;
      return $this;
    }

}
