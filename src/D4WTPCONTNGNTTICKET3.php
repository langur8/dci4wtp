<?php

namespace Axess\Dci4Wtp;

class D4WTPCONTNGNTTICKET3
{

    /**
     * @var ArrayOfD4WTPSUBCONTNGNTTICKET3 $ACTSUBCONTNGNTTICKET3
     */
    protected $ACTSUBCONTNGNTTICKET3 = null;

    /**
     * @var float $BOPTIONAL
     */
    protected $BOPTIONAL = null;

    /**
     * @var float $BSUBCONTINGENT
     */
    protected $BSUBCONTINGENT = null;

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NCONTINGENTNO
     */
    protected $NCONTINGENTNO = null;

    /**
     * @var float $NSORTNR
     */
    protected $NSORTNR = null;

    /**
     * @var float $NTICKETCOUNTFREE
     */
    protected $NTICKETCOUNTFREE = null;

    /**
     * @var float $NTICKETCOUNTTOTAL
     */
    protected $NTICKETCOUNTTOTAL = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSHORTNAME
     */
    protected $SZSHORTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSUBCONTNGNTTICKET3
     */
    public function getACTSUBCONTNGNTTICKET3()
    {
      return $this->ACTSUBCONTNGNTTICKET3;
    }

    /**
     * @param ArrayOfD4WTPSUBCONTNGNTTICKET3 $ACTSUBCONTNGNTTICKET3
     * @return \Axess\Dci4Wtp\D4WTPCONTNGNTTICKET3
     */
    public function setACTSUBCONTNGNTTICKET3($ACTSUBCONTNGNTTICKET3)
    {
      $this->ACTSUBCONTNGNTTICKET3 = $ACTSUBCONTNGNTTICKET3;
      return $this;
    }

    /**
     * @return float
     */
    public function getBOPTIONAL()
    {
      return $this->BOPTIONAL;
    }

    /**
     * @param float $BOPTIONAL
     * @return \Axess\Dci4Wtp\D4WTPCONTNGNTTICKET3
     */
    public function setBOPTIONAL($BOPTIONAL)
    {
      $this->BOPTIONAL = $BOPTIONAL;
      return $this;
    }

    /**
     * @return float
     */
    public function getBSUBCONTINGENT()
    {
      return $this->BSUBCONTINGENT;
    }

    /**
     * @param float $BSUBCONTINGENT
     * @return \Axess\Dci4Wtp\D4WTPCONTNGNTTICKET3
     */
    public function setBSUBCONTINGENT($BSUBCONTINGENT)
    {
      $this->BSUBCONTINGENT = $BSUBCONTINGENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPCONTNGNTTICKET3
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNO()
    {
      return $this->NCONTINGENTNO;
    }

    /**
     * @param float $NCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPCONTNGNTTICKET3
     */
    public function setNCONTINGENTNO($NCONTINGENTNO)
    {
      $this->NCONTINGENTNO = $NCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNR()
    {
      return $this->NSORTNR;
    }

    /**
     * @param float $NSORTNR
     * @return \Axess\Dci4Wtp\D4WTPCONTNGNTTICKET3
     */
    public function setNSORTNR($NSORTNR)
    {
      $this->NSORTNR = $NSORTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETCOUNTFREE()
    {
      return $this->NTICKETCOUNTFREE;
    }

    /**
     * @param float $NTICKETCOUNTFREE
     * @return \Axess\Dci4Wtp\D4WTPCONTNGNTTICKET3
     */
    public function setNTICKETCOUNTFREE($NTICKETCOUNTFREE)
    {
      $this->NTICKETCOUNTFREE = $NTICKETCOUNTFREE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETCOUNTTOTAL()
    {
      return $this->NTICKETCOUNTTOTAL;
    }

    /**
     * @param float $NTICKETCOUNTTOTAL
     * @return \Axess\Dci4Wtp\D4WTPCONTNGNTTICKET3
     */
    public function setNTICKETCOUNTTOTAL($NTICKETCOUNTTOTAL)
    {
      $this->NTICKETCOUNTTOTAL = $NTICKETCOUNTTOTAL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPCONTNGNTTICKET3
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHORTNAME()
    {
      return $this->SZSHORTNAME;
    }

    /**
     * @param string $SZSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPCONTNGNTTICKET3
     */
    public function setSZSHORTNAME($SZSHORTNAME)
    {
      $this->SZSHORTNAME = $SZSHORTNAME;
      return $this;
    }

}
