<?php

namespace Axess\Dci4Wtp;

class D4WTPREPLACETICKETREQ
{

    /**
     * @var ArrayOfD4WTPADDARTICLE $ACTADDARTICLE
     */
    protected $ACTADDARTICLE = null;

    /**
     * @var float $BISPREPAID
     */
    protected $BISPREPAID = null;

    /**
     * @var \DateTime $DTVALIDDATE
     */
    protected $DTVALIDDATE = null;

    /**
     * @var float $NKASSIERNO
     */
    protected $NKASSIERNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPOSNOOLD
     */
    protected $NPOSNOOLD = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NPROJNOOLD
     */
    protected $NPROJNOOLD = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NUNICODENO
     */
    protected $NUNICODENO = null;

    /**
     * @var float $NWTPMODE
     */
    protected $NWTPMODE = null;

    /**
     * @var string $SZAUFTRAGEXT
     */
    protected $SZAUFTRAGEXT = null;

    /**
     * @var string $SZCODINGMODE
     */
    protected $SZCODINGMODE = null;

    /**
     * @var string $SZDCCONTENT
     */
    protected $SZDCCONTENT = null;

    /**
     * @var string $SZDRIVERTYPE
     */
    protected $SZDRIVERTYPE = null;

    /**
     * @var string $SZWTPNUMBER
     */
    protected $SZWTPNUMBER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDARTICLE
     */
    public function getACTADDARTICLE()
    {
      return $this->ACTADDARTICLE;
    }

    /**
     * @param ArrayOfD4WTPADDARTICLE $ACTADDARTICLE
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setACTADDARTICLE($ACTADDARTICLE)
    {
      $this->ACTADDARTICLE = $ACTADDARTICLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISPREPAID()
    {
      return $this->BISPREPAID;
    }

    /**
     * @param float $BISPREPAID
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setBISPREPAID($BISPREPAID)
    {
      $this->BISPREPAID = $BISPREPAID;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDTVALIDDATE()
    {
      if ($this->DTVALIDDATE == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DTVALIDDATE);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DTVALIDDATE
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setDTVALIDDATE(\DateTime $DTVALIDDATE = null)
    {
      if ($DTVALIDDATE == null) {
       $this->DTVALIDDATE = null;
      } else {
        $this->DTVALIDDATE = $DTVALIDDATE->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return float
     */
    public function getNKASSIERNO()
    {
      return $this->NKASSIERNO;
    }

    /**
     * @param float $NKASSIERNO
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setNKASSIERNO($NKASSIERNO)
    {
      $this->NKASSIERNO = $NKASSIERNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNOOLD()
    {
      return $this->NPOSNOOLD;
    }

    /**
     * @param float $NPOSNOOLD
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setNPOSNOOLD($NPOSNOOLD)
    {
      $this->NPOSNOOLD = $NPOSNOOLD;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNOOLD()
    {
      return $this->NPROJNOOLD;
    }

    /**
     * @param float $NPROJNOOLD
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setNPROJNOOLD($NPROJNOOLD)
    {
      $this->NPROJNOOLD = $NPROJNOOLD;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENO()
    {
      return $this->NUNICODENO;
    }

    /**
     * @param float $NUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setNUNICODENO($NUNICODENO)
    {
      $this->NUNICODENO = $NUNICODENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPMODE()
    {
      return $this->NWTPMODE;
    }

    /**
     * @param float $NWTPMODE
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setNWTPMODE($NWTPMODE)
    {
      $this->NWTPMODE = $NWTPMODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAUFTRAGEXT()
    {
      return $this->SZAUFTRAGEXT;
    }

    /**
     * @param string $SZAUFTRAGEXT
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setSZAUFTRAGEXT($SZAUFTRAGEXT)
    {
      $this->SZAUFTRAGEXT = $SZAUFTRAGEXT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGMODE()
    {
      return $this->SZCODINGMODE;
    }

    /**
     * @param string $SZCODINGMODE
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setSZCODINGMODE($SZCODINGMODE)
    {
      $this->SZCODINGMODE = $SZCODINGMODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDCCONTENT()
    {
      return $this->SZDCCONTENT;
    }

    /**
     * @param string $SZDCCONTENT
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setSZDCCONTENT($SZDCCONTENT)
    {
      $this->SZDCCONTENT = $SZDCCONTENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDRIVERTYPE()
    {
      return $this->SZDRIVERTYPE;
    }

    /**
     * @param string $SZDRIVERTYPE
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setSZDRIVERTYPE($SZDRIVERTYPE)
    {
      $this->SZDRIVERTYPE = $SZDRIVERTYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNUMBER()
    {
      return $this->SZWTPNUMBER;
    }

    /**
     * @param string $SZWTPNUMBER
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETREQ
     */
    public function setSZWTPNUMBER($SZWTPNUMBER)
    {
      $this->SZWTPNUMBER = $SZWTPNUMBER;
      return $this;
    }

}
