<?php

namespace Axess\Dci4Wtp;

class D4WTPCONFIGREQUEST
{

    /**
     * @var float $NPARAMID
     */
    protected $NPARAMID = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPARAMID()
    {
      return $this->NPARAMID;
    }

    /**
     * @param float $NPARAMID
     * @return \Axess\Dci4Wtp\D4WTPCONFIGREQUEST
     */
    public function setNPARAMID($NPARAMID)
    {
      $this->NPARAMID = $NPARAMID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCONFIGREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
