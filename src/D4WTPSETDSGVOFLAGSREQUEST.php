<?php

namespace Axess\Dci4Wtp;

class D4WTPSETDSGVOFLAGSREQUEST
{

    /**
     * @var ArrayOfD4WTPDSGVOFLAGS $ACTDSGVOFLAGS
     */
    protected $ACTDSGVOFLAGS = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPDSGVOFLAGS
     */
    public function getACTDSGVOFLAGS()
    {
      return $this->ACTDSGVOFLAGS;
    }

    /**
     * @param ArrayOfD4WTPDSGVOFLAGS $ACTDSGVOFLAGS
     * @return \Axess\Dci4Wtp\D4WTPSETDSGVOFLAGSREQUEST
     */
    public function setACTDSGVOFLAGS($ACTDSGVOFLAGS)
    {
      $this->ACTDSGVOFLAGS = $ACTDSGVOFLAGS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSETDSGVOFLAGSREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
