<?php

namespace Axess\Dci4Wtp;

class assignPersToCompanyResponse
{

    /**
     * @var D4WTPRESULT $assignPersToCompanyResult
     */
    protected $assignPersToCompanyResult = null;

    /**
     * @param D4WTPRESULT $assignPersToCompanyResult
     */
    public function __construct($assignPersToCompanyResult)
    {
      $this->assignPersToCompanyResult = $assignPersToCompanyResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getAssignPersToCompanyResult()
    {
      return $this->assignPersToCompanyResult;
    }

    /**
     * @param D4WTPRESULT $assignPersToCompanyResult
     * @return \Axess\Dci4Wtp\assignPersToCompanyResponse
     */
    public function setAssignPersToCompanyResult($assignPersToCompanyResult)
    {
      $this->assignPersToCompanyResult = $assignPersToCompanyResult;
      return $this;
    }

}
