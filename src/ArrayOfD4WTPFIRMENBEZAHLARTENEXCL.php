<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPFIRMENBEZAHLARTENEXCL implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPFIRMENBEZAHLARTENEXCL[] $D4WTPFIRMENBEZAHLARTENEXCL
     */
    protected $D4WTPFIRMENBEZAHLARTENEXCL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPFIRMENBEZAHLARTENEXCL[]
     */
    public function getD4WTPFIRMENBEZAHLARTENEXCL()
    {
      return $this->D4WTPFIRMENBEZAHLARTENEXCL;
    }

    /**
     * @param D4WTPFIRMENBEZAHLARTENEXCL[] $D4WTPFIRMENBEZAHLARTENEXCL
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPFIRMENBEZAHLARTENEXCL
     */
    public function setD4WTPFIRMENBEZAHLARTENEXCL(array $D4WTPFIRMENBEZAHLARTENEXCL = null)
    {
      $this->D4WTPFIRMENBEZAHLARTENEXCL = $D4WTPFIRMENBEZAHLARTENEXCL;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPFIRMENBEZAHLARTENEXCL[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPFIRMENBEZAHLARTENEXCL
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPFIRMENBEZAHLARTENEXCL[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPFIRMENBEZAHLARTENEXCL $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPFIRMENBEZAHLARTENEXCL[] = $value;
      } else {
        $this->D4WTPFIRMENBEZAHLARTENEXCL[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPFIRMENBEZAHLARTENEXCL[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPFIRMENBEZAHLARTENEXCL Return the current element
     */
    public function current()
    {
      return current($this->D4WTPFIRMENBEZAHLARTENEXCL);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPFIRMENBEZAHLARTENEXCL);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPFIRMENBEZAHLARTENEXCL);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPFIRMENBEZAHLARTENEXCL);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPFIRMENBEZAHLARTENEXCL Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPFIRMENBEZAHLARTENEXCL);
    }

}
