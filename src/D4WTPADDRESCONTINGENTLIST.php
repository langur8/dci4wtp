<?php

namespace Axess\Dci4Wtp;

class D4WTPADDRESCONTINGENTLIST
{

    /**
     * @var float $NCONTINGENTNO
     */
    protected $NCONTINGENTNO = null;

    /**
     * @var float $NSUBCONTINGENTNO
     */
    protected $NSUBCONTINGENTNO = null;

    /**
     * @var string $SZRESERVATIONDATE
     */
    protected $SZRESERVATIONDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNO()
    {
      return $this->NCONTINGENTNO;
    }

    /**
     * @param float $NCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPADDRESCONTINGENTLIST
     */
    public function setNCONTINGENTNO($NCONTINGENTNO)
    {
      $this->NCONTINGENTNO = $NCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTINGENTNO()
    {
      return $this->NSUBCONTINGENTNO;
    }

    /**
     * @param float $NSUBCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPADDRESCONTINGENTLIST
     */
    public function setNSUBCONTINGENTNO($NSUBCONTINGENTNO)
    {
      $this->NSUBCONTINGENTNO = $NSUBCONTINGENTNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRESERVATIONDATE()
    {
      return $this->SZRESERVATIONDATE;
    }

    /**
     * @param string $SZRESERVATIONDATE
     * @return \Axess\Dci4Wtp\D4WTPADDRESCONTINGENTLIST
     */
    public function setSZRESERVATIONDATE($SZRESERVATIONDATE)
    {
      $this->SZRESERVATIONDATE = $SZRESERVATIONDATE;
      return $this;
    }

}
