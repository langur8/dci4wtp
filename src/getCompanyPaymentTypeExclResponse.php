<?php

namespace Axess\Dci4Wtp;

class getCompanyPaymentTypeExclResponse
{

    /**
     * @var D4WTPFIRMENBZHLARTEXCLRESULT $getCompanyPaymentTypeExclResult
     */
    protected $getCompanyPaymentTypeExclResult = null;

    /**
     * @param D4WTPFIRMENBZHLARTEXCLRESULT $getCompanyPaymentTypeExclResult
     */
    public function __construct($getCompanyPaymentTypeExclResult)
    {
      $this->getCompanyPaymentTypeExclResult = $getCompanyPaymentTypeExclResult;
    }

    /**
     * @return D4WTPFIRMENBZHLARTEXCLRESULT
     */
    public function getGetCompanyPaymentTypeExclResult()
    {
      return $this->getCompanyPaymentTypeExclResult;
    }

    /**
     * @param D4WTPFIRMENBZHLARTEXCLRESULT $getCompanyPaymentTypeExclResult
     * @return \Axess\Dci4Wtp\getCompanyPaymentTypeExclResponse
     */
    public function setGetCompanyPaymentTypeExclResult($getCompanyPaymentTypeExclResult)
    {
      $this->getCompanyPaymentTypeExclResult = $getCompanyPaymentTypeExclResult;
      return $this;
    }

}
