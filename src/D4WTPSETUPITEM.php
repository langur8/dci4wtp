<?php

namespace Axess\Dci4Wtp;

class D4WTPSETUPITEM
{

    /**
     * @var float $BDELETED
     */
    protected $BDELETED = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZDETAILS
     */
    protected $SZDETAILS = null;

    /**
     * @var string $SZGROUP
     */
    protected $SZGROUP = null;

    /**
     * @var string $SZITEM
     */
    protected $SZITEM = null;

    /**
     * @var string $SZUPDATEDATE
     */
    protected $SZUPDATEDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBDELETED()
    {
      return $this->BDELETED;
    }

    /**
     * @param float $BDELETED
     * @return \Axess\Dci4Wtp\D4WTPSETUPITEM
     */
    public function setBDELETED($BDELETED)
    {
      $this->BDELETED = $BDELETED;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPSETUPITEM
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDETAILS()
    {
      return $this->SZDETAILS;
    }

    /**
     * @param string $SZDETAILS
     * @return \Axess\Dci4Wtp\D4WTPSETUPITEM
     */
    public function setSZDETAILS($SZDETAILS)
    {
      $this->SZDETAILS = $SZDETAILS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGROUP()
    {
      return $this->SZGROUP;
    }

    /**
     * @param string $SZGROUP
     * @return \Axess\Dci4Wtp\D4WTPSETUPITEM
     */
    public function setSZGROUP($SZGROUP)
    {
      $this->SZGROUP = $SZGROUP;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZITEM()
    {
      return $this->SZITEM;
    }

    /**
     * @param string $SZITEM
     * @return \Axess\Dci4Wtp\D4WTPSETUPITEM
     */
    public function setSZITEM($SZITEM)
    {
      $this->SZITEM = $SZITEM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUPDATEDATE()
    {
      return $this->SZUPDATEDATE;
    }

    /**
     * @param string $SZUPDATEDATE
     * @return \Axess\Dci4Wtp\D4WTPSETUPITEM
     */
    public function setSZUPDATEDATE($SZUPDATEDATE)
    {
      $this->SZUPDATEDATE = $SZUPDATEDATE;
      return $this;
    }

}
