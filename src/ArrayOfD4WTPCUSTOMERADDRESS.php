<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCUSTOMERADDRESS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCUSTOMERADDRESS[] $D4WTPCUSTOMERADDRESS
     */
    protected $D4WTPCUSTOMERADDRESS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCUSTOMERADDRESS[]
     */
    public function getD4WTPCUSTOMERADDRESS()
    {
      return $this->D4WTPCUSTOMERADDRESS;
    }

    /**
     * @param D4WTPCUSTOMERADDRESS[] $D4WTPCUSTOMERADDRESS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCUSTOMERADDRESS
     */
    public function setD4WTPCUSTOMERADDRESS(array $D4WTPCUSTOMERADDRESS = null)
    {
      $this->D4WTPCUSTOMERADDRESS = $D4WTPCUSTOMERADDRESS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCUSTOMERADDRESS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCUSTOMERADDRESS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCUSTOMERADDRESS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCUSTOMERADDRESS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCUSTOMERADDRESS[] = $value;
      } else {
        $this->D4WTPCUSTOMERADDRESS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCUSTOMERADDRESS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCUSTOMERADDRESS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCUSTOMERADDRESS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCUSTOMERADDRESS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCUSTOMERADDRESS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCUSTOMERADDRESS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCUSTOMERADDRESS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCUSTOMERADDRESS);
    }

}
