<?php

namespace Axess\Dci4Wtp;

class D4WTPRENTALPERSTYPE
{

    /**
     * @var float $NRENTALPERSTYPNR
     */
    protected $NRENTALPERSTYPNR = null;

    /**
     * @var string $SZDOBFROM
     */
    protected $SZDOBFROM = null;

    /**
     * @var string $SZDOBTO
     */
    protected $SZDOBTO = null;

    /**
     * @var string $SZMASKNAME
     */
    protected $SZMASKNAME = null;

    /**
     * @var string $SZMASKSHORTNAME
     */
    protected $SZMASKSHORTNAME = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSHORTNAME
     */
    protected $SZSHORTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNRENTALPERSTYPNR()
    {
      return $this->NRENTALPERSTYPNR;
    }

    /**
     * @param float $NRENTALPERSTYPNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALPERSTYPE
     */
    public function setNRENTALPERSTYPNR($NRENTALPERSTYPNR)
    {
      $this->NRENTALPERSTYPNR = $NRENTALPERSTYPNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDOBFROM()
    {
      return $this->SZDOBFROM;
    }

    /**
     * @param string $SZDOBFROM
     * @return \Axess\Dci4Wtp\D4WTPRENTALPERSTYPE
     */
    public function setSZDOBFROM($SZDOBFROM)
    {
      $this->SZDOBFROM = $SZDOBFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDOBTO()
    {
      return $this->SZDOBTO;
    }

    /**
     * @param string $SZDOBTO
     * @return \Axess\Dci4Wtp\D4WTPRENTALPERSTYPE
     */
    public function setSZDOBTO($SZDOBTO)
    {
      $this->SZDOBTO = $SZDOBTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASKNAME()
    {
      return $this->SZMASKNAME;
    }

    /**
     * @param string $SZMASKNAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALPERSTYPE
     */
    public function setSZMASKNAME($SZMASKNAME)
    {
      $this->SZMASKNAME = $SZMASKNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASKSHORTNAME()
    {
      return $this->SZMASKSHORTNAME;
    }

    /**
     * @param string $SZMASKSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALPERSTYPE
     */
    public function setSZMASKSHORTNAME($SZMASKSHORTNAME)
    {
      $this->SZMASKSHORTNAME = $SZMASKSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALPERSTYPE
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHORTNAME()
    {
      return $this->SZSHORTNAME;
    }

    /**
     * @param string $SZSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALPERSTYPE
     */
    public function setSZSHORTNAME($SZSHORTNAME)
    {
      $this->SZSHORTNAME = $SZSHORTNAME;
      return $this;
    }

}
