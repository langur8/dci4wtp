<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSHOPCARTPOSDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSHOPCARTPOSDATA[] $D4WTPSHOPCARTPOSDATA
     */
    protected $D4WTPSHOPCARTPOSDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSHOPCARTPOSDATA[]
     */
    public function getD4WTPSHOPCARTPOSDATA()
    {
      return $this->D4WTPSHOPCARTPOSDATA;
    }

    /**
     * @param D4WTPSHOPCARTPOSDATA[] $D4WTPSHOPCARTPOSDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSHOPCARTPOSDATA
     */
    public function setD4WTPSHOPCARTPOSDATA(array $D4WTPSHOPCARTPOSDATA = null)
    {
      $this->D4WTPSHOPCARTPOSDATA = $D4WTPSHOPCARTPOSDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSHOPCARTPOSDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSHOPCARTPOSDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSHOPCARTPOSDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSHOPCARTPOSDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSHOPCARTPOSDATA[] = $value;
      } else {
        $this->D4WTPSHOPCARTPOSDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSHOPCARTPOSDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSHOPCARTPOSDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSHOPCARTPOSDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSHOPCARTPOSDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSHOPCARTPOSDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSHOPCARTPOSDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSHOPCARTPOSDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSHOPCARTPOSDATA);
    }

}
