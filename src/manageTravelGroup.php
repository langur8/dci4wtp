<?php

namespace Axess\Dci4Wtp;

class manageTravelGroup
{

    /**
     * @var D4WTPMANAGETRAVELGRPREQUEST $i_ctManageTravelGroupReq
     */
    protected $i_ctManageTravelGroupReq = null;

    /**
     * @param D4WTPMANAGETRAVELGRPREQUEST $i_ctManageTravelGroupReq
     */
    public function __construct($i_ctManageTravelGroupReq)
    {
      $this->i_ctManageTravelGroupReq = $i_ctManageTravelGroupReq;
    }

    /**
     * @return D4WTPMANAGETRAVELGRPREQUEST
     */
    public function getI_ctManageTravelGroupReq()
    {
      return $this->i_ctManageTravelGroupReq;
    }

    /**
     * @param D4WTPMANAGETRAVELGRPREQUEST $i_ctManageTravelGroupReq
     * @return \Axess\Dci4Wtp\manageTravelGroup
     */
    public function setI_ctManageTravelGroupReq($i_ctManageTravelGroupReq)
    {
      $this->i_ctManageTravelGroupReq = $i_ctManageTravelGroupReq;
      return $this;
    }

}
