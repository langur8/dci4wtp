<?php

namespace Axess\Dci4Wtp;

class reloadTicket
{

    /**
     * @var D4WTPRELOADTICKETREQUEST $i_ctReloadTicketReq
     */
    protected $i_ctReloadTicketReq = null;

    /**
     * @param D4WTPRELOADTICKETREQUEST $i_ctReloadTicketReq
     */
    public function __construct($i_ctReloadTicketReq)
    {
      $this->i_ctReloadTicketReq = $i_ctReloadTicketReq;
    }

    /**
     * @return D4WTPRELOADTICKETREQUEST
     */
    public function getI_ctReloadTicketReq()
    {
      return $this->i_ctReloadTicketReq;
    }

    /**
     * @param D4WTPRELOADTICKETREQUEST $i_ctReloadTicketReq
     * @return \Axess\Dci4Wtp\reloadTicket
     */
    public function setI_ctReloadTicketReq($i_ctReloadTicketReq)
    {
      $this->i_ctReloadTicketReq = $i_ctReloadTicketReq;
      return $this;
    }

}
