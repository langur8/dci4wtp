<?php

namespace Axess\Dci4Wtp;

class findPrepaidTicket4Response
{

    /**
     * @var D4WTPREPAIDRESULT4 $findPrepaidTicket4Result
     */
    protected $findPrepaidTicket4Result = null;

    /**
     * @param D4WTPREPAIDRESULT4 $findPrepaidTicket4Result
     */
    public function __construct($findPrepaidTicket4Result)
    {
      $this->findPrepaidTicket4Result = $findPrepaidTicket4Result;
    }

    /**
     * @return D4WTPREPAIDRESULT4
     */
    public function getFindPrepaidTicket4Result()
    {
      return $this->findPrepaidTicket4Result;
    }

    /**
     * @param D4WTPREPAIDRESULT4 $findPrepaidTicket4Result
     * @return \Axess\Dci4Wtp\findPrepaidTicket4Response
     */
    public function setFindPrepaidTicket4Result($findPrepaidTicket4Result)
    {
      $this->findPrepaidTicket4Result = $findPrepaidTicket4Result;
      return $this;
    }

}
