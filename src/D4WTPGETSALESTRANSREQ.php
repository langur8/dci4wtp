<?php

namespace Axess\Dci4Wtp;

class D4WTPGETSALESTRANSREQ
{

    /**
     * @var float $NCOMPANYNR
     */
    protected $NCOMPANYNR = null;

    /**
     * @var float $NCOMPANYPOSNR
     */
    protected $NCOMPANYPOSNR = null;

    /**
     * @var float $NCOMPANYPROJNR
     */
    protected $NCOMPANYPROJNR = null;

    /**
     * @var float $NMAXCOUNT
     */
    protected $NMAXCOUNT = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZFROM
     */
    protected $SZFROM = null;

    /**
     * @var string $SZTO
     */
    protected $SZTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCOMPANYNR()
    {
      return $this->NCOMPANYNR;
    }

    /**
     * @param float $NCOMPANYNR
     * @return \Axess\Dci4Wtp\D4WTPGETSALESTRANSREQ
     */
    public function setNCOMPANYNR($NCOMPANYNR)
    {
      $this->NCOMPANYNR = $NCOMPANYNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNR()
    {
      return $this->NCOMPANYPOSNR;
    }

    /**
     * @param float $NCOMPANYPOSNR
     * @return \Axess\Dci4Wtp\D4WTPGETSALESTRANSREQ
     */
    public function setNCOMPANYPOSNR($NCOMPANYPOSNR)
    {
      $this->NCOMPANYPOSNR = $NCOMPANYPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNR()
    {
      return $this->NCOMPANYPROJNR;
    }

    /**
     * @param float $NCOMPANYPROJNR
     * @return \Axess\Dci4Wtp\D4WTPGETSALESTRANSREQ
     */
    public function setNCOMPANYPROJNR($NCOMPANYPROJNR)
    {
      $this->NCOMPANYPROJNR = $NCOMPANYPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMAXCOUNT()
    {
      return $this->NMAXCOUNT;
    }

    /**
     * @param float $NMAXCOUNT
     * @return \Axess\Dci4Wtp\D4WTPGETSALESTRANSREQ
     */
    public function setNMAXCOUNT($NMAXCOUNT)
    {
      $this->NMAXCOUNT = $NMAXCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPGETSALESTRANSREQ
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPGETSALESTRANSREQ
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETSALESTRANSREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFROM()
    {
      return $this->SZFROM;
    }

    /**
     * @param string $SZFROM
     * @return \Axess\Dci4Wtp\D4WTPGETSALESTRANSREQ
     */
    public function setSZFROM($SZFROM)
    {
      $this->SZFROM = $SZFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTO()
    {
      return $this->SZTO;
    }

    /**
     * @param string $SZTO
     * @return \Axess\Dci4Wtp\D4WTPGETSALESTRANSREQ
     */
    public function setSZTO($SZTO)
    {
      $this->SZTO = $SZTO;
      return $this;
    }

}
