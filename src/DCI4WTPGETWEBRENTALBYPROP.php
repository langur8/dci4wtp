<?php

namespace Axess\Dci4Wtp;

class DCI4WTPGETWEBRENTALBYPROP
{

    /**
     * @var float $NPROPERTY
     */
    protected $NPROPERTY = null;

    /**
     * @var float $NPROPERTYTYPE
     */
    protected $NPROPERTYTYPE = null;

    /**
     * @var float $NPROPERTYVALUE
     */
    protected $NPROPERTYVALUE = null;

    /**
     * @var string $SZPROPERTYVALUE
     */
    protected $SZPROPERTYVALUE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPROPERTY()
    {
      return $this->NPROPERTY;
    }

    /**
     * @param float $NPROPERTY
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRENTALBYPROP
     */
    public function setNPROPERTY($NPROPERTY)
    {
      $this->NPROPERTY = $NPROPERTY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROPERTYTYPE()
    {
      return $this->NPROPERTYTYPE;
    }

    /**
     * @param float $NPROPERTYTYPE
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRENTALBYPROP
     */
    public function setNPROPERTYTYPE($NPROPERTYTYPE)
    {
      $this->NPROPERTYTYPE = $NPROPERTYTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROPERTYVALUE()
    {
      return $this->NPROPERTYVALUE;
    }

    /**
     * @param float $NPROPERTYVALUE
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRENTALBYPROP
     */
    public function setNPROPERTYVALUE($NPROPERTYVALUE)
    {
      $this->NPROPERTYVALUE = $NPROPERTYVALUE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPROPERTYVALUE()
    {
      return $this->SZPROPERTYVALUE;
    }

    /**
     * @param string $SZPROPERTYVALUE
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRENTALBYPROP
     */
    public function setSZPROPERTYVALUE($SZPROPERTYVALUE)
    {
      $this->SZPROPERTYVALUE = $SZPROPERTYVALUE;
      return $this;
    }

}
