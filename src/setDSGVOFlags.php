<?php

namespace Axess\Dci4Wtp;

class setDSGVOFlags
{

    /**
     * @var D4WTPSETDSGVOFLAGSREQUEST $i_ctsetDSGVOFlagsReq
     */
    protected $i_ctsetDSGVOFlagsReq = null;

    /**
     * @param D4WTPSETDSGVOFLAGSREQUEST $i_ctsetDSGVOFlagsReq
     */
    public function __construct($i_ctsetDSGVOFlagsReq)
    {
      $this->i_ctsetDSGVOFlagsReq = $i_ctsetDSGVOFlagsReq;
    }

    /**
     * @return D4WTPSETDSGVOFLAGSREQUEST
     */
    public function getI_ctsetDSGVOFlagsReq()
    {
      return $this->i_ctsetDSGVOFlagsReq;
    }

    /**
     * @param D4WTPSETDSGVOFLAGSREQUEST $i_ctsetDSGVOFlagsReq
     * @return \Axess\Dci4Wtp\setDSGVOFlags
     */
    public function setI_ctsetDSGVOFlagsReq($i_ctsetDSGVOFlagsReq)
    {
      $this->i_ctsetDSGVOFlagsReq = $i_ctsetDSGVOFlagsReq;
      return $this;
    }

}
