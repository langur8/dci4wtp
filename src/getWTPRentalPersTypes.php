<?php

namespace Axess\Dci4Wtp;

class getWTPRentalPersTypes
{

    /**
     * @var D4WTPGETRENTALPERSTYPESREQ $i_ctWTPRentalPersTypesReq
     */
    protected $i_ctWTPRentalPersTypesReq = null;

    /**
     * @param D4WTPGETRENTALPERSTYPESREQ $i_ctWTPRentalPersTypesReq
     */
    public function __construct($i_ctWTPRentalPersTypesReq)
    {
      $this->i_ctWTPRentalPersTypesReq = $i_ctWTPRentalPersTypesReq;
    }

    /**
     * @return D4WTPGETRENTALPERSTYPESREQ
     */
    public function getI_ctWTPRentalPersTypesReq()
    {
      return $this->i_ctWTPRentalPersTypesReq;
    }

    /**
     * @param D4WTPGETRENTALPERSTYPESREQ $i_ctWTPRentalPersTypesReq
     * @return \Axess\Dci4Wtp\getWTPRentalPersTypes
     */
    public function setI_ctWTPRentalPersTypesReq($i_ctWTPRentalPersTypesReq)
    {
      $this->i_ctWTPRentalPersTypesReq = $i_ctWTPRentalPersTypesReq;
      return $this;
    }

}
