<?php

namespace Axess\Dci4Wtp;

class D4WTPPERSONTYPE2RESULT
{

    /**
     * @var ArrayOfD4WTPPERSONTYPE2 $ACTPERSONTYPES2
     */
    protected $ACTPERSONTYPES2 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPERSONTYPE2
     */
    public function getACTPERSONTYPES2()
    {
      return $this->ACTPERSONTYPES2;
    }

    /**
     * @param ArrayOfD4WTPPERSONTYPE2 $ACTPERSONTYPES2
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE2RESULT
     */
    public function setACTPERSONTYPES2($ACTPERSONTYPES2)
    {
      $this->ACTPERSONTYPES2 = $ACTPERSONTYPES2;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPE2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
