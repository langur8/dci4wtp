<?php

namespace Axess\Dci4Wtp;

class getPackageTariffList8Response
{

    /**
     * @var D4WTPPKGTARIFFLIST8RESULT $getPackageTariffList8Result
     */
    protected $getPackageTariffList8Result = null;

    /**
     * @param D4WTPPKGTARIFFLIST8RESULT $getPackageTariffList8Result
     */
    public function __construct($getPackageTariffList8Result)
    {
      $this->getPackageTariffList8Result = $getPackageTariffList8Result;
    }

    /**
     * @return D4WTPPKGTARIFFLIST8RESULT
     */
    public function getGetPackageTariffList8Result()
    {
      return $this->getPackageTariffList8Result;
    }

    /**
     * @param D4WTPPKGTARIFFLIST8RESULT $getPackageTariffList8Result
     * @return \Axess\Dci4Wtp\getPackageTariffList8Response
     */
    public function setGetPackageTariffList8Result($getPackageTariffList8Result)
    {
      $this->getPackageTariffList8Result = $getPackageTariffList8Result;
      return $this;
    }

}
