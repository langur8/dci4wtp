<?php

namespace Axess\Dci4Wtp;

class D4WTPGETRECEIPTDATAREQUEST2
{

    /**
     * @var float $BCOMPANYISBUYER
     */
    protected $BCOMPANYISBUYER = null;

    /**
     * @var float $BISRECEIPTDUPLICATE
     */
    protected $BISRECEIPTDUPLICATE = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NRECEIPTTYPENO
     */
    protected $NRECEIPTTYPENO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBCOMPANYISBUYER()
    {
      return $this->BCOMPANYISBUYER;
    }

    /**
     * @param float $BCOMPANYISBUYER
     * @return \Axess\Dci4Wtp\D4WTPGETRECEIPTDATAREQUEST2
     */
    public function setBCOMPANYISBUYER($BCOMPANYISBUYER)
    {
      $this->BCOMPANYISBUYER = $BCOMPANYISBUYER;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISRECEIPTDUPLICATE()
    {
      return $this->BISRECEIPTDUPLICATE;
    }

    /**
     * @param float $BISRECEIPTDUPLICATE
     * @return \Axess\Dci4Wtp\D4WTPGETRECEIPTDATAREQUEST2
     */
    public function setBISRECEIPTDUPLICATE($BISRECEIPTDUPLICATE)
    {
      $this->BISRECEIPTDUPLICATE = $BISRECEIPTDUPLICATE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETRECEIPTDATAREQUEST2
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETRECEIPTDATAREQUEST2
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRECEIPTTYPENO()
    {
      return $this->NRECEIPTTYPENO;
    }

    /**
     * @param float $NRECEIPTTYPENO
     * @return \Axess\Dci4Wtp\D4WTPGETRECEIPTDATAREQUEST2
     */
    public function setNRECEIPTTYPENO($NRECEIPTTYPENO)
    {
      $this->NRECEIPTTYPENO = $NRECEIPTTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETRECEIPTDATAREQUEST2
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPGETRECEIPTDATAREQUEST2
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

}
