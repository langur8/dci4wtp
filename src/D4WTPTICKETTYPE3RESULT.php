<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETTYPE3RESULT
{

    /**
     * @var ArrayOfD4WTPTICKETTYPE3 $ACTTICKETTYPES3
     */
    protected $ACTTICKETTYPES3 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKETTYPE3
     */
    public function getACTTICKETTYPES3()
    {
      return $this->ACTTICKETTYPES3;
    }

    /**
     * @param ArrayOfD4WTPTICKETTYPE3 $ACTTICKETTYPES3
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3RESULT
     */
    public function setACTTICKETTYPES3($ACTTICKETTYPES3)
    {
      $this->ACTTICKETTYPES3 = $ACTTICKETTYPES3;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE3RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
