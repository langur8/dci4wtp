<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSHOPCARTFOUND implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSHOPCARTFOUND[] $D4WTPSHOPCARTFOUND
     */
    protected $D4WTPSHOPCARTFOUND = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSHOPCARTFOUND[]
     */
    public function getD4WTPSHOPCARTFOUND()
    {
      return $this->D4WTPSHOPCARTFOUND;
    }

    /**
     * @param D4WTPSHOPCARTFOUND[] $D4WTPSHOPCARTFOUND
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSHOPCARTFOUND
     */
    public function setD4WTPSHOPCARTFOUND(array $D4WTPSHOPCARTFOUND = null)
    {
      $this->D4WTPSHOPCARTFOUND = $D4WTPSHOPCARTFOUND;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSHOPCARTFOUND[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSHOPCARTFOUND
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSHOPCARTFOUND[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSHOPCARTFOUND $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSHOPCARTFOUND[] = $value;
      } else {
        $this->D4WTPSHOPCARTFOUND[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSHOPCARTFOUND[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSHOPCARTFOUND Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSHOPCARTFOUND);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSHOPCARTFOUND);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSHOPCARTFOUND);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSHOPCARTFOUND);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSHOPCARTFOUND Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSHOPCARTFOUND);
    }

}
