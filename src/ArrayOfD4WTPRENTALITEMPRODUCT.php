<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPRENTALITEMPRODUCT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPRENTALITEMPRODUCT[] $D4WTPRENTALITEMPRODUCT
     */
    protected $D4WTPRENTALITEMPRODUCT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPRENTALITEMPRODUCT[]
     */
    public function getD4WTPRENTALITEMPRODUCT()
    {
      return $this->D4WTPRENTALITEMPRODUCT;
    }

    /**
     * @param D4WTPRENTALITEMPRODUCT[] $D4WTPRENTALITEMPRODUCT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPRENTALITEMPRODUCT
     */
    public function setD4WTPRENTALITEMPRODUCT(array $D4WTPRENTALITEMPRODUCT = null)
    {
      $this->D4WTPRENTALITEMPRODUCT = $D4WTPRENTALITEMPRODUCT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPRENTALITEMPRODUCT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPRENTALITEMPRODUCT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPRENTALITEMPRODUCT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPRENTALITEMPRODUCT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPRENTALITEMPRODUCT[] = $value;
      } else {
        $this->D4WTPRENTALITEMPRODUCT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPRENTALITEMPRODUCT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPRENTALITEMPRODUCT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPRENTALITEMPRODUCT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPRENTALITEMPRODUCT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPRENTALITEMPRODUCT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPRENTALITEMPRODUCT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPRENTALITEMPRODUCT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPRENTALITEMPRODUCT);
    }

}
