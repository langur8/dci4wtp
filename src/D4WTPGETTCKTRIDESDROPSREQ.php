<?php

namespace Axess\Dci4Wtp;

class D4WTPGETTCKTRIDESDROPSREQ
{

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NUNICODENO
     */
    protected $NUNICODENO = null;

    /**
     * @var string $SZOCCPERMISSIONHANDLE
     */
    protected $SZOCCPERMISSIONHANDLE = null;

    /**
     * @var string $SZVALIDDATE
     */
    protected $SZVALIDDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTRIDESDROPSREQ
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTRIDESDROPSREQ
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTRIDESDROPSREQ
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTRIDESDROPSREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENO()
    {
      return $this->NUNICODENO;
    }

    /**
     * @param float $NUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTRIDESDROPSREQ
     */
    public function setNUNICODENO($NUNICODENO)
    {
      $this->NUNICODENO = $NUNICODENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZOCCPERMISSIONHANDLE()
    {
      return $this->SZOCCPERMISSIONHANDLE;
    }

    /**
     * @param string $SZOCCPERMISSIONHANDLE
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTRIDESDROPSREQ
     */
    public function setSZOCCPERMISSIONHANDLE($SZOCCPERMISSIONHANDLE)
    {
      $this->SZOCCPERMISSIONHANDLE = $SZOCCPERMISSIONHANDLE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDDATE()
    {
      return $this->SZVALIDDATE;
    }

    /**
     * @param string $SZVALIDDATE
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTRIDESDROPSREQ
     */
    public function setSZVALIDDATE($SZVALIDDATE)
    {
      $this->SZVALIDDATE = $SZVALIDDATE;
      return $this;
    }

}
