<?php

namespace Axess\Dci4Wtp;

class getTicketSalesData3
{

    /**
     * @var D4WTPSALESDATAREQUEST3 $i_ctSalesDataReq
     */
    protected $i_ctSalesDataReq = null;

    /**
     * @param D4WTPSALESDATAREQUEST3 $i_ctSalesDataReq
     */
    public function __construct($i_ctSalesDataReq)
    {
      $this->i_ctSalesDataReq = $i_ctSalesDataReq;
    }

    /**
     * @return D4WTPSALESDATAREQUEST3
     */
    public function getI_ctSalesDataReq()
    {
      return $this->i_ctSalesDataReq;
    }

    /**
     * @param D4WTPSALESDATAREQUEST3 $i_ctSalesDataReq
     * @return \Axess\Dci4Wtp\getTicketSalesData3
     */
    public function setI_ctSalesDataReq($i_ctSalesDataReq)
    {
      $this->i_ctSalesDataReq = $i_ctSalesDataReq;
      return $this;
    }

}
