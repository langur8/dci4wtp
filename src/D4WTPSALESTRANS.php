<?php

namespace Axess\Dci4Wtp;

class D4WTPSALESTRANS
{

    /**
     * @var ArrayOfD4WTPSALESDETAIL $ACTSALESDETAIL
     */
    protected $ACTSALESDETAIL = null;

    /**
     * @var float $FPRICE
     */
    protected $FPRICE = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NTRANSNR
     */
    protected $NTRANSNR = null;

    /**
     * @var float $NTRANSSTATUS
     */
    protected $NTRANSSTATUS = null;

    /**
     * @var string $SZBOOKINGOPTION
     */
    protected $SZBOOKINGOPTION = null;

    /**
     * @var string $SZISSUEDATE
     */
    protected $SZISSUEDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSALESDETAIL
     */
    public function getACTSALESDETAIL()
    {
      return $this->ACTSALESDETAIL;
    }

    /**
     * @param ArrayOfD4WTPSALESDETAIL $ACTSALESDETAIL
     * @return \Axess\Dci4Wtp\D4WTPSALESTRANS
     */
    public function setACTSALESDETAIL($ACTSALESDETAIL)
    {
      $this->ACTSALESDETAIL = $ACTSALESDETAIL;
      return $this;
    }

    /**
     * @return float
     */
    public function getFPRICE()
    {
      return $this->FPRICE;
    }

    /**
     * @param float $FPRICE
     * @return \Axess\Dci4Wtp\D4WTPSALESTRANS
     */
    public function setFPRICE($FPRICE)
    {
      $this->FPRICE = $FPRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPSALESTRANS
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPSALESTRANS
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNR()
    {
      return $this->NTRANSNR;
    }

    /**
     * @param float $NTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPSALESTRANS
     */
    public function setNTRANSNR($NTRANSNR)
    {
      $this->NTRANSNR = $NTRANSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSSTATUS()
    {
      return $this->NTRANSSTATUS;
    }

    /**
     * @param float $NTRANSSTATUS
     * @return \Axess\Dci4Wtp\D4WTPSALESTRANS
     */
    public function setNTRANSSTATUS($NTRANSSTATUS)
    {
      $this->NTRANSSTATUS = $NTRANSSTATUS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBOOKINGOPTION()
    {
      return $this->SZBOOKINGOPTION;
    }

    /**
     * @param string $SZBOOKINGOPTION
     * @return \Axess\Dci4Wtp\D4WTPSALESTRANS
     */
    public function setSZBOOKINGOPTION($SZBOOKINGOPTION)
    {
      $this->SZBOOKINGOPTION = $SZBOOKINGOPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZISSUEDATE()
    {
      return $this->SZISSUEDATE;
    }

    /**
     * @param string $SZISSUEDATE
     * @return \Axess\Dci4Wtp\D4WTPSALESTRANS
     */
    public function setSZISSUEDATE($SZISSUEDATE)
    {
      $this->SZISSUEDATE = $SZISSUEDATE;
      return $this;
    }

}
