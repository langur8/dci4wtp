<?php

namespace Axess\Dci4Wtp;

class checkIsSkiClubMemberResponse
{

    /**
     * @var D4WTPCHKSKICLUBMBRRESULT $checkIsSkiClubMemberResult
     */
    protected $checkIsSkiClubMemberResult = null;

    /**
     * @param D4WTPCHKSKICLUBMBRRESULT $checkIsSkiClubMemberResult
     */
    public function __construct($checkIsSkiClubMemberResult)
    {
      $this->checkIsSkiClubMemberResult = $checkIsSkiClubMemberResult;
    }

    /**
     * @return D4WTPCHKSKICLUBMBRRESULT
     */
    public function getCheckIsSkiClubMemberResult()
    {
      return $this->checkIsSkiClubMemberResult;
    }

    /**
     * @param D4WTPCHKSKICLUBMBRRESULT $checkIsSkiClubMemberResult
     * @return \Axess\Dci4Wtp\checkIsSkiClubMemberResponse
     */
    public function setCheckIsSkiClubMemberResult($checkIsSkiClubMemberResult)
    {
      $this->checkIsSkiClubMemberResult = $checkIsSkiClubMemberResult;
      return $this;
    }

}
