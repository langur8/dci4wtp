<?php

namespace Axess\Dci4Wtp;

class D4WTPPERSONTYPENO
{

    /**
     * @var float $NPERSTYPENO
     */
    protected $NPERSTYPENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPERSTYPENO()
    {
      return $this->NPERSTYPENO;
    }

    /**
     * @param float $NPERSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPERSONTYPENO
     */
    public function setNPERSTYPENO($NPERSTYPENO)
    {
      $this->NPERSTYPENO = $NPERSTYPENO;
      return $this;
    }

}
