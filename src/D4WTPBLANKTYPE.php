<?php

namespace Axess\Dci4Wtp;

class D4WTPBLANKTYPE
{

    /**
     * @var float $NBLANKTYPENR
     */
    protected $NBLANKTYPENR = null;

    /**
     * @var float $NDATACARRIERNO
     */
    protected $NDATACARRIERNO = null;

    /**
     * @var float $NPICBRIGHTNESS
     */
    protected $NPICBRIGHTNESS = null;

    /**
     * @var float $NPICORIENTATION
     */
    protected $NPICORIENTATION = null;

    /**
     * @var float $NPICPOSBOTTOM
     */
    protected $NPICPOSBOTTOM = null;

    /**
     * @var float $NPICPOSLEFT
     */
    protected $NPICPOSLEFT = null;

    /**
     * @var float $NPICPOSRIGHT
     */
    protected $NPICPOSRIGHT = null;

    /**
     * @var float $NPICPOSTOP
     */
    protected $NPICPOSTOP = null;

    /**
     * @var string $SZBLANKNAME
     */
    protected $SZBLANKNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNBLANKTYPENR()
    {
      return $this->NBLANKTYPENR;
    }

    /**
     * @param float $NBLANKTYPENR
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPE
     */
    public function setNBLANKTYPENR($NBLANKTYPENR)
    {
      $this->NBLANKTYPENR = $NBLANKTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATACARRIERNO()
    {
      return $this->NDATACARRIERNO;
    }

    /**
     * @param float $NDATACARRIERNO
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPE
     */
    public function setNDATACARRIERNO($NDATACARRIERNO)
    {
      $this->NDATACARRIERNO = $NDATACARRIERNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICBRIGHTNESS()
    {
      return $this->NPICBRIGHTNESS;
    }

    /**
     * @param float $NPICBRIGHTNESS
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPE
     */
    public function setNPICBRIGHTNESS($NPICBRIGHTNESS)
    {
      $this->NPICBRIGHTNESS = $NPICBRIGHTNESS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICORIENTATION()
    {
      return $this->NPICORIENTATION;
    }

    /**
     * @param float $NPICORIENTATION
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPE
     */
    public function setNPICORIENTATION($NPICORIENTATION)
    {
      $this->NPICORIENTATION = $NPICORIENTATION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICPOSBOTTOM()
    {
      return $this->NPICPOSBOTTOM;
    }

    /**
     * @param float $NPICPOSBOTTOM
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPE
     */
    public function setNPICPOSBOTTOM($NPICPOSBOTTOM)
    {
      $this->NPICPOSBOTTOM = $NPICPOSBOTTOM;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICPOSLEFT()
    {
      return $this->NPICPOSLEFT;
    }

    /**
     * @param float $NPICPOSLEFT
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPE
     */
    public function setNPICPOSLEFT($NPICPOSLEFT)
    {
      $this->NPICPOSLEFT = $NPICPOSLEFT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICPOSRIGHT()
    {
      return $this->NPICPOSRIGHT;
    }

    /**
     * @param float $NPICPOSRIGHT
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPE
     */
    public function setNPICPOSRIGHT($NPICPOSRIGHT)
    {
      $this->NPICPOSRIGHT = $NPICPOSRIGHT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICPOSTOP()
    {
      return $this->NPICPOSTOP;
    }

    /**
     * @param float $NPICPOSTOP
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPE
     */
    public function setNPICPOSTOP($NPICPOSTOP)
    {
      $this->NPICPOSTOP = $NPICPOSTOP;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBLANKNAME()
    {
      return $this->SZBLANKNAME;
    }

    /**
     * @param string $SZBLANKNAME
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPE
     */
    public function setSZBLANKNAME($SZBLANKNAME)
    {
      $this->SZBLANKNAME = $SZBLANKNAME;
      return $this;
    }

}
