<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPRODUCTORDER implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPRODUCTORDER[] $D4WTPPRODUCTORDER
     */
    protected $D4WTPPRODUCTORDER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPRODUCTORDER[]
     */
    public function getD4WTPPRODUCTORDER()
    {
      return $this->D4WTPPRODUCTORDER;
    }

    /**
     * @param D4WTPPRODUCTORDER[] $D4WTPPRODUCTORDER
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPRODUCTORDER
     */
    public function setD4WTPPRODUCTORDER(array $D4WTPPRODUCTORDER = null)
    {
      $this->D4WTPPRODUCTORDER = $D4WTPPRODUCTORDER;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPRODUCTORDER[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPRODUCTORDER
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPRODUCTORDER[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPRODUCTORDER $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPRODUCTORDER[] = $value;
      } else {
        $this->D4WTPPRODUCTORDER[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPRODUCTORDER[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPRODUCTORDER Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPRODUCTORDER);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPRODUCTORDER);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPRODUCTORDER);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPRODUCTORDER);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPRODUCTORDER Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPRODUCTORDER);
    }

}
