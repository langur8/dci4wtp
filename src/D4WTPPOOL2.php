<?php

namespace Axess\Dci4Wtp;

class D4WTPPOOL2
{

    /**
     * @var float $NICONSYSNR
     */
    protected $NICONSYSNR = null;

    /**
     * @var float $NICONTYPNR
     */
    protected $NICONTYPNR = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var string $SZICONNAME
     */
    protected $SZICONNAME = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNICONSYSNR()
    {
      return $this->NICONSYSNR;
    }

    /**
     * @param float $NICONSYSNR
     * @return \Axess\Dci4Wtp\D4WTPPOOL2
     */
    public function setNICONSYSNR($NICONSYSNR)
    {
      $this->NICONSYSNR = $NICONSYSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNICONTYPNR()
    {
      return $this->NICONTYPNR;
    }

    /**
     * @param float $NICONTYPNR
     * @return \Axess\Dci4Wtp\D4WTPPOOL2
     */
    public function setNICONTYPNR($NICONTYPNR)
    {
      $this->NICONTYPNR = $NICONTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPPOOL2
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPPOOL2
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZICONNAME()
    {
      return $this->SZICONNAME;
    }

    /**
     * @param string $SZICONNAME
     * @return \Axess\Dci4Wtp\D4WTPPOOL2
     */
    public function setSZICONNAME($SZICONNAME)
    {
      $this->SZICONNAME = $SZICONNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPPOOL2
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
