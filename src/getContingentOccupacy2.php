<?php

namespace Axess\Dci4Wtp;

class getContingentOccupacy2
{

    /**
     * @var D4WTPGETCNTNGNTOCUPACYREQ2 $i_request
     */
    protected $i_request = null;

    /**
     * @param D4WTPGETCNTNGNTOCUPACYREQ2 $i_request
     */
    public function __construct($i_request)
    {
      $this->i_request = $i_request;
    }

    /**
     * @return D4WTPGETCNTNGNTOCUPACYREQ2
     */
    public function getI_request()
    {
      return $this->i_request;
    }

    /**
     * @param D4WTPGETCNTNGNTOCUPACYREQ2 $i_request
     * @return \Axess\Dci4Wtp\getContingentOccupacy2
     */
    public function setI_request($i_request)
    {
      $this->i_request = $i_request;
      return $this;
    }

}
