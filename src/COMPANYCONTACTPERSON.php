<?php

namespace Axess\Dci4Wtp;

class COMPANYCONTACTPERSON
{

    /**
     * @var float $NCOMPANYNR
     */
    protected $NCOMPANYNR = null;

    /**
     * @var float $NCOMPANYPOSITIONNR
     */
    protected $NCOMPANYPOSITIONNR = null;

    /**
     * @var float $NCOMPANYPOSNR
     */
    protected $NCOMPANYPOSNR = null;

    /**
     * @var float $NCOMPANYPROJNR
     */
    protected $NCOMPANYPROJNR = null;

    /**
     * @var float $NSORTNR
     */
    protected $NSORTNR = null;

    /**
     * @var float $NWTPPROFILNR
     */
    protected $NWTPPROFILNR = null;

    /**
     * @var string $SZDATEOFBIRTH
     */
    protected $SZDATEOFBIRTH = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZGENDER
     */
    protected $SZGENDER = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZMOBILENR
     */
    protected $SZMOBILENR = null;

    /**
     * @var string $SZSALUTATION
     */
    protected $SZSALUTATION = null;

    /**
     * @var string $SZTELEXTENSION
     */
    protected $SZTELEXTENSION = null;

    /**
     * @var string $SZTITEL
     */
    protected $SZTITEL = null;

    /**
     * @var string $SZWTPPASSWORD
     */
    protected $SZWTPPASSWORD = null;

    /**
     * @var string $SZWTPUSERNAME
     */
    protected $SZWTPUSERNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCOMPANYNR()
    {
      return $this->NCOMPANYNR;
    }

    /**
     * @param float $NCOMPANYNR
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setNCOMPANYNR($NCOMPANYNR)
    {
      $this->NCOMPANYNR = $NCOMPANYNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSITIONNR()
    {
      return $this->NCOMPANYPOSITIONNR;
    }

    /**
     * @param float $NCOMPANYPOSITIONNR
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setNCOMPANYPOSITIONNR($NCOMPANYPOSITIONNR)
    {
      $this->NCOMPANYPOSITIONNR = $NCOMPANYPOSITIONNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNR()
    {
      return $this->NCOMPANYPOSNR;
    }

    /**
     * @param float $NCOMPANYPOSNR
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setNCOMPANYPOSNR($NCOMPANYPOSNR)
    {
      $this->NCOMPANYPOSNR = $NCOMPANYPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNR()
    {
      return $this->NCOMPANYPROJNR;
    }

    /**
     * @param float $NCOMPANYPROJNR
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setNCOMPANYPROJNR($NCOMPANYPROJNR)
    {
      $this->NCOMPANYPROJNR = $NCOMPANYPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNR()
    {
      return $this->NSORTNR;
    }

    /**
     * @param float $NSORTNR
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setNSORTNR($NSORTNR)
    {
      $this->NSORTNR = $NSORTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILNR()
    {
      return $this->NWTPPROFILNR;
    }

    /**
     * @param float $NWTPPROFILNR
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setNWTPPROFILNR($NWTPPROFILNR)
    {
      $this->NWTPPROFILNR = $NWTPPROFILNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEOFBIRTH()
    {
      return $this->SZDATEOFBIRTH;
    }

    /**
     * @param string $SZDATEOFBIRTH
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setSZDATEOFBIRTH($SZDATEOFBIRTH)
    {
      $this->SZDATEOFBIRTH = $SZDATEOFBIRTH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGENDER()
    {
      return $this->SZGENDER;
    }

    /**
     * @param string $SZGENDER
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setSZGENDER($SZGENDER)
    {
      $this->SZGENDER = $SZGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMOBILENR()
    {
      return $this->SZMOBILENR;
    }

    /**
     * @param string $SZMOBILENR
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setSZMOBILENR($SZMOBILENR)
    {
      $this->SZMOBILENR = $SZMOBILENR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSALUTATION()
    {
      return $this->SZSALUTATION;
    }

    /**
     * @param string $SZSALUTATION
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setSZSALUTATION($SZSALUTATION)
    {
      $this->SZSALUTATION = $SZSALUTATION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTELEXTENSION()
    {
      return $this->SZTELEXTENSION;
    }

    /**
     * @param string $SZTELEXTENSION
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setSZTELEXTENSION($SZTELEXTENSION)
    {
      $this->SZTELEXTENSION = $SZTELEXTENSION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTITEL()
    {
      return $this->SZTITEL;
    }

    /**
     * @param string $SZTITEL
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setSZTITEL($SZTITEL)
    {
      $this->SZTITEL = $SZTITEL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPPASSWORD()
    {
      return $this->SZWTPPASSWORD;
    }

    /**
     * @param string $SZWTPPASSWORD
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setSZWTPPASSWORD($SZWTPPASSWORD)
    {
      $this->SZWTPPASSWORD = $SZWTPPASSWORD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPUSERNAME()
    {
      return $this->SZWTPUSERNAME;
    }

    /**
     * @param string $SZWTPUSERNAME
     * @return \Axess\Dci4Wtp\COMPANYCONTACTPERSON
     */
    public function setSZWTPUSERNAME($SZWTPUSERNAME)
    {
      $this->SZWTPUSERNAME = $SZWTPUSERNAME;
      return $this;
    }

}
