<?php

namespace Axess\Dci4Wtp;

class modifyReservation2Response
{

    /**
     * @var D4WTPRESULT $modifyReservation2Result
     */
    protected $modifyReservation2Result = null;

    /**
     * @param D4WTPRESULT $modifyReservation2Result
     */
    public function __construct($modifyReservation2Result)
    {
      $this->modifyReservation2Result = $modifyReservation2Result;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getModifyReservation2Result()
    {
      return $this->modifyReservation2Result;
    }

    /**
     * @param D4WTPRESULT $modifyReservation2Result
     * @return \Axess\Dci4Wtp\modifyReservation2Response
     */
    public function setModifyReservation2Result($modifyReservation2Result)
    {
      $this->modifyReservation2Result = $modifyReservation2Result;
      return $this;
    }

}
