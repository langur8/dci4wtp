<?php

namespace Axess\Dci4Wtp;

class checkIsSkiClubMember
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var string $i_szWTPNo
     */
    protected $i_szWTPNo = null;

    /**
     * @param float $i_nSessionID
     * @param string $i_szWTPNo
     */
    public function __construct($i_nSessionID, $i_szWTPNo)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_szWTPNo = $i_szWTPNo;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\checkIsSkiClubMember
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szWTPNo()
    {
      return $this->i_szWTPNo;
    }

    /**
     * @param string $i_szWTPNo
     * @return \Axess\Dci4Wtp\checkIsSkiClubMember
     */
    public function setI_szWTPNo($i_szWTPNo)
    {
      $this->i_szWTPNo = $i_szWTPNo;
      return $this;
    }

}
