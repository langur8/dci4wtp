<?php

namespace Axess\Dci4Wtp;

class D4WTPEMONEYACCOUNTDETAILSRES
{

    /**
     * @var ArrayOfD4WTPAXCOUNTMEMBER $ACTAXCOUNTMEMBER
     */
    protected $ACTAXCOUNTMEMBER = null;

    /**
     * @var float $BISCLOSED
     */
    protected $BISCLOSED = null;

    /**
     * @var float $FACCOUNTBALANCE
     */
    protected $FACCOUNTBALANCE = null;

    /**
     * @var float $NACCOUNTNR
     */
    protected $NACCOUNTNR = null;

    /**
     * @var float $NBANKNR
     */
    protected $NBANKNR = null;

    /**
     * @var float $NBRANCHNR
     */
    protected $NBRANCHNR = null;

    /**
     * @var float $NCOMPNR
     */
    protected $NCOMPNR = null;

    /**
     * @var float $NDESKNR
     */
    protected $NDESKNR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NTYPE
     */
    protected $NTYPE = null;

    /**
     * @var string $SZCLOSINGDATE
     */
    protected $SZCLOSINGDATE = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    /**
     * @var string $SZOPENINGDATE
     */
    protected $SZOPENINGDATE = null;

    /**
     * @var string $SZWTPNO
     */
    protected $SZWTPNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPAXCOUNTMEMBER
     */
    public function getACTAXCOUNTMEMBER()
    {
      return $this->ACTAXCOUNTMEMBER;
    }

    /**
     * @param ArrayOfD4WTPAXCOUNTMEMBER $ACTAXCOUNTMEMBER
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setACTAXCOUNTMEMBER($ACTAXCOUNTMEMBER)
    {
      $this->ACTAXCOUNTMEMBER = $ACTAXCOUNTMEMBER;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISCLOSED()
    {
      return $this->BISCLOSED;
    }

    /**
     * @param float $BISCLOSED
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setBISCLOSED($BISCLOSED)
    {
      $this->BISCLOSED = $BISCLOSED;
      return $this;
    }

    /**
     * @return float
     */
    public function getFACCOUNTBALANCE()
    {
      return $this->FACCOUNTBALANCE;
    }

    /**
     * @param float $FACCOUNTBALANCE
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setFACCOUNTBALANCE($FACCOUNTBALANCE)
    {
      $this->FACCOUNTBALANCE = $FACCOUNTBALANCE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNACCOUNTNR()
    {
      return $this->NACCOUNTNR;
    }

    /**
     * @param float $NACCOUNTNR
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setNACCOUNTNR($NACCOUNTNR)
    {
      $this->NACCOUNTNR = $NACCOUNTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBANKNR()
    {
      return $this->NBANKNR;
    }

    /**
     * @param float $NBANKNR
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setNBANKNR($NBANKNR)
    {
      $this->NBANKNR = $NBANKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBRANCHNR()
    {
      return $this->NBRANCHNR;
    }

    /**
     * @param float $NBRANCHNR
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setNBRANCHNR($NBRANCHNR)
    {
      $this->NBRANCHNR = $NBRANCHNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPNR()
    {
      return $this->NCOMPNR;
    }

    /**
     * @param float $NCOMPNR
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setNCOMPNR($NCOMPNR)
    {
      $this->NCOMPNR = $NCOMPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDESKNR()
    {
      return $this->NDESKNR;
    }

    /**
     * @param float $NDESKNR
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setNDESKNR($NDESKNR)
    {
      $this->NDESKNR = $NDESKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTYPE()
    {
      return $this->NTYPE;
    }

    /**
     * @param float $NTYPE
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setNTYPE($NTYPE)
    {
      $this->NTYPE = $NTYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCLOSINGDATE()
    {
      return $this->SZCLOSINGDATE;
    }

    /**
     * @param string $SZCLOSINGDATE
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setSZCLOSINGDATE($SZCLOSINGDATE)
    {
      $this->SZCLOSINGDATE = $SZCLOSINGDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZOPENINGDATE()
    {
      return $this->SZOPENINGDATE;
    }

    /**
     * @param string $SZOPENINGDATE
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setSZOPENINGDATE($SZOPENINGDATE)
    {
      $this->SZOPENINGDATE = $SZOPENINGDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNO()
    {
      return $this->SZWTPNO;
    }

    /**
     * @param string $SZWTPNO
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNTDETAILSRES
     */
    public function setSZWTPNO($SZWTPNO)
    {
      $this->SZWTPNO = $SZWTPNO;
      return $this;
    }

}
