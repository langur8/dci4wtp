<?php

namespace Axess\Dci4Wtp;

class D4WTPCHECKPROMOCODES
{

    /**
     * @var D4WTPPRODUCT $CTPRODUCT
     */
    protected $CTPRODUCT = null;

    /**
     * @var string $SZPROMOCODE
     */
    protected $SZPROMOCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPRODUCT
     */
    public function getCTPRODUCT()
    {
      return $this->CTPRODUCT;
    }

    /**
     * @param D4WTPPRODUCT $CTPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODES
     */
    public function setCTPRODUCT($CTPRODUCT)
    {
      $this->CTPRODUCT = $CTPRODUCT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPROMOCODE()
    {
      return $this->SZPROMOCODE;
    }

    /**
     * @param string $SZPROMOCODE
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODES
     */
    public function setSZPROMOCODE($SZPROMOCODE)
    {
      $this->SZPROMOCODE = $SZPROMOCODE;
      return $this;
    }

}
