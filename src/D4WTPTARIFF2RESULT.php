<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFF2RESULT
{

    /**
     * @var float $FDISCOUNTABS
     */
    protected $FDISCOUNTABS = null;

    /**
     * @var float $FDISCOUNTPERCENT
     */
    protected $FDISCOUNTPERCENT = null;

    /**
     * @var float $FPRICEDISCOUNTED
     */
    protected $FPRICEDISCOUNTED = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var string $SZCURRENCY
     */
    protected $SZCURRENCY = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFDISCOUNTABS()
    {
      return $this->FDISCOUNTABS;
    }

    /**
     * @param float $FDISCOUNTABS
     * @return \Axess\Dci4Wtp\D4WTPTARIFF2RESULT
     */
    public function setFDISCOUNTABS($FDISCOUNTABS)
    {
      $this->FDISCOUNTABS = $FDISCOUNTABS;
      return $this;
    }

    /**
     * @return float
     */
    public function getFDISCOUNTPERCENT()
    {
      return $this->FDISCOUNTPERCENT;
    }

    /**
     * @param float $FDISCOUNTPERCENT
     * @return \Axess\Dci4Wtp\D4WTPTARIFF2RESULT
     */
    public function setFDISCOUNTPERCENT($FDISCOUNTPERCENT)
    {
      $this->FDISCOUNTPERCENT = $FDISCOUNTPERCENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFPRICEDISCOUNTED()
    {
      return $this->FPRICEDISCOUNTED;
    }

    /**
     * @param float $FPRICEDISCOUNTED
     * @return \Axess\Dci4Wtp\D4WTPTARIFF2RESULT
     */
    public function setFPRICEDISCOUNTED($FPRICEDISCOUNTED)
    {
      $this->FPRICEDISCOUNTED = $FPRICEDISCOUNTED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFF2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPTARIFF2RESULT
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCY()
    {
      return $this->SZCURRENCY;
    }

    /**
     * @param string $SZCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPTARIFF2RESULT
     */
    public function setSZCURRENCY($SZCURRENCY)
    {
      $this->SZCURRENCY = $SZCURRENCY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTARIFF2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPTARIFF2RESULT
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
