<?php

namespace Axess\Dci4Wtp;

class registerSwissPassSale
{

    /**
     * @var D4WTPSWISSPASSREGREQ $i_SwissPassRegReq
     */
    protected $i_SwissPassRegReq = null;

    /**
     * @param D4WTPSWISSPASSREGREQ $i_SwissPassRegReq
     */
    public function __construct($i_SwissPassRegReq)
    {
      $this->i_SwissPassRegReq = $i_SwissPassRegReq;
    }

    /**
     * @return D4WTPSWISSPASSREGREQ
     */
    public function getI_SwissPassRegReq()
    {
      return $this->i_SwissPassRegReq;
    }

    /**
     * @param D4WTPSWISSPASSREGREQ $i_SwissPassRegReq
     * @return \Axess\Dci4Wtp\registerSwissPassSale
     */
    public function setI_SwissPassRegReq($i_SwissPassRegReq)
    {
      $this->i_SwissPassRegReq = $i_SwissPassRegReq;
      return $this;
    }

}
