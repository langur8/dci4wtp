<?php

namespace Axess\Dci4Wtp;

class D4WTPGETOPENSHIFTSRESULT
{

    /**
     * @var ArrayOfD4WTPCASHIERSHIFT $ACTCASHIERSHIFTS
     */
    protected $ACTCASHIERSHIFTS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCASHIERSHIFT
     */
    public function getACTCASHIERSHIFTS()
    {
      return $this->ACTCASHIERSHIFTS;
    }

    /**
     * @param ArrayOfD4WTPCASHIERSHIFT $ACTCASHIERSHIFTS
     * @return \Axess\Dci4Wtp\D4WTPGETOPENSHIFTSRESULT
     */
    public function setACTCASHIERSHIFTS($ACTCASHIERSHIFTS)
    {
      $this->ACTCASHIERSHIFTS = $ACTCASHIERSHIFTS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETOPENSHIFTSRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETOPENSHIFTSRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
