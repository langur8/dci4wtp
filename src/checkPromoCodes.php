<?php

namespace Axess\Dci4Wtp;

class checkPromoCodes
{

    /**
     * @var D4WTPCHECKPROMOCODESREQUEST $i_ctCheckPromoCodesReq
     */
    protected $i_ctCheckPromoCodesReq = null;

    /**
     * @param D4WTPCHECKPROMOCODESREQUEST $i_ctCheckPromoCodesReq
     */
    public function __construct($i_ctCheckPromoCodesReq)
    {
      $this->i_ctCheckPromoCodesReq = $i_ctCheckPromoCodesReq;
    }

    /**
     * @return D4WTPCHECKPROMOCODESREQUEST
     */
    public function getI_ctCheckPromoCodesReq()
    {
      return $this->i_ctCheckPromoCodesReq;
    }

    /**
     * @param D4WTPCHECKPROMOCODESREQUEST $i_ctCheckPromoCodesReq
     * @return \Axess\Dci4Wtp\checkPromoCodes
     */
    public function setI_ctCheckPromoCodesReq($i_ctCheckPromoCodesReq)
    {
      $this->i_ctCheckPromoCodesReq = $i_ctCheckPromoCodesReq;
      return $this;
    }

}
