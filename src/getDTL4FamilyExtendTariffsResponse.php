<?php

namespace Axess\Dci4Wtp;

class getDTL4FamilyExtendTariffsResponse
{

    /**
     * @var D4WTPGETADDDAYSTARIFFSRES $getDTL4FamilyExtendTariffsResult
     */
    protected $getDTL4FamilyExtendTariffsResult = null;

    /**
     * @param D4WTPGETADDDAYSTARIFFSRES $getDTL4FamilyExtendTariffsResult
     */
    public function __construct($getDTL4FamilyExtendTariffsResult)
    {
      $this->getDTL4FamilyExtendTariffsResult = $getDTL4FamilyExtendTariffsResult;
    }

    /**
     * @return D4WTPGETADDDAYSTARIFFSRES
     */
    public function getGetDTL4FamilyExtendTariffsResult()
    {
      return $this->getDTL4FamilyExtendTariffsResult;
    }

    /**
     * @param D4WTPGETADDDAYSTARIFFSRES $getDTL4FamilyExtendTariffsResult
     * @return \Axess\Dci4Wtp\getDTL4FamilyExtendTariffsResponse
     */
    public function setGetDTL4FamilyExtendTariffsResult($getDTL4FamilyExtendTariffsResult)
    {
      $this->getDTL4FamilyExtendTariffsResult = $getDTL4FamilyExtendTariffsResult;
      return $this;
    }

}
