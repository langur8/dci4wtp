<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPWTPNO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPWTPNO[] $D4WTPWTPNO
     */
    protected $D4WTPWTPNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPWTPNO[]
     */
    public function getD4WTPWTPNO()
    {
      return $this->D4WTPWTPNO;
    }

    /**
     * @param D4WTPWTPNO[] $D4WTPWTPNO
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPWTPNO
     */
    public function setD4WTPWTPNO(array $D4WTPWTPNO = null)
    {
      $this->D4WTPWTPNO = $D4WTPWTPNO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPWTPNO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPWTPNO
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPWTPNO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPWTPNO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPWTPNO[] = $value;
      } else {
        $this->D4WTPWTPNO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPWTPNO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPWTPNO Return the current element
     */
    public function current()
    {
      return current($this->D4WTPWTPNO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPWTPNO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPWTPNO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPWTPNO);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPWTPNO Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPWTPNO);
    }

}
