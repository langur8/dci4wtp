<?php

namespace Axess\Dci4Wtp;

class D4WTPGETARTICLELIST3RESULT
{

    /**
     * @var ArrayOfD4WTPARTICLELIST3 $ACTARTICLELIST
     */
    protected $ACTARTICLELIST = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPARTICLELIST3
     */
    public function getACTARTICLELIST()
    {
      return $this->ACTARTICLELIST;
    }

    /**
     * @param ArrayOfD4WTPARTICLELIST3 $ACTARTICLELIST
     * @return \Axess\Dci4Wtp\D4WTPGETARTICLELIST3RESULT
     */
    public function setACTARTICLELIST($ACTARTICLELIST)
    {
      $this->ACTARTICLELIST = $ACTARTICLELIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETARTICLELIST3RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETARTICLELIST3RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
