<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPFAMILYMEMBERDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPFAMILYMEMBERDATA[] $D4WTPFAMILYMEMBERDATA
     */
    protected $D4WTPFAMILYMEMBERDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPFAMILYMEMBERDATA[]
     */
    public function getD4WTPFAMILYMEMBERDATA()
    {
      return $this->D4WTPFAMILYMEMBERDATA;
    }

    /**
     * @param D4WTPFAMILYMEMBERDATA[] $D4WTPFAMILYMEMBERDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPFAMILYMEMBERDATA
     */
    public function setD4WTPFAMILYMEMBERDATA(array $D4WTPFAMILYMEMBERDATA = null)
    {
      $this->D4WTPFAMILYMEMBERDATA = $D4WTPFAMILYMEMBERDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPFAMILYMEMBERDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPFAMILYMEMBERDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPFAMILYMEMBERDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPFAMILYMEMBERDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPFAMILYMEMBERDATA[] = $value;
      } else {
        $this->D4WTPFAMILYMEMBERDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPFAMILYMEMBERDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPFAMILYMEMBERDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPFAMILYMEMBERDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPFAMILYMEMBERDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPFAMILYMEMBERDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPFAMILYMEMBERDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPFAMILYMEMBERDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPFAMILYMEMBERDATA);
    }

}
