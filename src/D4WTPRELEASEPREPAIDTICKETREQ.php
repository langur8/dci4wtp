<?php

namespace Axess\Dci4Wtp;

class D4WTPRELEASEPREPAIDTICKETREQ
{

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NSUBPROJNR
     */
    protected $NSUBPROJNR = null;

    /**
     * @var float $NUNDOGLOBAL
     */
    protected $NUNDOGLOBAL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPRELEASEPREPAIDTICKETREQ
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPRELEASEPREPAIDTICKETREQ
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPRELEASEPREPAIDTICKETREQ
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPRELEASEPREPAIDTICKETREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBPROJNR()
    {
      return $this->NSUBPROJNR;
    }

    /**
     * @param float $NSUBPROJNR
     * @return \Axess\Dci4Wtp\D4WTPRELEASEPREPAIDTICKETREQ
     */
    public function setNSUBPROJNR($NSUBPROJNR)
    {
      $this->NSUBPROJNR = $NSUBPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNDOGLOBAL()
    {
      return $this->NUNDOGLOBAL;
    }

    /**
     * @param float $NUNDOGLOBAL
     * @return \Axess\Dci4Wtp\D4WTPRELEASEPREPAIDTICKETREQ
     */
    public function setNUNDOGLOBAL($NUNDOGLOBAL)
    {
      $this->NUNDOGLOBAL = $NUNDOGLOBAL;
      return $this;
    }

}
