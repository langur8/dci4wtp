<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST8
{

    /**
     * @var ArrayOfD4WTPTARIFFLISTDAY8 $ACTTARIFFLISTDAY8
     */
    protected $ACTTARIFFLISTDAY8 = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLISTDAY8
     */
    public function getACTTARIFFLISTDAY8()
    {
      return $this->ACTTARIFFLISTDAY8;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLISTDAY8 $ACTTARIFFLISTDAY8
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST8
     */
    public function setACTTARIFFLISTDAY8($ACTTARIFFLISTDAY8)
    {
      $this->ACTTARIFFLISTDAY8 = $ACTTARIFFLISTDAY8;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST8
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
