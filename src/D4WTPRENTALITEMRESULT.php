<?php

namespace Axess\Dci4Wtp;

class D4WTPRENTALITEMRESULT
{

    /**
     * @var ArrayOfD4WTPRENTALITEM $ACTWTPRENTALITEM
     */
    protected $ACTWTPRENTALITEM = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPRENTALITEM
     */
    public function getACTWTPRENTALITEM()
    {
      return $this->ACTWTPRENTALITEM;
    }

    /**
     * @param ArrayOfD4WTPRENTALITEM $ACTWTPRENTALITEM
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMRESULT
     */
    public function setACTWTPRENTALITEM($ACTWTPRENTALITEM)
    {
      $this->ACTWTPRENTALITEM = $ACTWTPRENTALITEM;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
