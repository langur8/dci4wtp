<?php

namespace Axess\Dci4Wtp;

class D4WTPSTORETEMPTXTREQUEST
{

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var float $NKASSANR
     */
    protected $NKASSANR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZTEXTLINE1
     */
    protected $SZTEXTLINE1 = null;

    /**
     * @var string $SZTEXTLINE2
     */
    protected $SZTEXTLINE2 = null;

    /**
     * @var string $SZTEXTLINE3
     */
    protected $SZTEXTLINE3 = null;

    /**
     * @var string $SZTEXTLINE4
     */
    protected $SZTEXTLINE4 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPSTORETEMPTXTREQUEST
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKASSANR()
    {
      return $this->NKASSANR;
    }

    /**
     * @param float $NKASSANR
     * @return \Axess\Dci4Wtp\D4WTPSTORETEMPTXTREQUEST
     */
    public function setNKASSANR($NKASSANR)
    {
      $this->NKASSANR = $NKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPSTORETEMPTXTREQUEST
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSTORETEMPTXTREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTEXTLINE1()
    {
      return $this->SZTEXTLINE1;
    }

    /**
     * @param string $SZTEXTLINE1
     * @return \Axess\Dci4Wtp\D4WTPSTORETEMPTXTREQUEST
     */
    public function setSZTEXTLINE1($SZTEXTLINE1)
    {
      $this->SZTEXTLINE1 = $SZTEXTLINE1;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTEXTLINE2()
    {
      return $this->SZTEXTLINE2;
    }

    /**
     * @param string $SZTEXTLINE2
     * @return \Axess\Dci4Wtp\D4WTPSTORETEMPTXTREQUEST
     */
    public function setSZTEXTLINE2($SZTEXTLINE2)
    {
      $this->SZTEXTLINE2 = $SZTEXTLINE2;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTEXTLINE3()
    {
      return $this->SZTEXTLINE3;
    }

    /**
     * @param string $SZTEXTLINE3
     * @return \Axess\Dci4Wtp\D4WTPSTORETEMPTXTREQUEST
     */
    public function setSZTEXTLINE3($SZTEXTLINE3)
    {
      $this->SZTEXTLINE3 = $SZTEXTLINE3;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTEXTLINE4()
    {
      return $this->SZTEXTLINE4;
    }

    /**
     * @param string $SZTEXTLINE4
     * @return \Axess\Dci4Wtp\D4WTPSTORETEMPTXTREQUEST
     */
    public function setSZTEXTLINE4($SZTEXTLINE4)
    {
      $this->SZTEXTLINE4 = $SZTEXTLINE4;
      return $this;
    }

}
