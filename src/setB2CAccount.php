<?php

namespace Axess\Dci4Wtp;

class setB2CAccount
{

    /**
     * @var D4WTPSETB2CACCOUNTSREQUEST $i_ctSetB2CAccReq
     */
    protected $i_ctSetB2CAccReq = null;

    /**
     * @param D4WTPSETB2CACCOUNTSREQUEST $i_ctSetB2CAccReq
     */
    public function __construct($i_ctSetB2CAccReq)
    {
      $this->i_ctSetB2CAccReq = $i_ctSetB2CAccReq;
    }

    /**
     * @return D4WTPSETB2CACCOUNTSREQUEST
     */
    public function getI_ctSetB2CAccReq()
    {
      return $this->i_ctSetB2CAccReq;
    }

    /**
     * @param D4WTPSETB2CACCOUNTSREQUEST $i_ctSetB2CAccReq
     * @return \Axess\Dci4Wtp\setB2CAccount
     */
    public function setI_ctSetB2CAccReq($i_ctSetB2CAccReq)
    {
      $this->i_ctSetB2CAccReq = $i_ctSetB2CAccReq;
      return $this;
    }

}
