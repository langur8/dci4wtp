<?php

namespace Axess\Dci4Wtp;

class getTravelGroupData2Response
{

    /**
     * @var D4WTPGETTRAVELGRPDATARESULT2 $getTravelGroupData2Result
     */
    protected $getTravelGroupData2Result = null;

    /**
     * @param D4WTPGETTRAVELGRPDATARESULT2 $getTravelGroupData2Result
     */
    public function __construct($getTravelGroupData2Result)
    {
      $this->getTravelGroupData2Result = $getTravelGroupData2Result;
    }

    /**
     * @return D4WTPGETTRAVELGRPDATARESULT2
     */
    public function getGetTravelGroupData2Result()
    {
      return $this->getTravelGroupData2Result;
    }

    /**
     * @param D4WTPGETTRAVELGRPDATARESULT2 $getTravelGroupData2Result
     * @return \Axess\Dci4Wtp\getTravelGroupData2Response
     */
    public function setGetTravelGroupData2Result($getTravelGroupData2Result)
    {
      $this->getTravelGroupData2Result = $getTravelGroupData2Result;
      return $this;
    }

}
