<?php

namespace Axess\Dci4Wtp;

class getACPXImageWithImageResponse
{

    /**
     * @var D4WTPACPXIMAGERESULT $getACPXImageWithImageResult
     */
    protected $getACPXImageWithImageResult = null;

    /**
     * @param D4WTPACPXIMAGERESULT $getACPXImageWithImageResult
     */
    public function __construct($getACPXImageWithImageResult)
    {
      $this->getACPXImageWithImageResult = $getACPXImageWithImageResult;
    }

    /**
     * @return D4WTPACPXIMAGERESULT
     */
    public function getGetACPXImageWithImageResult()
    {
      return $this->getACPXImageWithImageResult;
    }

    /**
     * @param D4WTPACPXIMAGERESULT $getACPXImageWithImageResult
     * @return \Axess\Dci4Wtp\getACPXImageWithImageResponse
     */
    public function setGetACPXImageWithImageResult($getACPXImageWithImageResult)
    {
      $this->getACPXImageWithImageResult = $getACPXImageWithImageResult;
      return $this;
    }

}
