<?php

namespace Axess\Dci4Wtp;

class getCompany2Response
{

    /**
     * @var D4WTPGETCOMPANY2RESULT $getCompany2Result
     */
    protected $getCompany2Result = null;

    /**
     * @param D4WTPGETCOMPANY2RESULT $getCompany2Result
     */
    public function __construct($getCompany2Result)
    {
      $this->getCompany2Result = $getCompany2Result;
    }

    /**
     * @return D4WTPGETCOMPANY2RESULT
     */
    public function getGetCompany2Result()
    {
      return $this->getCompany2Result;
    }

    /**
     * @param D4WTPGETCOMPANY2RESULT $getCompany2Result
     * @return \Axess\Dci4Wtp\getCompany2Response
     */
    public function setGetCompany2Result($getCompany2Result)
    {
      $this->getCompany2Result = $getCompany2Result;
      return $this;
    }

}
