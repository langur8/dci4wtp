<?php

namespace Axess\Dci4Wtp;

class D4WTPCHANGEPERSDATA4REQUEST
{

    /**
     * @var ArrayOfD4WTPCUSTOMERADDRESS $ACTCUSTOMERSPECADDRESS
     */
    protected $ACTCUSTOMERSPECADDRESS = null;

    /**
     * @var ArrayOfD4WTPMEMBERTYPE $ACTMEMBERTYPE
     */
    protected $ACTMEMBERTYPE = null;

    /**
     * @var D4WTPMEMBER $CTMEMBER
     */
    protected $CTMEMBER = null;

    /**
     * @var D4WTPMEMBERDETAILS $CTMEMBERDETAILS
     */
    protected $CTMEMBERDETAILS = null;

    /**
     * @var D4WTPERSDATA4FAMILY $CTPERSONDATA
     */
    protected $CTPERSONDATA = null;

    /**
     * @var D4WTPPERSRENTAL $CTPERSRENTAL
     */
    protected $CTPERSRENTAL = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCUSTOMERADDRESS
     */
    public function getACTCUSTOMERSPECADDRESS()
    {
      return $this->ACTCUSTOMERSPECADDRESS;
    }

    /**
     * @param ArrayOfD4WTPCUSTOMERADDRESS $ACTCUSTOMERSPECADDRESS
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPERSDATA4REQUEST
     */
    public function setACTCUSTOMERSPECADDRESS($ACTCUSTOMERSPECADDRESS)
    {
      $this->ACTCUSTOMERSPECADDRESS = $ACTCUSTOMERSPECADDRESS;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPMEMBERTYPE
     */
    public function getACTMEMBERTYPE()
    {
      return $this->ACTMEMBERTYPE;
    }

    /**
     * @param ArrayOfD4WTPMEMBERTYPE $ACTMEMBERTYPE
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPERSDATA4REQUEST
     */
    public function setACTMEMBERTYPE($ACTMEMBERTYPE)
    {
      $this->ACTMEMBERTYPE = $ACTMEMBERTYPE;
      return $this;
    }

    /**
     * @return D4WTPMEMBER
     */
    public function getCTMEMBER()
    {
      return $this->CTMEMBER;
    }

    /**
     * @param D4WTPMEMBER $CTMEMBER
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPERSDATA4REQUEST
     */
    public function setCTMEMBER($CTMEMBER)
    {
      $this->CTMEMBER = $CTMEMBER;
      return $this;
    }

    /**
     * @return D4WTPMEMBERDETAILS
     */
    public function getCTMEMBERDETAILS()
    {
      return $this->CTMEMBERDETAILS;
    }

    /**
     * @param D4WTPMEMBERDETAILS $CTMEMBERDETAILS
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPERSDATA4REQUEST
     */
    public function setCTMEMBERDETAILS($CTMEMBERDETAILS)
    {
      $this->CTMEMBERDETAILS = $CTMEMBERDETAILS;
      return $this;
    }

    /**
     * @return D4WTPERSDATA4FAMILY
     */
    public function getCTPERSONDATA()
    {
      return $this->CTPERSONDATA;
    }

    /**
     * @param D4WTPERSDATA4FAMILY $CTPERSONDATA
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPERSDATA4REQUEST
     */
    public function setCTPERSONDATA($CTPERSONDATA)
    {
      $this->CTPERSONDATA = $CTPERSONDATA;
      return $this;
    }

    /**
     * @return D4WTPPERSRENTAL
     */
    public function getCTPERSRENTAL()
    {
      return $this->CTPERSRENTAL;
    }

    /**
     * @param D4WTPPERSRENTAL $CTPERSRENTAL
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPERSDATA4REQUEST
     */
    public function setCTPERSRENTAL($CTPERSRENTAL)
    {
      $this->CTPERSRENTAL = $CTPERSRENTAL;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPERSDATA4REQUEST
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPERSDATA4REQUEST
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPERSDATA4REQUEST
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPERSDATA4REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
