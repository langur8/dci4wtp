<?php

namespace Axess\Dci4Wtp;

class QRCodeManuallyAddedData
{

    /**
     * @var string $ValidFor
     */
    protected $ValidFor = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getValidFor()
    {
      return $this->ValidFor;
    }

    /**
     * @param string $ValidFor
     * @return \Axess\Dci4Wtp\QRCodeManuallyAddedData
     */
    public function setValidFor($ValidFor)
    {
      $this->ValidFor = $ValidFor;
      return $this;
    }

}
