<?php

namespace Axess\Dci4Wtp;

class subscribeForLimitCheck
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var string $clientConnectionId
     */
    protected $clientConnectionId = null;

    /**
     * @param float $i_nSessionID
     * @param string $clientConnectionId
     */
    public function __construct($i_nSessionID, $clientConnectionId)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->clientConnectionId = $clientConnectionId;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\subscribeForLimitCheck
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return string
     */
    public function getClientConnectionId()
    {
      return $this->clientConnectionId;
    }

    /**
     * @param string $clientConnectionId
     * @return \Axess\Dci4Wtp\subscribeForLimitCheck
     */
    public function setClientConnectionId($clientConnectionId)
    {
      $this->clientConnectionId = $clientConnectionId;
      return $this;
    }

}
