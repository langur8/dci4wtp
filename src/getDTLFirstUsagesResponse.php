<?php

namespace Axess\Dci4Wtp;

class getDTLFirstUsagesResponse
{

    /**
     * @var D4WTPGETDTLUSAGESRESULT $getDTLFirstUsagesResult
     */
    protected $getDTLFirstUsagesResult = null;

    /**
     * @param D4WTPGETDTLUSAGESRESULT $getDTLFirstUsagesResult
     */
    public function __construct($getDTLFirstUsagesResult)
    {
      $this->getDTLFirstUsagesResult = $getDTLFirstUsagesResult;
    }

    /**
     * @return D4WTPGETDTLUSAGESRESULT
     */
    public function getGetDTLFirstUsagesResult()
    {
      return $this->getDTLFirstUsagesResult;
    }

    /**
     * @param D4WTPGETDTLUSAGESRESULT $getDTLFirstUsagesResult
     * @return \Axess\Dci4Wtp\getDTLFirstUsagesResponse
     */
    public function setGetDTLFirstUsagesResult($getDTLFirstUsagesResult)
    {
      $this->getDTLFirstUsagesResult = $getDTLFirstUsagesResult;
      return $this;
    }

}
