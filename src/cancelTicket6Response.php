<?php

namespace Axess\Dci4Wtp;

class cancelTicket6Response
{

    /**
     * @var D4WTPCANCELTICKET5RESULT $cancelTicket6Result
     */
    protected $cancelTicket6Result = null;

    /**
     * @param D4WTPCANCELTICKET5RESULT $cancelTicket6Result
     */
    public function __construct($cancelTicket6Result)
    {
      $this->cancelTicket6Result = $cancelTicket6Result;
    }

    /**
     * @return D4WTPCANCELTICKET5RESULT
     */
    public function getCancelTicket6Result()
    {
      return $this->cancelTicket6Result;
    }

    /**
     * @param D4WTPCANCELTICKET5RESULT $cancelTicket6Result
     * @return \Axess\Dci4Wtp\cancelTicket6Response
     */
    public function setCancelTicket6Result($cancelTicket6Result)
    {
      $this->cancelTicket6Result = $cancelTicket6Result;
      return $this;
    }

}
