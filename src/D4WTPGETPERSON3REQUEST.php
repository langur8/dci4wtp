<?php

namespace Axess\Dci4Wtp;

class D4WTPGETPERSON3REQUEST
{

    /**
     * @var float $BRETURNMEMBERDATA
     */
    protected $BRETURNMEMBERDATA = null;

    /**
     * @var float $BRETURNRENTALDATA
     */
    protected $BRETURNRENTALDATA = null;

    /**
     * @var float $NFAMPERSNO
     */
    protected $NFAMPERSNO = null;

    /**
     * @var float $NFAMPOSNO
     */
    protected $NFAMPOSNO = null;

    /**
     * @var float $NFAMPROJNO
     */
    protected $NFAMPROJNO = null;

    /**
     * @var float $NFIRSTNROWS
     */
    protected $NFIRSTNROWS = null;

    /**
     * @var float $NMAXROW2FETCH
     */
    protected $NMAXROW2FETCH = null;

    /**
     * @var float $NMEMBERSHIPTYPENO
     */
    protected $NMEMBERSHIPTYPENO = null;

    /**
     * @var float $NMINROW2FETCH
     */
    protected $NMINROW2FETCH = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZCITY
     */
    protected $SZCITY = null;

    /**
     * @var string $SZDATEOFBIRTH
     */
    protected $SZDATEOFBIRTH = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZGENDER
     */
    protected $SZGENDER = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZMEMBERUSER
     */
    protected $SZMEMBERUSER = null;

    /**
     * @var string $SZSORTORDER
     */
    protected $SZSORTORDER = null;

    /**
     * @var string $SZSTREET
     */
    protected $SZSTREET = null;

    /**
     * @var string $SZZIPCODE
     */
    protected $SZZIPCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBRETURNMEMBERDATA()
    {
      return $this->BRETURNMEMBERDATA;
    }

    /**
     * @param float $BRETURNMEMBERDATA
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setBRETURNMEMBERDATA($BRETURNMEMBERDATA)
    {
      $this->BRETURNMEMBERDATA = $BRETURNMEMBERDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getBRETURNRENTALDATA()
    {
      return $this->BRETURNRENTALDATA;
    }

    /**
     * @param float $BRETURNRENTALDATA
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setBRETURNRENTALDATA($BRETURNRENTALDATA)
    {
      $this->BRETURNRENTALDATA = $BRETURNRENTALDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMPERSNO()
    {
      return $this->NFAMPERSNO;
    }

    /**
     * @param float $NFAMPERSNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setNFAMPERSNO($NFAMPERSNO)
    {
      $this->NFAMPERSNO = $NFAMPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMPOSNO()
    {
      return $this->NFAMPOSNO;
    }

    /**
     * @param float $NFAMPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setNFAMPOSNO($NFAMPOSNO)
    {
      $this->NFAMPOSNO = $NFAMPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFAMPROJNO()
    {
      return $this->NFAMPROJNO;
    }

    /**
     * @param float $NFAMPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setNFAMPROJNO($NFAMPROJNO)
    {
      $this->NFAMPROJNO = $NFAMPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRSTNROWS()
    {
      return $this->NFIRSTNROWS;
    }

    /**
     * @param float $NFIRSTNROWS
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setNFIRSTNROWS($NFIRSTNROWS)
    {
      $this->NFIRSTNROWS = $NFIRSTNROWS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMAXROW2FETCH()
    {
      return $this->NMAXROW2FETCH;
    }

    /**
     * @param float $NMAXROW2FETCH
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setNMAXROW2FETCH($NMAXROW2FETCH)
    {
      $this->NMAXROW2FETCH = $NMAXROW2FETCH;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMEMBERSHIPTYPENO()
    {
      return $this->NMEMBERSHIPTYPENO;
    }

    /**
     * @param float $NMEMBERSHIPTYPENO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setNMEMBERSHIPTYPENO($NMEMBERSHIPTYPENO)
    {
      $this->NMEMBERSHIPTYPENO = $NMEMBERSHIPTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMINROW2FETCH()
    {
      return $this->NMINROW2FETCH;
    }

    /**
     * @param float $NMINROW2FETCH
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setNMINROW2FETCH($NMINROW2FETCH)
    {
      $this->NMINROW2FETCH = $NMINROW2FETCH;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCITY()
    {
      return $this->SZCITY;
    }

    /**
     * @param string $SZCITY
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setSZCITY($SZCITY)
    {
      $this->SZCITY = $SZCITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEOFBIRTH()
    {
      return $this->SZDATEOFBIRTH;
    }

    /**
     * @param string $SZDATEOFBIRTH
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setSZDATEOFBIRTH($SZDATEOFBIRTH)
    {
      $this->SZDATEOFBIRTH = $SZDATEOFBIRTH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGENDER()
    {
      return $this->SZGENDER;
    }

    /**
     * @param string $SZGENDER
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setSZGENDER($SZGENDER)
    {
      $this->SZGENDER = $SZGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEMBERUSER()
    {
      return $this->SZMEMBERUSER;
    }

    /**
     * @param string $SZMEMBERUSER
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setSZMEMBERUSER($SZMEMBERUSER)
    {
      $this->SZMEMBERUSER = $SZMEMBERUSER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSORTORDER()
    {
      return $this->SZSORTORDER;
    }

    /**
     * @param string $SZSORTORDER
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setSZSORTORDER($SZSORTORDER)
    {
      $this->SZSORTORDER = $SZSORTORDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREET()
    {
      return $this->SZSTREET;
    }

    /**
     * @param string $SZSTREET
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setSZSTREET($SZSTREET)
    {
      $this->SZSTREET = $SZSTREET;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZZIPCODE()
    {
      return $this->SZZIPCODE;
    }

    /**
     * @param string $SZZIPCODE
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3REQUEST
     */
    public function setSZZIPCODE($SZZIPCODE)
    {
      $this->SZZIPCODE = $SZZIPCODE;
      return $this;
    }

}
