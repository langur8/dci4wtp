<?php

namespace Axess\Dci4Wtp;

class getCompanyInfo2Response
{

    /**
     * @var D4WTPCOMPANYINFORESULT2 $getCompanyInfo2Result
     */
    protected $getCompanyInfo2Result = null;

    /**
     * @param D4WTPCOMPANYINFORESULT2 $getCompanyInfo2Result
     */
    public function __construct($getCompanyInfo2Result)
    {
      $this->getCompanyInfo2Result = $getCompanyInfo2Result;
    }

    /**
     * @return D4WTPCOMPANYINFORESULT2
     */
    public function getGetCompanyInfo2Result()
    {
      return $this->getCompanyInfo2Result;
    }

    /**
     * @param D4WTPCOMPANYINFORESULT2 $getCompanyInfo2Result
     * @return \Axess\Dci4Wtp\getCompanyInfo2Response
     */
    public function setGetCompanyInfo2Result($getCompanyInfo2Result)
    {
      $this->getCompanyInfo2Result = $getCompanyInfo2Result;
      return $this;
    }

}
