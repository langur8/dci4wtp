<?php

namespace Axess\Dci4Wtp;

class setPayer2Response
{

    /**
     * @var D4WTPPAYER2RESULT $setPayer2Result
     */
    protected $setPayer2Result = null;

    /**
     * @param D4WTPPAYER2RESULT $setPayer2Result
     */
    public function __construct($setPayer2Result)
    {
      $this->setPayer2Result = $setPayer2Result;
    }

    /**
     * @return D4WTPPAYER2RESULT
     */
    public function getSetPayer2Result()
    {
      return $this->setPayer2Result;
    }

    /**
     * @param D4WTPPAYER2RESULT $setPayer2Result
     * @return \Axess\Dci4Wtp\setPayer2Response
     */
    public function setSetPayer2Result($setPayer2Result)
    {
      $this->setPayer2Result = $setPayer2Result;
      return $this;
    }

}
