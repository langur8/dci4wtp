<?php

namespace Axess\Dci4Wtp;

class D4WTPMSGTICKETRESDATA
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZEXTORDERNUMBER
     */
    protected $SZEXTORDERNUMBER = null;

    /**
     * @var string $SZMESSAGETYPE
     */
    protected $SZMESSAGETYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTORDERNUMBER()
    {
      return $this->SZEXTORDERNUMBER;
    }

    /**
     * @param string $SZEXTORDERNUMBER
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA
     */
    public function setSZEXTORDERNUMBER($SZEXTORDERNUMBER)
    {
      $this->SZEXTORDERNUMBER = $SZEXTORDERNUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMESSAGETYPE()
    {
      return $this->SZMESSAGETYPE;
    }

    /**
     * @param string $SZMESSAGETYPE
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA
     */
    public function setSZMESSAGETYPE($SZMESSAGETYPE)
    {
      $this->SZMESSAGETYPE = $SZMESSAGETYPE;
      return $this;
    }

}
