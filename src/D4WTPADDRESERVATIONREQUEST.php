<?php

namespace Axess\Dci4Wtp;

class D4WTPADDRESERVATIONREQUEST
{

    /**
     * @var ArrayOfD4WTPADDRESCONTINGENTLIST $ACTADDRESCONTINGENTLIST
     */
    protected $ACTADDRESCONTINGENTLIST = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZRESERVATIONREFERENCE
     */
    protected $SZRESERVATIONREFERENCE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDRESCONTINGENTLIST
     */
    public function getACTADDRESCONTINGENTLIST()
    {
      return $this->ACTADDRESCONTINGENTLIST;
    }

    /**
     * @param ArrayOfD4WTPADDRESCONTINGENTLIST $ACTADDRESCONTINGENTLIST
     * @return \Axess\Dci4Wtp\D4WTPADDRESERVATIONREQUEST
     */
    public function setACTADDRESCONTINGENTLIST($ACTADDRESCONTINGENTLIST)
    {
      $this->ACTADDRESCONTINGENTLIST = $ACTADDRESCONTINGENTLIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPADDRESERVATIONREQUEST
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPADDRESERVATIONREQUEST
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPADDRESERVATIONREQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPADDRESERVATIONREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRESERVATIONREFERENCE()
    {
      return $this->SZRESERVATIONREFERENCE;
    }

    /**
     * @param string $SZRESERVATIONREFERENCE
     * @return \Axess\Dci4Wtp\D4WTPADDRESERVATIONREQUEST
     */
    public function setSZRESERVATIONREFERENCE($SZRESERVATIONREFERENCE)
    {
      $this->SZRESERVATIONREFERENCE = $SZRESERVATIONREFERENCE;
      return $this;
    }

}
