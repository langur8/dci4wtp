<?php

namespace Axess\Dci4Wtp;

class getTicketBlockStatus
{

    /**
     * @var D4WTPGETTCKTBLCKSTATUSREQ $i_ctGetTcktBlckStatusReq
     */
    protected $i_ctGetTcktBlckStatusReq = null;

    /**
     * @param D4WTPGETTCKTBLCKSTATUSREQ $i_ctGetTcktBlckStatusReq
     */
    public function __construct($i_ctGetTcktBlckStatusReq)
    {
      $this->i_ctGetTcktBlckStatusReq = $i_ctGetTcktBlckStatusReq;
    }

    /**
     * @return D4WTPGETTCKTBLCKSTATUSREQ
     */
    public function getI_ctGetTcktBlckStatusReq()
    {
      return $this->i_ctGetTcktBlckStatusReq;
    }

    /**
     * @param D4WTPGETTCKTBLCKSTATUSREQ $i_ctGetTcktBlckStatusReq
     * @return \Axess\Dci4Wtp\getTicketBlockStatus
     */
    public function setI_ctGetTcktBlckStatusReq($i_ctGetTcktBlckStatusReq)
    {
      $this->i_ctGetTcktBlckStatusReq = $i_ctGetTcktBlckStatusReq;
      return $this;
    }

}
