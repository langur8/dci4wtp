<?php

namespace Axess\Dci4Wtp;

class changeEmoneyAccount
{

    /**
     * @var D4WTPEMONEYACCOUNTREQ $i_ctEmoneyAccountReq
     */
    protected $i_ctEmoneyAccountReq = null;

    /**
     * @param D4WTPEMONEYACCOUNTREQ $i_ctEmoneyAccountReq
     */
    public function __construct($i_ctEmoneyAccountReq)
    {
      $this->i_ctEmoneyAccountReq = $i_ctEmoneyAccountReq;
    }

    /**
     * @return D4WTPEMONEYACCOUNTREQ
     */
    public function getI_ctEmoneyAccountReq()
    {
      return $this->i_ctEmoneyAccountReq;
    }

    /**
     * @param D4WTPEMONEYACCOUNTREQ $i_ctEmoneyAccountReq
     * @return \Axess\Dci4Wtp\changeEmoneyAccount
     */
    public function setI_ctEmoneyAccountReq($i_ctEmoneyAccountReq)
    {
      $this->i_ctEmoneyAccountReq = $i_ctEmoneyAccountReq;
      return $this;
    }

}
