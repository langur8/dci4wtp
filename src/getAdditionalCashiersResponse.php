<?php

namespace Axess\Dci4Wtp;

class getAdditionalCashiersResponse
{

    /**
     * @var D4WTPGETADDCASHIERSRESULT $getAdditionalCashiersResult
     */
    protected $getAdditionalCashiersResult = null;

    /**
     * @param D4WTPGETADDCASHIERSRESULT $getAdditionalCashiersResult
     */
    public function __construct($getAdditionalCashiersResult)
    {
      $this->getAdditionalCashiersResult = $getAdditionalCashiersResult;
    }

    /**
     * @return D4WTPGETADDCASHIERSRESULT
     */
    public function getGetAdditionalCashiersResult()
    {
      return $this->getAdditionalCashiersResult;
    }

    /**
     * @param D4WTPGETADDCASHIERSRESULT $getAdditionalCashiersResult
     * @return \Axess\Dci4Wtp\getAdditionalCashiersResponse
     */
    public function setGetAdditionalCashiersResult($getAdditionalCashiersResult)
    {
      $this->getAdditionalCashiersResult = $getAdditionalCashiersResult;
      return $this;
    }

}
