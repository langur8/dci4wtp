<?php

namespace Axess\Dci4Wtp;

class findPrepaidTicketResponse
{

    /**
     * @var D4WTPREPAIDRESULT $findPrepaidTicketResult
     */
    protected $findPrepaidTicketResult = null;

    /**
     * @param D4WTPREPAIDRESULT $findPrepaidTicketResult
     */
    public function __construct($findPrepaidTicketResult)
    {
      $this->findPrepaidTicketResult = $findPrepaidTicketResult;
    }

    /**
     * @return D4WTPREPAIDRESULT
     */
    public function getFindPrepaidTicketResult()
    {
      return $this->findPrepaidTicketResult;
    }

    /**
     * @param D4WTPREPAIDRESULT $findPrepaidTicketResult
     * @return \Axess\Dci4Wtp\findPrepaidTicketResponse
     */
    public function setFindPrepaidTicketResult($findPrepaidTicketResult)
    {
      $this->findPrepaidTicketResult = $findPrepaidTicketResult;
      return $this;
    }

}
