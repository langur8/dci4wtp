<?php

namespace Axess\Dci4Wtp;

class D4WTPGETGRPSUBCONTINGENTREQ2
{

    /**
     * @var ArrayOfDATE $ACTDAYS
     */
    protected $ACTDAYS = null;

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NCONFIGNO
     */
    protected $NCONFIGNO = null;

    /**
     * @var float $NCONTINGENTNO
     */
    protected $NCONTINGENTNO = null;

    /**
     * @var float $NMINFREEPLACES
     */
    protected $NMINFREEPLACES = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZDAY
     */
    protected $SZDAY = null;

    /**
     * @var string $SZFROMDATE
     */
    protected $SZFROMDATE = null;

    /**
     * @var string $SZTODATE
     */
    protected $SZTODATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfDATE
     */
    public function getACTDAYS()
    {
      return $this->ACTDAYS;
    }

    /**
     * @param ArrayOfDATE $ACTDAYS
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENTREQ2
     */
    public function setACTDAYS($ACTDAYS)
    {
      $this->ACTDAYS = $ACTDAYS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENTREQ2
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENTREQ2
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENTREQ2
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONFIGNO()
    {
      return $this->NCONFIGNO;
    }

    /**
     * @param float $NCONFIGNO
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENTREQ2
     */
    public function setNCONFIGNO($NCONFIGNO)
    {
      $this->NCONFIGNO = $NCONFIGNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNO()
    {
      return $this->NCONTINGENTNO;
    }

    /**
     * @param float $NCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENTREQ2
     */
    public function setNCONTINGENTNO($NCONTINGENTNO)
    {
      $this->NCONTINGENTNO = $NCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMINFREEPLACES()
    {
      return $this->NMINFREEPLACES;
    }

    /**
     * @param float $NMINFREEPLACES
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENTREQ2
     */
    public function setNMINFREEPLACES($NMINFREEPLACES)
    {
      $this->NMINFREEPLACES = $NMINFREEPLACES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENTREQ2
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENTREQ2
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDAY()
    {
      return $this->SZDAY;
    }

    /**
     * @param string $SZDAY
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENTREQ2
     */
    public function setSZDAY($SZDAY)
    {
      $this->SZDAY = $SZDAY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFROMDATE()
    {
      return $this->SZFROMDATE;
    }

    /**
     * @param string $SZFROMDATE
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENTREQ2
     */
    public function setSZFROMDATE($SZFROMDATE)
    {
      $this->SZFROMDATE = $SZFROMDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTODATE()
    {
      return $this->SZTODATE;
    }

    /**
     * @param string $SZTODATE
     * @return \Axess\Dci4Wtp\D4WTPGETGRPSUBCONTINGENTREQ2
     */
    public function setSZTODATE($SZTODATE)
    {
      $this->SZTODATE = $SZTODATE;
      return $this;
    }

}
