<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPRODUCT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPRODUCT[] $D4WTPPRODUCT
     */
    protected $D4WTPPRODUCT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPRODUCT[]
     */
    public function getD4WTPPRODUCT()
    {
      return $this->D4WTPPRODUCT;
    }

    /**
     * @param D4WTPPRODUCT[] $D4WTPPRODUCT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPRODUCT
     */
    public function setD4WTPPRODUCT(array $D4WTPPRODUCT = null)
    {
      $this->D4WTPPRODUCT = $D4WTPPRODUCT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPRODUCT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPRODUCT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPRODUCT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPRODUCT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPRODUCT[] = $value;
      } else {
        $this->D4WTPPRODUCT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPRODUCT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPRODUCT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPRODUCT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPRODUCT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPRODUCT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPRODUCT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPRODUCT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPRODUCT);
    }

}
