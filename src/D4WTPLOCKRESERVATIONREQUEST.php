<?php

namespace Axess\Dci4Wtp;

class D4WTPLOCKRESERVATIONREQUEST
{

    /**
     * @var ArrayOfD4WTPLOCKRESERVATION $ACTLOCKRESERVATION
     */
    protected $ACTLOCKRESERVATION = null;

    /**
     * @var float $BGROUPFLAG
     */
    protected $BGROUPFLAG = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPLOCKRESERVATION
     */
    public function getACTLOCKRESERVATION()
    {
      return $this->ACTLOCKRESERVATION;
    }

    /**
     * @param ArrayOfD4WTPLOCKRESERVATION $ACTLOCKRESERVATION
     * @return \Axess\Dci4Wtp\D4WTPLOCKRESERVATIONREQUEST
     */
    public function setACTLOCKRESERVATION($ACTLOCKRESERVATION)
    {
      $this->ACTLOCKRESERVATION = $ACTLOCKRESERVATION;
      return $this;
    }

    /**
     * @return float
     */
    public function getBGROUPFLAG()
    {
      return $this->BGROUPFLAG;
    }

    /**
     * @param float $BGROUPFLAG
     * @return \Axess\Dci4Wtp\D4WTPLOCKRESERVATIONREQUEST
     */
    public function setBGROUPFLAG($BGROUPFLAG)
    {
      $this->BGROUPFLAG = $BGROUPFLAG;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPLOCKRESERVATIONREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
