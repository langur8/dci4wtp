<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPRIDESANDDROPS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPRIDESANDDROPS[] $D4WTPRIDESANDDROPS
     */
    protected $D4WTPRIDESANDDROPS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPRIDESANDDROPS[]
     */
    public function getD4WTPRIDESANDDROPS()
    {
      return $this->D4WTPRIDESANDDROPS;
    }

    /**
     * @param D4WTPRIDESANDDROPS[] $D4WTPRIDESANDDROPS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPRIDESANDDROPS
     */
    public function setD4WTPRIDESANDDROPS(array $D4WTPRIDESANDDROPS = null)
    {
      $this->D4WTPRIDESANDDROPS = $D4WTPRIDESANDDROPS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPRIDESANDDROPS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPRIDESANDDROPS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPRIDESANDDROPS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPRIDESANDDROPS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPRIDESANDDROPS[] = $value;
      } else {
        $this->D4WTPRIDESANDDROPS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPRIDESANDDROPS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPRIDESANDDROPS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPRIDESANDDROPS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPRIDESANDDROPS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPRIDESANDDROPS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPRIDESANDDROPS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPRIDESANDDROPS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPRIDESANDDROPS);
    }

}
