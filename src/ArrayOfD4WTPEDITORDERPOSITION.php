<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPEDITORDERPOSITION implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPEDITORDERPOSITION[] $D4WTPEDITORDERPOSITION
     */
    protected $D4WTPEDITORDERPOSITION = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPEDITORDERPOSITION[]
     */
    public function getD4WTPEDITORDERPOSITION()
    {
      return $this->D4WTPEDITORDERPOSITION;
    }

    /**
     * @param D4WTPEDITORDERPOSITION[] $D4WTPEDITORDERPOSITION
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPEDITORDERPOSITION
     */
    public function setD4WTPEDITORDERPOSITION(array $D4WTPEDITORDERPOSITION = null)
    {
      $this->D4WTPEDITORDERPOSITION = $D4WTPEDITORDERPOSITION;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPEDITORDERPOSITION[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPEDITORDERPOSITION
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPEDITORDERPOSITION[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPEDITORDERPOSITION $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPEDITORDERPOSITION[] = $value;
      } else {
        $this->D4WTPEDITORDERPOSITION[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPEDITORDERPOSITION[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPEDITORDERPOSITION Return the current element
     */
    public function current()
    {
      return current($this->D4WTPEDITORDERPOSITION);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPEDITORDERPOSITION);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPEDITORDERPOSITION);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPEDITORDERPOSITION);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPEDITORDERPOSITION Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPEDITORDERPOSITION);
    }

}
