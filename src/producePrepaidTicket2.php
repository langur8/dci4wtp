<?php

namespace Axess\Dci4Wtp;

class producePrepaidTicket2
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var string $i_szCodingMode
     */
    protected $i_szCodingMode = null;

    /**
     * @var string $i_szDriverType
     */
    protected $i_szDriverType = null;

    /**
     * @var string $i_szDCContent
     */
    protected $i_szDCContent = null;

    /**
     * @var string $i_szWTPNo
     */
    protected $i_szWTPNo = null;

    /**
     * @var string $i_szExtOrderNr
     */
    protected $i_szExtOrderNr = null;

    /**
     * @var float $i_NPROJNR
     */
    protected $i_NPROJNR = null;

    /**
     * @var float $i_nPOSNr
     */
    protected $i_nPOSNr = null;

    /**
     * @var float $i_nJournalNr
     */
    protected $i_nJournalNr = null;

    /**
     * @var float $i_nPickupBoxNr
     */
    protected $i_nPickupBoxNr = null;

    /**
     * @param float $i_nSessionID
     * @param string $i_szCodingMode
     * @param string $i_szDriverType
     * @param string $i_szDCContent
     * @param string $i_szWTPNo
     * @param string $i_szExtOrderNr
     * @param float $i_NPROJNR
     * @param float $i_nPOSNr
     * @param float $i_nJournalNr
     * @param float $i_nPickupBoxNr
     */
    public function __construct($i_nSessionID, $i_szCodingMode, $i_szDriverType, $i_szDCContent, $i_szWTPNo, $i_szExtOrderNr, $i_NPROJNR, $i_nPOSNr, $i_nJournalNr, $i_nPickupBoxNr)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_szCodingMode = $i_szCodingMode;
      $this->i_szDriverType = $i_szDriverType;
      $this->i_szDCContent = $i_szDCContent;
      $this->i_szWTPNo = $i_szWTPNo;
      $this->i_szExtOrderNr = $i_szExtOrderNr;
      $this->i_NPROJNR = $i_NPROJNR;
      $this->i_nPOSNr = $i_nPOSNr;
      $this->i_nJournalNr = $i_nJournalNr;
      $this->i_nPickupBoxNr = $i_nPickupBoxNr;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\producePrepaidTicket2
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szCodingMode()
    {
      return $this->i_szCodingMode;
    }

    /**
     * @param string $i_szCodingMode
     * @return \Axess\Dci4Wtp\producePrepaidTicket2
     */
    public function setI_szCodingMode($i_szCodingMode)
    {
      $this->i_szCodingMode = $i_szCodingMode;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szDriverType()
    {
      return $this->i_szDriverType;
    }

    /**
     * @param string $i_szDriverType
     * @return \Axess\Dci4Wtp\producePrepaidTicket2
     */
    public function setI_szDriverType($i_szDriverType)
    {
      $this->i_szDriverType = $i_szDriverType;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szDCContent()
    {
      return $this->i_szDCContent;
    }

    /**
     * @param string $i_szDCContent
     * @return \Axess\Dci4Wtp\producePrepaidTicket2
     */
    public function setI_szDCContent($i_szDCContent)
    {
      $this->i_szDCContent = $i_szDCContent;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szWTPNo()
    {
      return $this->i_szWTPNo;
    }

    /**
     * @param string $i_szWTPNo
     * @return \Axess\Dci4Wtp\producePrepaidTicket2
     */
    public function setI_szWTPNo($i_szWTPNo)
    {
      $this->i_szWTPNo = $i_szWTPNo;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szExtOrderNr()
    {
      return $this->i_szExtOrderNr;
    }

    /**
     * @param string $i_szExtOrderNr
     * @return \Axess\Dci4Wtp\producePrepaidTicket2
     */
    public function setI_szExtOrderNr($i_szExtOrderNr)
    {
      $this->i_szExtOrderNr = $i_szExtOrderNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_NPROJNR()
    {
      return $this->i_NPROJNR;
    }

    /**
     * @param float $i_NPROJNR
     * @return \Axess\Dci4Wtp\producePrepaidTicket2
     */
    public function setI_NPROJNR($i_NPROJNR)
    {
      $this->i_NPROJNR = $i_NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nPOSNr()
    {
      return $this->i_nPOSNr;
    }

    /**
     * @param float $i_nPOSNr
     * @return \Axess\Dci4Wtp\producePrepaidTicket2
     */
    public function setI_nPOSNr($i_nPOSNr)
    {
      $this->i_nPOSNr = $i_nPOSNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nJournalNr()
    {
      return $this->i_nJournalNr;
    }

    /**
     * @param float $i_nJournalNr
     * @return \Axess\Dci4Wtp\producePrepaidTicket2
     */
    public function setI_nJournalNr($i_nJournalNr)
    {
      $this->i_nJournalNr = $i_nJournalNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nPickupBoxNr()
    {
      return $this->i_nPickupBoxNr;
    }

    /**
     * @param float $i_nPickupBoxNr
     * @return \Axess\Dci4Wtp\producePrepaidTicket2
     */
    public function setI_nPickupBoxNr($i_nPickupBoxNr)
    {
      $this->i_nPickupBoxNr = $i_nPickupBoxNr;
      return $this;
    }

}
