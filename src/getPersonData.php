<?php

namespace Axess\Dci4Wtp;

class getPersonData
{

    /**
     * @var D4WTPGETPERSONREQUEST $i_ctPersDataReq
     */
    protected $i_ctPersDataReq = null;

    /**
     * @param D4WTPGETPERSONREQUEST $i_ctPersDataReq
     */
    public function __construct($i_ctPersDataReq)
    {
      $this->i_ctPersDataReq = $i_ctPersDataReq;
    }

    /**
     * @return D4WTPGETPERSONREQUEST
     */
    public function getI_ctPersDataReq()
    {
      return $this->i_ctPersDataReq;
    }

    /**
     * @param D4WTPGETPERSONREQUEST $i_ctPersDataReq
     * @return \Axess\Dci4Wtp\getPersonData
     */
    public function setI_ctPersDataReq($i_ctPersDataReq)
    {
      $this->i_ctPersDataReq = $i_ctPersDataReq;
      return $this;
    }

}
