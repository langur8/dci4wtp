<?php

namespace Axess\Dci4Wtp;

class D4WTPWEBRENTALBYPROPERTYREQ
{

    /**
     * @var float $NARCNR
     */
    protected $NARCNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NRENTALITEMNR
     */
    protected $NRENTALITEMNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var ArrayOfDCI4WTPGETWEBRENTALBYPROP $SKI_PROPERTY
     */
    protected $SKI_PROPERTY = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    /**
     * @var string $SZRENTALEND
     */
    protected $SZRENTALEND = null;

    /**
     * @var string $SZRENTALSTART
     */
    protected $SZRENTALSTART = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNARCNR()
    {
      return $this->NARCNR;
    }

    /**
     * @param float $NARCNR
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYREQ
     */
    public function setNARCNR($NARCNR)
    {
      $this->NARCNR = $NARCNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYREQ
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNR()
    {
      return $this->NRENTALITEMNR;
    }

    /**
     * @param float $NRENTALITEMNR
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYREQ
     */
    public function setNRENTALITEMNR($NRENTALITEMNR)
    {
      $this->NRENTALITEMNR = $NRENTALITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return ArrayOfDCI4WTPGETWEBRENTALBYPROP
     */
    public function getSKI_PROPERTY()
    {
      return $this->SKI_PROPERTY;
    }

    /**
     * @param ArrayOfDCI4WTPGETWEBRENTALBYPROP $SKI_PROPERTY
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYREQ
     */
    public function setSKI_PROPERTY($SKI_PROPERTY)
    {
      $this->SKI_PROPERTY = $SKI_PROPERTY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYREQ
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRENTALEND()
    {
      return $this->SZRENTALEND;
    }

    /**
     * @param string $SZRENTALEND
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYREQ
     */
    public function setSZRENTALEND($SZRENTALEND)
    {
      $this->SZRENTALEND = $SZRENTALEND;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRENTALSTART()
    {
      return $this->SZRENTALSTART;
    }

    /**
     * @param string $SZRENTALSTART
     * @return \Axess\Dci4Wtp\D4WTPWEBRENTALBYPROPERTYREQ
     */
    public function setSZRENTALSTART($SZRENTALSTART)
    {
      $this->SZRENTALSTART = $SZRENTALSTART;
      return $this;
    }

}
