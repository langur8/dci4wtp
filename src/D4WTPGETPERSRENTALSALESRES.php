<?php

namespace Axess\Dci4Wtp;

class D4WTPGETPERSRENTALSALESRES
{

    /**
     * @var ArrayOfD4WTPRENTALSALE $ACTRENTALSALE
     */
    protected $ACTRENTALSALE = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var string $SZDATEFROM
     */
    protected $SZDATEFROM = null;

    /**
     * @var string $SZDATETO
     */
    protected $SZDATETO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPRENTALSALE
     */
    public function getACTRENTALSALE()
    {
      return $this->ACTRENTALSALE;
    }

    /**
     * @param ArrayOfD4WTPRENTALSALE $ACTRENTALSALE
     * @return \Axess\Dci4Wtp\D4WTPGETPERSRENTALSALESRES
     */
    public function setACTRENTALSALE($ACTRENTALSALE)
    {
      $this->ACTRENTALSALE = $ACTRENTALSALE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSRENTALSALESRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSRENTALSALESRES
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSRENTALSALESRES
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSRENTALSALESRES
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEFROM()
    {
      return $this->SZDATEFROM;
    }

    /**
     * @param string $SZDATEFROM
     * @return \Axess\Dci4Wtp\D4WTPGETPERSRENTALSALESRES
     */
    public function setSZDATEFROM($SZDATEFROM)
    {
      $this->SZDATEFROM = $SZDATEFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATETO()
    {
      return $this->SZDATETO;
    }

    /**
     * @param string $SZDATETO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSRENTALSALESRES
     */
    public function setSZDATETO($SZDATETO)
    {
      $this->SZDATETO = $SZDATETO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETPERSRENTALSALESRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
