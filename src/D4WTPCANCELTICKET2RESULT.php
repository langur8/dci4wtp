<?php

namespace Axess\Dci4Wtp;

class D4WTPCANCELTICKET2RESULT
{

    /**
     * @var D4WTPCANCELTICKETRESULT $CTCANCELTICKETRESULT
     */
    protected $CTCANCELTICKETRESULT = null;

    /**
     * @var float $FTICKETPRICEINCENT
     */
    protected $FTICKETPRICEINCENT = null;

    /**
     * @var float $NCLEARINGCARDTYPENO
     */
    protected $NCLEARINGCARDTYPENO = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPOSRECEIPTNO
     */
    protected $NPOSRECEIPTNO = null;

    /**
     * @var float $NTICKETJOURNALNO
     */
    protected $NTICKETJOURNALNO = null;

    /**
     * @var float $NTICKETUNICODENO
     */
    protected $NTICKETUNICODENO = null;

    /**
     * @var string $SZCLEARINGCARDNO
     */
    protected $SZCLEARINGCARDNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCANCELTICKETRESULT
     */
    public function getCTCANCELTICKETRESULT()
    {
      return $this->CTCANCELTICKETRESULT;
    }

    /**
     * @param D4WTPCANCELTICKETRESULT $CTCANCELTICKETRESULT
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET2RESULT
     */
    public function setCTCANCELTICKETRESULT($CTCANCELTICKETRESULT)
    {
      $this->CTCANCELTICKETRESULT = $CTCANCELTICKETRESULT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFTICKETPRICEINCENT()
    {
      return $this->FTICKETPRICEINCENT;
    }

    /**
     * @param float $FTICKETPRICEINCENT
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET2RESULT
     */
    public function setFTICKETPRICEINCENT($FTICKETPRICEINCENT)
    {
      $this->FTICKETPRICEINCENT = $FTICKETPRICEINCENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCLEARINGCARDTYPENO()
    {
      return $this->NCLEARINGCARDTYPENO;
    }

    /**
     * @param float $NCLEARINGCARDTYPENO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET2RESULT
     */
    public function setNCLEARINGCARDTYPENO($NCLEARINGCARDTYPENO)
    {
      $this->NCLEARINGCARDTYPENO = $NCLEARINGCARDTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSRECEIPTNO()
    {
      return $this->NPOSRECEIPTNO;
    }

    /**
     * @param float $NPOSRECEIPTNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET2RESULT
     */
    public function setNPOSRECEIPTNO($NPOSRECEIPTNO)
    {
      $this->NPOSRECEIPTNO = $NPOSRECEIPTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETJOURNALNO()
    {
      return $this->NTICKETJOURNALNO;
    }

    /**
     * @param float $NTICKETJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET2RESULT
     */
    public function setNTICKETJOURNALNO($NTICKETJOURNALNO)
    {
      $this->NTICKETJOURNALNO = $NTICKETJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETUNICODENO()
    {
      return $this->NTICKETUNICODENO;
    }

    /**
     * @param float $NTICKETUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET2RESULT
     */
    public function setNTICKETUNICODENO($NTICKETUNICODENO)
    {
      $this->NTICKETUNICODENO = $NTICKETUNICODENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCLEARINGCARDNO()
    {
      return $this->SZCLEARINGCARDNO;
    }

    /**
     * @param string $SZCLEARINGCARDNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET2RESULT
     */
    public function setSZCLEARINGCARDNO($SZCLEARINGCARDNO)
    {
      $this->SZCLEARINGCARDNO = $SZCLEARINGCARDNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKET2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
