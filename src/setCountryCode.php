<?php

namespace Axess\Dci4Wtp;

class setCountryCode
{

    /**
     * @var D4WTPSETCOUNTRYCODEREQUEST $i_ctSetCCReq
     */
    protected $i_ctSetCCReq = null;

    /**
     * @param D4WTPSETCOUNTRYCODEREQUEST $i_ctSetCCReq
     */
    public function __construct($i_ctSetCCReq)
    {
      $this->i_ctSetCCReq = $i_ctSetCCReq;
    }

    /**
     * @return D4WTPSETCOUNTRYCODEREQUEST
     */
    public function getI_ctSetCCReq()
    {
      return $this->i_ctSetCCReq;
    }

    /**
     * @param D4WTPSETCOUNTRYCODEREQUEST $i_ctSetCCReq
     * @return \Axess\Dci4Wtp\setCountryCode
     */
    public function setI_ctSetCCReq($i_ctSetCCReq)
    {
      $this->i_ctSetCCReq = $i_ctSetCCReq;
      return $this;
    }

}
