<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSTANDARTICLE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSTANDARTICLE[] $D4WTPSTANDARTICLE
     */
    protected $D4WTPSTANDARTICLE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSTANDARTICLE[]
     */
    public function getD4WTPSTANDARTICLE()
    {
      return $this->D4WTPSTANDARTICLE;
    }

    /**
     * @param D4WTPSTANDARTICLE[] $D4WTPSTANDARTICLE
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSTANDARTICLE
     */
    public function setD4WTPSTANDARTICLE(array $D4WTPSTANDARTICLE = null)
    {
      $this->D4WTPSTANDARTICLE = $D4WTPSTANDARTICLE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSTANDARTICLE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSTANDARTICLE
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSTANDARTICLE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSTANDARTICLE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSTANDARTICLE[] = $value;
      } else {
        $this->D4WTPSTANDARTICLE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSTANDARTICLE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSTANDARTICLE Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSTANDARTICLE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSTANDARTICLE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSTANDARTICLE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSTANDARTICLE);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSTANDARTICLE Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSTANDARTICLE);
    }

}
