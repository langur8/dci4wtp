<?php

namespace Axess\Dci4Wtp;

class CheckOneTimePwdResponse
{

    /**
     * @var CTCONFIGRESULT $CheckOneTimePwdResult
     */
    protected $CheckOneTimePwdResult = null;

    /**
     * @param CTCONFIGRESULT $CheckOneTimePwdResult
     */
    public function __construct($CheckOneTimePwdResult)
    {
      $this->CheckOneTimePwdResult = $CheckOneTimePwdResult;
    }

    /**
     * @return CTCONFIGRESULT
     */
    public function getCheckOneTimePwdResult()
    {
      return $this->CheckOneTimePwdResult;
    }

    /**
     * @param CTCONFIGRESULT $CheckOneTimePwdResult
     * @return \Axess\Dci4Wtp\CheckOneTimePwdResponse
     */
    public function setCheckOneTimePwdResult($CheckOneTimePwdResult)
    {
      $this->CheckOneTimePwdResult = $CheckOneTimePwdResult;
      return $this;
    }

}
