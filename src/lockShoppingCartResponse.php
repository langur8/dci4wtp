<?php

namespace Axess\Dci4Wtp;

class lockShoppingCartResponse
{

    /**
     * @var D4WTPRESULT $lockShoppingCartResult
     */
    protected $lockShoppingCartResult = null;

    /**
     * @param D4WTPRESULT $lockShoppingCartResult
     */
    public function __construct($lockShoppingCartResult)
    {
      $this->lockShoppingCartResult = $lockShoppingCartResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getLockShoppingCartResult()
    {
      return $this->lockShoppingCartResult;
    }

    /**
     * @param D4WTPRESULT $lockShoppingCartResult
     * @return \Axess\Dci4Wtp\lockShoppingCartResponse
     */
    public function setLockShoppingCartResult($lockShoppingCartResult)
    {
      $this->lockShoppingCartResult = $lockShoppingCartResult;
      return $this;
    }

}
