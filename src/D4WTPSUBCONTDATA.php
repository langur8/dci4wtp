<?php

namespace Axess\Dci4Wtp;

class D4WTPSUBCONTDATA
{

    /**
     * @var float $NARTICLENR
     */
    protected $NARTICLENR = null;

    /**
     * @var float $NCONTINGENTNO
     */
    protected $NCONTINGENTNO = null;

    /**
     * @var float $NSUBCONTDAY
     */
    protected $NSUBCONTDAY = null;

    /**
     * @var float $NSUBCONTINGENTNO
     */
    protected $NSUBCONTINGENTNO = null;

    /**
     * @var float $NSUBCONTLANGUAGEID
     */
    protected $NSUBCONTLANGUAGEID = null;

    /**
     * @var string $SZARTICLENAME
     */
    protected $SZARTICLENAME = null;

    /**
     * @var string $SZARTILEMASKSHORTNAME
     */
    protected $SZARTILEMASKSHORTNAME = null;

    /**
     * @var string $SZLANGCOUNTRYCODE
     */
    protected $SZLANGCOUNTRYCODE = null;

    /**
     * @var string $SZLANGNAME
     */
    protected $SZLANGNAME = null;

    /**
     * @var string $SZSUBCONTNAME
     */
    protected $SZSUBCONTNAME = null;

    /**
     * @var string $SZSUBCONTSHORTNAME
     */
    protected $SZSUBCONTSHORTNAME = null;

    /**
     * @var string $SZSUBCONTSTARTTIME
     */
    protected $SZSUBCONTSTARTTIME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNARTICLENR()
    {
      return $this->NARTICLENR;
    }

    /**
     * @param float $NARTICLENR
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTDATA
     */
    public function setNARTICLENR($NARTICLENR)
    {
      $this->NARTICLENR = $NARTICLENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNO()
    {
      return $this->NCONTINGENTNO;
    }

    /**
     * @param float $NCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTDATA
     */
    public function setNCONTINGENTNO($NCONTINGENTNO)
    {
      $this->NCONTINGENTNO = $NCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTDAY()
    {
      return $this->NSUBCONTDAY;
    }

    /**
     * @param float $NSUBCONTDAY
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTDATA
     */
    public function setNSUBCONTDAY($NSUBCONTDAY)
    {
      $this->NSUBCONTDAY = $NSUBCONTDAY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTINGENTNO()
    {
      return $this->NSUBCONTINGENTNO;
    }

    /**
     * @param float $NSUBCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTDATA
     */
    public function setNSUBCONTINGENTNO($NSUBCONTINGENTNO)
    {
      $this->NSUBCONTINGENTNO = $NSUBCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTLANGUAGEID()
    {
      return $this->NSUBCONTLANGUAGEID;
    }

    /**
     * @param float $NSUBCONTLANGUAGEID
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTDATA
     */
    public function setNSUBCONTLANGUAGEID($NSUBCONTLANGUAGEID)
    {
      $this->NSUBCONTLANGUAGEID = $NSUBCONTLANGUAGEID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZARTICLENAME()
    {
      return $this->SZARTICLENAME;
    }

    /**
     * @param string $SZARTICLENAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTDATA
     */
    public function setSZARTICLENAME($SZARTICLENAME)
    {
      $this->SZARTICLENAME = $SZARTICLENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZARTILEMASKSHORTNAME()
    {
      return $this->SZARTILEMASKSHORTNAME;
    }

    /**
     * @param string $SZARTILEMASKSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTDATA
     */
    public function setSZARTILEMASKSHORTNAME($SZARTILEMASKSHORTNAME)
    {
      $this->SZARTILEMASKSHORTNAME = $SZARTILEMASKSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLANGCOUNTRYCODE()
    {
      return $this->SZLANGCOUNTRYCODE;
    }

    /**
     * @param string $SZLANGCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTDATA
     */
    public function setSZLANGCOUNTRYCODE($SZLANGCOUNTRYCODE)
    {
      $this->SZLANGCOUNTRYCODE = $SZLANGCOUNTRYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLANGNAME()
    {
      return $this->SZLANGNAME;
    }

    /**
     * @param string $SZLANGNAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTDATA
     */
    public function setSZLANGNAME($SZLANGNAME)
    {
      $this->SZLANGNAME = $SZLANGNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBCONTNAME()
    {
      return $this->SZSUBCONTNAME;
    }

    /**
     * @param string $SZSUBCONTNAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTDATA
     */
    public function setSZSUBCONTNAME($SZSUBCONTNAME)
    {
      $this->SZSUBCONTNAME = $SZSUBCONTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBCONTSHORTNAME()
    {
      return $this->SZSUBCONTSHORTNAME;
    }

    /**
     * @param string $SZSUBCONTSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTDATA
     */
    public function setSZSUBCONTSHORTNAME($SZSUBCONTSHORTNAME)
    {
      $this->SZSUBCONTSHORTNAME = $SZSUBCONTSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBCONTSTARTTIME()
    {
      return $this->SZSUBCONTSTARTTIME;
    }

    /**
     * @param string $SZSUBCONTSTARTTIME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTDATA
     */
    public function setSZSUBCONTSTARTTIME($SZSUBCONTSTARTTIME)
    {
      $this->SZSUBCONTSTARTTIME = $SZSUBCONTSTARTTIME;
      return $this;
    }

}
