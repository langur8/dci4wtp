<?php

namespace Axess\Dci4Wtp;

class D4WTPEMPLOYEEASSIGNED
{

    /**
     * @var float $BISTICKETCANCELED
     */
    protected $BISTICKETCANCELED = null;

    /**
     * @var float $BPERSACTIVE
     */
    protected $BPERSACTIVE = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NTICKETPOSNO
     */
    protected $NTICKETPOSNO = null;

    /**
     * @var float $NTICKETPROJNO
     */
    protected $NTICKETPROJNO = null;

    /**
     * @var float $NTICKETSERIALNO
     */
    protected $NTICKETSERIALNO = null;

    /**
     * @var string $SZBARCODE
     */
    protected $SZBARCODE = null;

    /**
     * @var string $SZDATEOFBIRTH
     */
    protected $SZDATEOFBIRTH = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZMOBILE
     */
    protected $SZMOBILE = null;

    /**
     * @var string $SZPHONE
     */
    protected $SZPHONE = null;

    /**
     * @var string $SZTICKETVALIDFROM
     */
    protected $SZTICKETVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISTICKETCANCELED()
    {
      return $this->BISTICKETCANCELED;
    }

    /**
     * @param float $BISTICKETCANCELED
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setBISTICKETCANCELED($BISTICKETCANCELED)
    {
      $this->BISTICKETCANCELED = $BISTICKETCANCELED;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPERSACTIVE()
    {
      return $this->BPERSACTIVE;
    }

    /**
     * @param float $BPERSACTIVE
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setBPERSACTIVE($BPERSACTIVE)
    {
      $this->BPERSACTIVE = $BPERSACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETPOSNO()
    {
      return $this->NTICKETPOSNO;
    }

    /**
     * @param float $NTICKETPOSNO
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setNTICKETPOSNO($NTICKETPOSNO)
    {
      $this->NTICKETPOSNO = $NTICKETPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETPROJNO()
    {
      return $this->NTICKETPROJNO;
    }

    /**
     * @param float $NTICKETPROJNO
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setNTICKETPROJNO($NTICKETPROJNO)
    {
      $this->NTICKETPROJNO = $NTICKETPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETSERIALNO()
    {
      return $this->NTICKETSERIALNO;
    }

    /**
     * @param float $NTICKETSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setNTICKETSERIALNO($NTICKETSERIALNO)
    {
      $this->NTICKETSERIALNO = $NTICKETSERIALNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBARCODE()
    {
      return $this->SZBARCODE;
    }

    /**
     * @param string $SZBARCODE
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setSZBARCODE($SZBARCODE)
    {
      $this->SZBARCODE = $SZBARCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATEOFBIRTH()
    {
      return $this->SZDATEOFBIRTH;
    }

    /**
     * @param string $SZDATEOFBIRTH
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setSZDATEOFBIRTH($SZDATEOFBIRTH)
    {
      $this->SZDATEOFBIRTH = $SZDATEOFBIRTH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMOBILE()
    {
      return $this->SZMOBILE;
    }

    /**
     * @param string $SZMOBILE
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setSZMOBILE($SZMOBILE)
    {
      $this->SZMOBILE = $SZMOBILE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPHONE()
    {
      return $this->SZPHONE;
    }

    /**
     * @param string $SZPHONE
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setSZPHONE($SZPHONE)
    {
      $this->SZPHONE = $SZPHONE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETVALIDFROM()
    {
      return $this->SZTICKETVALIDFROM;
    }

    /**
     * @param string $SZTICKETVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEEASSIGNED
     */
    public function setSZTICKETVALIDFROM($SZTICKETVALIDFROM)
    {
      $this->SZTICKETVALIDFROM = $SZTICKETVALIDFROM;
      return $this;
    }

}
