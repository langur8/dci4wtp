<?php

namespace Axess\Dci4Wtp;

class getPersTypeRulesResponse
{

    /**
     * @var D4WTPPERSTYPERULERESULT $getPersTypeRulesResult
     */
    protected $getPersTypeRulesResult = null;

    /**
     * @param D4WTPPERSTYPERULERESULT $getPersTypeRulesResult
     */
    public function __construct($getPersTypeRulesResult)
    {
      $this->getPersTypeRulesResult = $getPersTypeRulesResult;
    }

    /**
     * @return D4WTPPERSTYPERULERESULT
     */
    public function getGetPersTypeRulesResult()
    {
      return $this->getPersTypeRulesResult;
    }

    /**
     * @param D4WTPPERSTYPERULERESULT $getPersTypeRulesResult
     * @return \Axess\Dci4Wtp\getPersTypeRulesResponse
     */
    public function setGetPersTypeRulesResult($getPersTypeRulesResult)
    {
      $this->getPersTypeRulesResult = $getPersTypeRulesResult;
      return $this;
    }

}
