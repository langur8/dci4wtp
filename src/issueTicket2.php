<?php

namespace Axess\Dci4Wtp;

class issueTicket2
{

    /**
     * @var D4WTPISSUETICKETREQUEST $i_ctIssueTicketReq
     */
    protected $i_ctIssueTicketReq = null;

    /**
     * @var float $i_bPrepaidTicket
     */
    protected $i_bPrepaidTicket = null;

    /**
     * @param D4WTPISSUETICKETREQUEST $i_ctIssueTicketReq
     * @param float $i_bPrepaidTicket
     */
    public function __construct($i_ctIssueTicketReq, $i_bPrepaidTicket)
    {
      $this->i_ctIssueTicketReq = $i_ctIssueTicketReq;
      $this->i_bPrepaidTicket = $i_bPrepaidTicket;
    }

    /**
     * @return D4WTPISSUETICKETREQUEST
     */
    public function getI_ctIssueTicketReq()
    {
      return $this->i_ctIssueTicketReq;
    }

    /**
     * @param D4WTPISSUETICKETREQUEST $i_ctIssueTicketReq
     * @return \Axess\Dci4Wtp\issueTicket2
     */
    public function setI_ctIssueTicketReq($i_ctIssueTicketReq)
    {
      $this->i_ctIssueTicketReq = $i_ctIssueTicketReq;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_bPrepaidTicket()
    {
      return $this->i_bPrepaidTicket;
    }

    /**
     * @param float $i_bPrepaidTicket
     * @return \Axess\Dci4Wtp\issueTicket2
     */
    public function setI_bPrepaidTicket($i_bPrepaidTicket)
    {
      $this->i_bPrepaidTicket = $i_bPrepaidTicket;
      return $this;
    }

}
