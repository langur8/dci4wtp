<?php

namespace Axess\Dci4Wtp;

class D4WTPNF525RECEIPTNORESULT
{

    /**
     * @var float $BISDUPLICATE
     */
    protected $BISDUPLICATE = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NNF525RECEIPTNO
     */
    protected $NNF525RECEIPTNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISDUPLICATE()
    {
      return $this->BISDUPLICATE;
    }

    /**
     * @param float $BISDUPLICATE
     * @return \Axess\Dci4Wtp\D4WTPNF525RECEIPTNORESULT
     */
    public function setBISDUPLICATE($BISDUPLICATE)
    {
      $this->BISDUPLICATE = $BISDUPLICATE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPNF525RECEIPTNORESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNF525RECEIPTNO()
    {
      return $this->NNF525RECEIPTNO;
    }

    /**
     * @param float $NNF525RECEIPTNO
     * @return \Axess\Dci4Wtp\D4WTPNF525RECEIPTNORESULT
     */
    public function setNNF525RECEIPTNO($NNF525RECEIPTNO)
    {
      $this->NNF525RECEIPTNO = $NNF525RECEIPTNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPNF525RECEIPTNORESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
