<?php

namespace Axess\Dci4Wtp;

class getTariffList3
{

    /**
     * @var D4WTPTARIFFLIST2REQUEST $i_ctTariffListReq
     */
    protected $i_ctTariffListReq = null;

    /**
     * @param D4WTPTARIFFLIST2REQUEST $i_ctTariffListReq
     */
    public function __construct($i_ctTariffListReq)
    {
      $this->i_ctTariffListReq = $i_ctTariffListReq;
    }

    /**
     * @return D4WTPTARIFFLIST2REQUEST
     */
    public function getI_ctTariffListReq()
    {
      return $this->i_ctTariffListReq;
    }

    /**
     * @param D4WTPTARIFFLIST2REQUEST $i_ctTariffListReq
     * @return \Axess\Dci4Wtp\getTariffList3
     */
    public function setI_ctTariffListReq($i_ctTariffListReq)
    {
      $this->i_ctTariffListReq = $i_ctTariffListReq;
      return $this;
    }

}
