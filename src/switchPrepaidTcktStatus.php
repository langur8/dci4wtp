<?php

namespace Axess\Dci4Wtp;

class switchPrepaidTcktStatus
{

    /**
     * @var D4WTPSWITCHPREPSTATUSREQUEST $i_ctSwitchPrepStatusReq
     */
    protected $i_ctSwitchPrepStatusReq = null;

    /**
     * @param D4WTPSWITCHPREPSTATUSREQUEST $i_ctSwitchPrepStatusReq
     */
    public function __construct($i_ctSwitchPrepStatusReq)
    {
      $this->i_ctSwitchPrepStatusReq = $i_ctSwitchPrepStatusReq;
    }

    /**
     * @return D4WTPSWITCHPREPSTATUSREQUEST
     */
    public function getI_ctSwitchPrepStatusReq()
    {
      return $this->i_ctSwitchPrepStatusReq;
    }

    /**
     * @param D4WTPSWITCHPREPSTATUSREQUEST $i_ctSwitchPrepStatusReq
     * @return \Axess\Dci4Wtp\switchPrepaidTcktStatus
     */
    public function setI_ctSwitchPrepStatusReq($i_ctSwitchPrepStatusReq)
    {
      $this->i_ctSwitchPrepStatusReq = $i_ctSwitchPrepStatusReq;
      return $this;
    }

}
