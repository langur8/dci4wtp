<?php

namespace Axess\Dci4Wtp;

class holeKundenDatenFromSwissPass
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var OBI4PSPAUSWEISDATENANFRAGE $i_ctAusweisDatenAnfrage
     */
    protected $i_ctAusweisDatenAnfrage = null;

    /**
     * @var OBI4PSPUIDANFRAGE $i_ctUidAnfrage
     */
    protected $i_ctUidAnfrage = null;

    /**
     * @var OBI4PSPKUNDENDATENANFRAGE $i_ctKundenDatenAnfrage
     */
    protected $i_ctKundenDatenAnfrage = null;

    /**
     * @var float $i_bSwissPassAboStatus
     */
    protected $i_bSwissPassAboStatus = null;

    /**
     * @var float $i_bKundenDaten
     */
    protected $i_bKundenDaten = null;

    /**
     * @var float $i_bFotoDaten
     */
    protected $i_bFotoDaten = null;

    /**
     * @var float $i_bAXKundenDaten
     */
    protected $i_bAXKundenDaten = null;

    /**
     * @param float $i_nSessionID
     * @param OBI4PSPAUSWEISDATENANFRAGE $i_ctAusweisDatenAnfrage
     * @param OBI4PSPUIDANFRAGE $i_ctUidAnfrage
     * @param OBI4PSPKUNDENDATENANFRAGE $i_ctKundenDatenAnfrage
     * @param float $i_bSwissPassAboStatus
     * @param float $i_bKundenDaten
     * @param float $i_bFotoDaten
     * @param float $i_bAXKundenDaten
     */
    public function __construct($i_nSessionID, $i_ctAusweisDatenAnfrage, $i_ctUidAnfrage, $i_ctKundenDatenAnfrage, $i_bSwissPassAboStatus, $i_bKundenDaten, $i_bFotoDaten, $i_bAXKundenDaten)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_ctAusweisDatenAnfrage = $i_ctAusweisDatenAnfrage;
      $this->i_ctUidAnfrage = $i_ctUidAnfrage;
      $this->i_ctKundenDatenAnfrage = $i_ctKundenDatenAnfrage;
      $this->i_bSwissPassAboStatus = $i_bSwissPassAboStatus;
      $this->i_bKundenDaten = $i_bKundenDaten;
      $this->i_bFotoDaten = $i_bFotoDaten;
      $this->i_bAXKundenDaten = $i_bAXKundenDaten;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\holeKundenDatenFromSwissPass
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return OBI4PSPAUSWEISDATENANFRAGE
     */
    public function getI_ctAusweisDatenAnfrage()
    {
      return $this->i_ctAusweisDatenAnfrage;
    }

    /**
     * @param OBI4PSPAUSWEISDATENANFRAGE $i_ctAusweisDatenAnfrage
     * @return \Axess\Dci4Wtp\holeKundenDatenFromSwissPass
     */
    public function setI_ctAusweisDatenAnfrage($i_ctAusweisDatenAnfrage)
    {
      $this->i_ctAusweisDatenAnfrage = $i_ctAusweisDatenAnfrage;
      return $this;
    }

    /**
     * @return OBI4PSPUIDANFRAGE
     */
    public function getI_ctUidAnfrage()
    {
      return $this->i_ctUidAnfrage;
    }

    /**
     * @param OBI4PSPUIDANFRAGE $i_ctUidAnfrage
     * @return \Axess\Dci4Wtp\holeKundenDatenFromSwissPass
     */
    public function setI_ctUidAnfrage($i_ctUidAnfrage)
    {
      $this->i_ctUidAnfrage = $i_ctUidAnfrage;
      return $this;
    }

    /**
     * @return OBI4PSPKUNDENDATENANFRAGE
     */
    public function getI_ctKundenDatenAnfrage()
    {
      return $this->i_ctKundenDatenAnfrage;
    }

    /**
     * @param OBI4PSPKUNDENDATENANFRAGE $i_ctKundenDatenAnfrage
     * @return \Axess\Dci4Wtp\holeKundenDatenFromSwissPass
     */
    public function setI_ctKundenDatenAnfrage($i_ctKundenDatenAnfrage)
    {
      $this->i_ctKundenDatenAnfrage = $i_ctKundenDatenAnfrage;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_bSwissPassAboStatus()
    {
      return $this->i_bSwissPassAboStatus;
    }

    /**
     * @param float $i_bSwissPassAboStatus
     * @return \Axess\Dci4Wtp\holeKundenDatenFromSwissPass
     */
    public function setI_bSwissPassAboStatus($i_bSwissPassAboStatus)
    {
      $this->i_bSwissPassAboStatus = $i_bSwissPassAboStatus;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_bKundenDaten()
    {
      return $this->i_bKundenDaten;
    }

    /**
     * @param float $i_bKundenDaten
     * @return \Axess\Dci4Wtp\holeKundenDatenFromSwissPass
     */
    public function setI_bKundenDaten($i_bKundenDaten)
    {
      $this->i_bKundenDaten = $i_bKundenDaten;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_bFotoDaten()
    {
      return $this->i_bFotoDaten;
    }

    /**
     * @param float $i_bFotoDaten
     * @return \Axess\Dci4Wtp\holeKundenDatenFromSwissPass
     */
    public function setI_bFotoDaten($i_bFotoDaten)
    {
      $this->i_bFotoDaten = $i_bFotoDaten;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_bAXKundenDaten()
    {
      return $this->i_bAXKundenDaten;
    }

    /**
     * @param float $i_bAXKundenDaten
     * @return \Axess\Dci4Wtp\holeKundenDatenFromSwissPass
     */
    public function setI_bAXKundenDaten($i_bAXKundenDaten)
    {
      $this->i_bAXKundenDaten = $i_bAXKundenDaten;
      return $this;
    }

}
