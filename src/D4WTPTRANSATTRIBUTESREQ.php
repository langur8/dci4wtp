<?php

namespace Axess\Dci4Wtp;

class D4WTPTRANSATTRIBUTESREQ
{

    /**
     * @var ArrayOfATTRIBUTE $ACTATTRIBUTELIST
     */
    protected $ACTATTRIBUTELIST = null;

    /**
     * @var float $BALLATTRIBUTES
     */
    protected $BALLATTRIBUTES = null;

    /**
     * @var float $NBLOCKNO
     */
    protected $NBLOCKNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfATTRIBUTE
     */
    public function getACTATTRIBUTELIST()
    {
      return $this->ACTATTRIBUTELIST;
    }

    /**
     * @param ArrayOfATTRIBUTE $ACTATTRIBUTELIST
     * @return \Axess\Dci4Wtp\D4WTPTRANSATTRIBUTESREQ
     */
    public function setACTATTRIBUTELIST($ACTATTRIBUTELIST)
    {
      $this->ACTATTRIBUTELIST = $ACTATTRIBUTELIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getBALLATTRIBUTES()
    {
      return $this->BALLATTRIBUTES;
    }

    /**
     * @param float $BALLATTRIBUTES
     * @return \Axess\Dci4Wtp\D4WTPTRANSATTRIBUTESREQ
     */
    public function setBALLATTRIBUTES($BALLATTRIBUTES)
    {
      $this->BALLATTRIBUTES = $BALLATTRIBUTES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKNO()
    {
      return $this->NBLOCKNO;
    }

    /**
     * @param float $NBLOCKNO
     * @return \Axess\Dci4Wtp\D4WTPTRANSATTRIBUTESREQ
     */
    public function setNBLOCKNO($NBLOCKNO)
    {
      $this->NBLOCKNO = $NBLOCKNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTRANSATTRIBUTESREQ
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTRANSATTRIBUTESREQ
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPTRANSATTRIBUTESREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPTRANSATTRIBUTESREQ
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

}
