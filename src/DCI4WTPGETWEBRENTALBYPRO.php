<?php

namespace Axess\Dci4Wtp;

class DCI4WTPGETWEBRENTALBYPRO
{

    /**
     * @var ArrayOfDCI4WTPGETWEBRIBYPROPOV $ARR_SKI_PROP
     */
    protected $ARR_SKI_PROP = null;

    /**
     * @var float $NAVAILABLEITEMS
     */
    protected $NAVAILABLEITEMS = null;

    /**
     * @var float $NPROPERTYNR
     */
    protected $NPROPERTYNR = null;

    /**
     * @var string $SZPROPERTYNAME
     */
    protected $SZPROPERTYNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfDCI4WTPGETWEBRIBYPROPOV
     */
    public function getARR_SKI_PROP()
    {
      return $this->ARR_SKI_PROP;
    }

    /**
     * @param ArrayOfDCI4WTPGETWEBRIBYPROPOV $ARR_SKI_PROP
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRENTALBYPRO
     */
    public function setARR_SKI_PROP($ARR_SKI_PROP)
    {
      $this->ARR_SKI_PROP = $ARR_SKI_PROP;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAVAILABLEITEMS()
    {
      return $this->NAVAILABLEITEMS;
    }

    /**
     * @param float $NAVAILABLEITEMS
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRENTALBYPRO
     */
    public function setNAVAILABLEITEMS($NAVAILABLEITEMS)
    {
      $this->NAVAILABLEITEMS = $NAVAILABLEITEMS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROPERTYNR()
    {
      return $this->NPROPERTYNR;
    }

    /**
     * @param float $NPROPERTYNR
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRENTALBYPRO
     */
    public function setNPROPERTYNR($NPROPERTYNR)
    {
      $this->NPROPERTYNR = $NPROPERTYNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPROPERTYNAME()
    {
      return $this->SZPROPERTYNAME;
    }

    /**
     * @param string $SZPROPERTYNAME
     * @return \Axess\Dci4Wtp\DCI4WTPGETWEBRENTALBYPRO
     */
    public function setSZPROPERTYNAME($SZPROPERTYNAME)
    {
      $this->SZPROPERTYNAME = $SZPROPERTYNAME;
      return $this;
    }

}
