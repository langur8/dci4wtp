<?php

namespace Axess\Dci4Wtp;

class ArrayOfRENTALITEMMWST implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var RENTALITEMMWST[] $RENTALITEMMWST
     */
    protected $RENTALITEMMWST = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RENTALITEMMWST[]
     */
    public function getRENTALITEMMWST()
    {
      return $this->RENTALITEMMWST;
    }

    /**
     * @param RENTALITEMMWST[] $RENTALITEMMWST
     * @return \Axess\Dci4Wtp\ArrayOfRENTALITEMMWST
     */
    public function setRENTALITEMMWST(array $RENTALITEMMWST = null)
    {
      $this->RENTALITEMMWST = $RENTALITEMMWST;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->RENTALITEMMWST[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return RENTALITEMMWST
     */
    public function offsetGet($offset)
    {
      return $this->RENTALITEMMWST[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param RENTALITEMMWST $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->RENTALITEMMWST[] = $value;
      } else {
        $this->RENTALITEMMWST[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->RENTALITEMMWST[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return RENTALITEMMWST Return the current element
     */
    public function current()
    {
      return current($this->RENTALITEMMWST);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->RENTALITEMMWST);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->RENTALITEMMWST);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->RENTALITEMMWST);
    }

    /**
     * Countable implementation
     *
     * @return RENTALITEMMWST Return count of elements
     */
    public function count()
    {
      return count($this->RENTALITEMMWST);
    }

}
