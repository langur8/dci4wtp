<?php

namespace Axess\Dci4Wtp;

class D4WTPPRODUCTS
{

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NPACKAGENO
     */
    protected $NPACKAGENO = null;

    /**
     * @var float $NPERSONTYPENO
     */
    protected $NPERSONTYPENO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPRODUCTNO
     */
    protected $NPRODUCTNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NRENTALITEMNO
     */
    protected $NRENTALITEMNO = null;

    /**
     * @var float $NRENTALPERSTYPENO
     */
    protected $NRENTALPERSTYPENO = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTS
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKAGENO()
    {
      return $this->NPACKAGENO;
    }

    /**
     * @param float $NPACKAGENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTS
     */
    public function setNPACKAGENO($NPACKAGENO)
    {
      $this->NPACKAGENO = $NPACKAGENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSONTYPENO()
    {
      return $this->NPERSONTYPENO;
    }

    /**
     * @param float $NPERSONTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTS
     */
    public function setNPERSONTYPENO($NPERSONTYPENO)
    {
      $this->NPERSONTYPENO = $NPERSONTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTS
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPRODUCTNO()
    {
      return $this->NPRODUCTNO;
    }

    /**
     * @param float $NPRODUCTNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTS
     */
    public function setNPRODUCTNO($NPRODUCTNO)
    {
      $this->NPRODUCTNO = $NPRODUCTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTS
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNO()
    {
      return $this->NRENTALITEMNO;
    }

    /**
     * @param float $NRENTALITEMNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTS
     */
    public function setNRENTALITEMNO($NRENTALITEMNO)
    {
      $this->NRENTALITEMNO = $NRENTALITEMNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPERSTYPENO()
    {
      return $this->NRENTALPERSTYPENO;
    }

    /**
     * @param float $NRENTALPERSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTS
     */
    public function setNRENTALPERSTYPENO($NRENTALPERSTYPENO)
    {
      $this->NRENTALPERSTYPENO = $NRENTALPERSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTS
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

}
