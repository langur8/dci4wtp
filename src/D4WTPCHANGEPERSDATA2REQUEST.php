<?php

namespace Axess\Dci4Wtp;

class D4WTPCHANGEPERSDATA2REQUEST
{

    /**
     * @var D4WTPERSONDATA3 $CTPERSONDATA
     */
    protected $CTPERSONDATA = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPERSONDATA3
     */
    public function getCTPERSONDATA()
    {
      return $this->CTPERSONDATA;
    }

    /**
     * @param D4WTPERSONDATA3 $CTPERSONDATA
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPERSDATA2REQUEST
     */
    public function setCTPERSONDATA($CTPERSONDATA)
    {
      $this->CTPERSONDATA = $CTPERSONDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPERSDATA2REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
