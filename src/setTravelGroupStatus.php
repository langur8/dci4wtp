<?php

namespace Axess\Dci4Wtp;

class setTravelGroupStatus
{

    /**
     * @var D4WTPSETTRAVELGROUPSTATUSREQ $i_ctSetTravelGroupStatusReq
     */
    protected $i_ctSetTravelGroupStatusReq = null;

    /**
     * @param D4WTPSETTRAVELGROUPSTATUSREQ $i_ctSetTravelGroupStatusReq
     */
    public function __construct($i_ctSetTravelGroupStatusReq)
    {
      $this->i_ctSetTravelGroupStatusReq = $i_ctSetTravelGroupStatusReq;
    }

    /**
     * @return D4WTPSETTRAVELGROUPSTATUSREQ
     */
    public function getI_ctSetTravelGroupStatusReq()
    {
      return $this->i_ctSetTravelGroupStatusReq;
    }

    /**
     * @param D4WTPSETTRAVELGROUPSTATUSREQ $i_ctSetTravelGroupStatusReq
     * @return \Axess\Dci4Wtp\setTravelGroupStatus
     */
    public function setI_ctSetTravelGroupStatusReq($i_ctSetTravelGroupStatusReq)
    {
      $this->i_ctSetTravelGroupStatusReq = $i_ctSetTravelGroupStatusReq;
      return $this;
    }

}
