<?php

namespace Axess\Dci4Wtp;

class producePrepaidTicket3Response
{

    /**
     * @var D4WTPRODUCEPREPAIDTICKETRES3 $producePrepaidTicket3Result
     */
    protected $producePrepaidTicket3Result = null;

    /**
     * @param D4WTPRODUCEPREPAIDTICKETRES3 $producePrepaidTicket3Result
     */
    public function __construct($producePrepaidTicket3Result)
    {
      $this->producePrepaidTicket3Result = $producePrepaidTicket3Result;
    }

    /**
     * @return D4WTPRODUCEPREPAIDTICKETRES3
     */
    public function getProducePrepaidTicket3Result()
    {
      return $this->producePrepaidTicket3Result;
    }

    /**
     * @param D4WTPRODUCEPREPAIDTICKETRES3 $producePrepaidTicket3Result
     * @return \Axess\Dci4Wtp\producePrepaidTicket3Response
     */
    public function setProducePrepaidTicket3Result($producePrepaidTicket3Result)
    {
      $this->producePrepaidTicket3Result = $producePrepaidTicket3Result;
      return $this;
    }

}
