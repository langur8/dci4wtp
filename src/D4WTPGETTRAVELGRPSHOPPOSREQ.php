<?php

namespace Axess\Dci4Wtp;

class D4WTPGETTRAVELGRPSHOPPOSREQ
{

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRAVELGROUPNO
     */
    protected $NTRAVELGROUPNO = null;

    /**
     * @var float $NTRAVELGROUPPOSNO
     */
    protected $NTRAVELGROUPPOSNO = null;

    /**
     * @var float $NTRAVELGROUPPROJNO
     */
    protected $NTRAVELGROUPPROJNO = null;

    /**
     * @var string $SZEXTGROUPCODE
     */
    protected $SZEXTGROUPCODE = null;

    /**
     * @var string $SZGROUPID
     */
    protected $SZGROUPID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPNO()
    {
      return $this->NTRAVELGROUPNO;
    }

    /**
     * @param float $NTRAVELGROUPNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSREQ
     */
    public function setNTRAVELGROUPNO($NTRAVELGROUPNO)
    {
      $this->NTRAVELGROUPNO = $NTRAVELGROUPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPOSNO()
    {
      return $this->NTRAVELGROUPPOSNO;
    }

    /**
     * @param float $NTRAVELGROUPPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSREQ
     */
    public function setNTRAVELGROUPPOSNO($NTRAVELGROUPPOSNO)
    {
      $this->NTRAVELGROUPPOSNO = $NTRAVELGROUPPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPROJNO()
    {
      return $this->NTRAVELGROUPPROJNO;
    }

    /**
     * @param float $NTRAVELGROUPPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSREQ
     */
    public function setNTRAVELGROUPPROJNO($NTRAVELGROUPPROJNO)
    {
      $this->NTRAVELGROUPPROJNO = $NTRAVELGROUPPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTGROUPCODE()
    {
      return $this->SZEXTGROUPCODE;
    }

    /**
     * @param string $SZEXTGROUPCODE
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSREQ
     */
    public function setSZEXTGROUPCODE($SZEXTGROUPCODE)
    {
      $this->SZEXTGROUPCODE = $SZEXTGROUPCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGROUPID()
    {
      return $this->SZGROUPID;
    }

    /**
     * @param string $SZGROUPID
     * @return \Axess\Dci4Wtp\D4WTPGETTRAVELGRPSHOPPOSREQ
     */
    public function setSZGROUPID($SZGROUPID)
    {
      $this->SZGROUPID = $SZGROUPID;
      return $this;
    }

}
