<?php

namespace Axess\Dci4Wtp;

class D4WTPUPLOADPRIVACYDOCREQ
{

    /**
     * @var base64Binary $BDOCUMENT
     */
    protected $BDOCUMENT = null;

    /**
     * @var float $BMARKETING
     */
    protected $BMARKETING = null;

    /**
     * @var float $BRETRANSMISSION3RDPARTY
     */
    protected $BRETRANSMISSION3RDPARTY = null;

    /**
     * @var float $BSTORAGE
     */
    protected $BSTORAGE = null;

    /**
     * @var float $BTRANSACTRETRANSMISSION
     */
    protected $BTRANSACTRETRANSMISSION = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZFILENAMEPOSTFIX
     */
    protected $SZFILENAMEPOSTFIX = null;

    /**
     * @var string $SZFILENAMEPREFIX
     */
    protected $SZFILENAMEPREFIX = null;

    /**
     * @var string $SZSIGNATUREDATE1
     */
    protected $SZSIGNATUREDATE1 = null;

    /**
     * @var string $SZSIGNATUREDATE2
     */
    protected $SZSIGNATUREDATE2 = null;

    /**
     * @var string $SZSIGNATUREDATE3
     */
    protected $SZSIGNATUREDATE3 = null;

    /**
     * @var string $SZSIGNATUREDATE4
     */
    protected $SZSIGNATUREDATE4 = null;

    /**
     * @var string $SZSIGNATUREDATE5
     */
    protected $SZSIGNATUREDATE5 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return base64Binary
     */
    public function getBDOCUMENT()
    {
      return $this->BDOCUMENT;
    }

    /**
     * @param base64Binary $BDOCUMENT
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setBDOCUMENT($BDOCUMENT)
    {
      $this->BDOCUMENT = $BDOCUMENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getBMARKETING()
    {
      return $this->BMARKETING;
    }

    /**
     * @param float $BMARKETING
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setBMARKETING($BMARKETING)
    {
      $this->BMARKETING = $BMARKETING;
      return $this;
    }

    /**
     * @return float
     */
    public function getBRETRANSMISSION3RDPARTY()
    {
      return $this->BRETRANSMISSION3RDPARTY;
    }

    /**
     * @param float $BRETRANSMISSION3RDPARTY
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setBRETRANSMISSION3RDPARTY($BRETRANSMISSION3RDPARTY)
    {
      $this->BRETRANSMISSION3RDPARTY = $BRETRANSMISSION3RDPARTY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBSTORAGE()
    {
      return $this->BSTORAGE;
    }

    /**
     * @param float $BSTORAGE
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setBSTORAGE($BSTORAGE)
    {
      $this->BSTORAGE = $BSTORAGE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBTRANSACTRETRANSMISSION()
    {
      return $this->BTRANSACTRETRANSMISSION;
    }

    /**
     * @param float $BTRANSACTRETRANSMISSION
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setBTRANSACTRETRANSMISSION($BTRANSACTRETRANSMISSION)
    {
      $this->BTRANSACTRETRANSMISSION = $BTRANSACTRETRANSMISSION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFILENAMEPOSTFIX()
    {
      return $this->SZFILENAMEPOSTFIX;
    }

    /**
     * @param string $SZFILENAMEPOSTFIX
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setSZFILENAMEPOSTFIX($SZFILENAMEPOSTFIX)
    {
      $this->SZFILENAMEPOSTFIX = $SZFILENAMEPOSTFIX;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFILENAMEPREFIX()
    {
      return $this->SZFILENAMEPREFIX;
    }

    /**
     * @param string $SZFILENAMEPREFIX
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setSZFILENAMEPREFIX($SZFILENAMEPREFIX)
    {
      $this->SZFILENAMEPREFIX = $SZFILENAMEPREFIX;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSIGNATUREDATE1()
    {
      return $this->SZSIGNATUREDATE1;
    }

    /**
     * @param string $SZSIGNATUREDATE1
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setSZSIGNATUREDATE1($SZSIGNATUREDATE1)
    {
      $this->SZSIGNATUREDATE1 = $SZSIGNATUREDATE1;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSIGNATUREDATE2()
    {
      return $this->SZSIGNATUREDATE2;
    }

    /**
     * @param string $SZSIGNATUREDATE2
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setSZSIGNATUREDATE2($SZSIGNATUREDATE2)
    {
      $this->SZSIGNATUREDATE2 = $SZSIGNATUREDATE2;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSIGNATUREDATE3()
    {
      return $this->SZSIGNATUREDATE3;
    }

    /**
     * @param string $SZSIGNATUREDATE3
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setSZSIGNATUREDATE3($SZSIGNATUREDATE3)
    {
      $this->SZSIGNATUREDATE3 = $SZSIGNATUREDATE3;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSIGNATUREDATE4()
    {
      return $this->SZSIGNATUREDATE4;
    }

    /**
     * @param string $SZSIGNATUREDATE4
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setSZSIGNATUREDATE4($SZSIGNATUREDATE4)
    {
      $this->SZSIGNATUREDATE4 = $SZSIGNATUREDATE4;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSIGNATUREDATE5()
    {
      return $this->SZSIGNATUREDATE5;
    }

    /**
     * @param string $SZSIGNATUREDATE5
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCREQ
     */
    public function setSZSIGNATUREDATE5($SZSIGNATUREDATE5)
    {
      $this->SZSIGNATUREDATE5 = $SZSIGNATUREDATE5;
      return $this;
    }

}
