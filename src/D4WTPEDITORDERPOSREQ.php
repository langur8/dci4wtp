<?php

namespace Axess\Dci4Wtp;

class D4WTPEDITORDERPOSREQ
{

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var ArrayOfDCI4WTPORDERPOSITION $ORDERPOSITIONS
     */
    protected $ORDERPOSITIONS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERPOSREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return ArrayOfDCI4WTPORDERPOSITION
     */
    public function getORDERPOSITIONS()
    {
      return $this->ORDERPOSITIONS;
    }

    /**
     * @param ArrayOfDCI4WTPORDERPOSITION $ORDERPOSITIONS
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERPOSREQ
     */
    public function setORDERPOSITIONS($ORDERPOSITIONS)
    {
      $this->ORDERPOSITIONS = $ORDERPOSITIONS;
      return $this;
    }

}
