<?php

namespace Axess\Dci4Wtp;

class nChangeGroupResponse
{

    /**
     * @var D4WTPGROUPRESULT $nChangeGroupResult
     */
    protected $nChangeGroupResult = null;

    /**
     * @param D4WTPGROUPRESULT $nChangeGroupResult
     */
    public function __construct($nChangeGroupResult)
    {
      $this->nChangeGroupResult = $nChangeGroupResult;
    }

    /**
     * @return D4WTPGROUPRESULT
     */
    public function getNChangeGroupResult()
    {
      return $this->nChangeGroupResult;
    }

    /**
     * @param D4WTPGROUPRESULT $nChangeGroupResult
     * @return \Axess\Dci4Wtp\nChangeGroupResponse
     */
    public function setNChangeGroupResult($nChangeGroupResult)
    {
      $this->nChangeGroupResult = $nChangeGroupResult;
      return $this;
    }

}
