<?php

namespace Axess\Dci4Wtp;

class D4WTPWTPNOSALESDATARESULT
{

    /**
     * @var ArrayOfD4WTPWTPNOSALESDATA $ACTWTPNOSALESDATA
     */
    protected $ACTWTPNOSALESDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPWTPNOSALESDATA
     */
    public function getACTWTPNOSALESDATA()
    {
      return $this->ACTWTPNOSALESDATA;
    }

    /**
     * @param ArrayOfD4WTPWTPNOSALESDATA $ACTWTPNOSALESDATA
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATARESULT
     */
    public function setACTWTPNOSALESDATA($ACTWTPNOSALESDATA)
    {
      $this->ACTWTPNOSALESDATA = $ACTWTPNOSALESDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATARESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATARESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
