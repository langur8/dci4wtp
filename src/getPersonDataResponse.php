<?php

namespace Axess\Dci4Wtp;

class getPersonDataResponse
{

    /**
     * @var D4WTPGETPERSONRESULT $getPersonDataResult
     */
    protected $getPersonDataResult = null;

    /**
     * @param D4WTPGETPERSONRESULT $getPersonDataResult
     */
    public function __construct($getPersonDataResult)
    {
      $this->getPersonDataResult = $getPersonDataResult;
    }

    /**
     * @return D4WTPGETPERSONRESULT
     */
    public function getGetPersonDataResult()
    {
      return $this->getPersonDataResult;
    }

    /**
     * @param D4WTPGETPERSONRESULT $getPersonDataResult
     * @return \Axess\Dci4Wtp\getPersonDataResponse
     */
    public function setGetPersonDataResult($getPersonDataResult)
    {
      $this->getPersonDataResult = $getPersonDataResult;
      return $this;
    }

}
