<?php

namespace Axess\Dci4Wtp;

class D4WTPCONTINGENTLIST3
{

    /**
     * @var ArrayOfD4WTPSUBCONTINGENTLIST2 $ACTSUBCONTINGENTLIST2
     */
    protected $ACTSUBCONTINGENTLIST2 = null;

    /**
     * @var float $BRESOPTIONAL
     */
    protected $BRESOPTIONAL = null;

    /**
     * @var D4WTPRESARTICLETARIFF $CTRESARTICLE
     */
    protected $CTRESARTICLE = null;

    /**
     * @var float $NCONTINGENTNR
     */
    protected $NCONTINGENTNR = null;

    /**
     * @var float $NCOUNTFREE
     */
    protected $NCOUNTFREE = null;

    /**
     * @var float $NSORTNR
     */
    protected $NSORTNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSUBCONTINGENTLIST2
     */
    public function getACTSUBCONTINGENTLIST2()
    {
      return $this->ACTSUBCONTINGENTLIST2;
    }

    /**
     * @param ArrayOfD4WTPSUBCONTINGENTLIST2 $ACTSUBCONTINGENTLIST2
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST3
     */
    public function setACTSUBCONTINGENTLIST2($ACTSUBCONTINGENTLIST2)
    {
      $this->ACTSUBCONTINGENTLIST2 = $ACTSUBCONTINGENTLIST2;
      return $this;
    }

    /**
     * @return float
     */
    public function getBRESOPTIONAL()
    {
      return $this->BRESOPTIONAL;
    }

    /**
     * @param float $BRESOPTIONAL
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST3
     */
    public function setBRESOPTIONAL($BRESOPTIONAL)
    {
      $this->BRESOPTIONAL = $BRESOPTIONAL;
      return $this;
    }

    /**
     * @return D4WTPRESARTICLETARIFF
     */
    public function getCTRESARTICLE()
    {
      return $this->CTRESARTICLE;
    }

    /**
     * @param D4WTPRESARTICLETARIFF $CTRESARTICLE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST3
     */
    public function setCTRESARTICLE($CTRESARTICLE)
    {
      $this->CTRESARTICLE = $CTRESARTICLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNR()
    {
      return $this->NCONTINGENTNR;
    }

    /**
     * @param float $NCONTINGENTNR
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST3
     */
    public function setNCONTINGENTNR($NCONTINGENTNR)
    {
      $this->NCONTINGENTNR = $NCONTINGENTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTFREE()
    {
      return $this->NCOUNTFREE;
    }

    /**
     * @param float $NCOUNTFREE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST3
     */
    public function setNCOUNTFREE($NCOUNTFREE)
    {
      $this->NCOUNTFREE = $NCOUNTFREE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNR()
    {
      return $this->NSORTNR;
    }

    /**
     * @param float $NSORTNR
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTLIST3
     */
    public function setNSORTNR($NSORTNR)
    {
      $this->NSORTNR = $NSORTNR;
      return $this;
    }

}
