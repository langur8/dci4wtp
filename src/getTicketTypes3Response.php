<?php

namespace Axess\Dci4Wtp;

class getTicketTypes3Response
{

    /**
     * @var D4WTPTICKETTYPE3RESULT $getTicketTypes3Result
     */
    protected $getTicketTypes3Result = null;

    /**
     * @param D4WTPTICKETTYPE3RESULT $getTicketTypes3Result
     */
    public function __construct($getTicketTypes3Result)
    {
      $this->getTicketTypes3Result = $getTicketTypes3Result;
    }

    /**
     * @return D4WTPTICKETTYPE3RESULT
     */
    public function getGetTicketTypes3Result()
    {
      return $this->getTicketTypes3Result;
    }

    /**
     * @param D4WTPTICKETTYPE3RESULT $getTicketTypes3Result
     * @return \Axess\Dci4Wtp\getTicketTypes3Response
     */
    public function setGetTicketTypes3Result($getTicketTypes3Result)
    {
      $this->getTicketTypes3Result = $getTicketTypes3Result;
      return $this;
    }

}
