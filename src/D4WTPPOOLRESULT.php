<?php

namespace Axess\Dci4Wtp;

class D4WTPPOOLRESULT
{

    /**
     * @var ArrayOfD4WTPPOOL $ACTPOOLS
     */
    protected $ACTPOOLS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPOOL
     */
    public function getACTPOOLS()
    {
      return $this->ACTPOOLS;
    }

    /**
     * @param ArrayOfD4WTPPOOL $ACTPOOLS
     * @return \Axess\Dci4Wtp\D4WTPPOOLRESULT
     */
    public function setACTPOOLS($ACTPOOLS)
    {
      $this->ACTPOOLS = $ACTPOOLS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPOOLRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPOOLRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
