<?php

namespace Axess\Dci4Wtp;

class D4WTPCONFIG
{

    /**
     * @var float $NPARAMID
     */
    protected $NPARAMID = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZVALUE
     */
    protected $SZVALUE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPARAMID()
    {
      return $this->NPARAMID;
    }

    /**
     * @param float $NPARAMID
     * @return \Axess\Dci4Wtp\D4WTPCONFIG
     */
    public function setNPARAMID($NPARAMID)
    {
      $this->NPARAMID = $NPARAMID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPCONFIG
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALUE()
    {
      return $this->SZVALUE;
    }

    /**
     * @param string $SZVALUE
     * @return \Axess\Dci4Wtp\D4WTPCONFIG
     */
    public function setSZVALUE($SZVALUE)
    {
      $this->SZVALUE = $SZVALUE;
      return $this;
    }

}
