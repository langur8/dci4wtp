<?php

namespace Axess\Dci4Wtp;

class D4WTPPROMPTS
{

    /**
     * @var ArrayOfD4WTPPROMPTELEMENTS $ACTPROMPTELEMENTS
     */
    protected $ACTPROMPTELEMENTS = null;

    /**
     * @var float $NPROMPTNO
     */
    protected $NPROMPTNO = null;

    /**
     * @var string $SZPROMPTNAME
     */
    protected $SZPROMPTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPROMPTELEMENTS
     */
    public function getACTPROMPTELEMENTS()
    {
      return $this->ACTPROMPTELEMENTS;
    }

    /**
     * @param ArrayOfD4WTPPROMPTELEMENTS $ACTPROMPTELEMENTS
     * @return \Axess\Dci4Wtp\D4WTPPROMPTS
     */
    public function setACTPROMPTELEMENTS($ACTPROMPTELEMENTS)
    {
      $this->ACTPROMPTELEMENTS = $ACTPROMPTELEMENTS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROMPTNO()
    {
      return $this->NPROMPTNO;
    }

    /**
     * @param float $NPROMPTNO
     * @return \Axess\Dci4Wtp\D4WTPPROMPTS
     */
    public function setNPROMPTNO($NPROMPTNO)
    {
      $this->NPROMPTNO = $NPROMPTNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPROMPTNAME()
    {
      return $this->SZPROMPTNAME;
    }

    /**
     * @param string $SZPROMPTNAME
     * @return \Axess\Dci4Wtp\D4WTPPROMPTS
     */
    public function setSZPROMPTNAME($SZPROMPTNAME)
    {
      $this->SZPROMPTNAME = $SZPROMPTNAME;
      return $this;
    }

}
