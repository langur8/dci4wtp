<?php

namespace Axess\Dci4Wtp;

class getPackageTariffList5Response
{

    /**
     * @var D4WTPPKGTARIFFLIST5RESULT $getPackageTariffList5Result
     */
    protected $getPackageTariffList5Result = null;

    /**
     * @param D4WTPPKGTARIFFLIST5RESULT $getPackageTariffList5Result
     */
    public function __construct($getPackageTariffList5Result)
    {
      $this->getPackageTariffList5Result = $getPackageTariffList5Result;
    }

    /**
     * @return D4WTPPKGTARIFFLIST5RESULT
     */
    public function getGetPackageTariffList5Result()
    {
      return $this->getPackageTariffList5Result;
    }

    /**
     * @param D4WTPPKGTARIFFLIST5RESULT $getPackageTariffList5Result
     * @return \Axess\Dci4Wtp\getPackageTariffList5Response
     */
    public function setGetPackageTariffList5Result($getPackageTariffList5Result)
    {
      $this->getPackageTariffList5Result = $getPackageTariffList5Result;
      return $this;
    }

}
