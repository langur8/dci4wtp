<?php

namespace Axess\Dci4Wtp;

class D4WTPSWISSPASSSALESREQ
{

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZAUSWEISID
     */
    protected $SZAUSWEISID = null;

    /**
     * @var string $SZDAYSOLD
     */
    protected $SZDAYSOLD = null;

    /**
     * @var string $SZPOSTLEITZAHL
     */
    protected $SZPOSTLEITZAHL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSSALESREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAUSWEISID()
    {
      return $this->SZAUSWEISID;
    }

    /**
     * @param string $SZAUSWEISID
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSSALESREQ
     */
    public function setSZAUSWEISID($SZAUSWEISID)
    {
      $this->SZAUSWEISID = $SZAUSWEISID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDAYSOLD()
    {
      return $this->SZDAYSOLD;
    }

    /**
     * @param string $SZDAYSOLD
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSSALESREQ
     */
    public function setSZDAYSOLD($SZDAYSOLD)
    {
      $this->SZDAYSOLD = $SZDAYSOLD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOSTLEITZAHL()
    {
      return $this->SZPOSTLEITZAHL;
    }

    /**
     * @param string $SZPOSTLEITZAHL
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSSALESREQ
     */
    public function setSZPOSTLEITZAHL($SZPOSTLEITZAHL)
    {
      $this->SZPOSTLEITZAHL = $SZPOSTLEITZAHL;
      return $this;
    }

}
