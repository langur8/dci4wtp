<?php

namespace Axess\Dci4Wtp;

class decContingent
{

    /**
     * @var D4WTPDECCONTINGENTREQUEST $i_ctDecContingentReq
     */
    protected $i_ctDecContingentReq = null;

    /**
     * @param D4WTPDECCONTINGENTREQUEST $i_ctDecContingentReq
     */
    public function __construct($i_ctDecContingentReq)
    {
      $this->i_ctDecContingentReq = $i_ctDecContingentReq;
    }

    /**
     * @return D4WTPDECCONTINGENTREQUEST
     */
    public function getI_ctDecContingentReq()
    {
      return $this->i_ctDecContingentReq;
    }

    /**
     * @param D4WTPDECCONTINGENTREQUEST $i_ctDecContingentReq
     * @return \Axess\Dci4Wtp\decContingent
     */
    public function setI_ctDecContingentReq($i_ctDecContingentReq)
    {
      $this->i_ctDecContingentReq = $i_ctDecContingentReq;
      return $this;
    }

}
