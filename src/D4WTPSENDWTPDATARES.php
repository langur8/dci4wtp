<?php

namespace Axess\Dci4Wtp;

class D4WTPSENDWTPDATARES
{

    /**
     * @var ArrayOfD4WTPWTPDATAOCC $ACTWTPDATA
     */
    protected $ACTWTPDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NMAXLOGNR
     */
    protected $NMAXLOGNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPWTPDATAOCC
     */
    public function getACTWTPDATA()
    {
      return $this->ACTWTPDATA;
    }

    /**
     * @param ArrayOfD4WTPWTPDATAOCC $ACTWTPDATA
     * @return \Axess\Dci4Wtp\D4WTPSENDWTPDATARES
     */
    public function setACTWTPDATA($ACTWTPDATA)
    {
      $this->ACTWTPDATA = $ACTWTPDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPSENDWTPDATARES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMAXLOGNR()
    {
      return $this->NMAXLOGNR;
    }

    /**
     * @param float $NMAXLOGNR
     * @return \Axess\Dci4Wtp\D4WTPSENDWTPDATARES
     */
    public function setNMAXLOGNR($NMAXLOGNR)
    {
      $this->NMAXLOGNR = $NMAXLOGNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPSENDWTPDATARES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
