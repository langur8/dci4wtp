<?php

namespace Axess\Dci4Wtp;

class D4WTPSALESDATA2
{

    /**
     * @var ArrayOfD4WTPADDARTICLEINFO $ACTADDARTICLEINFO
     */
    protected $ACTADDARTICLEINFO = null;

    /**
     * @var ArrayOfD4WTPADDLICENSEPLATES $ACTADDLICENSEPLATES
     */
    protected $ACTADDLICENSEPLATES = null;

    /**
     * @var float $BISCANCELLED
     */
    protected $BISCANCELLED = null;

    /**
     * @var D4WTPERSONDATA $CTPERSONDATA
     */
    protected $CTPERSONDATA = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NLFDNR
     */
    protected $NLFDNR = null;

    /**
     * @var float $NPERCENTVAT
     */
    protected $NPERCENTVAT = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NSTATUS
     */
    protected $NSTATUS = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NUNICODENO
     */
    protected $NUNICODENO = null;

    /**
     * @var string $SZCASHIERFIRSTNAME
     */
    protected $SZCASHIERFIRSTNAME = null;

    /**
     * @var string $SZCASHIERLASTNAME
     */
    protected $SZCASHIERLASTNAME = null;

    /**
     * @var string $SZCREATIONTIME
     */
    protected $SZCREATIONTIME = null;

    /**
     * @var string $SZCURRENCY
     */
    protected $SZCURRENCY = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZLICENSEPLATE
     */
    protected $SZLICENSEPLATE = null;

    /**
     * @var string $SZPAYMENTTYPENAME
     */
    protected $SZPAYMENTTYPENAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZSTELLPLATZ
     */
    protected $SZSTELLPLATZ = null;

    /**
     * @var string $SZTICKETTYPENAME
     */
    protected $SZTICKETTYPENAME = null;

    /**
     * @var string $SZTICKETTYPESHORTNAME
     */
    protected $SZTICKETTYPESHORTNAME = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    /**
     * @var string $SZWTPNO
     */
    protected $SZWTPNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDARTICLEINFO
     */
    public function getACTADDARTICLEINFO()
    {
      return $this->ACTADDARTICLEINFO;
    }

    /**
     * @param ArrayOfD4WTPADDARTICLEINFO $ACTADDARTICLEINFO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setACTADDARTICLEINFO($ACTADDARTICLEINFO)
    {
      $this->ACTADDARTICLEINFO = $ACTADDARTICLEINFO;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPADDLICENSEPLATES
     */
    public function getACTADDLICENSEPLATES()
    {
      return $this->ACTADDLICENSEPLATES;
    }

    /**
     * @param ArrayOfD4WTPADDLICENSEPLATES $ACTADDLICENSEPLATES
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setACTADDLICENSEPLATES($ACTADDLICENSEPLATES)
    {
      $this->ACTADDLICENSEPLATES = $ACTADDLICENSEPLATES;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISCANCELLED()
    {
      return $this->BISCANCELLED;
    }

    /**
     * @param float $BISCANCELLED
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setBISCANCELLED($BISCANCELLED)
    {
      $this->BISCANCELLED = $BISCANCELLED;
      return $this;
    }

    /**
     * @return D4WTPERSONDATA
     */
    public function getCTPERSONDATA()
    {
      return $this->CTPERSONDATA;
    }

    /**
     * @param D4WTPERSONDATA $CTPERSONDATA
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setCTPERSONDATA($CTPERSONDATA)
    {
      $this->CTPERSONDATA = $CTPERSONDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDNR()
    {
      return $this->NLFDNR;
    }

    /**
     * @param float $NLFDNR
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setNLFDNR($NLFDNR)
    {
      $this->NLFDNR = $NLFDNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERCENTVAT()
    {
      return $this->NPERCENTVAT;
    }

    /**
     * @param float $NPERCENTVAT
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setNPERCENTVAT($NPERCENTVAT)
    {
      $this->NPERCENTVAT = $NPERCENTVAT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTATUS()
    {
      return $this->NSTATUS;
    }

    /**
     * @param float $NSTATUS
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setNSTATUS($NSTATUS)
    {
      $this->NSTATUS = $NSTATUS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENO()
    {
      return $this->NUNICODENO;
    }

    /**
     * @param float $NUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setNUNICODENO($NUNICODENO)
    {
      $this->NUNICODENO = $NUNICODENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCASHIERFIRSTNAME()
    {
      return $this->SZCASHIERFIRSTNAME;
    }

    /**
     * @param string $SZCASHIERFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZCASHIERFIRSTNAME($SZCASHIERFIRSTNAME)
    {
      $this->SZCASHIERFIRSTNAME = $SZCASHIERFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCASHIERLASTNAME()
    {
      return $this->SZCASHIERLASTNAME;
    }

    /**
     * @param string $SZCASHIERLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZCASHIERLASTNAME($SZCASHIERLASTNAME)
    {
      $this->SZCASHIERLASTNAME = $SZCASHIERLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCREATIONTIME()
    {
      return $this->SZCREATIONTIME;
    }

    /**
     * @param string $SZCREATIONTIME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZCREATIONTIME($SZCREATIONTIME)
    {
      $this->SZCREATIONTIME = $SZCREATIONTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCY()
    {
      return $this->SZCURRENCY;
    }

    /**
     * @param string $SZCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZCURRENCY($SZCURRENCY)
    {
      $this->SZCURRENCY = $SZCURRENCY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLICENSEPLATE()
    {
      return $this->SZLICENSEPLATE;
    }

    /**
     * @param string $SZLICENSEPLATE
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZLICENSEPLATE($SZLICENSEPLATE)
    {
      $this->SZLICENSEPLATE = $SZLICENSEPLATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPAYMENTTYPENAME()
    {
      return $this->SZPAYMENTTYPENAME;
    }

    /**
     * @param string $SZPAYMENTTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZPAYMENTTYPENAME($SZPAYMENTTYPENAME)
    {
      $this->SZPAYMENTTYPENAME = $SZPAYMENTTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTELLPLATZ()
    {
      return $this->SZSTELLPLATZ;
    }

    /**
     * @param string $SZSTELLPLATZ
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZSTELLPLATZ($SZSTELLPLATZ)
    {
      $this->SZSTELLPLATZ = $SZSTELLPLATZ;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPENAME()
    {
      return $this->SZTICKETTYPENAME;
    }

    /**
     * @param string $SZTICKETTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZTICKETTYPENAME($SZTICKETTYPENAME)
    {
      $this->SZTICKETTYPENAME = $SZTICKETTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPESHORTNAME()
    {
      return $this->SZTICKETTYPESHORTNAME;
    }

    /**
     * @param string $SZTICKETTYPESHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZTICKETTYPESHORTNAME($SZTICKETTYPESHORTNAME)
    {
      $this->SZTICKETTYPESHORTNAME = $SZTICKETTYPESHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNO()
    {
      return $this->SZWTPNO;
    }

    /**
     * @param string $SZWTPNO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA2
     */
    public function setSZWTPNO($SZWTPNO)
    {
      $this->SZWTPNO = $SZWTPNO;
      return $this;
    }

}
