<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCURRENCYREC implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCURRENCYREC[] $D4WTPCURRENCYREC
     */
    protected $D4WTPCURRENCYREC = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCURRENCYREC[]
     */
    public function getD4WTPCURRENCYREC()
    {
      return $this->D4WTPCURRENCYREC;
    }

    /**
     * @param D4WTPCURRENCYREC[] $D4WTPCURRENCYREC
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCURRENCYREC
     */
    public function setD4WTPCURRENCYREC(array $D4WTPCURRENCYREC = null)
    {
      $this->D4WTPCURRENCYREC = $D4WTPCURRENCYREC;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCURRENCYREC[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCURRENCYREC
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCURRENCYREC[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCURRENCYREC $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCURRENCYREC[] = $value;
      } else {
        $this->D4WTPCURRENCYREC[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCURRENCYREC[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCURRENCYREC Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCURRENCYREC);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCURRENCYREC);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCURRENCYREC);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCURRENCYREC);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCURRENCYREC Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCURRENCYREC);
    }

}
