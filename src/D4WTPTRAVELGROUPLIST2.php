<?php

namespace Axess\Dci4Wtp;

class D4WTPTRAVELGROUPLIST2
{

    /**
     * @var ArrayOfD4WTPBOOKTIMESLOTINFOS $ACTBOOKTIMESLOTINFOS
     */
    protected $ACTBOOKTIMESLOTINFOS = null;

    /**
     * @var ArrayOfD4WTPSUBCONTDATA $ACTSUBKONTDATA
     */
    protected $ACTSUBKONTDATA = null;

    /**
     * @var ArrayOfD4WTPTICKETTYPELIST $ACTTICKETTYPELIST
     */
    protected $ACTTICKETTYPELIST = null;

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NLOCKPOSNO
     */
    protected $NLOCKPOSNO = null;

    /**
     * @var float $NLOCKPROJNO
     */
    protected $NLOCKPROJNO = null;

    /**
     * @var float $NNAMEDUSERID
     */
    protected $NNAMEDUSERID = null;

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var float $NSHOPPINGCARTPOSNO
     */
    protected $NSHOPPINGCARTPOSNO = null;

    /**
     * @var float $NSHOPPINGCARTPROJNO
     */
    protected $NSHOPPINGCARTPROJNO = null;

    /**
     * @var float $NSTATUSNO
     */
    protected $NSTATUSNO = null;

    /**
     * @var float $NTRAVELGROUPNO
     */
    protected $NTRAVELGROUPNO = null;

    /**
     * @var float $NTRAVELGROUPPOSNO
     */
    protected $NTRAVELGROUPPOSNO = null;

    /**
     * @var float $NTRAVELGROUPPROJNO
     */
    protected $NTRAVELGROUPPROJNO = null;

    /**
     * @var string $SZCOMPANYEMAIL
     */
    protected $SZCOMPANYEMAIL = null;

    /**
     * @var string $SZCOMPANYFAXNO
     */
    protected $SZCOMPANYFAXNO = null;

    /**
     * @var string $SZCOMPANYNAME
     */
    protected $SZCOMPANYNAME = null;

    /**
     * @var string $SZCOMPANYPHONENO
     */
    protected $SZCOMPANYPHONENO = null;

    /**
     * @var string $SZCREATEDATE
     */
    protected $SZCREATEDATE = null;

    /**
     * @var string $SZDBSESSIONID
     */
    protected $SZDBSESSIONID = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZEXTGROUPCODE
     */
    protected $SZEXTGROUPCODE = null;

    /**
     * @var string $SZLOCKUNTIL
     */
    protected $SZLOCKUNTIL = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSTATUS
     */
    protected $SZSTATUS = null;

    /**
     * @var string $SZTRAVELDATE
     */
    protected $SZTRAVELDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPBOOKTIMESLOTINFOS
     */
    public function getACTBOOKTIMESLOTINFOS()
    {
      return $this->ACTBOOKTIMESLOTINFOS;
    }

    /**
     * @param ArrayOfD4WTPBOOKTIMESLOTINFOS $ACTBOOKTIMESLOTINFOS
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setACTBOOKTIMESLOTINFOS($ACTBOOKTIMESLOTINFOS)
    {
      $this->ACTBOOKTIMESLOTINFOS = $ACTBOOKTIMESLOTINFOS;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPSUBCONTDATA
     */
    public function getACTSUBKONTDATA()
    {
      return $this->ACTSUBKONTDATA;
    }

    /**
     * @param ArrayOfD4WTPSUBCONTDATA $ACTSUBKONTDATA
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setACTSUBKONTDATA($ACTSUBKONTDATA)
    {
      $this->ACTSUBKONTDATA = $ACTSUBKONTDATA;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPTICKETTYPELIST
     */
    public function getACTTICKETTYPELIST()
    {
      return $this->ACTTICKETTYPELIST;
    }

    /**
     * @param ArrayOfD4WTPTICKETTYPELIST $ACTTICKETTYPELIST
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setACTTICKETTYPELIST($ACTTICKETTYPELIST)
    {
      $this->ACTTICKETTYPELIST = $ACTTICKETTYPELIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLOCKPOSNO()
    {
      return $this->NLOCKPOSNO;
    }

    /**
     * @param float $NLOCKPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setNLOCKPOSNO($NLOCKPOSNO)
    {
      $this->NLOCKPOSNO = $NLOCKPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLOCKPROJNO()
    {
      return $this->NLOCKPROJNO;
    }

    /**
     * @param float $NLOCKPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setNLOCKPROJNO($NLOCKPROJNO)
    {
      $this->NLOCKPROJNO = $NLOCKPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNAMEDUSERID()
    {
      return $this->NNAMEDUSERID;
    }

    /**
     * @param float $NNAMEDUSERID
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setNNAMEDUSERID($NNAMEDUSERID)
    {
      $this->NNAMEDUSERID = $NNAMEDUSERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPOSNO()
    {
      return $this->NSHOPPINGCARTPOSNO;
    }

    /**
     * @param float $NSHOPPINGCARTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setNSHOPPINGCARTPOSNO($NSHOPPINGCARTPOSNO)
    {
      $this->NSHOPPINGCARTPOSNO = $NSHOPPINGCARTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPROJNO()
    {
      return $this->NSHOPPINGCARTPROJNO;
    }

    /**
     * @param float $NSHOPPINGCARTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setNSHOPPINGCARTPROJNO($NSHOPPINGCARTPROJNO)
    {
      $this->NSHOPPINGCARTPROJNO = $NSHOPPINGCARTPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTATUSNO()
    {
      return $this->NSTATUSNO;
    }

    /**
     * @param float $NSTATUSNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setNSTATUSNO($NSTATUSNO)
    {
      $this->NSTATUSNO = $NSTATUSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPNO()
    {
      return $this->NTRAVELGROUPNO;
    }

    /**
     * @param float $NTRAVELGROUPNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setNTRAVELGROUPNO($NTRAVELGROUPNO)
    {
      $this->NTRAVELGROUPNO = $NTRAVELGROUPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPOSNO()
    {
      return $this->NTRAVELGROUPPOSNO;
    }

    /**
     * @param float $NTRAVELGROUPPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setNTRAVELGROUPPOSNO($NTRAVELGROUPPOSNO)
    {
      $this->NTRAVELGROUPPOSNO = $NTRAVELGROUPPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRAVELGROUPPROJNO()
    {
      return $this->NTRAVELGROUPPROJNO;
    }

    /**
     * @param float $NTRAVELGROUPPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setNTRAVELGROUPPROJNO($NTRAVELGROUPPROJNO)
    {
      $this->NTRAVELGROUPPROJNO = $NTRAVELGROUPPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYEMAIL()
    {
      return $this->SZCOMPANYEMAIL;
    }

    /**
     * @param string $SZCOMPANYEMAIL
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setSZCOMPANYEMAIL($SZCOMPANYEMAIL)
    {
      $this->SZCOMPANYEMAIL = $SZCOMPANYEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYFAXNO()
    {
      return $this->SZCOMPANYFAXNO;
    }

    /**
     * @param string $SZCOMPANYFAXNO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setSZCOMPANYFAXNO($SZCOMPANYFAXNO)
    {
      $this->SZCOMPANYFAXNO = $SZCOMPANYFAXNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYNAME()
    {
      return $this->SZCOMPANYNAME;
    }

    /**
     * @param string $SZCOMPANYNAME
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setSZCOMPANYNAME($SZCOMPANYNAME)
    {
      $this->SZCOMPANYNAME = $SZCOMPANYNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYPHONENO()
    {
      return $this->SZCOMPANYPHONENO;
    }

    /**
     * @param string $SZCOMPANYPHONENO
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setSZCOMPANYPHONENO($SZCOMPANYPHONENO)
    {
      $this->SZCOMPANYPHONENO = $SZCOMPANYPHONENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCREATEDATE()
    {
      return $this->SZCREATEDATE;
    }

    /**
     * @param string $SZCREATEDATE
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setSZCREATEDATE($SZCREATEDATE)
    {
      $this->SZCREATEDATE = $SZCREATEDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDBSESSIONID()
    {
      return $this->SZDBSESSIONID;
    }

    /**
     * @param string $SZDBSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setSZDBSESSIONID($SZDBSESSIONID)
    {
      $this->SZDBSESSIONID = $SZDBSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTGROUPCODE()
    {
      return $this->SZEXTGROUPCODE;
    }

    /**
     * @param string $SZEXTGROUPCODE
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setSZEXTGROUPCODE($SZEXTGROUPCODE)
    {
      $this->SZEXTGROUPCODE = $SZEXTGROUPCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOCKUNTIL()
    {
      return $this->SZLOCKUNTIL;
    }

    /**
     * @param string $SZLOCKUNTIL
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setSZLOCKUNTIL($SZLOCKUNTIL)
    {
      $this->SZLOCKUNTIL = $SZLOCKUNTIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTATUS()
    {
      return $this->SZSTATUS;
    }

    /**
     * @param string $SZSTATUS
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setSZSTATUS($SZSTATUS)
    {
      $this->SZSTATUS = $SZSTATUS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTRAVELDATE()
    {
      return $this->SZTRAVELDATE;
    }

    /**
     * @param string $SZTRAVELDATE
     * @return \Axess\Dci4Wtp\D4WTPTRAVELGROUPLIST2
     */
    public function setSZTRAVELDATE($SZTRAVELDATE)
    {
      $this->SZTRAVELDATE = $SZTRAVELDATE;
      return $this;
    }

}
