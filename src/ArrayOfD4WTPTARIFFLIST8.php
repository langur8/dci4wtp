<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTARIFFLIST8 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTARIFFLIST8[] $D4WTPTARIFFLIST8
     */
    protected $D4WTPTARIFFLIST8 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTARIFFLIST8[]
     */
    public function getD4WTPTARIFFLIST8()
    {
      return $this->D4WTPTARIFFLIST8;
    }

    /**
     * @param D4WTPTARIFFLIST8[] $D4WTPTARIFFLIST8
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTARIFFLIST8
     */
    public function setD4WTPTARIFFLIST8(array $D4WTPTARIFFLIST8 = null)
    {
      $this->D4WTPTARIFFLIST8 = $D4WTPTARIFFLIST8;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTARIFFLIST8[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTARIFFLIST8
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTARIFFLIST8[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTARIFFLIST8 $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTARIFFLIST8[] = $value;
      } else {
        $this->D4WTPTARIFFLIST8[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTARIFFLIST8[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTARIFFLIST8 Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTARIFFLIST8);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTARIFFLIST8);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTARIFFLIST8);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTARIFFLIST8);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTARIFFLIST8 Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTARIFFLIST8);
    }

}
