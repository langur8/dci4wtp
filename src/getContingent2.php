<?php

namespace Axess\Dci4Wtp;

class getContingent2
{

    /**
     * @var D4WTPCONTINGENT2REQUEST $i_ctContingentReq
     */
    protected $i_ctContingentReq = null;

    /**
     * @param D4WTPCONTINGENT2REQUEST $i_ctContingentReq
     */
    public function __construct($i_ctContingentReq)
    {
      $this->i_ctContingentReq = $i_ctContingentReq;
    }

    /**
     * @return D4WTPCONTINGENT2REQUEST
     */
    public function getI_ctContingentReq()
    {
      return $this->i_ctContingentReq;
    }

    /**
     * @param D4WTPCONTINGENT2REQUEST $i_ctContingentReq
     * @return \Axess\Dci4Wtp\getContingent2
     */
    public function setI_ctContingentReq($i_ctContingentReq)
    {
      $this->i_ctContingentReq = $i_ctContingentReq;
      return $this;
    }

}
