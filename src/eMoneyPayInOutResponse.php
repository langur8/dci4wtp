<?php

namespace Axess\Dci4Wtp;

class eMoneyPayInOutResponse
{

    /**
     * @var D4WTPEMONEYPAYINOUTRES $eMoneyPayInOutResult
     */
    protected $eMoneyPayInOutResult = null;

    /**
     * @param D4WTPEMONEYPAYINOUTRES $eMoneyPayInOutResult
     */
    public function __construct($eMoneyPayInOutResult)
    {
      $this->eMoneyPayInOutResult = $eMoneyPayInOutResult;
    }

    /**
     * @return D4WTPEMONEYPAYINOUTRES
     */
    public function getEMoneyPayInOutResult()
    {
      return $this->eMoneyPayInOutResult;
    }

    /**
     * @param D4WTPEMONEYPAYINOUTRES $eMoneyPayInOutResult
     * @return \Axess\Dci4Wtp\eMoneyPayInOutResponse
     */
    public function setEMoneyPayInOutResult($eMoneyPayInOutResult)
    {
      $this->eMoneyPayInOutResult = $eMoneyPayInOutResult;
      return $this;
    }

}
