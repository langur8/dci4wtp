<?php

namespace Axess\Dci4Wtp;

class D4WTPBOOKTIMESLOTINFOS
{

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var float $NCONFIGLIMITNO
     */
    protected $NCONFIGLIMITNO = null;

    /**
     * @var float $NCONFIGNO
     */
    protected $NCONFIGNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSLOTLFDNR
     */
    protected $NSLOTLFDNR = null;

    /**
     * @var string $SZCONFIGNAME
     */
    protected $SZCONFIGNAME = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZENDTIME
     */
    protected $SZENDTIME = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSTARTTIME
     */
    protected $SZSTARTTIME = null;

    /**
     * @var string $SZVALIDITYDAY
     */
    protected $SZVALIDITYDAY = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPBOOKTIMESLOTINFOS
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONFIGLIMITNO()
    {
      return $this->NCONFIGLIMITNO;
    }

    /**
     * @param float $NCONFIGLIMITNO
     * @return \Axess\Dci4Wtp\D4WTPBOOKTIMESLOTINFOS
     */
    public function setNCONFIGLIMITNO($NCONFIGLIMITNO)
    {
      $this->NCONFIGLIMITNO = $NCONFIGLIMITNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONFIGNO()
    {
      return $this->NCONFIGNO;
    }

    /**
     * @param float $NCONFIGNO
     * @return \Axess\Dci4Wtp\D4WTPBOOKTIMESLOTINFOS
     */
    public function setNCONFIGNO($NCONFIGNO)
    {
      $this->NCONFIGNO = $NCONFIGNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPBOOKTIMESLOTINFOS
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSLOTLFDNR()
    {
      return $this->NSLOTLFDNR;
    }

    /**
     * @param float $NSLOTLFDNR
     * @return \Axess\Dci4Wtp\D4WTPBOOKTIMESLOTINFOS
     */
    public function setNSLOTLFDNR($NSLOTLFDNR)
    {
      $this->NSLOTLFDNR = $NSLOTLFDNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCONFIGNAME()
    {
      return $this->SZCONFIGNAME;
    }

    /**
     * @param string $SZCONFIGNAME
     * @return \Axess\Dci4Wtp\D4WTPBOOKTIMESLOTINFOS
     */
    public function setSZCONFIGNAME($SZCONFIGNAME)
    {
      $this->SZCONFIGNAME = $SZCONFIGNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPBOOKTIMESLOTINFOS
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZENDTIME()
    {
      return $this->SZENDTIME;
    }

    /**
     * @param string $SZENDTIME
     * @return \Axess\Dci4Wtp\D4WTPBOOKTIMESLOTINFOS
     */
    public function setSZENDTIME($SZENDTIME)
    {
      $this->SZENDTIME = $SZENDTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPBOOKTIMESLOTINFOS
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTARTTIME()
    {
      return $this->SZSTARTTIME;
    }

    /**
     * @param string $SZSTARTTIME
     * @return \Axess\Dci4Wtp\D4WTPBOOKTIMESLOTINFOS
     */
    public function setSZSTARTTIME($SZSTARTTIME)
    {
      $this->SZSTARTTIME = $SZSTARTTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDITYDAY()
    {
      return $this->SZVALIDITYDAY;
    }

    /**
     * @param string $SZVALIDITYDAY
     * @return \Axess\Dci4Wtp\D4WTPBOOKTIMESLOTINFOS
     */
    public function setSZVALIDITYDAY($SZVALIDITYDAY)
    {
      $this->SZVALIDITYDAY = $SZVALIDITYDAY;
      return $this;
    }

}
