<?php

namespace Axess\Dci4Wtp;

class ChangeCompanyResponse
{

    /**
     * @var D4WTPCOMPANYRESULT $ChangeCompanyResult
     */
    protected $ChangeCompanyResult = null;

    /**
     * @param D4WTPCOMPANYRESULT $ChangeCompanyResult
     */
    public function __construct($ChangeCompanyResult)
    {
      $this->ChangeCompanyResult = $ChangeCompanyResult;
    }

    /**
     * @return D4WTPCOMPANYRESULT
     */
    public function getChangeCompanyResult()
    {
      return $this->ChangeCompanyResult;
    }

    /**
     * @param D4WTPCOMPANYRESULT $ChangeCompanyResult
     * @return \Axess\Dci4Wtp\ChangeCompanyResponse
     */
    public function setChangeCompanyResult($ChangeCompanyResult)
    {
      $this->ChangeCompanyResult = $ChangeCompanyResult;
      return $this;
    }

}
