<?php

namespace Axess\Dci4Wtp;

class D4WTPCOMPEXHIBITION
{

    /**
     * @var float $NEVENTNR
     */
    protected $NEVENTNR = null;

    /**
     * @var float $NEVENTPROJNR
     */
    protected $NEVENTPROJNR = null;

    /**
     * @var float $NEVENTROWNR
     */
    protected $NEVENTROWNR = null;

    /**
     * @var string $SZCMPALIAS
     */
    protected $SZCMPALIAS = null;

    /**
     * @var string $SZEXHIBITION
     */
    protected $SZEXHIBITION = null;

    /**
     * @var string $SZEXTCMPID
     */
    protected $SZEXTCMPID = null;

    /**
     * @var string $SZHALL
     */
    protected $SZHALL = null;

    /**
     * @var string $SZLEVEL
     */
    protected $SZLEVEL = null;

    /**
     * @var string $SZNAME2
     */
    protected $SZNAME2 = null;

    /**
     * @var string $SZNAME3
     */
    protected $SZNAME3 = null;

    /**
     * @var string $SZSTAND
     */
    protected $SZSTAND = null;

    /**
     * @var string $SZYEAR
     */
    protected $SZYEAR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNEVENTNR()
    {
      return $this->NEVENTNR;
    }

    /**
     * @param float $NEVENTNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPEXHIBITION
     */
    public function setNEVENTNR($NEVENTNR)
    {
      $this->NEVENTNR = $NEVENTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEVENTPROJNR()
    {
      return $this->NEVENTPROJNR;
    }

    /**
     * @param float $NEVENTPROJNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPEXHIBITION
     */
    public function setNEVENTPROJNR($NEVENTPROJNR)
    {
      $this->NEVENTPROJNR = $NEVENTPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEVENTROWNR()
    {
      return $this->NEVENTROWNR;
    }

    /**
     * @param float $NEVENTROWNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPEXHIBITION
     */
    public function setNEVENTROWNR($NEVENTROWNR)
    {
      $this->NEVENTROWNR = $NEVENTROWNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCMPALIAS()
    {
      return $this->SZCMPALIAS;
    }

    /**
     * @param string $SZCMPALIAS
     * @return \Axess\Dci4Wtp\D4WTPCOMPEXHIBITION
     */
    public function setSZCMPALIAS($SZCMPALIAS)
    {
      $this->SZCMPALIAS = $SZCMPALIAS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXHIBITION()
    {
      return $this->SZEXHIBITION;
    }

    /**
     * @param string $SZEXHIBITION
     * @return \Axess\Dci4Wtp\D4WTPCOMPEXHIBITION
     */
    public function setSZEXHIBITION($SZEXHIBITION)
    {
      $this->SZEXHIBITION = $SZEXHIBITION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTCMPID()
    {
      return $this->SZEXTCMPID;
    }

    /**
     * @param string $SZEXTCMPID
     * @return \Axess\Dci4Wtp\D4WTPCOMPEXHIBITION
     */
    public function setSZEXTCMPID($SZEXTCMPID)
    {
      $this->SZEXTCMPID = $SZEXTCMPID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZHALL()
    {
      return $this->SZHALL;
    }

    /**
     * @param string $SZHALL
     * @return \Axess\Dci4Wtp\D4WTPCOMPEXHIBITION
     */
    public function setSZHALL($SZHALL)
    {
      $this->SZHALL = $SZHALL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLEVEL()
    {
      return $this->SZLEVEL;
    }

    /**
     * @param string $SZLEVEL
     * @return \Axess\Dci4Wtp\D4WTPCOMPEXHIBITION
     */
    public function setSZLEVEL($SZLEVEL)
    {
      $this->SZLEVEL = $SZLEVEL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME2()
    {
      return $this->SZNAME2;
    }

    /**
     * @param string $SZNAME2
     * @return \Axess\Dci4Wtp\D4WTPCOMPEXHIBITION
     */
    public function setSZNAME2($SZNAME2)
    {
      $this->SZNAME2 = $SZNAME2;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME3()
    {
      return $this->SZNAME3;
    }

    /**
     * @param string $SZNAME3
     * @return \Axess\Dci4Wtp\D4WTPCOMPEXHIBITION
     */
    public function setSZNAME3($SZNAME3)
    {
      $this->SZNAME3 = $SZNAME3;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTAND()
    {
      return $this->SZSTAND;
    }

    /**
     * @param string $SZSTAND
     * @return \Axess\Dci4Wtp\D4WTPCOMPEXHIBITION
     */
    public function setSZSTAND($SZSTAND)
    {
      $this->SZSTAND = $SZSTAND;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZYEAR()
    {
      return $this->SZYEAR;
    }

    /**
     * @param string $SZYEAR
     * @return \Axess\Dci4Wtp\D4WTPCOMPEXHIBITION
     */
    public function setSZYEAR($SZYEAR)
    {
      $this->SZYEAR = $SZYEAR;
      return $this;
    }

}
