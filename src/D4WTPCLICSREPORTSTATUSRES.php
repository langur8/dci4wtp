<?php

namespace Axess\Dci4Wtp;

class D4WTPCLICSREPORTSTATUSRES
{

    /**
     * @var base64Binary $BLREPORT
     */
    protected $BLREPORT = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NSTATUS
     */
    protected $NSTATUS = null;

    /**
     * @var float $NWTPREPORTNR
     */
    protected $NWTPREPORTNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZFILENAME
     */
    protected $SZFILENAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return base64Binary
     */
    public function getBLREPORT()
    {
      return $this->BLREPORT;
    }

    /**
     * @param base64Binary $BLREPORT
     * @return \Axess\Dci4Wtp\D4WTPCLICSREPORTSTATUSRES
     */
    public function setBLREPORT($BLREPORT)
    {
      $this->BLREPORT = $BLREPORT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCLICSREPORTSTATUSRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTATUS()
    {
      return $this->NSTATUS;
    }

    /**
     * @param float $NSTATUS
     * @return \Axess\Dci4Wtp\D4WTPCLICSREPORTSTATUSRES
     */
    public function setNSTATUS($NSTATUS)
    {
      $this->NSTATUS = $NSTATUS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPREPORTNR()
    {
      return $this->NWTPREPORTNR;
    }

    /**
     * @param float $NWTPREPORTNR
     * @return \Axess\Dci4Wtp\D4WTPCLICSREPORTSTATUSRES
     */
    public function setNWTPREPORTNR($NWTPREPORTNR)
    {
      $this->NWTPREPORTNR = $NWTPREPORTNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCLICSREPORTSTATUSRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFILENAME()
    {
      return $this->SZFILENAME;
    }

    /**
     * @param string $SZFILENAME
     * @return \Axess\Dci4Wtp\D4WTPCLICSREPORTSTATUSRES
     */
    public function setSZFILENAME($SZFILENAME)
    {
      $this->SZFILENAME = $SZFILENAME;
      return $this;
    }

}
