<?php

namespace Axess\Dci4Wtp;

class D4WTPMSGCANCELTICKETREQUEST
{

    /**
     * @var float $NAPPTRANSLOGID
     */
    protected $NAPPTRANSLOGID = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    /**
     * @var string $SZMESSAGETYPE
     */
    protected $SZMESSAGETYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNAPPTRANSLOGID()
    {
      return $this->NAPPTRANSLOGID;
    }

    /**
     * @param float $NAPPTRANSLOGID
     * @return \Axess\Dci4Wtp\D4WTPMSGCANCELTICKETREQUEST
     */
    public function setNAPPTRANSLOGID($NAPPTRANSLOGID)
    {
      $this->NAPPTRANSLOGID = $NAPPTRANSLOGID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPMSGCANCELTICKETREQUEST
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPMSGCANCELTICKETREQUEST
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPMSGCANCELTICKETREQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPMSGCANCELTICKETREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPMSGCANCELTICKETREQUEST
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMESSAGETYPE()
    {
      return $this->SZMESSAGETYPE;
    }

    /**
     * @param string $SZMESSAGETYPE
     * @return \Axess\Dci4Wtp\D4WTPMSGCANCELTICKETREQUEST
     */
    public function setSZMESSAGETYPE($SZMESSAGETYPE)
    {
      $this->SZMESSAGETYPE = $SZMESSAGETYPE;
      return $this;
    }

}
