<?php

namespace Axess\Dci4Wtp;

class D4WTPGETFIRSTVALIDDAYRESULT
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var ArrayOfD4WTPVALIDDAYREC $VALIDDAYS
     */
    protected $VALIDDAYS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETFIRSTVALIDDAYRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETFIRSTVALIDDAYRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPVALIDDAYREC
     */
    public function getVALIDDAYS()
    {
      return $this->VALIDDAYS;
    }

    /**
     * @param ArrayOfD4WTPVALIDDAYREC $VALIDDAYS
     * @return \Axess\Dci4Wtp\D4WTPGETFIRSTVALIDDAYRESULT
     */
    public function setVALIDDAYS($VALIDDAYS)
    {
      $this->VALIDDAYS = $VALIDDAYS;
      return $this;
    }

}
