<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCASHIERSHIFT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCASHIERSHIFT[] $D4WTPCASHIERSHIFT
     */
    protected $D4WTPCASHIERSHIFT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCASHIERSHIFT[]
     */
    public function getD4WTPCASHIERSHIFT()
    {
      return $this->D4WTPCASHIERSHIFT;
    }

    /**
     * @param D4WTPCASHIERSHIFT[] $D4WTPCASHIERSHIFT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCASHIERSHIFT
     */
    public function setD4WTPCASHIERSHIFT(array $D4WTPCASHIERSHIFT = null)
    {
      $this->D4WTPCASHIERSHIFT = $D4WTPCASHIERSHIFT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCASHIERSHIFT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCASHIERSHIFT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCASHIERSHIFT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCASHIERSHIFT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCASHIERSHIFT[] = $value;
      } else {
        $this->D4WTPCASHIERSHIFT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCASHIERSHIFT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCASHIERSHIFT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCASHIERSHIFT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCASHIERSHIFT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCASHIERSHIFT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCASHIERSHIFT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCASHIERSHIFT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCASHIERSHIFT);
    }

}
