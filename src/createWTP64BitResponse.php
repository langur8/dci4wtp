<?php

namespace Axess\Dci4Wtp;

class createWTP64BitResponse
{

    /**
     * @var D4WTPCREATEWTP64BITRESULT $createWTP64BitResult
     */
    protected $createWTP64BitResult = null;

    /**
     * @param D4WTPCREATEWTP64BITRESULT $createWTP64BitResult
     */
    public function __construct($createWTP64BitResult)
    {
      $this->createWTP64BitResult = $createWTP64BitResult;
    }

    /**
     * @return D4WTPCREATEWTP64BITRESULT
     */
    public function getCreateWTP64BitResult()
    {
      return $this->createWTP64BitResult;
    }

    /**
     * @param D4WTPCREATEWTP64BITRESULT $createWTP64BitResult
     * @return \Axess\Dci4Wtp\createWTP64BitResponse
     */
    public function setCreateWTP64BitResult($createWTP64BitResult)
    {
      $this->createWTP64BitResult = $createWTP64BitResult;
      return $this;
    }

}
