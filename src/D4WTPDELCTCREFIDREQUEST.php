<?php

namespace Axess\Dci4Wtp;

class D4WTPDELCTCREFIDREQUEST
{

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NCTCARDTYPENO
     */
    protected $NCTCARDTYPENO = null;

    /**
     * @var float $NMEMBERSHIPTYPENO
     */
    protected $NMEMBERSHIPTYPENO = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZCTCARDREFID
     */
    protected $SZCTCARDREFID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPDELCTCREFIDREQUEST
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPDELCTCREFIDREQUEST
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPDELCTCREFIDREQUEST
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCTCARDTYPENO()
    {
      return $this->NCTCARDTYPENO;
    }

    /**
     * @param float $NCTCARDTYPENO
     * @return \Axess\Dci4Wtp\D4WTPDELCTCREFIDREQUEST
     */
    public function setNCTCARDTYPENO($NCTCARDTYPENO)
    {
      $this->NCTCARDTYPENO = $NCTCARDTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMEMBERSHIPTYPENO()
    {
      return $this->NMEMBERSHIPTYPENO;
    }

    /**
     * @param float $NMEMBERSHIPTYPENO
     * @return \Axess\Dci4Wtp\D4WTPDELCTCREFIDREQUEST
     */
    public function setNMEMBERSHIPTYPENO($NMEMBERSHIPTYPENO)
    {
      $this->NMEMBERSHIPTYPENO = $NMEMBERSHIPTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPDELCTCREFIDREQUEST
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPDELCTCREFIDREQUEST
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPDELCTCREFIDREQUEST
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPDELCTCREFIDREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCTCARDREFID()
    {
      return $this->SZCTCARDREFID;
    }

    /**
     * @param string $SZCTCARDREFID
     * @return \Axess\Dci4Wtp\D4WTPDELCTCREFIDREQUEST
     */
    public function setSZCTCARDREFID($SZCTCARDREFID)
    {
      $this->SZCTCARDREFID = $SZCTCARDREFID;
      return $this;
    }

}
