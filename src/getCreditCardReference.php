<?php

namespace Axess\Dci4Wtp;

class getCreditCardReference
{

    /**
     * @var D4WTPGETCTCREFIDREQUEST $i_ctGetCTCRefIdReq
     */
    protected $i_ctGetCTCRefIdReq = null;

    /**
     * @param D4WTPGETCTCREFIDREQUEST $i_ctGetCTCRefIdReq
     */
    public function __construct($i_ctGetCTCRefIdReq)
    {
      $this->i_ctGetCTCRefIdReq = $i_ctGetCTCRefIdReq;
    }

    /**
     * @return D4WTPGETCTCREFIDREQUEST
     */
    public function getI_ctGetCTCRefIdReq()
    {
      return $this->i_ctGetCTCRefIdReq;
    }

    /**
     * @param D4WTPGETCTCREFIDREQUEST $i_ctGetCTCRefIdReq
     * @return \Axess\Dci4Wtp\getCreditCardReference
     */
    public function setI_ctGetCTCRefIdReq($i_ctGetCTCRefIdReq)
    {
      $this->i_ctGetCTCRefIdReq = $i_ctGetCTCRefIdReq;
      return $this;
    }

}
