<?php

namespace Axess\Dci4Wtp;

class D4WTPCHANGEPWREQUEST
{

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZLOGINID
     */
    protected $SZLOGINID = null;

    /**
     * @var string $SZLOGINMODE
     */
    protected $SZLOGINMODE = null;

    /**
     * @var string $SZNEWPASSWORD
     */
    protected $SZNEWPASSWORD = null;

    /**
     * @var string $SZOLDPASSWORD
     */
    protected $SZOLDPASSWORD = null;

    /**
     * @var string $SZUSERNAME
     */
    protected $SZUSERNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPWREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOGINID()
    {
      return $this->SZLOGINID;
    }

    /**
     * @param string $SZLOGINID
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPWREQUEST
     */
    public function setSZLOGINID($SZLOGINID)
    {
      $this->SZLOGINID = $SZLOGINID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOGINMODE()
    {
      return $this->SZLOGINMODE;
    }

    /**
     * @param string $SZLOGINMODE
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPWREQUEST
     */
    public function setSZLOGINMODE($SZLOGINMODE)
    {
      $this->SZLOGINMODE = $SZLOGINMODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNEWPASSWORD()
    {
      return $this->SZNEWPASSWORD;
    }

    /**
     * @param string $SZNEWPASSWORD
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPWREQUEST
     */
    public function setSZNEWPASSWORD($SZNEWPASSWORD)
    {
      $this->SZNEWPASSWORD = $SZNEWPASSWORD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZOLDPASSWORD()
    {
      return $this->SZOLDPASSWORD;
    }

    /**
     * @param string $SZOLDPASSWORD
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPWREQUEST
     */
    public function setSZOLDPASSWORD($SZOLDPASSWORD)
    {
      $this->SZOLDPASSWORD = $SZOLDPASSWORD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUSERNAME()
    {
      return $this->SZUSERNAME;
    }

    /**
     * @param string $SZUSERNAME
     * @return \Axess\Dci4Wtp\D4WTPCHANGEPWREQUEST
     */
    public function setSZUSERNAME($SZUSERNAME)
    {
      $this->SZUSERNAME = $SZUSERNAME;
      return $this;
    }

}
