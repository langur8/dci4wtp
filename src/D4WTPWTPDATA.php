<?php

namespace Axess\Dci4Wtp;

class D4WTPWTPDATA
{

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var float $NDATACARRIERSERIALNUMBER
     */
    protected $NDATACARRIERSERIALNUMBER = null;

    /**
     * @var float $NPOOLNR
     */
    protected $NPOOLNR = null;

    /**
     * @var string $SZCODINGFIXDATA
     */
    protected $SZCODINGFIXDATA = null;

    /**
     * @var string $SZCODINGINFORMATION
     */
    protected $SZCODINGINFORMATION = null;

    /**
     * @var string $SZCODINGVARDATA
     */
    protected $SZCODINGVARDATA = null;

    /**
     * @var string $SZMEDIAIDHEX
     */
    protected $SZMEDIAIDHEX = null;

    /**
     * @var string $SZPERMISSIONHANDLE
     */
    protected $SZPERMISSIONHANDLE = null;

    /**
     * @var string $SZSOURCESYSTEM
     */
    protected $SZSOURCESYSTEM = null;

    /**
     * @var string $SZTICKETITEMID
     */
    protected $SZTICKETITEMID = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPWTPDATA
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATACARRIERSERIALNUMBER()
    {
      return $this->NDATACARRIERSERIALNUMBER;
    }

    /**
     * @param float $NDATACARRIERSERIALNUMBER
     * @return \Axess\Dci4Wtp\D4WTPWTPDATA
     */
    public function setNDATACARRIERSERIALNUMBER($NDATACARRIERSERIALNUMBER)
    {
      $this->NDATACARRIERSERIALNUMBER = $NDATACARRIERSERIALNUMBER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNR()
    {
      return $this->NPOOLNR;
    }

    /**
     * @param float $NPOOLNR
     * @return \Axess\Dci4Wtp\D4WTPWTPDATA
     */
    public function setNPOOLNR($NPOOLNR)
    {
      $this->NPOOLNR = $NPOOLNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGFIXDATA()
    {
      return $this->SZCODINGFIXDATA;
    }

    /**
     * @param string $SZCODINGFIXDATA
     * @return \Axess\Dci4Wtp\D4WTPWTPDATA
     */
    public function setSZCODINGFIXDATA($SZCODINGFIXDATA)
    {
      $this->SZCODINGFIXDATA = $SZCODINGFIXDATA;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGINFORMATION()
    {
      return $this->SZCODINGINFORMATION;
    }

    /**
     * @param string $SZCODINGINFORMATION
     * @return \Axess\Dci4Wtp\D4WTPWTPDATA
     */
    public function setSZCODINGINFORMATION($SZCODINGINFORMATION)
    {
      $this->SZCODINGINFORMATION = $SZCODINGINFORMATION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGVARDATA()
    {
      return $this->SZCODINGVARDATA;
    }

    /**
     * @param string $SZCODINGVARDATA
     * @return \Axess\Dci4Wtp\D4WTPWTPDATA
     */
    public function setSZCODINGVARDATA($SZCODINGVARDATA)
    {
      $this->SZCODINGVARDATA = $SZCODINGVARDATA;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAIDHEX()
    {
      return $this->SZMEDIAIDHEX;
    }

    /**
     * @param string $SZMEDIAIDHEX
     * @return \Axess\Dci4Wtp\D4WTPWTPDATA
     */
    public function setSZMEDIAIDHEX($SZMEDIAIDHEX)
    {
      $this->SZMEDIAIDHEX = $SZMEDIAIDHEX;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERMISSIONHANDLE()
    {
      return $this->SZPERMISSIONHANDLE;
    }

    /**
     * @param string $SZPERMISSIONHANDLE
     * @return \Axess\Dci4Wtp\D4WTPWTPDATA
     */
    public function setSZPERMISSIONHANDLE($SZPERMISSIONHANDLE)
    {
      $this->SZPERMISSIONHANDLE = $SZPERMISSIONHANDLE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSOURCESYSTEM()
    {
      return $this->SZSOURCESYSTEM;
    }

    /**
     * @param string $SZSOURCESYSTEM
     * @return \Axess\Dci4Wtp\D4WTPWTPDATA
     */
    public function setSZSOURCESYSTEM($SZSOURCESYSTEM)
    {
      $this->SZSOURCESYSTEM = $SZSOURCESYSTEM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETITEMID()
    {
      return $this->SZTICKETITEMID;
    }

    /**
     * @param string $SZTICKETITEMID
     * @return \Axess\Dci4Wtp\D4WTPWTPDATA
     */
    public function setSZTICKETITEMID($SZTICKETITEMID)
    {
      $this->SZTICKETITEMID = $SZTICKETITEMID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPWTPDATA
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPWTPDATA
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
