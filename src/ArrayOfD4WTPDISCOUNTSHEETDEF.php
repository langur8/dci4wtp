<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPDISCOUNTSHEETDEF implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPDISCOUNTSHEETDEF[] $D4WTPDISCOUNTSHEETDEF
     */
    protected $D4WTPDISCOUNTSHEETDEF = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPDISCOUNTSHEETDEF[]
     */
    public function getD4WTPDISCOUNTSHEETDEF()
    {
      return $this->D4WTPDISCOUNTSHEETDEF;
    }

    /**
     * @param D4WTPDISCOUNTSHEETDEF[] $D4WTPDISCOUNTSHEETDEF
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPDISCOUNTSHEETDEF
     */
    public function setD4WTPDISCOUNTSHEETDEF(array $D4WTPDISCOUNTSHEETDEF = null)
    {
      $this->D4WTPDISCOUNTSHEETDEF = $D4WTPDISCOUNTSHEETDEF;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPDISCOUNTSHEETDEF[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPDISCOUNTSHEETDEF
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPDISCOUNTSHEETDEF[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPDISCOUNTSHEETDEF $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPDISCOUNTSHEETDEF[] = $value;
      } else {
        $this->D4WTPDISCOUNTSHEETDEF[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPDISCOUNTSHEETDEF[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPDISCOUNTSHEETDEF Return the current element
     */
    public function current()
    {
      return current($this->D4WTPDISCOUNTSHEETDEF);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPDISCOUNTSHEETDEF);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPDISCOUNTSHEETDEF);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPDISCOUNTSHEETDEF);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPDISCOUNTSHEETDEF Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPDISCOUNTSHEETDEF);
    }

}
