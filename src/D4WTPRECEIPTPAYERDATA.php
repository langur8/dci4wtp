<?php

namespace Axess\Dci4Wtp;

class D4WTPRECEIPTPAYERDATA
{

    /**
     * @var float $NPAYERNO
     */
    protected $NPAYERNO = null;

    /**
     * @var float $NPAYERPOSNO
     */
    protected $NPAYERPOSNO = null;

    /**
     * @var float $NPAYERPROJNO
     */
    protected $NPAYERPROJNO = null;

    /**
     * @var string $SZLNECERTIFICATENO
     */
    protected $SZLNECERTIFICATENO = null;

    /**
     * @var string $SZPAYERCITY
     */
    protected $SZPAYERCITY = null;

    /**
     * @var string $SZPAYERCOUNTRY
     */
    protected $SZPAYERCOUNTRY = null;

    /**
     * @var string $SZPAYERMASKNAME
     */
    protected $SZPAYERMASKNAME = null;

    /**
     * @var string $SZPAYERNAME
     */
    protected $SZPAYERNAME = null;

    /**
     * @var string $SZPAYERSTREET
     */
    protected $SZPAYERSTREET = null;

    /**
     * @var string $SZPAYERSTREETNO
     */
    protected $SZPAYERSTREETNO = null;

    /**
     * @var string $SZPAYERTELNO
     */
    protected $SZPAYERTELNO = null;

    /**
     * @var string $SZPAYERUID
     */
    protected $SZPAYERUID = null;

    /**
     * @var string $SZPAYERZIPCODE
     */
    protected $SZPAYERZIPCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPAYERNO()
    {
      return $this->NPAYERNO;
    }

    /**
     * @param float $NPAYERNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYERDATA
     */
    public function setNPAYERNO($NPAYERNO)
    {
      $this->NPAYERNO = $NPAYERNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERPOSNO()
    {
      return $this->NPAYERPOSNO;
    }

    /**
     * @param float $NPAYERPOSNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYERDATA
     */
    public function setNPAYERPOSNO($NPAYERPOSNO)
    {
      $this->NPAYERPOSNO = $NPAYERPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERPROJNO()
    {
      return $this->NPAYERPROJNO;
    }

    /**
     * @param float $NPAYERPROJNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYERDATA
     */
    public function setNPAYERPROJNO($NPAYERPROJNO)
    {
      $this->NPAYERPROJNO = $NPAYERPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLNECERTIFICATENO()
    {
      return $this->SZLNECERTIFICATENO;
    }

    /**
     * @param string $SZLNECERTIFICATENO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYERDATA
     */
    public function setSZLNECERTIFICATENO($SZLNECERTIFICATENO)
    {
      $this->SZLNECERTIFICATENO = $SZLNECERTIFICATENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPAYERCITY()
    {
      return $this->SZPAYERCITY;
    }

    /**
     * @param string $SZPAYERCITY
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYERDATA
     */
    public function setSZPAYERCITY($SZPAYERCITY)
    {
      $this->SZPAYERCITY = $SZPAYERCITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPAYERCOUNTRY()
    {
      return $this->SZPAYERCOUNTRY;
    }

    /**
     * @param string $SZPAYERCOUNTRY
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYERDATA
     */
    public function setSZPAYERCOUNTRY($SZPAYERCOUNTRY)
    {
      $this->SZPAYERCOUNTRY = $SZPAYERCOUNTRY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPAYERMASKNAME()
    {
      return $this->SZPAYERMASKNAME;
    }

    /**
     * @param string $SZPAYERMASKNAME
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYERDATA
     */
    public function setSZPAYERMASKNAME($SZPAYERMASKNAME)
    {
      $this->SZPAYERMASKNAME = $SZPAYERMASKNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPAYERNAME()
    {
      return $this->SZPAYERNAME;
    }

    /**
     * @param string $SZPAYERNAME
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYERDATA
     */
    public function setSZPAYERNAME($SZPAYERNAME)
    {
      $this->SZPAYERNAME = $SZPAYERNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPAYERSTREET()
    {
      return $this->SZPAYERSTREET;
    }

    /**
     * @param string $SZPAYERSTREET
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYERDATA
     */
    public function setSZPAYERSTREET($SZPAYERSTREET)
    {
      $this->SZPAYERSTREET = $SZPAYERSTREET;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPAYERSTREETNO()
    {
      return $this->SZPAYERSTREETNO;
    }

    /**
     * @param string $SZPAYERSTREETNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYERDATA
     */
    public function setSZPAYERSTREETNO($SZPAYERSTREETNO)
    {
      $this->SZPAYERSTREETNO = $SZPAYERSTREETNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPAYERTELNO()
    {
      return $this->SZPAYERTELNO;
    }

    /**
     * @param string $SZPAYERTELNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYERDATA
     */
    public function setSZPAYERTELNO($SZPAYERTELNO)
    {
      $this->SZPAYERTELNO = $SZPAYERTELNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPAYERUID()
    {
      return $this->SZPAYERUID;
    }

    /**
     * @param string $SZPAYERUID
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYERDATA
     */
    public function setSZPAYERUID($SZPAYERUID)
    {
      $this->SZPAYERUID = $SZPAYERUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPAYERZIPCODE()
    {
      return $this->SZPAYERZIPCODE;
    }

    /**
     * @param string $SZPAYERZIPCODE
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTPAYERDATA
     */
    public function setSZPAYERZIPCODE($SZPAYERZIPCODE)
    {
      $this->SZPAYERZIPCODE = $SZPAYERZIPCODE;
      return $this;
    }

}
