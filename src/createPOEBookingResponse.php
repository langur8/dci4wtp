<?php

namespace Axess\Dci4Wtp;

class createPOEBookingResponse
{

    /**
     * @var WTPCreatePOEBookingResult $createPOEBookingResult
     */
    protected $createPOEBookingResult = null;

    /**
     * @param WTPCreatePOEBookingResult $createPOEBookingResult
     */
    public function __construct($createPOEBookingResult)
    {
      $this->createPOEBookingResult = $createPOEBookingResult;
    }

    /**
     * @return WTPCreatePOEBookingResult
     */
    public function getCreatePOEBookingResult()
    {
      return $this->createPOEBookingResult;
    }

    /**
     * @param WTPCreatePOEBookingResult $createPOEBookingResult
     * @return \Axess\Dci4Wtp\createPOEBookingResponse
     */
    public function setCreatePOEBookingResult($createPOEBookingResult)
    {
      $this->createPOEBookingResult = $createPOEBookingResult;
      return $this;
    }

}
