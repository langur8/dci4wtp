<?php

namespace Axess\Dci4Wtp;

class getTariffList8Response
{

    /**
     * @var D4WTPTARIFFLIST8RESULT $getTariffList8Result
     */
    protected $getTariffList8Result = null;

    /**
     * @param D4WTPTARIFFLIST8RESULT $getTariffList8Result
     */
    public function __construct($getTariffList8Result)
    {
      $this->getTariffList8Result = $getTariffList8Result;
    }

    /**
     * @return D4WTPTARIFFLIST8RESULT
     */
    public function getGetTariffList8Result()
    {
      return $this->getTariffList8Result;
    }

    /**
     * @param D4WTPTARIFFLIST8RESULT $getTariffList8Result
     * @return \Axess\Dci4Wtp\getTariffList8Response
     */
    public function setGetTariffList8Result($getTariffList8Result)
    {
      $this->getTariffList8Result = $getTariffList8Result;
      return $this;
    }

}
