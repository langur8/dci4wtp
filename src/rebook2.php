<?php

namespace Axess\Dci4Wtp;

class rebook2
{

    /**
     * @var D4WTPREBOOKREQUEST2 $i_ctRebookReq
     */
    protected $i_ctRebookReq = null;

    /**
     * @param D4WTPREBOOKREQUEST2 $i_ctRebookReq
     */
    public function __construct($i_ctRebookReq)
    {
      $this->i_ctRebookReq = $i_ctRebookReq;
    }

    /**
     * @return D4WTPREBOOKREQUEST2
     */
    public function getI_ctRebookReq()
    {
      return $this->i_ctRebookReq;
    }

    /**
     * @param D4WTPREBOOKREQUEST2 $i_ctRebookReq
     * @return \Axess\Dci4Wtp\rebook2
     */
    public function setI_ctRebookReq($i_ctRebookReq)
    {
      $this->i_ctRebookReq = $i_ctRebookReq;
      return $this;
    }

}
