<?php

namespace Axess\Dci4Wtp;

class D4WTPPRODUCTORDER3
{

    /**
     * @var ArrayOfD4WTPADDARTICLE $ACTADDARTICLES
     */
    protected $ACTADDARTICLES = null;

    /**
     * @var ArrayOfD4WTPDCCONTENT $ACTDCCONTENT
     */
    protected $ACTDCCONTENT = null;

    /**
     * @var ArrayOfD4WTPERSONDATA $ACTPERSON
     */
    protected $ACTPERSON = null;

    /**
     * @var ArrayOfD4WTPWTPNO $ACTWTPNO
     */
    protected $ACTWTPNO = null;

    /**
     * @var float $BCREATEPHOTO
     */
    protected $BCREATEPHOTO = null;

    /**
     * @var float $BPERSONAL
     */
    protected $BPERSONAL = null;

    /**
     * @var float $BPREPAIDTICKET
     */
    protected $BPREPAIDTICKET = null;

    /**
     * @var D4WTPPACKAGEPOSPRODUCT $CTPACKAGEPRODUCT
     */
    protected $CTPACKAGEPRODUCT = null;

    /**
     * @var D4WTPPRODUCT $CTPRODUCT
     */
    protected $CTPRODUCT = null;

    /**
     * @var float $NAPPTRANSLOGID
     */
    protected $NAPPTRANSLOGID = null;

    /**
     * @var float $NDATACARRIERTYPENO
     */
    protected $NDATACARRIERTYPENO = null;

    /**
     * @var float $NDISCOUNTNR
     */
    protected $NDISCOUNTNR = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var string $SZCODINGMODE
     */
    protected $SZCODINGMODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDARTICLE
     */
    public function getACTADDARTICLES()
    {
      return $this->ACTADDARTICLES;
    }

    /**
     * @param ArrayOfD4WTPADDARTICLE $ACTADDARTICLES
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setACTADDARTICLES($ACTADDARTICLES)
    {
      $this->ACTADDARTICLES = $ACTADDARTICLES;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPDCCONTENT
     */
    public function getACTDCCONTENT()
    {
      return $this->ACTDCCONTENT;
    }

    /**
     * @param ArrayOfD4WTPDCCONTENT $ACTDCCONTENT
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setACTDCCONTENT($ACTDCCONTENT)
    {
      $this->ACTDCCONTENT = $ACTDCCONTENT;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPERSONDATA
     */
    public function getACTPERSON()
    {
      return $this->ACTPERSON;
    }

    /**
     * @param ArrayOfD4WTPERSONDATA $ACTPERSON
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setACTPERSON($ACTPERSON)
    {
      $this->ACTPERSON = $ACTPERSON;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPWTPNO
     */
    public function getACTWTPNO()
    {
      return $this->ACTWTPNO;
    }

    /**
     * @param ArrayOfD4WTPWTPNO $ACTWTPNO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setACTWTPNO($ACTWTPNO)
    {
      $this->ACTWTPNO = $ACTWTPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getBCREATEPHOTO()
    {
      return $this->BCREATEPHOTO;
    }

    /**
     * @param float $BCREATEPHOTO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setBCREATEPHOTO($BCREATEPHOTO)
    {
      $this->BCREATEPHOTO = $BCREATEPHOTO;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPERSONAL()
    {
      return $this->BPERSONAL;
    }

    /**
     * @param float $BPERSONAL
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setBPERSONAL($BPERSONAL)
    {
      $this->BPERSONAL = $BPERSONAL;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPREPAIDTICKET()
    {
      return $this->BPREPAIDTICKET;
    }

    /**
     * @param float $BPREPAIDTICKET
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setBPREPAIDTICKET($BPREPAIDTICKET)
    {
      $this->BPREPAIDTICKET = $BPREPAIDTICKET;
      return $this;
    }

    /**
     * @return D4WTPPACKAGEPOSPRODUCT
     */
    public function getCTPACKAGEPRODUCT()
    {
      return $this->CTPACKAGEPRODUCT;
    }

    /**
     * @param D4WTPPACKAGEPOSPRODUCT $CTPACKAGEPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setCTPACKAGEPRODUCT($CTPACKAGEPRODUCT)
    {
      $this->CTPACKAGEPRODUCT = $CTPACKAGEPRODUCT;
      return $this;
    }

    /**
     * @return D4WTPPRODUCT
     */
    public function getCTPRODUCT()
    {
      return $this->CTPRODUCT;
    }

    /**
     * @param D4WTPPRODUCT $CTPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setCTPRODUCT($CTPRODUCT)
    {
      $this->CTPRODUCT = $CTPRODUCT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAPPTRANSLOGID()
    {
      return $this->NAPPTRANSLOGID;
    }

    /**
     * @param float $NAPPTRANSLOGID
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setNAPPTRANSLOGID($NAPPTRANSLOGID)
    {
      $this->NAPPTRANSLOGID = $NAPPTRANSLOGID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATACARRIERTYPENO()
    {
      return $this->NDATACARRIERTYPENO;
    }

    /**
     * @param float $NDATACARRIERTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setNDATACARRIERTYPENO($NDATACARRIERTYPENO)
    {
      $this->NDATACARRIERTYPENO = $NDATACARRIERTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDISCOUNTNR()
    {
      return $this->NDISCOUNTNR;
    }

    /**
     * @param float $NDISCOUNTNR
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setNDISCOUNTNR($NDISCOUNTNR)
    {
      $this->NDISCOUNTNR = $NDISCOUNTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGMODE()
    {
      return $this->SZCODINGMODE;
    }

    /**
     * @param string $SZCODINGMODE
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTORDER3
     */
    public function setSZCODINGMODE($SZCODINGMODE)
    {
      $this->SZCODINGMODE = $SZCODINGMODE;
      return $this;
    }

}
