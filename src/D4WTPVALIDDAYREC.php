<?php

namespace Axess\Dci4Wtp;

class D4WTPVALIDDAYREC
{

    /**
     * @var float $NKONTINGENTNO
     */
    protected $NKONTINGENTNO = null;

    /**
     * @var float $NPERSONTYPNO
     */
    protected $NPERSONTYPNO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSUBKONTINGENTNO
     */
    protected $NSUBKONTINGENTNO = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NTICKETSAVAILABLE
     */
    protected $NTICKETSAVAILABLE = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var string $SZFIRSTVALIDDATE
     */
    protected $SZFIRSTVALIDDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNKONTINGENTNO()
    {
      return $this->NKONTINGENTNO;
    }

    /**
     * @param float $NKONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPVALIDDAYREC
     */
    public function setNKONTINGENTNO($NKONTINGENTNO)
    {
      $this->NKONTINGENTNO = $NKONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSONTYPNO()
    {
      return $this->NPERSONTYPNO;
    }

    /**
     * @param float $NPERSONTYPNO
     * @return \Axess\Dci4Wtp\D4WTPVALIDDAYREC
     */
    public function setNPERSONTYPNO($NPERSONTYPNO)
    {
      $this->NPERSONTYPNO = $NPERSONTYPNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPVALIDDAYREC
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPVALIDDAYREC
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBKONTINGENTNO()
    {
      return $this->NSUBKONTINGENTNO;
    }

    /**
     * @param float $NSUBKONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPVALIDDAYREC
     */
    public function setNSUBKONTINGENTNO($NSUBKONTINGENTNO)
    {
      $this->NSUBKONTINGENTNO = $NSUBKONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPVALIDDAYREC
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETSAVAILABLE()
    {
      return $this->NTICKETSAVAILABLE;
    }

    /**
     * @param float $NTICKETSAVAILABLE
     * @return \Axess\Dci4Wtp\D4WTPVALIDDAYREC
     */
    public function setNTICKETSAVAILABLE($NTICKETSAVAILABLE)
    {
      $this->NTICKETSAVAILABLE = $NTICKETSAVAILABLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPVALIDDAYREC
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTVALIDDATE()
    {
      return $this->SZFIRSTVALIDDATE;
    }

    /**
     * @param string $SZFIRSTVALIDDATE
     * @return \Axess\Dci4Wtp\D4WTPVALIDDAYREC
     */
    public function setSZFIRSTVALIDDATE($SZFIRSTVALIDDATE)
    {
      $this->SZFIRSTVALIDDATE = $SZFIRSTVALIDDATE;
      return $this;
    }

}
