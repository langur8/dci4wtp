<?php

namespace Axess\Dci4Wtp;

class getReaderTransResponse
{

    /**
     * @var D4WTPGETREADERTRANSRESULT $getReaderTransResult
     */
    protected $getReaderTransResult = null;

    /**
     * @param D4WTPGETREADERTRANSRESULT $getReaderTransResult
     */
    public function __construct($getReaderTransResult)
    {
      $this->getReaderTransResult = $getReaderTransResult;
    }

    /**
     * @return D4WTPGETREADERTRANSRESULT
     */
    public function getGetReaderTransResult()
    {
      return $this->getReaderTransResult;
    }

    /**
     * @param D4WTPGETREADERTRANSRESULT $getReaderTransResult
     * @return \Axess\Dci4Wtp\getReaderTransResponse
     */
    public function setGetReaderTransResult($getReaderTransResult)
    {
      $this->getReaderTransResult = $getReaderTransResult;
      return $this;
    }

}
