<?php

namespace Axess\Dci4Wtp;

class D4WTPMANAGESHOPCARTRESULT
{

    /**
     * @var ArrayOfD4WTPSHOPPINGCARTPOSRES $ACTSHOPPINGCARTPOSRES
     */
    protected $ACTSHOPPINGCARTPOSRES = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSHOPPINGCARTPOSRES
     */
    public function getACTSHOPPINGCARTPOSRES()
    {
      return $this->ACTSHOPPINGCARTPOSRES;
    }

    /**
     * @param ArrayOfD4WTPSHOPPINGCARTPOSRES $ACTSHOPPINGCARTPOSRES
     * @return \Axess\Dci4Wtp\D4WTPMANAGESHOPCARTRESULT
     */
    public function setACTSHOPPINGCARTPOSRES($ACTSHOPPINGCARTPOSRES)
    {
      $this->ACTSHOPPINGCARTPOSRES = $ACTSHOPPINGCARTPOSRES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPMANAGESHOPCARTRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPMANAGESHOPCARTRESULT
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPMANAGESHOPCARTRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
