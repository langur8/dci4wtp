<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGELIST3
{

    /**
     * @var ArrayOfD4WTPPKGTARIFFLISTDAY6 $ACTPROGRAMPKGTARIFFLISTDAY
     */
    protected $ACTPROGRAMPKGTARIFFLISTDAY = null;

    /**
     * @var float $BQUANTITYFIX
     */
    protected $BQUANTITYFIX = null;

    /**
     * @var float $BTARIFFSHEETPRICE
     */
    protected $BTARIFFSHEETPRICE = null;

    /**
     * @var float $NMAXQUANTITY
     */
    protected $NMAXQUANTITY = null;

    /**
     * @var float $NMINQUANTITY
     */
    protected $NMINQUANTITY = null;

    /**
     * @var float $NPACKNO
     */
    protected $NPACKNO = null;

    /**
     * @var float $NPACKPOS
     */
    protected $NPACKPOS = null;

    /**
     * @var float $NPACKTARIFF
     */
    protected $NPACKTARIFF = null;

    /**
     * @var float $NPACKTARIFFSHEETNO
     */
    protected $NPACKTARIFFSHEETNO = null;

    /**
     * @var float $NPROGRAMGROUPNR
     */
    protected $NPROGRAMGROUPNR = null;

    /**
     * @var float $NPROGRAMNR
     */
    protected $NPROGRAMNR = null;

    /**
     * @var float $NPROGRAMPROJNR
     */
    protected $NPROGRAMPROJNR = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NSORTNO
     */
    protected $NSORTNO = null;

    /**
     * @var string $SZPROGRAMNAME
     */
    protected $SZPROGRAMNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPKGTARIFFLISTDAY6
     */
    public function getACTPROGRAMPKGTARIFFLISTDAY()
    {
      return $this->ACTPROGRAMPKGTARIFFLISTDAY;
    }

    /**
     * @param ArrayOfD4WTPPKGTARIFFLISTDAY6 $ACTPROGRAMPKGTARIFFLISTDAY
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setACTPROGRAMPKGTARIFFLISTDAY($ACTPROGRAMPKGTARIFFLISTDAY)
    {
      $this->ACTPROGRAMPKGTARIFFLISTDAY = $ACTPROGRAMPKGTARIFFLISTDAY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBQUANTITYFIX()
    {
      return $this->BQUANTITYFIX;
    }

    /**
     * @param float $BQUANTITYFIX
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setBQUANTITYFIX($BQUANTITYFIX)
    {
      $this->BQUANTITYFIX = $BQUANTITYFIX;
      return $this;
    }

    /**
     * @return float
     */
    public function getBTARIFFSHEETPRICE()
    {
      return $this->BTARIFFSHEETPRICE;
    }

    /**
     * @param float $BTARIFFSHEETPRICE
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setBTARIFFSHEETPRICE($BTARIFFSHEETPRICE)
    {
      $this->BTARIFFSHEETPRICE = $BTARIFFSHEETPRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMAXQUANTITY()
    {
      return $this->NMAXQUANTITY;
    }

    /**
     * @param float $NMAXQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setNMAXQUANTITY($NMAXQUANTITY)
    {
      $this->NMAXQUANTITY = $NMAXQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMINQUANTITY()
    {
      return $this->NMINQUANTITY;
    }

    /**
     * @param float $NMINQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setNMINQUANTITY($NMINQUANTITY)
    {
      $this->NMINQUANTITY = $NMINQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKNO()
    {
      return $this->NPACKNO;
    }

    /**
     * @param float $NPACKNO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setNPACKNO($NPACKNO)
    {
      $this->NPACKNO = $NPACKNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKPOS()
    {
      return $this->NPACKPOS;
    }

    /**
     * @param float $NPACKPOS
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setNPACKPOS($NPACKPOS)
    {
      $this->NPACKPOS = $NPACKPOS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKTARIFF()
    {
      return $this->NPACKTARIFF;
    }

    /**
     * @param float $NPACKTARIFF
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setNPACKTARIFF($NPACKTARIFF)
    {
      $this->NPACKTARIFF = $NPACKTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKTARIFFSHEETNO()
    {
      return $this->NPACKTARIFFSHEETNO;
    }

    /**
     * @param float $NPACKTARIFFSHEETNO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setNPACKTARIFFSHEETNO($NPACKTARIFFSHEETNO)
    {
      $this->NPACKTARIFFSHEETNO = $NPACKTARIFFSHEETNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROGRAMGROUPNR()
    {
      return $this->NPROGRAMGROUPNR;
    }

    /**
     * @param float $NPROGRAMGROUPNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setNPROGRAMGROUPNR($NPROGRAMGROUPNR)
    {
      $this->NPROGRAMGROUPNR = $NPROGRAMGROUPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROGRAMNR()
    {
      return $this->NPROGRAMNR;
    }

    /**
     * @param float $NPROGRAMNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setNPROGRAMNR($NPROGRAMNR)
    {
      $this->NPROGRAMNR = $NPROGRAMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROGRAMPROJNR()
    {
      return $this->NPROGRAMPROJNR;
    }

    /**
     * @param float $NPROGRAMPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setNPROGRAMPROJNR($NPROGRAMPROJNR)
    {
      $this->NPROGRAMPROJNR = $NPROGRAMPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNO()
    {
      return $this->NSORTNO;
    }

    /**
     * @param float $NSORTNO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setNSORTNO($NSORTNO)
    {
      $this->NSORTNO = $NSORTNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPROGRAMNAME()
    {
      return $this->SZPROGRAMNAME;
    }

    /**
     * @param string $SZPROGRAMNAME
     * @return \Axess\Dci4Wtp\D4WTPPACKAGELIST3
     */
    public function setSZPROGRAMNAME($SZPROGRAMNAME)
    {
      $this->SZPROGRAMNAME = $SZPROGRAMNAME;
      return $this;
    }

}
