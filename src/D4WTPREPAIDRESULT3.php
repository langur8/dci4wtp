<?php

namespace Axess\Dci4Wtp;

class D4WTPREPAIDRESULT3
{

    /**
     * @var ArrayOfD4WTPPREPAIDTICKET3 $ACTPREPAIDTICKET3
     */
    protected $ACTPREPAIDTICKET3 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPREPAIDTICKET3
     */
    public function getACTPREPAIDTICKET3()
    {
      return $this->ACTPREPAIDTICKET3;
    }

    /**
     * @param ArrayOfD4WTPPREPAIDTICKET3 $ACTPREPAIDTICKET3
     * @return \Axess\Dci4Wtp\D4WTPREPAIDRESULT3
     */
    public function setACTPREPAIDTICKET3($ACTPREPAIDTICKET3)
    {
      $this->ACTPREPAIDTICKET3 = $ACTPREPAIDTICKET3;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPREPAIDRESULT3
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPREPAIDRESULT3
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
