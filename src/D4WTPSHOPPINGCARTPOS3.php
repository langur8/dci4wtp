<?php

namespace Axess\Dci4Wtp;

class D4WTPSHOPPINGCARTPOS3
{

    /**
     * @var ArrayOfSHOPCBOOKTIMESLOT $ACTBOOKTIMESLOT
     */
    protected $ACTBOOKTIMESLOT = null;

    /**
     * @var ArrayOfD4WTPSHOPCSUBCONTINGENT $ACTSHOPCSUBCONTINGENT
     */
    protected $ACTSHOPCSUBCONTINGENT = null;

    /**
     * @var ArrayOfD4WTPSHOPPINGCARTPOSDETAIL2 $ACTSHOPPINGCARTPOSDETAIL
     */
    protected $ACTSHOPPINGCARTPOSDETAIL = null;

    /**
     * @var float $BARTICLESONLY
     */
    protected $BARTICLESONLY = null;

    /**
     * @var float $BDETAILSONLY
     */
    protected $BDETAILSONLY = null;

    /**
     * @var float $FDISCOUNTEDPRICE
     */
    protected $FDISCOUNTEDPRICE = null;

    /**
     * @var float $FUNITPRICE
     */
    protected $FUNITPRICE = null;

    /**
     * @var float $FWAREAMOUNT
     */
    protected $FWAREAMOUNT = null;

    /**
     * @var float $FWARETARIFF
     */
    protected $FWARETARIFF = null;

    /**
     * @var float $NALLDISCOUNTNO
     */
    protected $NALLDISCOUNTNO = null;

    /**
     * @var float $NALLDISCOUNTSTAFNO
     */
    protected $NALLDISCOUNTSTAFNO = null;

    /**
     * @var float $NARCNO
     */
    protected $NARCNO = null;

    /**
     * @var float $NARCPROJNO
     */
    protected $NARCPROJNO = null;

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NCURRSEASONNO
     */
    protected $NCURRSEASONNO = null;

    /**
     * @var float $NCUSTCARDTYPENO
     */
    protected $NCUSTCARDTYPENO = null;

    /**
     * @var float $NDATACARRIERTYPENO
     */
    protected $NDATACARRIERTYPENO = null;

    /**
     * @var float $NITEMLOCATIONNO
     */
    protected $NITEMLOCATIONNO = null;

    /**
     * @var float $NPACKAGENO
     */
    protected $NPACKAGENO = null;

    /**
     * @var float $NPACKPOSITION
     */
    protected $NPACKPOSITION = null;

    /**
     * @var float $NPERSTYPENO
     */
    protected $NPERSTYPENO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPOSITIONNO
     */
    protected $NPOSITIONNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NQUANTITY
     */
    protected $NQUANTITY = null;

    /**
     * @var float $NREFPOSITIONNO
     */
    protected $NREFPOSITIONNO = null;

    /**
     * @var float $NRENTALITEMNO
     */
    protected $NRENTALITEMNO = null;

    /**
     * @var float $NRENTALPERSTYPENO
     */
    protected $NRENTALPERSTYPENO = null;

    /**
     * @var float $NROHLINGSTYPENO
     */
    protected $NROHLINGSTYPENO = null;

    /**
     * @var float $NTARIFSHEETNO
     */
    protected $NTARIFSHEETNO = null;

    /**
     * @var float $NTARIFSHEETVALIDNO
     */
    protected $NTARIFSHEETVALIDNO = null;

    /**
     * @var float $NTIMESCALENO
     */
    protected $NTIMESCALENO = null;

    /**
     * @var float $NWARESALEITEMNO
     */
    protected $NWARESALEITEMNO = null;

    /**
     * @var float $NWARESALEQUANTITY
     */
    protected $NWARESALEQUANTITY = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZEXTMATERIALNO
     */
    protected $SZEXTMATERIALNO = null;

    /**
     * @var string $SZSHOPPINGCARTPOSCODE
     */
    protected $SZSHOPPINGCARTPOSCODE = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfSHOPCBOOKTIMESLOT
     */
    public function getACTBOOKTIMESLOT()
    {
      return $this->ACTBOOKTIMESLOT;
    }

    /**
     * @param ArrayOfSHOPCBOOKTIMESLOT $ACTBOOKTIMESLOT
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setACTBOOKTIMESLOT($ACTBOOKTIMESLOT)
    {
      $this->ACTBOOKTIMESLOT = $ACTBOOKTIMESLOT;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPSHOPCSUBCONTINGENT
     */
    public function getACTSHOPCSUBCONTINGENT()
    {
      return $this->ACTSHOPCSUBCONTINGENT;
    }

    /**
     * @param ArrayOfD4WTPSHOPCSUBCONTINGENT $ACTSHOPCSUBCONTINGENT
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setACTSHOPCSUBCONTINGENT($ACTSHOPCSUBCONTINGENT)
    {
      $this->ACTSHOPCSUBCONTINGENT = $ACTSHOPCSUBCONTINGENT;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPSHOPPINGCARTPOSDETAIL2
     */
    public function getACTSHOPPINGCARTPOSDETAIL()
    {
      return $this->ACTSHOPPINGCARTPOSDETAIL;
    }

    /**
     * @param ArrayOfD4WTPSHOPPINGCARTPOSDETAIL2 $ACTSHOPPINGCARTPOSDETAIL
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setACTSHOPPINGCARTPOSDETAIL($ACTSHOPPINGCARTPOSDETAIL)
    {
      $this->ACTSHOPPINGCARTPOSDETAIL = $ACTSHOPPINGCARTPOSDETAIL;
      return $this;
    }

    /**
     * @return float
     */
    public function getBARTICLESONLY()
    {
      return $this->BARTICLESONLY;
    }

    /**
     * @param float $BARTICLESONLY
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setBARTICLESONLY($BARTICLESONLY)
    {
      $this->BARTICLESONLY = $BARTICLESONLY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBDETAILSONLY()
    {
      return $this->BDETAILSONLY;
    }

    /**
     * @param float $BDETAILSONLY
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setBDETAILSONLY($BDETAILSONLY)
    {
      $this->BDETAILSONLY = $BDETAILSONLY;
      return $this;
    }

    /**
     * @return float
     */
    public function getFDISCOUNTEDPRICE()
    {
      return $this->FDISCOUNTEDPRICE;
    }

    /**
     * @param float $FDISCOUNTEDPRICE
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setFDISCOUNTEDPRICE($FDISCOUNTEDPRICE)
    {
      $this->FDISCOUNTEDPRICE = $FDISCOUNTEDPRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getFUNITPRICE()
    {
      return $this->FUNITPRICE;
    }

    /**
     * @param float $FUNITPRICE
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setFUNITPRICE($FUNITPRICE)
    {
      $this->FUNITPRICE = $FUNITPRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getFWAREAMOUNT()
    {
      return $this->FWAREAMOUNT;
    }

    /**
     * @param float $FWAREAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setFWAREAMOUNT($FWAREAMOUNT)
    {
      $this->FWAREAMOUNT = $FWAREAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFWARETARIFF()
    {
      return $this->FWARETARIFF;
    }

    /**
     * @param float $FWARETARIFF
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setFWARETARIFF($FWARETARIFF)
    {
      $this->FWARETARIFF = $FWARETARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNALLDISCOUNTNO()
    {
      return $this->NALLDISCOUNTNO;
    }

    /**
     * @param float $NALLDISCOUNTNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNALLDISCOUNTNO($NALLDISCOUNTNO)
    {
      $this->NALLDISCOUNTNO = $NALLDISCOUNTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNALLDISCOUNTSTAFNO()
    {
      return $this->NALLDISCOUNTSTAFNO;
    }

    /**
     * @param float $NALLDISCOUNTSTAFNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNALLDISCOUNTSTAFNO($NALLDISCOUNTSTAFNO)
    {
      $this->NALLDISCOUNTSTAFNO = $NALLDISCOUNTSTAFNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARCNO()
    {
      return $this->NARCNO;
    }

    /**
     * @param float $NARCNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNARCNO($NARCNO)
    {
      $this->NARCNO = $NARCNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARCPROJNO()
    {
      return $this->NARCPROJNO;
    }

    /**
     * @param float $NARCPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNARCPROJNO($NARCPROJNO)
    {
      $this->NARCPROJNO = $NARCPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCURRSEASONNO()
    {
      return $this->NCURRSEASONNO;
    }

    /**
     * @param float $NCURRSEASONNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNCURRSEASONNO($NCURRSEASONNO)
    {
      $this->NCURRSEASONNO = $NCURRSEASONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTCARDTYPENO()
    {
      return $this->NCUSTCARDTYPENO;
    }

    /**
     * @param float $NCUSTCARDTYPENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNCUSTCARDTYPENO($NCUSTCARDTYPENO)
    {
      $this->NCUSTCARDTYPENO = $NCUSTCARDTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATACARRIERTYPENO()
    {
      return $this->NDATACARRIERTYPENO;
    }

    /**
     * @param float $NDATACARRIERTYPENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNDATACARRIERTYPENO($NDATACARRIERTYPENO)
    {
      $this->NDATACARRIERTYPENO = $NDATACARRIERTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNITEMLOCATIONNO()
    {
      return $this->NITEMLOCATIONNO;
    }

    /**
     * @param float $NITEMLOCATIONNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNITEMLOCATIONNO($NITEMLOCATIONNO)
    {
      $this->NITEMLOCATIONNO = $NITEMLOCATIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKAGENO()
    {
      return $this->NPACKAGENO;
    }

    /**
     * @param float $NPACKAGENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNPACKAGENO($NPACKAGENO)
    {
      $this->NPACKAGENO = $NPACKAGENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKPOSITION()
    {
      return $this->NPACKPOSITION;
    }

    /**
     * @param float $NPACKPOSITION
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNPACKPOSITION($NPACKPOSITION)
    {
      $this->NPACKPOSITION = $NPACKPOSITION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSTYPENO()
    {
      return $this->NPERSTYPENO;
    }

    /**
     * @param float $NPERSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNPERSTYPENO($NPERSTYPENO)
    {
      $this->NPERSTYPENO = $NPERSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSITIONNO()
    {
      return $this->NPOSITIONNO;
    }

    /**
     * @param float $NPOSITIONNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNPOSITIONNO($NPOSITIONNO)
    {
      $this->NPOSITIONNO = $NPOSITIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNQUANTITY()
    {
      return $this->NQUANTITY;
    }

    /**
     * @param float $NQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNQUANTITY($NQUANTITY)
    {
      $this->NQUANTITY = $NQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNREFPOSITIONNO()
    {
      return $this->NREFPOSITIONNO;
    }

    /**
     * @param float $NREFPOSITIONNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNREFPOSITIONNO($NREFPOSITIONNO)
    {
      $this->NREFPOSITIONNO = $NREFPOSITIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNO()
    {
      return $this->NRENTALITEMNO;
    }

    /**
     * @param float $NRENTALITEMNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNRENTALITEMNO($NRENTALITEMNO)
    {
      $this->NRENTALITEMNO = $NRENTALITEMNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPERSTYPENO()
    {
      return $this->NRENTALPERSTYPENO;
    }

    /**
     * @param float $NRENTALPERSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNRENTALPERSTYPENO($NRENTALPERSTYPENO)
    {
      $this->NRENTALPERSTYPENO = $NRENTALPERSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNROHLINGSTYPENO()
    {
      return $this->NROHLINGSTYPENO;
    }

    /**
     * @param float $NROHLINGSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNROHLINGSTYPENO($NROHLINGSTYPENO)
    {
      $this->NROHLINGSTYPENO = $NROHLINGSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFSHEETNO()
    {
      return $this->NTARIFSHEETNO;
    }

    /**
     * @param float $NTARIFSHEETNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNTARIFSHEETNO($NTARIFSHEETNO)
    {
      $this->NTARIFSHEETNO = $NTARIFSHEETNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFSHEETVALIDNO()
    {
      return $this->NTARIFSHEETVALIDNO;
    }

    /**
     * @param float $NTARIFSHEETVALIDNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNTARIFSHEETVALIDNO($NTARIFSHEETVALIDNO)
    {
      $this->NTARIFSHEETVALIDNO = $NTARIFSHEETVALIDNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTIMESCALENO()
    {
      return $this->NTIMESCALENO;
    }

    /**
     * @param float $NTIMESCALENO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNTIMESCALENO($NTIMESCALENO)
    {
      $this->NTIMESCALENO = $NTIMESCALENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARESALEITEMNO()
    {
      return $this->NWARESALEITEMNO;
    }

    /**
     * @param float $NWARESALEITEMNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNWARESALEITEMNO($NWARESALEITEMNO)
    {
      $this->NWARESALEITEMNO = $NWARESALEITEMNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARESALEQUANTITY()
    {
      return $this->NWARESALEQUANTITY;
    }

    /**
     * @param float $NWARESALEQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setNWARESALEQUANTITY($NWARESALEQUANTITY)
    {
      $this->NWARESALEQUANTITY = $NWARESALEQUANTITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTMATERIALNO()
    {
      return $this->SZEXTMATERIALNO;
    }

    /**
     * @param string $SZEXTMATERIALNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setSZEXTMATERIALNO($SZEXTMATERIALNO)
    {
      $this->SZEXTMATERIALNO = $SZEXTMATERIALNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHOPPINGCARTPOSCODE()
    {
      return $this->SZSHOPPINGCARTPOSCODE;
    }

    /**
     * @param string $SZSHOPPINGCARTPOSCODE
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setSZSHOPPINGCARTPOSCODE($SZSHOPPINGCARTPOSCODE)
    {
      $this->SZSHOPPINGCARTPOSCODE = $SZSHOPPINGCARTPOSCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOS3
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
