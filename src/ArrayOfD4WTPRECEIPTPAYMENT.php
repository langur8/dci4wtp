<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPRECEIPTPAYMENT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPRECEIPTPAYMENT[] $D4WTPRECEIPTPAYMENT
     */
    protected $D4WTPRECEIPTPAYMENT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPRECEIPTPAYMENT[]
     */
    public function getD4WTPRECEIPTPAYMENT()
    {
      return $this->D4WTPRECEIPTPAYMENT;
    }

    /**
     * @param D4WTPRECEIPTPAYMENT[] $D4WTPRECEIPTPAYMENT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPRECEIPTPAYMENT
     */
    public function setD4WTPRECEIPTPAYMENT(array $D4WTPRECEIPTPAYMENT = null)
    {
      $this->D4WTPRECEIPTPAYMENT = $D4WTPRECEIPTPAYMENT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPRECEIPTPAYMENT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPRECEIPTPAYMENT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPRECEIPTPAYMENT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPRECEIPTPAYMENT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPRECEIPTPAYMENT[] = $value;
      } else {
        $this->D4WTPRECEIPTPAYMENT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPRECEIPTPAYMENT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPRECEIPTPAYMENT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPRECEIPTPAYMENT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPRECEIPTPAYMENT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPRECEIPTPAYMENT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPRECEIPTPAYMENT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPRECEIPTPAYMENT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPRECEIPTPAYMENT);
    }

}
