<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPEMPLOYEEASSIGNED implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPEMPLOYEEASSIGNED[] $D4WTPEMPLOYEEASSIGNED
     */
    protected $D4WTPEMPLOYEEASSIGNED = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPEMPLOYEEASSIGNED[]
     */
    public function getD4WTPEMPLOYEEASSIGNED()
    {
      return $this->D4WTPEMPLOYEEASSIGNED;
    }

    /**
     * @param D4WTPEMPLOYEEASSIGNED[] $D4WTPEMPLOYEEASSIGNED
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPEMPLOYEEASSIGNED
     */
    public function setD4WTPEMPLOYEEASSIGNED(array $D4WTPEMPLOYEEASSIGNED = null)
    {
      $this->D4WTPEMPLOYEEASSIGNED = $D4WTPEMPLOYEEASSIGNED;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPEMPLOYEEASSIGNED[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPEMPLOYEEASSIGNED
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPEMPLOYEEASSIGNED[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPEMPLOYEEASSIGNED $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPEMPLOYEEASSIGNED[] = $value;
      } else {
        $this->D4WTPEMPLOYEEASSIGNED[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPEMPLOYEEASSIGNED[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPEMPLOYEEASSIGNED Return the current element
     */
    public function current()
    {
      return current($this->D4WTPEMPLOYEEASSIGNED);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPEMPLOYEEASSIGNED);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPEMPLOYEEASSIGNED);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPEMPLOYEEASSIGNED);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPEMPLOYEEASSIGNED Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPEMPLOYEEASSIGNED);
    }

}
