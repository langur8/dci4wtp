<?php

namespace Axess\Dci4Wtp;

class loginResponse
{

    /**
     * @var D4WTPLOGINRESULT $loginResult
     */
    protected $loginResult = null;

    /**
     * @param D4WTPLOGINRESULT $loginResult
     */
    public function __construct($loginResult)
    {
      $this->loginResult = $loginResult;
    }

    /**
     * @return D4WTPLOGINRESULT
     */
    public function getLoginResult()
    {
      return $this->loginResult;
    }

    /**
     * @param D4WTPLOGINRESULT $loginResult
     * @return \Axess\Dci4Wtp\loginResponse
     */
    public function setLoginResult($loginResult)
    {
      $this->loginResult = $loginResult;
      return $this;
    }

}
