<?php

namespace Axess\Dci4Wtp;

class D4WTPGETTAXRES
{

    /**
     * @var ArrayOfTAXARTICLES $ACTARTICLES
     */
    protected $ACTARTICLES = null;

    /**
     * @var ArrayOfMWSTSATZ $ACTMWSTSATZLIST
     */
    protected $ACTMWSTSATZLIST = null;

    /**
     * @var ArrayOfRENTALITEMMWST $ACTRENTALITEMMWSTLIST
     */
    protected $ACTRENTALITEMMWSTLIST = null;

    /**
     * @var ArrayOfTAXARTICLES $ACTTAXARTICLELIST
     */
    protected $ACTTAXARTICLELIST = null;

    /**
     * @var ArrayOfTICKETTYPEMWST $ACTTICKETTYPEMWSTLIST
     */
    protected $ACTTICKETTYPEMWSTLIST = null;

    /**
     * @var float $BNETPRICESYSTEM
     */
    protected $BNETPRICESYSTEM = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfTAXARTICLES
     */
    public function getACTARTICLES()
    {
      return $this->ACTARTICLES;
    }

    /**
     * @param ArrayOfTAXARTICLES $ACTARTICLES
     * @return \Axess\Dci4Wtp\D4WTPGETTAXRES
     */
    public function setACTARTICLES($ACTARTICLES)
    {
      $this->ACTARTICLES = $ACTARTICLES;
      return $this;
    }

    /**
     * @return ArrayOfMWSTSATZ
     */
    public function getACTMWSTSATZLIST()
    {
      return $this->ACTMWSTSATZLIST;
    }

    /**
     * @param ArrayOfMWSTSATZ $ACTMWSTSATZLIST
     * @return \Axess\Dci4Wtp\D4WTPGETTAXRES
     */
    public function setACTMWSTSATZLIST($ACTMWSTSATZLIST)
    {
      $this->ACTMWSTSATZLIST = $ACTMWSTSATZLIST;
      return $this;
    }

    /**
     * @return ArrayOfRENTALITEMMWST
     */
    public function getACTRENTALITEMMWSTLIST()
    {
      return $this->ACTRENTALITEMMWSTLIST;
    }

    /**
     * @param ArrayOfRENTALITEMMWST $ACTRENTALITEMMWSTLIST
     * @return \Axess\Dci4Wtp\D4WTPGETTAXRES
     */
    public function setACTRENTALITEMMWSTLIST($ACTRENTALITEMMWSTLIST)
    {
      $this->ACTRENTALITEMMWSTLIST = $ACTRENTALITEMMWSTLIST;
      return $this;
    }

    /**
     * @return ArrayOfTAXARTICLES
     */
    public function getACTTAXARTICLELIST()
    {
      return $this->ACTTAXARTICLELIST;
    }

    /**
     * @param ArrayOfTAXARTICLES $ACTTAXARTICLELIST
     * @return \Axess\Dci4Wtp\D4WTPGETTAXRES
     */
    public function setACTTAXARTICLELIST($ACTTAXARTICLELIST)
    {
      $this->ACTTAXARTICLELIST = $ACTTAXARTICLELIST;
      return $this;
    }

    /**
     * @return ArrayOfTICKETTYPEMWST
     */
    public function getACTTICKETTYPEMWSTLIST()
    {
      return $this->ACTTICKETTYPEMWSTLIST;
    }

    /**
     * @param ArrayOfTICKETTYPEMWST $ACTTICKETTYPEMWSTLIST
     * @return \Axess\Dci4Wtp\D4WTPGETTAXRES
     */
    public function setACTTICKETTYPEMWSTLIST($ACTTICKETTYPEMWSTLIST)
    {
      $this->ACTTICKETTYPEMWSTLIST = $ACTTICKETTYPEMWSTLIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getBNETPRICESYSTEM()
    {
      return $this->BNETPRICESYSTEM;
    }

    /**
     * @param float $BNETPRICESYSTEM
     * @return \Axess\Dci4Wtp\D4WTPGETTAXRES
     */
    public function setBNETPRICESYSTEM($BNETPRICESYSTEM)
    {
      $this->BNETPRICESYSTEM = $BNETPRICESYSTEM;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETTAXRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETTAXRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
