<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCOMPANYREQUEST
{

    /**
     * @var float $BCOMPANYACTIVE
     */
    protected $BCOMPANYACTIVE = null;

    /**
     * @var float $NCOMPANYNR
     */
    protected $NCOMPANYNR = null;

    /**
     * @var float $NCOMPANYPOSNR
     */
    protected $NCOMPANYPOSNR = null;

    /**
     * @var float $NCOMPANYPROJNR
     */
    protected $NCOMPANYPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZCOMPANYNAME
     */
    protected $SZCOMPANYNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBCOMPANYACTIVE()
    {
      return $this->BCOMPANYACTIVE;
    }

    /**
     * @param float $BCOMPANYACTIVE
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANYREQUEST
     */
    public function setBCOMPANYACTIVE($BCOMPANYACTIVE)
    {
      $this->BCOMPANYACTIVE = $BCOMPANYACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNR()
    {
      return $this->NCOMPANYNR;
    }

    /**
     * @param float $NCOMPANYNR
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANYREQUEST
     */
    public function setNCOMPANYNR($NCOMPANYNR)
    {
      $this->NCOMPANYNR = $NCOMPANYNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNR()
    {
      return $this->NCOMPANYPOSNR;
    }

    /**
     * @param float $NCOMPANYPOSNR
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANYREQUEST
     */
    public function setNCOMPANYPOSNR($NCOMPANYPOSNR)
    {
      $this->NCOMPANYPOSNR = $NCOMPANYPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNR()
    {
      return $this->NCOMPANYPROJNR;
    }

    /**
     * @param float $NCOMPANYPROJNR
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANYREQUEST
     */
    public function setNCOMPANYPROJNR($NCOMPANYPROJNR)
    {
      $this->NCOMPANYPROJNR = $NCOMPANYPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANYREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPANYNAME()
    {
      return $this->SZCOMPANYNAME;
    }

    /**
     * @param string $SZCOMPANYNAME
     * @return \Axess\Dci4Wtp\D4WTPGETCOMPANYREQUEST
     */
    public function setSZCOMPANYNAME($SZCOMPANYNAME)
    {
      $this->SZCOMPANYNAME = $SZCOMPANYNAME;
      return $this;
    }

}
