<?php

namespace Axess\Dci4Wtp;

class D4WTPGETREADERTRANS2
{

    /**
     * @var float $BADDCHARGE
     */
    protected $BADDCHARGE = null;

    /**
     * @var float $NCARDBASETYPENO
     */
    protected $NCARDBASETYPENO = null;

    /**
     * @var float $NCPUNO
     */
    protected $NCPUNO = null;

    /**
     * @var float $NLANENO
     */
    protected $NLANENO = null;

    /**
     * @var float $NLFDCPUTRANSNO
     */
    protected $NLFDCPUTRANSNO = null;

    /**
     * @var float $NPOENO
     */
    protected $NPOENO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NRESTDAYSNEW
     */
    protected $NRESTDAYSNEW = null;

    /**
     * @var float $NRESTDAYSOLD
     */
    protected $NRESTDAYSOLD = null;

    /**
     * @var float $NRESTPOINTSNEW
     */
    protected $NRESTPOINTSNEW = null;

    /**
     * @var float $NRESTPOINTSOLD
     */
    protected $NRESTPOINTSOLD = null;

    /**
     * @var float $NRESTRIDES
     */
    protected $NRESTRIDES = null;

    /**
     * @var float $NRESTTIMEUNITINMIN
     */
    protected $NRESTTIMEUNITINMIN = null;

    /**
     * @var float $NSALEPROJNO
     */
    protected $NSALEPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NSPECIALCASENO
     */
    protected $NSPECIALCASENO = null;

    /**
     * @var float $NTIMEINMINCOEFF
     */
    protected $NTIMEINMINCOEFF = null;

    /**
     * @var float $NUNICODENO
     */
    protected $NUNICODENO = null;

    /**
     * @var string $SZCOMMENT
     */
    protected $SZCOMMENT = null;

    /**
     * @var string $SZDISPLAYTEXT
     */
    protected $SZDISPLAYTEXT = null;

    /**
     * @var string $SZPOENAME
     */
    protected $SZPOENAME = null;

    /**
     * @var string $SZSPECIALCASENAME
     */
    protected $SZSPECIALCASENAME = null;

    /**
     * @var string $SZTIMESTAMP
     */
    protected $SZTIMESTAMP = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBADDCHARGE()
    {
      return $this->BADDCHARGE;
    }

    /**
     * @param float $BADDCHARGE
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setBADDCHARGE($BADDCHARGE)
    {
      $this->BADDCHARGE = $BADDCHARGE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCARDBASETYPENO()
    {
      return $this->NCARDBASETYPENO;
    }

    /**
     * @param float $NCARDBASETYPENO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNCARDBASETYPENO($NCARDBASETYPENO)
    {
      $this->NCARDBASETYPENO = $NCARDBASETYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCPUNO()
    {
      return $this->NCPUNO;
    }

    /**
     * @param float $NCPUNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNCPUNO($NCPUNO)
    {
      $this->NCPUNO = $NCPUNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLANENO()
    {
      return $this->NLANENO;
    }

    /**
     * @param float $NLANENO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNLANENO($NLANENO)
    {
      $this->NLANENO = $NLANENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDCPUTRANSNO()
    {
      return $this->NLFDCPUTRANSNO;
    }

    /**
     * @param float $NLFDCPUTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNLFDCPUTRANSNO($NLFDCPUTRANSNO)
    {
      $this->NLFDCPUTRANSNO = $NLFDCPUTRANSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOENO()
    {
      return $this->NPOENO;
    }

    /**
     * @param float $NPOENO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNPOENO($NPOENO)
    {
      $this->NPOENO = $NPOENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRESTDAYSNEW()
    {
      return $this->NRESTDAYSNEW;
    }

    /**
     * @param float $NRESTDAYSNEW
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNRESTDAYSNEW($NRESTDAYSNEW)
    {
      $this->NRESTDAYSNEW = $NRESTDAYSNEW;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRESTDAYSOLD()
    {
      return $this->NRESTDAYSOLD;
    }

    /**
     * @param float $NRESTDAYSOLD
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNRESTDAYSOLD($NRESTDAYSOLD)
    {
      $this->NRESTDAYSOLD = $NRESTDAYSOLD;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRESTPOINTSNEW()
    {
      return $this->NRESTPOINTSNEW;
    }

    /**
     * @param float $NRESTPOINTSNEW
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNRESTPOINTSNEW($NRESTPOINTSNEW)
    {
      $this->NRESTPOINTSNEW = $NRESTPOINTSNEW;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRESTPOINTSOLD()
    {
      return $this->NRESTPOINTSOLD;
    }

    /**
     * @param float $NRESTPOINTSOLD
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNRESTPOINTSOLD($NRESTPOINTSOLD)
    {
      $this->NRESTPOINTSOLD = $NRESTPOINTSOLD;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRESTRIDES()
    {
      return $this->NRESTRIDES;
    }

    /**
     * @param float $NRESTRIDES
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNRESTRIDES($NRESTRIDES)
    {
      $this->NRESTRIDES = $NRESTRIDES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRESTTIMEUNITINMIN()
    {
      return $this->NRESTTIMEUNITINMIN;
    }

    /**
     * @param float $NRESTTIMEUNITINMIN
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNRESTTIMEUNITINMIN($NRESTTIMEUNITINMIN)
    {
      $this->NRESTTIMEUNITINMIN = $NRESTTIMEUNITINMIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSALEPROJNO()
    {
      return $this->NSALEPROJNO;
    }

    /**
     * @param float $NSALEPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNSALEPROJNO($NSALEPROJNO)
    {
      $this->NSALEPROJNO = $NSALEPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSPECIALCASENO()
    {
      return $this->NSPECIALCASENO;
    }

    /**
     * @param float $NSPECIALCASENO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNSPECIALCASENO($NSPECIALCASENO)
    {
      $this->NSPECIALCASENO = $NSPECIALCASENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTIMEINMINCOEFF()
    {
      return $this->NTIMEINMINCOEFF;
    }

    /**
     * @param float $NTIMEINMINCOEFF
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNTIMEINMINCOEFF($NTIMEINMINCOEFF)
    {
      $this->NTIMEINMINCOEFF = $NTIMEINMINCOEFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENO()
    {
      return $this->NUNICODENO;
    }

    /**
     * @param float $NUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setNUNICODENO($NUNICODENO)
    {
      $this->NUNICODENO = $NUNICODENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMMENT()
    {
      return $this->SZCOMMENT;
    }

    /**
     * @param string $SZCOMMENT
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setSZCOMMENT($SZCOMMENT)
    {
      $this->SZCOMMENT = $SZCOMMENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDISPLAYTEXT()
    {
      return $this->SZDISPLAYTEXT;
    }

    /**
     * @param string $SZDISPLAYTEXT
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setSZDISPLAYTEXT($SZDISPLAYTEXT)
    {
      $this->SZDISPLAYTEXT = $SZDISPLAYTEXT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOENAME()
    {
      return $this->SZPOENAME;
    }

    /**
     * @param string $SZPOENAME
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setSZPOENAME($SZPOENAME)
    {
      $this->SZPOENAME = $SZPOENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSPECIALCASENAME()
    {
      return $this->SZSPECIALCASENAME;
    }

    /**
     * @param string $SZSPECIALCASENAME
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setSZSPECIALCASENAME($SZSPECIALCASENAME)
    {
      $this->SZSPECIALCASENAME = $SZSPECIALCASENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTIMESTAMP()
    {
      return $this->SZTIMESTAMP;
    }

    /**
     * @param string $SZTIMESTAMP
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS2
     */
    public function setSZTIMESTAMP($SZTIMESTAMP)
    {
      $this->SZTIMESTAMP = $SZTIMESTAMP;
      return $this;
    }

}
