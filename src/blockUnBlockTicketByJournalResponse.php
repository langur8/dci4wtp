<?php

namespace Axess\Dci4Wtp;

class blockUnBlockTicketByJournalResponse
{

    /**
     * @var D4WTPRESULT $blockUnBlockTicketByJournalResult
     */
    protected $blockUnBlockTicketByJournalResult = null;

    /**
     * @param D4WTPRESULT $blockUnBlockTicketByJournalResult
     */
    public function __construct($blockUnBlockTicketByJournalResult)
    {
      $this->blockUnBlockTicketByJournalResult = $blockUnBlockTicketByJournalResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getBlockUnBlockTicketByJournalResult()
    {
      return $this->blockUnBlockTicketByJournalResult;
    }

    /**
     * @param D4WTPRESULT $blockUnBlockTicketByJournalResult
     * @return \Axess\Dci4Wtp\blockUnBlockTicketByJournalResponse
     */
    public function setBlockUnBlockTicketByJournalResult($blockUnBlockTicketByJournalResult)
    {
      $this->blockUnBlockTicketByJournalResult = $blockUnBlockTicketByJournalResult;
      return $this;
    }

}
