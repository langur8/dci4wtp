<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPCOMPTICKET implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPCOMPTICKET[] $D4WTPPCOMPTICKET
     */
    protected $D4WTPPCOMPTICKET = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPCOMPTICKET[]
     */
    public function getD4WTPPCOMPTICKET()
    {
      return $this->D4WTPPCOMPTICKET;
    }

    /**
     * @param D4WTPPCOMPTICKET[] $D4WTPPCOMPTICKET
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPCOMPTICKET
     */
    public function setD4WTPPCOMPTICKET(array $D4WTPPCOMPTICKET = null)
    {
      $this->D4WTPPCOMPTICKET = $D4WTPPCOMPTICKET;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPCOMPTICKET[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPCOMPTICKET
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPCOMPTICKET[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPCOMPTICKET $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPCOMPTICKET[] = $value;
      } else {
        $this->D4WTPPCOMPTICKET[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPCOMPTICKET[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPCOMPTICKET Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPCOMPTICKET);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPCOMPTICKET);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPCOMPTICKET);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPCOMPTICKET);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPCOMPTICKET Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPCOMPTICKET);
    }

}
