<?php

namespace Axess\Dci4Wtp;

class getPersonTypes
{

    /**
     * @var D4WTPPERSONTYPEREQUEST $i_ctPersTypeReq
     */
    protected $i_ctPersTypeReq = null;

    /**
     * @param D4WTPPERSONTYPEREQUEST $i_ctPersTypeReq
     */
    public function __construct($i_ctPersTypeReq)
    {
      $this->i_ctPersTypeReq = $i_ctPersTypeReq;
    }

    /**
     * @return D4WTPPERSONTYPEREQUEST
     */
    public function getI_ctPersTypeReq()
    {
      return $this->i_ctPersTypeReq;
    }

    /**
     * @param D4WTPPERSONTYPEREQUEST $i_ctPersTypeReq
     * @return \Axess\Dci4Wtp\getPersonTypes
     */
    public function setI_ctPersTypeReq($i_ctPersTypeReq)
    {
      $this->i_ctPersTypeReq = $i_ctPersTypeReq;
      return $this;
    }

}
