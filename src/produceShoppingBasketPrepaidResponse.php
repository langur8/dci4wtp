<?php

namespace Axess\Dci4Wtp;

class produceShoppingBasketPrepaidResponse
{

    /**
     * @var D4WTPPRODSHOPPOSPREPAIDRES $produceShoppingBasketPrepaidResult
     */
    protected $produceShoppingBasketPrepaidResult = null;

    /**
     * @param D4WTPPRODSHOPPOSPREPAIDRES $produceShoppingBasketPrepaidResult
     */
    public function __construct($produceShoppingBasketPrepaidResult)
    {
      $this->produceShoppingBasketPrepaidResult = $produceShoppingBasketPrepaidResult;
    }

    /**
     * @return D4WTPPRODSHOPPOSPREPAIDRES
     */
    public function getProduceShoppingBasketPrepaidResult()
    {
      return $this->produceShoppingBasketPrepaidResult;
    }

    /**
     * @param D4WTPPRODSHOPPOSPREPAIDRES $produceShoppingBasketPrepaidResult
     * @return \Axess\Dci4Wtp\produceShoppingBasketPrepaidResponse
     */
    public function setProduceShoppingBasketPrepaidResult($produceShoppingBasketPrepaidResult)
    {
      $this->produceShoppingBasketPrepaidResult = $produceShoppingBasketPrepaidResult;
      return $this;
    }

}
