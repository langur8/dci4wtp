<?php

namespace Axess\Dci4Wtp;

class D4WTPCLICSREPORTSTATUSREQ
{

    /**
     * @var float $NFORMAT
     */
    protected $NFORMAT = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPREPORTNR
     */
    protected $NWTPREPORTNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNFORMAT()
    {
      return $this->NFORMAT;
    }

    /**
     * @param float $NFORMAT
     * @return \Axess\Dci4Wtp\D4WTPCLICSREPORTSTATUSREQ
     */
    public function setNFORMAT($NFORMAT)
    {
      $this->NFORMAT = $NFORMAT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCLICSREPORTSTATUSREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPREPORTNR()
    {
      return $this->NWTPREPORTNR;
    }

    /**
     * @param float $NWTPREPORTNR
     * @return \Axess\Dci4Wtp\D4WTPCLICSREPORTSTATUSREQ
     */
    public function setNWTPREPORTNR($NWTPREPORTNR)
    {
      $this->NWTPREPORTNR = $NWTPREPORTNR;
      return $this;
    }

}
