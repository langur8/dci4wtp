<?php

namespace Axess\Dci4Wtp;

class WEEKSCALE
{

    /**
     * @var float $BFRIDAY
     */
    protected $BFRIDAY = null;

    /**
     * @var float $BMONDAY
     */
    protected $BMONDAY = null;

    /**
     * @var float $BSATURDAY
     */
    protected $BSATURDAY = null;

    /**
     * @var float $BSUNDAY
     */
    protected $BSUNDAY = null;

    /**
     * @var float $BTHURSDAY
     */
    protected $BTHURSDAY = null;

    /**
     * @var float $BTUESDAY
     */
    protected $BTUESDAY = null;

    /**
     * @var float $BWEDNESDAY
     */
    protected $BWEDNESDAY = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NWOCHSTAFNR
     */
    protected $NWOCHSTAFNR = null;

    /**
     * @var float $NWOCHSTAFTABNR
     */
    protected $NWOCHSTAFTABNR = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZFRIDAYHOURS
     */
    protected $SZFRIDAYHOURS = null;

    /**
     * @var string $SZMONDAYHOURS
     */
    protected $SZMONDAYHOURS = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSATURDAYHOURS
     */
    protected $SZSATURDAYHOURS = null;

    /**
     * @var string $SZSUNDAYHOURS
     */
    protected $SZSUNDAYHOURS = null;

    /**
     * @var string $SZTHURSDAYHOURS
     */
    protected $SZTHURSDAYHOURS = null;

    /**
     * @var string $SZTUESDAYHOURS
     */
    protected $SZTUESDAYHOURS = null;

    /**
     * @var string $SZWEDNESDAYHOURS
     */
    protected $SZWEDNESDAYHOURS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBFRIDAY()
    {
      return $this->BFRIDAY;
    }

    /**
     * @param float $BFRIDAY
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setBFRIDAY($BFRIDAY)
    {
      $this->BFRIDAY = $BFRIDAY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBMONDAY()
    {
      return $this->BMONDAY;
    }

    /**
     * @param float $BMONDAY
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setBMONDAY($BMONDAY)
    {
      $this->BMONDAY = $BMONDAY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBSATURDAY()
    {
      return $this->BSATURDAY;
    }

    /**
     * @param float $BSATURDAY
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setBSATURDAY($BSATURDAY)
    {
      $this->BSATURDAY = $BSATURDAY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBSUNDAY()
    {
      return $this->BSUNDAY;
    }

    /**
     * @param float $BSUNDAY
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setBSUNDAY($BSUNDAY)
    {
      $this->BSUNDAY = $BSUNDAY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBTHURSDAY()
    {
      return $this->BTHURSDAY;
    }

    /**
     * @param float $BTHURSDAY
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setBTHURSDAY($BTHURSDAY)
    {
      $this->BTHURSDAY = $BTHURSDAY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBTUESDAY()
    {
      return $this->BTUESDAY;
    }

    /**
     * @param float $BTUESDAY
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setBTUESDAY($BTUESDAY)
    {
      $this->BTUESDAY = $BTUESDAY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBWEDNESDAY()
    {
      return $this->BWEDNESDAY;
    }

    /**
     * @param float $BWEDNESDAY
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setBWEDNESDAY($BWEDNESDAY)
    {
      $this->BWEDNESDAY = $BWEDNESDAY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWOCHSTAFNR()
    {
      return $this->NWOCHSTAFNR;
    }

    /**
     * @param float $NWOCHSTAFNR
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setNWOCHSTAFNR($NWOCHSTAFNR)
    {
      $this->NWOCHSTAFNR = $NWOCHSTAFNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWOCHSTAFTABNR()
    {
      return $this->NWOCHSTAFTABNR;
    }

    /**
     * @param float $NWOCHSTAFTABNR
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setNWOCHSTAFTABNR($NWOCHSTAFTABNR)
    {
      $this->NWOCHSTAFTABNR = $NWOCHSTAFTABNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFRIDAYHOURS()
    {
      return $this->SZFRIDAYHOURS;
    }

    /**
     * @param string $SZFRIDAYHOURS
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setSZFRIDAYHOURS($SZFRIDAYHOURS)
    {
      $this->SZFRIDAYHOURS = $SZFRIDAYHOURS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMONDAYHOURS()
    {
      return $this->SZMONDAYHOURS;
    }

    /**
     * @param string $SZMONDAYHOURS
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setSZMONDAYHOURS($SZMONDAYHOURS)
    {
      $this->SZMONDAYHOURS = $SZMONDAYHOURS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSATURDAYHOURS()
    {
      return $this->SZSATURDAYHOURS;
    }

    /**
     * @param string $SZSATURDAYHOURS
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setSZSATURDAYHOURS($SZSATURDAYHOURS)
    {
      $this->SZSATURDAYHOURS = $SZSATURDAYHOURS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUNDAYHOURS()
    {
      return $this->SZSUNDAYHOURS;
    }

    /**
     * @param string $SZSUNDAYHOURS
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setSZSUNDAYHOURS($SZSUNDAYHOURS)
    {
      $this->SZSUNDAYHOURS = $SZSUNDAYHOURS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTHURSDAYHOURS()
    {
      return $this->SZTHURSDAYHOURS;
    }

    /**
     * @param string $SZTHURSDAYHOURS
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setSZTHURSDAYHOURS($SZTHURSDAYHOURS)
    {
      $this->SZTHURSDAYHOURS = $SZTHURSDAYHOURS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTUESDAYHOURS()
    {
      return $this->SZTUESDAYHOURS;
    }

    /**
     * @param string $SZTUESDAYHOURS
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setSZTUESDAYHOURS($SZTUESDAYHOURS)
    {
      $this->SZTUESDAYHOURS = $SZTUESDAYHOURS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWEDNESDAYHOURS()
    {
      return $this->SZWEDNESDAYHOURS;
    }

    /**
     * @param string $SZWEDNESDAYHOURS
     * @return \Axess\Dci4Wtp\WEEKSCALE
     */
    public function setSZWEDNESDAYHOURS($SZWEDNESDAYHOURS)
    {
      $this->SZWEDNESDAYHOURS = $SZWEDNESDAYHOURS;
      return $this;
    }

}
