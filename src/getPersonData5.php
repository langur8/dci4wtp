<?php

namespace Axess\Dci4Wtp;

class getPersonData5
{

    /**
     * @var D4WTPGETPERSON4REQUEST $i_ctPersDataReg
     */
    protected $i_ctPersDataReg = null;

    /**
     * @param D4WTPGETPERSON4REQUEST $i_ctPersDataReg
     */
    public function __construct($i_ctPersDataReg)
    {
      $this->i_ctPersDataReg = $i_ctPersDataReg;
    }

    /**
     * @return D4WTPGETPERSON4REQUEST
     */
    public function getI_ctPersDataReg()
    {
      return $this->i_ctPersDataReg;
    }

    /**
     * @param D4WTPGETPERSON4REQUEST $i_ctPersDataReg
     * @return \Axess\Dci4Wtp\getPersonData5
     */
    public function setI_ctPersDataReg($i_ctPersDataReg)
    {
      $this->i_ctPersDataReg = $i_ctPersDataReg;
      return $this;
    }

}
