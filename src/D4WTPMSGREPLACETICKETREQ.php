<?php

namespace Axess\Dci4Wtp;

class D4WTPMSGREPLACETICKETREQ
{

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    /**
     * @var float $NUNICODENO
     */
    protected $NUNICODENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPMSGREPLACETICKETREQ
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPMSGREPLACETICKETREQ
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPMSGREPLACETICKETREQ
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPMSGREPLACETICKETREQ
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPMSGREPLACETICKETREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPMSGREPLACETICKETREQ
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENO()
    {
      return $this->NUNICODENO;
    }

    /**
     * @param float $NUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPMSGREPLACETICKETREQ
     */
    public function setNUNICODENO($NUNICODENO)
    {
      $this->NUNICODENO = $NUNICODENO;
      return $this;
    }

}
