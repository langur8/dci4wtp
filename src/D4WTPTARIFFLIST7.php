<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST7
{

    /**
     * @var ArrayOfD4WTPTARIFFLISTDAY7 $ACTTARIFFLISTDAY7
     */
    protected $ACTTARIFFLISTDAY7 = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLISTDAY7
     */
    public function getACTTARIFFLISTDAY7()
    {
      return $this->ACTTARIFFLISTDAY7;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLISTDAY7 $ACTTARIFFLISTDAY7
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7
     */
    public function setACTTARIFFLISTDAY7($ACTTARIFFLISTDAY7)
    {
      $this->ACTTARIFFLISTDAY7 = $ACTTARIFFLISTDAY7;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST7
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
