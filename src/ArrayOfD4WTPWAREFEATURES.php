<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPWAREFEATURES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPWAREFEATURES[] $D4WTPWAREFEATURES
     */
    protected $D4WTPWAREFEATURES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPWAREFEATURES[]
     */
    public function getD4WTPWAREFEATURES()
    {
      return $this->D4WTPWAREFEATURES;
    }

    /**
     * @param D4WTPWAREFEATURES[] $D4WTPWAREFEATURES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPWAREFEATURES
     */
    public function setD4WTPWAREFEATURES(array $D4WTPWAREFEATURES = null)
    {
      $this->D4WTPWAREFEATURES = $D4WTPWAREFEATURES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPWAREFEATURES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPWAREFEATURES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPWAREFEATURES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPWAREFEATURES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPWAREFEATURES[] = $value;
      } else {
        $this->D4WTPWAREFEATURES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPWAREFEATURES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPWAREFEATURES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPWAREFEATURES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPWAREFEATURES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPWAREFEATURES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPWAREFEATURES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPWAREFEATURES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPWAREFEATURES);
    }

}
