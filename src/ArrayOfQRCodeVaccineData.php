<?php

namespace Axess\Dci4Wtp;

class ArrayOfQRCodeVaccineData implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var QRCodeVaccineData[] $QRCodeVaccineData
     */
    protected $QRCodeVaccineData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return QRCodeVaccineData[]
     */
    public function getQRCodeVaccineData()
    {
      return $this->QRCodeVaccineData;
    }

    /**
     * @param QRCodeVaccineData[] $QRCodeVaccineData
     * @return \Axess\Dci4Wtp\ArrayOfQRCodeVaccineData
     */
    public function setQRCodeVaccineData(array $QRCodeVaccineData = null)
    {
      $this->QRCodeVaccineData = $QRCodeVaccineData;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->QRCodeVaccineData[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return QRCodeVaccineData
     */
    public function offsetGet($offset)
    {
      return $this->QRCodeVaccineData[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param QRCodeVaccineData $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->QRCodeVaccineData[] = $value;
      } else {
        $this->QRCodeVaccineData[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->QRCodeVaccineData[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return QRCodeVaccineData Return the current element
     */
    public function current()
    {
      return current($this->QRCodeVaccineData);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->QRCodeVaccineData);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->QRCodeVaccineData);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->QRCodeVaccineData);
    }

    /**
     * Countable implementation
     *
     * @return QRCodeVaccineData Return count of elements
     */
    public function count()
    {
      return count($this->QRCodeVaccineData);
    }

}
