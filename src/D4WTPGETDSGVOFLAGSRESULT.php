<?php

namespace Axess\Dci4Wtp;

class D4WTPGETDSGVOFLAGSRESULT
{

    /**
     * @var ArrayOfD4WTPDSGVOFLAGS $ACTDSGVOFLAGS
     */
    protected $ACTDSGVOFLAGS = null;

    /**
     * @var D4WTPDEFAULTDSGVOFLAGS $CTDEFAULTDSGVOFLAGS
     */
    protected $CTDEFAULTDSGVOFLAGS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPDSGVOFLAGS
     */
    public function getACTDSGVOFLAGS()
    {
      return $this->ACTDSGVOFLAGS;
    }

    /**
     * @param ArrayOfD4WTPDSGVOFLAGS $ACTDSGVOFLAGS
     * @return \Axess\Dci4Wtp\D4WTPGETDSGVOFLAGSRESULT
     */
    public function setACTDSGVOFLAGS($ACTDSGVOFLAGS)
    {
      $this->ACTDSGVOFLAGS = $ACTDSGVOFLAGS;
      return $this;
    }

    /**
     * @return D4WTPDEFAULTDSGVOFLAGS
     */
    public function getCTDEFAULTDSGVOFLAGS()
    {
      return $this->CTDEFAULTDSGVOFLAGS;
    }

    /**
     * @param D4WTPDEFAULTDSGVOFLAGS $CTDEFAULTDSGVOFLAGS
     * @return \Axess\Dci4Wtp\D4WTPGETDSGVOFLAGSRESULT
     */
    public function setCTDEFAULTDSGVOFLAGS($CTDEFAULTDSGVOFLAGS)
    {
      $this->CTDEFAULTDSGVOFLAGS = $CTDEFAULTDSGVOFLAGS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETDSGVOFLAGSRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETDSGVOFLAGSRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
