<?php

namespace Axess\Dci4Wtp;

class getFirstValidDay
{

    /**
     * @var D4WTPGETFIRSTVALIDDAYREQ $i_request
     */
    protected $i_request = null;

    /**
     * @param D4WTPGETFIRSTVALIDDAYREQ $i_request
     */
    public function __construct($i_request)
    {
      $this->i_request = $i_request;
    }

    /**
     * @return D4WTPGETFIRSTVALIDDAYREQ
     */
    public function getI_request()
    {
      return $this->i_request;
    }

    /**
     * @param D4WTPGETFIRSTVALIDDAYREQ $i_request
     * @return \Axess\Dci4Wtp\getFirstValidDay
     */
    public function setI_request($i_request)
    {
      $this->i_request = $i_request;
      return $this;
    }

}
