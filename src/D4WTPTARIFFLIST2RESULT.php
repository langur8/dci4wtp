<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST2RESULT
{

    /**
     * @var ArrayOfD4WTPTARIFFLIST2 $ACTTARIFFLIST2
     */
    protected $ACTTARIFFLIST2 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLIST2
     */
    public function getACTTARIFFLIST2()
    {
      return $this->ACTTARIFFLIST2;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLIST2 $ACTTARIFFLIST2
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST2RESULT
     */
    public function setACTTARIFFLIST2($ACTTARIFFLIST2)
    {
      $this->ACTTARIFFLIST2 = $ACTTARIFFLIST2;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST2RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST2RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
