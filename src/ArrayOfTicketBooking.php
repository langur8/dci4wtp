<?php

namespace Axess\Dci4Wtp;

class ArrayOfTicketBooking implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var TicketBooking[] $TicketBooking
     */
    protected $TicketBooking = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return TicketBooking[]
     */
    public function getTicketBooking()
    {
      return $this->TicketBooking;
    }

    /**
     * @param TicketBooking[] $TicketBooking
     * @return \Axess\Dci4Wtp\ArrayOfTicketBooking
     */
    public function setTicketBooking(array $TicketBooking = null)
    {
      $this->TicketBooking = $TicketBooking;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->TicketBooking[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return TicketBooking
     */
    public function offsetGet($offset)
    {
      return $this->TicketBooking[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param TicketBooking $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->TicketBooking[] = $value;
      } else {
        $this->TicketBooking[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->TicketBooking[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return TicketBooking Return the current element
     */
    public function current()
    {
      return current($this->TicketBooking);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->TicketBooking);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->TicketBooking);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->TicketBooking);
    }

    /**
     * Countable implementation
     *
     * @return TicketBooking Return count of elements
     */
    public function count()
    {
      return count($this->TicketBooking);
    }

}
