<?php

namespace Axess\Dci4Wtp;

class D4WTPGETPERSON3RESULT
{

    /**
     * @var ArrayOfD4WTPERSONDATA3 $ACTPERSONDATA3
     */
    protected $ACTPERSONDATA3 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NROWCOUNT
     */
    protected $NROWCOUNT = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPERSONDATA3
     */
    public function getACTPERSONDATA3()
    {
      return $this->ACTPERSONDATA3;
    }

    /**
     * @param ArrayOfD4WTPERSONDATA3 $ACTPERSONDATA3
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3RESULT
     */
    public function setACTPERSONDATA3($ACTPERSONDATA3)
    {
      $this->ACTPERSONDATA3 = $ACTPERSONDATA3;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3RESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNROWCOUNT()
    {
      return $this->NROWCOUNT;
    }

    /**
     * @param float $NROWCOUNT
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3RESULT
     */
    public function setNROWCOUNT($NROWCOUNT)
    {
      $this->NROWCOUNT = $NROWCOUNT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETPERSON3RESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
