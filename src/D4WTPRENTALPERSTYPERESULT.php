<?php

namespace Axess\Dci4Wtp;

class D4WTPRENTALPERSTYPERESULT
{

    /**
     * @var ArrayOfD4WTPRENTALPERSTYPE $ACTRENTALPERSTYPE
     */
    protected $ACTRENTALPERSTYPE = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPRENTALPERSTYPE
     */
    public function getACTRENTALPERSTYPE()
    {
      return $this->ACTRENTALPERSTYPE;
    }

    /**
     * @param ArrayOfD4WTPRENTALPERSTYPE $ACTRENTALPERSTYPE
     * @return \Axess\Dci4Wtp\D4WTPRENTALPERSTYPERESULT
     */
    public function setACTRENTALPERSTYPE($ACTRENTALPERSTYPE)
    {
      $this->ACTRENTALPERSTYPE = $ACTRENTALPERSTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPRENTALPERSTYPERESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPRENTALPERSTYPERESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
