<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETTYPE4
{

    /**
     * @var float $BWTPNOPRINTER
     */
    protected $BWTPNOPRINTER = null;

    /**
     * @var float $BWTPWITHPRINTER
     */
    protected $BWTPWITHPRINTER = null;

    /**
     * @var float $NCARDBASETYPENR
     */
    protected $NCARDBASETYPENR = null;

    /**
     * @var float $NGROUPID
     */
    protected $NGROUPID = null;

    /**
     * @var float $NICONSYSNR
     */
    protected $NICONSYSNR = null;

    /**
     * @var float $NICONTYPNR
     */
    protected $NICONTYPNR = null;

    /**
     * @var float $NMAXGROUPPERSON
     */
    protected $NMAXGROUPPERSON = null;

    /**
     * @var float $NMINGROUPPERSON
     */
    protected $NMINGROUPPERSON = null;

    /**
     * @var float $NOFFSETPERSAMOUNT
     */
    protected $NOFFSETPERSAMOUNT = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPREDATEABLE
     */
    protected $NPREDATEABLE = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var string $SZGROUPNAME
     */
    protected $SZGROUPNAME = null;

    /**
     * @var string $SZICONNAME
     */
    protected $SZICONNAME = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBWTPNOPRINTER()
    {
      return $this->BWTPNOPRINTER;
    }

    /**
     * @param float $BWTPNOPRINTER
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setBWTPNOPRINTER($BWTPNOPRINTER)
    {
      $this->BWTPNOPRINTER = $BWTPNOPRINTER;
      return $this;
    }

    /**
     * @return float
     */
    public function getBWTPWITHPRINTER()
    {
      return $this->BWTPWITHPRINTER;
    }

    /**
     * @param float $BWTPWITHPRINTER
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setBWTPWITHPRINTER($BWTPWITHPRINTER)
    {
      $this->BWTPWITHPRINTER = $BWTPWITHPRINTER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCARDBASETYPENR()
    {
      return $this->NCARDBASETYPENR;
    }

    /**
     * @param float $NCARDBASETYPENR
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setNCARDBASETYPENR($NCARDBASETYPENR)
    {
      $this->NCARDBASETYPENR = $NCARDBASETYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNGROUPID()
    {
      return $this->NGROUPID;
    }

    /**
     * @param float $NGROUPID
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setNGROUPID($NGROUPID)
    {
      $this->NGROUPID = $NGROUPID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNICONSYSNR()
    {
      return $this->NICONSYSNR;
    }

    /**
     * @param float $NICONSYSNR
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setNICONSYSNR($NICONSYSNR)
    {
      $this->NICONSYSNR = $NICONSYSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNICONTYPNR()
    {
      return $this->NICONTYPNR;
    }

    /**
     * @param float $NICONTYPNR
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setNICONTYPNR($NICONTYPNR)
    {
      $this->NICONTYPNR = $NICONTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMAXGROUPPERSON()
    {
      return $this->NMAXGROUPPERSON;
    }

    /**
     * @param float $NMAXGROUPPERSON
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setNMAXGROUPPERSON($NMAXGROUPPERSON)
    {
      $this->NMAXGROUPPERSON = $NMAXGROUPPERSON;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMINGROUPPERSON()
    {
      return $this->NMINGROUPPERSON;
    }

    /**
     * @param float $NMINGROUPPERSON
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setNMINGROUPPERSON($NMINGROUPPERSON)
    {
      $this->NMINGROUPPERSON = $NMINGROUPPERSON;
      return $this;
    }

    /**
     * @return float
     */
    public function getNOFFSETPERSAMOUNT()
    {
      return $this->NOFFSETPERSAMOUNT;
    }

    /**
     * @param float $NOFFSETPERSAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setNOFFSETPERSAMOUNT($NOFFSETPERSAMOUNT)
    {
      $this->NOFFSETPERSAMOUNT = $NOFFSETPERSAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPREDATEABLE()
    {
      return $this->NPREDATEABLE;
    }

    /**
     * @param float $NPREDATEABLE
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setNPREDATEABLE($NPREDATEABLE)
    {
      $this->NPREDATEABLE = $NPREDATEABLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGROUPNAME()
    {
      return $this->SZGROUPNAME;
    }

    /**
     * @param string $SZGROUPNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setSZGROUPNAME($SZGROUPNAME)
    {
      $this->SZGROUPNAME = $SZGROUPNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZICONNAME()
    {
      return $this->SZICONNAME;
    }

    /**
     * @param string $SZICONNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setSZICONNAME($SZICONNAME)
    {
      $this->SZICONNAME = $SZICONNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE4
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
