<?php

namespace Axess\Dci4Wtp;

class D4WTPWORKORDER
{

    /**
     * @var ArrayOfD4WTPEMPLOYEEASSIGNED $ACTEMPLOYEEASSIGNED
     */
    protected $ACTEMPLOYEEASSIGNED = null;

    /**
     * @var D4WTPCONTACTPERSON $CTCONTACTPERSON
     */
    protected $CTCONTACTPERSON = null;

    /**
     * @var float $NPARENTWORKORDER
     */
    protected $NPARENTWORKORDER = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NROWCOUNT
     */
    protected $NROWCOUNT = null;

    /**
     * @var float $NWORKORDER
     */
    protected $NWORKORDER = null;

    /**
     * @var float $NWORKORDERSTATUS
     */
    protected $NWORKORDERSTATUS = null;

    /**
     * @var string $SZDETAILS
     */
    protected $SZDETAILS = null;

    /**
     * @var string $SZENDDATE
     */
    protected $SZENDDATE = null;

    /**
     * @var string $SZEXTWORKORDER
     */
    protected $SZEXTWORKORDER = null;

    /**
     * @var string $SZORDERSUBMITTEDDATE
     */
    protected $SZORDERSUBMITTEDDATE = null;

    /**
     * @var string $SZSTARTDATE
     */
    protected $SZSTARTDATE = null;

    /**
     * @var string $SZSUBJECT
     */
    protected $SZSUBJECT = null;

    /**
     * @var string $SZTITLE
     */
    protected $SZTITLE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPEMPLOYEEASSIGNED
     */
    public function getACTEMPLOYEEASSIGNED()
    {
      return $this->ACTEMPLOYEEASSIGNED;
    }

    /**
     * @param ArrayOfD4WTPEMPLOYEEASSIGNED $ACTEMPLOYEEASSIGNED
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setACTEMPLOYEEASSIGNED($ACTEMPLOYEEASSIGNED)
    {
      $this->ACTEMPLOYEEASSIGNED = $ACTEMPLOYEEASSIGNED;
      return $this;
    }

    /**
     * @return D4WTPCONTACTPERSON
     */
    public function getCTCONTACTPERSON()
    {
      return $this->CTCONTACTPERSON;
    }

    /**
     * @param D4WTPCONTACTPERSON $CTCONTACTPERSON
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setCTCONTACTPERSON($CTCONTACTPERSON)
    {
      $this->CTCONTACTPERSON = $CTCONTACTPERSON;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPARENTWORKORDER()
    {
      return $this->NPARENTWORKORDER;
    }

    /**
     * @param float $NPARENTWORKORDER
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setNPARENTWORKORDER($NPARENTWORKORDER)
    {
      $this->NPARENTWORKORDER = $NPARENTWORKORDER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNROWCOUNT()
    {
      return $this->NROWCOUNT;
    }

    /**
     * @param float $NROWCOUNT
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setNROWCOUNT($NROWCOUNT)
    {
      $this->NROWCOUNT = $NROWCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWORKORDER()
    {
      return $this->NWORKORDER;
    }

    /**
     * @param float $NWORKORDER
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setNWORKORDER($NWORKORDER)
    {
      $this->NWORKORDER = $NWORKORDER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWORKORDERSTATUS()
    {
      return $this->NWORKORDERSTATUS;
    }

    /**
     * @param float $NWORKORDERSTATUS
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setNWORKORDERSTATUS($NWORKORDERSTATUS)
    {
      $this->NWORKORDERSTATUS = $NWORKORDERSTATUS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDETAILS()
    {
      return $this->SZDETAILS;
    }

    /**
     * @param string $SZDETAILS
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setSZDETAILS($SZDETAILS)
    {
      $this->SZDETAILS = $SZDETAILS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZENDDATE()
    {
      return $this->SZENDDATE;
    }

    /**
     * @param string $SZENDDATE
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setSZENDDATE($SZENDDATE)
    {
      $this->SZENDDATE = $SZENDDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTWORKORDER()
    {
      return $this->SZEXTWORKORDER;
    }

    /**
     * @param string $SZEXTWORKORDER
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setSZEXTWORKORDER($SZEXTWORKORDER)
    {
      $this->SZEXTWORKORDER = $SZEXTWORKORDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZORDERSUBMITTEDDATE()
    {
      return $this->SZORDERSUBMITTEDDATE;
    }

    /**
     * @param string $SZORDERSUBMITTEDDATE
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setSZORDERSUBMITTEDDATE($SZORDERSUBMITTEDDATE)
    {
      $this->SZORDERSUBMITTEDDATE = $SZORDERSUBMITTEDDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTARTDATE()
    {
      return $this->SZSTARTDATE;
    }

    /**
     * @param string $SZSTARTDATE
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setSZSTARTDATE($SZSTARTDATE)
    {
      $this->SZSTARTDATE = $SZSTARTDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBJECT()
    {
      return $this->SZSUBJECT;
    }

    /**
     * @param string $SZSUBJECT
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setSZSUBJECT($SZSUBJECT)
    {
      $this->SZSUBJECT = $SZSUBJECT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTITLE()
    {
      return $this->SZTITLE;
    }

    /**
     * @param string $SZTITLE
     * @return \Axess\Dci4Wtp\D4WTPWORKORDER
     */
    public function setSZTITLE($SZTITLE)
    {
      $this->SZTITLE = $SZTITLE;
      return $this;
    }

}
