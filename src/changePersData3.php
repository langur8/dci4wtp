<?php

namespace Axess\Dci4Wtp;

class changePersData3
{

    /**
     * @var D4WTPCHANGEPERSDATA3REQUEST $i_ctChangePersDataReq
     */
    protected $i_ctChangePersDataReq = null;

    /**
     * @param D4WTPCHANGEPERSDATA3REQUEST $i_ctChangePersDataReq
     */
    public function __construct($i_ctChangePersDataReq)
    {
      $this->i_ctChangePersDataReq = $i_ctChangePersDataReq;
    }

    /**
     * @return D4WTPCHANGEPERSDATA3REQUEST
     */
    public function getI_ctChangePersDataReq()
    {
      return $this->i_ctChangePersDataReq;
    }

    /**
     * @param D4WTPCHANGEPERSDATA3REQUEST $i_ctChangePersDataReq
     * @return \Axess\Dci4Wtp\changePersData3
     */
    public function setI_ctChangePersDataReq($i_ctChangePersDataReq)
    {
      $this->i_ctChangePersDataReq = $i_ctChangePersDataReq;
      return $this;
    }

}
