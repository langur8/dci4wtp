<?php

namespace Axess\Dci4Wtp;

class DTL4FAMCODINGRESULT
{

    /**
     * @var ArrayOfDTL4FAMCODINGMASTER $ACTMCMASTER
     */
    protected $ACTMCMASTER = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfDTL4FAMCODINGMASTER
     */
    public function getACTMCMASTER()
    {
      return $this->ACTMCMASTER;
    }

    /**
     * @param ArrayOfDTL4FAMCODINGMASTER $ACTMCMASTER
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGRESULT
     */
    public function setACTMCMASTER($ACTMCMASTER)
    {
      $this->ACTMCMASTER = $ACTMCMASTER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
