<?php

namespace Axess\Dci4Wtp;

class D4WTPGETTCKTRIDESDROPSRES2
{

    /**
     * @var ArrayOfD4WTPRIDESANDDROPS2 $ACTRIDESANDDROPS2
     */
    protected $ACTRIDESANDDROPS2 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPRIDESANDDROPS2
     */
    public function getACTRIDESANDDROPS2()
    {
      return $this->ACTRIDESANDDROPS2;
    }

    /**
     * @param ArrayOfD4WTPRIDESANDDROPS2 $ACTRIDESANDDROPS2
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTRIDESDROPSRES2
     */
    public function setACTRIDESANDDROPS2($ACTRIDESANDDROPS2)
    {
      $this->ACTRIDESANDDROPS2 = $ACTRIDESANDDROPS2;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTRIDESDROPSRES2
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTRIDESDROPSRES2
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
