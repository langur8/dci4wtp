<?php

namespace Axess\Dci4Wtp;

class createPOEBookingWithCertificate
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var CreatePOEBookingWithCertRequest $request
     */
    protected $request = null;

    /**
     * @param float $i_nSessionID
     * @param CreatePOEBookingWithCertRequest $request
     */
    public function __construct($i_nSessionID, $request)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->request = $request;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\createPOEBookingWithCertificate
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return CreatePOEBookingWithCertRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param CreatePOEBookingWithCertRequest $request
     * @return \Axess\Dci4Wtp\createPOEBookingWithCertificate
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
