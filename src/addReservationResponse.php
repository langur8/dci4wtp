<?php

namespace Axess\Dci4Wtp;

class addReservationResponse
{

    /**
     * @var D4WTPRESULT $addReservationResult
     */
    protected $addReservationResult = null;

    /**
     * @param D4WTPRESULT $addReservationResult
     */
    public function __construct($addReservationResult)
    {
      $this->addReservationResult = $addReservationResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getAddReservationResult()
    {
      return $this->addReservationResult;
    }

    /**
     * @param D4WTPRESULT $addReservationResult
     * @return \Axess\Dci4Wtp\addReservationResponse
     */
    public function setAddReservationResult($addReservationResult)
    {
      $this->addReservationResult = $addReservationResult;
      return $this;
    }

}
