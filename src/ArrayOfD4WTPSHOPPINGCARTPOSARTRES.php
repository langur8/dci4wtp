<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSHOPPINGCARTPOSARTRES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSHOPPINGCARTPOSARTRES[] $D4WTPSHOPPINGCARTPOSARTRES
     */
    protected $D4WTPSHOPPINGCARTPOSARTRES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSHOPPINGCARTPOSARTRES[]
     */
    public function getD4WTPSHOPPINGCARTPOSARTRES()
    {
      return $this->D4WTPSHOPPINGCARTPOSARTRES;
    }

    /**
     * @param D4WTPSHOPPINGCARTPOSARTRES[] $D4WTPSHOPPINGCARTPOSARTRES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSHOPPINGCARTPOSARTRES
     */
    public function setD4WTPSHOPPINGCARTPOSARTRES(array $D4WTPSHOPPINGCARTPOSARTRES = null)
    {
      $this->D4WTPSHOPPINGCARTPOSARTRES = $D4WTPSHOPPINGCARTPOSARTRES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSHOPPINGCARTPOSARTRES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSHOPPINGCARTPOSARTRES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSHOPPINGCARTPOSARTRES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSHOPPINGCARTPOSARTRES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSHOPPINGCARTPOSARTRES[] = $value;
      } else {
        $this->D4WTPSHOPPINGCARTPOSARTRES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSHOPPINGCARTPOSARTRES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSHOPPINGCARTPOSARTRES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSHOPPINGCARTPOSARTRES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSHOPPINGCARTPOSARTRES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSHOPPINGCARTPOSARTRES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSHOPPINGCARTPOSARTRES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSHOPPINGCARTPOSARTRES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSHOPPINGCARTPOSARTRES);
    }

}
