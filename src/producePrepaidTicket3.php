<?php

namespace Axess\Dci4Wtp;

class producePrepaidTicket3
{

    /**
     * @var D4WTPPRODUCEPREPAIDREQ $i_producePrepaidReq
     */
    protected $i_producePrepaidReq = null;

    /**
     * @param D4WTPPRODUCEPREPAIDREQ $i_producePrepaidReq
     */
    public function __construct($i_producePrepaidReq)
    {
      $this->i_producePrepaidReq = $i_producePrepaidReq;
    }

    /**
     * @return D4WTPPRODUCEPREPAIDREQ
     */
    public function getI_producePrepaidReq()
    {
      return $this->i_producePrepaidReq;
    }

    /**
     * @param D4WTPPRODUCEPREPAIDREQ $i_producePrepaidReq
     * @return \Axess\Dci4Wtp\producePrepaidTicket3
     */
    public function setI_producePrepaidReq($i_producePrepaidReq)
    {
      $this->i_producePrepaidReq = $i_producePrepaidReq;
      return $this;
    }

}
