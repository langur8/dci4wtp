<?php

namespace Axess\Dci4Wtp;

class D4WTPGROUPSRES
{

    /**
     * @var ArrayOfD4WTPGROUP $ACTGROUP
     */
    protected $ACTGROUP = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPGROUP
     */
    public function getACTGROUP()
    {
      return $this->ACTGROUP;
    }

    /**
     * @param ArrayOfD4WTPGROUP $ACTGROUP
     * @return \Axess\Dci4Wtp\D4WTPGROUPSRES
     */
    public function setACTGROUP($ACTGROUP)
    {
      $this->ACTGROUP = $ACTGROUP;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGROUPSRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGROUPSRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
