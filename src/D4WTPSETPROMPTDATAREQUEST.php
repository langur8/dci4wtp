<?php

namespace Axess\Dci4Wtp;

class D4WTPSETPROMPTDATAREQUEST
{

    /**
     * @var ArrayOfD4WTPPROMPTDATA $ACTPROMPTDATA
     */
    protected $ACTPROMPTDATA = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPROMPTDATA
     */
    public function getACTPROMPTDATA()
    {
      return $this->ACTPROMPTDATA;
    }

    /**
     * @param ArrayOfD4WTPPROMPTDATA $ACTPROMPTDATA
     * @return \Axess\Dci4Wtp\D4WTPSETPROMPTDATAREQUEST
     */
    public function setACTPROMPTDATA($ACTPROMPTDATA)
    {
      $this->ACTPROMPTDATA = $ACTPROMPTDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSETPROMPTDATAREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
