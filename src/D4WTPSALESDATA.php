<?php

namespace Axess\Dci4Wtp;

class D4WTPSALESDATA
{

    /**
     * @var ArrayOfD4WTPADDARTICLEINFO $ACTADDARTICLEINFO
     */
    protected $ACTADDARTICLEINFO = null;

    /**
     * @var float $BISCANCELLED
     */
    protected $BISCANCELLED = null;

    /**
     * @var D4WTPERSONDATA $CTPERSONDATA
     */
    protected $CTPERSONDATA = null;

    /**
     * @var float $NPERCENTVAT
     */
    protected $NPERCENTVAT = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var string $SZCASHIERFIRSTNAME
     */
    protected $SZCASHIERFIRSTNAME = null;

    /**
     * @var string $SZCASHIERLASTNAME
     */
    protected $SZCASHIERLASTNAME = null;

    /**
     * @var string $SZCREATIONTIME
     */
    protected $SZCREATIONTIME = null;

    /**
     * @var string $SZCURRENCY
     */
    protected $SZCURRENCY = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZPAYMENTTYPENAME
     */
    protected $SZPAYMENTTYPENAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZTICKETTYPENAME
     */
    protected $SZTICKETTYPENAME = null;

    /**
     * @var string $SZTICKETTYPESHORTNAME
     */
    protected $SZTICKETTYPESHORTNAME = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    /**
     * @var string $SZWTPNO
     */
    protected $SZWTPNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDARTICLEINFO
     */
    public function getACTADDARTICLEINFO()
    {
      return $this->ACTADDARTICLEINFO;
    }

    /**
     * @param ArrayOfD4WTPADDARTICLEINFO $ACTADDARTICLEINFO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setACTADDARTICLEINFO($ACTADDARTICLEINFO)
    {
      $this->ACTADDARTICLEINFO = $ACTADDARTICLEINFO;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISCANCELLED()
    {
      return $this->BISCANCELLED;
    }

    /**
     * @param float $BISCANCELLED
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setBISCANCELLED($BISCANCELLED)
    {
      $this->BISCANCELLED = $BISCANCELLED;
      return $this;
    }

    /**
     * @return D4WTPERSONDATA
     */
    public function getCTPERSONDATA()
    {
      return $this->CTPERSONDATA;
    }

    /**
     * @param D4WTPERSONDATA $CTPERSONDATA
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setCTPERSONDATA($CTPERSONDATA)
    {
      $this->CTPERSONDATA = $CTPERSONDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERCENTVAT()
    {
      return $this->NPERCENTVAT;
    }

    /**
     * @param float $NPERCENTVAT
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setNPERCENTVAT($NPERCENTVAT)
    {
      $this->NPERCENTVAT = $NPERCENTVAT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCASHIERFIRSTNAME()
    {
      return $this->SZCASHIERFIRSTNAME;
    }

    /**
     * @param string $SZCASHIERFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setSZCASHIERFIRSTNAME($SZCASHIERFIRSTNAME)
    {
      $this->SZCASHIERFIRSTNAME = $SZCASHIERFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCASHIERLASTNAME()
    {
      return $this->SZCASHIERLASTNAME;
    }

    /**
     * @param string $SZCASHIERLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setSZCASHIERLASTNAME($SZCASHIERLASTNAME)
    {
      $this->SZCASHIERLASTNAME = $SZCASHIERLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCREATIONTIME()
    {
      return $this->SZCREATIONTIME;
    }

    /**
     * @param string $SZCREATIONTIME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setSZCREATIONTIME($SZCREATIONTIME)
    {
      $this->SZCREATIONTIME = $SZCREATIONTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCY()
    {
      return $this->SZCURRENCY;
    }

    /**
     * @param string $SZCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setSZCURRENCY($SZCURRENCY)
    {
      $this->SZCURRENCY = $SZCURRENCY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPAYMENTTYPENAME()
    {
      return $this->SZPAYMENTTYPENAME;
    }

    /**
     * @param string $SZPAYMENTTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setSZPAYMENTTYPENAME($SZPAYMENTTYPENAME)
    {
      $this->SZPAYMENTTYPENAME = $SZPAYMENTTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPENAME()
    {
      return $this->SZTICKETTYPENAME;
    }

    /**
     * @param string $SZTICKETTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setSZTICKETTYPENAME($SZTICKETTYPENAME)
    {
      $this->SZTICKETTYPENAME = $SZTICKETTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPESHORTNAME()
    {
      return $this->SZTICKETTYPESHORTNAME;
    }

    /**
     * @param string $SZTICKETTYPESHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setSZTICKETTYPESHORTNAME($SZTICKETTYPESHORTNAME)
    {
      $this->SZTICKETTYPESHORTNAME = $SZTICKETTYPESHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNO()
    {
      return $this->SZWTPNO;
    }

    /**
     * @param string $SZWTPNO
     * @return \Axess\Dci4Wtp\D4WTPSALESDATA
     */
    public function setSZWTPNO($SZWTPNO)
    {
      $this->SZWTPNO = $SZWTPNO;
      return $this;
    }

}
