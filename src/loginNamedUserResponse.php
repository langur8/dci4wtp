<?php

namespace Axess\Dci4Wtp;

class loginNamedUserResponse
{

    /**
     * @var D4WTPLOGINNAMEDUSERRESULT $loginNamedUserResult
     */
    protected $loginNamedUserResult = null;

    /**
     * @param D4WTPLOGINNAMEDUSERRESULT $loginNamedUserResult
     */
    public function __construct($loginNamedUserResult)
    {
      $this->loginNamedUserResult = $loginNamedUserResult;
    }

    /**
     * @return D4WTPLOGINNAMEDUSERRESULT
     */
    public function getLoginNamedUserResult()
    {
      return $this->loginNamedUserResult;
    }

    /**
     * @param D4WTPLOGINNAMEDUSERRESULT $loginNamedUserResult
     * @return \Axess\Dci4Wtp\loginNamedUserResponse
     */
    public function setLoginNamedUserResult($loginNamedUserResult)
    {
      $this->loginNamedUserResult = $loginNamedUserResult;
      return $this;
    }

}
