<?php

namespace Axess\Dci4Wtp;

class getConfigFileResponse
{

    /**
     * @var string $getConfigFileResult
     */
    protected $getConfigFileResult = null;

    /**
     * @param string $getConfigFileResult
     */
    public function __construct($getConfigFileResult)
    {
      $this->getConfigFileResult = $getConfigFileResult;
    }

    /**
     * @return string
     */
    public function getGetConfigFileResult()
    {
      return $this->getConfigFileResult;
    }

    /**
     * @param string $getConfigFileResult
     * @return \Axess\Dci4Wtp\getConfigFileResponse
     */
    public function setGetConfigFileResult($getConfigFileResult)
    {
      $this->getConfigFileResult = $getConfigFileResult;
      return $this;
    }

}
