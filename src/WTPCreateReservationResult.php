<?php

namespace Axess\Dci4Wtp;

class WTPCreateReservationResult
{

    /**
     * @var int $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var int $ReservationNo
     */
    protected $ReservationNo = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var int $bOpenWeb
     */
    protected $bOpenWeb = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param int $NERRORNO
     * @return \Axess\Dci4Wtp\WTPCreateReservationResult
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return int
     */
    public function getReservationNo()
    {
      return $this->ReservationNo;
    }

    /**
     * @param int $ReservationNo
     * @return \Axess\Dci4Wtp\WTPCreateReservationResult
     */
    public function setReservationNo($ReservationNo)
    {
      $this->ReservationNo = $ReservationNo;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\WTPCreateReservationResult
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return int
     */
    public function getBOpenWeb()
    {
      return $this->bOpenWeb;
    }

    /**
     * @param int $bOpenWeb
     * @return \Axess\Dci4Wtp\WTPCreateReservationResult
     */
    public function setBOpenWeb($bOpenWeb)
    {
      $this->bOpenWeb = $bOpenWeb;
      return $this;
    }

}
