<?php

namespace Axess\Dci4Wtp;

class getPackageTariffList4
{

    /**
     * @var D4WTPPKGTARIFFLIST2REQUEST $i_ctPkgTariffListReq
     */
    protected $i_ctPkgTariffListReq = null;

    /**
     * @param D4WTPPKGTARIFFLIST2REQUEST $i_ctPkgTariffListReq
     */
    public function __construct($i_ctPkgTariffListReq)
    {
      $this->i_ctPkgTariffListReq = $i_ctPkgTariffListReq;
    }

    /**
     * @return D4WTPPKGTARIFFLIST2REQUEST
     */
    public function getI_ctPkgTariffListReq()
    {
      return $this->i_ctPkgTariffListReq;
    }

    /**
     * @param D4WTPPKGTARIFFLIST2REQUEST $i_ctPkgTariffListReq
     * @return \Axess\Dci4Wtp\getPackageTariffList4
     */
    public function setI_ctPkgTariffListReq($i_ctPkgTariffListReq)
    {
      $this->i_ctPkgTariffListReq = $i_ctPkgTariffListReq;
      return $this;
    }

}
