<?php

namespace Axess\Dci4Wtp;

class setShoppingCartStatusResponse
{

    /**
     * @var D4WTPRESULT $setShoppingCartStatusResult
     */
    protected $setShoppingCartStatusResult = null;

    /**
     * @param D4WTPRESULT $setShoppingCartStatusResult
     */
    public function __construct($setShoppingCartStatusResult)
    {
      $this->setShoppingCartStatusResult = $setShoppingCartStatusResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSetShoppingCartStatusResult()
    {
      return $this->setShoppingCartStatusResult;
    }

    /**
     * @param D4WTPRESULT $setShoppingCartStatusResult
     * @return \Axess\Dci4Wtp\setShoppingCartStatusResponse
     */
    public function setSetShoppingCartStatusResult($setShoppingCartStatusResult)
    {
      $this->setShoppingCartStatusResult = $setShoppingCartStatusResult;
      return $this;
    }

}
