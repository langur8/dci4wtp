<?php

namespace Axess\Dci4Wtp;

class createPOEBookingWithCertificateResponse
{

    /**
     * @var WTPCreatePOEBookingWithCertResult $createPOEBookingWithCertificateResult
     */
    protected $createPOEBookingWithCertificateResult = null;

    /**
     * @param WTPCreatePOEBookingWithCertResult $createPOEBookingWithCertificateResult
     */
    public function __construct($createPOEBookingWithCertificateResult)
    {
      $this->createPOEBookingWithCertificateResult = $createPOEBookingWithCertificateResult;
    }

    /**
     * @return WTPCreatePOEBookingWithCertResult
     */
    public function getCreatePOEBookingWithCertificateResult()
    {
      return $this->createPOEBookingWithCertificateResult;
    }

    /**
     * @param WTPCreatePOEBookingWithCertResult $createPOEBookingWithCertificateResult
     * @return \Axess\Dci4Wtp\createPOEBookingWithCertificateResponse
     */
    public function setCreatePOEBookingWithCertificateResult($createPOEBookingWithCertificateResult)
    {
      $this->createPOEBookingWithCertificateResult = $createPOEBookingWithCertificateResult;
      return $this;
    }

}
