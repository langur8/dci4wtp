<?php

namespace Axess\Dci4Wtp;

class getRentalItemTariff2Response
{

    /**
     * @var D4WTPGETRENTALITEMTARIFF2RES $getRentalItemTariff2Result
     */
    protected $getRentalItemTariff2Result = null;

    /**
     * @param D4WTPGETRENTALITEMTARIFF2RES $getRentalItemTariff2Result
     */
    public function __construct($getRentalItemTariff2Result)
    {
      $this->getRentalItemTariff2Result = $getRentalItemTariff2Result;
    }

    /**
     * @return D4WTPGETRENTALITEMTARIFF2RES
     */
    public function getGetRentalItemTariff2Result()
    {
      return $this->getRentalItemTariff2Result;
    }

    /**
     * @param D4WTPGETRENTALITEMTARIFF2RES $getRentalItemTariff2Result
     * @return \Axess\Dci4Wtp\getRentalItemTariff2Response
     */
    public function setGetRentalItemTariff2Result($getRentalItemTariff2Result)
    {
      $this->getRentalItemTariff2Result = $getRentalItemTariff2Result;
      return $this;
    }

}
