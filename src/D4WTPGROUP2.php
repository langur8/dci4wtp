<?php

namespace Axess\Dci4Wtp;

class D4WTPGROUP2
{

    /**
     * @var ArrayOfWTPPOOLTICKETTYPE $ACTPOOLTICKETTYPE
     */
    protected $ACTPOOLTICKETTYPE = null;

    /**
     * @var ArrayOfD4WTPPACKAGE $ACTPPACKAGE
     */
    protected $ACTPPACKAGE = null;

    /**
     * @var ArrayOfD4WTPSTANDARTICLE $ACTSTANDARTICLE
     */
    protected $ACTSTANDARTICLE = null;

    /**
     * @var ArrayOfD4WTPGROUPWARE $ACTWARE
     */
    protected $ACTWARE = null;

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var base64Binary $BLOBPICTURE
     */
    protected $BLOBPICTURE = null;

    /**
     * @var float $NID
     */
    protected $NID = null;

    /**
     * @var float $NPARENTSNR
     */
    protected $NPARENTSNR = null;

    /**
     * @var float $NSORTNR
     */
    protected $NSORTNR = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSHORTNAME
     */
    protected $SZSHORTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfWTPPOOLTICKETTYPE
     */
    public function getACTPOOLTICKETTYPE()
    {
      return $this->ACTPOOLTICKETTYPE;
    }

    /**
     * @param ArrayOfWTPPOOLTICKETTYPE $ACTPOOLTICKETTYPE
     * @return \Axess\Dci4Wtp\D4WTPGROUP2
     */
    public function setACTPOOLTICKETTYPE($ACTPOOLTICKETTYPE)
    {
      $this->ACTPOOLTICKETTYPE = $ACTPOOLTICKETTYPE;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPPACKAGE
     */
    public function getACTPPACKAGE()
    {
      return $this->ACTPPACKAGE;
    }

    /**
     * @param ArrayOfD4WTPPACKAGE $ACTPPACKAGE
     * @return \Axess\Dci4Wtp\D4WTPGROUP2
     */
    public function setACTPPACKAGE($ACTPPACKAGE)
    {
      $this->ACTPPACKAGE = $ACTPPACKAGE;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPSTANDARTICLE
     */
    public function getACTSTANDARTICLE()
    {
      return $this->ACTSTANDARTICLE;
    }

    /**
     * @param ArrayOfD4WTPSTANDARTICLE $ACTSTANDARTICLE
     * @return \Axess\Dci4Wtp\D4WTPGROUP2
     */
    public function setACTSTANDARTICLE($ACTSTANDARTICLE)
    {
      $this->ACTSTANDARTICLE = $ACTSTANDARTICLE;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPGROUPWARE
     */
    public function getACTWARE()
    {
      return $this->ACTWARE;
    }

    /**
     * @param ArrayOfD4WTPGROUPWARE $ACTWARE
     * @return \Axess\Dci4Wtp\D4WTPGROUP2
     */
    public function setACTWARE($ACTWARE)
    {
      $this->ACTWARE = $ACTWARE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPGROUP2
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return base64Binary
     */
    public function getBLOBPICTURE()
    {
      return $this->BLOBPICTURE;
    }

    /**
     * @param base64Binary $BLOBPICTURE
     * @return \Axess\Dci4Wtp\D4WTPGROUP2
     */
    public function setBLOBPICTURE($BLOBPICTURE)
    {
      $this->BLOBPICTURE = $BLOBPICTURE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNID()
    {
      return $this->NID;
    }

    /**
     * @param float $NID
     * @return \Axess\Dci4Wtp\D4WTPGROUP2
     */
    public function setNID($NID)
    {
      $this->NID = $NID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPARENTSNR()
    {
      return $this->NPARENTSNR;
    }

    /**
     * @param float $NPARENTSNR
     * @return \Axess\Dci4Wtp\D4WTPGROUP2
     */
    public function setNPARENTSNR($NPARENTSNR)
    {
      $this->NPARENTSNR = $NPARENTSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNR()
    {
      return $this->NSORTNR;
    }

    /**
     * @param float $NSORTNR
     * @return \Axess\Dci4Wtp\D4WTPGROUP2
     */
    public function setNSORTNR($NSORTNR)
    {
      $this->NSORTNR = $NSORTNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPGROUP2
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPGROUP2
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHORTNAME()
    {
      return $this->SZSHORTNAME;
    }

    /**
     * @param string $SZSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPGROUP2
     */
    public function setSZSHORTNAME($SZSHORTNAME)
    {
      $this->SZSHORTNAME = $SZSHORTNAME;
      return $this;
    }

}
