<?php

namespace Axess\Dci4Wtp;

class ArrayOfTAXARTICLES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var TAXARTICLES[] $TAXARTICLES
     */
    protected $TAXARTICLES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return TAXARTICLES[]
     */
    public function getTAXARTICLES()
    {
      return $this->TAXARTICLES;
    }

    /**
     * @param TAXARTICLES[] $TAXARTICLES
     * @return \Axess\Dci4Wtp\ArrayOfTAXARTICLES
     */
    public function setTAXARTICLES(array $TAXARTICLES = null)
    {
      $this->TAXARTICLES = $TAXARTICLES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->TAXARTICLES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return TAXARTICLES
     */
    public function offsetGet($offset)
    {
      return $this->TAXARTICLES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param TAXARTICLES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->TAXARTICLES[] = $value;
      } else {
        $this->TAXARTICLES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->TAXARTICLES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return TAXARTICLES Return the current element
     */
    public function current()
    {
      return current($this->TAXARTICLES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->TAXARTICLES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->TAXARTICLES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->TAXARTICLES);
    }

    /**
     * Countable implementation
     *
     * @return TAXARTICLES Return count of elements
     */
    public function count()
    {
      return count($this->TAXARTICLES);
    }

}
