<?php

namespace Axess\Dci4Wtp;

class D4WTPSAVEVOUCHER
{

    /**
     * @var float $NEINZELJOURNALNR
     */
    protected $NEINZELJOURNALNR = null;

    /**
     * @var float $NEINZELKASSANR
     */
    protected $NEINZELKASSANR = null;

    /**
     * @var float $NEINZELPROJNR
     */
    protected $NEINZELPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZVOUCHERID
     */
    protected $SZVOUCHERID = null;

    /**
     * @var string $SZVOUCHERTYPE
     */
    protected $SZVOUCHERTYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNEINZELJOURNALNR()
    {
      return $this->NEINZELJOURNALNR;
    }

    /**
     * @param float $NEINZELJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPSAVEVOUCHER
     */
    public function setNEINZELJOURNALNR($NEINZELJOURNALNR)
    {
      $this->NEINZELJOURNALNR = $NEINZELJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEINZELKASSANR()
    {
      return $this->NEINZELKASSANR;
    }

    /**
     * @param float $NEINZELKASSANR
     * @return \Axess\Dci4Wtp\D4WTPSAVEVOUCHER
     */
    public function setNEINZELKASSANR($NEINZELKASSANR)
    {
      $this->NEINZELKASSANR = $NEINZELKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEINZELPROJNR()
    {
      return $this->NEINZELPROJNR;
    }

    /**
     * @param float $NEINZELPROJNR
     * @return \Axess\Dci4Wtp\D4WTPSAVEVOUCHER
     */
    public function setNEINZELPROJNR($NEINZELPROJNR)
    {
      $this->NEINZELPROJNR = $NEINZELPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSAVEVOUCHER
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVOUCHERID()
    {
      return $this->SZVOUCHERID;
    }

    /**
     * @param string $SZVOUCHERID
     * @return \Axess\Dci4Wtp\D4WTPSAVEVOUCHER
     */
    public function setSZVOUCHERID($SZVOUCHERID)
    {
      $this->SZVOUCHERID = $SZVOUCHERID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVOUCHERTYPE()
    {
      return $this->SZVOUCHERTYPE;
    }

    /**
     * @param string $SZVOUCHERTYPE
     * @return \Axess\Dci4Wtp\D4WTPSAVEVOUCHER
     */
    public function setSZVOUCHERTYPE($SZVOUCHERTYPE)
    {
      $this->SZVOUCHERTYPE = $SZVOUCHERTYPE;
      return $this;
    }

}
