<?php

namespace Axess\Dci4Wtp;

class getARCubesResponse
{

    /**
     * @var D4WTPGETARCUBESRESULT $getARCubesResult
     */
    protected $getARCubesResult = null;

    /**
     * @param D4WTPGETARCUBESRESULT $getARCubesResult
     */
    public function __construct($getARCubesResult)
    {
      $this->getARCubesResult = $getARCubesResult;
    }

    /**
     * @return D4WTPGETARCUBESRESULT
     */
    public function getGetARCubesResult()
    {
      return $this->getARCubesResult;
    }

    /**
     * @param D4WTPGETARCUBESRESULT $getARCubesResult
     * @return \Axess\Dci4Wtp\getARCubesResponse
     */
    public function setGetARCubesResult($getARCubesResult)
    {
      $this->getARCubesResult = $getARCubesResult;
      return $this;
    }

}
