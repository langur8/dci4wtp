<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPLOCKRESERVATION implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPLOCKRESERVATION[] $D4WTPLOCKRESERVATION
     */
    protected $D4WTPLOCKRESERVATION = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPLOCKRESERVATION[]
     */
    public function getD4WTPLOCKRESERVATION()
    {
      return $this->D4WTPLOCKRESERVATION;
    }

    /**
     * @param D4WTPLOCKRESERVATION[] $D4WTPLOCKRESERVATION
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPLOCKRESERVATION
     */
    public function setD4WTPLOCKRESERVATION(array $D4WTPLOCKRESERVATION = null)
    {
      $this->D4WTPLOCKRESERVATION = $D4WTPLOCKRESERVATION;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPLOCKRESERVATION[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPLOCKRESERVATION
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPLOCKRESERVATION[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPLOCKRESERVATION $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPLOCKRESERVATION[] = $value;
      } else {
        $this->D4WTPLOCKRESERVATION[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPLOCKRESERVATION[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPLOCKRESERVATION Return the current element
     */
    public function current()
    {
      return current($this->D4WTPLOCKRESERVATION);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPLOCKRESERVATION);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPLOCKRESERVATION);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPLOCKRESERVATION);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPLOCKRESERVATION Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPLOCKRESERVATION);
    }

}
