<?php

namespace Axess\Dci4Wtp;

class modifyReservation
{

    /**
     * @var D4WTPMODIFYRESREQUEST $i_ctmodifyReservationReq
     */
    protected $i_ctmodifyReservationReq = null;

    /**
     * @param D4WTPMODIFYRESREQUEST $i_ctmodifyReservationReq
     */
    public function __construct($i_ctmodifyReservationReq)
    {
      $this->i_ctmodifyReservationReq = $i_ctmodifyReservationReq;
    }

    /**
     * @return D4WTPMODIFYRESREQUEST
     */
    public function getI_ctmodifyReservationReq()
    {
      return $this->i_ctmodifyReservationReq;
    }

    /**
     * @param D4WTPMODIFYRESREQUEST $i_ctmodifyReservationReq
     * @return \Axess\Dci4Wtp\modifyReservation
     */
    public function setI_ctmodifyReservationReq($i_ctmodifyReservationReq)
    {
      $this->i_ctmodifyReservationReq = $i_ctmodifyReservationReq;
      return $this;
    }

}
