<?php

namespace Axess\Dci4Wtp;

class D4WTPMANAGEWORKORDERREQUEST
{

    /**
     * @var D4WTPMANAGEEMPLOYEE $CTMANAGEEMPLOYEE
     */
    protected $CTMANAGEEMPLOYEE = null;

    /**
     * @var float $NCOMPANYNO
     */
    protected $NCOMPANYNO = null;

    /**
     * @var float $NCOMPANYPOSNO
     */
    protected $NCOMPANYPOSNO = null;

    /**
     * @var float $NCOMPANYPROJNO
     */
    protected $NCOMPANYPROJNO = null;

    /**
     * @var float $NPARENTWORKORDER
     */
    protected $NPARENTWORKORDER = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWORKORDER
     */
    protected $NWORKORDER = null;

    /**
     * @var float $NWORKORDERSTATUS
     */
    protected $NWORKORDERSTATUS = null;

    /**
     * @var string $SZDETAILS
     */
    protected $SZDETAILS = null;

    /**
     * @var string $SZENDDATE
     */
    protected $SZENDDATE = null;

    /**
     * @var string $SZEXTWORKORDER
     */
    protected $SZEXTWORKORDER = null;

    /**
     * @var string $SZMODE
     */
    protected $SZMODE = null;

    /**
     * @var string $SZSTARTDATE
     */
    protected $SZSTARTDATE = null;

    /**
     * @var string $SZSUBJECT
     */
    protected $SZSUBJECT = null;

    /**
     * @var string $SZTITLE
     */
    protected $SZTITLE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPMANAGEEMPLOYEE
     */
    public function getCTMANAGEEMPLOYEE()
    {
      return $this->CTMANAGEEMPLOYEE;
    }

    /**
     * @param D4WTPMANAGEEMPLOYEE $CTMANAGEEMPLOYEE
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setCTMANAGEEMPLOYEE($CTMANAGEEMPLOYEE)
    {
      $this->CTMANAGEEMPLOYEE = $CTMANAGEEMPLOYEE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNO()
    {
      return $this->NCOMPANYNO;
    }

    /**
     * @param float $NCOMPANYNO
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setNCOMPANYNO($NCOMPANYNO)
    {
      $this->NCOMPANYNO = $NCOMPANYNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNO()
    {
      return $this->NCOMPANYPOSNO;
    }

    /**
     * @param float $NCOMPANYPOSNO
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setNCOMPANYPOSNO($NCOMPANYPOSNO)
    {
      $this->NCOMPANYPOSNO = $NCOMPANYPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNO()
    {
      return $this->NCOMPANYPROJNO;
    }

    /**
     * @param float $NCOMPANYPROJNO
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setNCOMPANYPROJNO($NCOMPANYPROJNO)
    {
      $this->NCOMPANYPROJNO = $NCOMPANYPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPARENTWORKORDER()
    {
      return $this->NPARENTWORKORDER;
    }

    /**
     * @param float $NPARENTWORKORDER
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setNPARENTWORKORDER($NPARENTWORKORDER)
    {
      $this->NPARENTWORKORDER = $NPARENTWORKORDER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWORKORDER()
    {
      return $this->NWORKORDER;
    }

    /**
     * @param float $NWORKORDER
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setNWORKORDER($NWORKORDER)
    {
      $this->NWORKORDER = $NWORKORDER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWORKORDERSTATUS()
    {
      return $this->NWORKORDERSTATUS;
    }

    /**
     * @param float $NWORKORDERSTATUS
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setNWORKORDERSTATUS($NWORKORDERSTATUS)
    {
      $this->NWORKORDERSTATUS = $NWORKORDERSTATUS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDETAILS()
    {
      return $this->SZDETAILS;
    }

    /**
     * @param string $SZDETAILS
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setSZDETAILS($SZDETAILS)
    {
      $this->SZDETAILS = $SZDETAILS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZENDDATE()
    {
      return $this->SZENDDATE;
    }

    /**
     * @param string $SZENDDATE
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setSZENDDATE($SZENDDATE)
    {
      $this->SZENDDATE = $SZENDDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTWORKORDER()
    {
      return $this->SZEXTWORKORDER;
    }

    /**
     * @param string $SZEXTWORKORDER
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setSZEXTWORKORDER($SZEXTWORKORDER)
    {
      $this->SZEXTWORKORDER = $SZEXTWORKORDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMODE()
    {
      return $this->SZMODE;
    }

    /**
     * @param string $SZMODE
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setSZMODE($SZMODE)
    {
      $this->SZMODE = $SZMODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTARTDATE()
    {
      return $this->SZSTARTDATE;
    }

    /**
     * @param string $SZSTARTDATE
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setSZSTARTDATE($SZSTARTDATE)
    {
      $this->SZSTARTDATE = $SZSTARTDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBJECT()
    {
      return $this->SZSUBJECT;
    }

    /**
     * @param string $SZSUBJECT
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setSZSUBJECT($SZSUBJECT)
    {
      $this->SZSUBJECT = $SZSUBJECT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTITLE()
    {
      return $this->SZTITLE;
    }

    /**
     * @param string $SZTITLE
     * @return \Axess\Dci4Wtp\D4WTPMANAGEWORKORDERREQUEST
     */
    public function setSZTITLE($SZTITLE)
    {
      $this->SZTITLE = $SZTITLE;
      return $this;
    }

}
