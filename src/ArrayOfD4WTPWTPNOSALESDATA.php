<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPWTPNOSALESDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPWTPNOSALESDATA[] $D4WTPWTPNOSALESDATA
     */
    protected $D4WTPWTPNOSALESDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPWTPNOSALESDATA[]
     */
    public function getD4WTPWTPNOSALESDATA()
    {
      return $this->D4WTPWTPNOSALESDATA;
    }

    /**
     * @param D4WTPWTPNOSALESDATA[] $D4WTPWTPNOSALESDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPWTPNOSALESDATA
     */
    public function setD4WTPWTPNOSALESDATA(array $D4WTPWTPNOSALESDATA = null)
    {
      $this->D4WTPWTPNOSALESDATA = $D4WTPWTPNOSALESDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPWTPNOSALESDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPWTPNOSALESDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPWTPNOSALESDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPWTPNOSALESDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPWTPNOSALESDATA[] = $value;
      } else {
        $this->D4WTPWTPNOSALESDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPWTPNOSALESDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPWTPNOSALESDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPWTPNOSALESDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPWTPNOSALESDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPWTPNOSALESDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPWTPNOSALESDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPWTPNOSALESDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPWTPNOSALESDATA);
    }

}
