<?php

namespace Axess\Dci4Wtp;

class checkWTPNo2Response
{

    /**
     * @var D4WTPCHECKWTPNO2RESULT $checkWTPNo2Result
     */
    protected $checkWTPNo2Result = null;

    /**
     * @param D4WTPCHECKWTPNO2RESULT $checkWTPNo2Result
     */
    public function __construct($checkWTPNo2Result)
    {
      $this->checkWTPNo2Result = $checkWTPNo2Result;
    }

    /**
     * @return D4WTPCHECKWTPNO2RESULT
     */
    public function getCheckWTPNo2Result()
    {
      return $this->checkWTPNo2Result;
    }

    /**
     * @param D4WTPCHECKWTPNO2RESULT $checkWTPNo2Result
     * @return \Axess\Dci4Wtp\checkWTPNo2Response
     */
    public function setCheckWTPNo2Result($checkWTPNo2Result)
    {
      $this->checkWTPNo2Result = $checkWTPNo2Result;
      return $this;
    }

}
