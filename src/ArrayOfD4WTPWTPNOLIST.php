<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPWTPNOLIST implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPWTPNOLIST[] $D4WTPWTPNOLIST
     */
    protected $D4WTPWTPNOLIST = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPWTPNOLIST[]
     */
    public function getD4WTPWTPNOLIST()
    {
      return $this->D4WTPWTPNOLIST;
    }

    /**
     * @param D4WTPWTPNOLIST[] $D4WTPWTPNOLIST
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPWTPNOLIST
     */
    public function setD4WTPWTPNOLIST(array $D4WTPWTPNOLIST = null)
    {
      $this->D4WTPWTPNOLIST = $D4WTPWTPNOLIST;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPWTPNOLIST[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPWTPNOLIST
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPWTPNOLIST[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPWTPNOLIST $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPWTPNOLIST[] = $value;
      } else {
        $this->D4WTPWTPNOLIST[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPWTPNOLIST[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPWTPNOLIST Return the current element
     */
    public function current()
    {
      return current($this->D4WTPWTPNOLIST);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPWTPNOLIST);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPWTPNOLIST);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPWTPNOLIST);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPWTPNOLIST Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPWTPNOLIST);
    }

}
