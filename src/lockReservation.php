<?php

namespace Axess\Dci4Wtp;

class lockReservation
{

    /**
     * @var D4WTPLOCKRESERVATIONREQUEST $i_ctlockReservationReq
     */
    protected $i_ctlockReservationReq = null;

    /**
     * @param D4WTPLOCKRESERVATIONREQUEST $i_ctlockReservationReq
     */
    public function __construct($i_ctlockReservationReq)
    {
      $this->i_ctlockReservationReq = $i_ctlockReservationReq;
    }

    /**
     * @return D4WTPLOCKRESERVATIONREQUEST
     */
    public function getI_ctlockReservationReq()
    {
      return $this->i_ctlockReservationReq;
    }

    /**
     * @param D4WTPLOCKRESERVATIONREQUEST $i_ctlockReservationReq
     * @return \Axess\Dci4Wtp\lockReservation
     */
    public function setI_ctlockReservationReq($i_ctlockReservationReq)
    {
      $this->i_ctlockReservationReq = $i_ctlockReservationReq;
      return $this;
    }

}
