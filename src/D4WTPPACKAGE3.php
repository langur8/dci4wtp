<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGE3
{

    /**
     * @var float $NPACKNR
     */
    protected $NPACKNR = null;

    /**
     * @var float $NPACKTARIFFSHEETNR
     */
    protected $NPACKTARIFFSHEETNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZPACKTARIFFSHEETNAME
     */
    protected $SZPACKTARIFFSHEETNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPACKNR()
    {
      return $this->NPACKNR;
    }

    /**
     * @param float $NPACKNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE3
     */
    public function setNPACKNR($NPACKNR)
    {
      $this->NPACKNR = $NPACKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKTARIFFSHEETNR()
    {
      return $this->NPACKTARIFFSHEETNR;
    }

    /**
     * @param float $NPACKTARIFFSHEETNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE3
     */
    public function setNPACKTARIFFSHEETNR($NPACKTARIFFSHEETNR)
    {
      $this->NPACKTARIFFSHEETNR = $NPACKTARIFFSHEETNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE3
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE3
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPACKTARIFFSHEETNAME()
    {
      return $this->SZPACKTARIFFSHEETNAME;
    }

    /**
     * @param string $SZPACKTARIFFSHEETNAME
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE3
     */
    public function setSZPACKTARIFFSHEETNAME($SZPACKTARIFFSHEETNAME)
    {
      $this->SZPACKTARIFFSHEETNAME = $SZPACKTARIFFSHEETNAME;
      return $this;
    }

}
