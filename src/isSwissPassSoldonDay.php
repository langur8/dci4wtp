<?php

namespace Axess\Dci4Wtp;

class isSwissPassSoldonDay
{

    /**
     * @var D4WTPSWISSPASSSALESREQ $i_SwissPassSalesReq
     */
    protected $i_SwissPassSalesReq = null;

    /**
     * @param D4WTPSWISSPASSSALESREQ $i_SwissPassSalesReq
     */
    public function __construct($i_SwissPassSalesReq)
    {
      $this->i_SwissPassSalesReq = $i_SwissPassSalesReq;
    }

    /**
     * @return D4WTPSWISSPASSSALESREQ
     */
    public function getI_SwissPassSalesReq()
    {
      return $this->i_SwissPassSalesReq;
    }

    /**
     * @param D4WTPSWISSPASSSALESREQ $i_SwissPassSalesReq
     * @return \Axess\Dci4Wtp\isSwissPassSoldonDay
     */
    public function setI_SwissPassSalesReq($i_SwissPassSalesReq)
    {
      $this->i_SwissPassSalesReq = $i_SwissPassSalesReq;
      return $this;
    }

}
