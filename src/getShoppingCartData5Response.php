<?php

namespace Axess\Dci4Wtp;

class getShoppingCartData5Response
{

    /**
     * @var D4WTPGETSHOPCARTDATA5RES $getShoppingCartData5Result
     */
    protected $getShoppingCartData5Result = null;

    /**
     * @param D4WTPGETSHOPCARTDATA5RES $getShoppingCartData5Result
     */
    public function __construct($getShoppingCartData5Result)
    {
      $this->getShoppingCartData5Result = $getShoppingCartData5Result;
    }

    /**
     * @return D4WTPGETSHOPCARTDATA5RES
     */
    public function getGetShoppingCartData5Result()
    {
      return $this->getShoppingCartData5Result;
    }

    /**
     * @param D4WTPGETSHOPCARTDATA5RES $getShoppingCartData5Result
     * @return \Axess\Dci4Wtp\getShoppingCartData5Response
     */
    public function setGetShoppingCartData5Result($getShoppingCartData5Result)
    {
      $this->getShoppingCartData5Result = $getShoppingCartData5Result;
      return $this;
    }

}
