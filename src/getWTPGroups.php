<?php

namespace Axess\Dci4Wtp;

class getWTPGroups
{

    /**
     * @var D4WTPGROUPSREQ $i_ctGroupReq
     */
    protected $i_ctGroupReq = null;

    /**
     * @param D4WTPGROUPSREQ $i_ctGroupReq
     */
    public function __construct($i_ctGroupReq)
    {
      $this->i_ctGroupReq = $i_ctGroupReq;
    }

    /**
     * @return D4WTPGROUPSREQ
     */
    public function getI_ctGroupReq()
    {
      return $this->i_ctGroupReq;
    }

    /**
     * @param D4WTPGROUPSREQ $i_ctGroupReq
     * @return \Axess\Dci4Wtp\getWTPGroups
     */
    public function setI_ctGroupReq($i_ctGroupReq)
    {
      $this->i_ctGroupReq = $i_ctGroupReq;
      return $this;
    }

}
