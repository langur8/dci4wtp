<?php

namespace Axess\Dci4Wtp;

class cancelTicket5
{

    /**
     * @var D4WTPCANCELTICKET5REQUEST $i_ctCancelTicket5Request
     */
    protected $i_ctCancelTicket5Request = null;

    /**
     * @param D4WTPCANCELTICKET5REQUEST $i_ctCancelTicket5Request
     */
    public function __construct($i_ctCancelTicket5Request)
    {
      $this->i_ctCancelTicket5Request = $i_ctCancelTicket5Request;
    }

    /**
     * @return D4WTPCANCELTICKET5REQUEST
     */
    public function getI_ctCancelTicket5Request()
    {
      return $this->i_ctCancelTicket5Request;
    }

    /**
     * @param D4WTPCANCELTICKET5REQUEST $i_ctCancelTicket5Request
     * @return \Axess\Dci4Wtp\cancelTicket5
     */
    public function setI_ctCancelTicket5Request($i_ctCancelTicket5Request)
    {
      $this->i_ctCancelTicket5Request = $i_ctCancelTicket5Request;
      return $this;
    }

}
