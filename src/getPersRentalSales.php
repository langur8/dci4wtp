<?php

namespace Axess\Dci4Wtp;

class getPersRentalSales
{

    /**
     * @var D4WTPPERSRENTALSALESREQ $i_ctGetPersRentalSalesReq
     */
    protected $i_ctGetPersRentalSalesReq = null;

    /**
     * @param D4WTPPERSRENTALSALESREQ $i_ctGetPersRentalSalesReq
     */
    public function __construct($i_ctGetPersRentalSalesReq)
    {
      $this->i_ctGetPersRentalSalesReq = $i_ctGetPersRentalSalesReq;
    }

    /**
     * @return D4WTPPERSRENTALSALESREQ
     */
    public function getI_ctGetPersRentalSalesReq()
    {
      return $this->i_ctGetPersRentalSalesReq;
    }

    /**
     * @param D4WTPPERSRENTALSALESREQ $i_ctGetPersRentalSalesReq
     * @return \Axess\Dci4Wtp\getPersRentalSales
     */
    public function setI_ctGetPersRentalSalesReq($i_ctGetPersRentalSalesReq)
    {
      $this->i_ctGetPersRentalSalesReq = $i_ctGetPersRentalSalesReq;
      return $this;
    }

}
