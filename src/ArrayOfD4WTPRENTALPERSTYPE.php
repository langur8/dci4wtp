<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPRENTALPERSTYPE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPRENTALPERSTYPE[] $D4WTPRENTALPERSTYPE
     */
    protected $D4WTPRENTALPERSTYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPRENTALPERSTYPE[]
     */
    public function getD4WTPRENTALPERSTYPE()
    {
      return $this->D4WTPRENTALPERSTYPE;
    }

    /**
     * @param D4WTPRENTALPERSTYPE[] $D4WTPRENTALPERSTYPE
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPRENTALPERSTYPE
     */
    public function setD4WTPRENTALPERSTYPE(array $D4WTPRENTALPERSTYPE = null)
    {
      $this->D4WTPRENTALPERSTYPE = $D4WTPRENTALPERSTYPE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPRENTALPERSTYPE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPRENTALPERSTYPE
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPRENTALPERSTYPE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPRENTALPERSTYPE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPRENTALPERSTYPE[] = $value;
      } else {
        $this->D4WTPRENTALPERSTYPE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPRENTALPERSTYPE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPRENTALPERSTYPE Return the current element
     */
    public function current()
    {
      return current($this->D4WTPRENTALPERSTYPE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPRENTALPERSTYPE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPRENTALPERSTYPE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPRENTALPERSTYPE);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPRENTALPERSTYPE Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPRENTALPERSTYPE);
    }

}
