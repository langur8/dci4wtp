<?php

namespace Axess\Dci4Wtp;

class getShoppingCartData4Response
{

    /**
     * @var D4WTPGETSHOPCARTDATA4RES $getShoppingCartData4Result
     */
    protected $getShoppingCartData4Result = null;

    /**
     * @param D4WTPGETSHOPCARTDATA4RES $getShoppingCartData4Result
     */
    public function __construct($getShoppingCartData4Result)
    {
      $this->getShoppingCartData4Result = $getShoppingCartData4Result;
    }

    /**
     * @return D4WTPGETSHOPCARTDATA4RES
     */
    public function getGetShoppingCartData4Result()
    {
      return $this->getShoppingCartData4Result;
    }

    /**
     * @param D4WTPGETSHOPCARTDATA4RES $getShoppingCartData4Result
     * @return \Axess\Dci4Wtp\getShoppingCartData4Response
     */
    public function setGetShoppingCartData4Result($getShoppingCartData4Result)
    {
      $this->getShoppingCartData4Result = $getShoppingCartData4Result;
      return $this;
    }

}
