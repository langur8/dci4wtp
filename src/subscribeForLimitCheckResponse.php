<?php

namespace Axess\Dci4Wtp;

class subscribeForLimitCheckResponse
{

    /**
     * @var D4WTPRESULT $subscribeForLimitCheckResult
     */
    protected $subscribeForLimitCheckResult = null;

    /**
     * @param D4WTPRESULT $subscribeForLimitCheckResult
     */
    public function __construct($subscribeForLimitCheckResult)
    {
      $this->subscribeForLimitCheckResult = $subscribeForLimitCheckResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSubscribeForLimitCheckResult()
    {
      return $this->subscribeForLimitCheckResult;
    }

    /**
     * @param D4WTPRESULT $subscribeForLimitCheckResult
     * @return \Axess\Dci4Wtp\subscribeForLimitCheckResponse
     */
    public function setSubscribeForLimitCheckResult($subscribeForLimitCheckResult)
    {
      $this->subscribeForLimitCheckResult = $subscribeForLimitCheckResult;
      return $this;
    }

}
