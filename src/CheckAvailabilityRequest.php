<?php

namespace Axess\Dci4Wtp;

class CheckAvailabilityRequest
{

    /**
     * @var ArrayOfstring $Days
     */
    protected $Days = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfstring
     */
    public function getDays()
    {
      return $this->Days;
    }

    /**
     * @param ArrayOfstring $Days
     * @return \Axess\Dci4Wtp\CheckAvailabilityRequest
     */
    public function setDays($Days)
    {
      $this->Days = $Days;
      return $this;
    }

}
