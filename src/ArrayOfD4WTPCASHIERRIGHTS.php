<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCASHIERRIGHTS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCASHIERRIGHTS[] $D4WTPCASHIERRIGHTS
     */
    protected $D4WTPCASHIERRIGHTS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCASHIERRIGHTS[]
     */
    public function getD4WTPCASHIERRIGHTS()
    {
      return $this->D4WTPCASHIERRIGHTS;
    }

    /**
     * @param D4WTPCASHIERRIGHTS[] $D4WTPCASHIERRIGHTS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCASHIERRIGHTS
     */
    public function setD4WTPCASHIERRIGHTS(array $D4WTPCASHIERRIGHTS = null)
    {
      $this->D4WTPCASHIERRIGHTS = $D4WTPCASHIERRIGHTS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCASHIERRIGHTS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCASHIERRIGHTS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCASHIERRIGHTS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCASHIERRIGHTS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCASHIERRIGHTS[] = $value;
      } else {
        $this->D4WTPCASHIERRIGHTS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCASHIERRIGHTS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCASHIERRIGHTS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCASHIERRIGHTS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCASHIERRIGHTS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCASHIERRIGHTS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCASHIERRIGHTS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCASHIERRIGHTS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCASHIERRIGHTS);
    }

}
