<?php

namespace Axess\Dci4Wtp;

class D4WTPRODUCEPREPAIDTICKETRES
{

    /**
     * @var D4WTPCODINGDATA $CTCODINGDATA
     */
    protected $CTCODINGDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var float $NKASSANR
     */
    protected $NKASSANR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var string $SZAUFTRAGEXT
     */
    protected $SZAUFTRAGEXT = null;

    /**
     * @var string $SZBINCODE
     */
    protected $SZBINCODE = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZPRINTDATA
     */
    protected $SZPRINTDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCODINGDATA
     */
    public function getCTCODINGDATA()
    {
      return $this->CTCODINGDATA;
    }

    /**
     * @param D4WTPCODINGDATA $CTCODINGDATA
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES
     */
    public function setCTCODINGDATA($CTCODINGDATA)
    {
      $this->CTCODINGDATA = $CTCODINGDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKASSANR()
    {
      return $this->NKASSANR;
    }

    /**
     * @param float $NKASSANR
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES
     */
    public function setNKASSANR($NKASSANR)
    {
      $this->NKASSANR = $NKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAUFTRAGEXT()
    {
      return $this->SZAUFTRAGEXT;
    }

    /**
     * @param string $SZAUFTRAGEXT
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES
     */
    public function setSZAUFTRAGEXT($SZAUFTRAGEXT)
    {
      $this->SZAUFTRAGEXT = $SZAUFTRAGEXT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBINCODE()
    {
      return $this->SZBINCODE;
    }

    /**
     * @param string $SZBINCODE
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES
     */
    public function setSZBINCODE($SZBINCODE)
    {
      $this->SZBINCODE = $SZBINCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPRINTDATA()
    {
      return $this->SZPRINTDATA;
    }

    /**
     * @param string $SZPRINTDATA
     * @return \Axess\Dci4Wtp\D4WTPRODUCEPREPAIDTICKETRES
     */
    public function setSZPRINTDATA($SZPRINTDATA)
    {
      $this->SZPRINTDATA = $SZPRINTDATA;
      return $this;
    }

}
