<?php

namespace Axess\Dci4Wtp;

class login
{

    /**
     * @var D4WTPLOGINREQUEST $i_ctLoginReq
     */
    protected $i_ctLoginReq = null;

    /**
     * @param D4WTPLOGINREQUEST $i_ctLoginReq
     */
    public function __construct($i_ctLoginReq)
    {
      $this->i_ctLoginReq = $i_ctLoginReq;
    }

    /**
     * @return D4WTPLOGINREQUEST
     */
    public function getI_ctLoginReq()
    {
      return $this->i_ctLoginReq;
    }

    /**
     * @param D4WTPLOGINREQUEST $i_ctLoginReq
     * @return \Axess\Dci4Wtp\login
     */
    public function setI_ctLoginReq($i_ctLoginReq)
    {
      $this->i_ctLoginReq = $i_ctLoginReq;
      return $this;
    }

}
