<?php

namespace Axess\Dci4Wtp;

class ArrayOfSWISSTICKETSALE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var SWISSTICKETSALE[] $SWISSTICKETSALE
     */
    protected $SWISSTICKETSALE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SWISSTICKETSALE[]
     */
    public function getSWISSTICKETSALE()
    {
      return $this->SWISSTICKETSALE;
    }

    /**
     * @param SWISSTICKETSALE[] $SWISSTICKETSALE
     * @return \Axess\Dci4Wtp\ArrayOfSWISSTICKETSALE
     */
    public function setSWISSTICKETSALE(array $SWISSTICKETSALE = null)
    {
      $this->SWISSTICKETSALE = $SWISSTICKETSALE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->SWISSTICKETSALE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return SWISSTICKETSALE
     */
    public function offsetGet($offset)
    {
      return $this->SWISSTICKETSALE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param SWISSTICKETSALE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->SWISSTICKETSALE[] = $value;
      } else {
        $this->SWISSTICKETSALE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->SWISSTICKETSALE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return SWISSTICKETSALE Return the current element
     */
    public function current()
    {
      return current($this->SWISSTICKETSALE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->SWISSTICKETSALE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->SWISSTICKETSALE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->SWISSTICKETSALE);
    }

    /**
     * Countable implementation
     *
     * @return SWISSTICKETSALE Return count of elements
     */
    public function count()
    {
      return count($this->SWISSTICKETSALE);
    }

}
