<?php

namespace Axess\Dci4Wtp;

class D4WTPLOCATIONINFO
{

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var float $NITEMLOCATIONNR
     */
    protected $NITEMLOCATIONNR = null;

    /**
     * @var float $NITEMLOCATIONTYPENR
     */
    protected $NITEMLOCATIONTYPENR = null;

    /**
     * @var float $NNRKREISART
     */
    protected $NNRKREISART = null;

    /**
     * @var string $SZBITMAPNAME
     */
    protected $SZBITMAPNAME = null;

    /**
     * @var string $SZCODE
     */
    protected $SZCODE = null;

    /**
     * @var string $SZCOLOR
     */
    protected $SZCOLOR = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZTELNR
     */
    protected $SZTELNR = null;

    /**
     * @var string $SZURL
     */
    protected $SZURL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPLOCATIONINFO
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNITEMLOCATIONNR()
    {
      return $this->NITEMLOCATIONNR;
    }

    /**
     * @param float $NITEMLOCATIONNR
     * @return \Axess\Dci4Wtp\D4WTPLOCATIONINFO
     */
    public function setNITEMLOCATIONNR($NITEMLOCATIONNR)
    {
      $this->NITEMLOCATIONNR = $NITEMLOCATIONNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNITEMLOCATIONTYPENR()
    {
      return $this->NITEMLOCATIONTYPENR;
    }

    /**
     * @param float $NITEMLOCATIONTYPENR
     * @return \Axess\Dci4Wtp\D4WTPLOCATIONINFO
     */
    public function setNITEMLOCATIONTYPENR($NITEMLOCATIONTYPENR)
    {
      $this->NITEMLOCATIONTYPENR = $NITEMLOCATIONTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNRKREISART()
    {
      return $this->NNRKREISART;
    }

    /**
     * @param float $NNRKREISART
     * @return \Axess\Dci4Wtp\D4WTPLOCATIONINFO
     */
    public function setNNRKREISART($NNRKREISART)
    {
      $this->NNRKREISART = $NNRKREISART;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBITMAPNAME()
    {
      return $this->SZBITMAPNAME;
    }

    /**
     * @param string $SZBITMAPNAME
     * @return \Axess\Dci4Wtp\D4WTPLOCATIONINFO
     */
    public function setSZBITMAPNAME($SZBITMAPNAME)
    {
      $this->SZBITMAPNAME = $SZBITMAPNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODE()
    {
      return $this->SZCODE;
    }

    /**
     * @param string $SZCODE
     * @return \Axess\Dci4Wtp\D4WTPLOCATIONINFO
     */
    public function setSZCODE($SZCODE)
    {
      $this->SZCODE = $SZCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOLOR()
    {
      return $this->SZCOLOR;
    }

    /**
     * @param string $SZCOLOR
     * @return \Axess\Dci4Wtp\D4WTPLOCATIONINFO
     */
    public function setSZCOLOR($SZCOLOR)
    {
      $this->SZCOLOR = $SZCOLOR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPLOCATIONINFO
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPLOCATIONINFO
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTELNR()
    {
      return $this->SZTELNR;
    }

    /**
     * @param string $SZTELNR
     * @return \Axess\Dci4Wtp\D4WTPLOCATIONINFO
     */
    public function setSZTELNR($SZTELNR)
    {
      $this->SZTELNR = $SZTELNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZURL()
    {
      return $this->SZURL;
    }

    /**
     * @param string $SZURL
     * @return \Axess\Dci4Wtp\D4WTPLOCATIONINFO
     */
    public function setSZURL($SZURL)
    {
      $this->SZURL = $SZURL;
      return $this;
    }

}
