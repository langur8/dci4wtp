<?php

namespace Axess\Dci4Wtp;

class D4WTPPAYERREQUEST
{

    /**
     * @var float $BTESTEXPERT
     */
    protected $BTESTEXPERT = null;

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var float $NPERSPERSNR
     */
    protected $NPERSPERSNR = null;

    /**
     * @var float $NPERSPOSNR
     */
    protected $NPERSPOSNR = null;

    /**
     * @var float $NPERSPROJNR
     */
    protected $NPERSPROJNR = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSERIALNR
     */
    protected $NSERIALNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNR
     */
    protected $NTRANSNR = null;

    /**
     * @var float $NUNICODENR
     */
    protected $NUNICODENR = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBTESTEXPERT()
    {
      return $this->BTESTEXPERT;
    }

    /**
     * @param float $BTESTEXPERT
     * @return \Axess\Dci4Wtp\D4WTPPAYERREQUEST
     */
    public function setBTESTEXPERT($BTESTEXPERT)
    {
      $this->BTESTEXPERT = $BTESTEXPERT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPPAYERREQUEST
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPERSNR()
    {
      return $this->NPERSPERSNR;
    }

    /**
     * @param float $NPERSPERSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYERREQUEST
     */
    public function setNPERSPERSNR($NPERSPERSNR)
    {
      $this->NPERSPERSNR = $NPERSPERSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNR()
    {
      return $this->NPERSPOSNR;
    }

    /**
     * @param float $NPERSPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYERREQUEST
     */
    public function setNPERSPOSNR($NPERSPOSNR)
    {
      $this->NPERSPOSNR = $NPERSPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNR()
    {
      return $this->NPERSPROJNR;
    }

    /**
     * @param float $NPERSPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPAYERREQUEST
     */
    public function setNPERSPROJNR($NPERSPROJNR)
    {
      $this->NPERSPROJNR = $NPERSPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYERREQUEST
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPAYERREQUEST
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNR()
    {
      return $this->NSERIALNR;
    }

    /**
     * @param float $NSERIALNR
     * @return \Axess\Dci4Wtp\D4WTPPAYERREQUEST
     */
    public function setNSERIALNR($NSERIALNR)
    {
      $this->NSERIALNR = $NSERIALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPPAYERREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNR()
    {
      return $this->NTRANSNR;
    }

    /**
     * @param float $NTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYERREQUEST
     */
    public function setNTRANSNR($NTRANSNR)
    {
      $this->NTRANSNR = $NTRANSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENR()
    {
      return $this->NUNICODENR;
    }

    /**
     * @param float $NUNICODENR
     * @return \Axess\Dci4Wtp\D4WTPPAYERREQUEST
     */
    public function setNUNICODENR($NUNICODENR)
    {
      $this->NUNICODENR = $NUNICODENR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPPAYERREQUEST
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
