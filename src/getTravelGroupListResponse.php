<?php

namespace Axess\Dci4Wtp;

class getTravelGroupListResponse
{

    /**
     * @var D4WTPGETTRAVELGRPLISTRESULT $getTravelGroupListResult
     */
    protected $getTravelGroupListResult = null;

    /**
     * @param D4WTPGETTRAVELGRPLISTRESULT $getTravelGroupListResult
     */
    public function __construct($getTravelGroupListResult)
    {
      $this->getTravelGroupListResult = $getTravelGroupListResult;
    }

    /**
     * @return D4WTPGETTRAVELGRPLISTRESULT
     */
    public function getGetTravelGroupListResult()
    {
      return $this->getTravelGroupListResult;
    }

    /**
     * @param D4WTPGETTRAVELGRPLISTRESULT $getTravelGroupListResult
     * @return \Axess\Dci4Wtp\getTravelGroupListResponse
     */
    public function setGetTravelGroupListResult($getTravelGroupListResult)
    {
      $this->getTravelGroupListResult = $getTravelGroupListResult;
      return $this;
    }

}
