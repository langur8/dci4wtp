<?php

namespace Axess\Dci4Wtp;

class D4WTPCOMPANYINFOREQUEST
{

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYINFOREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
