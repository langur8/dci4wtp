<?php

namespace Axess\Dci4Wtp;

class getGroupSubContingent3Response
{

    /**
     * @var D4WTPGETGRPSUBCONTINGENT3RES $getGroupSubContingent3Result
     */
    protected $getGroupSubContingent3Result = null;

    /**
     * @param D4WTPGETGRPSUBCONTINGENT3RES $getGroupSubContingent3Result
     */
    public function __construct($getGroupSubContingent3Result)
    {
      $this->getGroupSubContingent3Result = $getGroupSubContingent3Result;
    }

    /**
     * @return D4WTPGETGRPSUBCONTINGENT3RES
     */
    public function getGetGroupSubContingent3Result()
    {
      return $this->getGroupSubContingent3Result;
    }

    /**
     * @param D4WTPGETGRPSUBCONTINGENT3RES $getGroupSubContingent3Result
     * @return \Axess\Dci4Wtp\getGroupSubContingent3Response
     */
    public function setGetGroupSubContingent3Result($getGroupSubContingent3Result)
    {
      $this->getGroupSubContingent3Result = $getGroupSubContingent3Result;
      return $this;
    }

}
