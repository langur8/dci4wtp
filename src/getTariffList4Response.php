<?php

namespace Axess\Dci4Wtp;

class getTariffList4Response
{

    /**
     * @var D4WTPTARIFFLIST4RESULT $getTariffList4Result
     */
    protected $getTariffList4Result = null;

    /**
     * @param D4WTPTARIFFLIST4RESULT $getTariffList4Result
     */
    public function __construct($getTariffList4Result)
    {
      $this->getTariffList4Result = $getTariffList4Result;
    }

    /**
     * @return D4WTPTARIFFLIST4RESULT
     */
    public function getGetTariffList4Result()
    {
      return $this->getTariffList4Result;
    }

    /**
     * @param D4WTPTARIFFLIST4RESULT $getTariffList4Result
     * @return \Axess\Dci4Wtp\getTariffList4Response
     */
    public function setGetTariffList4Result($getTariffList4Result)
    {
      $this->getTariffList4Result = $getTariffList4Result;
      return $this;
    }

}
