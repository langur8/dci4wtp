<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPADDLICENSEPLATES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPADDLICENSEPLATES[] $D4WTPADDLICENSEPLATES
     */
    protected $D4WTPADDLICENSEPLATES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPADDLICENSEPLATES[]
     */
    public function getD4WTPADDLICENSEPLATES()
    {
      return $this->D4WTPADDLICENSEPLATES;
    }

    /**
     * @param D4WTPADDLICENSEPLATES[] $D4WTPADDLICENSEPLATES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPADDLICENSEPLATES
     */
    public function setD4WTPADDLICENSEPLATES(array $D4WTPADDLICENSEPLATES = null)
    {
      $this->D4WTPADDLICENSEPLATES = $D4WTPADDLICENSEPLATES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPADDLICENSEPLATES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPADDLICENSEPLATES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPADDLICENSEPLATES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPADDLICENSEPLATES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPADDLICENSEPLATES[] = $value;
      } else {
        $this->D4WTPADDLICENSEPLATES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPADDLICENSEPLATES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPADDLICENSEPLATES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPADDLICENSEPLATES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPADDLICENSEPLATES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPADDLICENSEPLATES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPADDLICENSEPLATES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPADDLICENSEPLATES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPADDLICENSEPLATES);
    }

}
