<?php

namespace Axess\Dci4Wtp;

class reloadTicketResponse
{

    /**
     * @var D4WTPRELOADTICKETRESULT $reloadTicketResult
     */
    protected $reloadTicketResult = null;

    /**
     * @param D4WTPRELOADTICKETRESULT $reloadTicketResult
     */
    public function __construct($reloadTicketResult)
    {
      $this->reloadTicketResult = $reloadTicketResult;
    }

    /**
     * @return D4WTPRELOADTICKETRESULT
     */
    public function getReloadTicketResult()
    {
      return $this->reloadTicketResult;
    }

    /**
     * @param D4WTPRELOADTICKETRESULT $reloadTicketResult
     * @return \Axess\Dci4Wtp\reloadTicketResponse
     */
    public function setReloadTicketResult($reloadTicketResult)
    {
      $this->reloadTicketResult = $reloadTicketResult;
      return $this;
    }

}
