<?php

namespace Axess\Dci4Wtp;

class eMoneyPayInOut
{

    /**
     * @var D4WTPEMONEYPAYINOUTREQ $i_ctEmoneyPayInOutReq
     */
    protected $i_ctEmoneyPayInOutReq = null;

    /**
     * @param D4WTPEMONEYPAYINOUTREQ $i_ctEmoneyPayInOutReq
     */
    public function __construct($i_ctEmoneyPayInOutReq)
    {
      $this->i_ctEmoneyPayInOutReq = $i_ctEmoneyPayInOutReq;
    }

    /**
     * @return D4WTPEMONEYPAYINOUTREQ
     */
    public function getI_ctEmoneyPayInOutReq()
    {
      return $this->i_ctEmoneyPayInOutReq;
    }

    /**
     * @param D4WTPEMONEYPAYINOUTREQ $i_ctEmoneyPayInOutReq
     * @return \Axess\Dci4Wtp\eMoneyPayInOut
     */
    public function setI_ctEmoneyPayInOutReq($i_ctEmoneyPayInOutReq)
    {
      $this->i_ctEmoneyPayInOutReq = $i_ctEmoneyPayInOutReq;
      return $this;
    }

}
