<?php

namespace Axess\Dci4Wtp;

class D4WTPWAREFEATURES
{

    /**
     * @var float $BISVARIANTRELATED
     */
    protected $BISVARIANTRELATED = null;

    /**
     * @var float $NFEATURETYPENO
     */
    protected $NFEATURETYPENO = null;

    /**
     * @var string $SZCOLOR
     */
    protected $SZCOLOR = null;

    /**
     * @var string $SZFEATURENAME
     */
    protected $SZFEATURENAME = null;

    /**
     * @var string $SZFEATUREVALUE
     */
    protected $SZFEATUREVALUE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISVARIANTRELATED()
    {
      return $this->BISVARIANTRELATED;
    }

    /**
     * @param float $BISVARIANTRELATED
     * @return \Axess\Dci4Wtp\D4WTPWAREFEATURES
     */
    public function setBISVARIANTRELATED($BISVARIANTRELATED)
    {
      $this->BISVARIANTRELATED = $BISVARIANTRELATED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFEATURETYPENO()
    {
      return $this->NFEATURETYPENO;
    }

    /**
     * @param float $NFEATURETYPENO
     * @return \Axess\Dci4Wtp\D4WTPWAREFEATURES
     */
    public function setNFEATURETYPENO($NFEATURETYPENO)
    {
      $this->NFEATURETYPENO = $NFEATURETYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOLOR()
    {
      return $this->SZCOLOR;
    }

    /**
     * @param string $SZCOLOR
     * @return \Axess\Dci4Wtp\D4WTPWAREFEATURES
     */
    public function setSZCOLOR($SZCOLOR)
    {
      $this->SZCOLOR = $SZCOLOR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFEATURENAME()
    {
      return $this->SZFEATURENAME;
    }

    /**
     * @param string $SZFEATURENAME
     * @return \Axess\Dci4Wtp\D4WTPWAREFEATURES
     */
    public function setSZFEATURENAME($SZFEATURENAME)
    {
      $this->SZFEATURENAME = $SZFEATURENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFEATUREVALUE()
    {
      return $this->SZFEATUREVALUE;
    }

    /**
     * @param string $SZFEATUREVALUE
     * @return \Axess\Dci4Wtp\D4WTPWAREFEATURES
     */
    public function setSZFEATUREVALUE($SZFEATUREVALUE)
    {
      $this->SZFEATUREVALUE = $SZFEATUREVALUE;
      return $this;
    }

}
