<?php

namespace Axess\Dci4Wtp;

class createPOEBookingWithManualCertificateResponse
{

    /**
     * @var WTPCreatePOEBookingWithmanualCertResult $createPOEBookingWithManualCertificateResult
     */
    protected $createPOEBookingWithManualCertificateResult = null;

    /**
     * @param WTPCreatePOEBookingWithmanualCertResult $createPOEBookingWithManualCertificateResult
     */
    public function __construct($createPOEBookingWithManualCertificateResult)
    {
      $this->createPOEBookingWithManualCertificateResult = $createPOEBookingWithManualCertificateResult;
    }

    /**
     * @return WTPCreatePOEBookingWithmanualCertResult
     */
    public function getCreatePOEBookingWithManualCertificateResult()
    {
      return $this->createPOEBookingWithManualCertificateResult;
    }

    /**
     * @param WTPCreatePOEBookingWithmanualCertResult $createPOEBookingWithManualCertificateResult
     * @return \Axess\Dci4Wtp\createPOEBookingWithManualCertificateResponse
     */
    public function setCreatePOEBookingWithManualCertificateResult($createPOEBookingWithManualCertificateResult)
    {
      $this->createPOEBookingWithManualCertificateResult = $createPOEBookingWithManualCertificateResult;
      return $this;
    }

}
