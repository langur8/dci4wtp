<?php

namespace Axess\Dci4Wtp;

class eMoneyPayTransaction
{

    /**
     * @var D4WTPEMONEYPAYTRANSACTIONREQ $i_ctEmoneyPayTransactionReq
     */
    protected $i_ctEmoneyPayTransactionReq = null;

    /**
     * @param D4WTPEMONEYPAYTRANSACTIONREQ $i_ctEmoneyPayTransactionReq
     */
    public function __construct($i_ctEmoneyPayTransactionReq)
    {
      $this->i_ctEmoneyPayTransactionReq = $i_ctEmoneyPayTransactionReq;
    }

    /**
     * @return D4WTPEMONEYPAYTRANSACTIONREQ
     */
    public function getI_ctEmoneyPayTransactionReq()
    {
      return $this->i_ctEmoneyPayTransactionReq;
    }

    /**
     * @param D4WTPEMONEYPAYTRANSACTIONREQ $i_ctEmoneyPayTransactionReq
     * @return \Axess\Dci4Wtp\eMoneyPayTransaction
     */
    public function setI_ctEmoneyPayTransactionReq($i_ctEmoneyPayTransactionReq)
    {
      $this->i_ctEmoneyPayTransactionReq = $i_ctEmoneyPayTransactionReq;
      return $this;
    }

}
