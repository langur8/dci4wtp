<?php

namespace Axess\Dci4Wtp;

class cancelTicketResponse
{

    /**
     * @var D4WTPCANCELTICKETRESULT $cancelTicketResult
     */
    protected $cancelTicketResult = null;

    /**
     * @param D4WTPCANCELTICKETRESULT $cancelTicketResult
     */
    public function __construct($cancelTicketResult)
    {
      $this->cancelTicketResult = $cancelTicketResult;
    }

    /**
     * @return D4WTPCANCELTICKETRESULT
     */
    public function getCancelTicketResult()
    {
      return $this->cancelTicketResult;
    }

    /**
     * @param D4WTPCANCELTICKETRESULT $cancelTicketResult
     * @return \Axess\Dci4Wtp\cancelTicketResponse
     */
    public function setCancelTicketResult($cancelTicketResult)
    {
      $this->cancelTicketResult = $cancelTicketResult;
      return $this;
    }

}
