<?php

namespace Axess\Dci4Wtp;

class getCustomerProfile2Response
{

    /**
     * @var D4WTPCUSTOMERPROFILE2RESULT $getCustomerProfile2Result
     */
    protected $getCustomerProfile2Result = null;

    /**
     * @param D4WTPCUSTOMERPROFILE2RESULT $getCustomerProfile2Result
     */
    public function __construct($getCustomerProfile2Result)
    {
      $this->getCustomerProfile2Result = $getCustomerProfile2Result;
    }

    /**
     * @return D4WTPCUSTOMERPROFILE2RESULT
     */
    public function getGetCustomerProfile2Result()
    {
      return $this->getCustomerProfile2Result;
    }

    /**
     * @param D4WTPCUSTOMERPROFILE2RESULT $getCustomerProfile2Result
     * @return \Axess\Dci4Wtp\getCustomerProfile2Response
     */
    public function setGetCustomerProfile2Result($getCustomerProfile2Result)
    {
      $this->getCustomerProfile2Result = $getCustomerProfile2Result;
      return $this;
    }

}
