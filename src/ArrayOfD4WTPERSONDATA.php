<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPERSONDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPERSONDATA[] $D4WTPERSONDATA
     */
    protected $D4WTPERSONDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPERSONDATA[]
     */
    public function getD4WTPERSONDATA()
    {
      return $this->D4WTPERSONDATA;
    }

    /**
     * @param D4WTPERSONDATA[] $D4WTPERSONDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPERSONDATA
     */
    public function setD4WTPERSONDATA(array $D4WTPERSONDATA = null)
    {
      $this->D4WTPERSONDATA = $D4WTPERSONDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPERSONDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPERSONDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPERSONDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPERSONDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPERSONDATA[] = $value;
      } else {
        $this->D4WTPERSONDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPERSONDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPERSONDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPERSONDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPERSONDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPERSONDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPERSONDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPERSONDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPERSONDATA);
    }

}
