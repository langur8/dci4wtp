<?php

namespace Axess\Dci4Wtp;

class getTariffList6
{

    /**
     * @var D4WTPTARIFFLIST6REQUEST $i_ctTariffListReq
     */
    protected $i_ctTariffListReq = null;

    /**
     * @param D4WTPTARIFFLIST6REQUEST $i_ctTariffListReq
     */
    public function __construct($i_ctTariffListReq)
    {
      $this->i_ctTariffListReq = $i_ctTariffListReq;
    }

    /**
     * @return D4WTPTARIFFLIST6REQUEST
     */
    public function getI_ctTariffListReq()
    {
      return $this->i_ctTariffListReq;
    }

    /**
     * @param D4WTPTARIFFLIST6REQUEST $i_ctTariffListReq
     * @return \Axess\Dci4Wtp\getTariffList6
     */
    public function setI_ctTariffListReq($i_ctTariffListReq)
    {
      $this->i_ctTariffListReq = $i_ctTariffListReq;
      return $this;
    }

}
