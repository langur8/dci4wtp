<?php

namespace Axess\Dci4Wtp;

class getContingent4Response
{

    /**
     * @var D4WTPGETCONTINGENT4RESULT $getContingent4Result
     */
    protected $getContingent4Result = null;

    /**
     * @param D4WTPGETCONTINGENT4RESULT $getContingent4Result
     */
    public function __construct($getContingent4Result)
    {
      $this->getContingent4Result = $getContingent4Result;
    }

    /**
     * @return D4WTPGETCONTINGENT4RESULT
     */
    public function getGetContingent4Result()
    {
      return $this->getContingent4Result;
    }

    /**
     * @param D4WTPGETCONTINGENT4RESULT $getContingent4Result
     * @return \Axess\Dci4Wtp\getContingent4Response
     */
    public function setGetContingent4Result($getContingent4Result)
    {
      $this->getContingent4Result = $getContingent4Result;
      return $this;
    }

}
