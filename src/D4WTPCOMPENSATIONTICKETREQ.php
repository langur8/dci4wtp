<?php

namespace Axess\Dci4Wtp;

class D4WTPCOMPENSATIONTICKETREQ
{

    /**
     * @var ArrayOfD4WTPPCOMPTICKET $ACTTICKET
     */
    protected $ACTTICKET = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPCOMPTICKET
     */
    public function getACTTICKET()
    {
      return $this->ACTTICKET;
    }

    /**
     * @param ArrayOfD4WTPPCOMPTICKET $ACTTICKET
     * @return \Axess\Dci4Wtp\D4WTPCOMPENSATIONTICKETREQ
     */
    public function setACTTICKET($ACTTICKET)
    {
      $this->ACTTICKET = $ACTTICKET;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCOMPENSATIONTICKETREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
