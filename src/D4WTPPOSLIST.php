<?php

namespace Axess\Dci4Wtp;

class D4WTPPOSLIST
{

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPPOSLIST
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

}
