<?php

namespace Axess\Dci4Wtp;

class D4WTPCOMPANYCONDITION
{

    /**
     * @var float $BISVARIABLE
     */
    protected $BISVARIABLE = null;

    /**
     * @var float $NCONDITIONNR
     */
    protected $NCONDITIONNR = null;

    /**
     * @var float $NCONDITIONPROJNO
     */
    protected $NCONDITIONPROJNO = null;

    /**
     * @var float $NDISCOUNTEDTARIFF
     */
    protected $NDISCOUNTEDTARIFF = null;

    /**
     * @var float $NDISCOUNTNO
     */
    protected $NDISCOUNTNO = null;

    /**
     * @var float $NDISCOUNTPERCENT
     */
    protected $NDISCOUNTPERCENT = null;

    /**
     * @var float $NDISCOUNTSCALENO
     */
    protected $NDISCOUNTSCALENO = null;

    /**
     * @var string $SZCONDITIONCODE
     */
    protected $SZCONDITIONCODE = null;

    /**
     * @var string $SZCONDITIONNAME
     */
    protected $SZCONDITIONNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISVARIABLE()
    {
      return $this->BISVARIABLE;
    }

    /**
     * @param float $BISVARIABLE
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYCONDITION
     */
    public function setBISVARIABLE($BISVARIABLE)
    {
      $this->BISVARIABLE = $BISVARIABLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONDITIONNR()
    {
      return $this->NCONDITIONNR;
    }

    /**
     * @param float $NCONDITIONNR
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYCONDITION
     */
    public function setNCONDITIONNR($NCONDITIONNR)
    {
      $this->NCONDITIONNR = $NCONDITIONNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONDITIONPROJNO()
    {
      return $this->NCONDITIONPROJNO;
    }

    /**
     * @param float $NCONDITIONPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYCONDITION
     */
    public function setNCONDITIONPROJNO($NCONDITIONPROJNO)
    {
      $this->NCONDITIONPROJNO = $NCONDITIONPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDISCOUNTEDTARIFF()
    {
      return $this->NDISCOUNTEDTARIFF;
    }

    /**
     * @param float $NDISCOUNTEDTARIFF
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYCONDITION
     */
    public function setNDISCOUNTEDTARIFF($NDISCOUNTEDTARIFF)
    {
      $this->NDISCOUNTEDTARIFF = $NDISCOUNTEDTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDISCOUNTNO()
    {
      return $this->NDISCOUNTNO;
    }

    /**
     * @param float $NDISCOUNTNO
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYCONDITION
     */
    public function setNDISCOUNTNO($NDISCOUNTNO)
    {
      $this->NDISCOUNTNO = $NDISCOUNTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDISCOUNTPERCENT()
    {
      return $this->NDISCOUNTPERCENT;
    }

    /**
     * @param float $NDISCOUNTPERCENT
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYCONDITION
     */
    public function setNDISCOUNTPERCENT($NDISCOUNTPERCENT)
    {
      $this->NDISCOUNTPERCENT = $NDISCOUNTPERCENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDISCOUNTSCALENO()
    {
      return $this->NDISCOUNTSCALENO;
    }

    /**
     * @param float $NDISCOUNTSCALENO
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYCONDITION
     */
    public function setNDISCOUNTSCALENO($NDISCOUNTSCALENO)
    {
      $this->NDISCOUNTSCALENO = $NDISCOUNTSCALENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCONDITIONCODE()
    {
      return $this->SZCONDITIONCODE;
    }

    /**
     * @param string $SZCONDITIONCODE
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYCONDITION
     */
    public function setSZCONDITIONCODE($SZCONDITIONCODE)
    {
      $this->SZCONDITIONCODE = $SZCONDITIONCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCONDITIONNAME()
    {
      return $this->SZCONDITIONNAME;
    }

    /**
     * @param string $SZCONDITIONNAME
     * @return \Axess\Dci4Wtp\D4WTPCOMPANYCONDITION
     */
    public function setSZCONDITIONNAME($SZCONDITIONNAME)
    {
      $this->SZCONDITIONNAME = $SZCONDITIONNAME;
      return $this;
    }

}
