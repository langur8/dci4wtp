<?php

namespace Axess\Dci4Wtp;

class manageWorkOrder
{

    /**
     * @var D4WTPMANAGEWORKORDERREQUEST $i_ctManageWorkOrderReq
     */
    protected $i_ctManageWorkOrderReq = null;

    /**
     * @param D4WTPMANAGEWORKORDERREQUEST $i_ctManageWorkOrderReq
     */
    public function __construct($i_ctManageWorkOrderReq)
    {
      $this->i_ctManageWorkOrderReq = $i_ctManageWorkOrderReq;
    }

    /**
     * @return D4WTPMANAGEWORKORDERREQUEST
     */
    public function getI_ctManageWorkOrderReq()
    {
      return $this->i_ctManageWorkOrderReq;
    }

    /**
     * @param D4WTPMANAGEWORKORDERREQUEST $i_ctManageWorkOrderReq
     * @return \Axess\Dci4Wtp\manageWorkOrder
     */
    public function setI_ctManageWorkOrderReq($i_ctManageWorkOrderReq)
    {
      $this->i_ctManageWorkOrderReq = $i_ctManageWorkOrderReq;
      return $this;
    }

}
