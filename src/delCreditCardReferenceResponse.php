<?php

namespace Axess\Dci4Wtp;

class delCreditCardReferenceResponse
{

    /**
     * @var D4WTPRESULT $delCreditCardReferenceResult
     */
    protected $delCreditCardReferenceResult = null;

    /**
     * @param D4WTPRESULT $delCreditCardReferenceResult
     */
    public function __construct($delCreditCardReferenceResult)
    {
      $this->delCreditCardReferenceResult = $delCreditCardReferenceResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getDelCreditCardReferenceResult()
    {
      return $this->delCreditCardReferenceResult;
    }

    /**
     * @param D4WTPRESULT $delCreditCardReferenceResult
     * @return \Axess\Dci4Wtp\delCreditCardReferenceResponse
     */
    public function setDelCreditCardReferenceResult($delCreditCardReferenceResult)
    {
      $this->delCreditCardReferenceResult = $delCreditCardReferenceResult;
      return $this;
    }

}
