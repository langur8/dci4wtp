<?php

namespace Axess\Dci4Wtp;

class D4WTPCONFIGRESULT
{

    /**
     * @var ArrayOfD4WTPCONFIG $ACTCONFIG
     */
    protected $ACTCONFIG = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCONFIG
     */
    public function getACTCONFIG()
    {
      return $this->ACTCONFIG;
    }

    /**
     * @param ArrayOfD4WTPCONFIG $ACTCONFIG
     * @return \Axess\Dci4Wtp\D4WTPCONFIGRESULT
     */
    public function setACTCONFIG($ACTCONFIG)
    {
      $this->ACTCONFIG = $ACTCONFIG;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCONFIGRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCONFIGRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
