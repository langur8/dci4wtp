<?php

namespace Axess\Dci4Wtp;

class D4WTPDECCONTINGENTREQUEST
{

    /**
     * @var float $NCOUNT
     */
    protected $NCOUNT = null;

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var float $NKASSANR
     */
    protected $NKASSANR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZDAY
     */
    protected $SZDAY = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCOUNT()
    {
      return $this->NCOUNT;
    }

    /**
     * @param float $NCOUNT
     * @return \Axess\Dci4Wtp\D4WTPDECCONTINGENTREQUEST
     */
    public function setNCOUNT($NCOUNT)
    {
      $this->NCOUNT = $NCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPDECCONTINGENTREQUEST
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKASSANR()
    {
      return $this->NKASSANR;
    }

    /**
     * @param float $NKASSANR
     * @return \Axess\Dci4Wtp\D4WTPDECCONTINGENTREQUEST
     */
    public function setNKASSANR($NKASSANR)
    {
      $this->NKASSANR = $NKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPDECCONTINGENTREQUEST
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPDECCONTINGENTREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDAY()
    {
      return $this->SZDAY;
    }

    /**
     * @param string $SZDAY
     * @return \Axess\Dci4Wtp\D4WTPDECCONTINGENTREQUEST
     */
    public function setSZDAY($SZDAY)
    {
      $this->SZDAY = $SZDAY;
      return $this;
    }

}
