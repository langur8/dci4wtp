<?php

namespace Axess\Dci4Wtp;

class ArrayOfDCI4WTPLISTSERIALNO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DCI4WTPLISTSERIALNO[] $DCI4WTPLISTSERIALNO
     */
    protected $DCI4WTPLISTSERIALNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DCI4WTPLISTSERIALNO[]
     */
    public function getDCI4WTPLISTSERIALNO()
    {
      return $this->DCI4WTPLISTSERIALNO;
    }

    /**
     * @param DCI4WTPLISTSERIALNO[] $DCI4WTPLISTSERIALNO
     * @return \Axess\Dci4Wtp\ArrayOfDCI4WTPLISTSERIALNO
     */
    public function setDCI4WTPLISTSERIALNO(array $DCI4WTPLISTSERIALNO = null)
    {
      $this->DCI4WTPLISTSERIALNO = $DCI4WTPLISTSERIALNO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DCI4WTPLISTSERIALNO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DCI4WTPLISTSERIALNO
     */
    public function offsetGet($offset)
    {
      return $this->DCI4WTPLISTSERIALNO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DCI4WTPLISTSERIALNO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DCI4WTPLISTSERIALNO[] = $value;
      } else {
        $this->DCI4WTPLISTSERIALNO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DCI4WTPLISTSERIALNO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DCI4WTPLISTSERIALNO Return the current element
     */
    public function current()
    {
      return current($this->DCI4WTPLISTSERIALNO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DCI4WTPLISTSERIALNO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DCI4WTPLISTSERIALNO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DCI4WTPLISTSERIALNO);
    }

    /**
     * Countable implementation
     *
     * @return DCI4WTPLISTSERIALNO Return count of elements
     */
    public function count()
    {
      return count($this->DCI4WTPLISTSERIALNO);
    }

}
