<?php

namespace Axess\Dci4Wtp;

class issueTicket9Response
{

    /**
     * @var D4WTPISSUETICKET6RESULT $issueTicket9Result
     */
    protected $issueTicket9Result = null;

    /**
     * @param D4WTPISSUETICKET6RESULT $issueTicket9Result
     */
    public function __construct($issueTicket9Result)
    {
      $this->issueTicket9Result = $issueTicket9Result;
    }

    /**
     * @return D4WTPISSUETICKET6RESULT
     */
    public function getIssueTicket9Result()
    {
      return $this->issueTicket9Result;
    }

    /**
     * @param D4WTPISSUETICKET6RESULT $issueTicket9Result
     * @return \Axess\Dci4Wtp\issueTicket9Response
     */
    public function setIssueTicket9Result($issueTicket9Result)
    {
      $this->issueTicket9Result = $issueTicket9Result;
      return $this;
    }

}
