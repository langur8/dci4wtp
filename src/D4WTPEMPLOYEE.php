<?php

namespace Axess\Dci4Wtp;

class D4WTPEMPLOYEE
{

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEE
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEE
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPEMPLOYEE
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

}
