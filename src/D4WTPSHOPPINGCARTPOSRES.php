<?php

namespace Axess\Dci4Wtp;

class D4WTPSHOPPINGCARTPOSRES
{

    /**
     * @var ArrayOfD4WTPSHOPPINGCARTPOSDETRES $ACTSHOPPINGCARTPOSDETRES
     */
    protected $ACTSHOPPINGCARTPOSDETRES = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPOSITIONNO
     */
    protected $NPOSITIONNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSHOPPINGCARTPOSDETRES
     */
    public function getACTSHOPPINGCARTPOSDETRES()
    {
      return $this->ACTSHOPPINGCARTPOSDETRES;
    }

    /**
     * @param ArrayOfD4WTPSHOPPINGCARTPOSDETRES $ACTSHOPPINGCARTPOSDETRES
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSRES
     */
    public function setACTSHOPPINGCARTPOSDETRES($ACTSHOPPINGCARTPOSDETRES)
    {
      $this->ACTSHOPPINGCARTPOSDETRES = $ACTSHOPPINGCARTPOSDETRES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSITIONNO()
    {
      return $this->NPOSITIONNO;
    }

    /**
     * @param float $NPOSITIONNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSRES
     */
    public function setNPOSITIONNO($NPOSITIONNO)
    {
      $this->NPOSITIONNO = $NPOSITIONNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
