<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGEPOSPRODUCT
{

    /**
     * @var float $NPACKNR
     */
    protected $NPACKNR = null;

    /**
     * @var float $NPACKPOS
     */
    protected $NPACKPOS = null;

    /**
     * @var float $NPACKTARIFFSHEETNR
     */
    protected $NPACKTARIFFSHEETNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPACKNR()
    {
      return $this->NPACKNR;
    }

    /**
     * @param float $NPACKNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSPRODUCT
     */
    public function setNPACKNR($NPACKNR)
    {
      $this->NPACKNR = $NPACKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKPOS()
    {
      return $this->NPACKPOS;
    }

    /**
     * @param float $NPACKPOS
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSPRODUCT
     */
    public function setNPACKPOS($NPACKPOS)
    {
      $this->NPACKPOS = $NPACKPOS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKTARIFFSHEETNR()
    {
      return $this->NPACKTARIFFSHEETNR;
    }

    /**
     * @param float $NPACKTARIFFSHEETNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSPRODUCT
     */
    public function setNPACKTARIFFSHEETNR($NPACKTARIFFSHEETNR)
    {
      $this->NPACKTARIFFSHEETNR = $NPACKTARIFFSHEETNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSPRODUCT
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPPACKAGEPOSPRODUCT
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
