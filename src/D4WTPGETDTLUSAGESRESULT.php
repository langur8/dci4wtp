<?php

namespace Axess\Dci4Wtp;

class D4WTPGETDTLUSAGESRESULT
{

    /**
     * @var ArrayOfD4WTPDTLUSAGESDATA $ACTDTLUSAGESDATA
     */
    protected $ACTDTLUSAGESDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPDTLUSAGESDATA
     */
    public function getACTDTLUSAGESDATA()
    {
      return $this->ACTDTLUSAGESDATA;
    }

    /**
     * @param ArrayOfD4WTPDTLUSAGESDATA $ACTDTLUSAGESDATA
     * @return \Axess\Dci4Wtp\D4WTPGETDTLUSAGESRESULT
     */
    public function setACTDTLUSAGESDATA($ACTDTLUSAGESDATA)
    {
      $this->ACTDTLUSAGESDATA = $ACTDTLUSAGESDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETDTLUSAGESRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETDTLUSAGESRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
