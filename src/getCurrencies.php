<?php

namespace Axess\Dci4Wtp;

class getCurrencies
{

    /**
     * @var D4WTPGETCURRENCIESREQUEST $i_ctGetCurrenciesReq
     */
    protected $i_ctGetCurrenciesReq = null;

    /**
     * @param D4WTPGETCURRENCIESREQUEST $i_ctGetCurrenciesReq
     */
    public function __construct($i_ctGetCurrenciesReq)
    {
      $this->i_ctGetCurrenciesReq = $i_ctGetCurrenciesReq;
    }

    /**
     * @return D4WTPGETCURRENCIESREQUEST
     */
    public function getI_ctGetCurrenciesReq()
    {
      return $this->i_ctGetCurrenciesReq;
    }

    /**
     * @param D4WTPGETCURRENCIESREQUEST $i_ctGetCurrenciesReq
     * @return \Axess\Dci4Wtp\getCurrencies
     */
    public function setI_ctGetCurrenciesReq($i_ctGetCurrenciesReq)
    {
      $this->i_ctGetCurrenciesReq = $i_ctGetCurrenciesReq;
      return $this;
    }

}
