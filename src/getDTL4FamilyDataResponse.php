<?php

namespace Axess\Dci4Wtp;

class getDTL4FamilyDataResponse
{

    /**
     * @var DTL4FAMCODINGRESULT $getDTL4FamilyDataResult
     */
    protected $getDTL4FamilyDataResult = null;

    /**
     * @param DTL4FAMCODINGRESULT $getDTL4FamilyDataResult
     */
    public function __construct($getDTL4FamilyDataResult)
    {
      $this->getDTL4FamilyDataResult = $getDTL4FamilyDataResult;
    }

    /**
     * @return DTL4FAMCODINGRESULT
     */
    public function getGetDTL4FamilyDataResult()
    {
      return $this->getDTL4FamilyDataResult;
    }

    /**
     * @param DTL4FAMCODINGRESULT $getDTL4FamilyDataResult
     * @return \Axess\Dci4Wtp\getDTL4FamilyDataResponse
     */
    public function setGetDTL4FamilyDataResult($getDTL4FamilyDataResult)
    {
      $this->getDTL4FamilyDataResult = $getDTL4FamilyDataResult;
      return $this;
    }

}
