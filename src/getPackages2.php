<?php

namespace Axess\Dci4Wtp;

class getPackages2
{

    /**
     * @var D4WTPPACKAGEREQUEST $i_ctPackageReq
     */
    protected $i_ctPackageReq = null;

    /**
     * @param D4WTPPACKAGEREQUEST $i_ctPackageReq
     */
    public function __construct($i_ctPackageReq)
    {
      $this->i_ctPackageReq = $i_ctPackageReq;
    }

    /**
     * @return D4WTPPACKAGEREQUEST
     */
    public function getI_ctPackageReq()
    {
      return $this->i_ctPackageReq;
    }

    /**
     * @param D4WTPPACKAGEREQUEST $i_ctPackageReq
     * @return \Axess\Dci4Wtp\getPackages2
     */
    public function setI_ctPackageReq($i_ctPackageReq)
    {
      $this->i_ctPackageReq = $i_ctPackageReq;
      return $this;
    }

}
