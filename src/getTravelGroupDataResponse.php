<?php

namespace Axess\Dci4Wtp;

class getTravelGroupDataResponse
{

    /**
     * @var D4WTPGETTRAVELGRPDATARESULT $getTravelGroupDataResult
     */
    protected $getTravelGroupDataResult = null;

    /**
     * @param D4WTPGETTRAVELGRPDATARESULT $getTravelGroupDataResult
     */
    public function __construct($getTravelGroupDataResult)
    {
      $this->getTravelGroupDataResult = $getTravelGroupDataResult;
    }

    /**
     * @return D4WTPGETTRAVELGRPDATARESULT
     */
    public function getGetTravelGroupDataResult()
    {
      return $this->getTravelGroupDataResult;
    }

    /**
     * @param D4WTPGETTRAVELGRPDATARESULT $getTravelGroupDataResult
     * @return \Axess\Dci4Wtp\getTravelGroupDataResponse
     */
    public function setGetTravelGroupDataResult($getTravelGroupDataResult)
    {
      $this->getTravelGroupDataResult = $getTravelGroupDataResult;
      return $this;
    }

}
