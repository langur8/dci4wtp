<?php

namespace Axess\Dci4Wtp;

class CancelReservationRequest
{

    /**
     * @var int $ReservationNo
     */
    protected $ReservationNo = null;

    /**
     * @var ArrayOfCancelReservationTicket $Tickets
     */
    protected $Tickets = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getReservationNo()
    {
      return $this->ReservationNo;
    }

    /**
     * @param int $ReservationNo
     * @return \Axess\Dci4Wtp\CancelReservationRequest
     */
    public function setReservationNo($ReservationNo)
    {
      $this->ReservationNo = $ReservationNo;
      return $this;
    }

    /**
     * @return ArrayOfCancelReservationTicket
     */
    public function getTickets()
    {
      return $this->Tickets;
    }

    /**
     * @param ArrayOfCancelReservationTicket $Tickets
     * @return \Axess\Dci4Wtp\CancelReservationRequest
     */
    public function setTickets($Tickets)
    {
      $this->Tickets = $Tickets;
      return $this;
    }

}
