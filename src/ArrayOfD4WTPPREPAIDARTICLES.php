<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPREPAIDARTICLES implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPREPAIDARTICLES[] $D4WTPPREPAIDARTICLES
     */
    protected $D4WTPPREPAIDARTICLES = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPREPAIDARTICLES[]
     */
    public function getD4WTPPREPAIDARTICLES()
    {
      return $this->D4WTPPREPAIDARTICLES;
    }

    /**
     * @param D4WTPPREPAIDARTICLES[] $D4WTPPREPAIDARTICLES
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPREPAIDARTICLES
     */
    public function setD4WTPPREPAIDARTICLES(array $D4WTPPREPAIDARTICLES = null)
    {
      $this->D4WTPPREPAIDARTICLES = $D4WTPPREPAIDARTICLES;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPREPAIDARTICLES[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPREPAIDARTICLES
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPREPAIDARTICLES[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPREPAIDARTICLES $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPREPAIDARTICLES[] = $value;
      } else {
        $this->D4WTPPREPAIDARTICLES[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPREPAIDARTICLES[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPREPAIDARTICLES Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPREPAIDARTICLES);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPREPAIDARTICLES);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPREPAIDARTICLES);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPREPAIDARTICLES);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPREPAIDARTICLES Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPREPAIDARTICLES);
    }

}
