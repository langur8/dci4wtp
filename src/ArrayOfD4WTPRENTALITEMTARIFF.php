<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPRENTALITEMTARIFF implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPRENTALITEMTARIFF[] $D4WTPRENTALITEMTARIFF
     */
    protected $D4WTPRENTALITEMTARIFF = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPRENTALITEMTARIFF[]
     */
    public function getD4WTPRENTALITEMTARIFF()
    {
      return $this->D4WTPRENTALITEMTARIFF;
    }

    /**
     * @param D4WTPRENTALITEMTARIFF[] $D4WTPRENTALITEMTARIFF
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPRENTALITEMTARIFF
     */
    public function setD4WTPRENTALITEMTARIFF(array $D4WTPRENTALITEMTARIFF = null)
    {
      $this->D4WTPRENTALITEMTARIFF = $D4WTPRENTALITEMTARIFF;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPRENTALITEMTARIFF[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPRENTALITEMTARIFF
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPRENTALITEMTARIFF[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPRENTALITEMTARIFF $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPRENTALITEMTARIFF[] = $value;
      } else {
        $this->D4WTPRENTALITEMTARIFF[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPRENTALITEMTARIFF[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPRENTALITEMTARIFF Return the current element
     */
    public function current()
    {
      return current($this->D4WTPRENTALITEMTARIFF);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPRENTALITEMTARIFF);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPRENTALITEMTARIFF);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPRENTALITEMTARIFF);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPRENTALITEMTARIFF Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPRENTALITEMTARIFF);
    }

}
