<?php

namespace Axess\Dci4Wtp;

class D4WTPPAYER2REQUEST
{

    /**
     * @var float $NFIRMENNR
     */
    protected $NFIRMENNR = null;

    /**
     * @var float $NFIRMENPOSNR
     */
    protected $NFIRMENPOSNR = null;

    /**
     * @var float $NFIRMENPROJNR
     */
    protected $NFIRMENPROJNR = null;

    /**
     * @var float $NPERSPERSNR
     */
    protected $NPERSPERSNR = null;

    /**
     * @var float $NPERSPOSNR
     */
    protected $NPERSPOSNR = null;

    /**
     * @var float $NPERSPROJNR
     */
    protected $NPERSPROJNR = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNR
     */
    protected $NTRANSNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNFIRMENNR()
    {
      return $this->NFIRMENNR;
    }

    /**
     * @param float $NFIRMENNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2REQUEST
     */
    public function setNFIRMENNR($NFIRMENNR)
    {
      $this->NFIRMENNR = $NFIRMENNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRMENPOSNR()
    {
      return $this->NFIRMENPOSNR;
    }

    /**
     * @param float $NFIRMENPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2REQUEST
     */
    public function setNFIRMENPOSNR($NFIRMENPOSNR)
    {
      $this->NFIRMENPOSNR = $NFIRMENPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNFIRMENPROJNR()
    {
      return $this->NFIRMENPROJNR;
    }

    /**
     * @param float $NFIRMENPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2REQUEST
     */
    public function setNFIRMENPROJNR($NFIRMENPROJNR)
    {
      $this->NFIRMENPROJNR = $NFIRMENPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPERSNR()
    {
      return $this->NPERSPERSNR;
    }

    /**
     * @param float $NPERSPERSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2REQUEST
     */
    public function setNPERSPERSNR($NPERSPERSNR)
    {
      $this->NPERSPERSNR = $NPERSPERSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNR()
    {
      return $this->NPERSPOSNR;
    }

    /**
     * @param float $NPERSPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2REQUEST
     */
    public function setNPERSPOSNR($NPERSPOSNR)
    {
      $this->NPERSPOSNR = $NPERSPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNR()
    {
      return $this->NPERSPROJNR;
    }

    /**
     * @param float $NPERSPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2REQUEST
     */
    public function setNPERSPROJNR($NPERSPROJNR)
    {
      $this->NPERSPROJNR = $NPERSPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2REQUEST
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2REQUEST
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPPAYER2REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNR()
    {
      return $this->NTRANSNR;
    }

    /**
     * @param float $NTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPPAYER2REQUEST
     */
    public function setNTRANSNR($NTRANSNR)
    {
      $this->NTRANSNR = $NTRANSNR;
      return $this;
    }

}
