<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPDAYOCCUPACY implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPDAYOCCUPACY[] $D4WTPDAYOCCUPACY
     */
    protected $D4WTPDAYOCCUPACY = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPDAYOCCUPACY[]
     */
    public function getD4WTPDAYOCCUPACY()
    {
      return $this->D4WTPDAYOCCUPACY;
    }

    /**
     * @param D4WTPDAYOCCUPACY[] $D4WTPDAYOCCUPACY
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPDAYOCCUPACY
     */
    public function setD4WTPDAYOCCUPACY(array $D4WTPDAYOCCUPACY = null)
    {
      $this->D4WTPDAYOCCUPACY = $D4WTPDAYOCCUPACY;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPDAYOCCUPACY[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPDAYOCCUPACY
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPDAYOCCUPACY[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPDAYOCCUPACY $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPDAYOCCUPACY[] = $value;
      } else {
        $this->D4WTPDAYOCCUPACY[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPDAYOCCUPACY[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPDAYOCCUPACY Return the current element
     */
    public function current()
    {
      return current($this->D4WTPDAYOCCUPACY);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPDAYOCCUPACY);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPDAYOCCUPACY);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPDAYOCCUPACY);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPDAYOCCUPACY Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPDAYOCCUPACY);
    }

}
