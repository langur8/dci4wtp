<?php

namespace Axess\Dci4Wtp;

class issueTicket8
{

    /**
     * @var D4WTPISSUETICKET8REQUEST $i_ctIssueTicketReq
     */
    protected $i_ctIssueTicketReq = null;

    /**
     * @param D4WTPISSUETICKET8REQUEST $i_ctIssueTicketReq
     */
    public function __construct($i_ctIssueTicketReq)
    {
      $this->i_ctIssueTicketReq = $i_ctIssueTicketReq;
    }

    /**
     * @return D4WTPISSUETICKET8REQUEST
     */
    public function getI_ctIssueTicketReq()
    {
      return $this->i_ctIssueTicketReq;
    }

    /**
     * @param D4WTPISSUETICKET8REQUEST $i_ctIssueTicketReq
     * @return \Axess\Dci4Wtp\issueTicket8
     */
    public function setI_ctIssueTicketReq($i_ctIssueTicketReq)
    {
      $this->i_ctIssueTicketReq = $i_ctIssueTicketReq;
      return $this;
    }

}
