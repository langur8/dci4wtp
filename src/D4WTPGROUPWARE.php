<?php

namespace Axess\Dci4Wtp;

class D4WTPGROUPWARE
{

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSORTNR
     */
    protected $NSORTNR = null;

    /**
     * @var float $NWARENO
     */
    protected $NWARENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPGROUPWARE
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPGROUPWARE
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNR()
    {
      return $this->NSORTNR;
    }

    /**
     * @param float $NSORTNR
     * @return \Axess\Dci4Wtp\D4WTPGROUPWARE
     */
    public function setNSORTNR($NSORTNR)
    {
      $this->NSORTNR = $NSORTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARENO()
    {
      return $this->NWARENO;
    }

    /**
     * @param float $NWARENO
     * @return \Axess\Dci4Wtp\D4WTPGROUPWARE
     */
    public function setNWARENO($NWARENO)
    {
      $this->NWARENO = $NWARENO;
      return $this;
    }

}
