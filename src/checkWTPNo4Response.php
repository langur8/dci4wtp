<?php

namespace Axess\Dci4Wtp;

class checkWTPNo4Response
{

    /**
     * @var D4WTPCHECKWTPNO3RESULT $checkWTPNo4Result
     */
    protected $checkWTPNo4Result = null;

    /**
     * @param D4WTPCHECKWTPNO3RESULT $checkWTPNo4Result
     */
    public function __construct($checkWTPNo4Result)
    {
      $this->checkWTPNo4Result = $checkWTPNo4Result;
    }

    /**
     * @return D4WTPCHECKWTPNO3RESULT
     */
    public function getCheckWTPNo4Result()
    {
      return $this->checkWTPNo4Result;
    }

    /**
     * @param D4WTPCHECKWTPNO3RESULT $checkWTPNo4Result
     * @return \Axess\Dci4Wtp\checkWTPNo4Response
     */
    public function setCheckWTPNo4Result($checkWTPNo4Result)
    {
      $this->checkWTPNo4Result = $checkWTPNo4Result;
      return $this;
    }

}
