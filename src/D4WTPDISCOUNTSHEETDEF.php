<?php

namespace Axess\Dci4Wtp;

class D4WTPDISCOUNTSHEETDEF
{

    /**
     * @var ArrayOfD4WTPDISCOUNTDEF $ACTDISCOUNTS
     */
    protected $ACTDISCOUNTS = null;

    /**
     * @var float $BISCONDITION
     */
    protected $BISCONDITION = null;

    /**
     * @var float $NDISCOUNTSHEETNO
     */
    protected $NDISCOUNTSHEETNO = null;

    /**
     * @var float $NDISCOUNTTYPENO
     */
    protected $NDISCOUNTTYPENO = null;

    /**
     * @var float $NORGANISATIONNO
     */
    protected $NORGANISATIONNO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var string $SZDISCOUNTTYPENAME
     */
    protected $SZDISCOUNTTYPENAME = null;

    /**
     * @var string $SZORGANISATIONNAME
     */
    protected $SZORGANISATIONNAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPDISCOUNTDEF
     */
    public function getACTDISCOUNTS()
    {
      return $this->ACTDISCOUNTS;
    }

    /**
     * @param ArrayOfD4WTPDISCOUNTDEF $ACTDISCOUNTS
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTSHEETDEF
     */
    public function setACTDISCOUNTS($ACTDISCOUNTS)
    {
      $this->ACTDISCOUNTS = $ACTDISCOUNTS;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISCONDITION()
    {
      return $this->BISCONDITION;
    }

    /**
     * @param float $BISCONDITION
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTSHEETDEF
     */
    public function setBISCONDITION($BISCONDITION)
    {
      $this->BISCONDITION = $BISCONDITION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDISCOUNTSHEETNO()
    {
      return $this->NDISCOUNTSHEETNO;
    }

    /**
     * @param float $NDISCOUNTSHEETNO
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTSHEETDEF
     */
    public function setNDISCOUNTSHEETNO($NDISCOUNTSHEETNO)
    {
      $this->NDISCOUNTSHEETNO = $NDISCOUNTSHEETNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDISCOUNTTYPENO()
    {
      return $this->NDISCOUNTTYPENO;
    }

    /**
     * @param float $NDISCOUNTTYPENO
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTSHEETDEF
     */
    public function setNDISCOUNTTYPENO($NDISCOUNTTYPENO)
    {
      $this->NDISCOUNTTYPENO = $NDISCOUNTTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORGANISATIONNO()
    {
      return $this->NORGANISATIONNO;
    }

    /**
     * @param float $NORGANISATIONNO
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTSHEETDEF
     */
    public function setNORGANISATIONNO($NORGANISATIONNO)
    {
      $this->NORGANISATIONNO = $NORGANISATIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTSHEETDEF
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDISCOUNTTYPENAME()
    {
      return $this->SZDISCOUNTTYPENAME;
    }

    /**
     * @param string $SZDISCOUNTTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTSHEETDEF
     */
    public function setSZDISCOUNTTYPENAME($SZDISCOUNTTYPENAME)
    {
      $this->SZDISCOUNTTYPENAME = $SZDISCOUNTTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZORGANISATIONNAME()
    {
      return $this->SZORGANISATIONNAME;
    }

    /**
     * @param string $SZORGANISATIONNAME
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTSHEETDEF
     */
    public function setSZORGANISATIONNAME($SZORGANISATIONNAME)
    {
      $this->SZORGANISATIONNAME = $SZORGANISATIONNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPDISCOUNTSHEETDEF
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

}
