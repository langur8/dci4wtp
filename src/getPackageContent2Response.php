<?php

namespace Axess\Dci4Wtp;

class getPackageContent2Response
{

    /**
     * @var D4WTPPACKAGECONTENT2RESULT $getPackageContent2Result
     */
    protected $getPackageContent2Result = null;

    /**
     * @param D4WTPPACKAGECONTENT2RESULT $getPackageContent2Result
     */
    public function __construct($getPackageContent2Result)
    {
      $this->getPackageContent2Result = $getPackageContent2Result;
    }

    /**
     * @return D4WTPPACKAGECONTENT2RESULT
     */
    public function getGetPackageContent2Result()
    {
      return $this->getPackageContent2Result;
    }

    /**
     * @param D4WTPPACKAGECONTENT2RESULT $getPackageContent2Result
     * @return \Axess\Dci4Wtp\getPackageContent2Response
     */
    public function setGetPackageContent2Result($getPackageContent2Result)
    {
      $this->getPackageContent2Result = $getPackageContent2Result;
      return $this;
    }

}
