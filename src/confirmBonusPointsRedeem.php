<?php

namespace Axess\Dci4Wtp;

class confirmBonusPointsRedeem
{

    /**
     * @var D4WTPCONFIRMBONUSPOINTSREQ $i_ctConfirmBonusPointsReq
     */
    protected $i_ctConfirmBonusPointsReq = null;

    /**
     * @param D4WTPCONFIRMBONUSPOINTSREQ $i_ctConfirmBonusPointsReq
     */
    public function __construct($i_ctConfirmBonusPointsReq)
    {
      $this->i_ctConfirmBonusPointsReq = $i_ctConfirmBonusPointsReq;
    }

    /**
     * @return D4WTPCONFIRMBONUSPOINTSREQ
     */
    public function getI_ctConfirmBonusPointsReq()
    {
      return $this->i_ctConfirmBonusPointsReq;
    }

    /**
     * @param D4WTPCONFIRMBONUSPOINTSREQ $i_ctConfirmBonusPointsReq
     * @return \Axess\Dci4Wtp\confirmBonusPointsRedeem
     */
    public function setI_ctConfirmBonusPointsReq($i_ctConfirmBonusPointsReq)
    {
      $this->i_ctConfirmBonusPointsReq = $i_ctConfirmBonusPointsReq;
      return $this;
    }

}
