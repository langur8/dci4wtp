<?php

namespace Axess\Dci4Wtp;

class issueTicket4Response
{

    /**
     * @var D4WTPISSUETICKET3RESULT $issueTicket4Result
     */
    protected $issueTicket4Result = null;

    /**
     * @param D4WTPISSUETICKET3RESULT $issueTicket4Result
     */
    public function __construct($issueTicket4Result)
    {
      $this->issueTicket4Result = $issueTicket4Result;
    }

    /**
     * @return D4WTPISSUETICKET3RESULT
     */
    public function getIssueTicket4Result()
    {
      return $this->issueTicket4Result;
    }

    /**
     * @param D4WTPISSUETICKET3RESULT $issueTicket4Result
     * @return \Axess\Dci4Wtp\issueTicket4Response
     */
    public function setIssueTicket4Result($issueTicket4Result)
    {
      $this->issueTicket4Result = $issueTicket4Result;
      return $this;
    }

}
