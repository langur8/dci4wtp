<?php

namespace Axess\Dci4Wtp;

class msgIssueTicket3
{

    /**
     * @var D4WTPMSGISSUETICKETREQUEST2 $i_ctMsgTicketReq
     */
    protected $i_ctMsgTicketReq = null;

    /**
     * @var float $i_bPrepaidTicket
     */
    protected $i_bPrepaidTicket = null;

    /**
     * @param D4WTPMSGISSUETICKETREQUEST2 $i_ctMsgTicketReq
     * @param float $i_bPrepaidTicket
     */
    public function __construct($i_ctMsgTicketReq, $i_bPrepaidTicket)
    {
      $this->i_ctMsgTicketReq = $i_ctMsgTicketReq;
      $this->i_bPrepaidTicket = $i_bPrepaidTicket;
    }

    /**
     * @return D4WTPMSGISSUETICKETREQUEST2
     */
    public function getI_ctMsgTicketReq()
    {
      return $this->i_ctMsgTicketReq;
    }

    /**
     * @param D4WTPMSGISSUETICKETREQUEST2 $i_ctMsgTicketReq
     * @return \Axess\Dci4Wtp\msgIssueTicket3
     */
    public function setI_ctMsgTicketReq($i_ctMsgTicketReq)
    {
      $this->i_ctMsgTicketReq = $i_ctMsgTicketReq;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_bPrepaidTicket()
    {
      return $this->i_bPrepaidTicket;
    }

    /**
     * @param float $i_bPrepaidTicket
     * @return \Axess\Dci4Wtp\msgIssueTicket3
     */
    public function setI_bPrepaidTicket($i_bPrepaidTicket)
    {
      $this->i_bPrepaidTicket = $i_bPrepaidTicket;
      return $this;
    }

}
