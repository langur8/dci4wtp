<?php

namespace Axess\Dci4Wtp;

class D4WTPCHKSKICLUBMBRRESULT
{

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZADDRESS
     */
    protected $SZADDRESS = null;

    /**
     * @var string $SZAPPELLATION
     */
    protected $SZAPPELLATION = null;

    /**
     * @var string $SZBIRTHDAY
     */
    protected $SZBIRTHDAY = null;

    /**
     * @var string $SZCOUNTRYINDICATOR
     */
    protected $SZCOUNTRYINDICATOR = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZFAXNO
     */
    protected $SZFAXNO = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZGENDER
     */
    protected $SZGENDER = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZLOCATION
     */
    protected $SZLOCATION = null;

    /**
     * @var string $SZMOBILENO
     */
    protected $SZMOBILENO = null;

    /**
     * @var string $SZPHONENO
     */
    protected $SZPHONENO = null;

    /**
     * @var string $SZSKICLUBMEMBERID
     */
    protected $SZSKICLUBMEMBERID = null;

    /**
     * @var string $SZTITLE
     */
    protected $SZTITLE = null;

    /**
     * @var string $SZWTPLOGINID
     */
    protected $SZWTPLOGINID = null;

    /**
     * @var string $SZWTPPASSWORD
     */
    protected $SZWTPPASSWORD = null;

    /**
     * @var string $SZWTPUSERNAME
     */
    protected $SZWTPUSERNAME = null;

    /**
     * @var string $SZZIPCODE
     */
    protected $SZZIPCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZADDRESS()
    {
      return $this->SZADDRESS;
    }

    /**
     * @param string $SZADDRESS
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZADDRESS($SZADDRESS)
    {
      $this->SZADDRESS = $SZADDRESS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAPPELLATION()
    {
      return $this->SZAPPELLATION;
    }

    /**
     * @param string $SZAPPELLATION
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZAPPELLATION($SZAPPELLATION)
    {
      $this->SZAPPELLATION = $SZAPPELLATION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBIRTHDAY()
    {
      return $this->SZBIRTHDAY;
    }

    /**
     * @param string $SZBIRTHDAY
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZBIRTHDAY($SZBIRTHDAY)
    {
      $this->SZBIRTHDAY = $SZBIRTHDAY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYINDICATOR()
    {
      return $this->SZCOUNTRYINDICATOR;
    }

    /**
     * @param string $SZCOUNTRYINDICATOR
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZCOUNTRYINDICATOR($SZCOUNTRYINDICATOR)
    {
      $this->SZCOUNTRYINDICATOR = $SZCOUNTRYINDICATOR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFAXNO()
    {
      return $this->SZFAXNO;
    }

    /**
     * @param string $SZFAXNO
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZFAXNO($SZFAXNO)
    {
      $this->SZFAXNO = $SZFAXNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGENDER()
    {
      return $this->SZGENDER;
    }

    /**
     * @param string $SZGENDER
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZGENDER($SZGENDER)
    {
      $this->SZGENDER = $SZGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLOCATION()
    {
      return $this->SZLOCATION;
    }

    /**
     * @param string $SZLOCATION
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZLOCATION($SZLOCATION)
    {
      $this->SZLOCATION = $SZLOCATION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMOBILENO()
    {
      return $this->SZMOBILENO;
    }

    /**
     * @param string $SZMOBILENO
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZMOBILENO($SZMOBILENO)
    {
      $this->SZMOBILENO = $SZMOBILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPHONENO()
    {
      return $this->SZPHONENO;
    }

    /**
     * @param string $SZPHONENO
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZPHONENO($SZPHONENO)
    {
      $this->SZPHONENO = $SZPHONENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSKICLUBMEMBERID()
    {
      return $this->SZSKICLUBMEMBERID;
    }

    /**
     * @param string $SZSKICLUBMEMBERID
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZSKICLUBMEMBERID($SZSKICLUBMEMBERID)
    {
      $this->SZSKICLUBMEMBERID = $SZSKICLUBMEMBERID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTITLE()
    {
      return $this->SZTITLE;
    }

    /**
     * @param string $SZTITLE
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZTITLE($SZTITLE)
    {
      $this->SZTITLE = $SZTITLE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPLOGINID()
    {
      return $this->SZWTPLOGINID;
    }

    /**
     * @param string $SZWTPLOGINID
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZWTPLOGINID($SZWTPLOGINID)
    {
      $this->SZWTPLOGINID = $SZWTPLOGINID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPPASSWORD()
    {
      return $this->SZWTPPASSWORD;
    }

    /**
     * @param string $SZWTPPASSWORD
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZWTPPASSWORD($SZWTPPASSWORD)
    {
      $this->SZWTPPASSWORD = $SZWTPPASSWORD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPUSERNAME()
    {
      return $this->SZWTPUSERNAME;
    }

    /**
     * @param string $SZWTPUSERNAME
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZWTPUSERNAME($SZWTPUSERNAME)
    {
      $this->SZWTPUSERNAME = $SZWTPUSERNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZZIPCODE()
    {
      return $this->SZZIPCODE;
    }

    /**
     * @param string $SZZIPCODE
     * @return \Axess\Dci4Wtp\D4WTPCHKSKICLUBMBRRESULT
     */
    public function setSZZIPCODE($SZZIPCODE)
    {
      $this->SZZIPCODE = $SZZIPCODE;
      return $this;
    }

}
