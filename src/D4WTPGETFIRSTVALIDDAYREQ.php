<?php

namespace Axess\Dci4Wtp;

class D4WTPGETFIRSTVALIDDAYREQ
{

    /**
     * @var ArrayOfD4WTPPACKAGE $ACTWTPPACKAGE
     */
    protected $ACTWTPPACKAGE = null;

    /**
     * @var ArrayOfD4WTPPRODUCT $ACTWTPPRODUCT
     */
    protected $ACTWTPPRODUCT = null;

    /**
     * @var float $NAMOUNTPLACES
     */
    protected $NAMOUNTPLACES = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZENDDATE
     */
    protected $SZENDDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPACKAGE
     */
    public function getACTWTPPACKAGE()
    {
      return $this->ACTWTPPACKAGE;
    }

    /**
     * @param ArrayOfD4WTPPACKAGE $ACTWTPPACKAGE
     * @return \Axess\Dci4Wtp\D4WTPGETFIRSTVALIDDAYREQ
     */
    public function setACTWTPPACKAGE($ACTWTPPACKAGE)
    {
      $this->ACTWTPPACKAGE = $ACTWTPPACKAGE;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPPRODUCT
     */
    public function getACTWTPPRODUCT()
    {
      return $this->ACTWTPPRODUCT;
    }

    /**
     * @param ArrayOfD4WTPPRODUCT $ACTWTPPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPGETFIRSTVALIDDAYREQ
     */
    public function setACTWTPPRODUCT($ACTWTPPRODUCT)
    {
      $this->ACTWTPPRODUCT = $ACTWTPPRODUCT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAMOUNTPLACES()
    {
      return $this->NAMOUNTPLACES;
    }

    /**
     * @param float $NAMOUNTPLACES
     * @return \Axess\Dci4Wtp\D4WTPGETFIRSTVALIDDAYREQ
     */
    public function setNAMOUNTPLACES($NAMOUNTPLACES)
    {
      $this->NAMOUNTPLACES = $NAMOUNTPLACES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETFIRSTVALIDDAYREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPGETFIRSTVALIDDAYREQ
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZENDDATE()
    {
      return $this->SZENDDATE;
    }

    /**
     * @param string $SZENDDATE
     * @return \Axess\Dci4Wtp\D4WTPGETFIRSTVALIDDAYREQ
     */
    public function setSZENDDATE($SZENDDATE)
    {
      $this->SZENDDATE = $SZENDDATE;
      return $this;
    }

}
