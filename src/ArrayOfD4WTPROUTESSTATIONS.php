<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPROUTESSTATIONS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPROUTESSTATIONS[] $D4WTPROUTESSTATIONS
     */
    protected $D4WTPROUTESSTATIONS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPROUTESSTATIONS[]
     */
    public function getD4WTPROUTESSTATIONS()
    {
      return $this->D4WTPROUTESSTATIONS;
    }

    /**
     * @param D4WTPROUTESSTATIONS[] $D4WTPROUTESSTATIONS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPROUTESSTATIONS
     */
    public function setD4WTPROUTESSTATIONS(array $D4WTPROUTESSTATIONS = null)
    {
      $this->D4WTPROUTESSTATIONS = $D4WTPROUTESSTATIONS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPROUTESSTATIONS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPROUTESSTATIONS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPROUTESSTATIONS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPROUTESSTATIONS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPROUTESSTATIONS[] = $value;
      } else {
        $this->D4WTPROUTESSTATIONS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPROUTESSTATIONS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPROUTESSTATIONS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPROUTESSTATIONS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPROUTESSTATIONS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPROUTESSTATIONS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPROUTESSTATIONS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPROUTESSTATIONS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPROUTESSTATIONS);
    }

}
