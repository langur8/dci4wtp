<?php

namespace Axess\Dci4Wtp;

class getTaxResponse
{

    /**
     * @var D4WTPGETTAXRES $getTaxResult
     */
    protected $getTaxResult = null;

    /**
     * @param D4WTPGETTAXRES $getTaxResult
     */
    public function __construct($getTaxResult)
    {
      $this->getTaxResult = $getTaxResult;
    }

    /**
     * @return D4WTPGETTAXRES
     */
    public function getGetTaxResult()
    {
      return $this->getTaxResult;
    }

    /**
     * @param D4WTPGETTAXRES $getTaxResult
     * @return \Axess\Dci4Wtp\getTaxResponse
     */
    public function setGetTaxResult($getTaxResult)
    {
      $this->getTaxResult = $getTaxResult;
      return $this;
    }

}
