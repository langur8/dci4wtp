<?php

namespace Axess\Dci4Wtp;

class setPrepaidTcktPlusData
{

    /**
     * @var D4WTPSETPREPTCKTPLUSREQUEST $i_ctPrepTcktPlusReq
     */
    protected $i_ctPrepTcktPlusReq = null;

    /**
     * @param D4WTPSETPREPTCKTPLUSREQUEST $i_ctPrepTcktPlusReq
     */
    public function __construct($i_ctPrepTcktPlusReq)
    {
      $this->i_ctPrepTcktPlusReq = $i_ctPrepTcktPlusReq;
    }

    /**
     * @return D4WTPSETPREPTCKTPLUSREQUEST
     */
    public function getI_ctPrepTcktPlusReq()
    {
      return $this->i_ctPrepTcktPlusReq;
    }

    /**
     * @param D4WTPSETPREPTCKTPLUSREQUEST $i_ctPrepTcktPlusReq
     * @return \Axess\Dci4Wtp\setPrepaidTcktPlusData
     */
    public function setI_ctPrepTcktPlusReq($i_ctPrepTcktPlusReq)
    {
      $this->i_ctPrepTcktPlusReq = $i_ctPrepTcktPlusReq;
      return $this;
    }

}
