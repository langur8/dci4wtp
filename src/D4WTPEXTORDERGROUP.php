<?php

namespace Axess\Dci4Wtp;

class D4WTPEXTORDERGROUP
{

    /**
     * @var ArrayOfD4WTPTICKETSERIAL $ACTTICKETSERIAL
     */
    protected $ACTTICKETSERIAL = null;

    /**
     * @var string $SZGROUPID
     */
    protected $SZGROUPID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKETSERIAL
     */
    public function getACTTICKETSERIAL()
    {
      return $this->ACTTICKETSERIAL;
    }

    /**
     * @param ArrayOfD4WTPTICKETSERIAL $ACTTICKETSERIAL
     * @return \Axess\Dci4Wtp\D4WTPEXTORDERGROUP
     */
    public function setACTTICKETSERIAL($ACTTICKETSERIAL)
    {
      $this->ACTTICKETSERIAL = $ACTTICKETSERIAL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGROUPID()
    {
      return $this->SZGROUPID;
    }

    /**
     * @param string $SZGROUPID
     * @return \Axess\Dci4Wtp\D4WTPEXTORDERGROUP
     */
    public function setSZGROUPID($SZGROUPID)
    {
      $this->SZGROUPID = $SZGROUPID;
      return $this;
    }

}
