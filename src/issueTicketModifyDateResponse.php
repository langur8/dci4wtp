<?php

namespace Axess\Dci4Wtp;

class issueTicketModifyDateResponse
{

    /**
     * @var D4WTPISSUEMODIFYRES $issueTicketModifyDateResult
     */
    protected $issueTicketModifyDateResult = null;

    /**
     * @param D4WTPISSUEMODIFYRES $issueTicketModifyDateResult
     */
    public function __construct($issueTicketModifyDateResult)
    {
      $this->issueTicketModifyDateResult = $issueTicketModifyDateResult;
    }

    /**
     * @return D4WTPISSUEMODIFYRES
     */
    public function getIssueTicketModifyDateResult()
    {
      return $this->issueTicketModifyDateResult;
    }

    /**
     * @param D4WTPISSUEMODIFYRES $issueTicketModifyDateResult
     * @return \Axess\Dci4Wtp\issueTicketModifyDateResponse
     */
    public function setIssueTicketModifyDateResult($issueTicketModifyDateResult)
    {
      $this->issueTicketModifyDateResult = $issueTicketModifyDateResult;
      return $this;
    }

}
