<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPDTLUSAGESDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPDTLUSAGESDATA[] $D4WTPDTLUSAGESDATA
     */
    protected $D4WTPDTLUSAGESDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPDTLUSAGESDATA[]
     */
    public function getD4WTPDTLUSAGESDATA()
    {
      return $this->D4WTPDTLUSAGESDATA;
    }

    /**
     * @param D4WTPDTLUSAGESDATA[] $D4WTPDTLUSAGESDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPDTLUSAGESDATA
     */
    public function setD4WTPDTLUSAGESDATA(array $D4WTPDTLUSAGESDATA = null)
    {
      $this->D4WTPDTLUSAGESDATA = $D4WTPDTLUSAGESDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPDTLUSAGESDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPDTLUSAGESDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPDTLUSAGESDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPDTLUSAGESDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPDTLUSAGESDATA[] = $value;
      } else {
        $this->D4WTPDTLUSAGESDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPDTLUSAGESDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPDTLUSAGESDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPDTLUSAGESDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPDTLUSAGESDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPDTLUSAGESDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPDTLUSAGESDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPDTLUSAGESDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPDTLUSAGESDATA);
    }

}
