<?php

namespace Axess\Dci4Wtp;

class D4WTPEDITORDERPOSITION
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NORDERNO
     */
    protected $NORDERNO = null;

    /**
     * @var float $NPOSITIONNO
     */
    protected $NPOSITIONNO = null;

    /**
     * @var float $NSUBPOSITIONNO
     */
    protected $NSUBPOSITIONNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERPOSITION
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORDERNO()
    {
      return $this->NORDERNO;
    }

    /**
     * @param float $NORDERNO
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERPOSITION
     */
    public function setNORDERNO($NORDERNO)
    {
      $this->NORDERNO = $NORDERNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSITIONNO()
    {
      return $this->NPOSITIONNO;
    }

    /**
     * @param float $NPOSITIONNO
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERPOSITION
     */
    public function setNPOSITIONNO($NPOSITIONNO)
    {
      $this->NPOSITIONNO = $NPOSITIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBPOSITIONNO()
    {
      return $this->NSUBPOSITIONNO;
    }

    /**
     * @param float $NSUBPOSITIONNO
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERPOSITION
     */
    public function setNSUBPOSITIONNO($NSUBPOSITIONNO)
    {
      $this->NSUBPOSITIONNO = $NSUBPOSITIONNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERPOSITION
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
