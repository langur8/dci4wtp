<?php

namespace Axess\Dci4Wtp;

class addDTL4FamilyMember
{

    /**
     * @var D4WTPADDDTL4FAMILYMEMBERREQ $i_ctAddDTL4FamilyMemberReq
     */
    protected $i_ctAddDTL4FamilyMemberReq = null;

    /**
     * @param D4WTPADDDTL4FAMILYMEMBERREQ $i_ctAddDTL4FamilyMemberReq
     */
    public function __construct($i_ctAddDTL4FamilyMemberReq)
    {
      $this->i_ctAddDTL4FamilyMemberReq = $i_ctAddDTL4FamilyMemberReq;
    }

    /**
     * @return D4WTPADDDTL4FAMILYMEMBERREQ
     */
    public function getI_ctAddDTL4FamilyMemberReq()
    {
      return $this->i_ctAddDTL4FamilyMemberReq;
    }

    /**
     * @param D4WTPADDDTL4FAMILYMEMBERREQ $i_ctAddDTL4FamilyMemberReq
     * @return \Axess\Dci4Wtp\addDTL4FamilyMember
     */
    public function setI_ctAddDTL4FamilyMemberReq($i_ctAddDTL4FamilyMemberReq)
    {
      $this->i_ctAddDTL4FamilyMemberReq = $i_ctAddDTL4FamilyMemberReq;
      return $this;
    }

}
