<?php

namespace Axess\Dci4Wtp;

class D4WTPRENTALITEM2
{

    /**
     * @var float $NDEFAULTRETURNLOCARCNR
     */
    protected $NDEFAULTRETURNLOCARCNR = null;

    /**
     * @var float $NDEFAULTRETURNLOCNR
     */
    protected $NDEFAULTRETURNLOCNR = null;

    /**
     * @var float $NDEFAULTRETURNLOCPROJNR
     */
    protected $NDEFAULTRETURNLOCPROJNR = null;

    /**
     * @var float $NITEMTYPENR
     */
    protected $NITEMTYPENR = null;

    /**
     * @var float $NRENTALITEMNR
     */
    protected $NRENTALITEMNR = null;

    /**
     * @var float $NVATPERCENT
     */
    protected $NVATPERCENT = null;

    /**
     * @var string $SZDEFAULTRETURNLOCNAME
     */
    protected $SZDEFAULTRETURNLOCNAME = null;

    /**
     * @var string $SZDOBFROM
     */
    protected $SZDOBFROM = null;

    /**
     * @var string $SZDOBTO
     */
    protected $SZDOBTO = null;

    /**
     * @var string $SZITEMTYPENAME
     */
    protected $SZITEMTYPENAME = null;

    /**
     * @var string $SZMASKNAME
     */
    protected $SZMASKNAME = null;

    /**
     * @var string $SZMASKSHORTNAME
     */
    protected $SZMASKSHORTNAME = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSHORTNAME
     */
    protected $SZSHORTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNDEFAULTRETURNLOCARCNR()
    {
      return $this->NDEFAULTRETURNLOCARCNR;
    }

    /**
     * @param float $NDEFAULTRETURNLOCARCNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setNDEFAULTRETURNLOCARCNR($NDEFAULTRETURNLOCARCNR)
    {
      $this->NDEFAULTRETURNLOCARCNR = $NDEFAULTRETURNLOCARCNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDEFAULTRETURNLOCNR()
    {
      return $this->NDEFAULTRETURNLOCNR;
    }

    /**
     * @param float $NDEFAULTRETURNLOCNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setNDEFAULTRETURNLOCNR($NDEFAULTRETURNLOCNR)
    {
      $this->NDEFAULTRETURNLOCNR = $NDEFAULTRETURNLOCNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDEFAULTRETURNLOCPROJNR()
    {
      return $this->NDEFAULTRETURNLOCPROJNR;
    }

    /**
     * @param float $NDEFAULTRETURNLOCPROJNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setNDEFAULTRETURNLOCPROJNR($NDEFAULTRETURNLOCPROJNR)
    {
      $this->NDEFAULTRETURNLOCPROJNR = $NDEFAULTRETURNLOCPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNITEMTYPENR()
    {
      return $this->NITEMTYPENR;
    }

    /**
     * @param float $NITEMTYPENR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setNITEMTYPENR($NITEMTYPENR)
    {
      $this->NITEMTYPENR = $NITEMTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNR()
    {
      return $this->NRENTALITEMNR;
    }

    /**
     * @param float $NRENTALITEMNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setNRENTALITEMNR($NRENTALITEMNR)
    {
      $this->NRENTALITEMNR = $NRENTALITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATPERCENT()
    {
      return $this->NVATPERCENT;
    }

    /**
     * @param float $NVATPERCENT
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setNVATPERCENT($NVATPERCENT)
    {
      $this->NVATPERCENT = $NVATPERCENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDEFAULTRETURNLOCNAME()
    {
      return $this->SZDEFAULTRETURNLOCNAME;
    }

    /**
     * @param string $SZDEFAULTRETURNLOCNAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setSZDEFAULTRETURNLOCNAME($SZDEFAULTRETURNLOCNAME)
    {
      $this->SZDEFAULTRETURNLOCNAME = $SZDEFAULTRETURNLOCNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDOBFROM()
    {
      return $this->SZDOBFROM;
    }

    /**
     * @param string $SZDOBFROM
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setSZDOBFROM($SZDOBFROM)
    {
      $this->SZDOBFROM = $SZDOBFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDOBTO()
    {
      return $this->SZDOBTO;
    }

    /**
     * @param string $SZDOBTO
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setSZDOBTO($SZDOBTO)
    {
      $this->SZDOBTO = $SZDOBTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZITEMTYPENAME()
    {
      return $this->SZITEMTYPENAME;
    }

    /**
     * @param string $SZITEMTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setSZITEMTYPENAME($SZITEMTYPENAME)
    {
      $this->SZITEMTYPENAME = $SZITEMTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASKNAME()
    {
      return $this->SZMASKNAME;
    }

    /**
     * @param string $SZMASKNAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setSZMASKNAME($SZMASKNAME)
    {
      $this->SZMASKNAME = $SZMASKNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASKSHORTNAME()
    {
      return $this->SZMASKSHORTNAME;
    }

    /**
     * @param string $SZMASKSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setSZMASKSHORTNAME($SZMASKSHORTNAME)
    {
      $this->SZMASKSHORTNAME = $SZMASKSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHORTNAME()
    {
      return $this->SZSHORTNAME;
    }

    /**
     * @param string $SZSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEM2
     */
    public function setSZSHORTNAME($SZSHORTNAME)
    {
      $this->SZSHORTNAME = $SZSHORTNAME;
      return $this;
    }

}
