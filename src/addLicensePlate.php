<?php

namespace Axess\Dci4Wtp;

class addLicensePlate
{

    /**
     * @var D4WTPADDLICENSEPLATEREQ $i_addlicenseplatereq
     */
    protected $i_addlicenseplatereq = null;

    /**
     * @param D4WTPADDLICENSEPLATEREQ $i_addlicenseplatereq
     */
    public function __construct($i_addlicenseplatereq)
    {
      $this->i_addlicenseplatereq = $i_addlicenseplatereq;
    }

    /**
     * @return D4WTPADDLICENSEPLATEREQ
     */
    public function getI_addlicenseplatereq()
    {
      return $this->i_addlicenseplatereq;
    }

    /**
     * @param D4WTPADDLICENSEPLATEREQ $i_addlicenseplatereq
     * @return \Axess\Dci4Wtp\addLicensePlate
     */
    public function setI_addlicenseplatereq($i_addlicenseplatereq)
    {
      $this->i_addlicenseplatereq = $i_addlicenseplatereq;
      return $this;
    }

}
