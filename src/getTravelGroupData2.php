<?php

namespace Axess\Dci4Wtp;

class getTravelGroupData2
{

    /**
     * @var D4WTPGETTRAVELGRPDATAREQUEST $i_ctGetTravelGroupDataReq
     */
    protected $i_ctGetTravelGroupDataReq = null;

    /**
     * @param D4WTPGETTRAVELGRPDATAREQUEST $i_ctGetTravelGroupDataReq
     */
    public function __construct($i_ctGetTravelGroupDataReq)
    {
      $this->i_ctGetTravelGroupDataReq = $i_ctGetTravelGroupDataReq;
    }

    /**
     * @return D4WTPGETTRAVELGRPDATAREQUEST
     */
    public function getI_ctGetTravelGroupDataReq()
    {
      return $this->i_ctGetTravelGroupDataReq;
    }

    /**
     * @param D4WTPGETTRAVELGRPDATAREQUEST $i_ctGetTravelGroupDataReq
     * @return \Axess\Dci4Wtp\getTravelGroupData2
     */
    public function setI_ctGetTravelGroupDataReq($i_ctGetTravelGroupDataReq)
    {
      $this->i_ctGetTravelGroupDataReq = $i_ctGetTravelGroupDataReq;
      return $this;
    }

}
