<?php

namespace Axess\Dci4Wtp;

class getCashierRightsResponse
{

    /**
     * @var D4WTPCASHIERRIGHTSRESULT $getCashierRightsResult
     */
    protected $getCashierRightsResult = null;

    /**
     * @param D4WTPCASHIERRIGHTSRESULT $getCashierRightsResult
     */
    public function __construct($getCashierRightsResult)
    {
      $this->getCashierRightsResult = $getCashierRightsResult;
    }

    /**
     * @return D4WTPCASHIERRIGHTSRESULT
     */
    public function getGetCashierRightsResult()
    {
      return $this->getCashierRightsResult;
    }

    /**
     * @param D4WTPCASHIERRIGHTSRESULT $getCashierRightsResult
     * @return \Axess\Dci4Wtp\getCashierRightsResponse
     */
    public function setGetCashierRightsResult($getCashierRightsResult)
    {
      $this->getCashierRightsResult = $getCashierRightsResult;
      return $this;
    }

}
