<?php

namespace Axess\Dci4Wtp;

class D4WTPCONTINGENT5REQUEST
{

    /**
     * @var ArrayOfDATE $ACTDAYS
     */
    protected $ACTDAYS = null;

    /**
     * @var ArrayOfD4WTPSUBCONTINGENT $ACTSUBCONTINGENT
     */
    protected $ACTSUBCONTINGENT = null;

    /**
     * @var float $BATLEASTONEFLAG
     */
    protected $BATLEASTONEFLAG = null;

    /**
     * @var float $BIGNOREDEVICETYPE
     */
    protected $BIGNOREDEVICETYPE = null;

    /**
     * @var float $BIGNORENRRANGE
     */
    protected $BIGNORENRRANGE = null;

    /**
     * @var float $BIGNORESALESCHANNEL
     */
    protected $BIGNORESALESCHANNEL = null;

    /**
     * @var float $BRETURNFIRSTDAYONLY
     */
    protected $BRETURNFIRSTDAYONLY = null;

    /**
     * @var float $NAMOUNTPLACES
     */
    protected $NAMOUNTPLACES = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZFROMDATE
     */
    protected $SZFROMDATE = null;

    /**
     * @var string $SZRESERVATIONREFERENCE
     */
    protected $SZRESERVATIONREFERENCE = null;

    /**
     * @var string $SZTODATE
     */
    protected $SZTODATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfDATE
     */
    public function getACTDAYS()
    {
      return $this->ACTDAYS;
    }

    /**
     * @param ArrayOfDATE $ACTDAYS
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT5REQUEST
     */
    public function setACTDAYS($ACTDAYS)
    {
      $this->ACTDAYS = $ACTDAYS;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPSUBCONTINGENT
     */
    public function getACTSUBCONTINGENT()
    {
      return $this->ACTSUBCONTINGENT;
    }

    /**
     * @param ArrayOfD4WTPSUBCONTINGENT $ACTSUBCONTINGENT
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT5REQUEST
     */
    public function setACTSUBCONTINGENT($ACTSUBCONTINGENT)
    {
      $this->ACTSUBCONTINGENT = $ACTSUBCONTINGENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getBATLEASTONEFLAG()
    {
      return $this->BATLEASTONEFLAG;
    }

    /**
     * @param float $BATLEASTONEFLAG
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT5REQUEST
     */
    public function setBATLEASTONEFLAG($BATLEASTONEFLAG)
    {
      $this->BATLEASTONEFLAG = $BATLEASTONEFLAG;
      return $this;
    }

    /**
     * @return float
     */
    public function getBIGNOREDEVICETYPE()
    {
      return $this->BIGNOREDEVICETYPE;
    }

    /**
     * @param float $BIGNOREDEVICETYPE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT5REQUEST
     */
    public function setBIGNOREDEVICETYPE($BIGNOREDEVICETYPE)
    {
      $this->BIGNOREDEVICETYPE = $BIGNOREDEVICETYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBIGNORENRRANGE()
    {
      return $this->BIGNORENRRANGE;
    }

    /**
     * @param float $BIGNORENRRANGE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT5REQUEST
     */
    public function setBIGNORENRRANGE($BIGNORENRRANGE)
    {
      $this->BIGNORENRRANGE = $BIGNORENRRANGE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBIGNORESALESCHANNEL()
    {
      return $this->BIGNORESALESCHANNEL;
    }

    /**
     * @param float $BIGNORESALESCHANNEL
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT5REQUEST
     */
    public function setBIGNORESALESCHANNEL($BIGNORESALESCHANNEL)
    {
      $this->BIGNORESALESCHANNEL = $BIGNORESALESCHANNEL;
      return $this;
    }

    /**
     * @return float
     */
    public function getBRETURNFIRSTDAYONLY()
    {
      return $this->BRETURNFIRSTDAYONLY;
    }

    /**
     * @param float $BRETURNFIRSTDAYONLY
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT5REQUEST
     */
    public function setBRETURNFIRSTDAYONLY($BRETURNFIRSTDAYONLY)
    {
      $this->BRETURNFIRSTDAYONLY = $BRETURNFIRSTDAYONLY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAMOUNTPLACES()
    {
      return $this->NAMOUNTPLACES;
    }

    /**
     * @param float $NAMOUNTPLACES
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT5REQUEST
     */
    public function setNAMOUNTPLACES($NAMOUNTPLACES)
    {
      $this->NAMOUNTPLACES = $NAMOUNTPLACES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT5REQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT5REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFROMDATE()
    {
      return $this->SZFROMDATE;
    }

    /**
     * @param string $SZFROMDATE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT5REQUEST
     */
    public function setSZFROMDATE($SZFROMDATE)
    {
      $this->SZFROMDATE = $SZFROMDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRESERVATIONREFERENCE()
    {
      return $this->SZRESERVATIONREFERENCE;
    }

    /**
     * @param string $SZRESERVATIONREFERENCE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT5REQUEST
     */
    public function setSZRESERVATIONREFERENCE($SZRESERVATIONREFERENCE)
    {
      $this->SZRESERVATIONREFERENCE = $SZRESERVATIONREFERENCE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTODATE()
    {
      return $this->SZTODATE;
    }

    /**
     * @param string $SZTODATE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT5REQUEST
     */
    public function setSZTODATE($SZTODATE)
    {
      $this->SZTODATE = $SZTODATE;
      return $this;
    }

}
