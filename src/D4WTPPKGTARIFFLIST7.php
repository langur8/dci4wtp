<?php

namespace Axess\Dci4Wtp;

class D4WTPPKGTARIFFLIST7
{

    /**
     * @var ArrayOfD4WTPPKGTARIFFLISTDAY7 $ACTPKGTARIFFLISTDAY
     */
    protected $ACTPKGTARIFFLISTDAY = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPKGTARIFFLISTDAY7
     */
    public function getACTPKGTARIFFLISTDAY()
    {
      return $this->ACTPKGTARIFFLISTDAY;
    }

    /**
     * @param ArrayOfD4WTPPKGTARIFFLISTDAY7 $ACTPKGTARIFFLISTDAY
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST7
     */
    public function setACTPKGTARIFFLISTDAY($ACTPKGTARIFFLISTDAY)
    {
      $this->ACTPKGTARIFFLISTDAY = $ACTPKGTARIFFLISTDAY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST7
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
