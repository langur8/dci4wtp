<?php

namespace Axess\Dci4Wtp;

class getSalesTransactions
{

    /**
     * @var D4WTPGETSALESTRANSREQ $i_ctGetSalesTransReq
     */
    protected $i_ctGetSalesTransReq = null;

    /**
     * @param D4WTPGETSALESTRANSREQ $i_ctGetSalesTransReq
     */
    public function __construct($i_ctGetSalesTransReq)
    {
      $this->i_ctGetSalesTransReq = $i_ctGetSalesTransReq;
    }

    /**
     * @return D4WTPGETSALESTRANSREQ
     */
    public function getI_ctGetSalesTransReq()
    {
      return $this->i_ctGetSalesTransReq;
    }

    /**
     * @param D4WTPGETSALESTRANSREQ $i_ctGetSalesTransReq
     * @return \Axess\Dci4Wtp\getSalesTransactions
     */
    public function setI_ctGetSalesTransReq($i_ctGetSalesTransReq)
    {
      $this->i_ctGetSalesTransReq = $i_ctGetSalesTransReq;
      return $this;
    }

}
