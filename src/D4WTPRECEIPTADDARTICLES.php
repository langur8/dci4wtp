<?php

namespace Axess\Dci4Wtp;

class D4WTPRECEIPTADDARTICLES
{

    /**
     * @var float $NAMOUNT
     */
    protected $NAMOUNT = null;

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NBLOCKNO
     */
    protected $NBLOCKNO = null;

    /**
     * @var float $NGROSSTARIFF
     */
    protected $NGROSSTARIFF = null;

    /**
     * @var float $NNETTARIFF
     */
    protected $NNETTARIFF = null;

    /**
     * @var float $NREFBLOCKNO
     */
    protected $NREFBLOCKNO = null;

    /**
     * @var float $NVATRATE
     */
    protected $NVATRATE = null;

    /**
     * @var float $NVATVALUE
     */
    protected $NVATVALUE = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNAMOUNT()
    {
      return $this->NAMOUNT;
    }

    /**
     * @param float $NAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTADDARTICLES
     */
    public function setNAMOUNT($NAMOUNT)
    {
      $this->NAMOUNT = $NAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTADDARTICLES
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKNO()
    {
      return $this->NBLOCKNO;
    }

    /**
     * @param float $NBLOCKNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTADDARTICLES
     */
    public function setNBLOCKNO($NBLOCKNO)
    {
      $this->NBLOCKNO = $NBLOCKNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNGROSSTARIFF()
    {
      return $this->NGROSSTARIFF;
    }

    /**
     * @param float $NGROSSTARIFF
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTADDARTICLES
     */
    public function setNGROSSTARIFF($NGROSSTARIFF)
    {
      $this->NGROSSTARIFF = $NGROSSTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNETTARIFF()
    {
      return $this->NNETTARIFF;
    }

    /**
     * @param float $NNETTARIFF
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTADDARTICLES
     */
    public function setNNETTARIFF($NNETTARIFF)
    {
      $this->NNETTARIFF = $NNETTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNREFBLOCKNO()
    {
      return $this->NREFBLOCKNO;
    }

    /**
     * @param float $NREFBLOCKNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTADDARTICLES
     */
    public function setNREFBLOCKNO($NREFBLOCKNO)
    {
      $this->NREFBLOCKNO = $NREFBLOCKNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATRATE()
    {
      return $this->NVATRATE;
    }

    /**
     * @param float $NVATRATE
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTADDARTICLES
     */
    public function setNVATRATE($NVATRATE)
    {
      $this->NVATRATE = $NVATRATE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATVALUE()
    {
      return $this->NVATVALUE;
    }

    /**
     * @param float $NVATVALUE
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTADDARTICLES
     */
    public function setNVATVALUE($NVATVALUE)
    {
      $this->NVATVALUE = $NVATVALUE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTADDARTICLES
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
