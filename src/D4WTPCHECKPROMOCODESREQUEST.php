<?php

namespace Axess\Dci4Wtp;

class D4WTPCHECKPROMOCODESREQUEST
{

    /**
     * @var ArrayOfD4WTPCHECKPROMOCODES $ACTCHECKPROMOCODES
     */
    protected $ACTCHECKPROMOCODES = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZPERSBIRTHDATE
     */
    protected $SZPERSBIRTHDATE = null;

    /**
     * @var string $SZPERSFIRSTNAME
     */
    protected $SZPERSFIRSTNAME = null;

    /**
     * @var string $SZPERSLASTNAME
     */
    protected $SZPERSLASTNAME = null;

    /**
     * @var string $SZPROMOCODEREFERENCE
     */
    protected $SZPROMOCODEREFERENCE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCHECKPROMOCODES
     */
    public function getACTCHECKPROMOCODES()
    {
      return $this->ACTCHECKPROMOCODES;
    }

    /**
     * @param ArrayOfD4WTPCHECKPROMOCODES $ACTCHECKPROMOCODES
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODESREQUEST
     */
    public function setACTCHECKPROMOCODES($ACTCHECKPROMOCODES)
    {
      $this->ACTCHECKPROMOCODES = $ACTCHECKPROMOCODES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODESREQUEST
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODESREQUEST
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODESREQUEST
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODESREQUEST
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODESREQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODESREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODESREQUEST
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODESREQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSBIRTHDATE()
    {
      return $this->SZPERSBIRTHDATE;
    }

    /**
     * @param string $SZPERSBIRTHDATE
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODESREQUEST
     */
    public function setSZPERSBIRTHDATE($SZPERSBIRTHDATE)
    {
      $this->SZPERSBIRTHDATE = $SZPERSBIRTHDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSFIRSTNAME()
    {
      return $this->SZPERSFIRSTNAME;
    }

    /**
     * @param string $SZPERSFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODESREQUEST
     */
    public function setSZPERSFIRSTNAME($SZPERSFIRSTNAME)
    {
      $this->SZPERSFIRSTNAME = $SZPERSFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSLASTNAME()
    {
      return $this->SZPERSLASTNAME;
    }

    /**
     * @param string $SZPERSLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODESREQUEST
     */
    public function setSZPERSLASTNAME($SZPERSLASTNAME)
    {
      $this->SZPERSLASTNAME = $SZPERSLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPROMOCODEREFERENCE()
    {
      return $this->SZPROMOCODEREFERENCE;
    }

    /**
     * @param string $SZPROMOCODEREFERENCE
     * @return \Axess\Dci4Wtp\D4WTPCHECKPROMOCODESREQUEST
     */
    public function setSZPROMOCODEREFERENCE($SZPROMOCODEREFERENCE)
    {
      $this->SZPROMOCODEREFERENCE = $SZPROMOCODEREFERENCE;
      return $this;
    }

}
