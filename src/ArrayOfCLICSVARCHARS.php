<?php

namespace Axess\Dci4Wtp;

class ArrayOfCLICSVARCHARS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CLICSVARCHARS[] $CLICSVARCHARS
     */
    protected $CLICSVARCHARS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CLICSVARCHARS[]
     */
    public function getCLICSVARCHARS()
    {
      return $this->CLICSVARCHARS;
    }

    /**
     * @param CLICSVARCHARS[] $CLICSVARCHARS
     * @return \Axess\Dci4Wtp\ArrayOfCLICSVARCHARS
     */
    public function setCLICSVARCHARS(array $CLICSVARCHARS = null)
    {
      $this->CLICSVARCHARS = $CLICSVARCHARS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CLICSVARCHARS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CLICSVARCHARS
     */
    public function offsetGet($offset)
    {
      return $this->CLICSVARCHARS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CLICSVARCHARS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->CLICSVARCHARS[] = $value;
      } else {
        $this->CLICSVARCHARS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CLICSVARCHARS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CLICSVARCHARS Return the current element
     */
    public function current()
    {
      return current($this->CLICSVARCHARS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CLICSVARCHARS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CLICSVARCHARS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CLICSVARCHARS);
    }

    /**
     * Countable implementation
     *
     * @return CLICSVARCHARS Return count of elements
     */
    public function count()
    {
      return count($this->CLICSVARCHARS);
    }

}
