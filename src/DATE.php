<?php

namespace Axess\Dci4Wtp;

class DATE
{

    /**
     * @var string $SZDATE
     */
    protected $SZDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZDATE()
    {
      return $this->SZDATE;
    }

    /**
     * @param string $SZDATE
     * @return \Axess\Dci4Wtp\DATE
     */
    public function setSZDATE($SZDATE)
    {
      $this->SZDATE = $SZDATE;
      return $this;
    }

}
