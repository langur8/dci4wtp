<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPWTPDATAOCC implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPWTPDATAOCC[] $D4WTPWTPDATAOCC
     */
    protected $D4WTPWTPDATAOCC = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPWTPDATAOCC[]
     */
    public function getD4WTPWTPDATAOCC()
    {
      return $this->D4WTPWTPDATAOCC;
    }

    /**
     * @param D4WTPWTPDATAOCC[] $D4WTPWTPDATAOCC
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPWTPDATAOCC
     */
    public function setD4WTPWTPDATAOCC(array $D4WTPWTPDATAOCC = null)
    {
      $this->D4WTPWTPDATAOCC = $D4WTPWTPDATAOCC;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPWTPDATAOCC[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPWTPDATAOCC
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPWTPDATAOCC[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPWTPDATAOCC $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPWTPDATAOCC[] = $value;
      } else {
        $this->D4WTPWTPDATAOCC[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPWTPDATAOCC[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPWTPDATAOCC Return the current element
     */
    public function current()
    {
      return current($this->D4WTPWTPDATAOCC);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPWTPDATAOCC);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPWTPDATAOCC);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPWTPDATAOCC);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPWTPDATAOCC Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPWTPDATAOCC);
    }

}
