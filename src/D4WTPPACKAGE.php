<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGE
{

    /**
     * @var float $NPACKNR
     */
    protected $NPACKNR = null;

    /**
     * @var float $NPACKTARIFFSHEETNR
     */
    protected $NPACKTARIFFSHEETNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNPACKNR()
    {
      return $this->NPACKNR;
    }

    /**
     * @param float $NPACKNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE
     */
    public function setNPACKNR($NPACKNR)
    {
      $this->NPACKNR = $NPACKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKTARIFFSHEETNR()
    {
      return $this->NPACKTARIFFSHEETNR;
    }

    /**
     * @param float $NPACKTARIFFSHEETNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE
     */
    public function setNPACKTARIFFSHEETNR($NPACKTARIFFSHEETNR)
    {
      $this->NPACKTARIFFSHEETNR = $NPACKTARIFFSHEETNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPPACKAGE
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
