<?php

namespace Axess\Dci4Wtp;

class D4WTPADDCASHIER
{

    /**
     * @var ArrayOfD4WTPCASHIERRIGHTS $ACTCASHIERRIGHTS
     */
    protected $ACTCASHIERRIGHTS = null;

    /**
     * @var float $NCASHIERID
     */
    protected $NCASHIERID = null;

    /**
     * @var string $NCASHIERRANK
     */
    protected $NCASHIERRANK = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCASHIERRIGHTS
     */
    public function getACTCASHIERRIGHTS()
    {
      return $this->ACTCASHIERRIGHTS;
    }

    /**
     * @param ArrayOfD4WTPCASHIERRIGHTS $ACTCASHIERRIGHTS
     * @return \Axess\Dci4Wtp\D4WTPADDCASHIER
     */
    public function setACTCASHIERRIGHTS($ACTCASHIERRIGHTS)
    {
      $this->ACTCASHIERRIGHTS = $ACTCASHIERRIGHTS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCASHIERID()
    {
      return $this->NCASHIERID;
    }

    /**
     * @param float $NCASHIERID
     * @return \Axess\Dci4Wtp\D4WTPADDCASHIER
     */
    public function setNCASHIERID($NCASHIERID)
    {
      $this->NCASHIERID = $NCASHIERID;
      return $this;
    }

    /**
     * @return string
     */
    public function getNCASHIERRANK()
    {
      return $this->NCASHIERRANK;
    }

    /**
     * @param string $NCASHIERRANK
     * @return \Axess\Dci4Wtp\D4WTPADDCASHIER
     */
    public function setNCASHIERRANK($NCASHIERRANK)
    {
      $this->NCASHIERRANK = $NCASHIERRANK;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPADDCASHIER
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPADDCASHIER
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

}
