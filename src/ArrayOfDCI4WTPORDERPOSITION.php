<?php

namespace Axess\Dci4Wtp;

class ArrayOfDCI4WTPORDERPOSITION implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DCI4WTPORDERPOSITION[] $DCI4WTPORDERPOSITION
     */
    protected $DCI4WTPORDERPOSITION = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DCI4WTPORDERPOSITION[]
     */
    public function getDCI4WTPORDERPOSITION()
    {
      return $this->DCI4WTPORDERPOSITION;
    }

    /**
     * @param DCI4WTPORDERPOSITION[] $DCI4WTPORDERPOSITION
     * @return \Axess\Dci4Wtp\ArrayOfDCI4WTPORDERPOSITION
     */
    public function setDCI4WTPORDERPOSITION(array $DCI4WTPORDERPOSITION = null)
    {
      $this->DCI4WTPORDERPOSITION = $DCI4WTPORDERPOSITION;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DCI4WTPORDERPOSITION[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DCI4WTPORDERPOSITION
     */
    public function offsetGet($offset)
    {
      return $this->DCI4WTPORDERPOSITION[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DCI4WTPORDERPOSITION $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DCI4WTPORDERPOSITION[] = $value;
      } else {
        $this->DCI4WTPORDERPOSITION[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DCI4WTPORDERPOSITION[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DCI4WTPORDERPOSITION Return the current element
     */
    public function current()
    {
      return current($this->DCI4WTPORDERPOSITION);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DCI4WTPORDERPOSITION);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DCI4WTPORDERPOSITION);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DCI4WTPORDERPOSITION);
    }

    /**
     * Countable implementation
     *
     * @return DCI4WTPORDERPOSITION Return count of elements
     */
    public function count()
    {
      return count($this->DCI4WTPORDERPOSITION);
    }

}
