<?php

namespace Axess\Dci4Wtp;

class getArticleList4Response
{

    /**
     * @var D4WTPGETARTICLELIST4RESULT $getArticleList4Result
     */
    protected $getArticleList4Result = null;

    /**
     * @param D4WTPGETARTICLELIST4RESULT $getArticleList4Result
     */
    public function __construct($getArticleList4Result)
    {
      $this->getArticleList4Result = $getArticleList4Result;
    }

    /**
     * @return D4WTPGETARTICLELIST4RESULT
     */
    public function getGetArticleList4Result()
    {
      return $this->getArticleList4Result;
    }

    /**
     * @param D4WTPGETARTICLELIST4RESULT $getArticleList4Result
     * @return \Axess\Dci4Wtp\getArticleList4Response
     */
    public function setGetArticleList4Result($getArticleList4Result)
    {
      $this->getArticleList4Result = $getArticleList4Result;
      return $this;
    }

}
