<?php

namespace Axess\Dci4Wtp;

class getCompany3
{

    /**
     * @var D4WTPGETALLCOMPANYREQ $i_ctGetCompanyReq
     */
    protected $i_ctGetCompanyReq = null;

    /**
     * @param D4WTPGETALLCOMPANYREQ $i_ctGetCompanyReq
     */
    public function __construct($i_ctGetCompanyReq)
    {
      $this->i_ctGetCompanyReq = $i_ctGetCompanyReq;
    }

    /**
     * @return D4WTPGETALLCOMPANYREQ
     */
    public function getI_ctGetCompanyReq()
    {
      return $this->i_ctGetCompanyReq;
    }

    /**
     * @param D4WTPGETALLCOMPANYREQ $i_ctGetCompanyReq
     * @return \Axess\Dci4Wtp\getCompany3
     */
    public function setI_ctGetCompanyReq($i_ctGetCompanyReq)
    {
      $this->i_ctGetCompanyReq = $i_ctGetCompanyReq;
      return $this;
    }

}
