<?php

namespace Axess\Dci4Wtp;

class getPackageTariffs
{

    /**
     * @var D4WTPPACKAGETARIFFSREQUEST $i_ctPackTariffsReq
     */
    protected $i_ctPackTariffsReq = null;

    /**
     * @param D4WTPPACKAGETARIFFSREQUEST $i_ctPackTariffsReq
     */
    public function __construct($i_ctPackTariffsReq)
    {
      $this->i_ctPackTariffsReq = $i_ctPackTariffsReq;
    }

    /**
     * @return D4WTPPACKAGETARIFFSREQUEST
     */
    public function getI_ctPackTariffsReq()
    {
      return $this->i_ctPackTariffsReq;
    }

    /**
     * @param D4WTPPACKAGETARIFFSREQUEST $i_ctPackTariffsReq
     * @return \Axess\Dci4Wtp\getPackageTariffs
     */
    public function setI_ctPackTariffsReq($i_ctPackTariffsReq)
    {
      $this->i_ctPackTariffsReq = $i_ctPackTariffsReq;
      return $this;
    }

}
