<?php

namespace Axess\Dci4Wtp;

class D4WTPSUBCONTINGENTRES3
{

    /**
     * @var ArrayOfD4WTPTRAVELGROUPRES2 $ACTTRAVELGROUPRES
     */
    protected $ACTTRAVELGROUPRES = null;

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var float $NCONFIGLIMITNO
     */
    protected $NCONFIGLIMITNO = null;

    /**
     * @var float $NCONFIGNO
     */
    protected $NCONFIGNO = null;

    /**
     * @var float $NSLOTLFDNR
     */
    protected $NSLOTLFDNR = null;

    /**
     * @var float $NSUBCONTINGENTNO
     */
    protected $NSUBCONTINGENTNO = null;

    /**
     * @var float $NTICKETCOUNTFREE
     */
    protected $NTICKETCOUNTFREE = null;

    /**
     * @var float $NTICKETCOUNTRESERVED
     */
    protected $NTICKETCOUNTRESERVED = null;

    /**
     * @var float $NTICKETCOUNTTOTAL
     */
    protected $NTICKETCOUNTTOTAL = null;

    /**
     * @var string $SZCCOLOR
     */
    protected $SZCCOLOR = null;

    /**
     * @var string $SZCONFIGNAME
     */
    protected $SZCONFIGNAME = null;

    /**
     * @var string $SZENDTIME
     */
    protected $SZENDTIME = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSHORTNAME
     */
    protected $SZSHORTNAME = null;

    /**
     * @var string $SZSLOTDESC
     */
    protected $SZSLOTDESC = null;

    /**
     * @var string $SZSLOTNAME
     */
    protected $SZSLOTNAME = null;

    /**
     * @var string $SZSTARTTIME
     */
    protected $SZSTARTTIME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTRAVELGROUPRES2
     */
    public function getACTTRAVELGROUPRES()
    {
      return $this->ACTTRAVELGROUPRES;
    }

    /**
     * @param ArrayOfD4WTPTRAVELGROUPRES2 $ACTTRAVELGROUPRES
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setACTTRAVELGROUPRES($ACTTRAVELGROUPRES)
    {
      $this->ACTTRAVELGROUPRES = $ACTTRAVELGROUPRES;
      return $this;
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONFIGLIMITNO()
    {
      return $this->NCONFIGLIMITNO;
    }

    /**
     * @param float $NCONFIGLIMITNO
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setNCONFIGLIMITNO($NCONFIGLIMITNO)
    {
      $this->NCONFIGLIMITNO = $NCONFIGLIMITNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCONFIGNO()
    {
      return $this->NCONFIGNO;
    }

    /**
     * @param float $NCONFIGNO
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setNCONFIGNO($NCONFIGNO)
    {
      $this->NCONFIGNO = $NCONFIGNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSLOTLFDNR()
    {
      return $this->NSLOTLFDNR;
    }

    /**
     * @param float $NSLOTLFDNR
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setNSLOTLFDNR($NSLOTLFDNR)
    {
      $this->NSLOTLFDNR = $NSLOTLFDNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTINGENTNO()
    {
      return $this->NSUBCONTINGENTNO;
    }

    /**
     * @param float $NSUBCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setNSUBCONTINGENTNO($NSUBCONTINGENTNO)
    {
      $this->NSUBCONTINGENTNO = $NSUBCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETCOUNTFREE()
    {
      return $this->NTICKETCOUNTFREE;
    }

    /**
     * @param float $NTICKETCOUNTFREE
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setNTICKETCOUNTFREE($NTICKETCOUNTFREE)
    {
      $this->NTICKETCOUNTFREE = $NTICKETCOUNTFREE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETCOUNTRESERVED()
    {
      return $this->NTICKETCOUNTRESERVED;
    }

    /**
     * @param float $NTICKETCOUNTRESERVED
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setNTICKETCOUNTRESERVED($NTICKETCOUNTRESERVED)
    {
      $this->NTICKETCOUNTRESERVED = $NTICKETCOUNTRESERVED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETCOUNTTOTAL()
    {
      return $this->NTICKETCOUNTTOTAL;
    }

    /**
     * @param float $NTICKETCOUNTTOTAL
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setNTICKETCOUNTTOTAL($NTICKETCOUNTTOTAL)
    {
      $this->NTICKETCOUNTTOTAL = $NTICKETCOUNTTOTAL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCCOLOR()
    {
      return $this->SZCCOLOR;
    }

    /**
     * @param string $SZCCOLOR
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setSZCCOLOR($SZCCOLOR)
    {
      $this->SZCCOLOR = $SZCCOLOR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCONFIGNAME()
    {
      return $this->SZCONFIGNAME;
    }

    /**
     * @param string $SZCONFIGNAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setSZCONFIGNAME($SZCONFIGNAME)
    {
      $this->SZCONFIGNAME = $SZCONFIGNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZENDTIME()
    {
      return $this->SZENDTIME;
    }

    /**
     * @param string $SZENDTIME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setSZENDTIME($SZENDTIME)
    {
      $this->SZENDTIME = $SZENDTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHORTNAME()
    {
      return $this->SZSHORTNAME;
    }

    /**
     * @param string $SZSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setSZSHORTNAME($SZSHORTNAME)
    {
      $this->SZSHORTNAME = $SZSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSLOTDESC()
    {
      return $this->SZSLOTDESC;
    }

    /**
     * @param string $SZSLOTDESC
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setSZSLOTDESC($SZSLOTDESC)
    {
      $this->SZSLOTDESC = $SZSLOTDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSLOTNAME()
    {
      return $this->SZSLOTNAME;
    }

    /**
     * @param string $SZSLOTNAME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setSZSLOTNAME($SZSLOTNAME)
    {
      $this->SZSLOTNAME = $SZSLOTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTARTTIME()
    {
      return $this->SZSTARTTIME;
    }

    /**
     * @param string $SZSTARTTIME
     * @return \Axess\Dci4Wtp\D4WTPSUBCONTINGENTRES3
     */
    public function setSZSTARTTIME($SZSTARTTIME)
    {
      $this->SZSTARTTIME = $SZSTARTTIME;
      return $this;
    }

}
