<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPEXTORDERGROUP implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPEXTORDERGROUP[] $D4WTPEXTORDERGROUP
     */
    protected $D4WTPEXTORDERGROUP = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPEXTORDERGROUP[]
     */
    public function getD4WTPEXTORDERGROUP()
    {
      return $this->D4WTPEXTORDERGROUP;
    }

    /**
     * @param D4WTPEXTORDERGROUP[] $D4WTPEXTORDERGROUP
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPEXTORDERGROUP
     */
    public function setD4WTPEXTORDERGROUP(array $D4WTPEXTORDERGROUP = null)
    {
      $this->D4WTPEXTORDERGROUP = $D4WTPEXTORDERGROUP;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPEXTORDERGROUP[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPEXTORDERGROUP
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPEXTORDERGROUP[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPEXTORDERGROUP $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPEXTORDERGROUP[] = $value;
      } else {
        $this->D4WTPEXTORDERGROUP[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPEXTORDERGROUP[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPEXTORDERGROUP Return the current element
     */
    public function current()
    {
      return current($this->D4WTPEXTORDERGROUP);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPEXTORDERGROUP);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPEXTORDERGROUP);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPEXTORDERGROUP);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPEXTORDERGROUP Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPEXTORDERGROUP);
    }

}
