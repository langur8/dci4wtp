<?php

namespace Axess\Dci4Wtp;

class D4WTPGETREADERTRANS
{

    /**
     * @var string $DTUSAGE
     */
    protected $DTUSAGE = null;

    /**
     * @var float $NCPUNO
     */
    protected $NCPUNO = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NLFDCPUTRANSNO
     */
    protected $NLFDCPUTRANSNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NREADERNO
     */
    protected $NREADERNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NVERKPROJNO
     */
    protected $NVERKPROJNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZGATENAME
     */
    protected $SZGATENAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDTUSAGE()
    {
      return $this->DTUSAGE;
    }

    /**
     * @param string $DTUSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS
     */
    public function setDTUSAGE($DTUSAGE)
    {
      $this->DTUSAGE = $DTUSAGE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCPUNO()
    {
      return $this->NCPUNO;
    }

    /**
     * @param float $NCPUNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS
     */
    public function setNCPUNO($NCPUNO)
    {
      $this->NCPUNO = $NCPUNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDCPUTRANSNO()
    {
      return $this->NLFDCPUTRANSNO;
    }

    /**
     * @param float $NLFDCPUTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS
     */
    public function setNLFDCPUTRANSNO($NLFDCPUTRANSNO)
    {
      $this->NLFDCPUTRANSNO = $NLFDCPUTRANSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNREADERNO()
    {
      return $this->NREADERNO;
    }

    /**
     * @param float $NREADERNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS
     */
    public function setNREADERNO($NREADERNO)
    {
      $this->NREADERNO = $NREADERNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVERKPROJNO()
    {
      return $this->NVERKPROJNO;
    }

    /**
     * @param float $NVERKPROJNO
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS
     */
    public function setNVERKPROJNO($NVERKPROJNO)
    {
      $this->NVERKPROJNO = $NVERKPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGATENAME()
    {
      return $this->SZGATENAME;
    }

    /**
     * @param string $SZGATENAME
     * @return \Axess\Dci4Wtp\D4WTPGETREADERTRANS
     */
    public function setSZGATENAME($SZGATENAME)
    {
      $this->SZGATENAME = $SZGATENAME;
      return $this;
    }

}
