<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST5
{

    /**
     * @var ArrayOfD4WTPTARIFFLISTDAY5 $ACTTARIFFLISTDAY5
     */
    protected $ACTTARIFFLISTDAY5 = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLISTDAY5
     */
    public function getACTTARIFFLISTDAY5()
    {
      return $this->ACTTARIFFLISTDAY5;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLISTDAY5 $ACTTARIFFLISTDAY5
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST5
     */
    public function setACTTARIFFLISTDAY5($ACTTARIFFLISTDAY5)
    {
      $this->ACTTARIFFLISTDAY5 = $ACTTARIFFLISTDAY5;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST5
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
