<?php

namespace Axess\Dci4Wtp;

class D4WTPPRODUCTTYPES
{

    /**
     * @var ArrayOfD4WTPPRODUCTS $ACTPRODUCTS
     */
    protected $ACTPRODUCTS = null;

    /**
     * @var float $NPRODUCTTYPENO
     */
    protected $NPRODUCTTYPENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPRODUCTS
     */
    public function getACTPRODUCTS()
    {
      return $this->ACTPRODUCTS;
    }

    /**
     * @param ArrayOfD4WTPPRODUCTS $ACTPRODUCTS
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTTYPES
     */
    public function setACTPRODUCTS($ACTPRODUCTS)
    {
      $this->ACTPRODUCTS = $ACTPRODUCTS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPRODUCTTYPENO()
    {
      return $this->NPRODUCTTYPENO;
    }

    /**
     * @param float $NPRODUCTTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPRODUCTTYPES
     */
    public function setNPRODUCTTYPENO($NPRODUCTTYPENO)
    {
      $this->NPRODUCTTYPENO = $NPRODUCTTYPENO;
      return $this;
    }

}
