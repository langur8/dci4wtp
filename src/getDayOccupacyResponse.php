<?php

namespace Axess\Dci4Wtp;

class getDayOccupacyResponse
{

    /**
     * @var D4WTPGETDAYOCUPACYRESULT $getDayOccupacyResult
     */
    protected $getDayOccupacyResult = null;

    /**
     * @param D4WTPGETDAYOCUPACYRESULT $getDayOccupacyResult
     */
    public function __construct($getDayOccupacyResult)
    {
      $this->getDayOccupacyResult = $getDayOccupacyResult;
    }

    /**
     * @return D4WTPGETDAYOCUPACYRESULT
     */
    public function getGetDayOccupacyResult()
    {
      return $this->getDayOccupacyResult;
    }

    /**
     * @param D4WTPGETDAYOCUPACYRESULT $getDayOccupacyResult
     * @return \Axess\Dci4Wtp\getDayOccupacyResponse
     */
    public function setGetDayOccupacyResult($getDayOccupacyResult)
    {
      $this->getDayOccupacyResult = $getDayOccupacyResult;
      return $this;
    }

}
