<?php

namespace Axess\Dci4Wtp;

class checkSession
{

    /**
     * @var D4WTPCHECKSESSIONREQUEST $i_ctCheckSessionReq
     */
    protected $i_ctCheckSessionReq = null;

    /**
     * @param D4WTPCHECKSESSIONREQUEST $i_ctCheckSessionReq
     */
    public function __construct($i_ctCheckSessionReq)
    {
      $this->i_ctCheckSessionReq = $i_ctCheckSessionReq;
    }

    /**
     * @return D4WTPCHECKSESSIONREQUEST
     */
    public function getI_ctCheckSessionReq()
    {
      return $this->i_ctCheckSessionReq;
    }

    /**
     * @param D4WTPCHECKSESSIONREQUEST $i_ctCheckSessionReq
     * @return \Axess\Dci4Wtp\checkSession
     */
    public function setI_ctCheckSessionReq($i_ctCheckSessionReq)
    {
      $this->i_ctCheckSessionReq = $i_ctCheckSessionReq;
      return $this;
    }

}
