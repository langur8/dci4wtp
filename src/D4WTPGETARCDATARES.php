<?php

namespace Axess\Dci4Wtp;

class D4WTPGETARCDATARES
{

    /**
     * @var ArrayOfD4WTPDURATIONINFO $ACTDURATIONINFO
     */
    protected $ACTDURATIONINFO = null;

    /**
     * @var ArrayOfD4WTPLESSONINSTINFO $ACTLESSONINSTINFO
     */
    protected $ACTLESSONINSTINFO = null;

    /**
     * @var ArrayOfD4WTPLOCATIONINFO $ACTLOCATIONINFO
     */
    protected $ACTLOCATIONINFO = null;

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var WEEKSCALE $CTCLOSINGWEEKSCALE
     */
    protected $CTCLOSINGWEEKSCALE = null;

    /**
     * @var WEEKSCALE $CTOPENINGWEEKSCALE
     */
    protected $CTOPENINGWEEKSCALE = null;

    /**
     * @var float $NEDEKASSANR
     */
    protected $NEDEKASSANR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NGESNR
     */
    protected $NGESNR = null;

    /**
     * @var float $NNAMEDUSERID
     */
    protected $NNAMEDUSERID = null;

    /**
     * @var float $NNAMEDUSERPROJNR
     */
    protected $NNAMEDUSERPROJNR = null;

    /**
     * @var float $NNRKREISART
     */
    protected $NNRKREISART = null;

    /**
     * @var float $NRECEIPTGESNR
     */
    protected $NRECEIPTGESNR = null;

    /**
     * @var float $NRECEIPTGESPROJNR
     */
    protected $NRECEIPTGESPROJNR = null;

    /**
     * @var string $SZCOMPUTERNAME
     */
    protected $SZCOMPUTERNAME = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZDISPLAYNAME
     */
    protected $SZDISPLAYNAME = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSUBDOMAINNAME
     */
    protected $SZSUBDOMAINNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPDURATIONINFO
     */
    public function getACTDURATIONINFO()
    {
      return $this->ACTDURATIONINFO;
    }

    /**
     * @param ArrayOfD4WTPDURATIONINFO $ACTDURATIONINFO
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setACTDURATIONINFO($ACTDURATIONINFO)
    {
      $this->ACTDURATIONINFO = $ACTDURATIONINFO;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPLESSONINSTINFO
     */
    public function getACTLESSONINSTINFO()
    {
      return $this->ACTLESSONINSTINFO;
    }

    /**
     * @param ArrayOfD4WTPLESSONINSTINFO $ACTLESSONINSTINFO
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setACTLESSONINSTINFO($ACTLESSONINSTINFO)
    {
      $this->ACTLESSONINSTINFO = $ACTLESSONINSTINFO;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPLOCATIONINFO
     */
    public function getACTLOCATIONINFO()
    {
      return $this->ACTLOCATIONINFO;
    }

    /**
     * @param ArrayOfD4WTPLOCATIONINFO $ACTLOCATIONINFO
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setACTLOCATIONINFO($ACTLOCATIONINFO)
    {
      $this->ACTLOCATIONINFO = $ACTLOCATIONINFO;
      return $this;
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return WEEKSCALE
     */
    public function getCTCLOSINGWEEKSCALE()
    {
      return $this->CTCLOSINGWEEKSCALE;
    }

    /**
     * @param WEEKSCALE $CTCLOSINGWEEKSCALE
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setCTCLOSINGWEEKSCALE($CTCLOSINGWEEKSCALE)
    {
      $this->CTCLOSINGWEEKSCALE = $CTCLOSINGWEEKSCALE;
      return $this;
    }

    /**
     * @return WEEKSCALE
     */
    public function getCTOPENINGWEEKSCALE()
    {
      return $this->CTOPENINGWEEKSCALE;
    }

    /**
     * @param WEEKSCALE $CTOPENINGWEEKSCALE
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setCTOPENINGWEEKSCALE($CTOPENINGWEEKSCALE)
    {
      $this->CTOPENINGWEEKSCALE = $CTOPENINGWEEKSCALE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEDEKASSANR()
    {
      return $this->NEDEKASSANR;
    }

    /**
     * @param float $NEDEKASSANR
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setNEDEKASSANR($NEDEKASSANR)
    {
      $this->NEDEKASSANR = $NEDEKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNGESNR()
    {
      return $this->NGESNR;
    }

    /**
     * @param float $NGESNR
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setNGESNR($NGESNR)
    {
      $this->NGESNR = $NGESNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNAMEDUSERID()
    {
      return $this->NNAMEDUSERID;
    }

    /**
     * @param float $NNAMEDUSERID
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setNNAMEDUSERID($NNAMEDUSERID)
    {
      $this->NNAMEDUSERID = $NNAMEDUSERID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNAMEDUSERPROJNR()
    {
      return $this->NNAMEDUSERPROJNR;
    }

    /**
     * @param float $NNAMEDUSERPROJNR
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setNNAMEDUSERPROJNR($NNAMEDUSERPROJNR)
    {
      $this->NNAMEDUSERPROJNR = $NNAMEDUSERPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNRKREISART()
    {
      return $this->NNRKREISART;
    }

    /**
     * @param float $NNRKREISART
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setNNRKREISART($NNRKREISART)
    {
      $this->NNRKREISART = $NNRKREISART;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRECEIPTGESNR()
    {
      return $this->NRECEIPTGESNR;
    }

    /**
     * @param float $NRECEIPTGESNR
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setNRECEIPTGESNR($NRECEIPTGESNR)
    {
      $this->NRECEIPTGESNR = $NRECEIPTGESNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRECEIPTGESPROJNR()
    {
      return $this->NRECEIPTGESPROJNR;
    }

    /**
     * @param float $NRECEIPTGESPROJNR
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setNRECEIPTGESPROJNR($NRECEIPTGESPROJNR)
    {
      $this->NRECEIPTGESPROJNR = $NRECEIPTGESPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOMPUTERNAME()
    {
      return $this->SZCOMPUTERNAME;
    }

    /**
     * @param string $SZCOMPUTERNAME
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setSZCOMPUTERNAME($SZCOMPUTERNAME)
    {
      $this->SZCOMPUTERNAME = $SZCOMPUTERNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDISPLAYNAME()
    {
      return $this->SZDISPLAYNAME;
    }

    /**
     * @param string $SZDISPLAYNAME
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setSZDISPLAYNAME($SZDISPLAYNAME)
    {
      $this->SZDISPLAYNAME = $SZDISPLAYNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBDOMAINNAME()
    {
      return $this->SZSUBDOMAINNAME;
    }

    /**
     * @param string $SZSUBDOMAINNAME
     * @return \Axess\Dci4Wtp\D4WTPGETARCDATARES
     */
    public function setSZSUBDOMAINNAME($SZSUBDOMAINNAME)
    {
      $this->SZSUBDOMAINNAME = $SZSUBDOMAINNAME;
      return $this;
    }

}
