<?php

namespace Axess\Dci4Wtp;

class unBlockTicketBySerial
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var float $i_NPROJNR
     */
    protected $i_NPROJNR = null;

    /**
     * @var float $i_nPOSNr
     */
    protected $i_nPOSNr = null;

    /**
     * @var float $i_nSerialNr
     */
    protected $i_nSerialNr = null;

    /**
     * @var float $i_nUniCodeNr
     */
    protected $i_nUniCodeNr = null;

    /**
     * @var string $i_szValidDate
     */
    protected $i_szValidDate = null;

    /**
     * @param float $i_nSessionID
     * @param float $i_NPROJNR
     * @param float $i_nPOSNr
     * @param float $i_nSerialNr
     * @param float $i_nUniCodeNr
     * @param string $i_szValidDate
     */
    public function __construct($i_nSessionID, $i_NPROJNR, $i_nPOSNr, $i_nSerialNr, $i_nUniCodeNr, $i_szValidDate)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_NPROJNR = $i_NPROJNR;
      $this->i_nPOSNr = $i_nPOSNr;
      $this->i_nSerialNr = $i_nSerialNr;
      $this->i_nUniCodeNr = $i_nUniCodeNr;
      $this->i_szValidDate = $i_szValidDate;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\unBlockTicketBySerial
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_NPROJNR()
    {
      return $this->i_NPROJNR;
    }

    /**
     * @param float $i_NPROJNR
     * @return \Axess\Dci4Wtp\unBlockTicketBySerial
     */
    public function setI_NPROJNR($i_NPROJNR)
    {
      $this->i_NPROJNR = $i_NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nPOSNr()
    {
      return $this->i_nPOSNr;
    }

    /**
     * @param float $i_nPOSNr
     * @return \Axess\Dci4Wtp\unBlockTicketBySerial
     */
    public function setI_nPOSNr($i_nPOSNr)
    {
      $this->i_nPOSNr = $i_nPOSNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nSerialNr()
    {
      return $this->i_nSerialNr;
    }

    /**
     * @param float $i_nSerialNr
     * @return \Axess\Dci4Wtp\unBlockTicketBySerial
     */
    public function setI_nSerialNr($i_nSerialNr)
    {
      $this->i_nSerialNr = $i_nSerialNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nUniCodeNr()
    {
      return $this->i_nUniCodeNr;
    }

    /**
     * @param float $i_nUniCodeNr
     * @return \Axess\Dci4Wtp\unBlockTicketBySerial
     */
    public function setI_nUniCodeNr($i_nUniCodeNr)
    {
      $this->i_nUniCodeNr = $i_nUniCodeNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szValidDate()
    {
      return $this->i_szValidDate;
    }

    /**
     * @param string $i_szValidDate
     * @return \Axess\Dci4Wtp\unBlockTicketBySerial
     */
    public function setI_szValidDate($i_szValidDate)
    {
      $this->i_szValidDate = $i_szValidDate;
      return $this;
    }

}
