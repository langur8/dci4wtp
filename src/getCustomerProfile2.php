<?php

namespace Axess\Dci4Wtp;

class getCustomerProfile2
{

    /**
     * @var D4WTPCUSTOMERPROFILEREQUEST $i_ctCProfileReq
     */
    protected $i_ctCProfileReq = null;

    /**
     * @param D4WTPCUSTOMERPROFILEREQUEST $i_ctCProfileReq
     */
    public function __construct($i_ctCProfileReq)
    {
      $this->i_ctCProfileReq = $i_ctCProfileReq;
    }

    /**
     * @return D4WTPCUSTOMERPROFILEREQUEST
     */
    public function getI_ctCProfileReq()
    {
      return $this->i_ctCProfileReq;
    }

    /**
     * @param D4WTPCUSTOMERPROFILEREQUEST $i_ctCProfileReq
     * @return \Axess\Dci4Wtp\getCustomerProfile2
     */
    public function setI_ctCProfileReq($i_ctCProfileReq)
    {
      $this->i_ctCProfileReq = $i_ctCProfileReq;
      return $this;
    }

}
