<?php

namespace Axess\Dci4Wtp;

class OBI4PSPHOLEKUNDENDATENRESULT
{

    /**
     * @var OBI4PSPAXKUNDENDATEN $CTAXKUNDENDATEN
     */
    protected $CTAXKUNDENDATEN = null;

    /**
     * @var OBI4PSPKUNDENDATEN $CTKUNDENDATEN
     */
    protected $CTKUNDENDATEN = null;

    /**
     * @var OBI4PSPSWISSPASSABOSTATUS $CTSWISSPASSABOSTATUS
     */
    protected $CTSWISSPASSABOSTATUS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZAUSWEISID
     */
    protected $SZAUSWEISID = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZKUNDENID
     */
    protected $SZKUNDENID = null;

    /**
     * @var string $SZUIDISO14443
     */
    protected $SZUIDISO14443 = null;

    /**
     * @var string $SZUIDISO15693
     */
    protected $SZUIDISO15693 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return OBI4PSPAXKUNDENDATEN
     */
    public function getCTAXKUNDENDATEN()
    {
      return $this->CTAXKUNDENDATEN;
    }

    /**
     * @param OBI4PSPAXKUNDENDATEN $CTAXKUNDENDATEN
     * @return \Axess\Dci4Wtp\OBI4PSPHOLEKUNDENDATENRESULT
     */
    public function setCTAXKUNDENDATEN($CTAXKUNDENDATEN)
    {
      $this->CTAXKUNDENDATEN = $CTAXKUNDENDATEN;
      return $this;
    }

    /**
     * @return OBI4PSPKUNDENDATEN
     */
    public function getCTKUNDENDATEN()
    {
      return $this->CTKUNDENDATEN;
    }

    /**
     * @param OBI4PSPKUNDENDATEN $CTKUNDENDATEN
     * @return \Axess\Dci4Wtp\OBI4PSPHOLEKUNDENDATENRESULT
     */
    public function setCTKUNDENDATEN($CTKUNDENDATEN)
    {
      $this->CTKUNDENDATEN = $CTKUNDENDATEN;
      return $this;
    }

    /**
     * @return OBI4PSPSWISSPASSABOSTATUS
     */
    public function getCTSWISSPASSABOSTATUS()
    {
      return $this->CTSWISSPASSABOSTATUS;
    }

    /**
     * @param OBI4PSPSWISSPASSABOSTATUS $CTSWISSPASSABOSTATUS
     * @return \Axess\Dci4Wtp\OBI4PSPHOLEKUNDENDATENRESULT
     */
    public function setCTSWISSPASSABOSTATUS($CTSWISSPASSABOSTATUS)
    {
      $this->CTSWISSPASSABOSTATUS = $CTSWISSPASSABOSTATUS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\OBI4PSPHOLEKUNDENDATENRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAUSWEISID()
    {
      return $this->SZAUSWEISID;
    }

    /**
     * @param string $SZAUSWEISID
     * @return \Axess\Dci4Wtp\OBI4PSPHOLEKUNDENDATENRESULT
     */
    public function setSZAUSWEISID($SZAUSWEISID)
    {
      $this->SZAUSWEISID = $SZAUSWEISID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\OBI4PSPHOLEKUNDENDATENRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZKUNDENID()
    {
      return $this->SZKUNDENID;
    }

    /**
     * @param string $SZKUNDENID
     * @return \Axess\Dci4Wtp\OBI4PSPHOLEKUNDENDATENRESULT
     */
    public function setSZKUNDENID($SZKUNDENID)
    {
      $this->SZKUNDENID = $SZKUNDENID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUIDISO14443()
    {
      return $this->SZUIDISO14443;
    }

    /**
     * @param string $SZUIDISO14443
     * @return \Axess\Dci4Wtp\OBI4PSPHOLEKUNDENDATENRESULT
     */
    public function setSZUIDISO14443($SZUIDISO14443)
    {
      $this->SZUIDISO14443 = $SZUIDISO14443;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUIDISO15693()
    {
      return $this->SZUIDISO15693;
    }

    /**
     * @param string $SZUIDISO15693
     * @return \Axess\Dci4Wtp\OBI4PSPHOLEKUNDENDATENRESULT
     */
    public function setSZUIDISO15693($SZUIDISO15693)
    {
      $this->SZUIDISO15693 = $SZUIDISO15693;
      return $this;
    }

}
