<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPERSONTYPENO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPERSONTYPENO[] $D4WTPPERSONTYPENO
     */
    protected $D4WTPPERSONTYPENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPERSONTYPENO[]
     */
    public function getD4WTPPERSONTYPENO()
    {
      return $this->D4WTPPERSONTYPENO;
    }

    /**
     * @param D4WTPPERSONTYPENO[] $D4WTPPERSONTYPENO
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPERSONTYPENO
     */
    public function setD4WTPPERSONTYPENO(array $D4WTPPERSONTYPENO = null)
    {
      $this->D4WTPPERSONTYPENO = $D4WTPPERSONTYPENO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPERSONTYPENO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPERSONTYPENO
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPERSONTYPENO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPERSONTYPENO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPERSONTYPENO[] = $value;
      } else {
        $this->D4WTPPERSONTYPENO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPERSONTYPENO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPERSONTYPENO Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPERSONTYPENO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPERSONTYPENO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPERSONTYPENO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPERSONTYPENO);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPERSONTYPENO Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPERSONTYPENO);
    }

}
