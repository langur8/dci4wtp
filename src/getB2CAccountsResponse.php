<?php

namespace Axess\Dci4Wtp;

class getB2CAccountsResponse
{

    /**
     * @var D4WTPB2CACCOUNTSRESULT $getB2CAccountsResult
     */
    protected $getB2CAccountsResult = null;

    /**
     * @param D4WTPB2CACCOUNTSRESULT $getB2CAccountsResult
     */
    public function __construct($getB2CAccountsResult)
    {
      $this->getB2CAccountsResult = $getB2CAccountsResult;
    }

    /**
     * @return D4WTPB2CACCOUNTSRESULT
     */
    public function getGetB2CAccountsResult()
    {
      return $this->getB2CAccountsResult;
    }

    /**
     * @param D4WTPB2CACCOUNTSRESULT $getB2CAccountsResult
     * @return \Axess\Dci4Wtp\getB2CAccountsResponse
     */
    public function setGetB2CAccountsResult($getB2CAccountsResult)
    {
      $this->getB2CAccountsResult = $getB2CAccountsResult;
      return $this;
    }

}
