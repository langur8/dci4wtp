<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPOOLNO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPOOLNO[] $D4WTPPOOLNO
     */
    protected $D4WTPPOOLNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPOOLNO[]
     */
    public function getD4WTPPOOLNO()
    {
      return $this->D4WTPPOOLNO;
    }

    /**
     * @param D4WTPPOOLNO[] $D4WTPPOOLNO
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPOOLNO
     */
    public function setD4WTPPOOLNO(array $D4WTPPOOLNO = null)
    {
      $this->D4WTPPOOLNO = $D4WTPPOOLNO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPOOLNO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPOOLNO
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPOOLNO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPOOLNO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPOOLNO[] = $value;
      } else {
        $this->D4WTPPOOLNO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPOOLNO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPOOLNO Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPOOLNO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPOOLNO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPOOLNO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPOOLNO);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPOOLNO Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPOOLNO);
    }

}
