<?php

namespace Axess\Dci4Wtp;

class setExtInvoiceNoResponse
{

    /**
     * @var D4WTPRESULT $setExtInvoiceNoResult
     */
    protected $setExtInvoiceNoResult = null;

    /**
     * @param D4WTPRESULT $setExtInvoiceNoResult
     */
    public function __construct($setExtInvoiceNoResult)
    {
      $this->setExtInvoiceNoResult = $setExtInvoiceNoResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSetExtInvoiceNoResult()
    {
      return $this->setExtInvoiceNoResult;
    }

    /**
     * @param D4WTPRESULT $setExtInvoiceNoResult
     * @return \Axess\Dci4Wtp\setExtInvoiceNoResponse
     */
    public function setSetExtInvoiceNoResult($setExtInvoiceNoResult)
    {
      $this->setExtInvoiceNoResult = $setExtInvoiceNoResult;
      return $this;
    }

}
