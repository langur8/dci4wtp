<?php

namespace Axess\Dci4Wtp;

class getCompanyBalanceResponse
{

    /**
     * @var D4WTPGETCOMPANYBALANCERES $getCompanyBalanceResult
     */
    protected $getCompanyBalanceResult = null;

    /**
     * @param D4WTPGETCOMPANYBALANCERES $getCompanyBalanceResult
     */
    public function __construct($getCompanyBalanceResult)
    {
      $this->getCompanyBalanceResult = $getCompanyBalanceResult;
    }

    /**
     * @return D4WTPGETCOMPANYBALANCERES
     */
    public function getGetCompanyBalanceResult()
    {
      return $this->getCompanyBalanceResult;
    }

    /**
     * @param D4WTPGETCOMPANYBALANCERES $getCompanyBalanceResult
     * @return \Axess\Dci4Wtp\getCompanyBalanceResponse
     */
    public function setGetCompanyBalanceResult($getCompanyBalanceResult)
    {
      $this->getCompanyBalanceResult = $getCompanyBalanceResult;
      return $this;
    }

}
