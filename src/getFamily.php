<?php

namespace Axess\Dci4Wtp;

class getFamily
{

    /**
     * @var D4WTPGETFAMILYREQUEST $i_ctFamilyReq
     */
    protected $i_ctFamilyReq = null;

    /**
     * @param D4WTPGETFAMILYREQUEST $i_ctFamilyReq
     */
    public function __construct($i_ctFamilyReq)
    {
      $this->i_ctFamilyReq = $i_ctFamilyReq;
    }

    /**
     * @return D4WTPGETFAMILYREQUEST
     */
    public function getI_ctFamilyReq()
    {
      return $this->i_ctFamilyReq;
    }

    /**
     * @param D4WTPGETFAMILYREQUEST $i_ctFamilyReq
     * @return \Axess\Dci4Wtp\getFamily
     */
    public function setI_ctFamilyReq($i_ctFamilyReq)
    {
      $this->i_ctFamilyReq = $i_ctFamilyReq;
      return $this;
    }

}
