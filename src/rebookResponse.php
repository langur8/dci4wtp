<?php

namespace Axess\Dci4Wtp;

class rebookResponse
{

    /**
     * @var D4WTPREBOOKRESULT $rebookResult
     */
    protected $rebookResult = null;

    /**
     * @param D4WTPREBOOKRESULT $rebookResult
     */
    public function __construct($rebookResult)
    {
      $this->rebookResult = $rebookResult;
    }

    /**
     * @return D4WTPREBOOKRESULT
     */
    public function getRebookResult()
    {
      return $this->rebookResult;
    }

    /**
     * @param D4WTPREBOOKRESULT $rebookResult
     * @return \Axess\Dci4Wtp\rebookResponse
     */
    public function setRebookResult($rebookResult)
    {
      $this->rebookResult = $rebookResult;
      return $this;
    }

}
