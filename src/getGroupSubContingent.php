<?php

namespace Axess\Dci4Wtp;

class getGroupSubContingent
{

    /**
     * @var D4WTPGETGRPSUBCONTINGENTREQ $i_ctgetGroupSubContingentReq
     */
    protected $i_ctgetGroupSubContingentReq = null;

    /**
     * @param D4WTPGETGRPSUBCONTINGENTREQ $i_ctgetGroupSubContingentReq
     */
    public function __construct($i_ctgetGroupSubContingentReq)
    {
      $this->i_ctgetGroupSubContingentReq = $i_ctgetGroupSubContingentReq;
    }

    /**
     * @return D4WTPGETGRPSUBCONTINGENTREQ
     */
    public function getI_ctgetGroupSubContingentReq()
    {
      return $this->i_ctgetGroupSubContingentReq;
    }

    /**
     * @param D4WTPGETGRPSUBCONTINGENTREQ $i_ctgetGroupSubContingentReq
     * @return \Axess\Dci4Wtp\getGroupSubContingent
     */
    public function setI_ctgetGroupSubContingentReq($i_ctgetGroupSubContingentReq)
    {
      $this->i_ctgetGroupSubContingentReq = $i_ctgetGroupSubContingentReq;
      return $this;
    }

}
