<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPARCUBE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPARCUBE[] $D4WTPARCUBE
     */
    protected $D4WTPARCUBE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPARCUBE[]
     */
    public function getD4WTPARCUBE()
    {
      return $this->D4WTPARCUBE;
    }

    /**
     * @param D4WTPARCUBE[] $D4WTPARCUBE
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPARCUBE
     */
    public function setD4WTPARCUBE(array $D4WTPARCUBE = null)
    {
      $this->D4WTPARCUBE = $D4WTPARCUBE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPARCUBE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPARCUBE
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPARCUBE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPARCUBE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPARCUBE[] = $value;
      } else {
        $this->D4WTPARCUBE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPARCUBE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPARCUBE Return the current element
     */
    public function current()
    {
      return current($this->D4WTPARCUBE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPARCUBE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPARCUBE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPARCUBE);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPARCUBE Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPARCUBE);
    }

}
