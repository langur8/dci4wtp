<?php

namespace Axess\Dci4Wtp;

class CreateReservationRequest
{

    /**
     * @var int $Duration
     */
    protected $Duration = null;

    /**
     * @var ArrayOfCreateReservationTicket $Tickets
     */
    protected $Tickets = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getDuration()
    {
      return $this->Duration;
    }

    /**
     * @param int $Duration
     * @return \Axess\Dci4Wtp\CreateReservationRequest
     */
    public function setDuration($Duration)
    {
      $this->Duration = $Duration;
      return $this;
    }

    /**
     * @return ArrayOfCreateReservationTicket
     */
    public function getTickets()
    {
      return $this->Tickets;
    }

    /**
     * @param ArrayOfCreateReservationTicket $Tickets
     * @return \Axess\Dci4Wtp\CreateReservationRequest
     */
    public function setTickets($Tickets)
    {
      $this->Tickets = $Tickets;
      return $this;
    }

}
