<?php

namespace Axess\Dci4Wtp;

class D4WTPCUSTOMERADDRESS
{

    /**
     * @var float $NADDRESSTYPENO
     */
    protected $NADDRESSTYPENO = null;

    /**
     * @var float $NADRKASSANR
     */
    protected $NADRKASSANR = null;

    /**
     * @var float $NADRNR
     */
    protected $NADRNR = null;

    /**
     * @var float $NADRPROJNR
     */
    protected $NADRPROJNR = null;

    /**
     * @var string $SZAREA
     */
    protected $SZAREA = null;

    /**
     * @var string $SZCITY
     */
    protected $SZCITY = null;

    /**
     * @var string $SZCO
     */
    protected $SZCO = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    /**
     * @var string $SZCOUNTRYSHORTNAME
     */
    protected $SZCOUNTRYSHORTNAME = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZFAX
     */
    protected $SZFAX = null;

    /**
     * @var string $SZHOMENUMBER
     */
    protected $SZHOMENUMBER = null;

    /**
     * @var string $SZPHONE
     */
    protected $SZPHONE = null;

    /**
     * @var string $SZSTREET
     */
    protected $SZSTREET = null;

    /**
     * @var string $SZSUBDIVISION
     */
    protected $SZSUBDIVISION = null;

    /**
     * @var string $SZZIPCODE
     */
    protected $SZZIPCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNADDRESSTYPENO()
    {
      return $this->NADDRESSTYPENO;
    }

    /**
     * @param float $NADDRESSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setNADDRESSTYPENO($NADDRESSTYPENO)
    {
      $this->NADDRESSTYPENO = $NADDRESSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNADRKASSANR()
    {
      return $this->NADRKASSANR;
    }

    /**
     * @param float $NADRKASSANR
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setNADRKASSANR($NADRKASSANR)
    {
      $this->NADRKASSANR = $NADRKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNADRNR()
    {
      return $this->NADRNR;
    }

    /**
     * @param float $NADRNR
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setNADRNR($NADRNR)
    {
      $this->NADRNR = $NADRNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNADRPROJNR()
    {
      return $this->NADRPROJNR;
    }

    /**
     * @param float $NADRPROJNR
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setNADRPROJNR($NADRPROJNR)
    {
      $this->NADRPROJNR = $NADRPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAREA()
    {
      return $this->SZAREA;
    }

    /**
     * @param string $SZAREA
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setSZAREA($SZAREA)
    {
      $this->SZAREA = $SZAREA;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCITY()
    {
      return $this->SZCITY;
    }

    /**
     * @param string $SZCITY
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setSZCITY($SZCITY)
    {
      $this->SZCITY = $SZCITY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCO()
    {
      return $this->SZCO;
    }

    /**
     * @param string $SZCO
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setSZCO($SZCO)
    {
      $this->SZCO = $SZCO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYSHORTNAME()
    {
      return $this->SZCOUNTRYSHORTNAME;
    }

    /**
     * @param string $SZCOUNTRYSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setSZCOUNTRYSHORTNAME($SZCOUNTRYSHORTNAME)
    {
      $this->SZCOUNTRYSHORTNAME = $SZCOUNTRYSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFAX()
    {
      return $this->SZFAX;
    }

    /**
     * @param string $SZFAX
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setSZFAX($SZFAX)
    {
      $this->SZFAX = $SZFAX;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZHOMENUMBER()
    {
      return $this->SZHOMENUMBER;
    }

    /**
     * @param string $SZHOMENUMBER
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setSZHOMENUMBER($SZHOMENUMBER)
    {
      $this->SZHOMENUMBER = $SZHOMENUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPHONE()
    {
      return $this->SZPHONE;
    }

    /**
     * @param string $SZPHONE
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setSZPHONE($SZPHONE)
    {
      $this->SZPHONE = $SZPHONE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTREET()
    {
      return $this->SZSTREET;
    }

    /**
     * @param string $SZSTREET
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setSZSTREET($SZSTREET)
    {
      $this->SZSTREET = $SZSTREET;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSUBDIVISION()
    {
      return $this->SZSUBDIVISION;
    }

    /**
     * @param string $SZSUBDIVISION
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setSZSUBDIVISION($SZSUBDIVISION)
    {
      $this->SZSUBDIVISION = $SZSUBDIVISION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZZIPCODE()
    {
      return $this->SZZIPCODE;
    }

    /**
     * @param string $SZZIPCODE
     * @return \Axess\Dci4Wtp\D4WTPCUSTOMERADDRESS
     */
    public function setSZZIPCODE($SZZIPCODE)
    {
      $this->SZZIPCODE = $SZZIPCODE;
      return $this;
    }

}
