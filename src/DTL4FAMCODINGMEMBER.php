<?php

namespace Axess\Dci4Wtp;

class DTL4FAMCODINGMEMBER
{

    /**
     * @var ArrayOfDTL4FAMCODINGTICKETUSAGE $ACTMCTICKETUSAGE
     */
    protected $ACTMCTICKETUSAGE = null;

    /**
     * @var float $BISDELETED
     */
    protected $BISDELETED = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    /**
     * @var string $SZWTPNUMBER
     */
    protected $SZWTPNUMBER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfDTL4FAMCODINGTICKETUSAGE
     */
    public function getACTMCTICKETUSAGE()
    {
      return $this->ACTMCTICKETUSAGE;
    }

    /**
     * @param ArrayOfDTL4FAMCODINGTICKETUSAGE $ACTMCTICKETUSAGE
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMEMBER
     */
    public function setACTMCTICKETUSAGE($ACTMCTICKETUSAGE)
    {
      $this->ACTMCTICKETUSAGE = $ACTMCTICKETUSAGE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISDELETED()
    {
      return $this->BISDELETED;
    }

    /**
     * @param float $BISDELETED
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMEMBER
     */
    public function setBISDELETED($BISDELETED)
    {
      $this->BISDELETED = $BISDELETED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMEMBER
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMEMBER
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMEMBER
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMEMBER
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMEMBER
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMEMBER
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNUMBER()
    {
      return $this->SZWTPNUMBER;
    }

    /**
     * @param string $SZWTPNUMBER
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGMEMBER
     */
    public function setSZWTPNUMBER($SZWTPNUMBER)
    {
      $this->SZWTPNUMBER = $SZWTPNUMBER;
      return $this;
    }

}
