<?php

namespace Axess\Dci4Wtp;

class D4WTPWTPDATAOCC
{

    /**
     * @var float $BBOOKED
     */
    protected $BBOOKED = null;

    /**
     * @var float $BFIXDATAINCLHEADER
     */
    protected $BFIXDATAINCLHEADER = null;

    /**
     * @var float $NDATACARRIERSERIALNUMBER
     */
    protected $NDATACARRIERSERIALNUMBER = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var float $NLOGNR
     */
    protected $NLOGNR = null;

    /**
     * @var float $NPOOLNR
     */
    protected $NPOOLNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSUBDOMAIN
     */
    protected $NSUBDOMAIN = null;

    /**
     * @var float $NSYSTEMCOMPNR
     */
    protected $NSYSTEMCOMPNR = null;

    /**
     * @var float $NSYSTEMCOMPTYPNR
     */
    protected $NSYSTEMCOMPTYPNR = null;

    /**
     * @var float $NTARGETREGIONID
     */
    protected $NTARGETREGIONID = null;

    /**
     * @var float $NUSAGEDOMAIN
     */
    protected $NUSAGEDOMAIN = null;

    /**
     * @var string $SZCODINGFIXDATA
     */
    protected $SZCODINGFIXDATA = null;

    /**
     * @var string $SZCODINGHEADER
     */
    protected $SZCODINGHEADER = null;

    /**
     * @var string $SZCODINGINFORMATION
     */
    protected $SZCODINGINFORMATION = null;

    /**
     * @var string $SZCODINGVARDATA
     */
    protected $SZCODINGVARDATA = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZMEDIAIDHEX
     */
    protected $SZMEDIAIDHEX = null;

    /**
     * @var string $SZPERMISSIONHANDLE
     */
    protected $SZPERMISSIONHANDLE = null;

    /**
     * @var string $SZSOURCESYSTEM
     */
    protected $SZSOURCESYSTEM = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBBOOKED()
    {
      return $this->BBOOKED;
    }

    /**
     * @param float $BBOOKED
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setBBOOKED($BBOOKED)
    {
      $this->BBOOKED = $BBOOKED;
      return $this;
    }

    /**
     * @return float
     */
    public function getBFIXDATAINCLHEADER()
    {
      return $this->BFIXDATAINCLHEADER;
    }

    /**
     * @param float $BFIXDATAINCLHEADER
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setBFIXDATAINCLHEADER($BFIXDATAINCLHEADER)
    {
      $this->BFIXDATAINCLHEADER = $BFIXDATAINCLHEADER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATACARRIERSERIALNUMBER()
    {
      return $this->NDATACARRIERSERIALNUMBER;
    }

    /**
     * @param float $NDATACARRIERSERIALNUMBER
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setNDATACARRIERSERIALNUMBER($NDATACARRIERSERIALNUMBER)
    {
      $this->NDATACARRIERSERIALNUMBER = $NDATACARRIERSERIALNUMBER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLOGNR()
    {
      return $this->NLOGNR;
    }

    /**
     * @param float $NLOGNR
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setNLOGNR($NLOGNR)
    {
      $this->NLOGNR = $NLOGNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNR()
    {
      return $this->NPOOLNR;
    }

    /**
     * @param float $NPOOLNR
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setNPOOLNR($NPOOLNR)
    {
      $this->NPOOLNR = $NPOOLNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBDOMAIN()
    {
      return $this->NSUBDOMAIN;
    }

    /**
     * @param float $NSUBDOMAIN
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setNSUBDOMAIN($NSUBDOMAIN)
    {
      $this->NSUBDOMAIN = $NSUBDOMAIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSYSTEMCOMPNR()
    {
      return $this->NSYSTEMCOMPNR;
    }

    /**
     * @param float $NSYSTEMCOMPNR
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setNSYSTEMCOMPNR($NSYSTEMCOMPNR)
    {
      $this->NSYSTEMCOMPNR = $NSYSTEMCOMPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSYSTEMCOMPTYPNR()
    {
      return $this->NSYSTEMCOMPTYPNR;
    }

    /**
     * @param float $NSYSTEMCOMPTYPNR
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setNSYSTEMCOMPTYPNR($NSYSTEMCOMPTYPNR)
    {
      $this->NSYSTEMCOMPTYPNR = $NSYSTEMCOMPTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARGETREGIONID()
    {
      return $this->NTARGETREGIONID;
    }

    /**
     * @param float $NTARGETREGIONID
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setNTARGETREGIONID($NTARGETREGIONID)
    {
      $this->NTARGETREGIONID = $NTARGETREGIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUSAGEDOMAIN()
    {
      return $this->NUSAGEDOMAIN;
    }

    /**
     * @param float $NUSAGEDOMAIN
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setNUSAGEDOMAIN($NUSAGEDOMAIN)
    {
      $this->NUSAGEDOMAIN = $NUSAGEDOMAIN;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGFIXDATA()
    {
      return $this->SZCODINGFIXDATA;
    }

    /**
     * @param string $SZCODINGFIXDATA
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setSZCODINGFIXDATA($SZCODINGFIXDATA)
    {
      $this->SZCODINGFIXDATA = $SZCODINGFIXDATA;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGHEADER()
    {
      return $this->SZCODINGHEADER;
    }

    /**
     * @param string $SZCODINGHEADER
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setSZCODINGHEADER($SZCODINGHEADER)
    {
      $this->SZCODINGHEADER = $SZCODINGHEADER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGINFORMATION()
    {
      return $this->SZCODINGINFORMATION;
    }

    /**
     * @param string $SZCODINGINFORMATION
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setSZCODINGINFORMATION($SZCODINGINFORMATION)
    {
      $this->SZCODINGINFORMATION = $SZCODINGINFORMATION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCODINGVARDATA()
    {
      return $this->SZCODINGVARDATA;
    }

    /**
     * @param string $SZCODINGVARDATA
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setSZCODINGVARDATA($SZCODINGVARDATA)
    {
      $this->SZCODINGVARDATA = $SZCODINGVARDATA;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAIDHEX()
    {
      return $this->SZMEDIAIDHEX;
    }

    /**
     * @param string $SZMEDIAIDHEX
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setSZMEDIAIDHEX($SZMEDIAIDHEX)
    {
      $this->SZMEDIAIDHEX = $SZMEDIAIDHEX;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERMISSIONHANDLE()
    {
      return $this->SZPERMISSIONHANDLE;
    }

    /**
     * @param string $SZPERMISSIONHANDLE
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setSZPERMISSIONHANDLE($SZPERMISSIONHANDLE)
    {
      $this->SZPERMISSIONHANDLE = $SZPERMISSIONHANDLE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSOURCESYSTEM()
    {
      return $this->SZSOURCESYSTEM;
    }

    /**
     * @param string $SZSOURCESYSTEM
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setSZSOURCESYSTEM($SZSOURCESYSTEM)
    {
      $this->SZSOURCESYSTEM = $SZSOURCESYSTEM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPWTPDATAOCC
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
