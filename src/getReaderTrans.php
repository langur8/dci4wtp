<?php

namespace Axess\Dci4Wtp;

class getReaderTrans
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var float $i_nProjectNr
     */
    protected $i_nProjectNr = null;

    /**
     * @var float $i_nPOSNr
     */
    protected $i_nPOSNr = null;

    /**
     * @var float $i_nSerialNr
     */
    protected $i_nSerialNr = null;

    /**
     * @var string $i_szValidDate
     */
    protected $i_szValidDate = null;

    /**
     * @param float $i_nSessionID
     * @param float $i_nProjectNr
     * @param float $i_nPOSNr
     * @param float $i_nSerialNr
     * @param string $i_szValidDate
     */
    public function __construct($i_nSessionID, $i_nProjectNr, $i_nPOSNr, $i_nSerialNr, $i_szValidDate)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->i_nProjectNr = $i_nProjectNr;
      $this->i_nPOSNr = $i_nPOSNr;
      $this->i_nSerialNr = $i_nSerialNr;
      $this->i_szValidDate = $i_szValidDate;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\getReaderTrans
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nProjectNr()
    {
      return $this->i_nProjectNr;
    }

    /**
     * @param float $i_nProjectNr
     * @return \Axess\Dci4Wtp\getReaderTrans
     */
    public function setI_nProjectNr($i_nProjectNr)
    {
      $this->i_nProjectNr = $i_nProjectNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nPOSNr()
    {
      return $this->i_nPOSNr;
    }

    /**
     * @param float $i_nPOSNr
     * @return \Axess\Dci4Wtp\getReaderTrans
     */
    public function setI_nPOSNr($i_nPOSNr)
    {
      $this->i_nPOSNr = $i_nPOSNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getI_nSerialNr()
    {
      return $this->i_nSerialNr;
    }

    /**
     * @param float $i_nSerialNr
     * @return \Axess\Dci4Wtp\getReaderTrans
     */
    public function setI_nSerialNr($i_nSerialNr)
    {
      $this->i_nSerialNr = $i_nSerialNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getI_szValidDate()
    {
      return $this->i_szValidDate;
    }

    /**
     * @param string $i_szValidDate
     * @return \Axess\Dci4Wtp\getReaderTrans
     */
    public function setI_szValidDate($i_szValidDate)
    {
      $this->i_szValidDate = $i_szValidDate;
      return $this;
    }

}
