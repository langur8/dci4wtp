<?php

namespace Axess\Dci4Wtp;

class cancelPOEBookingResponse
{

    /**
     * @var WTPCancelPOEBookingResult $cancelPOEBookingResult
     */
    protected $cancelPOEBookingResult = null;

    /**
     * @param WTPCancelPOEBookingResult $cancelPOEBookingResult
     */
    public function __construct($cancelPOEBookingResult)
    {
      $this->cancelPOEBookingResult = $cancelPOEBookingResult;
    }

    /**
     * @return WTPCancelPOEBookingResult
     */
    public function getCancelPOEBookingResult()
    {
      return $this->cancelPOEBookingResult;
    }

    /**
     * @param WTPCancelPOEBookingResult $cancelPOEBookingResult
     * @return \Axess\Dci4Wtp\cancelPOEBookingResponse
     */
    public function setCancelPOEBookingResult($cancelPOEBookingResult)
    {
      $this->cancelPOEBookingResult = $cancelPOEBookingResult;
      return $this;
    }

}
