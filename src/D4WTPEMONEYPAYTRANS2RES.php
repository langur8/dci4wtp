<?php

namespace Axess\Dci4Wtp;

class D4WTPEMONEYPAYTRANS2RES
{

    /**
     * @var ArrayOfD4WTPEMONEYACCOUNT $ACTEMONEYACCOUNTDET
     */
    protected $ACTEMONEYACCOUNTDET = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPEMONEYACCOUNT
     */
    public function getACTEMONEYACCOUNTDET()
    {
      return $this->ACTEMONEYACCOUNTDET;
    }

    /**
     * @param ArrayOfD4WTPEMONEYACCOUNT $ACTEMONEYACCOUNTDET
     * @return \Axess\Dci4Wtp\D4WTPEMONEYPAYTRANS2RES
     */
    public function setACTEMONEYACCOUNTDET($ACTEMONEYACCOUNTDET)
    {
      $this->ACTEMONEYACCOUNTDET = $ACTEMONEYACCOUNTDET;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPEMONEYPAYTRANS2RES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPEMONEYPAYTRANS2RES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
