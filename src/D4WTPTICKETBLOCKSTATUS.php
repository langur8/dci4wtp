<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETBLOCKSTATUS
{

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var float $BISOCC
     */
    protected $BISOCC = null;

    /**
     * @var float $NACTPOSNO
     */
    protected $NACTPOSNO = null;

    /**
     * @var float $NACTPROJNO
     */
    protected $NACTPROJNO = null;

    /**
     * @var float $NBLOCKCASHIERNO
     */
    protected $NBLOCKCASHIERNO = null;

    /**
     * @var float $NBLOCKREASONNO
     */
    protected $NBLOCKREASONNO = null;

    /**
     * @var float $NLFDBLOCKNO
     */
    protected $NLFDBLOCKNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NUNICODENO
     */
    protected $NUNICODENO = null;

    /**
     * @var float $NVARBLOCKMASKNO
     */
    protected $NVARBLOCKMASKNO = null;

    /**
     * @var string $SZBLOCKINGDATE
     */
    protected $SZBLOCKINGDATE = null;

    /**
     * @var string $SZENDOFBLOCKINGDATE
     */
    protected $SZENDOFBLOCKINGDATE = null;

    /**
     * @var string $SZVALIDDATE
     */
    protected $SZVALIDDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISOCC()
    {
      return $this->BISOCC;
    }

    /**
     * @param float $BISOCC
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setBISOCC($BISOCC)
    {
      $this->BISOCC = $BISOCC;
      return $this;
    }

    /**
     * @return float
     */
    public function getNACTPOSNO()
    {
      return $this->NACTPOSNO;
    }

    /**
     * @param float $NACTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setNACTPOSNO($NACTPOSNO)
    {
      $this->NACTPOSNO = $NACTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNACTPROJNO()
    {
      return $this->NACTPROJNO;
    }

    /**
     * @param float $NACTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setNACTPROJNO($NACTPROJNO)
    {
      $this->NACTPROJNO = $NACTPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKCASHIERNO()
    {
      return $this->NBLOCKCASHIERNO;
    }

    /**
     * @param float $NBLOCKCASHIERNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setNBLOCKCASHIERNO($NBLOCKCASHIERNO)
    {
      $this->NBLOCKCASHIERNO = $NBLOCKCASHIERNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKREASONNO()
    {
      return $this->NBLOCKREASONNO;
    }

    /**
     * @param float $NBLOCKREASONNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setNBLOCKREASONNO($NBLOCKREASONNO)
    {
      $this->NBLOCKREASONNO = $NBLOCKREASONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDBLOCKNO()
    {
      return $this->NLFDBLOCKNO;
    }

    /**
     * @param float $NLFDBLOCKNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setNLFDBLOCKNO($NLFDBLOCKNO)
    {
      $this->NLFDBLOCKNO = $NLFDBLOCKNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENO()
    {
      return $this->NUNICODENO;
    }

    /**
     * @param float $NUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setNUNICODENO($NUNICODENO)
    {
      $this->NUNICODENO = $NUNICODENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVARBLOCKMASKNO()
    {
      return $this->NVARBLOCKMASKNO;
    }

    /**
     * @param float $NVARBLOCKMASKNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setNVARBLOCKMASKNO($NVARBLOCKMASKNO)
    {
      $this->NVARBLOCKMASKNO = $NVARBLOCKMASKNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBLOCKINGDATE()
    {
      return $this->SZBLOCKINGDATE;
    }

    /**
     * @param string $SZBLOCKINGDATE
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setSZBLOCKINGDATE($SZBLOCKINGDATE)
    {
      $this->SZBLOCKINGDATE = $SZBLOCKINGDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZENDOFBLOCKINGDATE()
    {
      return $this->SZENDOFBLOCKINGDATE;
    }

    /**
     * @param string $SZENDOFBLOCKINGDATE
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setSZENDOFBLOCKINGDATE($SZENDOFBLOCKINGDATE)
    {
      $this->SZENDOFBLOCKINGDATE = $SZENDOFBLOCKINGDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDDATE()
    {
      return $this->SZVALIDDATE;
    }

    /**
     * @param string $SZVALIDDATE
     * @return \Axess\Dci4Wtp\D4WTPTICKETBLOCKSTATUS
     */
    public function setSZVALIDDATE($SZVALIDDATE)
    {
      $this->SZVALIDDATE = $SZVALIDDATE;
      return $this;
    }

}
