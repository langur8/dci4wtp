<?php

namespace Axess\Dci4Wtp;

class rebook
{

    /**
     * @var D4WTPREBOOKREQUEST $i_ctRebookReq
     */
    protected $i_ctRebookReq = null;

    /**
     * @param D4WTPREBOOKREQUEST $i_ctRebookReq
     */
    public function __construct($i_ctRebookReq)
    {
      $this->i_ctRebookReq = $i_ctRebookReq;
    }

    /**
     * @return D4WTPREBOOKREQUEST
     */
    public function getI_ctRebookReq()
    {
      return $this->i_ctRebookReq;
    }

    /**
     * @param D4WTPREBOOKREQUEST $i_ctRebookReq
     * @return \Axess\Dci4Wtp\rebook
     */
    public function setI_ctRebookReq($i_ctRebookReq)
    {
      $this->i_ctRebookReq = $i_ctRebookReq;
      return $this;
    }

}
