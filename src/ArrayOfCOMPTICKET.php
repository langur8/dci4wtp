<?php

namespace Axess\Dci4Wtp;

class ArrayOfCOMPTICKET implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var COMPTICKET[] $COMPTICKET
     */
    protected $COMPTICKET = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return COMPTICKET[]
     */
    public function getCOMPTICKET()
    {
      return $this->COMPTICKET;
    }

    /**
     * @param COMPTICKET[] $COMPTICKET
     * @return \Axess\Dci4Wtp\ArrayOfCOMPTICKET
     */
    public function setCOMPTICKET(array $COMPTICKET = null)
    {
      $this->COMPTICKET = $COMPTICKET;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->COMPTICKET[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return COMPTICKET
     */
    public function offsetGet($offset)
    {
      return $this->COMPTICKET[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param COMPTICKET $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->COMPTICKET[] = $value;
      } else {
        $this->COMPTICKET[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->COMPTICKET[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return COMPTICKET Return the current element
     */
    public function current()
    {
      return current($this->COMPTICKET);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->COMPTICKET);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->COMPTICKET);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->COMPTICKET);
    }

    /**
     * Countable implementation
     *
     * @return COMPTICKET Return count of elements
     */
    public function count()
    {
      return count($this->COMPTICKET);
    }

}
