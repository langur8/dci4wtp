<?php

namespace Axess\Dci4Wtp;

class getWTPGroups2
{

    /**
     * @var D4WTPGROUPS2REQ $i_ctGroup2Req
     */
    protected $i_ctGroup2Req = null;

    /**
     * @param D4WTPGROUPS2REQ $i_ctGroup2Req
     */
    public function __construct($i_ctGroup2Req)
    {
      $this->i_ctGroup2Req = $i_ctGroup2Req;
    }

    /**
     * @return D4WTPGROUPS2REQ
     */
    public function getI_ctGroup2Req()
    {
      return $this->i_ctGroup2Req;
    }

    /**
     * @param D4WTPGROUPS2REQ $i_ctGroup2Req
     * @return \Axess\Dci4Wtp\getWTPGroups2
     */
    public function setI_ctGroup2Req($i_ctGroup2Req)
    {
      $this->i_ctGroup2Req = $i_ctGroup2Req;
      return $this;
    }

}
