<?php

namespace Axess\Dci4Wtp;

class D4WTPADDLICENSEPLATEREQ
{

    /**
     * @var float $NKASSANR
     */
    protected $NKASSANR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSERIENNR
     */
    protected $NSERIENNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZLICENSEPLATE
     */
    protected $SZLICENSEPLATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNKASSANR()
    {
      return $this->NKASSANR;
    }

    /**
     * @param float $NKASSANR
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATEREQ
     */
    public function setNKASSANR($NKASSANR)
    {
      $this->NKASSANR = $NKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATEREQ
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIENNR()
    {
      return $this->NSERIENNR;
    }

    /**
     * @param float $NSERIENNR
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATEREQ
     */
    public function setNSERIENNR($NSERIENNR)
    {
      $this->NSERIENNR = $NSERIENNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATEREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLICENSEPLATE()
    {
      return $this->SZLICENSEPLATE;
    }

    /**
     * @param string $SZLICENSEPLATE
     * @return \Axess\Dci4Wtp\D4WTPADDLICENSEPLATEREQ
     */
    public function setSZLICENSEPLATE($SZLICENSEPLATE)
    {
      $this->SZLICENSEPLATE = $SZLICENSEPLATE;
      return $this;
    }

}
