<?php

namespace Axess\Dci4Wtp;

class getWTPNoSalesDataResponse
{

    /**
     * @var D4WTPWTPNOSALESDATARESULT $getWTPNoSalesDataResult
     */
    protected $getWTPNoSalesDataResult = null;

    /**
     * @param D4WTPWTPNOSALESDATARESULT $getWTPNoSalesDataResult
     */
    public function __construct($getWTPNoSalesDataResult)
    {
      $this->getWTPNoSalesDataResult = $getWTPNoSalesDataResult;
    }

    /**
     * @return D4WTPWTPNOSALESDATARESULT
     */
    public function getGetWTPNoSalesDataResult()
    {
      return $this->getWTPNoSalesDataResult;
    }

    /**
     * @param D4WTPWTPNOSALESDATARESULT $getWTPNoSalesDataResult
     * @return \Axess\Dci4Wtp\getWTPNoSalesDataResponse
     */
    public function setGetWTPNoSalesDataResult($getWTPNoSalesDataResult)
    {
      $this->getWTPNoSalesDataResult = $getWTPNoSalesDataResult;
      return $this;
    }

}
