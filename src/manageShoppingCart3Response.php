<?php

namespace Axess\Dci4Wtp;

class manageShoppingCart3Response
{

    /**
     * @var D4WTPMANAGESHOPCARTRESULT $manageShoppingCart3Result
     */
    protected $manageShoppingCart3Result = null;

    /**
     * @param D4WTPMANAGESHOPCARTRESULT $manageShoppingCart3Result
     */
    public function __construct($manageShoppingCart3Result)
    {
      $this->manageShoppingCart3Result = $manageShoppingCart3Result;
    }

    /**
     * @return D4WTPMANAGESHOPCARTRESULT
     */
    public function getManageShoppingCart3Result()
    {
      return $this->manageShoppingCart3Result;
    }

    /**
     * @param D4WTPMANAGESHOPCARTRESULT $manageShoppingCart3Result
     * @return \Axess\Dci4Wtp\manageShoppingCart3Response
     */
    public function setManageShoppingCart3Result($manageShoppingCart3Result)
    {
      $this->manageShoppingCart3Result = $manageShoppingCart3Result;
      return $this;
    }

}
