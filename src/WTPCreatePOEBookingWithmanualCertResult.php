<?php

namespace Axess\Dci4Wtp;

class WTPCreatePOEBookingWithmanualCertResult
{

    /**
     * @var int $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param int $NERRORNO
     * @return \Axess\Dci4Wtp\WTPCreatePOEBookingWithmanualCertResult
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\WTPCreatePOEBookingWithmanualCertResult
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
