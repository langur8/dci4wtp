<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETPRODDATA
{

    /**
     * @var D4WTPCODINGDATA $CTCODINGDATA
     */
    protected $CTCODINGDATA = null;

    /**
     * @var D4WTPPRINTINGDATA $CTPRINTINGDATA
     */
    protected $CTPRINTINGDATA = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCODINGDATA
     */
    public function getCTCODINGDATA()
    {
      return $this->CTCODINGDATA;
    }

    /**
     * @param D4WTPCODINGDATA $CTCODINGDATA
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA
     */
    public function setCTCODINGDATA($CTCODINGDATA)
    {
      $this->CTCODINGDATA = $CTCODINGDATA;
      return $this;
    }

    /**
     * @return D4WTPPRINTINGDATA
     */
    public function getCTPRINTINGDATA()
    {
      return $this->CTPRINTINGDATA;
    }

    /**
     * @param D4WTPPRINTINGDATA $CTPRINTINGDATA
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA
     */
    public function setCTPRINTINGDATA($CTPRINTINGDATA)
    {
      $this->CTPRINTINGDATA = $CTPRINTINGDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPTICKETPRODDATA
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
