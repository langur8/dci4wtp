<?php

namespace Axess\Dci4Wtp;

class uploadPrivacyDoc
{

    /**
     * @var D4WTPUPLOADPRIVACYDOCREQ $i_ctUploadPrivacyDocREQReq
     */
    protected $i_ctUploadPrivacyDocREQReq = null;

    /**
     * @param D4WTPUPLOADPRIVACYDOCREQ $i_ctUploadPrivacyDocREQReq
     */
    public function __construct($i_ctUploadPrivacyDocREQReq)
    {
      $this->i_ctUploadPrivacyDocREQReq = $i_ctUploadPrivacyDocREQReq;
    }

    /**
     * @return D4WTPUPLOADPRIVACYDOCREQ
     */
    public function getI_ctUploadPrivacyDocREQReq()
    {
      return $this->i_ctUploadPrivacyDocREQReq;
    }

    /**
     * @param D4WTPUPLOADPRIVACYDOCREQ $i_ctUploadPrivacyDocREQReq
     * @return \Axess\Dci4Wtp\uploadPrivacyDoc
     */
    public function setI_ctUploadPrivacyDocREQReq($i_ctUploadPrivacyDocREQReq)
    {
      $this->i_ctUploadPrivacyDocREQReq = $i_ctUploadPrivacyDocREQReq;
      return $this;
    }

}
