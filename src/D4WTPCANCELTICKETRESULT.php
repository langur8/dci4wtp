<?php

namespace Axess\Dci4Wtp;

class D4WTPCANCELTICKETRESULT
{

    /**
     * @var D4WTPCODINGDATA $CTCODINGDATA
     */
    protected $CTCODINGDATA = null;

    /**
     * @var D4WTPPRINTINGDATA $CTPRINTINGDATA
     */
    protected $CTPRINTINGDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NTICKETPOSNO
     */
    protected $NTICKETPOSNO = null;

    /**
     * @var float $NTICKETPROJNO
     */
    protected $NTICKETPROJNO = null;

    /**
     * @var float $NTICKETSERIALNO
     */
    protected $NTICKETSERIALNO = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCODINGDATA
     */
    public function getCTCODINGDATA()
    {
      return $this->CTCODINGDATA;
    }

    /**
     * @param D4WTPCODINGDATA $CTCODINGDATA
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETRESULT
     */
    public function setCTCODINGDATA($CTCODINGDATA)
    {
      $this->CTCODINGDATA = $CTCODINGDATA;
      return $this;
    }

    /**
     * @return D4WTPPRINTINGDATA
     */
    public function getCTPRINTINGDATA()
    {
      return $this->CTPRINTINGDATA;
    }

    /**
     * @param D4WTPPRINTINGDATA $CTPRINTINGDATA
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETRESULT
     */
    public function setCTPRINTINGDATA($CTPRINTINGDATA)
    {
      $this->CTPRINTINGDATA = $CTPRINTINGDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETRESULT
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETRESULT
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETRESULT
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETPOSNO()
    {
      return $this->NTICKETPOSNO;
    }

    /**
     * @param float $NTICKETPOSNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETRESULT
     */
    public function setNTICKETPOSNO($NTICKETPOSNO)
    {
      $this->NTICKETPOSNO = $NTICKETPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETPROJNO()
    {
      return $this->NTICKETPROJNO;
    }

    /**
     * @param float $NTICKETPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETRESULT
     */
    public function setNTICKETPROJNO($NTICKETPROJNO)
    {
      $this->NTICKETPROJNO = $NTICKETPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETSERIALNO()
    {
      return $this->NTICKETSERIALNO;
    }

    /**
     * @param float $NTICKETSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETRESULT
     */
    public function setNTICKETSERIALNO($NTICKETSERIALNO)
    {
      $this->NTICKETSERIALNO = $NTICKETSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETRESULT
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPCANCELTICKETRESULT
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
