<?php

namespace Axess\Dci4Wtp;

class ArrayOfQRCodeRecoveryData implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var QRCodeRecoveryData[] $QRCodeRecoveryData
     */
    protected $QRCodeRecoveryData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return QRCodeRecoveryData[]
     */
    public function getQRCodeRecoveryData()
    {
      return $this->QRCodeRecoveryData;
    }

    /**
     * @param QRCodeRecoveryData[] $QRCodeRecoveryData
     * @return \Axess\Dci4Wtp\ArrayOfQRCodeRecoveryData
     */
    public function setQRCodeRecoveryData(array $QRCodeRecoveryData = null)
    {
      $this->QRCodeRecoveryData = $QRCodeRecoveryData;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->QRCodeRecoveryData[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return QRCodeRecoveryData
     */
    public function offsetGet($offset)
    {
      return $this->QRCodeRecoveryData[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param QRCodeRecoveryData $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->QRCodeRecoveryData[] = $value;
      } else {
        $this->QRCodeRecoveryData[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->QRCodeRecoveryData[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return QRCodeRecoveryData Return the current element
     */
    public function current()
    {
      return current($this->QRCodeRecoveryData);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->QRCodeRecoveryData);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->QRCodeRecoveryData);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->QRCodeRecoveryData);
    }

    /**
     * Countable implementation
     *
     * @return QRCodeRecoveryData Return count of elements
     */
    public function count()
    {
      return count($this->QRCodeRecoveryData);
    }

}
