<?php

namespace Axess\Dci4Wtp;

class issueShoppingCart
{

    /**
     * @var D4WTPISSUESHOPCARTREQUEST $i_ctIssueShopCartReq
     */
    protected $i_ctIssueShopCartReq = null;

    /**
     * @param D4WTPISSUESHOPCARTREQUEST $i_ctIssueShopCartReq
     */
    public function __construct($i_ctIssueShopCartReq)
    {
      $this->i_ctIssueShopCartReq = $i_ctIssueShopCartReq;
    }

    /**
     * @return D4WTPISSUESHOPCARTREQUEST
     */
    public function getI_ctIssueShopCartReq()
    {
      return $this->i_ctIssueShopCartReq;
    }

    /**
     * @param D4WTPISSUESHOPCARTREQUEST $i_ctIssueShopCartReq
     * @return \Axess\Dci4Wtp\issueShoppingCart
     */
    public function setI_ctIssueShopCartReq($i_ctIssueShopCartReq)
    {
      $this->i_ctIssueShopCartReq = $i_ctIssueShopCartReq;
      return $this;
    }

}
