<?php

namespace Axess\Dci4Wtp;

class getArticleList2Response
{

    /**
     * @var D4WTPGETARTICLELIST2RESULT $getArticleList2Result
     */
    protected $getArticleList2Result = null;

    /**
     * @param D4WTPGETARTICLELIST2RESULT $getArticleList2Result
     */
    public function __construct($getArticleList2Result)
    {
      $this->getArticleList2Result = $getArticleList2Result;
    }

    /**
     * @return D4WTPGETARTICLELIST2RESULT
     */
    public function getGetArticleList2Result()
    {
      return $this->getArticleList2Result;
    }

    /**
     * @param D4WTPGETARTICLELIST2RESULT $getArticleList2Result
     * @return \Axess\Dci4Wtp\getArticleList2Response
     */
    public function setGetArticleList2Result($getArticleList2Result)
    {
      $this->getArticleList2Result = $getArticleList2Result;
      return $this;
    }

}
