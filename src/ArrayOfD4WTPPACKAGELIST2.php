<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPACKAGELIST2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPACKAGELIST2[] $D4WTPPACKAGELIST2
     */
    protected $D4WTPPACKAGELIST2 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPACKAGELIST2[]
     */
    public function getD4WTPPACKAGELIST2()
    {
      return $this->D4WTPPACKAGELIST2;
    }

    /**
     * @param D4WTPPACKAGELIST2[] $D4WTPPACKAGELIST2
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPACKAGELIST2
     */
    public function setD4WTPPACKAGELIST2(array $D4WTPPACKAGELIST2 = null)
    {
      $this->D4WTPPACKAGELIST2 = $D4WTPPACKAGELIST2;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPACKAGELIST2[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPACKAGELIST2
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPACKAGELIST2[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPACKAGELIST2 $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPACKAGELIST2[] = $value;
      } else {
        $this->D4WTPPACKAGELIST2[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPACKAGELIST2[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPACKAGELIST2 Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPACKAGELIST2);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPACKAGELIST2);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPACKAGELIST2);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPACKAGELIST2);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPACKAGELIST2 Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPACKAGELIST2);
    }

}
