<?php

namespace Axess\Dci4Wtp;

class D4WTPPKGTARIFFLIST2
{

    /**
     * @var ArrayOfD4WTPPKGTARIFFLISTDAY2 $ACTPKGTARIFFLISTDAY
     */
    protected $ACTPKGTARIFFLISTDAY = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPKGTARIFFLISTDAY2
     */
    public function getACTPKGTARIFFLISTDAY()
    {
      return $this->ACTPKGTARIFFLISTDAY;
    }

    /**
     * @param ArrayOfD4WTPPKGTARIFFLISTDAY2 $ACTPKGTARIFFLISTDAY
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2
     */
    public function setACTPKGTARIFFLISTDAY($ACTPKGTARIFFLISTDAY)
    {
      $this->ACTPKGTARIFFLISTDAY = $ACTPKGTARIFFLISTDAY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFFLIST2
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
