<?php

namespace Axess\Dci4Wtp;

class D4WTPREPLACEDTICKET
{

    /**
     * @var float $FTARIFF
     */
    protected $FTARIFF = null;

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var float $NKASSANO
     */
    protected $NKASSANO = null;

    /**
     * @var float $NNULLINFOBIT
     */
    protected $NNULLINFOBIT = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIENNO
     */
    protected $NSERIENNO = null;

    /**
     * @var float $NTRANSNR
     */
    protected $NTRANSNR = null;

    /**
     * @var float $NUNICODENR
     */
    protected $NUNICODENR = null;

    /**
     * @var string $SZAUFTRAGEXT
     */
    protected $SZAUFTRAGEXT = null;

    /**
     * @var string $SZBINCODE
     */
    protected $SZBINCODE = null;

    /**
     * @var string $SZPRINTDATA
     */
    protected $SZPRINTDATA = null;

    /**
     * @var string $SZSEGMENTE
     */
    protected $SZSEGMENTE = null;

    /**
     * @var string $SZWAEHRUNG
     */
    protected $SZWAEHRUNG = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFTARIFF()
    {
      return $this->FTARIFF;
    }

    /**
     * @param float $FTARIFF
     * @return \Axess\Dci4Wtp\D4WTPREPLACEDTICKET
     */
    public function setFTARIFF($FTARIFF)
    {
      $this->FTARIFF = $FTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPREPLACEDTICKET
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKASSANO()
    {
      return $this->NKASSANO;
    }

    /**
     * @param float $NKASSANO
     * @return \Axess\Dci4Wtp\D4WTPREPLACEDTICKET
     */
    public function setNKASSANO($NKASSANO)
    {
      $this->NKASSANO = $NKASSANO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNULLINFOBIT()
    {
      return $this->NNULLINFOBIT;
    }

    /**
     * @param float $NNULLINFOBIT
     * @return \Axess\Dci4Wtp\D4WTPREPLACEDTICKET
     */
    public function setNNULLINFOBIT($NNULLINFOBIT)
    {
      $this->NNULLINFOBIT = $NNULLINFOBIT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPREPLACEDTICKET
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIENNO()
    {
      return $this->NSERIENNO;
    }

    /**
     * @param float $NSERIENNO
     * @return \Axess\Dci4Wtp\D4WTPREPLACEDTICKET
     */
    public function setNSERIENNO($NSERIENNO)
    {
      $this->NSERIENNO = $NSERIENNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNR()
    {
      return $this->NTRANSNR;
    }

    /**
     * @param float $NTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPREPLACEDTICKET
     */
    public function setNTRANSNR($NTRANSNR)
    {
      $this->NTRANSNR = $NTRANSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENR()
    {
      return $this->NUNICODENR;
    }

    /**
     * @param float $NUNICODENR
     * @return \Axess\Dci4Wtp\D4WTPREPLACEDTICKET
     */
    public function setNUNICODENR($NUNICODENR)
    {
      $this->NUNICODENR = $NUNICODENR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAUFTRAGEXT()
    {
      return $this->SZAUFTRAGEXT;
    }

    /**
     * @param string $SZAUFTRAGEXT
     * @return \Axess\Dci4Wtp\D4WTPREPLACEDTICKET
     */
    public function setSZAUFTRAGEXT($SZAUFTRAGEXT)
    {
      $this->SZAUFTRAGEXT = $SZAUFTRAGEXT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBINCODE()
    {
      return $this->SZBINCODE;
    }

    /**
     * @param string $SZBINCODE
     * @return \Axess\Dci4Wtp\D4WTPREPLACEDTICKET
     */
    public function setSZBINCODE($SZBINCODE)
    {
      $this->SZBINCODE = $SZBINCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPRINTDATA()
    {
      return $this->SZPRINTDATA;
    }

    /**
     * @param string $SZPRINTDATA
     * @return \Axess\Dci4Wtp\D4WTPREPLACEDTICKET
     */
    public function setSZPRINTDATA($SZPRINTDATA)
    {
      $this->SZPRINTDATA = $SZPRINTDATA;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSEGMENTE()
    {
      return $this->SZSEGMENTE;
    }

    /**
     * @param string $SZSEGMENTE
     * @return \Axess\Dci4Wtp\D4WTPREPLACEDTICKET
     */
    public function setSZSEGMENTE($SZSEGMENTE)
    {
      $this->SZSEGMENTE = $SZSEGMENTE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWAEHRUNG()
    {
      return $this->SZWAEHRUNG;
    }

    /**
     * @param string $SZWAEHRUNG
     * @return \Axess\Dci4Wtp\D4WTPREPLACEDTICKET
     */
    public function setSZWAEHRUNG($SZWAEHRUNG)
    {
      $this->SZWAEHRUNG = $SZWAEHRUNG;
      return $this;
    }

}
