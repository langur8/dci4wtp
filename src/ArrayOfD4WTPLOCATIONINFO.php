<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPLOCATIONINFO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPLOCATIONINFO[] $D4WTPLOCATIONINFO
     */
    protected $D4WTPLOCATIONINFO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPLOCATIONINFO[]
     */
    public function getD4WTPLOCATIONINFO()
    {
      return $this->D4WTPLOCATIONINFO;
    }

    /**
     * @param D4WTPLOCATIONINFO[] $D4WTPLOCATIONINFO
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPLOCATIONINFO
     */
    public function setD4WTPLOCATIONINFO(array $D4WTPLOCATIONINFO = null)
    {
      $this->D4WTPLOCATIONINFO = $D4WTPLOCATIONINFO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPLOCATIONINFO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPLOCATIONINFO
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPLOCATIONINFO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPLOCATIONINFO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPLOCATIONINFO[] = $value;
      } else {
        $this->D4WTPLOCATIONINFO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPLOCATIONINFO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPLOCATIONINFO Return the current element
     */
    public function current()
    {
      return current($this->D4WTPLOCATIONINFO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPLOCATIONINFO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPLOCATIONINFO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPLOCATIONINFO);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPLOCATIONINFO Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPLOCATIONINFO);
    }

}
