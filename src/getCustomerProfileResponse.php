<?php

namespace Axess\Dci4Wtp;

class getCustomerProfileResponse
{

    /**
     * @var D4WTPCUSTOMERPROFILERESULT $getCustomerProfileResult
     */
    protected $getCustomerProfileResult = null;

    /**
     * @param D4WTPCUSTOMERPROFILERESULT $getCustomerProfileResult
     */
    public function __construct($getCustomerProfileResult)
    {
      $this->getCustomerProfileResult = $getCustomerProfileResult;
    }

    /**
     * @return D4WTPCUSTOMERPROFILERESULT
     */
    public function getGetCustomerProfileResult()
    {
      return $this->getCustomerProfileResult;
    }

    /**
     * @param D4WTPCUSTOMERPROFILERESULT $getCustomerProfileResult
     * @return \Axess\Dci4Wtp\getCustomerProfileResponse
     */
    public function setGetCustomerProfileResult($getCustomerProfileResult)
    {
      $this->getCustomerProfileResult = $getCustomerProfileResult;
      return $this;
    }

}
