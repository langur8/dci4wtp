<?php

namespace Axess\Dci4Wtp;

class D4WTPEMONEYPAYINOUTREQ
{

    /**
     * @var float $FAMOUNT
     */
    protected $FAMOUNT = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    /**
     * @var string $SZWTPNUMBER
     */
    protected $SZWTPNUMBER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFAMOUNT()
    {
      return $this->FAMOUNT;
    }

    /**
     * @param float $FAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPEMONEYPAYINOUTREQ
     */
    public function setFAMOUNT($FAMOUNT)
    {
      $this->FAMOUNT = $FAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPEMONEYPAYINOUTREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPEMONEYPAYINOUTREQ
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNUMBER()
    {
      return $this->SZWTPNUMBER;
    }

    /**
     * @param string $SZWTPNUMBER
     * @return \Axess\Dci4Wtp\D4WTPEMONEYPAYINOUTREQ
     */
    public function setSZWTPNUMBER($SZWTPNUMBER)
    {
      $this->SZWTPNUMBER = $SZWTPNUMBER;
      return $this;
    }

}
