<?php

namespace Axess\Dci4Wtp;

class getEmoneyAccountDetails
{

    /**
     * @var D4WTPEMONEYACCOUNTDETAILSREQ $i_ctEmoneyAccountDetailsReq
     */
    protected $i_ctEmoneyAccountDetailsReq = null;

    /**
     * @param D4WTPEMONEYACCOUNTDETAILSREQ $i_ctEmoneyAccountDetailsReq
     */
    public function __construct($i_ctEmoneyAccountDetailsReq)
    {
      $this->i_ctEmoneyAccountDetailsReq = $i_ctEmoneyAccountDetailsReq;
    }

    /**
     * @return D4WTPEMONEYACCOUNTDETAILSREQ
     */
    public function getI_ctEmoneyAccountDetailsReq()
    {
      return $this->i_ctEmoneyAccountDetailsReq;
    }

    /**
     * @param D4WTPEMONEYACCOUNTDETAILSREQ $i_ctEmoneyAccountDetailsReq
     * @return \Axess\Dci4Wtp\getEmoneyAccountDetails
     */
    public function setI_ctEmoneyAccountDetailsReq($i_ctEmoneyAccountDetailsReq)
    {
      $this->i_ctEmoneyAccountDetailsReq = $i_ctEmoneyAccountDetailsReq;
      return $this;
    }

}
