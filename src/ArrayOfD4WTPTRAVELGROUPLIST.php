<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPTRAVELGROUPLIST implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPTRAVELGROUPLIST[] $D4WTPTRAVELGROUPLIST
     */
    protected $D4WTPTRAVELGROUPLIST = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPTRAVELGROUPLIST[]
     */
    public function getD4WTPTRAVELGROUPLIST()
    {
      return $this->D4WTPTRAVELGROUPLIST;
    }

    /**
     * @param D4WTPTRAVELGROUPLIST[] $D4WTPTRAVELGROUPLIST
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPTRAVELGROUPLIST
     */
    public function setD4WTPTRAVELGROUPLIST(array $D4WTPTRAVELGROUPLIST = null)
    {
      $this->D4WTPTRAVELGROUPLIST = $D4WTPTRAVELGROUPLIST;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPTRAVELGROUPLIST[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPTRAVELGROUPLIST
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPTRAVELGROUPLIST[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPTRAVELGROUPLIST $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPTRAVELGROUPLIST[] = $value;
      } else {
        $this->D4WTPTRAVELGROUPLIST[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPTRAVELGROUPLIST[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPTRAVELGROUPLIST Return the current element
     */
    public function current()
    {
      return current($this->D4WTPTRAVELGROUPLIST);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPTRAVELGROUPLIST);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPTRAVELGROUPLIST);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPTRAVELGROUPLIST);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPTRAVELGROUPLIST Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPTRAVELGROUPLIST);
    }

}
