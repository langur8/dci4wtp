<?php

namespace Axess\Dci4Wtp;

class loginNamedUser
{

    /**
     * @var D4WTPLOGINNAMEDUSERREQUEST $i_ctLoginNamedUserRequest
     */
    protected $i_ctLoginNamedUserRequest = null;

    /**
     * @param D4WTPLOGINNAMEDUSERREQUEST $i_ctLoginNamedUserRequest
     */
    public function __construct($i_ctLoginNamedUserRequest)
    {
      $this->i_ctLoginNamedUserRequest = $i_ctLoginNamedUserRequest;
    }

    /**
     * @return D4WTPLOGINNAMEDUSERREQUEST
     */
    public function getI_ctLoginNamedUserRequest()
    {
      return $this->i_ctLoginNamedUserRequest;
    }

    /**
     * @param D4WTPLOGINNAMEDUSERREQUEST $i_ctLoginNamedUserRequest
     * @return \Axess\Dci4Wtp\loginNamedUser
     */
    public function setI_ctLoginNamedUserRequest($i_ctLoginNamedUserRequest)
    {
      $this->i_ctLoginNamedUserRequest = $i_ctLoginNamedUserRequest;
      return $this;
    }

}
