<?php

namespace Axess\Dci4Wtp;

class D4WTPCONTNGNTTICKET
{

    /**
     * @var float $NCONTINGENTNR
     */
    protected $NCONTINGENTNR = null;

    /**
     * @var float $NTICKETCOUNT
     */
    protected $NTICKETCOUNT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNR()
    {
      return $this->NCONTINGENTNR;
    }

    /**
     * @param float $NCONTINGENTNR
     * @return \Axess\Dci4Wtp\D4WTPCONTNGNTTICKET
     */
    public function setNCONTINGENTNR($NCONTINGENTNR)
    {
      $this->NCONTINGENTNR = $NCONTINGENTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETCOUNT()
    {
      return $this->NTICKETCOUNT;
    }

    /**
     * @param float $NTICKETCOUNT
     * @return \Axess\Dci4Wtp\D4WTPCONTNGNTTICKET
     */
    public function setNTICKETCOUNT($NTICKETCOUNT)
    {
      $this->NTICKETCOUNT = $NTICKETCOUNT;
      return $this;
    }

}
