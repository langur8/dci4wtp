<?php

namespace Axess\Dci4Wtp;

class getOpenCashierShiftsResponse
{

    /**
     * @var D4WTPGETOPENSHIFTSRESULT $getOpenCashierShiftsResult
     */
    protected $getOpenCashierShiftsResult = null;

    /**
     * @param D4WTPGETOPENSHIFTSRESULT $getOpenCashierShiftsResult
     */
    public function __construct($getOpenCashierShiftsResult)
    {
      $this->getOpenCashierShiftsResult = $getOpenCashierShiftsResult;
    }

    /**
     * @return D4WTPGETOPENSHIFTSRESULT
     */
    public function getGetOpenCashierShiftsResult()
    {
      return $this->getOpenCashierShiftsResult;
    }

    /**
     * @param D4WTPGETOPENSHIFTSRESULT $getOpenCashierShiftsResult
     * @return \Axess\Dci4Wtp\getOpenCashierShiftsResponse
     */
    public function setGetOpenCashierShiftsResult($getOpenCashierShiftsResult)
    {
      $this->getOpenCashierShiftsResult = $getOpenCashierShiftsResult;
      return $this;
    }

}
