<?php

namespace Axess\Dci4Wtp;

class D4WTPDECODE64WTPNORESULT
{

    /**
     * @var float $NDATTRAGTYPNR
     */
    protected $NDATTRAGTYPNR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNDATTRAGTYPNR()
    {
      return $this->NDATTRAGTYPNR;
    }

    /**
     * @param float $NDATTRAGTYPNR
     * @return \Axess\Dci4Wtp\D4WTPDECODE64WTPNORESULT
     */
    public function setNDATTRAGTYPNR($NDATTRAGTYPNR)
    {
      $this->NDATTRAGTYPNR = $NDATTRAGTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPDECODE64WTPNORESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPDECODE64WTPNORESULT
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPDECODE64WTPNORESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPDECODE64WTPNORESULT
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

}
