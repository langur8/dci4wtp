<?php

namespace Axess\Dci4Wtp;

class getAdditionalCashiers
{

    /**
     * @var D4WTPGETADDCASHIERSREQ $i_ctGetAddCashiersReq
     */
    protected $i_ctGetAddCashiersReq = null;

    /**
     * @param D4WTPGETADDCASHIERSREQ $i_ctGetAddCashiersReq
     */
    public function __construct($i_ctGetAddCashiersReq)
    {
      $this->i_ctGetAddCashiersReq = $i_ctGetAddCashiersReq;
    }

    /**
     * @return D4WTPGETADDCASHIERSREQ
     */
    public function getI_ctGetAddCashiersReq()
    {
      return $this->i_ctGetAddCashiersReq;
    }

    /**
     * @param D4WTPGETADDCASHIERSREQ $i_ctGetAddCashiersReq
     * @return \Axess\Dci4Wtp\getAdditionalCashiers
     */
    public function setI_ctGetAddCashiersReq($i_ctGetAddCashiersReq)
    {
      $this->i_ctGetAddCashiersReq = $i_ctGetAddCashiersReq;
      return $this;
    }

}
