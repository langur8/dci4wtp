<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPPACKAGELIST implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPPACKAGELIST[] $D4WTPPACKAGELIST
     */
    protected $D4WTPPACKAGELIST = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPACKAGELIST[]
     */
    public function getD4WTPPACKAGELIST()
    {
      return $this->D4WTPPACKAGELIST;
    }

    /**
     * @param D4WTPPACKAGELIST[] $D4WTPPACKAGELIST
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPPACKAGELIST
     */
    public function setD4WTPPACKAGELIST(array $D4WTPPACKAGELIST = null)
    {
      $this->D4WTPPACKAGELIST = $D4WTPPACKAGELIST;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPPACKAGELIST[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPPACKAGELIST
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPPACKAGELIST[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPPACKAGELIST $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPPACKAGELIST[] = $value;
      } else {
        $this->D4WTPPACKAGELIST[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPPACKAGELIST[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPPACKAGELIST Return the current element
     */
    public function current()
    {
      return current($this->D4WTPPACKAGELIST);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPPACKAGELIST);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPPACKAGELIST);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPPACKAGELIST);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPPACKAGELIST Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPPACKAGELIST);
    }

}
