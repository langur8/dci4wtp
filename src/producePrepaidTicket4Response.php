<?php

namespace Axess\Dci4Wtp;

class producePrepaidTicket4Response
{

    /**
     * @var D4WTPRODUCEPREPAIDTICKETRES4 $producePrepaidTicket4Result
     */
    protected $producePrepaidTicket4Result = null;

    /**
     * @param D4WTPRODUCEPREPAIDTICKETRES4 $producePrepaidTicket4Result
     */
    public function __construct($producePrepaidTicket4Result)
    {
      $this->producePrepaidTicket4Result = $producePrepaidTicket4Result;
    }

    /**
     * @return D4WTPRODUCEPREPAIDTICKETRES4
     */
    public function getProducePrepaidTicket4Result()
    {
      return $this->producePrepaidTicket4Result;
    }

    /**
     * @param D4WTPRODUCEPREPAIDTICKETRES4 $producePrepaidTicket4Result
     * @return \Axess\Dci4Wtp\producePrepaidTicket4Response
     */
    public function setProducePrepaidTicket4Result($producePrepaidTicket4Result)
    {
      $this->producePrepaidTicket4Result = $producePrepaidTicket4Result;
      return $this;
    }

}
