<?php

namespace Axess\Dci4Wtp;

class D4WTPDURATIONINFO
{

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var float $BCHECKIN
     */
    protected $BCHECKIN = null;

    /**
     * @var float $BISSEASON
     */
    protected $BISSEASON = null;

    /**
     * @var float $BTECHDESK
     */
    protected $BTECHDESK = null;

    /**
     * @var float $BWEB
     */
    protected $BWEB = null;

    /**
     * @var \DateTime $DTABSTO
     */
    protected $DTABSTO = null;

    /**
     * @var float $NDURATIONINDAYS
     */
    protected $NDURATIONINDAYS = null;

    /**
     * @var float $NDURATIONINMINUTES
     */
    protected $NDURATIONINMINUTES = null;

    /**
     * @var float $NDURATIONINMONTHS
     */
    protected $NDURATIONINMONTHS = null;

    /**
     * @var float $NDURATIONNR
     */
    protected $NDURATIONNR = null;

    /**
     * @var float $NNRKREISART
     */
    protected $NNRKREISART = null;

    /**
     * @var float $NSORTNR
     */
    protected $NSORTNR = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBCHECKIN()
    {
      return $this->BCHECKIN;
    }

    /**
     * @param float $BCHECKIN
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setBCHECKIN($BCHECKIN)
    {
      $this->BCHECKIN = $BCHECKIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISSEASON()
    {
      return $this->BISSEASON;
    }

    /**
     * @param float $BISSEASON
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setBISSEASON($BISSEASON)
    {
      $this->BISSEASON = $BISSEASON;
      return $this;
    }

    /**
     * @return float
     */
    public function getBTECHDESK()
    {
      return $this->BTECHDESK;
    }

    /**
     * @param float $BTECHDESK
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setBTECHDESK($BTECHDESK)
    {
      $this->BTECHDESK = $BTECHDESK;
      return $this;
    }

    /**
     * @return float
     */
    public function getBWEB()
    {
      return $this->BWEB;
    }

    /**
     * @param float $BWEB
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setBWEB($BWEB)
    {
      $this->BWEB = $BWEB;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDTABSTO()
    {
      if ($this->DTABSTO == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DTABSTO);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DTABSTO
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setDTABSTO(\DateTime $DTABSTO = null)
    {
      if ($DTABSTO == null) {
       $this->DTABSTO = null;
      } else {
        $this->DTABSTO = $DTABSTO->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return float
     */
    public function getNDURATIONINDAYS()
    {
      return $this->NDURATIONINDAYS;
    }

    /**
     * @param float $NDURATIONINDAYS
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setNDURATIONINDAYS($NDURATIONINDAYS)
    {
      $this->NDURATIONINDAYS = $NDURATIONINDAYS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDURATIONINMINUTES()
    {
      return $this->NDURATIONINMINUTES;
    }

    /**
     * @param float $NDURATIONINMINUTES
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setNDURATIONINMINUTES($NDURATIONINMINUTES)
    {
      $this->NDURATIONINMINUTES = $NDURATIONINMINUTES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDURATIONINMONTHS()
    {
      return $this->NDURATIONINMONTHS;
    }

    /**
     * @param float $NDURATIONINMONTHS
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setNDURATIONINMONTHS($NDURATIONINMONTHS)
    {
      $this->NDURATIONINMONTHS = $NDURATIONINMONTHS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDURATIONNR()
    {
      return $this->NDURATIONNR;
    }

    /**
     * @param float $NDURATIONNR
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setNDURATIONNR($NDURATIONNR)
    {
      $this->NDURATIONNR = $NDURATIONNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNRKREISART()
    {
      return $this->NNRKREISART;
    }

    /**
     * @param float $NNRKREISART
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setNNRKREISART($NNRKREISART)
    {
      $this->NNRKREISART = $NNRKREISART;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNR()
    {
      return $this->NSORTNR;
    }

    /**
     * @param float $NSORTNR
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setNSORTNR($NSORTNR)
    {
      $this->NSORTNR = $NSORTNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPDURATIONINFO
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
