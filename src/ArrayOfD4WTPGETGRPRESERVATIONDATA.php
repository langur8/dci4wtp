<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPGETGRPRESERVATIONDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPGETGRPRESERVATIONDATA[] $D4WTPGETGRPRESERVATIONDATA
     */
    protected $D4WTPGETGRPRESERVATIONDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPGETGRPRESERVATIONDATA[]
     */
    public function getD4WTPGETGRPRESERVATIONDATA()
    {
      return $this->D4WTPGETGRPRESERVATIONDATA;
    }

    /**
     * @param D4WTPGETGRPRESERVATIONDATA[] $D4WTPGETGRPRESERVATIONDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPGETGRPRESERVATIONDATA
     */
    public function setD4WTPGETGRPRESERVATIONDATA(array $D4WTPGETGRPRESERVATIONDATA = null)
    {
      $this->D4WTPGETGRPRESERVATIONDATA = $D4WTPGETGRPRESERVATIONDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPGETGRPRESERVATIONDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPGETGRPRESERVATIONDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPGETGRPRESERVATIONDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPGETGRPRESERVATIONDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPGETGRPRESERVATIONDATA[] = $value;
      } else {
        $this->D4WTPGETGRPRESERVATIONDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPGETGRPRESERVATIONDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPGETGRPRESERVATIONDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPGETGRPRESERVATIONDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPGETGRPRESERVATIONDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPGETGRPRESERVATIONDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPGETGRPRESERVATIONDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPGETGRPRESERVATIONDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPGETGRPRESERVATIONDATA);
    }

}
