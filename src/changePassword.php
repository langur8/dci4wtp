<?php

namespace Axess\Dci4Wtp;

class changePassword
{

    /**
     * @var D4WTPCHANGEPWREQUEST $i_ctChangePWReq
     */
    protected $i_ctChangePWReq = null;

    /**
     * @param D4WTPCHANGEPWREQUEST $i_ctChangePWReq
     */
    public function __construct($i_ctChangePWReq)
    {
      $this->i_ctChangePWReq = $i_ctChangePWReq;
    }

    /**
     * @return D4WTPCHANGEPWREQUEST
     */
    public function getI_ctChangePWReq()
    {
      return $this->i_ctChangePWReq;
    }

    /**
     * @param D4WTPCHANGEPWREQUEST $i_ctChangePWReq
     * @return \Axess\Dci4Wtp\changePassword
     */
    public function setI_ctChangePWReq($i_ctChangePWReq)
    {
      $this->i_ctChangePWReq = $i_ctChangePWReq;
      return $this;
    }

}
