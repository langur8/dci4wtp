<?php

namespace Axess\Dci4Wtp;

class getCompensationTicket
{

    /**
     * @var D4WTPGETCOMTICREQ $i_ctgetrequest
     */
    protected $i_ctgetrequest = null;

    /**
     * @param D4WTPGETCOMTICREQ $i_ctgetrequest
     */
    public function __construct($i_ctgetrequest)
    {
      $this->i_ctgetrequest = $i_ctgetrequest;
    }

    /**
     * @return D4WTPGETCOMTICREQ
     */
    public function getI_ctgetrequest()
    {
      return $this->i_ctgetrequest;
    }

    /**
     * @param D4WTPGETCOMTICREQ $i_ctgetrequest
     * @return \Axess\Dci4Wtp\getCompensationTicket
     */
    public function setI_ctgetrequest($i_ctgetrequest)
    {
      $this->i_ctgetrequest = $i_ctgetrequest;
      return $this;
    }

}
