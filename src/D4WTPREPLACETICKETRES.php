<?php

namespace Axess\Dci4Wtp;

class D4WTPREPLACETICKETRES
{

    /**
     * @var D4WTPREPLACEDTICKET $CTREPLACEDTICKET
     */
    protected $CTREPLACEDTICKET = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPREPLACEDTICKET
     */
    public function getCTREPLACEDTICKET()
    {
      return $this->CTREPLACEDTICKET;
    }

    /**
     * @param D4WTPREPLACEDTICKET $CTREPLACEDTICKET
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETRES
     */
    public function setCTREPLACEDTICKET($CTREPLACEDTICKET)
    {
      $this->CTREPLACEDTICKET = $CTREPLACEDTICKET;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPREPLACETICKETRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
