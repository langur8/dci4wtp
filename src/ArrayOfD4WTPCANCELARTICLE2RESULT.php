<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPCANCELARTICLE2RESULT implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPCANCELARTICLE2RESULT[] $D4WTPCANCELARTICLE2RESULT
     */
    protected $D4WTPCANCELARTICLE2RESULT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCANCELARTICLE2RESULT[]
     */
    public function getD4WTPCANCELARTICLE2RESULT()
    {
      return $this->D4WTPCANCELARTICLE2RESULT;
    }

    /**
     * @param D4WTPCANCELARTICLE2RESULT[] $D4WTPCANCELARTICLE2RESULT
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPCANCELARTICLE2RESULT
     */
    public function setD4WTPCANCELARTICLE2RESULT(array $D4WTPCANCELARTICLE2RESULT = null)
    {
      $this->D4WTPCANCELARTICLE2RESULT = $D4WTPCANCELARTICLE2RESULT;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPCANCELARTICLE2RESULT[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPCANCELARTICLE2RESULT
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPCANCELARTICLE2RESULT[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPCANCELARTICLE2RESULT $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPCANCELARTICLE2RESULT[] = $value;
      } else {
        $this->D4WTPCANCELARTICLE2RESULT[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPCANCELARTICLE2RESULT[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPCANCELARTICLE2RESULT Return the current element
     */
    public function current()
    {
      return current($this->D4WTPCANCELARTICLE2RESULT);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPCANCELARTICLE2RESULT);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPCANCELARTICLE2RESULT);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPCANCELARTICLE2RESULT);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPCANCELARTICLE2RESULT Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPCANCELARTICLE2RESULT);
    }

}
