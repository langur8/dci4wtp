<?php

namespace Axess\Dci4Wtp;

class D4WTPRENTALITEMTARIFF2
{

    /**
     * @var float $FTARIFF
     */
    protected $FTARIFF = null;

    /**
     * @var float $NARCNR
     */
    protected $NARCNR = null;

    /**
     * @var float $NARCPROJNR
     */
    protected $NARCPROJNR = null;

    /**
     * @var float $NAVAILABLEITEMCOUNT
     */
    protected $NAVAILABLEITEMCOUNT = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPACKAGENR
     */
    protected $NPACKAGENR = null;

    /**
     * @var float $NPACKAGEPOSNR
     */
    protected $NPACKAGEPOSNR = null;

    /**
     * @var float $NRENTALITEMNR
     */
    protected $NRENTALITEMNR = null;

    /**
     * @var float $NRENTALITEMTYPENR
     */
    protected $NRENTALITEMTYPENR = null;

    /**
     * @var float $NRENTALPERSTYPENR
     */
    protected $NRENTALPERSTYPENR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZPACKAGENAME
     */
    protected $SZPACKAGENAME = null;

    /**
     * @var string $SZRENTALITEMNAME
     */
    protected $SZRENTALITEMNAME = null;

    /**
     * @var string $SZRENTALITEMTYPENAME
     */
    protected $SZRENTALITEMTYPENAME = null;

    /**
     * @var string $SZRENTALPERSTYPENAME
     */
    protected $SZRENTALPERSTYPENAME = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFTARIFF()
    {
      return $this->FTARIFF;
    }

    /**
     * @param float $FTARIFF
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setFTARIFF($FTARIFF)
    {
      $this->FTARIFF = $FTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARCNR()
    {
      return $this->NARCNR;
    }

    /**
     * @param float $NARCNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setNARCNR($NARCNR)
    {
      $this->NARCNR = $NARCNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARCPROJNR()
    {
      return $this->NARCPROJNR;
    }

    /**
     * @param float $NARCPROJNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setNARCPROJNR($NARCPROJNR)
    {
      $this->NARCPROJNR = $NARCPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAVAILABLEITEMCOUNT()
    {
      return $this->NAVAILABLEITEMCOUNT;
    }

    /**
     * @param float $NAVAILABLEITEMCOUNT
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setNAVAILABLEITEMCOUNT($NAVAILABLEITEMCOUNT)
    {
      $this->NAVAILABLEITEMCOUNT = $NAVAILABLEITEMCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKAGENR()
    {
      return $this->NPACKAGENR;
    }

    /**
     * @param float $NPACKAGENR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setNPACKAGENR($NPACKAGENR)
    {
      $this->NPACKAGENR = $NPACKAGENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKAGEPOSNR()
    {
      return $this->NPACKAGEPOSNR;
    }

    /**
     * @param float $NPACKAGEPOSNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setNPACKAGEPOSNR($NPACKAGEPOSNR)
    {
      $this->NPACKAGEPOSNR = $NPACKAGEPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNR()
    {
      return $this->NRENTALITEMNR;
    }

    /**
     * @param float $NRENTALITEMNR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setNRENTALITEMNR($NRENTALITEMNR)
    {
      $this->NRENTALITEMNR = $NRENTALITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMTYPENR()
    {
      return $this->NRENTALITEMTYPENR;
    }

    /**
     * @param float $NRENTALITEMTYPENR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setNRENTALITEMTYPENR($NRENTALITEMTYPENR)
    {
      $this->NRENTALITEMTYPENR = $NRENTALITEMTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPERSTYPENR()
    {
      return $this->NRENTALPERSTYPENR;
    }

    /**
     * @param float $NRENTALPERSTYPENR
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setNRENTALPERSTYPENR($NRENTALPERSTYPENR)
    {
      $this->NRENTALPERSTYPENR = $NRENTALPERSTYPENR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPACKAGENAME()
    {
      return $this->SZPACKAGENAME;
    }

    /**
     * @param string $SZPACKAGENAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setSZPACKAGENAME($SZPACKAGENAME)
    {
      $this->SZPACKAGENAME = $SZPACKAGENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRENTALITEMNAME()
    {
      return $this->SZRENTALITEMNAME;
    }

    /**
     * @param string $SZRENTALITEMNAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setSZRENTALITEMNAME($SZRENTALITEMNAME)
    {
      $this->SZRENTALITEMNAME = $SZRENTALITEMNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRENTALITEMTYPENAME()
    {
      return $this->SZRENTALITEMTYPENAME;
    }

    /**
     * @param string $SZRENTALITEMTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setSZRENTALITEMTYPENAME($SZRENTALITEMTYPENAME)
    {
      $this->SZRENTALITEMTYPENAME = $SZRENTALITEMTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRENTALPERSTYPENAME()
    {
      return $this->SZRENTALPERSTYPENAME;
    }

    /**
     * @param string $SZRENTALPERSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setSZRENTALPERSTYPENAME($SZRENTALPERSTYPENAME)
    {
      $this->SZRENTALPERSTYPENAME = $SZRENTALPERSTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPRENTALITEMTARIFF2
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
