<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETDATAREQUEST
{

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZACCEPTANCENO
     */
    protected $SZACCEPTANCENO = null;

    /**
     * @var string $SZCHECKSUM
     */
    protected $SZCHECKSUM = null;

    /**
     * @var string $SZCHIPID
     */
    protected $SZCHIPID = null;

    /**
     * @var string $SZCOUNTRYCODE
     */
    protected $SZCOUNTRYCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATAREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZACCEPTANCENO()
    {
      return $this->SZACCEPTANCENO;
    }

    /**
     * @param string $SZACCEPTANCENO
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATAREQUEST
     */
    public function setSZACCEPTANCENO($SZACCEPTANCENO)
    {
      $this->SZACCEPTANCENO = $SZACCEPTANCENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCHECKSUM()
    {
      return $this->SZCHECKSUM;
    }

    /**
     * @param string $SZCHECKSUM
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATAREQUEST
     */
    public function setSZCHECKSUM($SZCHECKSUM)
    {
      $this->SZCHECKSUM = $SZCHECKSUM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCHIPID()
    {
      return $this->SZCHIPID;
    }

    /**
     * @param string $SZCHIPID
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATAREQUEST
     */
    public function setSZCHIPID($SZCHIPID)
    {
      $this->SZCHIPID = $SZCHIPID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCOUNTRYCODE()
    {
      return $this->SZCOUNTRYCODE;
    }

    /**
     * @param string $SZCOUNTRYCODE
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATAREQUEST
     */
    public function setSZCOUNTRYCODE($SZCOUNTRYCODE)
    {
      $this->SZCOUNTRYCODE = $SZCOUNTRYCODE;
      return $this;
    }

}
