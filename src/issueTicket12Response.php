<?php

namespace Axess\Dci4Wtp;

class issueTicket12Response
{

    /**
     * @var D4WTPISSUETICKET10RESULT $issueTicket12Result
     */
    protected $issueTicket12Result = null;

    /**
     * @param D4WTPISSUETICKET10RESULT $issueTicket12Result
     */
    public function __construct($issueTicket12Result)
    {
      $this->issueTicket12Result = $issueTicket12Result;
    }

    /**
     * @return D4WTPISSUETICKET10RESULT
     */
    public function getIssueTicket12Result()
    {
      return $this->issueTicket12Result;
    }

    /**
     * @param D4WTPISSUETICKET10RESULT $issueTicket12Result
     * @return \Axess\Dci4Wtp\issueTicket12Response
     */
    public function setIssueTicket12Result($issueTicket12Result)
    {
      $this->issueTicket12Result = $issueTicket12Result;
      return $this;
    }

}
