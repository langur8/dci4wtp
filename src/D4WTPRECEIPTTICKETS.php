<?php

namespace Axess\Dci4Wtp;

class D4WTPRECEIPTTICKETS
{

    /**
     * @var float $NBLOCKNO
     */
    protected $NBLOCKNO = null;

    /**
     * @var float $NGROSSTARIFF
     */
    protected $NGROSSTARIFF = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NNETTARIFF
     */
    protected $NNETTARIFF = null;

    /**
     * @var float $NPERSTYPENO
     */
    protected $NPERSTYPENO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var float $NUNICODENO
     */
    protected $NUNICODENO = null;

    /**
     * @var float $NVATRATE
     */
    protected $NVATRATE = null;

    /**
     * @var float $NVATVALUE
     */
    protected $NVATVALUE = null;

    /**
     * @var string $SZDATACARRIERTYPE
     */
    protected $SZDATACARRIERTYPE = null;

    /**
     * @var string $SZPERSTYPENAME
     */
    protected $SZPERSTYPENAME = null;

    /**
     * @var string $SZPERSTYPESHORTNAME
     */
    protected $SZPERSTYPESHORTNAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZTICKETTYPENAME
     */
    protected $SZTICKETTYPENAME = null;

    /**
     * @var string $SZTICKETTYPESHORTNAME
     */
    protected $SZTICKETTYPESHORTNAME = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNBLOCKNO()
    {
      return $this->NBLOCKNO;
    }

    /**
     * @param float $NBLOCKNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setNBLOCKNO($NBLOCKNO)
    {
      $this->NBLOCKNO = $NBLOCKNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNGROSSTARIFF()
    {
      return $this->NGROSSTARIFF;
    }

    /**
     * @param float $NGROSSTARIFF
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setNGROSSTARIFF($NGROSSTARIFF)
    {
      $this->NGROSSTARIFF = $NGROSSTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNETTARIFF()
    {
      return $this->NNETTARIFF;
    }

    /**
     * @param float $NNETTARIFF
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setNNETTARIFF($NNETTARIFF)
    {
      $this->NNETTARIFF = $NNETTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSTYPENO()
    {
      return $this->NPERSTYPENO;
    }

    /**
     * @param float $NPERSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setNPERSTYPENO($NPERSTYPENO)
    {
      $this->NPERSTYPENO = $NPERSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENO()
    {
      return $this->NUNICODENO;
    }

    /**
     * @param float $NUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setNUNICODENO($NUNICODENO)
    {
      $this->NUNICODENO = $NUNICODENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATRATE()
    {
      return $this->NVATRATE;
    }

    /**
     * @param float $NVATRATE
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setNVATRATE($NVATRATE)
    {
      $this->NVATRATE = $NVATRATE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATVALUE()
    {
      return $this->NVATVALUE;
    }

    /**
     * @param float $NVATVALUE
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setNVATVALUE($NVATVALUE)
    {
      $this->NVATVALUE = $NVATVALUE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATACARRIERTYPE()
    {
      return $this->SZDATACARRIERTYPE;
    }

    /**
     * @param string $SZDATACARRIERTYPE
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setSZDATACARRIERTYPE($SZDATACARRIERTYPE)
    {
      $this->SZDATACARRIERTYPE = $SZDATACARRIERTYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSTYPENAME()
    {
      return $this->SZPERSTYPENAME;
    }

    /**
     * @param string $SZPERSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setSZPERSTYPENAME($SZPERSTYPENAME)
    {
      $this->SZPERSTYPENAME = $SZPERSTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSTYPESHORTNAME()
    {
      return $this->SZPERSTYPESHORTNAME;
    }

    /**
     * @param string $SZPERSTYPESHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setSZPERSTYPESHORTNAME($SZPERSTYPESHORTNAME)
    {
      $this->SZPERSTYPESHORTNAME = $SZPERSTYPESHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPENAME()
    {
      return $this->SZTICKETTYPENAME;
    }

    /**
     * @param string $SZTICKETTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setSZTICKETTYPENAME($SZTICKETTYPENAME)
    {
      $this->SZTICKETTYPENAME = $SZTICKETTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPESHORTNAME()
    {
      return $this->SZTICKETTYPESHORTNAME;
    }

    /**
     * @param string $SZTICKETTYPESHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setSZTICKETTYPESHORTNAME($SZTICKETTYPESHORTNAME)
    {
      $this->SZTICKETTYPESHORTNAME = $SZTICKETTYPESHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTTICKETS
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
