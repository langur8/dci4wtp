<?php

namespace Axess\Dci4Wtp;

class getPackageTariffList4Response
{

    /**
     * @var D4WTPPKGTARIFFLIST4RESULT $getPackageTariffList4Result
     */
    protected $getPackageTariffList4Result = null;

    /**
     * @param D4WTPPKGTARIFFLIST4RESULT $getPackageTariffList4Result
     */
    public function __construct($getPackageTariffList4Result)
    {
      $this->getPackageTariffList4Result = $getPackageTariffList4Result;
    }

    /**
     * @return D4WTPPKGTARIFFLIST4RESULT
     */
    public function getGetPackageTariffList4Result()
    {
      return $this->getPackageTariffList4Result;
    }

    /**
     * @param D4WTPPKGTARIFFLIST4RESULT $getPackageTariffList4Result
     * @return \Axess\Dci4Wtp\getPackageTariffList4Response
     */
    public function setGetPackageTariffList4Result($getPackageTariffList4Result)
    {
      $this->getPackageTariffList4Result = $getPackageTariffList4Result;
      return $this;
    }

}
