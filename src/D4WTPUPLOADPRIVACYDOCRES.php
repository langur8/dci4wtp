<?php

namespace Axess\Dci4Wtp;

class D4WTPUPLOADPRIVACYDOCRES
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZCREATEDDOCPATH
     */
    protected $SZCREATEDDOCPATH = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCREATEDDOCPATH()
    {
      return $this->SZCREATEDDOCPATH;
    }

    /**
     * @param string $SZCREATEDDOCPATH
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCRES
     */
    public function setSZCREATEDDOCPATH($SZCREATEDDOCPATH)
    {
      $this->SZCREATEDDOCPATH = $SZCREATEDDOCPATH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPUPLOADPRIVACYDOCRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
