<?php

namespace Axess\Dci4Wtp;

class getRoutesStationsResponse
{

    /**
     * @var D4WTPGETROUTESSTATIONSRESULT $getRoutesStationsResult
     */
    protected $getRoutesStationsResult = null;

    /**
     * @param D4WTPGETROUTESSTATIONSRESULT $getRoutesStationsResult
     */
    public function __construct($getRoutesStationsResult)
    {
      $this->getRoutesStationsResult = $getRoutesStationsResult;
    }

    /**
     * @return D4WTPGETROUTESSTATIONSRESULT
     */
    public function getGetRoutesStationsResult()
    {
      return $this->getRoutesStationsResult;
    }

    /**
     * @param D4WTPGETROUTESSTATIONSRESULT $getRoutesStationsResult
     * @return \Axess\Dci4Wtp\getRoutesStationsResponse
     */
    public function setGetRoutesStationsResult($getRoutesStationsResult)
    {
      $this->getRoutesStationsResult = $getRoutesStationsResult;
      return $this;
    }

}
