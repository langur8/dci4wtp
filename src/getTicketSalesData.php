<?php

namespace Axess\Dci4Wtp;

class getTicketSalesData
{

    /**
     * @var D4WTPSALESDATAREQUEST $i_ctSalesDataReq
     */
    protected $i_ctSalesDataReq = null;

    /**
     * @param D4WTPSALESDATAREQUEST $i_ctSalesDataReq
     */
    public function __construct($i_ctSalesDataReq)
    {
      $this->i_ctSalesDataReq = $i_ctSalesDataReq;
    }

    /**
     * @return D4WTPSALESDATAREQUEST
     */
    public function getI_ctSalesDataReq()
    {
      return $this->i_ctSalesDataReq;
    }

    /**
     * @param D4WTPSALESDATAREQUEST $i_ctSalesDataReq
     * @return \Axess\Dci4Wtp\getTicketSalesData
     */
    public function setI_ctSalesDataReq($i_ctSalesDataReq)
    {
      $this->i_ctSalesDataReq = $i_ctSalesDataReq;
      return $this;
    }

}
