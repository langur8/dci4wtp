<?php

namespace Axess\Dci4Wtp;

class moveCash4CashierShift
{

    /**
     * @var D4WTPCASH4CASHIERSHIFTREQ $i_ctCashReq
     */
    protected $i_ctCashReq = null;

    /**
     * @param D4WTPCASH4CASHIERSHIFTREQ $i_ctCashReq
     */
    public function __construct($i_ctCashReq)
    {
      $this->i_ctCashReq = $i_ctCashReq;
    }

    /**
     * @return D4WTPCASH4CASHIERSHIFTREQ
     */
    public function getI_ctCashReq()
    {
      return $this->i_ctCashReq;
    }

    /**
     * @param D4WTPCASH4CASHIERSHIFTREQ $i_ctCashReq
     * @return \Axess\Dci4Wtp\moveCash4CashierShift
     */
    public function setI_ctCashReq($i_ctCashReq)
    {
      $this->i_ctCashReq = $i_ctCashReq;
      return $this;
    }

}
