<?php

namespace Axess\Dci4Wtp;

class D4WTPWARE3
{

    /**
     * @var ArrayOfD4WTPWAREFEATURES $ACTFEATURES
     */
    protected $ACTFEATURES = null;

    /**
     * @var float $BISMASTER
     */
    protected $BISMASTER = null;

    /**
     * @var float $BPREISVARIABEL
     */
    protected $BPREISVARIABEL = null;

    /**
     * @var float $NPARENTWARENR
     */
    protected $NPARENTWARENR = null;

    /**
     * @var float $NSORTNR
     */
    protected $NSORTNR = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NVATAMOUNT
     */
    protected $NVATAMOUNT = null;

    /**
     * @var float $NVATPERCENT
     */
    protected $NVATPERCENT = null;

    /**
     * @var float $NWAREBASETYPNR
     */
    protected $NWAREBASETYPNR = null;

    /**
     * @var float $NWAREITEMNR
     */
    protected $NWAREITEMNR = null;

    /**
     * @var float $NWARENR
     */
    protected $NWARENR = null;

    /**
     * @var float $NWARETYPNR
     */
    protected $NWARETYPNR = null;

    /**
     * @var string $SZCURRENCY
     */
    protected $SZCURRENCY = null;

    /**
     * @var string $SZMASKNAME
     */
    protected $SZMASKNAME = null;

    /**
     * @var string $SZMASKSHORTNAME
     */
    protected $SZMASKSHORTNAME = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSHORTNAME
     */
    protected $SZSHORTNAME = null;

    /**
     * @var string $SZWAREBASETYPNAME
     */
    protected $SZWAREBASETYPNAME = null;

    /**
     * @var string $SZWARENAME
     */
    protected $SZWARENAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPWAREFEATURES
     */
    public function getACTFEATURES()
    {
      return $this->ACTFEATURES;
    }

    /**
     * @param ArrayOfD4WTPWAREFEATURES $ACTFEATURES
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setACTFEATURES($ACTFEATURES)
    {
      $this->ACTFEATURES = $ACTFEATURES;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISMASTER()
    {
      return $this->BISMASTER;
    }

    /**
     * @param float $BISMASTER
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setBISMASTER($BISMASTER)
    {
      $this->BISMASTER = $BISMASTER;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPREISVARIABEL()
    {
      return $this->BPREISVARIABEL;
    }

    /**
     * @param float $BPREISVARIABEL
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setBPREISVARIABEL($BPREISVARIABEL)
    {
      $this->BPREISVARIABEL = $BPREISVARIABEL;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPARENTWARENR()
    {
      return $this->NPARENTWARENR;
    }

    /**
     * @param float $NPARENTWARENR
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setNPARENTWARENR($NPARENTWARENR)
    {
      $this->NPARENTWARENR = $NPARENTWARENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNR()
    {
      return $this->NSORTNR;
    }

    /**
     * @param float $NSORTNR
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setNSORTNR($NSORTNR)
    {
      $this->NSORTNR = $NSORTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATAMOUNT()
    {
      return $this->NVATAMOUNT;
    }

    /**
     * @param float $NVATAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setNVATAMOUNT($NVATAMOUNT)
    {
      $this->NVATAMOUNT = $NVATAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATPERCENT()
    {
      return $this->NVATPERCENT;
    }

    /**
     * @param float $NVATPERCENT
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setNVATPERCENT($NVATPERCENT)
    {
      $this->NVATPERCENT = $NVATPERCENT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWAREBASETYPNR()
    {
      return $this->NWAREBASETYPNR;
    }

    /**
     * @param float $NWAREBASETYPNR
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setNWAREBASETYPNR($NWAREBASETYPNR)
    {
      $this->NWAREBASETYPNR = $NWAREBASETYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWAREITEMNR()
    {
      return $this->NWAREITEMNR;
    }

    /**
     * @param float $NWAREITEMNR
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setNWAREITEMNR($NWAREITEMNR)
    {
      $this->NWAREITEMNR = $NWAREITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARENR()
    {
      return $this->NWARENR;
    }

    /**
     * @param float $NWARENR
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setNWARENR($NWARENR)
    {
      $this->NWARENR = $NWARENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARETYPNR()
    {
      return $this->NWARETYPNR;
    }

    /**
     * @param float $NWARETYPNR
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setNWARETYPNR($NWARETYPNR)
    {
      $this->NWARETYPNR = $NWARETYPNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCY()
    {
      return $this->SZCURRENCY;
    }

    /**
     * @param string $SZCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setSZCURRENCY($SZCURRENCY)
    {
      $this->SZCURRENCY = $SZCURRENCY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASKNAME()
    {
      return $this->SZMASKNAME;
    }

    /**
     * @param string $SZMASKNAME
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setSZMASKNAME($SZMASKNAME)
    {
      $this->SZMASKNAME = $SZMASKNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASKSHORTNAME()
    {
      return $this->SZMASKSHORTNAME;
    }

    /**
     * @param string $SZMASKSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setSZMASKSHORTNAME($SZMASKSHORTNAME)
    {
      $this->SZMASKSHORTNAME = $SZMASKSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHORTNAME()
    {
      return $this->SZSHORTNAME;
    }

    /**
     * @param string $SZSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setSZSHORTNAME($SZSHORTNAME)
    {
      $this->SZSHORTNAME = $SZSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWAREBASETYPNAME()
    {
      return $this->SZWAREBASETYPNAME;
    }

    /**
     * @param string $SZWAREBASETYPNAME
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setSZWAREBASETYPNAME($SZWAREBASETYPNAME)
    {
      $this->SZWAREBASETYPNAME = $SZWAREBASETYPNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWARENAME()
    {
      return $this->SZWARENAME;
    }

    /**
     * @param string $SZWARENAME
     * @return \Axess\Dci4Wtp\D4WTPWARE3
     */
    public function setSZWARENAME($SZWARENAME)
    {
      $this->SZWARENAME = $SZWARENAME;
      return $this;
    }

}
