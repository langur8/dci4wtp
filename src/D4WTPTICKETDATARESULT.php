<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETDATARESULT
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NSTATUSNR
     */
    protected $NSTATUSNR = null;

    /**
     * @var string $SZ1STENTRYCOMPANYNAME
     */
    protected $SZ1STENTRYCOMPANYNAME = null;

    /**
     * @var string $SZ1STENTRYPOENAME
     */
    protected $SZ1STENTRYPOENAME = null;

    /**
     * @var string $SZ1STENTRYTIME
     */
    protected $SZ1STENTRYTIME = null;

    /**
     * @var string $SZ1STENTRYVALLEYNAME
     */
    protected $SZ1STENTRYVALLEYNAME = null;

    /**
     * @var string $SZCREATIONTIME
     */
    protected $SZCREATIONTIME = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZPERSONTYPENAME
     */
    protected $SZPERSONTYPENAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZTICKETTYPENAME
     */
    protected $SZTICKETTYPENAME = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSTATUSNR()
    {
      return $this->NSTATUSNR;
    }

    /**
     * @param float $NSTATUSNR
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setNSTATUSNR($NSTATUSNR)
    {
      $this->NSTATUSNR = $NSTATUSNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZ1STENTRYCOMPANYNAME()
    {
      return $this->SZ1STENTRYCOMPANYNAME;
    }

    /**
     * @param string $SZ1STENTRYCOMPANYNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setSZ1STENTRYCOMPANYNAME($SZ1STENTRYCOMPANYNAME)
    {
      $this->SZ1STENTRYCOMPANYNAME = $SZ1STENTRYCOMPANYNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZ1STENTRYPOENAME()
    {
      return $this->SZ1STENTRYPOENAME;
    }

    /**
     * @param string $SZ1STENTRYPOENAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setSZ1STENTRYPOENAME($SZ1STENTRYPOENAME)
    {
      $this->SZ1STENTRYPOENAME = $SZ1STENTRYPOENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZ1STENTRYTIME()
    {
      return $this->SZ1STENTRYTIME;
    }

    /**
     * @param string $SZ1STENTRYTIME
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setSZ1STENTRYTIME($SZ1STENTRYTIME)
    {
      $this->SZ1STENTRYTIME = $SZ1STENTRYTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZ1STENTRYVALLEYNAME()
    {
      return $this->SZ1STENTRYVALLEYNAME;
    }

    /**
     * @param string $SZ1STENTRYVALLEYNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setSZ1STENTRYVALLEYNAME($SZ1STENTRYVALLEYNAME)
    {
      $this->SZ1STENTRYVALLEYNAME = $SZ1STENTRYVALLEYNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCREATIONTIME()
    {
      return $this->SZCREATIONTIME;
    }

    /**
     * @param string $SZCREATIONTIME
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setSZCREATIONTIME($SZCREATIONTIME)
    {
      $this->SZCREATIONTIME = $SZCREATIONTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSONTYPENAME()
    {
      return $this->SZPERSONTYPENAME;
    }

    /**
     * @param string $SZPERSONTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setSZPERSONTYPENAME($SZPERSONTYPENAME)
    {
      $this->SZPERSONTYPENAME = $SZPERSONTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPENAME()
    {
      return $this->SZTICKETTYPENAME;
    }

    /**
     * @param string $SZTICKETTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setSZTICKETTYPENAME($SZTICKETTYPENAME)
    {
      $this->SZTICKETTYPENAME = $SZTICKETTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPTICKETDATARESULT
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
