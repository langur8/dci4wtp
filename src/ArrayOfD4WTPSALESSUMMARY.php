<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPSALESSUMMARY implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPSALESSUMMARY[] $D4WTPSALESSUMMARY
     */
    protected $D4WTPSALESSUMMARY = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPSALESSUMMARY[]
     */
    public function getD4WTPSALESSUMMARY()
    {
      return $this->D4WTPSALESSUMMARY;
    }

    /**
     * @param D4WTPSALESSUMMARY[] $D4WTPSALESSUMMARY
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPSALESSUMMARY
     */
    public function setD4WTPSALESSUMMARY(array $D4WTPSALESSUMMARY = null)
    {
      $this->D4WTPSALESSUMMARY = $D4WTPSALESSUMMARY;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPSALESSUMMARY[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPSALESSUMMARY
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPSALESSUMMARY[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPSALESSUMMARY $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPSALESSUMMARY[] = $value;
      } else {
        $this->D4WTPSALESSUMMARY[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPSALESSUMMARY[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPSALESSUMMARY Return the current element
     */
    public function current()
    {
      return current($this->D4WTPSALESSUMMARY);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPSALESSUMMARY);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPSALESSUMMARY);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPSALESSUMMARY);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPSALESSUMMARY Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPSALESSUMMARY);
    }

}
