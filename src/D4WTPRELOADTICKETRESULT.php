<?php

namespace Axess\Dci4Wtp;

class D4WTPRELOADTICKETRESULT
{

    /**
     * @var D4WTPCODINGDATA $CTCODINGDATA
     */
    protected $CTCODINGDATA = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZBINCODE
     */
    protected $SZBINCODE = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZPRINTDATA
     */
    protected $SZPRINTDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPCODINGDATA
     */
    public function getCTCODINGDATA()
    {
      return $this->CTCODINGDATA;
    }

    /**
     * @param D4WTPCODINGDATA $CTCODINGDATA
     * @return \Axess\Dci4Wtp\D4WTPRELOADTICKETRESULT
     */
    public function setCTCODINGDATA($CTCODINGDATA)
    {
      $this->CTCODINGDATA = $CTCODINGDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPRELOADTICKETRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBINCODE()
    {
      return $this->SZBINCODE;
    }

    /**
     * @param string $SZBINCODE
     * @return \Axess\Dci4Wtp\D4WTPRELOADTICKETRESULT
     */
    public function setSZBINCODE($SZBINCODE)
    {
      $this->SZBINCODE = $SZBINCODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPRELOADTICKETRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPRINTDATA()
    {
      return $this->SZPRINTDATA;
    }

    /**
     * @param string $SZPRINTDATA
     * @return \Axess\Dci4Wtp\D4WTPRELOADTICKETRESULT
     */
    public function setSZPRINTDATA($SZPRINTDATA)
    {
      $this->SZPRINTDATA = $SZPRINTDATA;
      return $this;
    }

}
