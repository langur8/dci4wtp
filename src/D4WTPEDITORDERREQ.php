<?php

namespace Axess\Dci4Wtp;

class D4WTPEDITORDERREQ
{

    /**
     * @var float $BGLOBAL
     */
    protected $BGLOBAL = null;

    /**
     * @var ADDRESS $CTDELIVERYADDRESS
     */
    protected $CTDELIVERYADDRESS = null;

    /**
     * @var D4WTPCOMPANY $CTDELIVERYCOMPANY
     */
    protected $CTDELIVERYCOMPANY = null;

    /**
     * @var D4WTPERSONDATA3 $CTDELIVERYPERSON
     */
    protected $CTDELIVERYPERSON = null;

    /**
     * @var float $NCOREPAYMENTTYPENR
     */
    protected $NCOREPAYMENTTYPENR = null;

    /**
     * @var float $NCUSTOMERPAYMENTTYPENR
     */
    protected $NCUSTOMERPAYMENTTYPENR = null;

    /**
     * @var float $NEXTORDERNR
     */
    protected $NEXTORDERNR = null;

    /**
     * @var float $NORDERLISTNR
     */
    protected $NORDERLISTNR = null;

    /**
     * @var float $NORDERSTATENR
     */
    protected $NORDERSTATENR = null;

    /**
     * @var float $NPAYERCOMPNR
     */
    protected $NPAYERCOMPNR = null;

    /**
     * @var float $NPAYERCOMPPOSNR
     */
    protected $NPAYERCOMPPOSNR = null;

    /**
     * @var float $NPAYERCOMPPROJNR
     */
    protected $NPAYERCOMPPROJNR = null;

    /**
     * @var float $NPAYERPERSNR
     */
    protected $NPAYERPERSNR = null;

    /**
     * @var float $NPAYERPERSPOSNR
     */
    protected $NPAYERPERSPOSNR = null;

    /**
     * @var float $NPAYERPERSPROJNR
     */
    protected $NPAYERPERSPROJNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZEXTORDERREFERENCE
     */
    protected $SZEXTORDERREFERENCE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBGLOBAL()
    {
      return $this->BGLOBAL;
    }

    /**
     * @param float $BGLOBAL
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setBGLOBAL($BGLOBAL)
    {
      $this->BGLOBAL = $BGLOBAL;
      return $this;
    }

    /**
     * @return ADDRESS
     */
    public function getCTDELIVERYADDRESS()
    {
      return $this->CTDELIVERYADDRESS;
    }

    /**
     * @param ADDRESS $CTDELIVERYADDRESS
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setCTDELIVERYADDRESS($CTDELIVERYADDRESS)
    {
      $this->CTDELIVERYADDRESS = $CTDELIVERYADDRESS;
      return $this;
    }

    /**
     * @return D4WTPCOMPANY
     */
    public function getCTDELIVERYCOMPANY()
    {
      return $this->CTDELIVERYCOMPANY;
    }

    /**
     * @param D4WTPCOMPANY $CTDELIVERYCOMPANY
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setCTDELIVERYCOMPANY($CTDELIVERYCOMPANY)
    {
      $this->CTDELIVERYCOMPANY = $CTDELIVERYCOMPANY;
      return $this;
    }

    /**
     * @return D4WTPERSONDATA3
     */
    public function getCTDELIVERYPERSON()
    {
      return $this->CTDELIVERYPERSON;
    }

    /**
     * @param D4WTPERSONDATA3 $CTDELIVERYPERSON
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setCTDELIVERYPERSON($CTDELIVERYPERSON)
    {
      $this->CTDELIVERYPERSON = $CTDELIVERYPERSON;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOREPAYMENTTYPENR()
    {
      return $this->NCOREPAYMENTTYPENR;
    }

    /**
     * @param float $NCOREPAYMENTTYPENR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setNCOREPAYMENTTYPENR($NCOREPAYMENTTYPENR)
    {
      $this->NCOREPAYMENTTYPENR = $NCOREPAYMENTTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTOMERPAYMENTTYPENR()
    {
      return $this->NCUSTOMERPAYMENTTYPENR;
    }

    /**
     * @param float $NCUSTOMERPAYMENTTYPENR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setNCUSTOMERPAYMENTTYPENR($NCUSTOMERPAYMENTTYPENR)
    {
      $this->NCUSTOMERPAYMENTTYPENR = $NCUSTOMERPAYMENTTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEXTORDERNR()
    {
      return $this->NEXTORDERNR;
    }

    /**
     * @param float $NEXTORDERNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setNEXTORDERNR($NEXTORDERNR)
    {
      $this->NEXTORDERNR = $NEXTORDERNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORDERLISTNR()
    {
      return $this->NORDERLISTNR;
    }

    /**
     * @param float $NORDERLISTNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setNORDERLISTNR($NORDERLISTNR)
    {
      $this->NORDERLISTNR = $NORDERLISTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNORDERSTATENR()
    {
      return $this->NORDERSTATENR;
    }

    /**
     * @param float $NORDERSTATENR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setNORDERSTATENR($NORDERSTATENR)
    {
      $this->NORDERSTATENR = $NORDERSTATENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERCOMPNR()
    {
      return $this->NPAYERCOMPNR;
    }

    /**
     * @param float $NPAYERCOMPNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setNPAYERCOMPNR($NPAYERCOMPNR)
    {
      $this->NPAYERCOMPNR = $NPAYERCOMPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERCOMPPOSNR()
    {
      return $this->NPAYERCOMPPOSNR;
    }

    /**
     * @param float $NPAYERCOMPPOSNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setNPAYERCOMPPOSNR($NPAYERCOMPPOSNR)
    {
      $this->NPAYERCOMPPOSNR = $NPAYERCOMPPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERCOMPPROJNR()
    {
      return $this->NPAYERCOMPPROJNR;
    }

    /**
     * @param float $NPAYERCOMPPROJNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setNPAYERCOMPPROJNR($NPAYERCOMPPROJNR)
    {
      $this->NPAYERCOMPPROJNR = $NPAYERCOMPPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERPERSNR()
    {
      return $this->NPAYERPERSNR;
    }

    /**
     * @param float $NPAYERPERSNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setNPAYERPERSNR($NPAYERPERSNR)
    {
      $this->NPAYERPERSNR = $NPAYERPERSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERPERSPOSNR()
    {
      return $this->NPAYERPERSPOSNR;
    }

    /**
     * @param float $NPAYERPERSPOSNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setNPAYERPERSPOSNR($NPAYERPERSPOSNR)
    {
      $this->NPAYERPERSPOSNR = $NPAYERPERSPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYERPERSPROJNR()
    {
      return $this->NPAYERPERSPROJNR;
    }

    /**
     * @param float $NPAYERPERSPROJNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setNPAYERPERSPROJNR($NPAYERPERSPROJNR)
    {
      $this->NPAYERPERSPROJNR = $NPAYERPERSPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTORDERREFERENCE()
    {
      return $this->SZEXTORDERREFERENCE;
    }

    /**
     * @param string $SZEXTORDERREFERENCE
     * @return \Axess\Dci4Wtp\D4WTPEDITORDERREQ
     */
    public function setSZEXTORDERREFERENCE($SZEXTORDERREFERENCE)
    {
      $this->SZEXTORDERREFERENCE = $SZEXTORDERREFERENCE;
      return $this;
    }

}
