<?php

namespace Axess\Dci4Wtp;

class ArrayOfEntryStatePerDay implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var EntryStatePerDay[] $EntryStatePerDay
     */
    protected $EntryStatePerDay = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return EntryStatePerDay[]
     */
    public function getEntryStatePerDay()
    {
      return $this->EntryStatePerDay;
    }

    /**
     * @param EntryStatePerDay[] $EntryStatePerDay
     * @return \Axess\Dci4Wtp\ArrayOfEntryStatePerDay
     */
    public function setEntryStatePerDay(array $EntryStatePerDay = null)
    {
      $this->EntryStatePerDay = $EntryStatePerDay;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->EntryStatePerDay[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return EntryStatePerDay
     */
    public function offsetGet($offset)
    {
      return $this->EntryStatePerDay[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param EntryStatePerDay $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->EntryStatePerDay[] = $value;
      } else {
        $this->EntryStatePerDay[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->EntryStatePerDay[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return EntryStatePerDay Return the current element
     */
    public function current()
    {
      return current($this->EntryStatePerDay);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->EntryStatePerDay);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->EntryStatePerDay);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->EntryStatePerDay);
    }

    /**
     * Countable implementation
     *
     * @return EntryStatePerDay Return count of elements
     */
    public function count()
    {
      return count($this->EntryStatePerDay);
    }

}
