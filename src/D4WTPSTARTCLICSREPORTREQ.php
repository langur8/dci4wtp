<?php

namespace Axess\Dci4Wtp;

class D4WTPSTARTCLICSREPORTREQ
{

    /**
     * @var ArrayOfCLICSVARCHARS $ACTVARCHARS
     */
    protected $ACTVARCHARS = null;

    /**
     * @var float $NREPORTTYPENR
     */
    protected $NREPORTTYPENR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfCLICSVARCHARS
     */
    public function getACTVARCHARS()
    {
      return $this->ACTVARCHARS;
    }

    /**
     * @param ArrayOfCLICSVARCHARS $ACTVARCHARS
     * @return \Axess\Dci4Wtp\D4WTPSTARTCLICSREPORTREQ
     */
    public function setACTVARCHARS($ACTVARCHARS)
    {
      $this->ACTVARCHARS = $ACTVARCHARS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNREPORTTYPENR()
    {
      return $this->NREPORTTYPENR;
    }

    /**
     * @param float $NREPORTTYPENR
     * @return \Axess\Dci4Wtp\D4WTPSTARTCLICSREPORTREQ
     */
    public function setNREPORTTYPENR($NREPORTTYPENR)
    {
      $this->NREPORTTYPENR = $NREPORTTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSTARTCLICSREPORTREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

}
