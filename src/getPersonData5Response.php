<?php

namespace Axess\Dci4Wtp;

class getPersonData5Response
{

    /**
     * @var D4WTPGETPERSON5RESULT $getPersonData5Result
     */
    protected $getPersonData5Result = null;

    /**
     * @param D4WTPGETPERSON5RESULT $getPersonData5Result
     */
    public function __construct($getPersonData5Result)
    {
      $this->getPersonData5Result = $getPersonData5Result;
    }

    /**
     * @return D4WTPGETPERSON5RESULT
     */
    public function getGetPersonData5Result()
    {
      return $this->getPersonData5Result;
    }

    /**
     * @param D4WTPGETPERSON5RESULT $getPersonData5Result
     * @return \Axess\Dci4Wtp\getPersonData5Response
     */
    public function setGetPersonData5Result($getPersonData5Result)
    {
      $this->getPersonData5Result = $getPersonData5Result;
      return $this;
    }

}
