<?php

namespace Axess\Dci4Wtp;

class ArrayOfWTPPOOLTICKETTYPE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var WTPPOOLTICKETTYPE[] $WTPPOOLTICKETTYPE
     */
    protected $WTPPOOLTICKETTYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return WTPPOOLTICKETTYPE[]
     */
    public function getWTPPOOLTICKETTYPE()
    {
      return $this->WTPPOOLTICKETTYPE;
    }

    /**
     * @param WTPPOOLTICKETTYPE[] $WTPPOOLTICKETTYPE
     * @return \Axess\Dci4Wtp\ArrayOfWTPPOOLTICKETTYPE
     */
    public function setWTPPOOLTICKETTYPE(array $WTPPOOLTICKETTYPE = null)
    {
      $this->WTPPOOLTICKETTYPE = $WTPPOOLTICKETTYPE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->WTPPOOLTICKETTYPE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return WTPPOOLTICKETTYPE
     */
    public function offsetGet($offset)
    {
      return $this->WTPPOOLTICKETTYPE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param WTPPOOLTICKETTYPE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->WTPPOOLTICKETTYPE[] = $value;
      } else {
        $this->WTPPOOLTICKETTYPE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->WTPPOOLTICKETTYPE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return WTPPOOLTICKETTYPE Return the current element
     */
    public function current()
    {
      return current($this->WTPPOOLTICKETTYPE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->WTPPOOLTICKETTYPE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->WTPPOOLTICKETTYPE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->WTPPOOLTICKETTYPE);
    }

    /**
     * Countable implementation
     *
     * @return WTPPOOLTICKETTYPE Return count of elements
     */
    public function count()
    {
      return count($this->WTPPOOLTICKETTYPE);
    }

}
