<?php

namespace Axess\Dci4Wtp;

class getGroupContingent2Response
{

    /**
     * @var D4WTPGETGRPCONTINGENT2RESULT $getGroupContingent2Result
     */
    protected $getGroupContingent2Result = null;

    /**
     * @param D4WTPGETGRPCONTINGENT2RESULT $getGroupContingent2Result
     */
    public function __construct($getGroupContingent2Result)
    {
      $this->getGroupContingent2Result = $getGroupContingent2Result;
    }

    /**
     * @return D4WTPGETGRPCONTINGENT2RESULT
     */
    public function getGetGroupContingent2Result()
    {
      return $this->getGroupContingent2Result;
    }

    /**
     * @param D4WTPGETGRPCONTINGENT2RESULT $getGroupContingent2Result
     * @return \Axess\Dci4Wtp\getGroupContingent2Response
     */
    public function setGetGroupContingent2Result($getGroupContingent2Result)
    {
      $this->getGroupContingent2Result = $getGroupContingent2Result;
      return $this;
    }

}
