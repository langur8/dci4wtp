<?php

namespace Axess\Dci4Wtp;

class getCompanyBalance
{

    /**
     * @var D4WTPGETCOMPANYBALANCEREQ $i_ctCompanyBalanceReq
     */
    protected $i_ctCompanyBalanceReq = null;

    /**
     * @param D4WTPGETCOMPANYBALANCEREQ $i_ctCompanyBalanceReq
     */
    public function __construct($i_ctCompanyBalanceReq)
    {
      $this->i_ctCompanyBalanceReq = $i_ctCompanyBalanceReq;
    }

    /**
     * @return D4WTPGETCOMPANYBALANCEREQ
     */
    public function getI_ctCompanyBalanceReq()
    {
      return $this->i_ctCompanyBalanceReq;
    }

    /**
     * @param D4WTPGETCOMPANYBALANCEREQ $i_ctCompanyBalanceReq
     * @return \Axess\Dci4Wtp\getCompanyBalance
     */
    public function setI_ctCompanyBalanceReq($i_ctCompanyBalanceReq)
    {
      $this->i_ctCompanyBalanceReq = $i_ctCompanyBalanceReq;
      return $this;
    }

}
