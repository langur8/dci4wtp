<?php

namespace Axess\Dci4Wtp;

class D4WTPSWISSPASSREGREQ
{

    /**
     * @var float $BACTIVE
     */
    protected $BACTIVE = null;

    /**
     * @var float $NJOURNALNR
     */
    protected $NJOURNALNR = null;

    /**
     * @var float $NKASSANR
     */
    protected $NKASSANR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZAUSWEISID
     */
    protected $SZAUSWEISID = null;

    /**
     * @var string $SZPOSTLEITZAHL
     */
    protected $SZPOSTLEITZAHL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBACTIVE()
    {
      return $this->BACTIVE;
    }

    /**
     * @param float $BACTIVE
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSREGREQ
     */
    public function setBACTIVE($BACTIVE)
    {
      $this->BACTIVE = $BACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNR()
    {
      return $this->NJOURNALNR;
    }

    /**
     * @param float $NJOURNALNR
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSREGREQ
     */
    public function setNJOURNALNR($NJOURNALNR)
    {
      $this->NJOURNALNR = $NJOURNALNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKASSANR()
    {
      return $this->NKASSANR;
    }

    /**
     * @param float $NKASSANR
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSREGREQ
     */
    public function setNKASSANR($NKASSANR)
    {
      $this->NKASSANR = $NKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSREGREQ
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSREGREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAUSWEISID()
    {
      return $this->SZAUSWEISID;
    }

    /**
     * @param string $SZAUSWEISID
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSREGREQ
     */
    public function setSZAUSWEISID($SZAUSWEISID)
    {
      $this->SZAUSWEISID = $SZAUSWEISID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOSTLEITZAHL()
    {
      return $this->SZPOSTLEITZAHL;
    }

    /**
     * @param string $SZPOSTLEITZAHL
     * @return \Axess\Dci4Wtp\D4WTPSWISSPASSREGREQ
     */
    public function setSZPOSTLEITZAHL($SZPOSTLEITZAHL)
    {
      $this->SZPOSTLEITZAHL = $SZPOSTLEITZAHL;
      return $this;
    }

}
