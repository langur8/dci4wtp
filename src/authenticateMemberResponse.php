<?php

namespace Axess\Dci4Wtp;

class authenticateMemberResponse
{

    /**
     * @var D4WTPMEMBERRESULT $authenticateMemberResult
     */
    protected $authenticateMemberResult = null;

    /**
     * @param D4WTPMEMBERRESULT $authenticateMemberResult
     */
    public function __construct($authenticateMemberResult)
    {
      $this->authenticateMemberResult = $authenticateMemberResult;
    }

    /**
     * @return D4WTPMEMBERRESULT
     */
    public function getAuthenticateMemberResult()
    {
      return $this->authenticateMemberResult;
    }

    /**
     * @param D4WTPMEMBERRESULT $authenticateMemberResult
     * @return \Axess\Dci4Wtp\authenticateMemberResponse
     */
    public function setAuthenticateMemberResult($authenticateMemberResult)
    {
      $this->authenticateMemberResult = $authenticateMemberResult;
      return $this;
    }

}
