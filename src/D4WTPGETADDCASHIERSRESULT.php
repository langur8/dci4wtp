<?php

namespace Axess\Dci4Wtp;

class D4WTPGETADDCASHIERSRESULT
{

    /**
     * @var ArrayOfD4WTPADDCASHIER $ACTADDCASHIER
     */
    protected $ACTADDCASHIER = null;

    /**
     * @var float $NCOMPANYNR
     */
    protected $NCOMPANYNR = null;

    /**
     * @var float $NCOMPANYPOSNR
     */
    protected $NCOMPANYPOSNR = null;

    /**
     * @var float $NCOMPANYPROJNR
     */
    protected $NCOMPANYPROJNR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDCASHIER
     */
    public function getACTADDCASHIER()
    {
      return $this->ACTADDCASHIER;
    }

    /**
     * @param ArrayOfD4WTPADDCASHIER $ACTADDCASHIER
     * @return \Axess\Dci4Wtp\D4WTPGETADDCASHIERSRESULT
     */
    public function setACTADDCASHIER($ACTADDCASHIER)
    {
      $this->ACTADDCASHIER = $ACTADDCASHIER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYNR()
    {
      return $this->NCOMPANYNR;
    }

    /**
     * @param float $NCOMPANYNR
     * @return \Axess\Dci4Wtp\D4WTPGETADDCASHIERSRESULT
     */
    public function setNCOMPANYNR($NCOMPANYNR)
    {
      $this->NCOMPANYNR = $NCOMPANYNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPOSNR()
    {
      return $this->NCOMPANYPOSNR;
    }

    /**
     * @param float $NCOMPANYPOSNR
     * @return \Axess\Dci4Wtp\D4WTPGETADDCASHIERSRESULT
     */
    public function setNCOMPANYPOSNR($NCOMPANYPOSNR)
    {
      $this->NCOMPANYPOSNR = $NCOMPANYPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPANYPROJNR()
    {
      return $this->NCOMPANYPROJNR;
    }

    /**
     * @param float $NCOMPANYPROJNR
     * @return \Axess\Dci4Wtp\D4WTPGETADDCASHIERSRESULT
     */
    public function setNCOMPANYPROJNR($NCOMPANYPROJNR)
    {
      $this->NCOMPANYPROJNR = $NCOMPANYPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETADDCASHIERSRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETADDCASHIERSRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
