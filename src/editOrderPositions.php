<?php

namespace Axess\Dci4Wtp;

class editOrderPositions
{

    /**
     * @var D4WTPEDITORDERPOSREQ $i_ctEditOrderPosReq
     */
    protected $i_ctEditOrderPosReq = null;

    /**
     * @param D4WTPEDITORDERPOSREQ $i_ctEditOrderPosReq
     */
    public function __construct($i_ctEditOrderPosReq)
    {
      $this->i_ctEditOrderPosReq = $i_ctEditOrderPosReq;
    }

    /**
     * @return D4WTPEDITORDERPOSREQ
     */
    public function getI_ctEditOrderPosReq()
    {
      return $this->i_ctEditOrderPosReq;
    }

    /**
     * @param D4WTPEDITORDERPOSREQ $i_ctEditOrderPosReq
     * @return \Axess\Dci4Wtp\editOrderPositions
     */
    public function setI_ctEditOrderPosReq($i_ctEditOrderPosReq)
    {
      $this->i_ctEditOrderPosReq = $i_ctEditOrderPosReq;
      return $this;
    }

}
