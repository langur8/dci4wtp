<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPWARE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPWARE[] $D4WTPWARE
     */
    protected $D4WTPWARE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPWARE[]
     */
    public function getD4WTPWARE()
    {
      return $this->D4WTPWARE;
    }

    /**
     * @param D4WTPWARE[] $D4WTPWARE
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPWARE
     */
    public function setD4WTPWARE(array $D4WTPWARE = null)
    {
      $this->D4WTPWARE = $D4WTPWARE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPWARE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPWARE
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPWARE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPWARE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPWARE[] = $value;
      } else {
        $this->D4WTPWARE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPWARE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPWARE Return the current element
     */
    public function current()
    {
      return current($this->D4WTPWARE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPWARE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPWARE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPWARE);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPWARE Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPWARE);
    }

}
