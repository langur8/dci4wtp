<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPDSGVOFLAGS implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPDSGVOFLAGS[] $D4WTPDSGVOFLAGS
     */
    protected $D4WTPDSGVOFLAGS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPDSGVOFLAGS[]
     */
    public function getD4WTPDSGVOFLAGS()
    {
      return $this->D4WTPDSGVOFLAGS;
    }

    /**
     * @param D4WTPDSGVOFLAGS[] $D4WTPDSGVOFLAGS
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPDSGVOFLAGS
     */
    public function setD4WTPDSGVOFLAGS(array $D4WTPDSGVOFLAGS = null)
    {
      $this->D4WTPDSGVOFLAGS = $D4WTPDSGVOFLAGS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPDSGVOFLAGS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPDSGVOFLAGS
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPDSGVOFLAGS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPDSGVOFLAGS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPDSGVOFLAGS[] = $value;
      } else {
        $this->D4WTPDSGVOFLAGS[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPDSGVOFLAGS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPDSGVOFLAGS Return the current element
     */
    public function current()
    {
      return current($this->D4WTPDSGVOFLAGS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPDSGVOFLAGS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPDSGVOFLAGS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPDSGVOFLAGS);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPDSGVOFLAGS Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPDSGVOFLAGS);
    }

}
