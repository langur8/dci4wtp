<?php

namespace Axess\Dci4Wtp;

class getCompany2
{

    /**
     * @var D4WTPGETCOMPANYREQUEST $i_ctGetCompanyReq
     */
    protected $i_ctGetCompanyReq = null;

    /**
     * @param D4WTPGETCOMPANYREQUEST $i_ctGetCompanyReq
     */
    public function __construct($i_ctGetCompanyReq)
    {
      $this->i_ctGetCompanyReq = $i_ctGetCompanyReq;
    }

    /**
     * @return D4WTPGETCOMPANYREQUEST
     */
    public function getI_ctGetCompanyReq()
    {
      return $this->i_ctGetCompanyReq;
    }

    /**
     * @param D4WTPGETCOMPANYREQUEST $i_ctGetCompanyReq
     * @return \Axess\Dci4Wtp\getCompany2
     */
    public function setI_ctGetCompanyReq($i_ctGetCompanyReq)
    {
      $this->i_ctGetCompanyReq = $i_ctGetCompanyReq;
      return $this;
    }

}
