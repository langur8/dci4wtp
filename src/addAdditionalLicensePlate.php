<?php

namespace Axess\Dci4Wtp;

class addAdditionalLicensePlate
{

    /**
     * @var D4WTPADDADLICENSEPLATEREQ $i_addaddlicenseplatereq
     */
    protected $i_addaddlicenseplatereq = null;

    /**
     * @param D4WTPADDADLICENSEPLATEREQ $i_addaddlicenseplatereq
     */
    public function __construct($i_addaddlicenseplatereq)
    {
      $this->i_addaddlicenseplatereq = $i_addaddlicenseplatereq;
    }

    /**
     * @return D4WTPADDADLICENSEPLATEREQ
     */
    public function getI_addaddlicenseplatereq()
    {
      return $this->i_addaddlicenseplatereq;
    }

    /**
     * @param D4WTPADDADLICENSEPLATEREQ $i_addaddlicenseplatereq
     * @return \Axess\Dci4Wtp\addAdditionalLicensePlate
     */
    public function setI_addaddlicenseplatereq($i_addaddlicenseplatereq)
    {
      $this->i_addaddlicenseplatereq = $i_addaddlicenseplatereq;
      return $this;
    }

}
