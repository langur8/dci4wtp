<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLISTREQUEST
{

    /**
     * @var float $BINCLUDINGERRORS
     */
    protected $BINCLUDINGERRORS = null;

    /**
     * @var D4WTPPRODUCTLIST $CTPRODUCTLIST
     */
    protected $CTPRODUCTLIST = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBINCLUDINGERRORS()
    {
      return $this->BINCLUDINGERRORS;
    }

    /**
     * @param float $BINCLUDINGERRORS
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTREQUEST
     */
    public function setBINCLUDINGERRORS($BINCLUDINGERRORS)
    {
      $this->BINCLUDINGERRORS = $BINCLUDINGERRORS;
      return $this;
    }

    /**
     * @return D4WTPPRODUCTLIST
     */
    public function getCTPRODUCTLIST()
    {
      return $this->CTPRODUCTLIST;
    }

    /**
     * @param D4WTPPRODUCTLIST $CTPRODUCTLIST
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTREQUEST
     */
    public function setCTPRODUCTLIST($CTPRODUCTLIST)
    {
      $this->CTPRODUCTLIST = $CTPRODUCTLIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTREQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLISTREQUEST
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

}
