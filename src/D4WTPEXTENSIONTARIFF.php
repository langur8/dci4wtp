<?php

namespace Axess\Dci4Wtp;

class D4WTPEXTENSIONTARIFF
{

    /**
     * @var float $FEXTENSIONSKIDAYS
     */
    protected $FEXTENSIONSKIDAYS = null;

    /**
     * @var float $FSKIDAYS
     */
    protected $FSKIDAYS = null;

    /**
     * @var float $FTARIFPRICE
     */
    protected $FTARIFPRICE = null;

    /**
     * @var float $NPERSTYPENR
     */
    protected $NPERSTYPENR = null;

    /**
     * @var float $NPOOLNR
     */
    protected $NPOOLNR = null;

    /**
     * @var float $NTARIFNR
     */
    protected $NTARIFNR = null;

    /**
     * @var float $NTICKETTYPENR
     */
    protected $NTICKETTYPENR = null;

    /**
     * @var string $SZPERSTYPENAME
     */
    protected $SZPERSTYPENAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZTICKETTYPENAME
     */
    protected $SZTICKETTYPENAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFEXTENSIONSKIDAYS()
    {
      return $this->FEXTENSIONSKIDAYS;
    }

    /**
     * @param float $FEXTENSIONSKIDAYS
     * @return \Axess\Dci4Wtp\D4WTPEXTENSIONTARIFF
     */
    public function setFEXTENSIONSKIDAYS($FEXTENSIONSKIDAYS)
    {
      $this->FEXTENSIONSKIDAYS = $FEXTENSIONSKIDAYS;
      return $this;
    }

    /**
     * @return float
     */
    public function getFSKIDAYS()
    {
      return $this->FSKIDAYS;
    }

    /**
     * @param float $FSKIDAYS
     * @return \Axess\Dci4Wtp\D4WTPEXTENSIONTARIFF
     */
    public function setFSKIDAYS($FSKIDAYS)
    {
      $this->FSKIDAYS = $FSKIDAYS;
      return $this;
    }

    /**
     * @return float
     */
    public function getFTARIFPRICE()
    {
      return $this->FTARIFPRICE;
    }

    /**
     * @param float $FTARIFPRICE
     * @return \Axess\Dci4Wtp\D4WTPEXTENSIONTARIFF
     */
    public function setFTARIFPRICE($FTARIFPRICE)
    {
      $this->FTARIFPRICE = $FTARIFPRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSTYPENR()
    {
      return $this->NPERSTYPENR;
    }

    /**
     * @param float $NPERSTYPENR
     * @return \Axess\Dci4Wtp\D4WTPEXTENSIONTARIFF
     */
    public function setNPERSTYPENR($NPERSTYPENR)
    {
      $this->NPERSTYPENR = $NPERSTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNR()
    {
      return $this->NPOOLNR;
    }

    /**
     * @param float $NPOOLNR
     * @return \Axess\Dci4Wtp\D4WTPEXTENSIONTARIFF
     */
    public function setNPOOLNR($NPOOLNR)
    {
      $this->NPOOLNR = $NPOOLNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFNR()
    {
      return $this->NTARIFNR;
    }

    /**
     * @param float $NTARIFNR
     * @return \Axess\Dci4Wtp\D4WTPEXTENSIONTARIFF
     */
    public function setNTARIFNR($NTARIFNR)
    {
      $this->NTARIFNR = $NTARIFNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENR()
    {
      return $this->NTICKETTYPENR;
    }

    /**
     * @param float $NTICKETTYPENR
     * @return \Axess\Dci4Wtp\D4WTPEXTENSIONTARIFF
     */
    public function setNTICKETTYPENR($NTICKETTYPENR)
    {
      $this->NTICKETTYPENR = $NTICKETTYPENR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSTYPENAME()
    {
      return $this->SZPERSTYPENAME;
    }

    /**
     * @param string $SZPERSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPEXTENSIONTARIFF
     */
    public function setSZPERSTYPENAME($SZPERSTYPENAME)
    {
      $this->SZPERSTYPENAME = $SZPERSTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPEXTENSIONTARIFF
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPENAME()
    {
      return $this->SZTICKETTYPENAME;
    }

    /**
     * @param string $SZTICKETTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPEXTENSIONTARIFF
     */
    public function setSZTICKETTYPENAME($SZTICKETTYPENAME)
    {
      $this->SZTICKETTYPENAME = $SZTICKETTYPENAME;
      return $this;
    }

}
