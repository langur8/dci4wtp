<?php

namespace Axess\Dci4Wtp;

class D4WTPBLOCKSATZ
{

    /**
     * @var float $BDEPOTKARTE
     */
    protected $BDEPOTKARTE = null;

    /**
     * @var float $BEXTENDEDMASTER
     */
    protected $BEXTENDEDMASTER = null;

    /**
     * @var float $BFREIKARTE
     */
    protected $BFREIKARTE = null;

    /**
     * @var float $BHANDFEED
     */
    protected $BHANDFEED = null;

    /**
     * @var float $BSERVICEAUSGABE
     */
    protected $BSERVICEAUSGABE = null;

    /**
     * @var D4WTPDISCOUNT $CTDISCOUNT
     */
    protected $CTDISCOUNT = null;

    /**
     * @var float $FAMOUNT
     */
    protected $FAMOUNT = null;

    /**
     * @var float $FEINZELTARIF
     */
    protected $FEINZELTARIF = null;

    /**
     * @var float $FFIXEINZELRABATT
     */
    protected $FFIXEINZELRABATT = null;

    /**
     * @var float $FSUBTARIF
     */
    protected $FSUBTARIF = null;

    /**
     * @var float $FWARETARIF
     */
    protected $FWARETARIF = null;

    /**
     * @var float $NANZGRPPERSONEN
     */
    protected $NANZGRPPERSONEN = null;

    /**
     * @var float $NARTIKELNR
     */
    protected $NARTIKELNR = null;

    /**
     * @var float $NBLOCKNR
     */
    protected $NBLOCKNR = null;

    /**
     * @var float $NDATTRAEGTYPNR
     */
    protected $NDATTRAEGTYPNR = null;

    /**
     * @var float $NEINGEGSORTSTK
     */
    protected $NEINGEGSORTSTK = null;

    /**
     * @var float $NGESNR
     */
    protected $NGESNR = null;

    /**
     * @var float $NKARTENGRUNDTYPNR
     */
    protected $NKARTENGRUNDTYPNR = null;

    /**
     * @var float $NKARTENMASKENTYPNR
     */
    protected $NKARTENMASKENTYPNR = null;

    /**
     * @var float $NKASSANR
     */
    protected $NKASSANR = null;

    /**
     * @var float $NKASSANRALT
     */
    protected $NKASSANRALT = null;

    /**
     * @var float $NKUNDENKARTENTYPNR
     */
    protected $NKUNDENKARTENTYPNR = null;

    /**
     * @var float $NLFDNR
     */
    protected $NLFDNR = null;

    /**
     * @var float $NLFDSAISONNR
     */
    protected $NLFDSAISONNR = null;

    /**
     * @var float $NMISCHPREISMODNR
     */
    protected $NMISCHPREISMODNR = null;

    /**
     * @var float $NOCCREGIONID
     */
    protected $NOCCREGIONID = null;

    /**
     * @var float $NOCCTICKETID
     */
    protected $NOCCTICKETID = null;

    /**
     * @var float $NPACKAGENR
     */
    protected $NPACKAGENR = null;

    /**
     * @var float $NPERSTYPNR
     */
    protected $NPERSTYPNR = null;

    /**
     * @var float $NPOOLNR
     */
    protected $NPOOLNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NPROJNRALT
     */
    protected $NPROJNRALT = null;

    /**
     * @var float $NRABATTNR
     */
    protected $NRABATTNR = null;

    /**
     * @var float $NRABATTSTAFNR
     */
    protected $NRABATTSTAFNR = null;

    /**
     * @var float $NREFBLOCKNR
     */
    protected $NREFBLOCKNR = null;

    /**
     * @var float $NRENTALITEMNR
     */
    protected $NRENTALITEMNR = null;

    /**
     * @var float $NRENTALPERSTYPENR
     */
    protected $NRENTALPERSTYPENR = null;

    /**
     * @var float $NRESTPUNKTEALT
     */
    protected $NRESTPUNKTEALT = null;

    /**
     * @var float $NROHLINGSTYPNR
     */
    protected $NROHLINGSTYPNR = null;

    /**
     * @var float $NSALEQUANTITY
     */
    protected $NSALEQUANTITY = null;

    /**
     * @var float $NSCHACHTNR
     */
    protected $NSCHACHTNR = null;

    /**
     * @var float $NSERIENNRALT
     */
    protected $NSERIENNRALT = null;

    /**
     * @var float $NSERIENNRALT_TMP
     */
    protected $NSERIENNRALT_TMP = null;

    /**
     * @var float $NSUBPOOLNR
     */
    protected $NSUBPOOLNR = null;

    /**
     * @var float $NSUBPROJNR
     */
    protected $NSUBPROJNR = null;

    /**
     * @var float $NTARIFBLATTGUELTNR
     */
    protected $NTARIFBLATTGUELTNR = null;

    /**
     * @var float $NTARIFBLATTNR
     */
    protected $NTARIFBLATTNR = null;

    /**
     * @var float $NTRANSAKTART
     */
    protected $NTRANSAKTART = null;

    /**
     * @var float $NTRANSNR
     */
    protected $NTRANSNR = null;

    /**
     * @var float $NUNICODENRALT
     */
    protected $NUNICODENRALT = null;

    /**
     * @var float $NUSTAUFDRUCKMODENR
     */
    protected $NUSTAUFDRUCKMODENR = null;

    /**
     * @var float $NVERKPROJNR
     */
    protected $NVERKPROJNR = null;

    /**
     * @var float $NWARESALEITEMNR
     */
    protected $NWARESALEITEMNR = null;

    /**
     * @var float $NZEITSTAFNR
     */
    protected $NZEITSTAFNR = null;

    /**
     * @var string $SZAUSGABEDAT
     */
    protected $SZAUSGABEDAT = null;

    /**
     * @var string $SZBESCH
     */
    protected $SZBESCH = null;

    /**
     * @var string $SZGILTAB
     */
    protected $SZGILTAB = null;

    /**
     * @var string $SZGILTBIS
     */
    protected $SZGILTBIS = null;

    /**
     * @var string $SZGUELTIGABALT
     */
    protected $SZGUELTIGABALT = null;

    /**
     * @var string $SZGUELTIGBISALT
     */
    protected $SZGUELTIGBISALT = null;

    /**
     * @var string $SZPERMISSIONHANDLEOLD
     */
    protected $SZPERMISSIONHANDLEOLD = null;

    /**
     * @var string $SZSTARTZEIT
     */
    protected $SZSTARTZEIT = null;

    /**
     * @var string $SZUPDATE
     */
    protected $SZUPDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBDEPOTKARTE()
    {
      return $this->BDEPOTKARTE;
    }

    /**
     * @param float $BDEPOTKARTE
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setBDEPOTKARTE($BDEPOTKARTE)
    {
      $this->BDEPOTKARTE = $BDEPOTKARTE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBEXTENDEDMASTER()
    {
      return $this->BEXTENDEDMASTER;
    }

    /**
     * @param float $BEXTENDEDMASTER
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setBEXTENDEDMASTER($BEXTENDEDMASTER)
    {
      $this->BEXTENDEDMASTER = $BEXTENDEDMASTER;
      return $this;
    }

    /**
     * @return float
     */
    public function getBFREIKARTE()
    {
      return $this->BFREIKARTE;
    }

    /**
     * @param float $BFREIKARTE
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setBFREIKARTE($BFREIKARTE)
    {
      $this->BFREIKARTE = $BFREIKARTE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBHANDFEED()
    {
      return $this->BHANDFEED;
    }

    /**
     * @param float $BHANDFEED
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setBHANDFEED($BHANDFEED)
    {
      $this->BHANDFEED = $BHANDFEED;
      return $this;
    }

    /**
     * @return float
     */
    public function getBSERVICEAUSGABE()
    {
      return $this->BSERVICEAUSGABE;
    }

    /**
     * @param float $BSERVICEAUSGABE
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setBSERVICEAUSGABE($BSERVICEAUSGABE)
    {
      $this->BSERVICEAUSGABE = $BSERVICEAUSGABE;
      return $this;
    }

    /**
     * @return D4WTPDISCOUNT
     */
    public function getCTDISCOUNT()
    {
      return $this->CTDISCOUNT;
    }

    /**
     * @param D4WTPDISCOUNT $CTDISCOUNT
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setCTDISCOUNT($CTDISCOUNT)
    {
      $this->CTDISCOUNT = $CTDISCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFAMOUNT()
    {
      return $this->FAMOUNT;
    }

    /**
     * @param float $FAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setFAMOUNT($FAMOUNT)
    {
      $this->FAMOUNT = $FAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFEINZELTARIF()
    {
      return $this->FEINZELTARIF;
    }

    /**
     * @param float $FEINZELTARIF
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setFEINZELTARIF($FEINZELTARIF)
    {
      $this->FEINZELTARIF = $FEINZELTARIF;
      return $this;
    }

    /**
     * @return float
     */
    public function getFFIXEINZELRABATT()
    {
      return $this->FFIXEINZELRABATT;
    }

    /**
     * @param float $FFIXEINZELRABATT
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setFFIXEINZELRABATT($FFIXEINZELRABATT)
    {
      $this->FFIXEINZELRABATT = $FFIXEINZELRABATT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFSUBTARIF()
    {
      return $this->FSUBTARIF;
    }

    /**
     * @param float $FSUBTARIF
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setFSUBTARIF($FSUBTARIF)
    {
      $this->FSUBTARIF = $FSUBTARIF;
      return $this;
    }

    /**
     * @return float
     */
    public function getFWARETARIF()
    {
      return $this->FWARETARIF;
    }

    /**
     * @param float $FWARETARIF
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setFWARETARIF($FWARETARIF)
    {
      $this->FWARETARIF = $FWARETARIF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNANZGRPPERSONEN()
    {
      return $this->NANZGRPPERSONEN;
    }

    /**
     * @param float $NANZGRPPERSONEN
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNANZGRPPERSONEN($NANZGRPPERSONEN)
    {
      $this->NANZGRPPERSONEN = $NANZGRPPERSONEN;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTIKELNR()
    {
      return $this->NARTIKELNR;
    }

    /**
     * @param float $NARTIKELNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNARTIKELNR($NARTIKELNR)
    {
      $this->NARTIKELNR = $NARTIKELNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKNR()
    {
      return $this->NBLOCKNR;
    }

    /**
     * @param float $NBLOCKNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNBLOCKNR($NBLOCKNR)
    {
      $this->NBLOCKNR = $NBLOCKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATTRAEGTYPNR()
    {
      return $this->NDATTRAEGTYPNR;
    }

    /**
     * @param float $NDATTRAEGTYPNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNDATTRAEGTYPNR($NDATTRAEGTYPNR)
    {
      $this->NDATTRAEGTYPNR = $NDATTRAEGTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNEINGEGSORTSTK()
    {
      return $this->NEINGEGSORTSTK;
    }

    /**
     * @param float $NEINGEGSORTSTK
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNEINGEGSORTSTK($NEINGEGSORTSTK)
    {
      $this->NEINGEGSORTSTK = $NEINGEGSORTSTK;
      return $this;
    }

    /**
     * @return float
     */
    public function getNGESNR()
    {
      return $this->NGESNR;
    }

    /**
     * @param float $NGESNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNGESNR($NGESNR)
    {
      $this->NGESNR = $NGESNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKARTENGRUNDTYPNR()
    {
      return $this->NKARTENGRUNDTYPNR;
    }

    /**
     * @param float $NKARTENGRUNDTYPNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNKARTENGRUNDTYPNR($NKARTENGRUNDTYPNR)
    {
      $this->NKARTENGRUNDTYPNR = $NKARTENGRUNDTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKARTENMASKENTYPNR()
    {
      return $this->NKARTENMASKENTYPNR;
    }

    /**
     * @param float $NKARTENMASKENTYPNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNKARTENMASKENTYPNR($NKARTENMASKENTYPNR)
    {
      $this->NKARTENMASKENTYPNR = $NKARTENMASKENTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKASSANR()
    {
      return $this->NKASSANR;
    }

    /**
     * @param float $NKASSANR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNKASSANR($NKASSANR)
    {
      $this->NKASSANR = $NKASSANR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKASSANRALT()
    {
      return $this->NKASSANRALT;
    }

    /**
     * @param float $NKASSANRALT
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNKASSANRALT($NKASSANRALT)
    {
      $this->NKASSANRALT = $NKASSANRALT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKUNDENKARTENTYPNR()
    {
      return $this->NKUNDENKARTENTYPNR;
    }

    /**
     * @param float $NKUNDENKARTENTYPNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNKUNDENKARTENTYPNR($NKUNDENKARTENTYPNR)
    {
      $this->NKUNDENKARTENTYPNR = $NKUNDENKARTENTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDNR()
    {
      return $this->NLFDNR;
    }

    /**
     * @param float $NLFDNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNLFDNR($NLFDNR)
    {
      $this->NLFDNR = $NLFDNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDSAISONNR()
    {
      return $this->NLFDSAISONNR;
    }

    /**
     * @param float $NLFDSAISONNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNLFDSAISONNR($NLFDSAISONNR)
    {
      $this->NLFDSAISONNR = $NLFDSAISONNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMISCHPREISMODNR()
    {
      return $this->NMISCHPREISMODNR;
    }

    /**
     * @param float $NMISCHPREISMODNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNMISCHPREISMODNR($NMISCHPREISMODNR)
    {
      $this->NMISCHPREISMODNR = $NMISCHPREISMODNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNOCCREGIONID()
    {
      return $this->NOCCREGIONID;
    }

    /**
     * @param float $NOCCREGIONID
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNOCCREGIONID($NOCCREGIONID)
    {
      $this->NOCCREGIONID = $NOCCREGIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNOCCTICKETID()
    {
      return $this->NOCCTICKETID;
    }

    /**
     * @param float $NOCCTICKETID
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNOCCTICKETID($NOCCTICKETID)
    {
      $this->NOCCTICKETID = $NOCCTICKETID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKAGENR()
    {
      return $this->NPACKAGENR;
    }

    /**
     * @param float $NPACKAGENR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNPACKAGENR($NPACKAGENR)
    {
      $this->NPACKAGENR = $NPACKAGENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSTYPNR()
    {
      return $this->NPERSTYPNR;
    }

    /**
     * @param float $NPERSTYPNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNPERSTYPNR($NPERSTYPNR)
    {
      $this->NPERSTYPNR = $NPERSTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNR()
    {
      return $this->NPOOLNR;
    }

    /**
     * @param float $NPOOLNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNPOOLNR($NPOOLNR)
    {
      $this->NPOOLNR = $NPOOLNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNRALT()
    {
      return $this->NPROJNRALT;
    }

    /**
     * @param float $NPROJNRALT
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNPROJNRALT($NPROJNRALT)
    {
      $this->NPROJNRALT = $NPROJNRALT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRABATTNR()
    {
      return $this->NRABATTNR;
    }

    /**
     * @param float $NRABATTNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNRABATTNR($NRABATTNR)
    {
      $this->NRABATTNR = $NRABATTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRABATTSTAFNR()
    {
      return $this->NRABATTSTAFNR;
    }

    /**
     * @param float $NRABATTSTAFNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNRABATTSTAFNR($NRABATTSTAFNR)
    {
      $this->NRABATTSTAFNR = $NRABATTSTAFNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNREFBLOCKNR()
    {
      return $this->NREFBLOCKNR;
    }

    /**
     * @param float $NREFBLOCKNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNREFBLOCKNR($NREFBLOCKNR)
    {
      $this->NREFBLOCKNR = $NREFBLOCKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALITEMNR()
    {
      return $this->NRENTALITEMNR;
    }

    /**
     * @param float $NRENTALITEMNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNRENTALITEMNR($NRENTALITEMNR)
    {
      $this->NRENTALITEMNR = $NRENTALITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRENTALPERSTYPENR()
    {
      return $this->NRENTALPERSTYPENR;
    }

    /**
     * @param float $NRENTALPERSTYPENR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNRENTALPERSTYPENR($NRENTALPERSTYPENR)
    {
      $this->NRENTALPERSTYPENR = $NRENTALPERSTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRESTPUNKTEALT()
    {
      return $this->NRESTPUNKTEALT;
    }

    /**
     * @param float $NRESTPUNKTEALT
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNRESTPUNKTEALT($NRESTPUNKTEALT)
    {
      $this->NRESTPUNKTEALT = $NRESTPUNKTEALT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNROHLINGSTYPNR()
    {
      return $this->NROHLINGSTYPNR;
    }

    /**
     * @param float $NROHLINGSTYPNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNROHLINGSTYPNR($NROHLINGSTYPNR)
    {
      $this->NROHLINGSTYPNR = $NROHLINGSTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSALEQUANTITY()
    {
      return $this->NSALEQUANTITY;
    }

    /**
     * @param float $NSALEQUANTITY
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNSALEQUANTITY($NSALEQUANTITY)
    {
      $this->NSALEQUANTITY = $NSALEQUANTITY;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSCHACHTNR()
    {
      return $this->NSCHACHTNR;
    }

    /**
     * @param float $NSCHACHTNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNSCHACHTNR($NSCHACHTNR)
    {
      $this->NSCHACHTNR = $NSCHACHTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIENNRALT()
    {
      return $this->NSERIENNRALT;
    }

    /**
     * @param float $NSERIENNRALT
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNSERIENNRALT($NSERIENNRALT)
    {
      $this->NSERIENNRALT = $NSERIENNRALT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIENNRALT_TMP()
    {
      return $this->NSERIENNRALT_TMP;
    }

    /**
     * @param float $NSERIENNRALT_TMP
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNSERIENNRALT_TMP($NSERIENNRALT_TMP)
    {
      $this->NSERIENNRALT_TMP = $NSERIENNRALT_TMP;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBPOOLNR()
    {
      return $this->NSUBPOOLNR;
    }

    /**
     * @param float $NSUBPOOLNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNSUBPOOLNR($NSUBPOOLNR)
    {
      $this->NSUBPOOLNR = $NSUBPOOLNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBPROJNR()
    {
      return $this->NSUBPROJNR;
    }

    /**
     * @param float $NSUBPROJNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNSUBPROJNR($NSUBPROJNR)
    {
      $this->NSUBPROJNR = $NSUBPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFBLATTGUELTNR()
    {
      return $this->NTARIFBLATTGUELTNR;
    }

    /**
     * @param float $NTARIFBLATTGUELTNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNTARIFBLATTGUELTNR($NTARIFBLATTGUELTNR)
    {
      $this->NTARIFBLATTGUELTNR = $NTARIFBLATTGUELTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFBLATTNR()
    {
      return $this->NTARIFBLATTNR;
    }

    /**
     * @param float $NTARIFBLATTNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNTARIFBLATTNR($NTARIFBLATTNR)
    {
      $this->NTARIFBLATTNR = $NTARIFBLATTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSAKTART()
    {
      return $this->NTRANSAKTART;
    }

    /**
     * @param float $NTRANSAKTART
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNTRANSAKTART($NTRANSAKTART)
    {
      $this->NTRANSAKTART = $NTRANSAKTART;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNR()
    {
      return $this->NTRANSNR;
    }

    /**
     * @param float $NTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNTRANSNR($NTRANSNR)
    {
      $this->NTRANSNR = $NTRANSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENRALT()
    {
      return $this->NUNICODENRALT;
    }

    /**
     * @param float $NUNICODENRALT
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNUNICODENRALT($NUNICODENRALT)
    {
      $this->NUNICODENRALT = $NUNICODENRALT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUSTAUFDRUCKMODENR()
    {
      return $this->NUSTAUFDRUCKMODENR;
    }

    /**
     * @param float $NUSTAUFDRUCKMODENR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNUSTAUFDRUCKMODENR($NUSTAUFDRUCKMODENR)
    {
      $this->NUSTAUFDRUCKMODENR = $NUSTAUFDRUCKMODENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVERKPROJNR()
    {
      return $this->NVERKPROJNR;
    }

    /**
     * @param float $NVERKPROJNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNVERKPROJNR($NVERKPROJNR)
    {
      $this->NVERKPROJNR = $NVERKPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARESALEITEMNR()
    {
      return $this->NWARESALEITEMNR;
    }

    /**
     * @param float $NWARESALEITEMNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNWARESALEITEMNR($NWARESALEITEMNR)
    {
      $this->NWARESALEITEMNR = $NWARESALEITEMNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNZEITSTAFNR()
    {
      return $this->NZEITSTAFNR;
    }

    /**
     * @param float $NZEITSTAFNR
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setNZEITSTAFNR($NZEITSTAFNR)
    {
      $this->NZEITSTAFNR = $NZEITSTAFNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAUSGABEDAT()
    {
      return $this->SZAUSGABEDAT;
    }

    /**
     * @param string $SZAUSGABEDAT
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setSZAUSGABEDAT($SZAUSGABEDAT)
    {
      $this->SZAUSGABEDAT = $SZAUSGABEDAT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBESCH()
    {
      return $this->SZBESCH;
    }

    /**
     * @param string $SZBESCH
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setSZBESCH($SZBESCH)
    {
      $this->SZBESCH = $SZBESCH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGILTAB()
    {
      return $this->SZGILTAB;
    }

    /**
     * @param string $SZGILTAB
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setSZGILTAB($SZGILTAB)
    {
      $this->SZGILTAB = $SZGILTAB;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGILTBIS()
    {
      return $this->SZGILTBIS;
    }

    /**
     * @param string $SZGILTBIS
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setSZGILTBIS($SZGILTBIS)
    {
      $this->SZGILTBIS = $SZGILTBIS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGUELTIGABALT()
    {
      return $this->SZGUELTIGABALT;
    }

    /**
     * @param string $SZGUELTIGABALT
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setSZGUELTIGABALT($SZGUELTIGABALT)
    {
      $this->SZGUELTIGABALT = $SZGUELTIGABALT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGUELTIGBISALT()
    {
      return $this->SZGUELTIGBISALT;
    }

    /**
     * @param string $SZGUELTIGBISALT
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setSZGUELTIGBISALT($SZGUELTIGBISALT)
    {
      $this->SZGUELTIGBISALT = $SZGUELTIGBISALT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERMISSIONHANDLEOLD()
    {
      return $this->SZPERMISSIONHANDLEOLD;
    }

    /**
     * @param string $SZPERMISSIONHANDLEOLD
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setSZPERMISSIONHANDLEOLD($SZPERMISSIONHANDLEOLD)
    {
      $this->SZPERMISSIONHANDLEOLD = $SZPERMISSIONHANDLEOLD;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSTARTZEIT()
    {
      return $this->SZSTARTZEIT;
    }

    /**
     * @param string $SZSTARTZEIT
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setSZSTARTZEIT($SZSTARTZEIT)
    {
      $this->SZSTARTZEIT = $SZSTARTZEIT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZUPDATE()
    {
      return $this->SZUPDATE;
    }

    /**
     * @param string $SZUPDATE
     * @return \Axess\Dci4Wtp\D4WTPBLOCKSATZ
     */
    public function setSZUPDATE($SZUPDATE)
    {
      $this->SZUPDATE = $SZUPDATE;
      return $this;
    }

}
