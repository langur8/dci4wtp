<?php

namespace Axess\Dci4Wtp;

class D4WTPPRODSHOPPOSPREPAIDREQ2
{

    /**
     * @var float $NBASICPAYMENTTYPE
     */
    protected $NBASICPAYMENTTYPE = null;

    /**
     * @var float $NCUSTPAYMENTTYPE
     */
    protected $NCUSTPAYMENTTYPE = null;

    /**
     * @var float $NINDEXNO
     */
    protected $NINDEXNO = null;

    /**
     * @var float $NPOSITIONNO
     */
    protected $NPOSITIONNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var float $NSHOPPINGCARTPOSNO
     */
    protected $NSHOPPINGCARTPOSNO = null;

    /**
     * @var float $NSHOPPINGCARTPROJNO
     */
    protected $NSHOPPINGCARTPROJNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNBASICPAYMENTTYPE()
    {
      return $this->NBASICPAYMENTTYPE;
    }

    /**
     * @param float $NBASICPAYMENTTYPE
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ2
     */
    public function setNBASICPAYMENTTYPE($NBASICPAYMENTTYPE)
    {
      $this->NBASICPAYMENTTYPE = $NBASICPAYMENTTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCUSTPAYMENTTYPE()
    {
      return $this->NCUSTPAYMENTTYPE;
    }

    /**
     * @param float $NCUSTPAYMENTTYPE
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ2
     */
    public function setNCUSTPAYMENTTYPE($NCUSTPAYMENTTYPE)
    {
      $this->NCUSTPAYMENTTYPE = $NCUSTPAYMENTTYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNINDEXNO()
    {
      return $this->NINDEXNO;
    }

    /**
     * @param float $NINDEXNO
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ2
     */
    public function setNINDEXNO($NINDEXNO)
    {
      $this->NINDEXNO = $NINDEXNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSITIONNO()
    {
      return $this->NPOSITIONNO;
    }

    /**
     * @param float $NPOSITIONNO
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ2
     */
    public function setNPOSITIONNO($NPOSITIONNO)
    {
      $this->NPOSITIONNO = $NPOSITIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ2
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ2
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPOSNO()
    {
      return $this->NSHOPPINGCARTPOSNO;
    }

    /**
     * @param float $NSHOPPINGCARTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ2
     */
    public function setNSHOPPINGCARTPOSNO($NSHOPPINGCARTPOSNO)
    {
      $this->NSHOPPINGCARTPOSNO = $NSHOPPINGCARTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPROJNO()
    {
      return $this->NSHOPPINGCARTPROJNO;
    }

    /**
     * @param float $NSHOPPINGCARTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDREQ2
     */
    public function setNSHOPPINGCARTPROJNO($NSHOPPINGCARTPROJNO)
    {
      $this->NSHOPPINGCARTPROJNO = $NSHOPPINGCARTPROJNO;
      return $this;
    }

}
