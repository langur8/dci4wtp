<?php

namespace Axess\Dci4Wtp;

class setARCube
{

    /**
     * @var D4WTPSETARCUBEREQUEST $i_ctSetARCubeReq
     */
    protected $i_ctSetARCubeReq = null;

    /**
     * @param D4WTPSETARCUBEREQUEST $i_ctSetARCubeReq
     */
    public function __construct($i_ctSetARCubeReq)
    {
      $this->i_ctSetARCubeReq = $i_ctSetARCubeReq;
    }

    /**
     * @return D4WTPSETARCUBEREQUEST
     */
    public function getI_ctSetARCubeReq()
    {
      return $this->i_ctSetARCubeReq;
    }

    /**
     * @param D4WTPSETARCUBEREQUEST $i_ctSetARCubeReq
     * @return \Axess\Dci4Wtp\setARCube
     */
    public function setI_ctSetARCubeReq($i_ctSetARCubeReq)
    {
      $this->i_ctSetARCubeReq = $i_ctSetARCubeReq;
      return $this;
    }

}
