<?php

namespace Axess\Dci4Wtp;

class D4WTPRENTALPRODUCTORDER3
{

    /**
     * @var ArrayOfD4WTPRENTALITEMPRODUCT4 $ACTRENTALITEMPRODUCT
     */
    protected $ACTRENTALITEMPRODUCT = null;

    /**
     * @var string $SZRENTALFROM
     */
    protected $SZRENTALFROM = null;

    /**
     * @var string $SZRENTALTO
     */
    protected $SZRENTALTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPRENTALITEMPRODUCT4
     */
    public function getACTRENTALITEMPRODUCT()
    {
      return $this->ACTRENTALITEMPRODUCT;
    }

    /**
     * @param ArrayOfD4WTPRENTALITEMPRODUCT4 $ACTRENTALITEMPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCTORDER3
     */
    public function setACTRENTALITEMPRODUCT($ACTRENTALITEMPRODUCT)
    {
      $this->ACTRENTALITEMPRODUCT = $ACTRENTALITEMPRODUCT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRENTALFROM()
    {
      return $this->SZRENTALFROM;
    }

    /**
     * @param string $SZRENTALFROM
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCTORDER3
     */
    public function setSZRENTALFROM($SZRENTALFROM)
    {
      $this->SZRENTALFROM = $SZRENTALFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRENTALTO()
    {
      return $this->SZRENTALTO;
    }

    /**
     * @param string $SZRENTALTO
     * @return \Axess\Dci4Wtp\D4WTPRENTALPRODUCTORDER3
     */
    public function setSZRENTALTO($SZRENTALTO)
    {
      $this->SZRENTALTO = $SZRENTALTO;
      return $this;
    }

}
