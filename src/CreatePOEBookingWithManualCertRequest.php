<?php

namespace Axess\Dci4Wtp;

class CreatePOEBookingWithManualCertRequest
{

    /**
     * @var ArrayOfPoeBooking $PoeBookings
     */
    protected $PoeBookings = null;

    /**
     * @var int $nKassierID
     */
    protected $nKassierID = null;

    /**
     * @var string $szReason
     */
    protected $szReason = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfPoeBooking
     */
    public function getPoeBookings()
    {
      return $this->PoeBookings;
    }

    /**
     * @param ArrayOfPoeBooking $PoeBookings
     * @return \Axess\Dci4Wtp\CreatePOEBookingWithManualCertRequest
     */
    public function setPoeBookings($PoeBookings)
    {
      $this->PoeBookings = $PoeBookings;
      return $this;
    }

    /**
     * @return int
     */
    public function getNKassierID()
    {
      return $this->nKassierID;
    }

    /**
     * @param int $nKassierID
     * @return \Axess\Dci4Wtp\CreatePOEBookingWithManualCertRequest
     */
    public function setNKassierID($nKassierID)
    {
      $this->nKassierID = $nKassierID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzReason()
    {
      return $this->szReason;
    }

    /**
     * @param string $szReason
     * @return \Axess\Dci4Wtp\CreatePOEBookingWithManualCertRequest
     */
    public function setSzReason($szReason)
    {
      $this->szReason = $szReason;
      return $this;
    }

}
