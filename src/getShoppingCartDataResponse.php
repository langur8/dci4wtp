<?php

namespace Axess\Dci4Wtp;

class getShoppingCartDataResponse
{

    /**
     * @var D4WTPGETSHOPCARTDATARESULT $getShoppingCartDataResult
     */
    protected $getShoppingCartDataResult = null;

    /**
     * @param D4WTPGETSHOPCARTDATARESULT $getShoppingCartDataResult
     */
    public function __construct($getShoppingCartDataResult)
    {
      $this->getShoppingCartDataResult = $getShoppingCartDataResult;
    }

    /**
     * @return D4WTPGETSHOPCARTDATARESULT
     */
    public function getGetShoppingCartDataResult()
    {
      return $this->getShoppingCartDataResult;
    }

    /**
     * @param D4WTPGETSHOPCARTDATARESULT $getShoppingCartDataResult
     * @return \Axess\Dci4Wtp\getShoppingCartDataResponse
     */
    public function setGetShoppingCartDataResult($getShoppingCartDataResult)
    {
      $this->getShoppingCartDataResult = $getShoppingCartDataResult;
      return $this;
    }

}
