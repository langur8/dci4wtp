<?php

namespace Axess\Dci4Wtp;

class D4WTPPRODSHOPPOSPREPAIDRES
{

    /**
     * @var ArrayOfD4WTPPREPAIDTICKET3 $ACTPPREPAIDTICKET
     */
    protected $ACTPPREPAIDTICKET = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPREPAIDTICKET3
     */
    public function getACTPPREPAIDTICKET()
    {
      return $this->ACTPPREPAIDTICKET;
    }

    /**
     * @param ArrayOfD4WTPPREPAIDTICKET3 $ACTPPREPAIDTICKET
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDRES
     */
    public function setACTPPREPAIDTICKET($ACTPPREPAIDTICKET)
    {
      $this->ACTPPREPAIDTICKET = $ACTPPREPAIDTICKET;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPRODSHOPPOSPREPAIDRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
