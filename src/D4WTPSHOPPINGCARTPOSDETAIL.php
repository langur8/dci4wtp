<?php

namespace Axess\Dci4Wtp;

class D4WTPSHOPPINGCARTPOSDETAIL
{

    /**
     * @var ArrayOfD4WTPSHOPPINGCARTPOSADDART $ACTSHOPPINGCARTPOSADDART
     */
    protected $ACTSHOPPINGCARTPOSADDART = null;

    /**
     * @var float $NCHIPPROJNO
     */
    protected $NCHIPPROJNO = null;

    /**
     * @var float $NLFDPOSNO
     */
    protected $NLFDPOSNO = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var string $SZBIRTHDATE
     */
    protected $SZBIRTHDATE = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZGENDER
     */
    protected $SZGENDER = null;

    /**
     * @var string $SZINFO
     */
    protected $SZINFO = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    /**
     * @var string $SZPERSLANGCODE
     */
    protected $SZPERSLANGCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSHOPPINGCARTPOSADDART
     */
    public function getACTSHOPPINGCARTPOSADDART()
    {
      return $this->ACTSHOPPINGCARTPOSADDART;
    }

    /**
     * @param ArrayOfD4WTPSHOPPINGCARTPOSADDART $ACTSHOPPINGCARTPOSADDART
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setACTSHOPPINGCARTPOSADDART($ACTSHOPPINGCARTPOSADDART)
    {
      $this->ACTSHOPPINGCARTPOSADDART = $ACTSHOPPINGCARTPOSADDART;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCHIPPROJNO()
    {
      return $this->NCHIPPROJNO;
    }

    /**
     * @param float $NCHIPPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setNCHIPPROJNO($NCHIPPROJNO)
    {
      $this->NCHIPPROJNO = $NCHIPPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDPOSNO()
    {
      return $this->NLFDPOSNO;
    }

    /**
     * @param float $NLFDPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setNLFDPOSNO($NLFDPOSNO)
    {
      $this->NLFDPOSNO = $NLFDPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBIRTHDATE()
    {
      return $this->SZBIRTHDATE;
    }

    /**
     * @param string $SZBIRTHDATE
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setSZBIRTHDATE($SZBIRTHDATE)
    {
      $this->SZBIRTHDATE = $SZBIRTHDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGENDER()
    {
      return $this->SZGENDER;
    }

    /**
     * @param string $SZGENDER
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setSZGENDER($SZGENDER)
    {
      $this->SZGENDER = $SZGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZINFO()
    {
      return $this->SZINFO;
    }

    /**
     * @param string $SZINFO
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setSZINFO($SZINFO)
    {
      $this->SZINFO = $SZINFO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSLANGCODE()
    {
      return $this->SZPERSLANGCODE;
    }

    /**
     * @param string $SZPERSLANGCODE
     * @return \Axess\Dci4Wtp\D4WTPSHOPPINGCARTPOSDETAIL
     */
    public function setSZPERSLANGCODE($SZPERSLANGCODE)
    {
      $this->SZPERSLANGCODE = $SZPERSLANGCODE;
      return $this;
    }

}
