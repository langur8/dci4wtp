<?php

namespace Axess\Dci4Wtp;

class D4WTPWTPNOSALESDATA2
{

    /**
     * @var ArrayOfD4WTPADDARTICLEINFO $ACTADDARTICLEINFO
     */
    protected $ACTADDARTICLEINFO = null;

    /**
     * @var float $BISCANCELLED
     */
    protected $BISCANCELLED = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NPERSTYPENO
     */
    protected $NPERSTYPENO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPOSTYPENO
     */
    protected $NPOSTYPENO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSERIALNO
     */
    protected $NSERIALNO = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var float $NTRANSNO
     */
    protected $NTRANSNO = null;

    /**
     * @var float $NUNICODENO
     */
    protected $NUNICODENO = null;

    /**
     * @var string $SZCREATIONTIME
     */
    protected $SZCREATIONTIME = null;

    /**
     * @var string $SZCURRENCY
     */
    protected $SZCURRENCY = null;

    /**
     * @var string $SZPERSTYPENAME
     */
    protected $SZPERSTYPENAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZPOSTYPENAME
     */
    protected $SZPOSTYPENAME = null;

    /**
     * @var string $SZTICKETTYPENAME
     */
    protected $SZTICKETTYPENAME = null;

    /**
     * @var string $SZTIMESCALENAME
     */
    protected $SZTIMESCALENAME = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDARTICLEINFO
     */
    public function getACTADDARTICLEINFO()
    {
      return $this->ACTADDARTICLEINFO;
    }

    /**
     * @param ArrayOfD4WTPADDARTICLEINFO $ACTADDARTICLEINFO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setACTADDARTICLEINFO($ACTADDARTICLEINFO)
    {
      $this->ACTADDARTICLEINFO = $ACTADDARTICLEINFO;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISCANCELLED()
    {
      return $this->BISCANCELLED;
    }

    /**
     * @param float $BISCANCELLED
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setBISCANCELLED($BISCANCELLED)
    {
      $this->BISCANCELLED = $BISCANCELLED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSTYPENO()
    {
      return $this->NPERSTYPENO;
    }

    /**
     * @param float $NPERSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNPERSTYPENO($NPERSTYPENO)
    {
      $this->NPERSTYPENO = $NPERSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSTYPENO()
    {
      return $this->NPOSTYPENO;
    }

    /**
     * @param float $NPOSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNPOSTYPENO($NPOSTYPENO)
    {
      $this->NPOSTYPENO = $NPOSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSERIALNO()
    {
      return $this->NSERIALNO;
    }

    /**
     * @param float $NSERIALNO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNSERIALNO($NSERIALNO)
    {
      $this->NSERIALNO = $NSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNO()
    {
      return $this->NTRANSNO;
    }

    /**
     * @param float $NTRANSNO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNTRANSNO($NTRANSNO)
    {
      $this->NTRANSNO = $NTRANSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNUNICODENO()
    {
      return $this->NUNICODENO;
    }

    /**
     * @param float $NUNICODENO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setNUNICODENO($NUNICODENO)
    {
      $this->NUNICODENO = $NUNICODENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCREATIONTIME()
    {
      return $this->SZCREATIONTIME;
    }

    /**
     * @param string $SZCREATIONTIME
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setSZCREATIONTIME($SZCREATIONTIME)
    {
      $this->SZCREATIONTIME = $SZCREATIONTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCY()
    {
      return $this->SZCURRENCY;
    }

    /**
     * @param string $SZCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setSZCURRENCY($SZCURRENCY)
    {
      $this->SZCURRENCY = $SZCURRENCY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSTYPENAME()
    {
      return $this->SZPERSTYPENAME;
    }

    /**
     * @param string $SZPERSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setSZPERSTYPENAME($SZPERSTYPENAME)
    {
      $this->SZPERSTYPENAME = $SZPERSTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOSTYPENAME()
    {
      return $this->SZPOSTYPENAME;
    }

    /**
     * @param string $SZPOSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setSZPOSTYPENAME($SZPOSTYPENAME)
    {
      $this->SZPOSTYPENAME = $SZPOSTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPENAME()
    {
      return $this->SZTICKETTYPENAME;
    }

    /**
     * @param string $SZTICKETTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setSZTICKETTYPENAME($SZTICKETTYPENAME)
    {
      $this->SZTICKETTYPENAME = $SZTICKETTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTIMESCALENAME()
    {
      return $this->SZTIMESCALENAME;
    }

    /**
     * @param string $SZTIMESCALENAME
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setSZTIMESCALENAME($SZTIMESCALENAME)
    {
      $this->SZTIMESCALENAME = $SZTIMESCALENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOSALESDATA2
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
