<?php

namespace Axess\Dci4Wtp;

class setPayer
{

    /**
     * @var D4WTPPAYERREQUEST $i_payerReq
     */
    protected $i_payerReq = null;

    /**
     * @param D4WTPPAYERREQUEST $i_payerReq
     */
    public function __construct($i_payerReq)
    {
      $this->i_payerReq = $i_payerReq;
    }

    /**
     * @return D4WTPPAYERREQUEST
     */
    public function getI_payerReq()
    {
      return $this->i_payerReq;
    }

    /**
     * @param D4WTPPAYERREQUEST $i_payerReq
     * @return \Axess\Dci4Wtp\setPayer
     */
    public function setI_payerReq($i_payerReq)
    {
      $this->i_payerReq = $i_payerReq;
      return $this;
    }

}
