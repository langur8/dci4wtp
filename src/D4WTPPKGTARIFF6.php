<?php

namespace Axess\Dci4Wtp;

class D4WTPPKGTARIFF6
{

    /**
     * @var ArrayOfD4WTPADDARTTARIFF4 $ACTADDARTTARIFF
     */
    protected $ACTADDARTTARIFF = null;

    /**
     * @var ArrayOfD4WTPCONTINGENTLIST6 $ACTCONTINGENTLIST
     */
    protected $ACTCONTINGENTLIST = null;

    /**
     * @var ArrayOfD4WTPDATTRAEGNO $ACTDATTRAEGNO
     */
    protected $ACTDATTRAEGNO = null;

    /**
     * @var float $BPARKINGFEATURE
     */
    protected $BPARKINGFEATURE = null;

    /**
     * @var float $BPERSDATA
     */
    protected $BPERSDATA = null;

    /**
     * @var float $BPHOTOMANDATORY
     */
    protected $BPHOTOMANDATORY = null;

    /**
     * @var float $BPREDATEABLE
     */
    protected $BPREDATEABLE = null;

    /**
     * @var float $BWTPWITHOUTPRINTER
     */
    protected $BWTPWITHOUTPRINTER = null;

    /**
     * @var float $BWTPWITHPRINTER
     */
    protected $BWTPWITHPRINTER = null;

    /**
     * @var D4WTPCOMPANYCONDITION $CTCOMPANYCONDITION
     */
    protected $CTCOMPANYCONDITION = null;

    /**
     * @var D4WTPDYNAMICPRICING $CTDYNAMICPRICING
     */
    protected $CTDYNAMICPRICING = null;

    /**
     * @var float $NBASICCARDTYPENO
     */
    protected $NBASICCARDTYPENO = null;

    /**
     * @var float $NDEFDATACARRIERTYPENO
     */
    protected $NDEFDATACARRIERTYPENO = null;

    /**
     * @var float $NDEFROHLINGSTYPENO
     */
    protected $NDEFROHLINGSTYPENO = null;

    /**
     * @var float $NPERSONTYPENO
     */
    protected $NPERSONTYPENO = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NTARIFFNO
     */
    protected $NTARIFFNO = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var float $NVATAMOUNT
     */
    protected $NVATAMOUNT = null;

    /**
     * @var float $NVATPERCENT
     */
    protected $NVATPERCENT = null;

    /**
     * @var string $SZCURRENCY
     */
    protected $SZCURRENCY = null;

    /**
     * @var string $SZEXTMATERIALNO
     */
    protected $SZEXTMATERIALNO = null;

    /**
     * @var string $SZPERSTYPENAME
     */
    protected $SZPERSTYPENAME = null;

    /**
     * @var string $SZPOOLNAME
     */
    protected $SZPOOLNAME = null;

    /**
     * @var string $SZTARIFVALIDTO
     */
    protected $SZTARIFVALIDTO = null;

    /**
     * @var string $SZTICKETTYPENAME
     */
    protected $SZTICKETTYPENAME = null;

    /**
     * @var string $SZVALIDTO
     */
    protected $SZVALIDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPADDARTTARIFF4
     */
    public function getACTADDARTTARIFF()
    {
      return $this->ACTADDARTTARIFF;
    }

    /**
     * @param ArrayOfD4WTPADDARTTARIFF4 $ACTADDARTTARIFF
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setACTADDARTTARIFF($ACTADDARTTARIFF)
    {
      $this->ACTADDARTTARIFF = $ACTADDARTTARIFF;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPCONTINGENTLIST6
     */
    public function getACTCONTINGENTLIST()
    {
      return $this->ACTCONTINGENTLIST;
    }

    /**
     * @param ArrayOfD4WTPCONTINGENTLIST6 $ACTCONTINGENTLIST
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setACTCONTINGENTLIST($ACTCONTINGENTLIST)
    {
      $this->ACTCONTINGENTLIST = $ACTCONTINGENTLIST;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPDATTRAEGNO
     */
    public function getACTDATTRAEGNO()
    {
      return $this->ACTDATTRAEGNO;
    }

    /**
     * @param ArrayOfD4WTPDATTRAEGNO $ACTDATTRAEGNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setACTDATTRAEGNO($ACTDATTRAEGNO)
    {
      $this->ACTDATTRAEGNO = $ACTDATTRAEGNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPARKINGFEATURE()
    {
      return $this->BPARKINGFEATURE;
    }

    /**
     * @param float $BPARKINGFEATURE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setBPARKINGFEATURE($BPARKINGFEATURE)
    {
      $this->BPARKINGFEATURE = $BPARKINGFEATURE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPERSDATA()
    {
      return $this->BPERSDATA;
    }

    /**
     * @param float $BPERSDATA
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setBPERSDATA($BPERSDATA)
    {
      $this->BPERSDATA = $BPERSDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPHOTOMANDATORY()
    {
      return $this->BPHOTOMANDATORY;
    }

    /**
     * @param float $BPHOTOMANDATORY
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setBPHOTOMANDATORY($BPHOTOMANDATORY)
    {
      $this->BPHOTOMANDATORY = $BPHOTOMANDATORY;
      return $this;
    }

    /**
     * @return float
     */
    public function getBPREDATEABLE()
    {
      return $this->BPREDATEABLE;
    }

    /**
     * @param float $BPREDATEABLE
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setBPREDATEABLE($BPREDATEABLE)
    {
      $this->BPREDATEABLE = $BPREDATEABLE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBWTPWITHOUTPRINTER()
    {
      return $this->BWTPWITHOUTPRINTER;
    }

    /**
     * @param float $BWTPWITHOUTPRINTER
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setBWTPWITHOUTPRINTER($BWTPWITHOUTPRINTER)
    {
      $this->BWTPWITHOUTPRINTER = $BWTPWITHOUTPRINTER;
      return $this;
    }

    /**
     * @return float
     */
    public function getBWTPWITHPRINTER()
    {
      return $this->BWTPWITHPRINTER;
    }

    /**
     * @param float $BWTPWITHPRINTER
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setBWTPWITHPRINTER($BWTPWITHPRINTER)
    {
      $this->BWTPWITHPRINTER = $BWTPWITHPRINTER;
      return $this;
    }

    /**
     * @return D4WTPCOMPANYCONDITION
     */
    public function getCTCOMPANYCONDITION()
    {
      return $this->CTCOMPANYCONDITION;
    }

    /**
     * @param D4WTPCOMPANYCONDITION $CTCOMPANYCONDITION
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setCTCOMPANYCONDITION($CTCOMPANYCONDITION)
    {
      $this->CTCOMPANYCONDITION = $CTCOMPANYCONDITION;
      return $this;
    }

    /**
     * @return D4WTPDYNAMICPRICING
     */
    public function getCTDYNAMICPRICING()
    {
      return $this->CTDYNAMICPRICING;
    }

    /**
     * @param D4WTPDYNAMICPRICING $CTDYNAMICPRICING
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setCTDYNAMICPRICING($CTDYNAMICPRICING)
    {
      $this->CTDYNAMICPRICING = $CTDYNAMICPRICING;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBASICCARDTYPENO()
    {
      return $this->NBASICCARDTYPENO;
    }

    /**
     * @param float $NBASICCARDTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setNBASICCARDTYPENO($NBASICCARDTYPENO)
    {
      $this->NBASICCARDTYPENO = $NBASICCARDTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDEFDATACARRIERTYPENO()
    {
      return $this->NDEFDATACARRIERTYPENO;
    }

    /**
     * @param float $NDEFDATACARRIERTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setNDEFDATACARRIERTYPENO($NDEFDATACARRIERTYPENO)
    {
      $this->NDEFDATACARRIERTYPENO = $NDEFDATACARRIERTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDEFROHLINGSTYPENO()
    {
      return $this->NDEFROHLINGSTYPENO;
    }

    /**
     * @param float $NDEFROHLINGSTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setNDEFROHLINGSTYPENO($NDEFROHLINGSTYPENO)
    {
      $this->NDEFROHLINGSTYPENO = $NDEFROHLINGSTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSONTYPENO()
    {
      return $this->NPERSONTYPENO;
    }

    /**
     * @param float $NPERSONTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setNPERSONTYPENO($NPERSONTYPENO)
    {
      $this->NPERSONTYPENO = $NPERSONTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFFNO()
    {
      return $this->NTARIFFNO;
    }

    /**
     * @param float $NTARIFFNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setNTARIFFNO($NTARIFFNO)
    {
      $this->NTARIFFNO = $NTARIFFNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATAMOUNT()
    {
      return $this->NVATAMOUNT;
    }

    /**
     * @param float $NVATAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setNVATAMOUNT($NVATAMOUNT)
    {
      $this->NVATAMOUNT = $NVATAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATPERCENT()
    {
      return $this->NVATPERCENT;
    }

    /**
     * @param float $NVATPERCENT
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setNVATPERCENT($NVATPERCENT)
    {
      $this->NVATPERCENT = $NVATPERCENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCY()
    {
      return $this->SZCURRENCY;
    }

    /**
     * @param string $SZCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setSZCURRENCY($SZCURRENCY)
    {
      $this->SZCURRENCY = $SZCURRENCY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTMATERIALNO()
    {
      return $this->SZEXTMATERIALNO;
    }

    /**
     * @param string $SZEXTMATERIALNO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setSZEXTMATERIALNO($SZEXTMATERIALNO)
    {
      $this->SZEXTMATERIALNO = $SZEXTMATERIALNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSTYPENAME()
    {
      return $this->SZPERSTYPENAME;
    }

    /**
     * @param string $SZPERSTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setSZPERSTYPENAME($SZPERSTYPENAME)
    {
      $this->SZPERSTYPENAME = $SZPERSTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOOLNAME()
    {
      return $this->SZPOOLNAME;
    }

    /**
     * @param string $SZPOOLNAME
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setSZPOOLNAME($SZPOOLNAME)
    {
      $this->SZPOOLNAME = $SZPOOLNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTARIFVALIDTO()
    {
      return $this->SZTARIFVALIDTO;
    }

    /**
     * @param string $SZTARIFVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setSZTARIFVALIDTO($SZTARIFVALIDTO)
    {
      $this->SZTARIFVALIDTO = $SZTARIFVALIDTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTICKETTYPENAME()
    {
      return $this->SZTICKETTYPENAME;
    }

    /**
     * @param string $SZTICKETTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setSZTICKETTYPENAME($SZTICKETTYPENAME)
    {
      $this->SZTICKETTYPENAME = $SZTICKETTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDTO()
    {
      return $this->SZVALIDTO;
    }

    /**
     * @param string $SZVALIDTO
     * @return \Axess\Dci4Wtp\D4WTPPKGTARIFF6
     */
    public function setSZVALIDTO($SZVALIDTO)
    {
      $this->SZVALIDTO = $SZVALIDTO;
      return $this;
    }

}
