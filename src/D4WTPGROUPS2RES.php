<?php

namespace Axess\Dci4Wtp;

class D4WTPGROUPS2RES
{

    /**
     * @var ArrayOfD4WTPGROUP2 $ACTGROUP2
     */
    protected $ACTGROUP2 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPGROUP2
     */
    public function getACTGROUP2()
    {
      return $this->ACTGROUP2;
    }

    /**
     * @param ArrayOfD4WTPGROUP2 $ACTGROUP2
     * @return \Axess\Dci4Wtp\D4WTPGROUPS2RES
     */
    public function setACTGROUP2($ACTGROUP2)
    {
      $this->ACTGROUP2 = $ACTGROUP2;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGROUPS2RES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGROUPS2RES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
