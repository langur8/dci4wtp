<?php

namespace Axess\Dci4Wtp;

class getCreditCardReferenceResponse
{

    /**
     * @var D4WTPGETCTCREFIDRESULT $getCreditCardReferenceResult
     */
    protected $getCreditCardReferenceResult = null;

    /**
     * @param D4WTPGETCTCREFIDRESULT $getCreditCardReferenceResult
     */
    public function __construct($getCreditCardReferenceResult)
    {
      $this->getCreditCardReferenceResult = $getCreditCardReferenceResult;
    }

    /**
     * @return D4WTPGETCTCREFIDRESULT
     */
    public function getGetCreditCardReferenceResult()
    {
      return $this->getCreditCardReferenceResult;
    }

    /**
     * @param D4WTPGETCTCREFIDRESULT $getCreditCardReferenceResult
     * @return \Axess\Dci4Wtp\getCreditCardReferenceResponse
     */
    public function setGetCreditCardReferenceResult($getCreditCardReferenceResult)
    {
      $this->getCreditCardReferenceResult = $getCreditCardReferenceResult;
      return $this;
    }

}
