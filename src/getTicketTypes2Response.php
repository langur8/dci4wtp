<?php

namespace Axess\Dci4Wtp;

class getTicketTypes2Response
{

    /**
     * @var D4WTPTICKETTYPE2RESULT $getTicketTypes2Result
     */
    protected $getTicketTypes2Result = null;

    /**
     * @param D4WTPTICKETTYPE2RESULT $getTicketTypes2Result
     */
    public function __construct($getTicketTypes2Result)
    {
      $this->getTicketTypes2Result = $getTicketTypes2Result;
    }

    /**
     * @return D4WTPTICKETTYPE2RESULT
     */
    public function getGetTicketTypes2Result()
    {
      return $this->getTicketTypes2Result;
    }

    /**
     * @param D4WTPTICKETTYPE2RESULT $getTicketTypes2Result
     * @return \Axess\Dci4Wtp\getTicketTypes2Response
     */
    public function setGetTicketTypes2Result($getTicketTypes2Result)
    {
      $this->getTicketTypes2Result = $getTicketTypes2Result;
      return $this;
    }

}
