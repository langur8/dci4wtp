<?php

namespace Axess\Dci4Wtp;

class getGroupReservationResponse
{

    /**
     * @var D4WTPGETGRPRESERVATIONRESULT $getGroupReservationResult
     */
    protected $getGroupReservationResult = null;

    /**
     * @param D4WTPGETGRPRESERVATIONRESULT $getGroupReservationResult
     */
    public function __construct($getGroupReservationResult)
    {
      $this->getGroupReservationResult = $getGroupReservationResult;
    }

    /**
     * @return D4WTPGETGRPRESERVATIONRESULT
     */
    public function getGetGroupReservationResult()
    {
      return $this->getGroupReservationResult;
    }

    /**
     * @param D4WTPGETGRPRESERVATIONRESULT $getGroupReservationResult
     * @return \Axess\Dci4Wtp\getGroupReservationResponse
     */
    public function setGetGroupReservationResult($getGroupReservationResult)
    {
      $this->getGroupReservationResult = $getGroupReservationResult;
      return $this;
    }

}
