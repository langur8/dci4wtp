<?php

namespace Axess\Dci4Wtp;

class ArrayOfDATE implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DATE[] $DATE
     */
    protected $DATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DATE[]
     */
    public function getDATE()
    {
      return $this->DATE;
    }

    /**
     * @param DATE[] $DATE
     * @return \Axess\Dci4Wtp\ArrayOfDATE
     */
    public function setDATE(array $DATE = null)
    {
      $this->DATE = $DATE;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DATE[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DATE
     */
    public function offsetGet($offset)
    {
      return $this->DATE[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DATE $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DATE[] = $value;
      } else {
        $this->DATE[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DATE[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DATE Return the current element
     */
    public function current()
    {
      return current($this->DATE);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DATE);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DATE);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DATE);
    }

    /**
     * Countable implementation
     *
     * @return DATE Return count of elements
     */
    public function count()
    {
      return count($this->DATE);
    }

}
