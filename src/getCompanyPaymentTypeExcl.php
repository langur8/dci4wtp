<?php

namespace Axess\Dci4Wtp;

class getCompanyPaymentTypeExcl
{

    /**
     * @var D4WTPFIRMENBZHLARTEXREQUEST $i_ctCompanyPaymentTypeExcl
     */
    protected $i_ctCompanyPaymentTypeExcl = null;

    /**
     * @param D4WTPFIRMENBZHLARTEXREQUEST $i_ctCompanyPaymentTypeExcl
     */
    public function __construct($i_ctCompanyPaymentTypeExcl)
    {
      $this->i_ctCompanyPaymentTypeExcl = $i_ctCompanyPaymentTypeExcl;
    }

    /**
     * @return D4WTPFIRMENBZHLARTEXREQUEST
     */
    public function getI_ctCompanyPaymentTypeExcl()
    {
      return $this->i_ctCompanyPaymentTypeExcl;
    }

    /**
     * @param D4WTPFIRMENBZHLARTEXREQUEST $i_ctCompanyPaymentTypeExcl
     * @return \Axess\Dci4Wtp\getCompanyPaymentTypeExcl
     */
    public function setI_ctCompanyPaymentTypeExcl($i_ctCompanyPaymentTypeExcl)
    {
      $this->i_ctCompanyPaymentTypeExcl = $i_ctCompanyPaymentTypeExcl;
      return $this;
    }

}
