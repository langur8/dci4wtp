<?php

namespace Axess\Dci4Wtp;

class getTariff2Response
{

    /**
     * @var D4WTPTARIFF2RESULT $getTariff2Result
     */
    protected $getTariff2Result = null;

    /**
     * @param D4WTPTARIFF2RESULT $getTariff2Result
     */
    public function __construct($getTariff2Result)
    {
      $this->getTariff2Result = $getTariff2Result;
    }

    /**
     * @return D4WTPTARIFF2RESULT
     */
    public function getGetTariff2Result()
    {
      return $this->getTariff2Result;
    }

    /**
     * @param D4WTPTARIFF2RESULT $getTariff2Result
     * @return \Axess\Dci4Wtp\getTariff2Response
     */
    public function setGetTariff2Result($getTariff2Result)
    {
      $this->getTariff2Result = $getTariff2Result;
      return $this;
    }

}
