<?php

namespace Axess\Dci4Wtp;

class msgCancelTicketResponse
{

    /**
     * @var D4WTPMSGCANCELTICKETRESULT $msgCancelTicketResult
     */
    protected $msgCancelTicketResult = null;

    /**
     * @param D4WTPMSGCANCELTICKETRESULT $msgCancelTicketResult
     */
    public function __construct($msgCancelTicketResult)
    {
      $this->msgCancelTicketResult = $msgCancelTicketResult;
    }

    /**
     * @return D4WTPMSGCANCELTICKETRESULT
     */
    public function getMsgCancelTicketResult()
    {
      return $this->msgCancelTicketResult;
    }

    /**
     * @param D4WTPMSGCANCELTICKETRESULT $msgCancelTicketResult
     * @return \Axess\Dci4Wtp\msgCancelTicketResponse
     */
    public function setMsgCancelTicketResult($msgCancelTicketResult)
    {
      $this->msgCancelTicketResult = $msgCancelTicketResult;
      return $this;
    }

}
