<?php

namespace Axess\Dci4Wtp;

class setPrepaidTcktPlusDataResponse
{

    /**
     * @var D4WTPRESULT $setPrepaidTcktPlusDataResult
     */
    protected $setPrepaidTcktPlusDataResult = null;

    /**
     * @param D4WTPRESULT $setPrepaidTcktPlusDataResult
     */
    public function __construct($setPrepaidTcktPlusDataResult)
    {
      $this->setPrepaidTcktPlusDataResult = $setPrepaidTcktPlusDataResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSetPrepaidTcktPlusDataResult()
    {
      return $this->setPrepaidTcktPlusDataResult;
    }

    /**
     * @param D4WTPRESULT $setPrepaidTcktPlusDataResult
     * @return \Axess\Dci4Wtp\setPrepaidTcktPlusDataResponse
     */
    public function setSetPrepaidTcktPlusDataResult($setPrepaidTcktPlusDataResult)
    {
      $this->setPrepaidTcktPlusDataResult = $setPrepaidTcktPlusDataResult;
      return $this;
    }

}
