<?php

namespace Axess\Dci4Wtp;

class D4WTPTICKETTYPE2
{

    /**
     * @var float $BVORDATIERBAR
     */
    protected $BVORDATIERBAR = null;

    /**
     * @var float $BWTPMITDRUCKER
     */
    protected $BWTPMITDRUCKER = null;

    /**
     * @var float $BWTPOHNEDRUCKER
     */
    protected $BWTPOHNEDRUCKER = null;

    /**
     * @var float $NKARTENGRUNDTYPNR
     */
    protected $NKARTENGRUNDTYPNR = null;

    /**
     * @var float $NPOOLNO
     */
    protected $NPOOLNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NTICKETTYPENO
     */
    protected $NTICKETTYPENO = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBVORDATIERBAR()
    {
      return $this->BVORDATIERBAR;
    }

    /**
     * @param float $BVORDATIERBAR
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE2
     */
    public function setBVORDATIERBAR($BVORDATIERBAR)
    {
      $this->BVORDATIERBAR = $BVORDATIERBAR;
      return $this;
    }

    /**
     * @return float
     */
    public function getBWTPMITDRUCKER()
    {
      return $this->BWTPMITDRUCKER;
    }

    /**
     * @param float $BWTPMITDRUCKER
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE2
     */
    public function setBWTPMITDRUCKER($BWTPMITDRUCKER)
    {
      $this->BWTPMITDRUCKER = $BWTPMITDRUCKER;
      return $this;
    }

    /**
     * @return float
     */
    public function getBWTPOHNEDRUCKER()
    {
      return $this->BWTPOHNEDRUCKER;
    }

    /**
     * @param float $BWTPOHNEDRUCKER
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE2
     */
    public function setBWTPOHNEDRUCKER($BWTPOHNEDRUCKER)
    {
      $this->BWTPOHNEDRUCKER = $BWTPOHNEDRUCKER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNKARTENGRUNDTYPNR()
    {
      return $this->NKARTENGRUNDTYPNR;
    }

    /**
     * @param float $NKARTENGRUNDTYPNR
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE2
     */
    public function setNKARTENGRUNDTYPNR($NKARTENGRUNDTYPNR)
    {
      $this->NKARTENGRUNDTYPNR = $NKARTENGRUNDTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOOLNO()
    {
      return $this->NPOOLNO;
    }

    /**
     * @param float $NPOOLNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE2
     */
    public function setNPOOLNO($NPOOLNO)
    {
      $this->NPOOLNO = $NPOOLNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE2
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTICKETTYPENO()
    {
      return $this->NTICKETTYPENO;
    }

    /**
     * @param float $NTICKETTYPENO
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE2
     */
    public function setNTICKETTYPENO($NTICKETTYPENO)
    {
      $this->NTICKETTYPENO = $NTICKETTYPENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPTICKETTYPE2
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

}
