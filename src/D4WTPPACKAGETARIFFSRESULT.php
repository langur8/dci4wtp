<?php

namespace Axess\Dci4Wtp;

class D4WTPPACKAGETARIFFSRESULT
{

    /**
     * @var ArrayOfD4WTPPACKAGETARIFF $ACTPACKAGETARIFF
     */
    protected $ACTPACKAGETARIFF = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPPACKAGETARIFF
     */
    public function getACTPACKAGETARIFF()
    {
      return $this->ACTPACKAGETARIFF;
    }

    /**
     * @param ArrayOfD4WTPPACKAGETARIFF $ACTPACKAGETARIFF
     * @return \Axess\Dci4Wtp\D4WTPPACKAGETARIFFSRESULT
     */
    public function setACTPACKAGETARIFF($ACTPACKAGETARIFF)
    {
      $this->ACTPACKAGETARIFF = $ACTPACKAGETARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPPACKAGETARIFFSRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPPACKAGETARIFFSRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
