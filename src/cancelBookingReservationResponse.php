<?php

namespace Axess\Dci4Wtp;

class cancelBookingReservationResponse
{

    /**
     * @var WTPCancelBookingResult $cancelBookingReservationResult
     */
    protected $cancelBookingReservationResult = null;

    /**
     * @param WTPCancelBookingResult $cancelBookingReservationResult
     */
    public function __construct($cancelBookingReservationResult)
    {
      $this->cancelBookingReservationResult = $cancelBookingReservationResult;
    }

    /**
     * @return WTPCancelBookingResult
     */
    public function getCancelBookingReservationResult()
    {
      return $this->cancelBookingReservationResult;
    }

    /**
     * @param WTPCancelBookingResult $cancelBookingReservationResult
     * @return \Axess\Dci4Wtp\cancelBookingReservationResponse
     */
    public function setCancelBookingReservationResult($cancelBookingReservationResult)
    {
      $this->cancelBookingReservationResult = $cancelBookingReservationResult;
      return $this;
    }

}
