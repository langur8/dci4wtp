<?php

namespace Axess\Dci4Wtp;

class D4WTPREDEEMPROMOCODES
{

    /**
     * @var D4WTPPRODUCT $CTPRODUCT
     */
    protected $CTPRODUCT = null;

    /**
     * @var float $NCOUNTREDEEMED
     */
    protected $NCOUNTREDEEMED = null;

    /**
     * @var float $NTIMEOUTINMINUTES
     */
    protected $NTIMEOUTINMINUTES = null;

    /**
     * @var string $SZPROMOCODE
     */
    protected $SZPROMOCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPPRODUCT
     */
    public function getCTPRODUCT()
    {
      return $this->CTPRODUCT;
    }

    /**
     * @param D4WTPPRODUCT $CTPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPREDEEMPROMOCODES
     */
    public function setCTPRODUCT($CTPRODUCT)
    {
      $this->CTPRODUCT = $CTPRODUCT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTREDEEMED()
    {
      return $this->NCOUNTREDEEMED;
    }

    /**
     * @param float $NCOUNTREDEEMED
     * @return \Axess\Dci4Wtp\D4WTPREDEEMPROMOCODES
     */
    public function setNCOUNTREDEEMED($NCOUNTREDEEMED)
    {
      $this->NCOUNTREDEEMED = $NCOUNTREDEEMED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTIMEOUTINMINUTES()
    {
      return $this->NTIMEOUTINMINUTES;
    }

    /**
     * @param float $NTIMEOUTINMINUTES
     * @return \Axess\Dci4Wtp\D4WTPREDEEMPROMOCODES
     */
    public function setNTIMEOUTINMINUTES($NTIMEOUTINMINUTES)
    {
      $this->NTIMEOUTINMINUTES = $NTIMEOUTINMINUTES;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPROMOCODE()
    {
      return $this->SZPROMOCODE;
    }

    /**
     * @param string $SZPROMOCODE
     * @return \Axess\Dci4Wtp\D4WTPREDEEMPROMOCODES
     */
    public function setSZPROMOCODE($SZPROMOCODE)
    {
      $this->SZPROMOCODE = $SZPROMOCODE;
      return $this;
    }

}
