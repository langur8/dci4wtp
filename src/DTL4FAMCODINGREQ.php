<?php

namespace Axess\Dci4Wtp;

class DTL4FAMCODINGREQ
{

    /**
     * @var float $BISMEMBER
     */
    protected $BISMEMBER = null;

    /**
     * @var float $NMASTERJOURNALNO
     */
    protected $NMASTERJOURNALNO = null;

    /**
     * @var float $NMASTERPOSNO
     */
    protected $NMASTERPOSNO = null;

    /**
     * @var float $NMASTERPROJNO
     */
    protected $NMASTERPROJNO = null;

    /**
     * @var float $NMASTERSERIALNO
     */
    protected $NMASTERSERIALNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var string $SZMASTERVALID
     */
    protected $SZMASTERVALID = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    /**
     * @var string $SZWTPNR64
     */
    protected $SZWTPNR64 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBISMEMBER()
    {
      return $this->BISMEMBER;
    }

    /**
     * @param float $BISMEMBER
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGREQ
     */
    public function setBISMEMBER($BISMEMBER)
    {
      $this->BISMEMBER = $BISMEMBER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERJOURNALNO()
    {
      return $this->NMASTERJOURNALNO;
    }

    /**
     * @param float $NMASTERJOURNALNO
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGREQ
     */
    public function setNMASTERJOURNALNO($NMASTERJOURNALNO)
    {
      $this->NMASTERJOURNALNO = $NMASTERJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERPOSNO()
    {
      return $this->NMASTERPOSNO;
    }

    /**
     * @param float $NMASTERPOSNO
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGREQ
     */
    public function setNMASTERPOSNO($NMASTERPOSNO)
    {
      $this->NMASTERPOSNO = $NMASTERPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERPROJNO()
    {
      return $this->NMASTERPROJNO;
    }

    /**
     * @param float $NMASTERPROJNO
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGREQ
     */
    public function setNMASTERPROJNO($NMASTERPROJNO)
    {
      $this->NMASTERPROJNO = $NMASTERPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMASTERSERIALNO()
    {
      return $this->NMASTERSERIALNO;
    }

    /**
     * @param float $NMASTERSERIALNO
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGREQ
     */
    public function setNMASTERSERIALNO($NMASTERSERIALNO)
    {
      $this->NMASTERSERIALNO = $NMASTERSERIALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASTERVALID()
    {
      return $this->SZMASTERVALID;
    }

    /**
     * @param string $SZMASTERVALID
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGREQ
     */
    public function setSZMASTERVALID($SZMASTERVALID)
    {
      $this->SZMASTERVALID = $SZMASTERVALID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGREQ
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNR64()
    {
      return $this->SZWTPNR64;
    }

    /**
     * @param string $SZWTPNR64
     * @return \Axess\Dci4Wtp\DTL4FAMCODINGREQ
     */
    public function setSZWTPNR64($SZWTPNR64)
    {
      $this->SZWTPNR64 = $SZWTPNR64;
      return $this;
    }

}
