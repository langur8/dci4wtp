<?php

namespace Axess\Dci4Wtp;

class forgotPassword
{

    /**
     * @var D4WTPFORGOTPASSREQ $i_ctforgotPass
     */
    protected $i_ctforgotPass = null;

    /**
     * @param D4WTPFORGOTPASSREQ $i_ctforgotPass
     */
    public function __construct($i_ctforgotPass)
    {
      $this->i_ctforgotPass = $i_ctforgotPass;
    }

    /**
     * @return D4WTPFORGOTPASSREQ
     */
    public function getI_ctforgotPass()
    {
      return $this->i_ctforgotPass;
    }

    /**
     * @param D4WTPFORGOTPASSREQ $i_ctforgotPass
     * @return \Axess\Dci4Wtp\forgotPassword
     */
    public function setI_ctforgotPass($i_ctforgotPass)
    {
      $this->i_ctforgotPass = $i_ctforgotPass;
      return $this;
    }

}
