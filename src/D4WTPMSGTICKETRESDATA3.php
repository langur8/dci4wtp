<?php

namespace Axess\Dci4Wtp;

class D4WTPMSGTICKETRESDATA3
{

    /**
     * @var D4WTPMSGRENTALRES $CTMSGRENTALRES
     */
    protected $CTMSGRENTALRES = null;

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NBLOCKNR
     */
    protected $NBLOCKNR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NJOURNALNO
     */
    protected $NJOURNALNO = null;

    /**
     * @var float $NPOSNO
     */
    protected $NPOSNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NTRANSNR
     */
    protected $NTRANSNR = null;

    /**
     * @var float $NWAREITEMNO
     */
    protected $NWAREITEMNO = null;

    /**
     * @var float $NWARESINGLENR
     */
    protected $NWARESINGLENR = null;

    /**
     * @var string $SZBARCODEIDENTIFIER
     */
    protected $SZBARCODEIDENTIFIER = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZEXTORDERNUMBER
     */
    protected $SZEXTORDERNUMBER = null;

    /**
     * @var string $SZMESSAGETYPE
     */
    protected $SZMESSAGETYPE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPMSGRENTALRES
     */
    public function getCTMSGRENTALRES()
    {
      return $this->CTMSGRENTALRES;
    }

    /**
     * @param D4WTPMSGRENTALRES $CTMSGRENTALRES
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setCTMSGRENTALRES($CTMSGRENTALRES)
    {
      $this->CTMSGRENTALRES = $CTMSGRENTALRES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLOCKNR()
    {
      return $this->NBLOCKNR;
    }

    /**
     * @param float $NBLOCKNR
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setNBLOCKNR($NBLOCKNR)
    {
      $this->NBLOCKNR = $NBLOCKNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNJOURNALNO()
    {
      return $this->NJOURNALNO;
    }

    /**
     * @param float $NJOURNALNO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setNJOURNALNO($NJOURNALNO)
    {
      $this->NJOURNALNO = $NJOURNALNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNO()
    {
      return $this->NPOSNO;
    }

    /**
     * @param float $NPOSNO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setNPOSNO($NPOSNO)
    {
      $this->NPOSNO = $NPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNR()
    {
      return $this->NTRANSNR;
    }

    /**
     * @param float $NTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setNTRANSNR($NTRANSNR)
    {
      $this->NTRANSNR = $NTRANSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWAREITEMNO()
    {
      return $this->NWAREITEMNO;
    }

    /**
     * @param float $NWAREITEMNO
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setNWAREITEMNO($NWAREITEMNO)
    {
      $this->NWAREITEMNO = $NWAREITEMNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWARESINGLENR()
    {
      return $this->NWARESINGLENR;
    }

    /**
     * @param float $NWARESINGLENR
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setNWARESINGLENR($NWARESINGLENR)
    {
      $this->NWARESINGLENR = $NWARESINGLENR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBARCODEIDENTIFIER()
    {
      return $this->SZBARCODEIDENTIFIER;
    }

    /**
     * @param string $SZBARCODEIDENTIFIER
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setSZBARCODEIDENTIFIER($SZBARCODEIDENTIFIER)
    {
      $this->SZBARCODEIDENTIFIER = $SZBARCODEIDENTIFIER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTORDERNUMBER()
    {
      return $this->SZEXTORDERNUMBER;
    }

    /**
     * @param string $SZEXTORDERNUMBER
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setSZEXTORDERNUMBER($SZEXTORDERNUMBER)
    {
      $this->SZEXTORDERNUMBER = $SZEXTORDERNUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMESSAGETYPE()
    {
      return $this->SZMESSAGETYPE;
    }

    /**
     * @param string $SZMESSAGETYPE
     * @return \Axess\Dci4Wtp\D4WTPMSGTICKETRESDATA3
     */
    public function setSZMESSAGETYPE($SZMESSAGETYPE)
    {
      $this->SZMESSAGETYPE = $SZMESSAGETYPE;
      return $this;
    }

}
