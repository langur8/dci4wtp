<?php

namespace Axess\Dci4Wtp;

class D4WTPRECEIPTDATA
{

    /**
     * @var ArrayOfD4WTPRECEIPTADDARTICLES $ACTADDARTICLEDATA
     */
    protected $ACTADDARTICLEDATA = null;

    /**
     * @var ArrayOfD4WTPRECEIPTARTICLES $ACTARTICLEDATA
     */
    protected $ACTARTICLEDATA = null;

    /**
     * @var ArrayOfD4WTPRECEIPTPAYMENT $ACTPAYMENTDATA
     */
    protected $ACTPAYMENTDATA = null;

    /**
     * @var ArrayOfD4WTPRECEIPTTICKETS $ACTTICKETDATA
     */
    protected $ACTTICKETDATA = null;

    /**
     * @var float $BISRECEIPTDUPLICATE
     */
    protected $BISRECEIPTDUPLICATE = null;

    /**
     * @var D4WTPRECEIPTPAYERDATA $CTPAYERDATA
     */
    protected $CTPAYERDATA = null;

    /**
     * @var float $NGROSSTARIFF
     */
    protected $NGROSSTARIFF = null;

    /**
     * @var float $NNETTARIFF
     */
    protected $NNETTARIFF = null;

    /**
     * @var float $NNF525RECEIPTNO
     */
    protected $NNF525RECEIPTNO = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NRECEIPTNO
     */
    protected $NRECEIPTNO = null;

    /**
     * @var float $NTRANSNR
     */
    protected $NTRANSNR = null;

    /**
     * @var float $NTRANSSTATUS
     */
    protected $NTRANSSTATUS = null;

    /**
     * @var string $SZCREATIONTIME
     */
    protected $SZCREATIONTIME = null;

    /**
     * @var string $SZCURRENCY
     */
    protected $SZCURRENCY = null;

    /**
     * @var string $SZEXTINVOICENO
     */
    protected $SZEXTINVOICENO = null;

    /**
     * @var string $SZWSHOPRECEIPTNO
     */
    protected $SZWSHOPRECEIPTNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPRECEIPTADDARTICLES
     */
    public function getACTADDARTICLEDATA()
    {
      return $this->ACTADDARTICLEDATA;
    }

    /**
     * @param ArrayOfD4WTPRECEIPTADDARTICLES $ACTADDARTICLEDATA
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setACTADDARTICLEDATA($ACTADDARTICLEDATA)
    {
      $this->ACTADDARTICLEDATA = $ACTADDARTICLEDATA;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPRECEIPTARTICLES
     */
    public function getACTARTICLEDATA()
    {
      return $this->ACTARTICLEDATA;
    }

    /**
     * @param ArrayOfD4WTPRECEIPTARTICLES $ACTARTICLEDATA
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setACTARTICLEDATA($ACTARTICLEDATA)
    {
      $this->ACTARTICLEDATA = $ACTARTICLEDATA;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPRECEIPTPAYMENT
     */
    public function getACTPAYMENTDATA()
    {
      return $this->ACTPAYMENTDATA;
    }

    /**
     * @param ArrayOfD4WTPRECEIPTPAYMENT $ACTPAYMENTDATA
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setACTPAYMENTDATA($ACTPAYMENTDATA)
    {
      $this->ACTPAYMENTDATA = $ACTPAYMENTDATA;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPRECEIPTTICKETS
     */
    public function getACTTICKETDATA()
    {
      return $this->ACTTICKETDATA;
    }

    /**
     * @param ArrayOfD4WTPRECEIPTTICKETS $ACTTICKETDATA
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setACTTICKETDATA($ACTTICKETDATA)
    {
      $this->ACTTICKETDATA = $ACTTICKETDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISRECEIPTDUPLICATE()
    {
      return $this->BISRECEIPTDUPLICATE;
    }

    /**
     * @param float $BISRECEIPTDUPLICATE
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setBISRECEIPTDUPLICATE($BISRECEIPTDUPLICATE)
    {
      $this->BISRECEIPTDUPLICATE = $BISRECEIPTDUPLICATE;
      return $this;
    }

    /**
     * @return D4WTPRECEIPTPAYERDATA
     */
    public function getCTPAYERDATA()
    {
      return $this->CTPAYERDATA;
    }

    /**
     * @param D4WTPRECEIPTPAYERDATA $CTPAYERDATA
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setCTPAYERDATA($CTPAYERDATA)
    {
      $this->CTPAYERDATA = $CTPAYERDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNGROSSTARIFF()
    {
      return $this->NGROSSTARIFF;
    }

    /**
     * @param float $NGROSSTARIFF
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setNGROSSTARIFF($NGROSSTARIFF)
    {
      $this->NGROSSTARIFF = $NGROSSTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNETTARIFF()
    {
      return $this->NNETTARIFF;
    }

    /**
     * @param float $NNETTARIFF
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setNNETTARIFF($NNETTARIFF)
    {
      $this->NNETTARIFF = $NNETTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNF525RECEIPTNO()
    {
      return $this->NNF525RECEIPTNO;
    }

    /**
     * @param float $NNF525RECEIPTNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setNNF525RECEIPTNO($NNF525RECEIPTNO)
    {
      $this->NNF525RECEIPTNO = $NNF525RECEIPTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRECEIPTNO()
    {
      return $this->NRECEIPTNO;
    }

    /**
     * @param float $NRECEIPTNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setNRECEIPTNO($NRECEIPTNO)
    {
      $this->NRECEIPTNO = $NRECEIPTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNR()
    {
      return $this->NTRANSNR;
    }

    /**
     * @param float $NTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setNTRANSNR($NTRANSNR)
    {
      $this->NTRANSNR = $NTRANSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSSTATUS()
    {
      return $this->NTRANSSTATUS;
    }

    /**
     * @param float $NTRANSSTATUS
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setNTRANSSTATUS($NTRANSSTATUS)
    {
      $this->NTRANSSTATUS = $NTRANSSTATUS;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCREATIONTIME()
    {
      return $this->SZCREATIONTIME;
    }

    /**
     * @param string $SZCREATIONTIME
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setSZCREATIONTIME($SZCREATIONTIME)
    {
      $this->SZCREATIONTIME = $SZCREATIONTIME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCY()
    {
      return $this->SZCURRENCY;
    }

    /**
     * @param string $SZCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setSZCURRENCY($SZCURRENCY)
    {
      $this->SZCURRENCY = $SZCURRENCY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEXTINVOICENO()
    {
      return $this->SZEXTINVOICENO;
    }

    /**
     * @param string $SZEXTINVOICENO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setSZEXTINVOICENO($SZEXTINVOICENO)
    {
      $this->SZEXTINVOICENO = $SZEXTINVOICENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWSHOPRECEIPTNO()
    {
      return $this->SZWSHOPRECEIPTNO;
    }

    /**
     * @param string $SZWSHOPRECEIPTNO
     * @return \Axess\Dci4Wtp\D4WTPRECEIPTDATA
     */
    public function setSZWSHOPRECEIPTNO($SZWSHOPRECEIPTNO)
    {
      $this->SZWSHOPRECEIPTNO = $SZWSHOPRECEIPTNO;
      return $this;
    }

}
