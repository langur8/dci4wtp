<?php

namespace Axess\Dci4Wtp;

class delCreditCardReference
{

    /**
     * @var D4WTPDELCTCREFIDREQUEST $i_ctdelCTCRefIdReq
     */
    protected $i_ctdelCTCRefIdReq = null;

    /**
     * @param D4WTPDELCTCREFIDREQUEST $i_ctdelCTCRefIdReq
     */
    public function __construct($i_ctdelCTCRefIdReq)
    {
      $this->i_ctdelCTCRefIdReq = $i_ctdelCTCRefIdReq;
    }

    /**
     * @return D4WTPDELCTCREFIDREQUEST
     */
    public function getI_ctdelCTCRefIdReq()
    {
      return $this->i_ctdelCTCRefIdReq;
    }

    /**
     * @param D4WTPDELCTCREFIDREQUEST $i_ctdelCTCRefIdReq
     * @return \Axess\Dci4Wtp\delCreditCardReference
     */
    public function setI_ctdelCTCRefIdReq($i_ctdelCTCRefIdReq)
    {
      $this->i_ctdelCTCRefIdReq = $i_ctdelCTCRefIdReq;
      return $this;
    }

}
