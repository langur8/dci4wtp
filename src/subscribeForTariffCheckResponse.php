<?php

namespace Axess\Dci4Wtp;

class subscribeForTariffCheckResponse
{

    /**
     * @var D4WTPRESULT $subscribeForTariffCheckResult
     */
    protected $subscribeForTariffCheckResult = null;

    /**
     * @param D4WTPRESULT $subscribeForTariffCheckResult
     */
    public function __construct($subscribeForTariffCheckResult)
    {
      $this->subscribeForTariffCheckResult = $subscribeForTariffCheckResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSubscribeForTariffCheckResult()
    {
      return $this->subscribeForTariffCheckResult;
    }

    /**
     * @param D4WTPRESULT $subscribeForTariffCheckResult
     * @return \Axess\Dci4Wtp\subscribeForTariffCheckResponse
     */
    public function setSubscribeForTariffCheckResult($subscribeForTariffCheckResult)
    {
      $this->subscribeForTariffCheckResult = $subscribeForTariffCheckResult;
      return $this;
    }

}
