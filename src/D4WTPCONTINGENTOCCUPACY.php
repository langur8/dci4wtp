<?php

namespace Axess\Dci4Wtp;

class D4WTPCONTINGENTOCCUPACY
{

    /**
     * @var float $NFREECOUNT
     */
    protected $NFREECOUNT = null;

    /**
     * @var float $NRESERVEDCOUNT
     */
    protected $NRESERVEDCOUNT = null;

    /**
     * @var float $NRESERVEDPERPOSCOUNT
     */
    protected $NRESERVEDPERPOSCOUNT = null;

    /**
     * @var float $NSOLDCOUNT
     */
    protected $NSOLDCOUNT = null;

    /**
     * @var float $NTOTALCOUNT
     */
    protected $NTOTALCOUNT = null;

    /**
     * @var string $SZDATE
     */
    protected $SZDATE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNFREECOUNT()
    {
      return $this->NFREECOUNT;
    }

    /**
     * @param float $NFREECOUNT
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTOCCUPACY
     */
    public function setNFREECOUNT($NFREECOUNT)
    {
      $this->NFREECOUNT = $NFREECOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRESERVEDCOUNT()
    {
      return $this->NRESERVEDCOUNT;
    }

    /**
     * @param float $NRESERVEDCOUNT
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTOCCUPACY
     */
    public function setNRESERVEDCOUNT($NRESERVEDCOUNT)
    {
      $this->NRESERVEDCOUNT = $NRESERVEDCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNRESERVEDPERPOSCOUNT()
    {
      return $this->NRESERVEDPERPOSCOUNT;
    }

    /**
     * @param float $NRESERVEDPERPOSCOUNT
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTOCCUPACY
     */
    public function setNRESERVEDPERPOSCOUNT($NRESERVEDPERPOSCOUNT)
    {
      $this->NRESERVEDPERPOSCOUNT = $NRESERVEDPERPOSCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSOLDCOUNT()
    {
      return $this->NSOLDCOUNT;
    }

    /**
     * @param float $NSOLDCOUNT
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTOCCUPACY
     */
    public function setNSOLDCOUNT($NSOLDCOUNT)
    {
      $this->NSOLDCOUNT = $NSOLDCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTOTALCOUNT()
    {
      return $this->NTOTALCOUNT;
    }

    /**
     * @param float $NTOTALCOUNT
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTOCCUPACY
     */
    public function setNTOTALCOUNT($NTOTALCOUNT)
    {
      $this->NTOTALCOUNT = $NTOTALCOUNT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATE()
    {
      return $this->SZDATE;
    }

    /**
     * @param string $SZDATE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENTOCCUPACY
     */
    public function setSZDATE($SZDATE)
    {
      $this->SZDATE = $SZDATE;
      return $this;
    }

}
