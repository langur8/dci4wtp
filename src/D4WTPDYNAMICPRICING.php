<?php

namespace Axess\Dci4Wtp;

class D4WTPDYNAMICPRICING
{

    /**
     * @var ArrayOfD4WTPDISCOUNT $ACTDISCOUNT
     */
    protected $ACTDISCOUNT = null;

    /**
     * @var float $NDYNTARIFF
     */
    protected $NDYNTARIFF = null;

    /**
     * @var float $NDYNVATAMOUNT
     */
    protected $NDYNVATAMOUNT = null;

    /**
     * @var float $NDYNVATPERCENT
     */
    protected $NDYNVATPERCENT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPDISCOUNT
     */
    public function getACTDISCOUNT()
    {
      return $this->ACTDISCOUNT;
    }

    /**
     * @param ArrayOfD4WTPDISCOUNT $ACTDISCOUNT
     * @return \Axess\Dci4Wtp\D4WTPDYNAMICPRICING
     */
    public function setACTDISCOUNT($ACTDISCOUNT)
    {
      $this->ACTDISCOUNT = $ACTDISCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDYNTARIFF()
    {
      return $this->NDYNTARIFF;
    }

    /**
     * @param float $NDYNTARIFF
     * @return \Axess\Dci4Wtp\D4WTPDYNAMICPRICING
     */
    public function setNDYNTARIFF($NDYNTARIFF)
    {
      $this->NDYNTARIFF = $NDYNTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDYNVATAMOUNT()
    {
      return $this->NDYNVATAMOUNT;
    }

    /**
     * @param float $NDYNVATAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPDYNAMICPRICING
     */
    public function setNDYNVATAMOUNT($NDYNVATAMOUNT)
    {
      $this->NDYNVATAMOUNT = $NDYNVATAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDYNVATPERCENT()
    {
      return $this->NDYNVATPERCENT;
    }

    /**
     * @param float $NDYNVATPERCENT
     * @return \Axess\Dci4Wtp\D4WTPDYNAMICPRICING
     */
    public function setNDYNVATPERCENT($NDYNVATPERCENT)
    {
      $this->NDYNVATPERCENT = $NDYNVATPERCENT;
      return $this;
    }

}
