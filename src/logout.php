<?php

namespace Axess\Dci4Wtp;

class logout
{

    /**
     * @var D4WTPLOGOUTREQUEST $i_ctLogoutReq
     */
    protected $i_ctLogoutReq = null;

    /**
     * @param D4WTPLOGOUTREQUEST $i_ctLogoutReq
     */
    public function __construct($i_ctLogoutReq)
    {
      $this->i_ctLogoutReq = $i_ctLogoutReq;
    }

    /**
     * @return D4WTPLOGOUTREQUEST
     */
    public function getI_ctLogoutReq()
    {
      return $this->i_ctLogoutReq;
    }

    /**
     * @param D4WTPLOGOUTREQUEST $i_ctLogoutReq
     * @return \Axess\Dci4Wtp\logout
     */
    public function setI_ctLogoutReq($i_ctLogoutReq)
    {
      $this->i_ctLogoutReq = $i_ctLogoutReq;
      return $this;
    }

}
