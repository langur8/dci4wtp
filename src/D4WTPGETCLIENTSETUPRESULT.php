<?php

namespace Axess\Dci4Wtp;

class D4WTPGETCLIENTSETUPRESULT
{

    /**
     * @var ArrayOfD4WTPSETUPITEM $ACTSETUPITEM
     */
    protected $ACTSETUPITEM = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSETUPITEM
     */
    public function getACTSETUPITEM()
    {
      return $this->ACTSETUPITEM;
    }

    /**
     * @param ArrayOfD4WTPSETUPITEM $ACTSETUPITEM
     * @return \Axess\Dci4Wtp\D4WTPGETCLIENTSETUPRESULT
     */
    public function setACTSETUPITEM($ACTSETUPITEM)
    {
      $this->ACTSETUPITEM = $ACTSETUPITEM;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETCLIENTSETUPRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETCLIENTSETUPRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
