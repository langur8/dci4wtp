<?php

namespace Axess\Dci4Wtp;

class D4WTPGETGRPCONTINGENTRESULT
{

    /**
     * @var ArrayOfD4WTPCONTNGNTTICKET3 $ACTCONTNGNTTICKET3
     */
    protected $ACTCONTNGNTTICKET3 = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPCONTNGNTTICKET3
     */
    public function getACTCONTNGNTTICKET3()
    {
      return $this->ACTCONTNGNTTICKET3;
    }

    /**
     * @param ArrayOfD4WTPCONTNGNTTICKET3 $ACTCONTNGNTTICKET3
     * @return \Axess\Dci4Wtp\D4WTPGETGRPCONTINGENTRESULT
     */
    public function setACTCONTNGNTTICKET3($ACTCONTNGNTTICKET3)
    {
      $this->ACTCONTNGNTTICKET3 = $ACTCONTNGNTTICKET3;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETGRPCONTINGENTRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETGRPCONTINGENTRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
