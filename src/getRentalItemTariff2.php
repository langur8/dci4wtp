<?php

namespace Axess\Dci4Wtp;

class getRentalItemTariff2
{

    /**
     * @var D4WTPGETRENTALITEMTARIFF2REQ $i_ctGetRentalItemTariffReq
     */
    protected $i_ctGetRentalItemTariffReq = null;

    /**
     * @param D4WTPGETRENTALITEMTARIFF2REQ $i_ctGetRentalItemTariffReq
     */
    public function __construct($i_ctGetRentalItemTariffReq)
    {
      $this->i_ctGetRentalItemTariffReq = $i_ctGetRentalItemTariffReq;
    }

    /**
     * @return D4WTPGETRENTALITEMTARIFF2REQ
     */
    public function getI_ctGetRentalItemTariffReq()
    {
      return $this->i_ctGetRentalItemTariffReq;
    }

    /**
     * @param D4WTPGETRENTALITEMTARIFF2REQ $i_ctGetRentalItemTariffReq
     * @return \Axess\Dci4Wtp\getRentalItemTariff2
     */
    public function setI_ctGetRentalItemTariffReq($i_ctGetRentalItemTariffReq)
    {
      $this->i_ctGetRentalItemTariffReq = $i_ctGetRentalItemTariffReq;
      return $this;
    }

}
