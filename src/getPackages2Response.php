<?php

namespace Axess\Dci4Wtp;

class getPackages2Response
{

    /**
     * @var D4WTPPACKAGE2RESULT $getPackages2Result
     */
    protected $getPackages2Result = null;

    /**
     * @param D4WTPPACKAGE2RESULT $getPackages2Result
     */
    public function __construct($getPackages2Result)
    {
      $this->getPackages2Result = $getPackages2Result;
    }

    /**
     * @return D4WTPPACKAGE2RESULT
     */
    public function getGetPackages2Result()
    {
      return $this->getPackages2Result;
    }

    /**
     * @param D4WTPPACKAGE2RESULT $getPackages2Result
     * @return \Axess\Dci4Wtp\getPackages2Response
     */
    public function setGetPackages2Result($getPackages2Result)
    {
      $this->getPackages2Result = $getPackages2Result;
      return $this;
    }

}
