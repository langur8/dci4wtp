<?php

namespace Axess\Dci4Wtp;

class eMoneyBalanceHistoryResponse
{

    /**
     * @var D4WTPHISTORYRES $eMoneyBalanceHistoryResult
     */
    protected $eMoneyBalanceHistoryResult = null;

    /**
     * @param D4WTPHISTORYRES $eMoneyBalanceHistoryResult
     */
    public function __construct($eMoneyBalanceHistoryResult)
    {
      $this->eMoneyBalanceHistoryResult = $eMoneyBalanceHistoryResult;
    }

    /**
     * @return D4WTPHISTORYRES
     */
    public function getEMoneyBalanceHistoryResult()
    {
      return $this->eMoneyBalanceHistoryResult;
    }

    /**
     * @param D4WTPHISTORYRES $eMoneyBalanceHistoryResult
     * @return \Axess\Dci4Wtp\eMoneyBalanceHistoryResponse
     */
    public function setEMoneyBalanceHistoryResult($eMoneyBalanceHistoryResult)
    {
      $this->eMoneyBalanceHistoryResult = $eMoneyBalanceHistoryResult;
      return $this;
    }

}
