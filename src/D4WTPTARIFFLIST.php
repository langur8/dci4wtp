<?php

namespace Axess\Dci4Wtp;

class D4WTPTARIFFLIST
{

    /**
     * @var ArrayOfD4WTPTARIFFLISTDAY $ACTTARIFFLISTDAY
     */
    protected $ACTTARIFFLISTDAY = null;

    /**
     * @var string $SZVALIDFROM
     */
    protected $SZVALIDFROM = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTARIFFLISTDAY
     */
    public function getACTTARIFFLISTDAY()
    {
      return $this->ACTTARIFFLISTDAY;
    }

    /**
     * @param ArrayOfD4WTPTARIFFLISTDAY $ACTTARIFFLISTDAY
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST
     */
    public function setACTTARIFFLISTDAY($ACTTARIFFLISTDAY)
    {
      $this->ACTTARIFFLISTDAY = $ACTTARIFFLISTDAY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZVALIDFROM()
    {
      return $this->SZVALIDFROM;
    }

    /**
     * @param string $SZVALIDFROM
     * @return \Axess\Dci4Wtp\D4WTPTARIFFLIST
     */
    public function setSZVALIDFROM($SZVALIDFROM)
    {
      $this->SZVALIDFROM = $SZVALIDFROM;
      return $this;
    }

}
