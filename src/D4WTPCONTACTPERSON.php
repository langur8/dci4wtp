<?php

namespace Axess\Dci4Wtp;

class D4WTPCONTACTPERSON
{

    /**
     * @var float $NSORTNO
     */
    protected $NSORTNO = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZMOBILENO
     */
    protected $SZMOBILENO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNSORTNO()
    {
      return $this->NSORTNO;
    }

    /**
     * @param float $NSORTNO
     * @return \Axess\Dci4Wtp\D4WTPCONTACTPERSON
     */
    public function setNSORTNO($NSORTNO)
    {
      $this->NSORTNO = $NSORTNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\D4WTPCONTACTPERSON
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPCONTACTPERSON
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPCONTACTPERSON
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMOBILENO()
    {
      return $this->SZMOBILENO;
    }

    /**
     * @param string $SZMOBILENO
     * @return \Axess\Dci4Wtp\D4WTPCONTACTPERSON
     */
    public function setSZMOBILENO($SZMOBILENO)
    {
      $this->SZMOBILENO = $SZMOBILENO;
      return $this;
    }

}
