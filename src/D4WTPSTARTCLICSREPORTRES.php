<?php

namespace Axess\Dci4Wtp;

class D4WTPSTARTCLICSREPORTRES
{

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NWTPREPORTNR
     */
    protected $NWTPREPORTNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPSTARTCLICSREPORTRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPREPORTNR()
    {
      return $this->NWTPREPORTNR;
    }

    /**
     * @param float $NWTPREPORTNR
     * @return \Axess\Dci4Wtp\D4WTPSTARTCLICSREPORTRES
     */
    public function setNWTPREPORTNR($NWTPREPORTNR)
    {
      $this->NWTPREPORTNR = $NWTPREPORTNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPSTARTCLICSREPORTRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
