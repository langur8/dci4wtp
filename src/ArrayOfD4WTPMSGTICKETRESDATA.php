<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPMSGTICKETRESDATA implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPMSGTICKETRESDATA[] $D4WTPMSGTICKETRESDATA
     */
    protected $D4WTPMSGTICKETRESDATA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPMSGTICKETRESDATA[]
     */
    public function getD4WTPMSGTICKETRESDATA()
    {
      return $this->D4WTPMSGTICKETRESDATA;
    }

    /**
     * @param D4WTPMSGTICKETRESDATA[] $D4WTPMSGTICKETRESDATA
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPMSGTICKETRESDATA
     */
    public function setD4WTPMSGTICKETRESDATA(array $D4WTPMSGTICKETRESDATA = null)
    {
      $this->D4WTPMSGTICKETRESDATA = $D4WTPMSGTICKETRESDATA;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPMSGTICKETRESDATA[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPMSGTICKETRESDATA
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPMSGTICKETRESDATA[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPMSGTICKETRESDATA $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPMSGTICKETRESDATA[] = $value;
      } else {
        $this->D4WTPMSGTICKETRESDATA[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPMSGTICKETRESDATA[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPMSGTICKETRESDATA Return the current element
     */
    public function current()
    {
      return current($this->D4WTPMSGTICKETRESDATA);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPMSGTICKETRESDATA);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPMSGTICKETRESDATA);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPMSGTICKETRESDATA);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPMSGTICKETRESDATA Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPMSGTICKETRESDATA);
    }

}
