<?php

namespace Axess\Dci4Wtp;

class changePersData2Response
{

    /**
     * @var D4WTPCHANGEPERSDATA2RESULT $changePersData2Result
     */
    protected $changePersData2Result = null;

    /**
     * @param D4WTPCHANGEPERSDATA2RESULT $changePersData2Result
     */
    public function __construct($changePersData2Result)
    {
      $this->changePersData2Result = $changePersData2Result;
    }

    /**
     * @return D4WTPCHANGEPERSDATA2RESULT
     */
    public function getChangePersData2Result()
    {
      return $this->changePersData2Result;
    }

    /**
     * @param D4WTPCHANGEPERSDATA2RESULT $changePersData2Result
     * @return \Axess\Dci4Wtp\changePersData2Response
     */
    public function setChangePersData2Result($changePersData2Result)
    {
      $this->changePersData2Result = $changePersData2Result;
      return $this;
    }

}
