<?php

namespace Axess\Dci4Wtp;

class setCompensationTicket
{

    /**
     * @var D4WTPCOMPENSATIONTICKETREQ $i_ctrequest
     */
    protected $i_ctrequest = null;

    /**
     * @param D4WTPCOMPENSATIONTICKETREQ $i_ctrequest
     */
    public function __construct($i_ctrequest)
    {
      $this->i_ctrequest = $i_ctrequest;
    }

    /**
     * @return D4WTPCOMPENSATIONTICKETREQ
     */
    public function getI_ctrequest()
    {
      return $this->i_ctrequest;
    }

    /**
     * @param D4WTPCOMPENSATIONTICKETREQ $i_ctrequest
     * @return \Axess\Dci4Wtp\setCompensationTicket
     */
    public function setI_ctrequest($i_ctrequest)
    {
      $this->i_ctrequest = $i_ctrequest;
      return $this;
    }

}
