<?php

namespace Axess\Dci4Wtp;

class issueTicket2Response
{

    /**
     * @var D4WTPISSUETICKET2RESULT $issueTicket2Result
     */
    protected $issueTicket2Result = null;

    /**
     * @param D4WTPISSUETICKET2RESULT $issueTicket2Result
     */
    public function __construct($issueTicket2Result)
    {
      $this->issueTicket2Result = $issueTicket2Result;
    }

    /**
     * @return D4WTPISSUETICKET2RESULT
     */
    public function getIssueTicket2Result()
    {
      return $this->issueTicket2Result;
    }

    /**
     * @param D4WTPISSUETICKET2RESULT $issueTicket2Result
     * @return \Axess\Dci4Wtp\issueTicket2Response
     */
    public function setIssueTicket2Result($issueTicket2Result)
    {
      $this->issueTicket2Result = $issueTicket2Result;
      return $this;
    }

}
