<?php

namespace Axess\Dci4Wtp;

class D4WTPCHIPCARD
{

    /**
     * @var float $NCHIPKARTENREGID
     */
    protected $NCHIPKARTENREGID = null;

    /**
     * @var float $NCHIPPROJNR
     */
    protected $NCHIPPROJNR = null;

    /**
     * @var float $NCHIPTYPENR
     */
    protected $NCHIPTYPENR = null;

    /**
     * @var float $NDATACARRIERNR
     */
    protected $NDATACARRIERNR = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var float $NPACKAGETYPENR
     */
    protected $NPACKAGETYPENR = null;

    /**
     * @var string $SZCARDNR
     */
    protected $SZCARDNR = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    /**
     * @var string $SZPOEFIRSTUSAGEDATE
     */
    protected $SZPOEFIRSTUSAGEDATE = null;

    /**
     * @var string $SZPOELASTUSAGEDATE
     */
    protected $SZPOELASTUSAGEDATE = null;

    /**
     * @var string $SZPOSFIRSTUSAGEDATE
     */
    protected $SZPOSFIRSTUSAGEDATE = null;

    /**
     * @var string $SZPOSLASTUSAGEDATE
     */
    protected $SZPOSLASTUSAGEDATE = null;

    /**
     * @var string $SZWTPNUMBER64BIT
     */
    protected $SZWTPNUMBER64BIT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCHIPKARTENREGID()
    {
      return $this->NCHIPKARTENREGID;
    }

    /**
     * @param float $NCHIPKARTENREGID
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setNCHIPKARTENREGID($NCHIPKARTENREGID)
    {
      $this->NCHIPKARTENREGID = $NCHIPKARTENREGID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCHIPPROJNR()
    {
      return $this->NCHIPPROJNR;
    }

    /**
     * @param float $NCHIPPROJNR
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setNCHIPPROJNR($NCHIPPROJNR)
    {
      $this->NCHIPPROJNR = $NCHIPPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCHIPTYPENR()
    {
      return $this->NCHIPTYPENR;
    }

    /**
     * @param float $NCHIPTYPENR
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setNCHIPTYPENR($NCHIPTYPENR)
    {
      $this->NCHIPTYPENR = $NCHIPTYPENR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATACARRIERNR()
    {
      return $this->NDATACARRIERNR;
    }

    /**
     * @param float $NDATACARRIERNR
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setNDATACARRIERNR($NDATACARRIERNR)
    {
      $this->NDATACARRIERNR = $NDATACARRIERNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPACKAGETYPENR()
    {
      return $this->NPACKAGETYPENR;
    }

    /**
     * @param float $NPACKAGETYPENR
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setNPACKAGETYPENR($NPACKAGETYPENR)
    {
      $this->NPACKAGETYPENR = $NPACKAGETYPENR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCARDNR()
    {
      return $this->SZCARDNR;
    }

    /**
     * @param string $SZCARDNR
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setSZCARDNR($SZCARDNR)
    {
      $this->SZCARDNR = $SZCARDNR;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOEFIRSTUSAGEDATE()
    {
      return $this->SZPOEFIRSTUSAGEDATE;
    }

    /**
     * @param string $SZPOEFIRSTUSAGEDATE
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setSZPOEFIRSTUSAGEDATE($SZPOEFIRSTUSAGEDATE)
    {
      $this->SZPOEFIRSTUSAGEDATE = $SZPOEFIRSTUSAGEDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOELASTUSAGEDATE()
    {
      return $this->SZPOELASTUSAGEDATE;
    }

    /**
     * @param string $SZPOELASTUSAGEDATE
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setSZPOELASTUSAGEDATE($SZPOELASTUSAGEDATE)
    {
      $this->SZPOELASTUSAGEDATE = $SZPOELASTUSAGEDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOSFIRSTUSAGEDATE()
    {
      return $this->SZPOSFIRSTUSAGEDATE;
    }

    /**
     * @param string $SZPOSFIRSTUSAGEDATE
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setSZPOSFIRSTUSAGEDATE($SZPOSFIRSTUSAGEDATE)
    {
      $this->SZPOSFIRSTUSAGEDATE = $SZPOSFIRSTUSAGEDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPOSLASTUSAGEDATE()
    {
      return $this->SZPOSLASTUSAGEDATE;
    }

    /**
     * @param string $SZPOSLASTUSAGEDATE
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setSZPOSLASTUSAGEDATE($SZPOSLASTUSAGEDATE)
    {
      $this->SZPOSLASTUSAGEDATE = $SZPOSLASTUSAGEDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNUMBER64BIT()
    {
      return $this->SZWTPNUMBER64BIT;
    }

    /**
     * @param string $SZWTPNUMBER64BIT
     * @return \Axess\Dci4Wtp\D4WTPCHIPCARD
     */
    public function setSZWTPNUMBER64BIT($SZWTPNUMBER64BIT)
    {
      $this->SZWTPNUMBER64BIT = $SZWTPNUMBER64BIT;
      return $this;
    }

}
