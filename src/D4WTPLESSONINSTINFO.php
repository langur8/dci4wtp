<?php

namespace Axess\Dci4Wtp;

class D4WTPLESSONINSTINFO
{

    /**
     * @var float $BHASINSURANCE
     */
    protected $BHASINSURANCE = null;

    /**
     * @var float $BISACTIVE
     */
    protected $BISACTIVE = null;

    /**
     * @var \DateTime $DTDATEOFBIRTH
     */
    protected $DTDATEOFBIRTH = null;

    /**
     * @var float $NHOMELOCARCNR
     */
    protected $NHOMELOCARCNR = null;

    /**
     * @var float $NHOMELOCATIONNR
     */
    protected $NHOMELOCATIONNR = null;

    /**
     * @var float $NHOMELOCPROJNR
     */
    protected $NHOMELOCPROJNR = null;

    /**
     * @var float $NINSTRUCTORNR
     */
    protected $NINSTRUCTORNR = null;

    /**
     * @var float $NNRKREISART
     */
    protected $NNRKREISART = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    /**
     * @var string $SZEMAIL
     */
    protected $SZEMAIL = null;

    /**
     * @var string $SZGENDER
     */
    protected $SZGENDER = null;

    /**
     * @var string $SZINFORMATION
     */
    protected $SZINFORMATION = null;

    /**
     * @var string $SZMOBILE
     */
    protected $SZMOBILE = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZPHONE
     */
    protected $SZPHONE = null;

    /**
     * @var string $SZPICTUREPATH
     */
    protected $SZPICTUREPATH = null;

    /**
     * @var string $SZSURNAME
     */
    protected $SZSURNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBHASINSURANCE()
    {
      return $this->BHASINSURANCE;
    }

    /**
     * @param float $BHASINSURANCE
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setBHASINSURANCE($BHASINSURANCE)
    {
      $this->BHASINSURANCE = $BHASINSURANCE;
      return $this;
    }

    /**
     * @return float
     */
    public function getBISACTIVE()
    {
      return $this->BISACTIVE;
    }

    /**
     * @param float $BISACTIVE
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setBISACTIVE($BISACTIVE)
    {
      $this->BISACTIVE = $BISACTIVE;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDTDATEOFBIRTH()
    {
      if ($this->DTDATEOFBIRTH == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DTDATEOFBIRTH);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DTDATEOFBIRTH
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setDTDATEOFBIRTH(\DateTime $DTDATEOFBIRTH = null)
    {
      if ($DTDATEOFBIRTH == null) {
       $this->DTDATEOFBIRTH = null;
      } else {
        $this->DTDATEOFBIRTH = $DTDATEOFBIRTH->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return float
     */
    public function getNHOMELOCARCNR()
    {
      return $this->NHOMELOCARCNR;
    }

    /**
     * @param float $NHOMELOCARCNR
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setNHOMELOCARCNR($NHOMELOCARCNR)
    {
      $this->NHOMELOCARCNR = $NHOMELOCARCNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNHOMELOCATIONNR()
    {
      return $this->NHOMELOCATIONNR;
    }

    /**
     * @param float $NHOMELOCATIONNR
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setNHOMELOCATIONNR($NHOMELOCATIONNR)
    {
      $this->NHOMELOCATIONNR = $NHOMELOCATIONNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNHOMELOCPROJNR()
    {
      return $this->NHOMELOCPROJNR;
    }

    /**
     * @param float $NHOMELOCPROJNR
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setNHOMELOCPROJNR($NHOMELOCPROJNR)
    {
      $this->NHOMELOCPROJNR = $NHOMELOCPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNINSTRUCTORNR()
    {
      return $this->NINSTRUCTORNR;
    }

    /**
     * @param float $NINSTRUCTORNR
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setNINSTRUCTORNR($NINSTRUCTORNR)
    {
      $this->NINSTRUCTORNR = $NINSTRUCTORNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNNRKREISART()
    {
      return $this->NNRKREISART;
    }

    /**
     * @param float $NNRKREISART
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setNNRKREISART($NNRKREISART)
    {
      $this->NNRKREISART = $NNRKREISART;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZEMAIL()
    {
      return $this->SZEMAIL;
    }

    /**
     * @param string $SZEMAIL
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setSZEMAIL($SZEMAIL)
    {
      $this->SZEMAIL = $SZEMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGENDER()
    {
      return $this->SZGENDER;
    }

    /**
     * @param string $SZGENDER
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setSZGENDER($SZGENDER)
    {
      $this->SZGENDER = $SZGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZINFORMATION()
    {
      return $this->SZINFORMATION;
    }

    /**
     * @param string $SZINFORMATION
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setSZINFORMATION($SZINFORMATION)
    {
      $this->SZINFORMATION = $SZINFORMATION;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMOBILE()
    {
      return $this->SZMOBILE;
    }

    /**
     * @param string $SZMOBILE
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setSZMOBILE($SZMOBILE)
    {
      $this->SZMOBILE = $SZMOBILE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPHONE()
    {
      return $this->SZPHONE;
    }

    /**
     * @param string $SZPHONE
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setSZPHONE($SZPHONE)
    {
      $this->SZPHONE = $SZPHONE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPICTUREPATH()
    {
      return $this->SZPICTUREPATH;
    }

    /**
     * @param string $SZPICTUREPATH
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setSZPICTUREPATH($SZPICTUREPATH)
    {
      $this->SZPICTUREPATH = $SZPICTUREPATH;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSURNAME()
    {
      return $this->SZSURNAME;
    }

    /**
     * @param string $SZSURNAME
     * @return \Axess\Dci4Wtp\D4WTPLESSONINSTINFO
     */
    public function setSZSURNAME($SZSURNAME)
    {
      $this->SZSURNAME = $SZSURNAME;
      return $this;
    }

}
