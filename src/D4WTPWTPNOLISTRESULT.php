<?php

namespace Axess\Dci4Wtp;

class D4WTPWTPNOLISTRESULT
{

    /**
     * @var ArrayOfD4WTPWTPNOLIST $ACTWTPNOLIST
     */
    protected $ACTWTPNOLIST = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPWTPNOLIST
     */
    public function getACTWTPNOLIST()
    {
      return $this->ACTWTPNOLIST;
    }

    /**
     * @param ArrayOfD4WTPWTPNOLIST $ACTWTPNOLIST
     * @return \Axess\Dci4Wtp\D4WTPWTPNOLISTRESULT
     */
    public function setACTWTPNOLIST($ACTWTPNOLIST)
    {
      $this->ACTWTPNOLIST = $ACTWTPNOLIST;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPWTPNOLISTRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPWTPNOLISTRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
