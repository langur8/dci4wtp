<?php

namespace Axess\Dci4Wtp;

class forgotPasswordResponse
{

    /**
     * @var D4WTPFORGOTPASSRES $forgotPasswordResult
     */
    protected $forgotPasswordResult = null;

    /**
     * @param D4WTPFORGOTPASSRES $forgotPasswordResult
     */
    public function __construct($forgotPasswordResult)
    {
      $this->forgotPasswordResult = $forgotPasswordResult;
    }

    /**
     * @return D4WTPFORGOTPASSRES
     */
    public function getForgotPasswordResult()
    {
      return $this->forgotPasswordResult;
    }

    /**
     * @param D4WTPFORGOTPASSRES $forgotPasswordResult
     * @return \Axess\Dci4Wtp\forgotPasswordResponse
     */
    public function setForgotPasswordResult($forgotPasswordResult)
    {
      $this->forgotPasswordResult = $forgotPasswordResult;
      return $this;
    }

}
