<?php

namespace Axess\Dci4Wtp;

class D4WTPADDDTL4FAMILYMEMBERRES
{

    /**
     * @var ArrayOfD4WTPMEDIAADDED $ACTMEDIAADDED
     */
    protected $ACTMEDIAADDED = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPMEDIAADDED
     */
    public function getACTMEDIAADDED()
    {
      return $this->ACTMEDIAADDED;
    }

    /**
     * @param ArrayOfD4WTPMEDIAADDED $ACTMEDIAADDED
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYMEMBERRES
     */
    public function setACTMEDIAADDED($ACTMEDIAADDED)
    {
      $this->ACTMEDIAADDED = $ACTMEDIAADDED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYMEMBERRES
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPADDDTL4FAMILYMEMBERRES
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
