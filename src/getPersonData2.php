<?php

namespace Axess\Dci4Wtp;

class getPersonData2
{

    /**
     * @var D4WTPGETPERSON2REQUEST $i_ctPersData2Req
     */
    protected $i_ctPersData2Req = null;

    /**
     * @param D4WTPGETPERSON2REQUEST $i_ctPersData2Req
     */
    public function __construct($i_ctPersData2Req)
    {
      $this->i_ctPersData2Req = $i_ctPersData2Req;
    }

    /**
     * @return D4WTPGETPERSON2REQUEST
     */
    public function getI_ctPersData2Req()
    {
      return $this->i_ctPersData2Req;
    }

    /**
     * @param D4WTPGETPERSON2REQUEST $i_ctPersData2Req
     * @return \Axess\Dci4Wtp\getPersonData2
     */
    public function setI_ctPersData2Req($i_ctPersData2Req)
    {
      $this->i_ctPersData2Req = $i_ctPersData2Req;
      return $this;
    }

}
