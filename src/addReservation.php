<?php

namespace Axess\Dci4Wtp;

class addReservation
{

    /**
     * @var D4WTPADDRESERVATIONREQUEST $i_ctAddReservationReq
     */
    protected $i_ctAddReservationReq = null;

    /**
     * @param D4WTPADDRESERVATIONREQUEST $i_ctAddReservationReq
     */
    public function __construct($i_ctAddReservationReq)
    {
      $this->i_ctAddReservationReq = $i_ctAddReservationReq;
    }

    /**
     * @return D4WTPADDRESERVATIONREQUEST
     */
    public function getI_ctAddReservationReq()
    {
      return $this->i_ctAddReservationReq;
    }

    /**
     * @param D4WTPADDRESERVATIONREQUEST $i_ctAddReservationReq
     * @return \Axess\Dci4Wtp\addReservation
     */
    public function setI_ctAddReservationReq($i_ctAddReservationReq)
    {
      $this->i_ctAddReservationReq = $i_ctAddReservationReq;
      return $this;
    }

}
