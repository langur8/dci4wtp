<?php

namespace Axess\Dci4Wtp;

class getWTPRentalItems2Response
{

    /**
     * @var D4WTPRENTALITEMRESULT2 $getWTPRentalItems2Result
     */
    protected $getWTPRentalItems2Result = null;

    /**
     * @param D4WTPRENTALITEMRESULT2 $getWTPRentalItems2Result
     */
    public function __construct($getWTPRentalItems2Result)
    {
      $this->getWTPRentalItems2Result = $getWTPRentalItems2Result;
    }

    /**
     * @return D4WTPRENTALITEMRESULT2
     */
    public function getGetWTPRentalItems2Result()
    {
      return $this->getWTPRentalItems2Result;
    }

    /**
     * @param D4WTPRENTALITEMRESULT2 $getWTPRentalItems2Result
     * @return \Axess\Dci4Wtp\getWTPRentalItems2Response
     */
    public function setGetWTPRentalItems2Result($getWTPRentalItems2Result)
    {
      $this->getWTPRentalItems2Result = $getWTPRentalItems2Result;
      return $this;
    }

}
