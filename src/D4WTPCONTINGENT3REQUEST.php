<?php

namespace Axess\Dci4Wtp;

class D4WTPCONTINGENT3REQUEST
{

    /**
     * @var float $NCONTINGENTNO
     */
    protected $NCONTINGENTNO = null;

    /**
     * @var float $NPROJNO
     */
    protected $NPROJNO = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NSUBCONTINGENTNO
     */
    protected $NSUBCONTINGENTNO = null;

    /**
     * @var string $SZDAY
     */
    protected $SZDAY = null;

    /**
     * @var string $SZRESERVATIONREFERENCE
     */
    protected $SZRESERVATIONREFERENCE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCONTINGENTNO()
    {
      return $this->NCONTINGENTNO;
    }

    /**
     * @param float $NCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT3REQUEST
     */
    public function setNCONTINGENTNO($NCONTINGENTNO)
    {
      $this->NCONTINGENTNO = $NCONTINGENTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNO()
    {
      return $this->NPROJNO;
    }

    /**
     * @param float $NPROJNO
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT3REQUEST
     */
    public function setNPROJNO($NPROJNO)
    {
      $this->NPROJNO = $NPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT3REQUEST
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUBCONTINGENTNO()
    {
      return $this->NSUBCONTINGENTNO;
    }

    /**
     * @param float $NSUBCONTINGENTNO
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT3REQUEST
     */
    public function setNSUBCONTINGENTNO($NSUBCONTINGENTNO)
    {
      $this->NSUBCONTINGENTNO = $NSUBCONTINGENTNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDAY()
    {
      return $this->SZDAY;
    }

    /**
     * @param string $SZDAY
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT3REQUEST
     */
    public function setSZDAY($SZDAY)
    {
      $this->SZDAY = $SZDAY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZRESERVATIONREFERENCE()
    {
      return $this->SZRESERVATIONREFERENCE;
    }

    /**
     * @param string $SZRESERVATIONREFERENCE
     * @return \Axess\Dci4Wtp\D4WTPCONTINGENT3REQUEST
     */
    public function setSZRESERVATIONREFERENCE($SZRESERVATIONREFERENCE)
    {
      $this->SZRESERVATIONREFERENCE = $SZRESERVATIONREFERENCE;
      return $this;
    }

}
