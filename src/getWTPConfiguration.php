<?php

namespace Axess\Dci4Wtp;

class getWTPConfiguration
{

    /**
     * @var D4WTPCONFIGREQUEST $i_ctConfigReq
     */
    protected $i_ctConfigReq = null;

    /**
     * @param D4WTPCONFIGREQUEST $i_ctConfigReq
     */
    public function __construct($i_ctConfigReq)
    {
      $this->i_ctConfigReq = $i_ctConfigReq;
    }

    /**
     * @return D4WTPCONFIGREQUEST
     */
    public function getI_ctConfigReq()
    {
      return $this->i_ctConfigReq;
    }

    /**
     * @param D4WTPCONFIGREQUEST $i_ctConfigReq
     * @return \Axess\Dci4Wtp\getWTPConfiguration
     */
    public function setI_ctConfigReq($i_ctConfigReq)
    {
      $this->i_ctConfigReq = $i_ctConfigReq;
      return $this;
    }

}
