<?php

namespace Axess\Dci4Wtp;

class setClientSetupResponse
{

    /**
     * @var D4WTPRESULT $setClientSetupResult
     */
    protected $setClientSetupResult = null;

    /**
     * @param D4WTPRESULT $setClientSetupResult
     */
    public function __construct($setClientSetupResult)
    {
      $this->setClientSetupResult = $setClientSetupResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSetClientSetupResult()
    {
      return $this->setClientSetupResult;
    }

    /**
     * @param D4WTPRESULT $setClientSetupResult
     * @return \Axess\Dci4Wtp\setClientSetupResponse
     */
    public function setSetClientSetupResult($setClientSetupResult)
    {
      $this->setClientSetupResult = $setClientSetupResult;
      return $this;
    }

}
