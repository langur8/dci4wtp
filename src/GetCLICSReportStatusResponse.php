<?php

namespace Axess\Dci4Wtp;

class GetCLICSReportStatusResponse
{

    /**
     * @var D4WTPCLICSREPORTSTATUSRES $GetCLICSReportStatusResult
     */
    protected $GetCLICSReportStatusResult = null;

    /**
     * @param D4WTPCLICSREPORTSTATUSRES $GetCLICSReportStatusResult
     */
    public function __construct($GetCLICSReportStatusResult)
    {
      $this->GetCLICSReportStatusResult = $GetCLICSReportStatusResult;
    }

    /**
     * @return D4WTPCLICSREPORTSTATUSRES
     */
    public function getGetCLICSReportStatusResult()
    {
      return $this->GetCLICSReportStatusResult;
    }

    /**
     * @param D4WTPCLICSREPORTSTATUSRES $GetCLICSReportStatusResult
     * @return \Axess\Dci4Wtp\GetCLICSReportStatusResponse
     */
    public function setGetCLICSReportStatusResult($GetCLICSReportStatusResult)
    {
      $this->GetCLICSReportStatusResult = $GetCLICSReportStatusResult;
      return $this;
    }

}
