<?php

namespace Axess\Dci4Wtp;

class D4WTPARTICLE4
{

    /**
     * @var float $NARTICLENO
     */
    protected $NARTICLENO = null;

    /**
     * @var float $NARTICLETYPENO
     */
    protected $NARTICLETYPENO = null;

    /**
     * @var float $NTARIFF
     */
    protected $NTARIFF = null;

    /**
     * @var float $NVATAMOUNT
     */
    protected $NVATAMOUNT = null;

    /**
     * @var float $NVATPERCENT
     */
    protected $NVATPERCENT = null;

    /**
     * @var string $SZARTICLETYPENAME
     */
    protected $SZARTICLETYPENAME = null;

    /**
     * @var string $SZCURRENCY
     */
    protected $SZCURRENCY = null;

    /**
     * @var string $SZMASKNAME
     */
    protected $SZMASKNAME = null;

    /**
     * @var string $SZMASKSHORTNAME
     */
    protected $SZMASKSHORTNAME = null;

    /**
     * @var string $SZNAME
     */
    protected $SZNAME = null;

    /**
     * @var string $SZSHORTNAME
     */
    protected $SZSHORTNAME = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNARTICLENO()
    {
      return $this->NARTICLENO;
    }

    /**
     * @param float $NARTICLENO
     * @return \Axess\Dci4Wtp\D4WTPARTICLE4
     */
    public function setNARTICLENO($NARTICLENO)
    {
      $this->NARTICLENO = $NARTICLENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNARTICLETYPENO()
    {
      return $this->NARTICLETYPENO;
    }

    /**
     * @param float $NARTICLETYPENO
     * @return \Axess\Dci4Wtp\D4WTPARTICLE4
     */
    public function setNARTICLETYPENO($NARTICLETYPENO)
    {
      $this->NARTICLETYPENO = $NARTICLETYPENO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTARIFF()
    {
      return $this->NTARIFF;
    }

    /**
     * @param float $NTARIFF
     * @return \Axess\Dci4Wtp\D4WTPARTICLE4
     */
    public function setNTARIFF($NTARIFF)
    {
      $this->NTARIFF = $NTARIFF;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATAMOUNT()
    {
      return $this->NVATAMOUNT;
    }

    /**
     * @param float $NVATAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPARTICLE4
     */
    public function setNVATAMOUNT($NVATAMOUNT)
    {
      $this->NVATAMOUNT = $NVATAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNVATPERCENT()
    {
      return $this->NVATPERCENT;
    }

    /**
     * @param float $NVATPERCENT
     * @return \Axess\Dci4Wtp\D4WTPARTICLE4
     */
    public function setNVATPERCENT($NVATPERCENT)
    {
      $this->NVATPERCENT = $NVATPERCENT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZARTICLETYPENAME()
    {
      return $this->SZARTICLETYPENAME;
    }

    /**
     * @param string $SZARTICLETYPENAME
     * @return \Axess\Dci4Wtp\D4WTPARTICLE4
     */
    public function setSZARTICLETYPENAME($SZARTICLETYPENAME)
    {
      $this->SZARTICLETYPENAME = $SZARTICLETYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZCURRENCY()
    {
      return $this->SZCURRENCY;
    }

    /**
     * @param string $SZCURRENCY
     * @return \Axess\Dci4Wtp\D4WTPARTICLE4
     */
    public function setSZCURRENCY($SZCURRENCY)
    {
      $this->SZCURRENCY = $SZCURRENCY;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASKNAME()
    {
      return $this->SZMASKNAME;
    }

    /**
     * @param string $SZMASKNAME
     * @return \Axess\Dci4Wtp\D4WTPARTICLE4
     */
    public function setSZMASKNAME($SZMASKNAME)
    {
      $this->SZMASKNAME = $SZMASKNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMASKSHORTNAME()
    {
      return $this->SZMASKSHORTNAME;
    }

    /**
     * @param string $SZMASKSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPARTICLE4
     */
    public function setSZMASKSHORTNAME($SZMASKSHORTNAME)
    {
      $this->SZMASKSHORTNAME = $SZMASKSHORTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZNAME()
    {
      return $this->SZNAME;
    }

    /**
     * @param string $SZNAME
     * @return \Axess\Dci4Wtp\D4WTPARTICLE4
     */
    public function setSZNAME($SZNAME)
    {
      $this->SZNAME = $SZNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZSHORTNAME()
    {
      return $this->SZSHORTNAME;
    }

    /**
     * @param string $SZSHORTNAME
     * @return \Axess\Dci4Wtp\D4WTPARTICLE4
     */
    public function setSZSHORTNAME($SZSHORTNAME)
    {
      $this->SZSHORTNAME = $SZSHORTNAME;
      return $this;
    }

}
