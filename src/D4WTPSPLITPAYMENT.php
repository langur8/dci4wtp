<?php

namespace Axess\Dci4Wtp;

class D4WTPSPLITPAYMENT
{

    /**
     * @var float $BDELETED
     */
    protected $BDELETED = null;

    /**
     * @var float $BSPLITTEDTO
     */
    protected $BSPLITTEDTO = null;

    /**
     * @var float $FAMOUNT
     */
    protected $FAMOUNT = null;

    /**
     * @var float $NCOREPAYMENTNR
     */
    protected $NCOREPAYMENTNR = null;

    /**
     * @var float $NLFDNR
     */
    protected $NLFDNR = null;

    /**
     * @var float $NPAYMENTNR
     */
    protected $NPAYMENTNR = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NTRANSNR
     */
    protected $NTRANSNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBDELETED()
    {
      return $this->BDELETED;
    }

    /**
     * @param float $BDELETED
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENT
     */
    public function setBDELETED($BDELETED)
    {
      $this->BDELETED = $BDELETED;
      return $this;
    }

    /**
     * @return float
     */
    public function getBSPLITTEDTO()
    {
      return $this->BSPLITTEDTO;
    }

    /**
     * @param float $BSPLITTEDTO
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENT
     */
    public function setBSPLITTEDTO($BSPLITTEDTO)
    {
      $this->BSPLITTEDTO = $BSPLITTEDTO;
      return $this;
    }

    /**
     * @return float
     */
    public function getFAMOUNT()
    {
      return $this->FAMOUNT;
    }

    /**
     * @param float $FAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENT
     */
    public function setFAMOUNT($FAMOUNT)
    {
      $this->FAMOUNT = $FAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOREPAYMENTNR()
    {
      return $this->NCOREPAYMENTNR;
    }

    /**
     * @param float $NCOREPAYMENTNR
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENT
     */
    public function setNCOREPAYMENTNR($NCOREPAYMENTNR)
    {
      $this->NCOREPAYMENTNR = $NCOREPAYMENTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDNR()
    {
      return $this->NLFDNR;
    }

    /**
     * @param float $NLFDNR
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENT
     */
    public function setNLFDNR($NLFDNR)
    {
      $this->NLFDNR = $NLFDNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPAYMENTNR()
    {
      return $this->NPAYMENTNR;
    }

    /**
     * @param float $NPAYMENTNR
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENT
     */
    public function setNPAYMENTNR($NPAYMENTNR)
    {
      $this->NPAYMENTNR = $NPAYMENTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENT
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENT
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNR()
    {
      return $this->NTRANSNR;
    }

    /**
     * @param float $NTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPSPLITPAYMENT
     */
    public function setNTRANSNR($NTRANSNR)
    {
      $this->NTRANSNR = $NTRANSNR;
      return $this;
    }

}
