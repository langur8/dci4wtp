<?php

namespace Axess\Dci4Wtp;

class getReservation
{

    /**
     * @var D4WTPGETRESERVATIONREQUEST $i_ctgetReservationReq
     */
    protected $i_ctgetReservationReq = null;

    /**
     * @param D4WTPGETRESERVATIONREQUEST $i_ctgetReservationReq
     */
    public function __construct($i_ctgetReservationReq)
    {
      $this->i_ctgetReservationReq = $i_ctgetReservationReq;
    }

    /**
     * @return D4WTPGETRESERVATIONREQUEST
     */
    public function getI_ctgetReservationReq()
    {
      return $this->i_ctgetReservationReq;
    }

    /**
     * @param D4WTPGETRESERVATIONREQUEST $i_ctgetReservationReq
     * @return \Axess\Dci4Wtp\getReservation
     */
    public function setI_ctgetReservationReq($i_ctgetReservationReq)
    {
      $this->i_ctgetReservationReq = $i_ctgetReservationReq;
      return $this;
    }

}
