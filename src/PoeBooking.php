<?php

namespace Axess\Dci4Wtp;

class PoeBooking
{

    /**
     * @var float $KartenGrundTypNr
     */
    protected $KartenGrundTypNr = null;

    /**
     * @var float $KassaNR
     */
    protected $KassaNR = null;

    /**
     * @var float $KundenKartenTypNr
     */
    protected $KundenKartenTypNr = null;

    /**
     * @var string $MediaID
     */
    protected $MediaID = null;

    /**
     * @var float $PersonTypeNr
     */
    protected $PersonTypeNr = null;

    /**
     * @var float $PoolNr
     */
    protected $PoolNr = null;

    /**
     * @var float $SerienNr
     */
    protected $SerienNr = null;

    /**
     * @var float $TicketPoolNr
     */
    protected $TicketPoolNr = null;

    /**
     * @var string $Uid
     */
    protected $Uid = null;

    /**
     * @var string $UsageDateTime
     */
    protected $UsageDateTime = null;

    /**
     * @var float $VerkProjNr
     */
    protected $VerkProjNr = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getKartenGrundTypNr()
    {
      return $this->KartenGrundTypNr;
    }

    /**
     * @param float $KartenGrundTypNr
     * @return \Axess\Dci4Wtp\PoeBooking
     */
    public function setKartenGrundTypNr($KartenGrundTypNr)
    {
      $this->KartenGrundTypNr = $KartenGrundTypNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getKassaNR()
    {
      return $this->KassaNR;
    }

    /**
     * @param float $KassaNR
     * @return \Axess\Dci4Wtp\PoeBooking
     */
    public function setKassaNR($KassaNR)
    {
      $this->KassaNR = $KassaNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getKundenKartenTypNr()
    {
      return $this->KundenKartenTypNr;
    }

    /**
     * @param float $KundenKartenTypNr
     * @return \Axess\Dci4Wtp\PoeBooking
     */
    public function setKundenKartenTypNr($KundenKartenTypNr)
    {
      $this->KundenKartenTypNr = $KundenKartenTypNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getMediaID()
    {
      return $this->MediaID;
    }

    /**
     * @param string $MediaID
     * @return \Axess\Dci4Wtp\PoeBooking
     */
    public function setMediaID($MediaID)
    {
      $this->MediaID = $MediaID;
      return $this;
    }

    /**
     * @return float
     */
    public function getPersonTypeNr()
    {
      return $this->PersonTypeNr;
    }

    /**
     * @param float $PersonTypeNr
     * @return \Axess\Dci4Wtp\PoeBooking
     */
    public function setPersonTypeNr($PersonTypeNr)
    {
      $this->PersonTypeNr = $PersonTypeNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getPoolNr()
    {
      return $this->PoolNr;
    }

    /**
     * @param float $PoolNr
     * @return \Axess\Dci4Wtp\PoeBooking
     */
    public function setPoolNr($PoolNr)
    {
      $this->PoolNr = $PoolNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getSerienNr()
    {
      return $this->SerienNr;
    }

    /**
     * @param float $SerienNr
     * @return \Axess\Dci4Wtp\PoeBooking
     */
    public function setSerienNr($SerienNr)
    {
      $this->SerienNr = $SerienNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getTicketPoolNr()
    {
      return $this->TicketPoolNr;
    }

    /**
     * @param float $TicketPoolNr
     * @return \Axess\Dci4Wtp\PoeBooking
     */
    public function setTicketPoolNr($TicketPoolNr)
    {
      $this->TicketPoolNr = $TicketPoolNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getUid()
    {
      return $this->Uid;
    }

    /**
     * @param string $Uid
     * @return \Axess\Dci4Wtp\PoeBooking
     */
    public function setUid($Uid)
    {
      $this->Uid = $Uid;
      return $this;
    }

    /**
     * @return string
     */
    public function getUsageDateTime()
    {
      return $this->UsageDateTime;
    }

    /**
     * @param string $UsageDateTime
     * @return \Axess\Dci4Wtp\PoeBooking
     */
    public function setUsageDateTime($UsageDateTime)
    {
      $this->UsageDateTime = $UsageDateTime;
      return $this;
    }

    /**
     * @return float
     */
    public function getVerkProjNr()
    {
      return $this->VerkProjNr;
    }

    /**
     * @param float $VerkProjNr
     * @return \Axess\Dci4Wtp\PoeBooking
     */
    public function setVerkProjNr($VerkProjNr)
    {
      $this->VerkProjNr = $VerkProjNr;
      return $this;
    }

}
