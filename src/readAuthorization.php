<?php

namespace Axess\Dci4Wtp;

class readAuthorization
{

    /**
     * @var D4WTPREADTICKETREQUEST $i_ctReadTicketReq
     */
    protected $i_ctReadTicketReq = null;

    /**
     * @param D4WTPREADTICKETREQUEST $i_ctReadTicketReq
     */
    public function __construct($i_ctReadTicketReq)
    {
      $this->i_ctReadTicketReq = $i_ctReadTicketReq;
    }

    /**
     * @return D4WTPREADTICKETREQUEST
     */
    public function getI_ctReadTicketReq()
    {
      return $this->i_ctReadTicketReq;
    }

    /**
     * @param D4WTPREADTICKETREQUEST $i_ctReadTicketReq
     * @return \Axess\Dci4Wtp\readAuthorization
     */
    public function setI_ctReadTicketReq($i_ctReadTicketReq)
    {
      $this->i_ctReadTicketReq = $i_ctReadTicketReq;
      return $this;
    }

}
