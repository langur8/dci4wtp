<?php

namespace Axess\Dci4Wtp;

class findPrepaidTicket4
{

    /**
     * @var D4WTPFINDPREPAIDTICKETREQ2 $i_findPrepaidTicket
     */
    protected $i_findPrepaidTicket = null;

    /**
     * @param D4WTPFINDPREPAIDTICKETREQ2 $i_findPrepaidTicket
     */
    public function __construct($i_findPrepaidTicket)
    {
      $this->i_findPrepaidTicket = $i_findPrepaidTicket;
    }

    /**
     * @return D4WTPFINDPREPAIDTICKETREQ2
     */
    public function getI_findPrepaidTicket()
    {
      return $this->i_findPrepaidTicket;
    }

    /**
     * @param D4WTPFINDPREPAIDTICKETREQ2 $i_findPrepaidTicket
     * @return \Axess\Dci4Wtp\findPrepaidTicket4
     */
    public function setI_findPrepaidTicket($i_findPrepaidTicket)
    {
      $this->i_findPrepaidTicket = $i_findPrepaidTicket;
      return $this;
    }

}
