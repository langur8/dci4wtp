<?php

namespace Axess\Dci4Wtp;

class D4WTPSHOPCARTPOSDETDATA
{

    /**
     * @var ArrayOfD4WTPSHOPCARTPOSDETARTDATA $ACTSHOPCARTPOSDETARTDATA
     */
    protected $ACTSHOPCARTPOSDETARTDATA = null;

    /**
     * @var float $NCHIPPROJNO
     */
    protected $NCHIPPROJNO = null;

    /**
     * @var float $NLFDPOSNO
     */
    protected $NLFDPOSNO = null;

    /**
     * @var float $NPERSNO
     */
    protected $NPERSNO = null;

    /**
     * @var float $NPERSPOSNO
     */
    protected $NPERSPOSNO = null;

    /**
     * @var float $NPERSPROJNO
     */
    protected $NPERSPROJNO = null;

    /**
     * @var float $NPOSITIONNO
     */
    protected $NPOSITIONNO = null;

    /**
     * @var float $NSHOPPINGCARTNO
     */
    protected $NSHOPPINGCARTNO = null;

    /**
     * @var float $NSHOPPINGCARTPOSNO
     */
    protected $NSHOPPINGCARTPOSNO = null;

    /**
     * @var float $NSHOPPINGCARTPROJNO
     */
    protected $NSHOPPINGCARTPROJNO = null;

    /**
     * @var string $SZBIRTHDATE
     */
    protected $SZBIRTHDATE = null;

    /**
     * @var string $SZDESC
     */
    protected $SZDESC = null;

    /**
     * @var string $SZFIRSTNAME
     */
    protected $SZFIRSTNAME = null;

    /**
     * @var string $SZGENDER
     */
    protected $SZGENDER = null;

    /**
     * @var string $SZINFO
     */
    protected $SZINFO = null;

    /**
     * @var string $SZLASTNAME
     */
    protected $SZLASTNAME = null;

    /**
     * @var string $SZMEDIAID
     */
    protected $SZMEDIAID = null;

    /**
     * @var string $SZPERSLANGCODE
     */
    protected $SZPERSLANGCODE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPSHOPCARTPOSDETARTDATA
     */
    public function getACTSHOPCARTPOSDETARTDATA()
    {
      return $this->ACTSHOPCARTPOSDETARTDATA;
    }

    /**
     * @param ArrayOfD4WTPSHOPCARTPOSDETARTDATA $ACTSHOPCARTPOSDETARTDATA
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setACTSHOPCARTPOSDETARTDATA($ACTSHOPCARTPOSDETARTDATA)
    {
      $this->ACTSHOPCARTPOSDETARTDATA = $ACTSHOPCARTPOSDETARTDATA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCHIPPROJNO()
    {
      return $this->NCHIPPROJNO;
    }

    /**
     * @param float $NCHIPPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setNCHIPPROJNO($NCHIPPROJNO)
    {
      $this->NCHIPPROJNO = $NCHIPPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLFDPOSNO()
    {
      return $this->NLFDPOSNO;
    }

    /**
     * @param float $NLFDPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setNLFDPOSNO($NLFDPOSNO)
    {
      $this->NLFDPOSNO = $NLFDPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSNO()
    {
      return $this->NPERSNO;
    }

    /**
     * @param float $NPERSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setNPERSNO($NPERSNO)
    {
      $this->NPERSNO = $NPERSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPOSNO()
    {
      return $this->NPERSPOSNO;
    }

    /**
     * @param float $NPERSPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setNPERSPOSNO($NPERSPOSNO)
    {
      $this->NPERSPOSNO = $NPERSPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSPROJNO()
    {
      return $this->NPERSPROJNO;
    }

    /**
     * @param float $NPERSPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setNPERSPROJNO($NPERSPROJNO)
    {
      $this->NPERSPROJNO = $NPERSPROJNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSITIONNO()
    {
      return $this->NPOSITIONNO;
    }

    /**
     * @param float $NPOSITIONNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setNPOSITIONNO($NPOSITIONNO)
    {
      $this->NPOSITIONNO = $NPOSITIONNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTNO()
    {
      return $this->NSHOPPINGCARTNO;
    }

    /**
     * @param float $NSHOPPINGCARTNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setNSHOPPINGCARTNO($NSHOPPINGCARTNO)
    {
      $this->NSHOPPINGCARTNO = $NSHOPPINGCARTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPOSNO()
    {
      return $this->NSHOPPINGCARTPOSNO;
    }

    /**
     * @param float $NSHOPPINGCARTPOSNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setNSHOPPINGCARTPOSNO($NSHOPPINGCARTPOSNO)
    {
      $this->NSHOPPINGCARTPOSNO = $NSHOPPINGCARTPOSNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSHOPPINGCARTPROJNO()
    {
      return $this->NSHOPPINGCARTPROJNO;
    }

    /**
     * @param float $NSHOPPINGCARTPROJNO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setNSHOPPINGCARTPROJNO($NSHOPPINGCARTPROJNO)
    {
      $this->NSHOPPINGCARTPROJNO = $NSHOPPINGCARTPROJNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZBIRTHDATE()
    {
      return $this->SZBIRTHDATE;
    }

    /**
     * @param string $SZBIRTHDATE
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setSZBIRTHDATE($SZBIRTHDATE)
    {
      $this->SZBIRTHDATE = $SZBIRTHDATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESC()
    {
      return $this->SZDESC;
    }

    /**
     * @param string $SZDESC
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setSZDESC($SZDESC)
    {
      $this->SZDESC = $SZDESC;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFIRSTNAME()
    {
      return $this->SZFIRSTNAME;
    }

    /**
     * @param string $SZFIRSTNAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setSZFIRSTNAME($SZFIRSTNAME)
    {
      $this->SZFIRSTNAME = $SZFIRSTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZGENDER()
    {
      return $this->SZGENDER;
    }

    /**
     * @param string $SZGENDER
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setSZGENDER($SZGENDER)
    {
      $this->SZGENDER = $SZGENDER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZINFO()
    {
      return $this->SZINFO;
    }

    /**
     * @param string $SZINFO
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setSZINFO($SZINFO)
    {
      $this->SZINFO = $SZINFO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZLASTNAME()
    {
      return $this->SZLASTNAME;
    }

    /**
     * @param string $SZLASTNAME
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setSZLASTNAME($SZLASTNAME)
    {
      $this->SZLASTNAME = $SZLASTNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZMEDIAID()
    {
      return $this->SZMEDIAID;
    }

    /**
     * @param string $SZMEDIAID
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setSZMEDIAID($SZMEDIAID)
    {
      $this->SZMEDIAID = $SZMEDIAID;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZPERSLANGCODE()
    {
      return $this->SZPERSLANGCODE;
    }

    /**
     * @param string $SZPERSLANGCODE
     * @return \Axess\Dci4Wtp\D4WTPSHOPCARTPOSDETDATA
     */
    public function setSZPERSLANGCODE($SZPERSLANGCODE)
    {
      $this->SZPERSLANGCODE = $SZPERSLANGCODE;
      return $this;
    }

}
