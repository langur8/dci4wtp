<?php

namespace Axess\Dci4Wtp;

class setTravelGroupStatusResponse
{

    /**
     * @var D4WTPRESULT $setTravelGroupStatusResult
     */
    protected $setTravelGroupStatusResult = null;

    /**
     * @param D4WTPRESULT $setTravelGroupStatusResult
     */
    public function __construct($setTravelGroupStatusResult)
    {
      $this->setTravelGroupStatusResult = $setTravelGroupStatusResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getSetTravelGroupStatusResult()
    {
      return $this->setTravelGroupStatusResult;
    }

    /**
     * @param D4WTPRESULT $setTravelGroupStatusResult
     * @return \Axess\Dci4Wtp\setTravelGroupStatusResponse
     */
    public function setSetTravelGroupStatusResult($setTravelGroupStatusResult)
    {
      $this->setTravelGroupStatusResult = $setTravelGroupStatusResult;
      return $this;
    }

}
