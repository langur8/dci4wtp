<?php

namespace Axess\Dci4Wtp;

class getArticleListResponse
{

    /**
     * @var D4WTPGETARTICLELISTRESULT $getArticleListResult
     */
    protected $getArticleListResult = null;

    /**
     * @param D4WTPGETARTICLELISTRESULT $getArticleListResult
     */
    public function __construct($getArticleListResult)
    {
      $this->getArticleListResult = $getArticleListResult;
    }

    /**
     * @return D4WTPGETARTICLELISTRESULT
     */
    public function getGetArticleListResult()
    {
      return $this->getArticleListResult;
    }

    /**
     * @param D4WTPGETARTICLELISTRESULT $getArticleListResult
     * @return \Axess\Dci4Wtp\getArticleListResponse
     */
    public function setGetArticleListResult($getArticleListResult)
    {
      $this->getArticleListResult = $getArticleListResult;
      return $this;
    }

}
