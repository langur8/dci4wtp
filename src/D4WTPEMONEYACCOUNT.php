<?php

namespace Axess\Dci4Wtp;

class D4WTPEMONEYACCOUNT
{

    /**
     * @var string $SZACCOUNTIDENTIFIER
     */
    protected $SZACCOUNTIDENTIFIER = null;

    /**
     * @var string $SZWTPNO
     */
    protected $SZWTPNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSZACCOUNTIDENTIFIER()
    {
      return $this->SZACCOUNTIDENTIFIER;
    }

    /**
     * @param string $SZACCOUNTIDENTIFIER
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNT
     */
    public function setSZACCOUNTIDENTIFIER($SZACCOUNTIDENTIFIER)
    {
      $this->SZACCOUNTIDENTIFIER = $SZACCOUNTIDENTIFIER;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZWTPNO()
    {
      return $this->SZWTPNO;
    }

    /**
     * @param string $SZWTPNO
     * @return \Axess\Dci4Wtp\D4WTPEMONEYACCOUNT
     */
    public function setSZWTPNO($SZWTPNO)
    {
      $this->SZWTPNO = $SZWTPNO;
      return $this;
    }

}
