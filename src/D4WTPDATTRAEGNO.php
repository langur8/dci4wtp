<?php

namespace Axess\Dci4Wtp;

class D4WTPDATTRAEGNO
{

    /**
     * @var float $NDATTRAEGNO
     */
    protected $NDATTRAEGNO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNDATTRAEGNO()
    {
      return $this->NDATTRAEGNO;
    }

    /**
     * @param float $NDATTRAEGNO
     * @return \Axess\Dci4Wtp\D4WTPDATTRAEGNO
     */
    public function setNDATTRAEGNO($NDATTRAEGNO)
    {
      $this->NDATTRAEGNO = $NDATTRAEGNO;
      return $this;
    }

}
