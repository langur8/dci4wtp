<?php

namespace Axess\Dci4Wtp;

class getLNECertificateNumberResponse
{

    /**
     * @var D4WTPLNECERTIFICATENORESULT $getLNECertificateNumberResult
     */
    protected $getLNECertificateNumberResult = null;

    /**
     * @param D4WTPLNECERTIFICATENORESULT $getLNECertificateNumberResult
     */
    public function __construct($getLNECertificateNumberResult)
    {
      $this->getLNECertificateNumberResult = $getLNECertificateNumberResult;
    }

    /**
     * @return D4WTPLNECERTIFICATENORESULT
     */
    public function getGetLNECertificateNumberResult()
    {
      return $this->getLNECertificateNumberResult;
    }

    /**
     * @param D4WTPLNECERTIFICATENORESULT $getLNECertificateNumberResult
     * @return \Axess\Dci4Wtp\getLNECertificateNumberResponse
     */
    public function setGetLNECertificateNumberResult($getLNECertificateNumberResult)
    {
      $this->getLNECertificateNumberResult = $getLNECertificateNumberResult;
      return $this;
    }

}
