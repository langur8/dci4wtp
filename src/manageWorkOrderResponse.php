<?php

namespace Axess\Dci4Wtp;

class manageWorkOrderResponse
{

    /**
     * @var D4WTPRESULT $manageWorkOrderResult
     */
    protected $manageWorkOrderResult = null;

    /**
     * @param D4WTPRESULT $manageWorkOrderResult
     */
    public function __construct($manageWorkOrderResult)
    {
      $this->manageWorkOrderResult = $manageWorkOrderResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getManageWorkOrderResult()
    {
      return $this->manageWorkOrderResult;
    }

    /**
     * @param D4WTPRESULT $manageWorkOrderResult
     * @return \Axess\Dci4Wtp\manageWorkOrderResponse
     */
    public function setManageWorkOrderResult($manageWorkOrderResult)
    {
      $this->manageWorkOrderResult = $manageWorkOrderResult;
      return $this;
    }

}
