<?php

namespace Axess\Dci4Wtp;

class issueTicketResponse
{

    /**
     * @var D4WTPISSUETICKETRESULT $issueTicketResult
     */
    protected $issueTicketResult = null;

    /**
     * @param D4WTPISSUETICKETRESULT $issueTicketResult
     */
    public function __construct($issueTicketResult)
    {
      $this->issueTicketResult = $issueTicketResult;
    }

    /**
     * @return D4WTPISSUETICKETRESULT
     */
    public function getIssueTicketResult()
    {
      return $this->issueTicketResult;
    }

    /**
     * @param D4WTPISSUETICKETRESULT $issueTicketResult
     * @return \Axess\Dci4Wtp\issueTicketResponse
     */
    public function setIssueTicketResult($issueTicketResult)
    {
      $this->issueTicketResult = $issueTicketResult;
      return $this;
    }

}
