<?php

namespace Axess\Dci4Wtp;

class D4WTPBLANKTYPEEN
{

    /**
     * @var float $NACTIVE
     */
    protected $NACTIVE = null;

    /**
     * @var float $NADJUSTPIC
     */
    protected $NADJUSTPIC = null;

    /**
     * @var float $NAREATYPE
     */
    protected $NAREATYPE = null;

    /**
     * @var float $NBLANKDCTYPNR
     */
    protected $NBLANKDCTYPNR = null;

    /**
     * @var float $NCOUNTMEDIA
     */
    protected $NCOUNTMEDIA = null;

    /**
     * @var float $NDATACARRIERTYPNR
     */
    protected $NDATACARRIERTYPNR = null;

    /**
     * @var float $NLANGUAGEID
     */
    protected $NLANGUAGEID = null;

    /**
     * @var float $NLIFTERASEROLLER
     */
    protected $NLIFTERASEROLLER = null;

    /**
     * @var float $NMIRRORBARCODE
     */
    protected $NMIRRORBARCODE = null;

    /**
     * @var float $NPERSDATAREQUIRED
     */
    protected $NPERSDATAREQUIRED = null;

    /**
     * @var float $NPICATPOE
     */
    protected $NPICATPOE = null;

    /**
     * @var float $NPICBOTTOM
     */
    protected $NPICBOTTOM = null;

    /**
     * @var float $NPICBRIGHTNESS
     */
    protected $NPICBRIGHTNESS = null;

    /**
     * @var float $NPICLEFT
     */
    protected $NPICLEFT = null;

    /**
     * @var float $NPICONMEDIA
     */
    protected $NPICONMEDIA = null;

    /**
     * @var float $NPICORIENTATION
     */
    protected $NPICORIENTATION = null;

    /**
     * @var float $NPICRIGHT
     */
    protected $NPICRIGHT = null;

    /**
     * @var float $NPICTOP
     */
    protected $NPICTOP = null;

    /**
     * @var float $NPRINTINTENSITYCP2
     */
    protected $NPRINTINTENSITYCP2 = null;

    /**
     * @var float $NPRINTINTENSITYSP
     */
    protected $NPRINTINTENSITYSP = null;

    /**
     * @var float $NPRINTONLYUNREGCARD
     */
    protected $NPRINTONLYUNREGCARD = null;

    /**
     * @var float $NSORTNR
     */
    protected $NSORTNR = null;

    /**
     * @var float $NSUSPENDPRINT
     */
    protected $NSUSPENDPRINT = null;

    /**
     * @var string $SZAREATYPE
     */
    protected $SZAREATYPE = null;

    /**
     * @var string $SZDATACARRIERNAME
     */
    protected $SZDATACARRIERNAME = null;

    /**
     * @var string $SZDCTYPENAME
     */
    protected $SZDCTYPENAME = null;

    /**
     * @var string $SZDESCRIPTION
     */
    protected $SZDESCRIPTION = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNACTIVE()
    {
      return $this->NACTIVE;
    }

    /**
     * @param float $NACTIVE
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNACTIVE($NACTIVE)
    {
      $this->NACTIVE = $NACTIVE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNADJUSTPIC()
    {
      return $this->NADJUSTPIC;
    }

    /**
     * @param float $NADJUSTPIC
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNADJUSTPIC($NADJUSTPIC)
    {
      $this->NADJUSTPIC = $NADJUSTPIC;
      return $this;
    }

    /**
     * @return float
     */
    public function getNAREATYPE()
    {
      return $this->NAREATYPE;
    }

    /**
     * @param float $NAREATYPE
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNAREATYPE($NAREATYPE)
    {
      $this->NAREATYPE = $NAREATYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNBLANKDCTYPNR()
    {
      return $this->NBLANKDCTYPNR;
    }

    /**
     * @param float $NBLANKDCTYPNR
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNBLANKDCTYPNR($NBLANKDCTYPNR)
    {
      $this->NBLANKDCTYPNR = $NBLANKDCTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOUNTMEDIA()
    {
      return $this->NCOUNTMEDIA;
    }

    /**
     * @param float $NCOUNTMEDIA
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNCOUNTMEDIA($NCOUNTMEDIA)
    {
      $this->NCOUNTMEDIA = $NCOUNTMEDIA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNDATACARRIERTYPNR()
    {
      return $this->NDATACARRIERTYPNR;
    }

    /**
     * @param float $NDATACARRIERTYPNR
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNDATACARRIERTYPNR($NDATACARRIERTYPNR)
    {
      $this->NDATACARRIERTYPNR = $NDATACARRIERTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLANGUAGEID()
    {
      return $this->NLANGUAGEID;
    }

    /**
     * @param float $NLANGUAGEID
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNLANGUAGEID($NLANGUAGEID)
    {
      $this->NLANGUAGEID = $NLANGUAGEID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNLIFTERASEROLLER()
    {
      return $this->NLIFTERASEROLLER;
    }

    /**
     * @param float $NLIFTERASEROLLER
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNLIFTERASEROLLER($NLIFTERASEROLLER)
    {
      $this->NLIFTERASEROLLER = $NLIFTERASEROLLER;
      return $this;
    }

    /**
     * @return float
     */
    public function getNMIRRORBARCODE()
    {
      return $this->NMIRRORBARCODE;
    }

    /**
     * @param float $NMIRRORBARCODE
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNMIRRORBARCODE($NMIRRORBARCODE)
    {
      $this->NMIRRORBARCODE = $NMIRRORBARCODE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPERSDATAREQUIRED()
    {
      return $this->NPERSDATAREQUIRED;
    }

    /**
     * @param float $NPERSDATAREQUIRED
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNPERSDATAREQUIRED($NPERSDATAREQUIRED)
    {
      $this->NPERSDATAREQUIRED = $NPERSDATAREQUIRED;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICATPOE()
    {
      return $this->NPICATPOE;
    }

    /**
     * @param float $NPICATPOE
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNPICATPOE($NPICATPOE)
    {
      $this->NPICATPOE = $NPICATPOE;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICBOTTOM()
    {
      return $this->NPICBOTTOM;
    }

    /**
     * @param float $NPICBOTTOM
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNPICBOTTOM($NPICBOTTOM)
    {
      $this->NPICBOTTOM = $NPICBOTTOM;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICBRIGHTNESS()
    {
      return $this->NPICBRIGHTNESS;
    }

    /**
     * @param float $NPICBRIGHTNESS
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNPICBRIGHTNESS($NPICBRIGHTNESS)
    {
      $this->NPICBRIGHTNESS = $NPICBRIGHTNESS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICLEFT()
    {
      return $this->NPICLEFT;
    }

    /**
     * @param float $NPICLEFT
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNPICLEFT($NPICLEFT)
    {
      $this->NPICLEFT = $NPICLEFT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICONMEDIA()
    {
      return $this->NPICONMEDIA;
    }

    /**
     * @param float $NPICONMEDIA
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNPICONMEDIA($NPICONMEDIA)
    {
      $this->NPICONMEDIA = $NPICONMEDIA;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICORIENTATION()
    {
      return $this->NPICORIENTATION;
    }

    /**
     * @param float $NPICORIENTATION
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNPICORIENTATION($NPICORIENTATION)
    {
      $this->NPICORIENTATION = $NPICORIENTATION;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICRIGHT()
    {
      return $this->NPICRIGHT;
    }

    /**
     * @param float $NPICRIGHT
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNPICRIGHT($NPICRIGHT)
    {
      $this->NPICRIGHT = $NPICRIGHT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPICTOP()
    {
      return $this->NPICTOP;
    }

    /**
     * @param float $NPICTOP
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNPICTOP($NPICTOP)
    {
      $this->NPICTOP = $NPICTOP;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPRINTINTENSITYCP2()
    {
      return $this->NPRINTINTENSITYCP2;
    }

    /**
     * @param float $NPRINTINTENSITYCP2
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNPRINTINTENSITYCP2($NPRINTINTENSITYCP2)
    {
      $this->NPRINTINTENSITYCP2 = $NPRINTINTENSITYCP2;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPRINTINTENSITYSP()
    {
      return $this->NPRINTINTENSITYSP;
    }

    /**
     * @param float $NPRINTINTENSITYSP
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNPRINTINTENSITYSP($NPRINTINTENSITYSP)
    {
      $this->NPRINTINTENSITYSP = $NPRINTINTENSITYSP;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPRINTONLYUNREGCARD()
    {
      return $this->NPRINTONLYUNREGCARD;
    }

    /**
     * @param float $NPRINTONLYUNREGCARD
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNPRINTONLYUNREGCARD($NPRINTONLYUNREGCARD)
    {
      $this->NPRINTONLYUNREGCARD = $NPRINTONLYUNREGCARD;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSORTNR()
    {
      return $this->NSORTNR;
    }

    /**
     * @param float $NSORTNR
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNSORTNR($NSORTNR)
    {
      $this->NSORTNR = $NSORTNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSUSPENDPRINT()
    {
      return $this->NSUSPENDPRINT;
    }

    /**
     * @param float $NSUSPENDPRINT
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setNSUSPENDPRINT($NSUSPENDPRINT)
    {
      $this->NSUSPENDPRINT = $NSUSPENDPRINT;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZAREATYPE()
    {
      return $this->SZAREATYPE;
    }

    /**
     * @param string $SZAREATYPE
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setSZAREATYPE($SZAREATYPE)
    {
      $this->SZAREATYPE = $SZAREATYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDATACARRIERNAME()
    {
      return $this->SZDATACARRIERNAME;
    }

    /**
     * @param string $SZDATACARRIERNAME
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setSZDATACARRIERNAME($SZDATACARRIERNAME)
    {
      $this->SZDATACARRIERNAME = $SZDATACARRIERNAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDCTYPENAME()
    {
      return $this->SZDCTYPENAME;
    }

    /**
     * @param string $SZDCTYPENAME
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setSZDCTYPENAME($SZDCTYPENAME)
    {
      $this->SZDCTYPENAME = $SZDCTYPENAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZDESCRIPTION()
    {
      return $this->SZDESCRIPTION;
    }

    /**
     * @param string $SZDESCRIPTION
     * @return \Axess\Dci4Wtp\D4WTPBLANKTYPEEN
     */
    public function setSZDESCRIPTION($SZDESCRIPTION)
    {
      $this->SZDESCRIPTION = $SZDESCRIPTION;
      return $this;
    }

}
