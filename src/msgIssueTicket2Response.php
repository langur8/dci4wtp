<?php

namespace Axess\Dci4Wtp;

class msgIssueTicket2Response
{

    /**
     * @var D4WTPMSGISSUETICKETRESULT2 $msgIssueTicket2Result
     */
    protected $msgIssueTicket2Result = null;

    /**
     * @param D4WTPMSGISSUETICKETRESULT2 $msgIssueTicket2Result
     */
    public function __construct($msgIssueTicket2Result)
    {
      $this->msgIssueTicket2Result = $msgIssueTicket2Result;
    }

    /**
     * @return D4WTPMSGISSUETICKETRESULT2
     */
    public function getMsgIssueTicket2Result()
    {
      return $this->msgIssueTicket2Result;
    }

    /**
     * @param D4WTPMSGISSUETICKETRESULT2 $msgIssueTicket2Result
     * @return \Axess\Dci4Wtp\msgIssueTicket2Response
     */
    public function setMsgIssueTicket2Result($msgIssueTicket2Result)
    {
      $this->msgIssueTicket2Result = $msgIssueTicket2Result;
      return $this;
    }

}
