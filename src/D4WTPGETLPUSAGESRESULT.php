<?php

namespace Axess\Dci4Wtp;

class D4WTPGETLPUSAGESRESULT
{

    /**
     * @var ArrayOfD4WTPLPUSAGES $ACTLPUSAGES
     */
    protected $ACTLPUSAGES = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPLPUSAGES
     */
    public function getACTLPUSAGES()
    {
      return $this->ACTLPUSAGES;
    }

    /**
     * @param ArrayOfD4WTPLPUSAGES $ACTLPUSAGES
     * @return \Axess\Dci4Wtp\D4WTPGETLPUSAGESRESULT
     */
    public function setACTLPUSAGES($ACTLPUSAGES)
    {
      $this->ACTLPUSAGES = $ACTLPUSAGES;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETLPUSAGESRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETLPUSAGESRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
