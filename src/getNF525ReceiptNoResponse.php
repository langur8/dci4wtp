<?php

namespace Axess\Dci4Wtp;

class getNF525ReceiptNoResponse
{

    /**
     * @var D4WTPNF525RECEIPTNORESULT $getNF525ReceiptNoResult
     */
    protected $getNF525ReceiptNoResult = null;

    /**
     * @param D4WTPNF525RECEIPTNORESULT $getNF525ReceiptNoResult
     */
    public function __construct($getNF525ReceiptNoResult)
    {
      $this->getNF525ReceiptNoResult = $getNF525ReceiptNoResult;
    }

    /**
     * @return D4WTPNF525RECEIPTNORESULT
     */
    public function getGetNF525ReceiptNoResult()
    {
      return $this->getNF525ReceiptNoResult;
    }

    /**
     * @param D4WTPNF525RECEIPTNORESULT $getNF525ReceiptNoResult
     * @return \Axess\Dci4Wtp\getNF525ReceiptNoResponse
     */
    public function setGetNF525ReceiptNoResult($getNF525ReceiptNoResult)
    {
      $this->getNF525ReceiptNoResult = $getNF525ReceiptNoResult;
      return $this;
    }

}
