<?php

namespace Axess\Dci4Wtp;

class ArrayOfWTPProductsToIgnore implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var WTPProductsToIgnore[] $WTPProductsToIgnore
     */
    protected $WTPProductsToIgnore = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return WTPProductsToIgnore[]
     */
    public function getWTPProductsToIgnore()
    {
      return $this->WTPProductsToIgnore;
    }

    /**
     * @param WTPProductsToIgnore[] $WTPProductsToIgnore
     * @return \Axess\Dci4Wtp\ArrayOfWTPProductsToIgnore
     */
    public function setWTPProductsToIgnore(array $WTPProductsToIgnore = null)
    {
      $this->WTPProductsToIgnore = $WTPProductsToIgnore;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->WTPProductsToIgnore[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return WTPProductsToIgnore
     */
    public function offsetGet($offset)
    {
      return $this->WTPProductsToIgnore[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param WTPProductsToIgnore $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->WTPProductsToIgnore[] = $value;
      } else {
        $this->WTPProductsToIgnore[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->WTPProductsToIgnore[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return WTPProductsToIgnore Return the current element
     */
    public function current()
    {
      return current($this->WTPProductsToIgnore);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->WTPProductsToIgnore);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->WTPProductsToIgnore);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->WTPProductsToIgnore);
    }

    /**
     * Countable implementation
     *
     * @return WTPProductsToIgnore Return count of elements
     */
    public function count()
    {
      return count($this->WTPProductsToIgnore);
    }

}
