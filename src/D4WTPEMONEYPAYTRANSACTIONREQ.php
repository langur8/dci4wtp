<?php

namespace Axess\Dci4Wtp;

class D4WTPEMONEYPAYTRANSACTIONREQ
{

    /**
     * @var float $BPAYWHOLETRANSACTION
     */
    protected $BPAYWHOLETRANSACTION = null;

    /**
     * @var D4WTPEMONEYACCOUNT $CTEMONEYACCOUNT
     */
    protected $CTEMONEYACCOUNT = null;

    /**
     * @var float $FAMOUNT
     */
    protected $FAMOUNT = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNR
     */
    protected $NTRANSNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBPAYWHOLETRANSACTION()
    {
      return $this->BPAYWHOLETRANSACTION;
    }

    /**
     * @param float $BPAYWHOLETRANSACTION
     * @return \Axess\Dci4Wtp\D4WTPEMONEYPAYTRANSACTIONREQ
     */
    public function setBPAYWHOLETRANSACTION($BPAYWHOLETRANSACTION)
    {
      $this->BPAYWHOLETRANSACTION = $BPAYWHOLETRANSACTION;
      return $this;
    }

    /**
     * @return D4WTPEMONEYACCOUNT
     */
    public function getCTEMONEYACCOUNT()
    {
      return $this->CTEMONEYACCOUNT;
    }

    /**
     * @param D4WTPEMONEYACCOUNT $CTEMONEYACCOUNT
     * @return \Axess\Dci4Wtp\D4WTPEMONEYPAYTRANSACTIONREQ
     */
    public function setCTEMONEYACCOUNT($CTEMONEYACCOUNT)
    {
      $this->CTEMONEYACCOUNT = $CTEMONEYACCOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFAMOUNT()
    {
      return $this->FAMOUNT;
    }

    /**
     * @param float $FAMOUNT
     * @return \Axess\Dci4Wtp\D4WTPEMONEYPAYTRANSACTIONREQ
     */
    public function setFAMOUNT($FAMOUNT)
    {
      $this->FAMOUNT = $FAMOUNT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPEMONEYPAYTRANSACTIONREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNR()
    {
      return $this->NTRANSNR;
    }

    /**
     * @param float $NTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPEMONEYPAYTRANSACTIONREQ
     */
    public function setNTRANSNR($NTRANSNR)
    {
      $this->NTRANSNR = $NTRANSNR;
      return $this;
    }

}
