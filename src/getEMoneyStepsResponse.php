<?php

namespace Axess\Dci4Wtp;

class getEMoneyStepsResponse
{

    /**
     * @var D4AXCOUNTSTEPRES $getEMoneyStepsResult
     */
    protected $getEMoneyStepsResult = null;

    /**
     * @param D4AXCOUNTSTEPRES $getEMoneyStepsResult
     */
    public function __construct($getEMoneyStepsResult)
    {
      $this->getEMoneyStepsResult = $getEMoneyStepsResult;
    }

    /**
     * @return D4AXCOUNTSTEPRES
     */
    public function getGetEMoneyStepsResult()
    {
      return $this->getEMoneyStepsResult;
    }

    /**
     * @param D4AXCOUNTSTEPRES $getEMoneyStepsResult
     * @return \Axess\Dci4Wtp\getEMoneyStepsResponse
     */
    public function setGetEMoneyStepsResult($getEMoneyStepsResult)
    {
      $this->getEMoneyStepsResult = $getEMoneyStepsResult;
      return $this;
    }

}
