<?php

namespace Axess\Dci4Wtp;

class WTPTicketDataResult
{

    /**
     * @var int $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var TicketValidity $Result
     */
    protected $Result = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param int $NERRORNO
     * @return \Axess\Dci4Wtp\WTPTicketDataResult
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return TicketValidity
     */
    public function getResult()
    {
      return $this->Result;
    }

    /**
     * @param TicketValidity $Result
     * @return \Axess\Dci4Wtp\WTPTicketDataResult
     */
    public function setResult($Result)
    {
      $this->Result = $Result;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\WTPTicketDataResult
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
