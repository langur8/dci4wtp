<?php

namespace Axess\Dci4Wtp;

class restoreTicketStatusResponse
{

    /**
     * @var D4WTPRESULT $restoreTicketStatusResult
     */
    protected $restoreTicketStatusResult = null;

    /**
     * @param D4WTPRESULT $restoreTicketStatusResult
     */
    public function __construct($restoreTicketStatusResult)
    {
      $this->restoreTicketStatusResult = $restoreTicketStatusResult;
    }

    /**
     * @return D4WTPRESULT
     */
    public function getRestoreTicketStatusResult()
    {
      return $this->restoreTicketStatusResult;
    }

    /**
     * @param D4WTPRESULT $restoreTicketStatusResult
     * @return \Axess\Dci4Wtp\restoreTicketStatusResponse
     */
    public function setRestoreTicketStatusResult($restoreTicketStatusResult)
    {
      $this->restoreTicketStatusResult = $restoreTicketStatusResult;
      return $this;
    }

}
