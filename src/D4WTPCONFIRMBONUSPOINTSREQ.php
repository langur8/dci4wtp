<?php

namespace Axess\Dci4Wtp;

class D4WTPCONFIRMBONUSPOINTSREQ
{

    /**
     * @var float $NCUSTOMERACCOUNTNO
     */
    protected $NCUSTOMERACCOUNTNO = null;

    /**
     * @var float $NPOSNR
     */
    protected $NPOSNR = null;

    /**
     * @var float $NPROJNR
     */
    protected $NPROJNR = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NTRANSNR
     */
    protected $NTRANSNR = null;

    /**
     * @var float $NTRANSSTATUSNR
     */
    protected $NTRANSSTATUSNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getNCUSTOMERACCOUNTNO()
    {
      return $this->NCUSTOMERACCOUNTNO;
    }

    /**
     * @param float $NCUSTOMERACCOUNTNO
     * @return \Axess\Dci4Wtp\D4WTPCONFIRMBONUSPOINTSREQ
     */
    public function setNCUSTOMERACCOUNTNO($NCUSTOMERACCOUNTNO)
    {
      $this->NCUSTOMERACCOUNTNO = $NCUSTOMERACCOUNTNO;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPOSNR()
    {
      return $this->NPOSNR;
    }

    /**
     * @param float $NPOSNR
     * @return \Axess\Dci4Wtp\D4WTPCONFIRMBONUSPOINTSREQ
     */
    public function setNPOSNR($NPOSNR)
    {
      $this->NPOSNR = $NPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNPROJNR()
    {
      return $this->NPROJNR;
    }

    /**
     * @param float $NPROJNR
     * @return \Axess\Dci4Wtp\D4WTPCONFIRMBONUSPOINTSREQ
     */
    public function setNPROJNR($NPROJNR)
    {
      $this->NPROJNR = $NPROJNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPCONFIRMBONUSPOINTSREQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSNR()
    {
      return $this->NTRANSNR;
    }

    /**
     * @param float $NTRANSNR
     * @return \Axess\Dci4Wtp\D4WTPCONFIRMBONUSPOINTSREQ
     */
    public function setNTRANSNR($NTRANSNR)
    {
      $this->NTRANSNR = $NTRANSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNTRANSSTATUSNR()
    {
      return $this->NTRANSSTATUSNR;
    }

    /**
     * @param float $NTRANSSTATUSNR
     * @return \Axess\Dci4Wtp\D4WTPCONFIRMBONUSPOINTSREQ
     */
    public function setNTRANSSTATUSNR($NTRANSSTATUSNR)
    {
      $this->NTRANSSTATUSNR = $NTRANSSTATUSNR;
      return $this;
    }

}
