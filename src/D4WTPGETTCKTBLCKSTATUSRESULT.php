<?php

namespace Axess\Dci4Wtp;

class D4WTPGETTCKTBLCKSTATUSRESULT
{

    /**
     * @var ArrayOfD4WTPTICKETBLOCKSTATUS $ACTTICKETBLOCKSTATUS
     */
    protected $ACTTICKETBLOCKSTATUS = null;

    /**
     * @var float $NERRORNO
     */
    protected $NERRORNO = null;

    /**
     * @var string $SZERRORMESSAGE
     */
    protected $SZERRORMESSAGE = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfD4WTPTICKETBLOCKSTATUS
     */
    public function getACTTICKETBLOCKSTATUS()
    {
      return $this->ACTTICKETBLOCKSTATUS;
    }

    /**
     * @param ArrayOfD4WTPTICKETBLOCKSTATUS $ACTTICKETBLOCKSTATUS
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTBLCKSTATUSRESULT
     */
    public function setACTTICKETBLOCKSTATUS($ACTTICKETBLOCKSTATUS)
    {
      $this->ACTTICKETBLOCKSTATUS = $ACTTICKETBLOCKSTATUS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNERRORNO()
    {
      return $this->NERRORNO;
    }

    /**
     * @param float $NERRORNO
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTBLCKSTATUSRESULT
     */
    public function setNERRORNO($NERRORNO)
    {
      $this->NERRORNO = $NERRORNO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZERRORMESSAGE()
    {
      return $this->SZERRORMESSAGE;
    }

    /**
     * @param string $SZERRORMESSAGE
     * @return \Axess\Dci4Wtp\D4WTPGETTCKTBLCKSTATUSRESULT
     */
    public function setSZERRORMESSAGE($SZERRORMESSAGE)
    {
      $this->SZERRORMESSAGE = $SZERRORMESSAGE;
      return $this;
    }

}
