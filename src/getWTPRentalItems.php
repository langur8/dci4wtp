<?php

namespace Axess\Dci4Wtp;

class getWTPRentalItems
{

    /**
     * @var D4WTPRENTALITEMREQ $i_ctWTPRentalItemReq
     */
    protected $i_ctWTPRentalItemReq = null;

    /**
     * @param D4WTPRENTALITEMREQ $i_ctWTPRentalItemReq
     */
    public function __construct($i_ctWTPRentalItemReq)
    {
      $this->i_ctWTPRentalItemReq = $i_ctWTPRentalItemReq;
    }

    /**
     * @return D4WTPRENTALITEMREQ
     */
    public function getI_ctWTPRentalItemReq()
    {
      return $this->i_ctWTPRentalItemReq;
    }

    /**
     * @param D4WTPRENTALITEMREQ $i_ctWTPRentalItemReq
     * @return \Axess\Dci4Wtp\getWTPRentalItems
     */
    public function setI_ctWTPRentalItemReq($i_ctWTPRentalItemReq)
    {
      $this->i_ctWTPRentalItemReq = $i_ctWTPRentalItemReq;
      return $this;
    }

}
