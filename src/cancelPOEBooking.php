<?php

namespace Axess\Dci4Wtp;

class cancelPOEBooking
{

    /**
     * @var float $i_nSessionID
     */
    protected $i_nSessionID = null;

    /**
     * @var CancelPOEBookingRequest $request
     */
    protected $request = null;

    /**
     * @param float $i_nSessionID
     * @param CancelPOEBookingRequest $request
     */
    public function __construct($i_nSessionID, $request)
    {
      $this->i_nSessionID = $i_nSessionID;
      $this->request = $request;
    }

    /**
     * @return float
     */
    public function getI_nSessionID()
    {
      return $this->i_nSessionID;
    }

    /**
     * @param float $i_nSessionID
     * @return \Axess\Dci4Wtp\cancelPOEBooking
     */
    public function setI_nSessionID($i_nSessionID)
    {
      $this->i_nSessionID = $i_nSessionID;
      return $this;
    }

    /**
     * @return CancelPOEBookingRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param CancelPOEBookingRequest $request
     * @return \Axess\Dci4Wtp\cancelPOEBooking
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
