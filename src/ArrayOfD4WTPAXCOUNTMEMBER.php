<?php

namespace Axess\Dci4Wtp;

class ArrayOfD4WTPAXCOUNTMEMBER implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var D4WTPAXCOUNTMEMBER[] $D4WTPAXCOUNTMEMBER
     */
    protected $D4WTPAXCOUNTMEMBER = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return D4WTPAXCOUNTMEMBER[]
     */
    public function getD4WTPAXCOUNTMEMBER()
    {
      return $this->D4WTPAXCOUNTMEMBER;
    }

    /**
     * @param D4WTPAXCOUNTMEMBER[] $D4WTPAXCOUNTMEMBER
     * @return \Axess\Dci4Wtp\ArrayOfD4WTPAXCOUNTMEMBER
     */
    public function setD4WTPAXCOUNTMEMBER(array $D4WTPAXCOUNTMEMBER = null)
    {
      $this->D4WTPAXCOUNTMEMBER = $D4WTPAXCOUNTMEMBER;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->D4WTPAXCOUNTMEMBER[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return D4WTPAXCOUNTMEMBER
     */
    public function offsetGet($offset)
    {
      return $this->D4WTPAXCOUNTMEMBER[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param D4WTPAXCOUNTMEMBER $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->D4WTPAXCOUNTMEMBER[] = $value;
      } else {
        $this->D4WTPAXCOUNTMEMBER[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->D4WTPAXCOUNTMEMBER[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return D4WTPAXCOUNTMEMBER Return the current element
     */
    public function current()
    {
      return current($this->D4WTPAXCOUNTMEMBER);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->D4WTPAXCOUNTMEMBER);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->D4WTPAXCOUNTMEMBER);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->D4WTPAXCOUNTMEMBER);
    }

    /**
     * Countable implementation
     *
     * @return D4WTPAXCOUNTMEMBER Return count of elements
     */
    public function count()
    {
      return count($this->D4WTPAXCOUNTMEMBER);
    }

}
