<?php

namespace Axess\Dci4Wtp;

class SPECIALADDRESS
{

    /**
     * @var ADDRESS $ADDRESS
     */
    protected $ADDRESS = null;

    /**
     * @var float $NADRTYPNR
     */
    protected $NADRTYPNR = null;

    /**
     * @var float $NCOMPNR
     */
    protected $NCOMPNR = null;

    /**
     * @var float $NCOMPPOSNR
     */
    protected $NCOMPPOSNR = null;

    /**
     * @var float $NCOMPPROJNR
     */
    protected $NCOMPPROJNR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ADDRESS
     */
    public function getADDRESS()
    {
      return $this->ADDRESS;
    }

    /**
     * @param ADDRESS $ADDRESS
     * @return \Axess\Dci4Wtp\SPECIALADDRESS
     */
    public function setADDRESS($ADDRESS)
    {
      $this->ADDRESS = $ADDRESS;
      return $this;
    }

    /**
     * @return float
     */
    public function getNADRTYPNR()
    {
      return $this->NADRTYPNR;
    }

    /**
     * @param float $NADRTYPNR
     * @return \Axess\Dci4Wtp\SPECIALADDRESS
     */
    public function setNADRTYPNR($NADRTYPNR)
    {
      $this->NADRTYPNR = $NADRTYPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPNR()
    {
      return $this->NCOMPNR;
    }

    /**
     * @param float $NCOMPNR
     * @return \Axess\Dci4Wtp\SPECIALADDRESS
     */
    public function setNCOMPNR($NCOMPNR)
    {
      $this->NCOMPNR = $NCOMPNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPPOSNR()
    {
      return $this->NCOMPPOSNR;
    }

    /**
     * @param float $NCOMPPOSNR
     * @return \Axess\Dci4Wtp\SPECIALADDRESS
     */
    public function setNCOMPPOSNR($NCOMPPOSNR)
    {
      $this->NCOMPPOSNR = $NCOMPPOSNR;
      return $this;
    }

    /**
     * @return float
     */
    public function getNCOMPPROJNR()
    {
      return $this->NCOMPPROJNR;
    }

    /**
     * @param float $NCOMPPROJNR
     * @return \Axess\Dci4Wtp\SPECIALADDRESS
     */
    public function setNCOMPPROJNR($NCOMPPROJNR)
    {
      $this->NCOMPPROJNR = $NCOMPPROJNR;
      return $this;
    }

}
