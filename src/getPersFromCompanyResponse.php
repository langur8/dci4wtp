<?php

namespace Axess\Dci4Wtp;

class getPersFromCompanyResponse
{

    /**
     * @var D4WTPGETPERSFROMCMPNYRESULT $getPersFromCompanyResult
     */
    protected $getPersFromCompanyResult = null;

    /**
     * @param D4WTPGETPERSFROMCMPNYRESULT $getPersFromCompanyResult
     */
    public function __construct($getPersFromCompanyResult)
    {
      $this->getPersFromCompanyResult = $getPersFromCompanyResult;
    }

    /**
     * @return D4WTPGETPERSFROMCMPNYRESULT
     */
    public function getGetPersFromCompanyResult()
    {
      return $this->getPersFromCompanyResult;
    }

    /**
     * @param D4WTPGETPERSFROMCMPNYRESULT $getPersFromCompanyResult
     * @return \Axess\Dci4Wtp\getPersFromCompanyResponse
     */
    public function setGetPersFromCompanyResult($getPersFromCompanyResult)
    {
      $this->getPersFromCompanyResult = $getPersFromCompanyResult;
      return $this;
    }

}
