<?php

namespace Axess\Dci4Wtp;

class EntryStatePerDay
{

    /**
     * @var boolean $CanEnter
     */
    protected $CanEnter = null;

    /**
     * @var string $Reason
     */
    protected $Reason = null;

    /**
     * @var string $UsageDate
     */
    protected $UsageDate = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getCanEnter()
    {
      return $this->CanEnter;
    }

    /**
     * @param boolean $CanEnter
     * @return \Axess\Dci4Wtp\EntryStatePerDay
     */
    public function setCanEnter($CanEnter)
    {
      $this->CanEnter = $CanEnter;
      return $this;
    }

    /**
     * @return string
     */
    public function getReason()
    {
      return $this->Reason;
    }

    /**
     * @param string $Reason
     * @return \Axess\Dci4Wtp\EntryStatePerDay
     */
    public function setReason($Reason)
    {
      $this->Reason = $Reason;
      return $this;
    }

    /**
     * @return string
     */
    public function getUsageDate()
    {
      return $this->UsageDate;
    }

    /**
     * @param string $UsageDate
     * @return \Axess\Dci4Wtp\EntryStatePerDay
     */
    public function setUsageDate($UsageDate)
    {
      $this->UsageDate = $UsageDate;
      return $this;
    }

}
