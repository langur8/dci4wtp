<?php

namespace Axess\Dci4Wtp;

class storeTempTXT
{

    /**
     * @var D4WTPSTORETEMPTXTREQUEST $i_ctTempTxtReq
     */
    protected $i_ctTempTxtReq = null;

    /**
     * @param D4WTPSTORETEMPTXTREQUEST $i_ctTempTxtReq
     */
    public function __construct($i_ctTempTxtReq)
    {
      $this->i_ctTempTxtReq = $i_ctTempTxtReq;
    }

    /**
     * @return D4WTPSTORETEMPTXTREQUEST
     */
    public function getI_ctTempTxtReq()
    {
      return $this->i_ctTempTxtReq;
    }

    /**
     * @param D4WTPSTORETEMPTXTREQUEST $i_ctTempTxtReq
     * @return \Axess\Dci4Wtp\storeTempTXT
     */
    public function setI_ctTempTxtReq($i_ctTempTxtReq)
    {
      $this->i_ctTempTxtReq = $i_ctTempTxtReq;
      return $this;
    }

}
