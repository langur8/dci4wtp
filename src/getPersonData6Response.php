<?php

namespace Axess\Dci4Wtp;

class getPersonData6Response
{

    /**
     * @var D4WTPGETPERSON6RESULT $getPersonData6Result
     */
    protected $getPersonData6Result = null;

    /**
     * @param D4WTPGETPERSON6RESULT $getPersonData6Result
     */
    public function __construct($getPersonData6Result)
    {
      $this->getPersonData6Result = $getPersonData6Result;
    }

    /**
     * @return D4WTPGETPERSON6RESULT
     */
    public function getGetPersonData6Result()
    {
      return $this->getPersonData6Result;
    }

    /**
     * @param D4WTPGETPERSON6RESULT $getPersonData6Result
     * @return \Axess\Dci4Wtp\getPersonData6Response
     */
    public function setGetPersonData6Result($getPersonData6Result)
    {
      $this->getPersonData6Result = $getPersonData6Result;
      return $this;
    }

}
