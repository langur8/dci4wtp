<?php

namespace Axess\Dci4Wtp;

class getTariffList
{

    /**
     * @var D4WTPTARIFFLISTREQUEST $i_ctTariffListReq
     */
    protected $i_ctTariffListReq = null;

    /**
     * @param D4WTPTARIFFLISTREQUEST $i_ctTariffListReq
     */
    public function __construct($i_ctTariffListReq)
    {
      $this->i_ctTariffListReq = $i_ctTariffListReq;
    }

    /**
     * @return D4WTPTARIFFLISTREQUEST
     */
    public function getI_ctTariffListReq()
    {
      return $this->i_ctTariffListReq;
    }

    /**
     * @param D4WTPTARIFFLISTREQUEST $i_ctTariffListReq
     * @return \Axess\Dci4Wtp\getTariffList
     */
    public function setI_ctTariffListReq($i_ctTariffListReq)
    {
      $this->i_ctTariffListReq = $i_ctTariffListReq;
      return $this;
    }

}
