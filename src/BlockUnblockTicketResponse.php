<?php

namespace Axess\Dci4Wtp;

class BlockUnblockTicketResponse
{

    /**
     * @var D4WTPBLOCKUNBLOCKRESULT $BlockUnblockTicketResult
     */
    protected $BlockUnblockTicketResult = null;

    /**
     * @param D4WTPBLOCKUNBLOCKRESULT $BlockUnblockTicketResult
     */
    public function __construct($BlockUnblockTicketResult)
    {
      $this->BlockUnblockTicketResult = $BlockUnblockTicketResult;
    }

    /**
     * @return D4WTPBLOCKUNBLOCKRESULT
     */
    public function getBlockUnblockTicketResult()
    {
      return $this->BlockUnblockTicketResult;
    }

    /**
     * @param D4WTPBLOCKUNBLOCKRESULT $BlockUnblockTicketResult
     * @return \Axess\Dci4Wtp\BlockUnblockTicketResponse
     */
    public function setBlockUnblockTicketResult($BlockUnblockTicketResult)
    {
      $this->BlockUnblockTicketResult = $BlockUnblockTicketResult;
      return $this;
    }

}
