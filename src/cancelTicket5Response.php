<?php

namespace Axess\Dci4Wtp;

class cancelTicket5Response
{

    /**
     * @var D4WTPCANCELTICKET5RESULT $cancelTicket5Result
     */
    protected $cancelTicket5Result = null;

    /**
     * @param D4WTPCANCELTICKET5RESULT $cancelTicket5Result
     */
    public function __construct($cancelTicket5Result)
    {
      $this->cancelTicket5Result = $cancelTicket5Result;
    }

    /**
     * @return D4WTPCANCELTICKET5RESULT
     */
    public function getCancelTicket5Result()
    {
      return $this->cancelTicket5Result;
    }

    /**
     * @param D4WTPCANCELTICKET5RESULT $cancelTicket5Result
     * @return \Axess\Dci4Wtp\cancelTicket5Response
     */
    public function setCancelTicket5Result($cancelTicket5Result)
    {
      $this->cancelTicket5Result = $cancelTicket5Result;
      return $this;
    }

}
