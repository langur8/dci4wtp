<?php

namespace Axess\Dci4Wtp;

class editOrderHeader
{

    /**
     * @var D4WTPEDITORDERREQ $i_ctEditOrderReq
     */
    protected $i_ctEditOrderReq = null;

    /**
     * @param D4WTPEDITORDERREQ $i_ctEditOrderReq
     */
    public function __construct($i_ctEditOrderReq)
    {
      $this->i_ctEditOrderReq = $i_ctEditOrderReq;
    }

    /**
     * @return D4WTPEDITORDERREQ
     */
    public function getI_ctEditOrderReq()
    {
      return $this->i_ctEditOrderReq;
    }

    /**
     * @param D4WTPEDITORDERREQ $i_ctEditOrderReq
     * @return \Axess\Dci4Wtp\editOrderHeader
     */
    public function setI_ctEditOrderReq($i_ctEditOrderReq)
    {
      $this->i_ctEditOrderReq = $i_ctEditOrderReq;
      return $this;
    }

}
