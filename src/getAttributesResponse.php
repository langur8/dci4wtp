<?php

namespace Axess\Dci4Wtp;

class getAttributesResponse
{

    /**
     * @var D4WTPTRANSATTRIBUTESRES $getAttributesResult
     */
    protected $getAttributesResult = null;

    /**
     * @param D4WTPTRANSATTRIBUTESRES $getAttributesResult
     */
    public function __construct($getAttributesResult)
    {
      $this->getAttributesResult = $getAttributesResult;
    }

    /**
     * @return D4WTPTRANSATTRIBUTESRES
     */
    public function getGetAttributesResult()
    {
      return $this->getAttributesResult;
    }

    /**
     * @param D4WTPTRANSATTRIBUTESRES $getAttributesResult
     * @return \Axess\Dci4Wtp\getAttributesResponse
     */
    public function setGetAttributesResult($getAttributesResult)
    {
      $this->getAttributesResult = $getAttributesResult;
      return $this;
    }

}
