<?php

namespace Axess\Dci4Wtp;

class D4WTPGETRENTALITEMTARIFF3REQ
{

    /**
     * @var float $BCHECKAVAILCOUNT
     */
    protected $BCHECKAVAILCOUNT = null;

    /**
     * @var ArrayOfD4WTPRENTALITEMPRODUCT5 $CTRENTALITEMPRODUCT
     */
    protected $CTRENTALITEMPRODUCT = null;

    /**
     * @var float $NSESSIONID
     */
    protected $NSESSIONID = null;

    /**
     * @var float $NWTPPROFILENO
     */
    protected $NWTPPROFILENO = null;

    /**
     * @var string $SZFROM
     */
    protected $SZFROM = null;

    /**
     * @var string $SZTO
     */
    protected $SZTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBCHECKAVAILCOUNT()
    {
      return $this->BCHECKAVAILCOUNT;
    }

    /**
     * @param float $BCHECKAVAILCOUNT
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFF3REQ
     */
    public function setBCHECKAVAILCOUNT($BCHECKAVAILCOUNT)
    {
      $this->BCHECKAVAILCOUNT = $BCHECKAVAILCOUNT;
      return $this;
    }

    /**
     * @return ArrayOfD4WTPRENTALITEMPRODUCT5
     */
    public function getCTRENTALITEMPRODUCT()
    {
      return $this->CTRENTALITEMPRODUCT;
    }

    /**
     * @param ArrayOfD4WTPRENTALITEMPRODUCT5 $CTRENTALITEMPRODUCT
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFF3REQ
     */
    public function setCTRENTALITEMPRODUCT($CTRENTALITEMPRODUCT)
    {
      $this->CTRENTALITEMPRODUCT = $CTRENTALITEMPRODUCT;
      return $this;
    }

    /**
     * @return float
     */
    public function getNSESSIONID()
    {
      return $this->NSESSIONID;
    }

    /**
     * @param float $NSESSIONID
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFF3REQ
     */
    public function setNSESSIONID($NSESSIONID)
    {
      $this->NSESSIONID = $NSESSIONID;
      return $this;
    }

    /**
     * @return float
     */
    public function getNWTPPROFILENO()
    {
      return $this->NWTPPROFILENO;
    }

    /**
     * @param float $NWTPPROFILENO
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFF3REQ
     */
    public function setNWTPPROFILENO($NWTPPROFILENO)
    {
      $this->NWTPPROFILENO = $NWTPPROFILENO;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZFROM()
    {
      return $this->SZFROM;
    }

    /**
     * @param string $SZFROM
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFF3REQ
     */
    public function setSZFROM($SZFROM)
    {
      $this->SZFROM = $SZFROM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSZTO()
    {
      return $this->SZTO;
    }

    /**
     * @param string $SZTO
     * @return \Axess\Dci4Wtp\D4WTPGETRENTALITEMTARIFF3REQ
     */
    public function setSZTO($SZTO)
    {
      $this->SZTO = $SZTO;
      return $this;
    }

}
